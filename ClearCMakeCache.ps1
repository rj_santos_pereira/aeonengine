#Simple script to clear CMake cached files
Get-ChildItem * -Recurse -Filter CMakeCache.txt | Remove-Item
