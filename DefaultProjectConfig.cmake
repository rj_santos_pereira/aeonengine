message(STATUS "Project: ${PROJECT_NAME}")

# Solving Binaries directory weirdness:
# https://stackoverflow.com/questions/47175912/using-cmake-how-to-stop-the-debug-and-release-subdirectories

set_target_properties(${PROJECT_NAME} PROPERTIES RUNTIME_OUTPUT_DIRECTORY
	${CMAKE_SOURCE_DIR}/Binaries/${AEON_PLATFORM_NAME}/${AEON_BUILD_TYPE}/${PROJECT_NAME})
#set_target_properties(${PROJECT_NAME} PROPERTIES ARCHIVE_OUTPUT_DIRECTORY
#	${CMAKE_SOURCE_DIR}/Binaries/${AEON_PLATFORM_NAME}/${AEON_BUILD_TYPE}/$<IF:$<BOOL:${AEON_BASE_PROJECT}>,${AEON_BASE_PROJECT_NAME},${PROJECT_NAME}>)
#set_target_properties(${PROJECT_NAME} PROPERTIES LIBRARY_OUTPUT_DIRECTORY
#	${CMAKE_SOURCE_DIR}/Binaries/${AEON_PLATFORM_NAME}/${AEON_BUILD_TYPE}/$<IF:$<BOOL:${AEON_BASE_PROJECT}>,${AEON_BASE_PROJECT_NAME},${PROJECT_NAME}>)
set_target_properties(${PROJECT_NAME} PROPERTIES ARCHIVE_OUTPUT_DIRECTORY
	${CMAKE_SOURCE_DIR}/Binaries/${AEON_PLATFORM_NAME}/${AEON_BUILD_TYPE}/${PROJECT_NAME})
set_target_properties(${PROJECT_NAME} PROPERTIES LIBRARY_OUTPUT_DIRECTORY
	${CMAKE_SOURCE_DIR}/Binaries/${AEON_PLATFORM_NAME}/${AEON_BUILD_TYPE}/${PROJECT_NAME})

# TODO copy headers to binary folder if specific flag (TBD) is passed to build

# Clang docs: https://clang.llvm.org/docs/index.html

# MSVC conformance flags:
# https://docs.microsoft.com/en-us/cpp/build/reference/zc-conformance?view=msvc-170
# MSVC exception handling model:
# https://docs.microsoft.com/en-us/cpp/build/reference/eh-exception-handling-model?view=msvc-170
# MSVC preprocessor notes:
# https://docs.microsoft.com/en-us/cpp/preprocessor/preprocessor-experimental-overview?view=msvc-170

# TODO control certain compiler flags through CMake flags

if (AEON_PLATFORM_WINDOWS)
	# TODO use COMPILER_DRIVER to coalesce this into a single target_compile_options statement
	if (AEON_COMPILER_MSVC)
		target_compile_options(${PROJECT_NAME}
			PUBLIC
			#	/arch:AVX512 # Ryzen 9 5950X doesn't seem to support AVX512
				/arch:AVX2
			#	/arch:AVX

				/std:c++20

			#	/fp:fast
				/fp:precise
				/fp:contract
				/fp:except-
			#	/fp:strict

			#	/d2vzeroupper

				/MP
				/W4
			#	/GR- # no-RTTI
				/permissive-
				/volatile:iso
				/utf-8

				/wd4068 # -Wno-unknown-pragma
				/wd4141 # 'inline': used more than once
				/we4456 # -Werror=shadow

				# TODO See: https://learn.microsoft.com/en-us/cpp/build/reference/j-default-char-type-is-unsigned?view=msvc-170
				#/J
				/EHsc

				/Zc:__cplusplus
				/Zc:__STDC__
				/Zc:alignedNew
				/Zc:auto
				/Zc:char8_t
			#	/Zc:enumTypes
				/Zc:externC
				/Zc:externConstexpr
				/Zc:forScope
			#	/Zc:gotoScope
				/Zc:hiddenFriend
				/Zc:implicitNoexcept
				/Zc:inline
				/Zc:lambda
				/Zc:noexceptTypes
			#	/Zc:nrvo
				/Zc:preprocessor
				/Zc:referenceBinding
				/Zc:rvalueCast
				/Zc:sizedDealloc
				/Zc:strictStrings
				/Zc:static_assert
			#	/Zc:templateScope
				/Zc:ternary
				/Zc:threadSafeInit
				/Zc:throwingNew
				/Zc:tlsGuards
			#	/Zc:twoPhase
				/Zc:wchar_t
				/Zc:zeroSizeArrayNew
			)
	else()
		target_compile_options(${PROJECT_NAME}
			PUBLIC
			#	-march=native
			#	-mavx512f
			#	-mavx512vl
			#	-mavx512bw
			#	-mavx512dq
			#	-mavx512cd
				-mavx2
				-mavx

				-mfpmath=sse

				-fexec-charset=UTF-8
				-finput-charset=UTF-8

			#	TODO study this https://stackoverflow.com/questions/7420665/what-does-gccs-ffast-math-actually-do#22135559
			#	-ffast-math

				-std=c++20

			#	-fsanitize=undefined
				# TODO see https://github.com/google/sanitizers/wiki/AddressSanitizerWindowsPort#debugging for setup on Windows
			#	-fsanitize=address
				# NOTE not supported on Windows
			#	-fsanitize=leak

				# Default warnings
				-Wall
				-Wextra
				-Wpedantic

				# Extra warnings
				-Wnon-virtual-dtor

				# Disable certain warnings which aren't very helpful
				-Wno-unknown-pragmas
				-Wno-extra-semi
				# C++20 relaxed many usages of 'typename'
				-Wno-typename-missing

				# Promote certain warnings to errors
				-Werror=return-type
				-Werror=shadow
				# TODO make all MSVC extensions errors
				-Werror=microsoft-template
				-Werror=microsoft-inaccessible-base

				# Unused in Windows (always uses the msvcrt libs)
			#	-stdlib=libc++

			#	-fno-rtti
				-fno-delayed-template-parsing
				# Disable compatibility with MSVC
			#	FIXME currently not working; study this further, document pros and cons
			#	-fmsc-version=0

			#	-faligned-allocation
			#	-faligned-new
			#	-glldb
			)
	endif()

	# CHECKME https://stackoverflow.com/questions/49480535/how-to-differentiate-between-clang-and-clang-cl
	target_compile_options(${PROJECT_NAME}
		PUBLIC
			$<IF:$<BOOL:${AEON_COMPILER_DRIVER_GNU}>,-D,/D>AEON_USING_SIMD=1

			$<IF:$<BOOL:${AEON_COMPILER_DRIVER_GNU}>,-D,/D>_WIN32=1
			$<IF:$<BOOL:${AEON_COMPILER_DRIVER_GNU}>,-D,/D>WIN32_LEAN_AND_MEAN=1
			$<IF:$<BOOL:${AEON_COMPILER_DRIVER_GNU}>,-D,/D>_UNICODE=1
			$<IF:$<BOOL:${AEON_COMPILER_DRIVER_GNU}>,-D,/D>UNICODE=1
			$<IF:$<BOOL:${AEON_COMPILER_DRIVER_GNU}>,-D,/D>NOMINMAX=1
			$<IF:$<BOOL:${AEON_COMPILER_DRIVER_GNU}>,-D,/D>_CRT_SECURE_NO_WARNINGS=1
			$<IF:$<BOOL:${AEON_COMPILER_DRIVER_GNU}>,-D,/D>_SILENCE_CXX17_CODECVT_HEADER_DEPRECATION_WARNING=1
			$<IF:$<BOOL:${AEON_COMPILER_DRIVER_GNU}>,-D,/D>MICROSOFT_WINDOWS_WINBASE_H_DEFINE_INTERLOCKED_CPLUSPLUS_OVERLOADS=0
		)
else()
	# TODO other platforms
	message(FATAL_ERROR "Not implemented")
endif()

if(AEON_PLATFORM_WINDOWS)
	target_link_options(${PROJECT_NAME}
		PUBLIC
			$<IF:$<BOOL:${AEON_COMPILER_DRIVER_GNU}>,-Wl$<COMMA>-subsystem:windows,/subsystem:WINDOWS>
			$<IF:$<BOOL:${AEON_COMPILER_DRIVER_GNU}>,-Wl$<COMMA>-machine:x64,/machine:X64>
		)
else()
	# TODO other platforms
	message(FATAL_ERROR "Not implemented")
endif()

if(AEON_PLATFORM_WINDOWS)
	target_link_libraries(${PROJECT_NAME}
		PUBLIC
			kernel32.lib # Windows NT Base API
			advapi32.lib # Advanced Windows Base API
			user32.lib # Multi-User Windows USER API
			gdi32.lib # Graphics Device Interface (GDI)
			winmm.lib # Media Control Interface (MCI)
			shell32.lib # Windows Shell (GUI) Common
#			comdlg32.lib # Common Dialogs API
			dbghelp.lib # Windows Image Helper
#			ole32.lib # Object Linking and Embedding (OLE)
#			oleaut32.lib
#			uuid.lib
#			Synchronization.lib
		)
else()
	# TODO other platforms
	message(FATAL_ERROR "Not implemented")
endif()
