# Based on: https://matgomes.com/integrate-google-test-into-cmake/

#find_package(GTest REQUIRED)
message(STATUS "Project: googletest")
message(STATUS "Project: googlebenchmark")

include(FetchContent)

# Version 1.12.1
FetchContent_Declare(googletest
	GIT_REPOSITORY https://github.com/google/googletest.git
	GIT_TAG        release-1.12.1
	)
# Version 1.8.0
FetchContent_Declare(googlebenchmark
	GIT_REPOSITORY https://github.com/google/benchmark.git
	GIT_TAG        v1.8.0
	)

# Build libraries as static libraries
set(BUILD_SHARED_LIBS OFF)

# For Windows: Prevent overriding the parent project's compiler/linker settings
if (MSVC)
	set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
endif()

# Skip building the google/googletest target for installing
set(INSTALL_GTEST OFF)

# Build google/benchmark with exceptions disabled
#set(BENCHMARK_ENABLE_EXCEPTIONS OFF CACHE BOOL "" FORCE)

# Skip building the google/benchmark tests
set(BENCHMARK_ENABLE_GTEST_TESTS OFF)
set(BENCHMARK_ENABLE_TESTING OFF)
# Skip building the google/benchmark target for installing
set(BENCHMARK_ENABLE_INSTALL OFF)

# Change debug flags to compile the libraries as "production" even when build is "development" (need to match the CRT library)
if (AEON_PLATFORM_WINDOWS)
	if (AEON_COMPILER_MSVC)
		set(CMAKE_CXX_FLAGS_DEBUG "/MDd /Zi /O2 /Ob1 /DNDEBUG")
	elseif(AEON_COMPILER_CLANG)
		# NOTE linking to msvcrtd is not enough, _DEBUG must also be defined so that the library versions match (MSVC doesn't seem to care).
		set(CMAKE_CXX_FLAGS_DEBUG "-O2 -g -DNDEBUG -Xclang -gcodeview -D_DEBUG -D_DLL -D_MT -Xclang --dependent-lib=msvcrtd")
	else()
		message(FATAL_ERROR "Not implemented")
	endif()
else()
	message(FATAL_ERROR "Not implemented")
endif()

message(STATUS "Changing debug flags: ${CMAKE_CXX_FLAGS_DEBUG}")

FetchContent_MakeAvailable(googletest)
FetchContent_MakeAvailable(googlebenchmark)

set(CMAKE_CXX_FLAGS_DEBUG ${AEON_CXX_FLAGS_DEV})

message(STATUS "Restoring debug flags: ${CMAKE_CXX_FLAGS_DEBUG}")

#add_library(GTest::GTest INTERFACE IMPORTED)
#target_link_libraries(GTest::GTest INTERFACE gtest)

#add_library(GBenchmark::GBenchmark ALIAS benchmark)
#add_library(GBenchmark::GBenchmark INTERFACE IMPORTED)
#target_link_libraries(GBenchmark::GBenchmark INTERFACE benchmark::benchmark)

add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Aeon_Tests)
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Aeon_Benchmarks)
