#include <benchmark/benchmark.h>

#include "Aeon/Framework/Framework.hpp"

class HashMap_Benchmarks : public benchmark::Fixture
{
public:
	void SetUp(benchmark::State const& State) override
	{
		AEON_UNUSED(State);

	//	TestMap.SetLoadFactorLimit(0.99f);
	//	TestMap.Reserve(1'000'000'000);
	}

	void TearDown(benchmark::State const& State) override
	{
		AEON_UNUSED(State);
	}

	Aeon::GHashMap<uint32, Aeon::SString> TestMap;
};

BENCHMARK_F(HashMap_Benchmarks, HashMap_Aeon_Find_1_000_000_000)(benchmark::State& State)
{
	for (auto _ : State)
	{
	//	::benchmark::DoNotOptimize();
	}
}