#include <benchmark/benchmark.h>

#include "Aeon/Framework/Framework.hpp"

template <szint AlignValue = alignof(uint32)>
struct alignas(AlignValue) GVarAlignedInt
{
	bool operator==(GVarAlignedInt const&) const = default;
	strong_ordering operator<=>(GVarAlignedInt const&) const = default;

	uint32 Val;
};

class Array_Benchmarks : public benchmark::Fixture
{
public:
	void SetUp(benchmark::State const& State) override
	{
		AEON_UNUSED(State);
	}

	void TearDown(benchmark::State const& State) override
	{
		AEON_UNUSED(State);
	}

	void Run_LinearFind(benchmark::State& State, szint NumElements)
	{
		TestArray.Resize(NumElements);
		szint Index = 0;
		for (auto& Element : TestArray)
		{
			Element = GVarAlignedInt<>{ uint32(Index++) };
		}

		for (auto _ : State)
		{
			auto ValToFind = RNG.GenerateInRange(0, NumElements - 1);
			auto ElementOpt = TestArray.Find(GVarAlignedInt<>{ValToFind});
			::benchmark::DoNotOptimize(*ElementOpt);
		}
	}

	void Run_BinaryFind(benchmark::State& State, szint NumElements)
	{
		TestArray.Resize(NumElements);
		szint Index = 0;
		for (auto& Element : TestArray)
		{
			Element = GVarAlignedInt<>{ uint32(Index++) };
		}

		for (auto _ : State)
		{
			auto ValToFind = RNG.GenerateInRange(0, NumElements - 1);
			auto ElementOpt = TestArray.FindSorted(GVarAlignedInt<>{ValToFind});
			::benchmark::DoNotOptimize(*ElementOpt);
		}
	}

	Aeon::GRandomGenerator<uint32> RNG;
	Aeon::GArray<GVarAlignedInt<>> TestArray;
};

BENCHMARK_F(Array_Benchmarks, Aeon_Array_LinearFind_10)(benchmark::State& State)
{
	Run_LinearFind(State, 10);
}

BENCHMARK_F(Array_Benchmarks, Aeon_Array_LinearFind_20)(benchmark::State& State)
{
	Run_LinearFind(State, 20);
}

BENCHMARK_F(Array_Benchmarks, Aeon_Array_LinearFind_40)(benchmark::State& State)
{
	Run_LinearFind(State, 40);
}

BENCHMARK_F(Array_Benchmarks, Aeon_Array_LinearFind_80)(benchmark::State& State)
{
	Run_LinearFind(State, 80);
}

BENCHMARK_F(Array_Benchmarks, Aeon_Array_LinearFind_100)(benchmark::State& State)
{
	Run_LinearFind(State, 100);
}

BENCHMARK_F(Array_Benchmarks, Aeon_Array_LinearFind_120)(benchmark::State& State)
{
	Run_LinearFind(State, 120);
}

BENCHMARK_F(Array_Benchmarks, Aeon_Array_LinearFind_140)(benchmark::State& State)
{
	Run_LinearFind(State, 140);
}

BENCHMARK_F(Array_Benchmarks, Aeon_Array_LinearFind_160)(benchmark::State& State)
{
	Run_LinearFind(State, 160);
}

BENCHMARK_F(Array_Benchmarks, Aeon_Array_LinearFind_180)(benchmark::State& State)
{
	Run_LinearFind(State, 180);
}

BENCHMARK_F(Array_Benchmarks, Aeon_Array_LinearFind_200)(benchmark::State& State)
{
	Run_LinearFind(State, 200);
}

BENCHMARK_F(Array_Benchmarks, Aeon_Array_LinearFind_220)(benchmark::State& State)
{
	Run_LinearFind(State, 220);
}

BENCHMARK_F(Array_Benchmarks, Aeon_Array_LinearFind_240)(benchmark::State& State)
{
	Run_LinearFind(State, 240);
}

BENCHMARK_F(Array_Benchmarks, Aeon_Array_LinearFind_260)(benchmark::State& State)
{
	Run_LinearFind(State, 260);
}

BENCHMARK_F(Array_Benchmarks, Aeon_Array_LinearFind_280)(benchmark::State& State)
{
	Run_LinearFind(State, 280);
}

BENCHMARK_F(Array_Benchmarks, Aeon_Array_LinearFind_300)(benchmark::State& State)
{
	Run_LinearFind(State, 300);
}

BENCHMARK_F(Array_Benchmarks, Aeon_Array_LinearFind_320)(benchmark::State& State)
{
	Run_LinearFind(State, 320);
}

BENCHMARK_F(Array_Benchmarks, Aeon_Array_LinearFind_640)(benchmark::State& State)
{
	Run_LinearFind(State, 640);
}

BENCHMARK_F(Array_Benchmarks, Aeon_Array_LinearFind_1280)(benchmark::State& State)
{
	Run_LinearFind(State, 1280);
}



BENCHMARK_F(Array_Benchmarks, Aeon_Array_BinaryFind_10)(benchmark::State& State)
{
	Run_BinaryFind(State, 10);
}

BENCHMARK_F(Array_Benchmarks, Aeon_Array_BinaryFind_20)(benchmark::State& State)
{
	Run_BinaryFind(State, 20);
}

BENCHMARK_F(Array_Benchmarks, Aeon_Array_BinaryFind_40)(benchmark::State& State)
{
	Run_BinaryFind(State, 40);
}

BENCHMARK_F(Array_Benchmarks, Aeon_Array_BinaryFind_80)(benchmark::State& State)
{
	Run_BinaryFind(State, 80);
}

BENCHMARK_F(Array_Benchmarks, Aeon_Array_BinaryFind_100)(benchmark::State& State)
{
	Run_BinaryFind(State, 100);
}

BENCHMARK_F(Array_Benchmarks, Aeon_Array_BinaryFind_120)(benchmark::State& State)
{
	Run_BinaryFind(State, 120);
}

BENCHMARK_F(Array_Benchmarks, Aeon_Array_BinaryFind_140)(benchmark::State& State)
{
	Run_BinaryFind(State, 140);
}

BENCHMARK_F(Array_Benchmarks, Aeon_Array_BinaryFind_160)(benchmark::State& State)
{
	Run_BinaryFind(State, 160);
}

BENCHMARK_F(Array_Benchmarks, Aeon_Array_BinaryFind_180)(benchmark::State& State)
{
	Run_BinaryFind(State, 180);
}

BENCHMARK_F(Array_Benchmarks, Aeon_Array_BinaryFind_200)(benchmark::State& State)
{
	Run_BinaryFind(State, 200);
}

BENCHMARK_F(Array_Benchmarks, Aeon_Array_BinaryFind_220)(benchmark::State& State)
{
	Run_BinaryFind(State, 220);
}

BENCHMARK_F(Array_Benchmarks, Aeon_Array_BinaryFind_240)(benchmark::State& State)
{
	Run_BinaryFind(State, 240);
}

BENCHMARK_F(Array_Benchmarks, Aeon_Array_BinaryFind_260)(benchmark::State& State)
{
	Run_BinaryFind(State, 260);
}

BENCHMARK_F(Array_Benchmarks, Aeon_Array_BinaryFind_280)(benchmark::State& State)
{
	Run_BinaryFind(State, 280);
}

BENCHMARK_F(Array_Benchmarks, Aeon_Array_BinaryFind_300)(benchmark::State& State)
{
	Run_BinaryFind(State, 300);
}

BENCHMARK_F(Array_Benchmarks, Aeon_Array_BinaryFind_320)(benchmark::State& State)
{
	Run_BinaryFind(State, 320);
}

BENCHMARK_F(Array_Benchmarks, Aeon_Array_BinaryFind_640)(benchmark::State& State)
{
	Run_BinaryFind(State, 640);
}

BENCHMARK_F(Array_Benchmarks, Aeon_Array_BinaryFind_1280)(benchmark::State& State)
{
	Run_BinaryFind(State, 1280);
}
