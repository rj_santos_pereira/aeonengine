#include <benchmark/benchmark.h>

#include "Aeon/Framework/Framework.hpp"

// NOTE: Clang's codegen is wicked smart... so even when we were dynamically creating the strings, because they were defined in source,
//  Clang could figure out their size, and just emitted that straight away.
void LoadData(char* Buffer, szint DataLineIndex, benchmark::State& State)
{
	auto CurrentWorkingDirectory = std::filesystem::path(__FILE__).parent_path();
	auto DataFilePath = CurrentWorkingDirectory / "StringData_Benchmarks.txt";

	if (!exists(DataFilePath))
	{
		State.SkipWithError("Failed to load data: Couldn't find file");
		return;
	}

	bool HasData = false;
	szint CurrDataLineIndex = 0;

	std::ifstream DataFile{ DataFilePath };
	std::string DataLine;

	while (std::getline(DataFile, DataLine))
	{
		if (DataLine.empty())
		{
			continue;
		}

		for (auto Char : DataLine)
		{
			if (Char == ';')
			{
				++CurrDataLineIndex;
				break;
			}

			if (CurrDataLineIndex == DataLineIndex)
			{
				if (Char != ' ' && Char != ',' && Char != '\0')
				{
					*Buffer++ = Char;
					HasData = true;
				}
			}
		}
	}

	if (!HasData)
	{
		State.SkipWithError("Failed to load data: Couldn't find the data index");
		return;
	}

	*Buffer++ = '\0';
}

template <Aeon::CCharacter CharType>
static constexpr szint Length_Naive(CharType const* StrPtr)
{
	szint StrLen = 0;
	while (*StrPtr++ != Aeon::StringOps::NullTerminator<CharType>)
	{
		++StrLen;
	}
	return StrLen;
}

class String_Benchmarks : public benchmark::Fixture
{
public:
	void SetUp(benchmark::State const& State) override
	{
		AEON_UNUSED(State);
		Buffer = new char[10'000];
	}
	
	void TearDown(benchmark::State const& State) override
	{
		AEON_UNUSED(State);
		delete[] Buffer;
	}

	void Run_StringOps_LengthNaive(benchmark::State& State, szint DataIndex)
	{
		LoadData(Buffer, DataIndex, State);

		szint LengthExample = 0;
		for (auto _ : State)
		{
			auto Length = Length_Naive(Buffer);
			LengthExample = Length;
			::benchmark::DoNotOptimize(Length);
		}

		State.counters["NumElems"] = float64(LengthExample);
	}

	void Run_StringOps_LengthGlibc(benchmark::State& State, szint DataIndex)
	{
		LoadData(Buffer, DataIndex, State);
		
		szint LengthExample = 0;
		for (auto _ : State)
		{
			auto Length = std::strlen(Buffer);
			LengthExample = Length;
			::benchmark::DoNotOptimize(Length);
		}

		State.counters["NumElems"] = float64(LengthExample);
	}

	void Run_StringOps_Length(benchmark::State& State, szint DataIndex)
	{
		LoadData(Buffer, DataIndex, State);
		
		szint LengthExample = 0;
		for (auto _ : State)
		{
			auto Length = Aeon::StringOps::Length(Buffer);
			LengthExample = Length;
			::benchmark::DoNotOptimize(Length);
		}

		State.counters["NumElems"] = float64(LengthExample);
	}
	
	void Run_String_Normalize(benchmark::State& State, szint DataIndex)
	{
		LoadData(Buffer, DataIndex, State);

		Aeon::STextString String{ Buffer };
		
		for (auto _ : State)
		{
			String.Normalize();
			::benchmark::DoNotOptimize(String);
		}
	}

private:
	char* Buffer = new char[10'000];
};

BENCHMARK_F(String_Benchmarks, StringOps_LengthNaive_ASCII_10)(benchmark::State& State) 
{
	Run_StringOps_LengthNaive(State, 1);
}
BENCHMARK_F(String_Benchmarks, StringOps_LengthNaive_ASCII_50)(benchmark::State& State) 
{
	Run_StringOps_LengthNaive(State, 2);
}
BENCHMARK_F(String_Benchmarks, StringOps_LengthNaive_ASCII_100)(benchmark::State& State) 
{
	Run_StringOps_LengthNaive(State, 3);
}
BENCHMARK_F(String_Benchmarks, StringOps_LengthNaive_ASCII_500)(benchmark::State& State) 
{
	Run_StringOps_LengthNaive(State, 4);
}
BENCHMARK_F(String_Benchmarks, StringOps_LengthNaive_ASCII_1000)(benchmark::State& State) 
{
	Run_StringOps_LengthNaive(State, 5);
}
BENCHMARK_F(String_Benchmarks, StringOps_LengthNaive_ASCII_5000)(benchmark::State& State) 
{
	Run_StringOps_LengthNaive(State, 6);
}

BENCHMARK_F(String_Benchmarks, StringOps_LengthGlibc_ASCII_10)(benchmark::State& State)
{
	Run_StringOps_LengthGlibc(State, 1);
}
BENCHMARK_F(String_Benchmarks, StringOps_LengthGlibc_ASCII_50)(benchmark::State& State)
{
	Run_StringOps_LengthGlibc(State, 2);
}
BENCHMARK_F(String_Benchmarks, StringOps_LengthGlibc_ASCII_100)(benchmark::State& State)
{
	Run_StringOps_LengthGlibc(State, 3);
}
BENCHMARK_F(String_Benchmarks, StringOps_LengthGlibc_ASCII_500)(benchmark::State& State)
{
	Run_StringOps_LengthGlibc(State, 4);
}
BENCHMARK_F(String_Benchmarks, StringOps_LengthGlibc_ASCII_1000)(benchmark::State& State)
{
	Run_StringOps_LengthGlibc(State, 5);
}
BENCHMARK_F(String_Benchmarks, StringOps_LengthGlibc_ASCII_5000)(benchmark::State& State)
{
	Run_StringOps_LengthGlibc(State, 6);
}

BENCHMARK_F(String_Benchmarks, StringOps_Length_ASCII_10)(benchmark::State& State)
{
	Run_StringOps_Length(State, 1);
}
BENCHMARK_F(String_Benchmarks, StringOps_Length_ASCII_50)(benchmark::State& State)
{
	Run_StringOps_Length(State, 2);
}
BENCHMARK_F(String_Benchmarks, StringOps_Length_ASCII_100)(benchmark::State& State)
{
	Run_StringOps_Length(State, 3);
}
BENCHMARK_F(String_Benchmarks, StringOps_Length_ASCII_500)(benchmark::State& State)
{
	Run_StringOps_Length(State, 4);
}
BENCHMARK_F(String_Benchmarks, StringOps_Length_ASCII_1000)(benchmark::State& State)
{
	Run_StringOps_Length(State, 5);
}
BENCHMARK_F(String_Benchmarks, StringOps_Length_ASCII_5000)(benchmark::State& State)
{
	Run_StringOps_Length(State, 6);
}

BENCHMARK_F(String_Benchmarks, String_Normalize_UTF8_10)(benchmark::State& State)
{
	Run_String_Normalize(State, 1);
}
BENCHMARK_F(String_Benchmarks, String_Normalize_UTF8_50)(benchmark::State& State)
{
	Run_String_Normalize(State, 2);
}
BENCHMARK_F(String_Benchmarks, String_Normalize_UTF8_100)(benchmark::State& State)
{
	Run_String_Normalize(State, 3);
}
BENCHMARK_F(String_Benchmarks, String_Normalize_UTF8_500)(benchmark::State& State)
{
	Run_String_Normalize(State, 4);
}
BENCHMARK_F(String_Benchmarks, String_Normalize_UTF8_1000)(benchmark::State& State)
{
	Run_String_Normalize(State, 5);
}
BENCHMARK_F(String_Benchmarks, String_Normalize_UTF8_5000)(benchmark::State& State)
{
	Run_String_Normalize(State, 6);
}
