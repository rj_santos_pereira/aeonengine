project(AeonFrameworkBenchmarks CXX)

add_executable(${PROJECT_NAME})

include_directories(${CMAKE_SOURCE_DIR}/Source)

include(${CMAKE_SOURCE_DIR}/DefaultProjectConfig.cmake)

# FIXME at some point when reworking the tests, make this depend on Framework only
target_link_libraries(${PROJECT_NAME}
	PUBLIC
		benchmark::benchmark
		AeonEngine
	)

add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Framework_Benchmarks)
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Misc_Benchmarks)

target_sources(${PROJECT_NAME}
	PRIVATE
		${CMAKE_CURRENT_SOURCE_DIR}/LaunchBenchmarks.cpp
	)
