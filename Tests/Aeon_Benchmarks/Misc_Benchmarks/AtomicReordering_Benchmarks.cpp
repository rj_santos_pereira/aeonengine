#include "benchmark/benchmark.h"

#include "Aeon/Framework/Framework.hpp"

class AtomicReordering_Benchmark : public benchmark::Fixture
{
public:
	void SetUp(benchmark::State const& State) override
	{
		AEON_UNUSED(State);
	}

	void TearDown(benchmark::State const& State) override
	{
		AEON_UNUSED(State);
	}

	template <Aeon::CFunctionPointer ThreadFunc1Type, Aeon::CFunctionPointer ThreadFunc2Type>
	void Run_Test(ThreadFunc1Type ThreadFunc1, ThreadFunc2Type ThreadFunc2, benchmark::State& State)
	{
		static bool ConcurrencyGuard = true;

		AEON_ASSERT(ConcurrencyGuard);
		ConcurrencyGuard = false;

		Aeon::SThread Thread1{ Aeon::DThreadParams{ .Name = "AtomicReordering_Benchmark_Thread1" } };
		Aeon::SThread Thread2{ Aeon::DThreadParams{ .Name = "AtomicReordering_Benchmark_Thread2" } };

		Aeon::SSemaphore Start1, Start2, Stop;

		Aeon::GAtomicUint32<Aeon::EMemoryOrder::Relaxed> X, Y;
		int32 volatile R1, R2;

		Thread1.Start(ThreadFunc1, &Start1, &Stop, &X, &Y, &R1);
		Thread2.Start(ThreadFunc2, &Start2, &Stop, &X, &Y, &R2);

		szint NumReorderings = 0;
		for (auto _ : State)
		{
			// Init/Reset values for current iteration
			X.template Store<Aeon::EMemoryOrder::Relaxed>(0);
			Y.template Store<Aeon::EMemoryOrder::Relaxed>(0);

			R1 = 0;
			R2 = 0;

			// Sync the values to the threads that will run the test
			Start1.Release();
			Start2.Release();

			// Wait for the threads to finish running the test and sync the results
			Stop.Acquire();
			Stop.Acquire();

			::benchmark::DoNotOptimize(R1);
			::benchmark::DoNotOptimize(R2);

			// Check the results, if both values are 0, there was a store-load reordering (by the CPU scheduler)
			if (R1 == R2 && R1 == 0)
			{
				++NumReorderings;
			}
		}

		Thread1.RequestStop();
		Thread2.RequestStop();

		Start1.Release();
		Start2.Release();

		Thread1.Join();
		Thread2.Join();

		State.counters["Reorderings"] = float64(NumReorderings);

		ConcurrencyGuard = true;
	}

	template <Aeon::EMemoryOrder StoreMemoryOrderValue,
	    	  Aeon::EMemoryOrder LoadMemoryOrderValue>
	static void Run_StoreLoad_Thread1(Aeon::SSemaphore* Start, Aeon::SSemaphore* Stop,
									  Aeon::GAtomicUint32<Aeon::EMemoryOrder::Relaxed>* X, 
									  Aeon::GAtomicUint32<Aeon::EMemoryOrder::Relaxed>* Y,
									  int32 volatile* R1)
	{
		Aeon::GRandomGenerator<uint32> Random;
		while (!Aeon::SThread::ShouldStop())
		{
			// Wait for main thread to init/reset values for current iteration
			Start->Acquire();

			// Produce random jitter in execution cycles
			while (Random() % 16 != 0);

			// /////////////////////////////////////////////////////////////////////////////////////////////////////////
			// Now run the actual test

			X->template Store<StoreMemoryOrderValue>(1);
			*R1 = Y->template Load<LoadMemoryOrderValue>();

			// /////////////////////////////////////////////////////////////////////////////////////////////////////////

		//	benchmark::DoNotOptimize(*R1);

			// Sync results of current iteration to main thread
			Stop->Release();
		}
	}

	template <Aeon::EMemoryOrder StoreMemoryOrderValue,
			  Aeon::EMemoryOrder LoadMemoryOrderValue>
	static void Run_StoreLoad_Thread2(Aeon::SSemaphore* Start, Aeon::SSemaphore* Stop,
									  Aeon::GAtomicUint32<Aeon::EMemoryOrder::Relaxed>* X,
									  Aeon::GAtomicUint32<Aeon::EMemoryOrder::Relaxed>* Y,
									  int32 volatile* R2)
	{
		Aeon::GRandomGenerator<uint32> Random;
		while (!Aeon::SThread::ShouldStop())
		{
			// Wait for main thread to init/reset values for current iteration
			Start->Acquire();

			// Produce random jitter in execution cycles
			while (Random() % 16 != 0);

			// /////////////////////////////////////////////////////////////////////////////////////////////////////////
			// Now run the actual test

			Y->template Store<StoreMemoryOrderValue>(1);
			*R2 = X->template Load<LoadMemoryOrderValue>();

			// /////////////////////////////////////////////////////////////////////////////////////////////////////////

		//	benchmark::DoNotOptimize(*R2);

			// Sync results of current iteration to main thread
			Stop->Release();
		}
	}

	template <Aeon::EMemoryOrder RMWMemoryOrderValue,
			  Aeon::EMemoryOrder LoadMemoryOrderValue>
	static void Run_ExchangeLoad_Thread1(Aeon::SSemaphore* Start, Aeon::SSemaphore* Stop,
										 Aeon::GAtomicUint32<Aeon::EMemoryOrder::Relaxed>* X,
										 Aeon::GAtomicUint32<Aeon::EMemoryOrder::Relaxed>* Y,
										 int32 volatile* R1)
	{
		Aeon::GRandomGenerator<uint32> Random;
		while (!Aeon::SThread::ShouldStop())
		{
			// Wait for main thread to init/reset values for current iteration
			Start->Acquire();

			// Produce random jitter in execution cycles
			while (Random() % 16 != 0);

			// /////////////////////////////////////////////////////////////////////////////////////////////////////////
			// Now run the actual test

			X->template Exchange<RMWMemoryOrderValue>(1);
			*R1 = Y->template Load<LoadMemoryOrderValue>();

			// /////////////////////////////////////////////////////////////////////////////////////////////////////////

		//	benchmark::DoNotOptimize(*R1);

			// Sync results of current iteration to main thread
			Stop->Release();
		}
	}

	template <Aeon::EMemoryOrder RMWMemoryOrderValue, Aeon::EMemoryOrder LoadMemoryOrderValue>
	static void Run_ExchangeLoad_Thread2(Aeon::SSemaphore* Start, Aeon::SSemaphore* Stop,
										 Aeon::GAtomicUint32<Aeon::EMemoryOrder::Relaxed>* X,
										 Aeon::GAtomicUint32<Aeon::EMemoryOrder::Relaxed>* Y,
										 int32 volatile* R2)
	{
		Aeon::GRandomGenerator<uint32> Random;
		while (!Aeon::SThread::ShouldStop())
		{
			// Wait for main thread to init/reset values for current iteration
			Start->Acquire();

			// Produce random jitter in execution cycles
			while (Random() % 16 != 0);

			// /////////////////////////////////////////////////////////////////////////////////////////////////////////
			// Now run the actual test

			Y->template Exchange<RMWMemoryOrderValue>(1);
			*R2 = X->template Load<LoadMemoryOrderValue>();

			// /////////////////////////////////////////////////////////////////////////////////////////////////////////

		//	benchmark::DoNotOptimize(*R2);

			// Sync results of current iteration to main thread
			Stop->Release();
		}
	}

	template <Aeon::EMemoryOrder StoreMemoryOrderValue,
			  Aeon::EMemoryOrder RMWMemoryOrderValue>
	static void Run_StoreExchange_Thread1(Aeon::SSemaphore* Start, Aeon::SSemaphore* Stop,
										  Aeon::GAtomicUint32<Aeon::EMemoryOrder::Relaxed>* X,
										  Aeon::GAtomicUint32<Aeon::EMemoryOrder::Relaxed>* Y,
										  int32 volatile* R1)
	{
		Aeon::GRandomGenerator<uint32> Random;
		while (!Aeon::SThread::ShouldStop())
		{
			// Wait for main thread to init/reset values for current iteration
			Start->Acquire();

			// Produce random jitter in execution cycles
			while (Random() % 16 != 0);

			// /////////////////////////////////////////////////////////////////////////////////////////////////////////
			// Now run the actual test

			X->template Store<StoreMemoryOrderValue>(1);
			*R1 = Y->template Exchange<RMWMemoryOrderValue>(2);

			// /////////////////////////////////////////////////////////////////////////////////////////////////////////

		//	benchmark::DoNotOptimize(*R1);

			// Sync results of current iteration to main thread
			Stop->Release();
		}
	}

	template <Aeon::EMemoryOrder StoreMemoryOrderValue,
			  Aeon::EMemoryOrder RMWMemoryOrderValue>
	static void Run_StoreExchange_Thread2(Aeon::SSemaphore* Start, Aeon::SSemaphore* Stop,
										  Aeon::GAtomicUint32<Aeon::EMemoryOrder::Relaxed>* X,
										  Aeon::GAtomicUint32<Aeon::EMemoryOrder::Relaxed>* Y,
										  int32 volatile* R2)
	{
		Aeon::GRandomGenerator<uint32> Random;
		while (!Aeon::SThread::ShouldStop())
		{
			// Wait for main thread to init/reset values for current iteration
			Start->Acquire();

			// Produce random jitter in execution cycles
			while (Random() % 16 != 0);

			// /////////////////////////////////////////////////////////////////////////////////////////////////////////
			// Now run the actual test

			Y->template Store<StoreMemoryOrderValue>(1);
			*R2 = X->template Exchange<RMWMemoryOrderValue>(2);

			// /////////////////////////////////////////////////////////////////////////////////////////////////////////

		//	benchmark::DoNotOptimize(*R2);

			// Sync results of current iteration to main thread
			Stop->Release();
		}
	}

	template <Aeon::EMemoryOrder RMWMemoryOrderValue, Aeon::EMemoryOrder>
	static void Run_RMW_Coherence_Thread1(Aeon::SSemaphore* Start, Aeon::SSemaphore* Stop,
										  Aeon::GAtomicUint32<Aeon::EMemoryOrder::Relaxed>* X,
										  Aeon::GAtomicUint32<Aeon::EMemoryOrder::Relaxed>* Y,
										  int32 volatile* R1)
	{

		AEON_UNUSED(Y);

		Aeon::GRandomGenerator<uint32> Random;
		while (!Aeon::SThread::ShouldStop())
		{
			// Wait for main thread to init/reset values for current iteration
			Start->Acquire();

			// Produce random jitter in execution cycles
			while (Random() % 16 != 0);

			// /////////////////////////////////////////////////////////////////////////////////////////////////////////
			// Now run the actual test

			*R1 = X->template Exchange<RMWMemoryOrderValue>(1);

			// /////////////////////////////////////////////////////////////////////////////////////////////////////////

		//	benchmark::DoNotOptimize(*R1);

			// Sync results of current iteration to main thread
			Stop->Release();
		}
	}

	template <Aeon::EMemoryOrder RMWMemoryOrderValue, Aeon::EMemoryOrder>
	static void Run_RMW_Coherence_Thread2(Aeon::SSemaphore* Start, Aeon::SSemaphore* Stop,
										  Aeon::GAtomicUint32<Aeon::EMemoryOrder::Relaxed>* X,
										  Aeon::GAtomicUint32<Aeon::EMemoryOrder::Relaxed>* Y,
										  int32 volatile* R2)
	{

		AEON_UNUSED(Y);

		Aeon::GRandomGenerator<uint32> Random;
		while (!Aeon::SThread::ShouldStop())
		{
			// Wait for main thread to init/reset values for current iteration
			Start->Acquire();

			// Produce random jitter in execution cycles
			while (Random() % 16 != 0);

			// /////////////////////////////////////////////////////////////////////////////////////////////////////////
			// Now run the actual test

			*R2 = X->template Exchange<RMWMemoryOrderValue>(2);

			// /////////////////////////////////////////////////////////////////////////////////////////////////////////

		//	benchmark::DoNotOptimize(*R2);

			// Sync results of current iteration to main thread
			Stop->Release();
		}
	}
};

BENCHMARK_DEFINE_F(AtomicReordering_Benchmark, StoreLoad_AcqRel)(benchmark::State& State)
{
	auto ThreadFunc1 = &Run_StoreLoad_Thread1<Aeon::EMemoryOrder::Release, Aeon::EMemoryOrder::Acquire>;
	auto ThreadFunc2 = &Run_StoreLoad_Thread2<Aeon::EMemoryOrder::Release, Aeon::EMemoryOrder::Acquire>;
	Run_Test(ThreadFunc1, ThreadFunc2, State);
}
BENCHMARK_REGISTER_F(AtomicReordering_Benchmark, StoreLoad_AcqRel);//->Threads(1);

BENCHMARK_DEFINE_F(AtomicReordering_Benchmark, StoreLoad_SeqCst)(benchmark::State& State)
{
	auto ThreadFunc1 = &Run_StoreLoad_Thread1<Aeon::EMemoryOrder::SeqCst, Aeon::EMemoryOrder::SeqCst>;
	auto ThreadFunc2 = &Run_StoreLoad_Thread2<Aeon::EMemoryOrder::SeqCst, Aeon::EMemoryOrder::SeqCst>;
	Run_Test(ThreadFunc1, ThreadFunc2, State);
}
BENCHMARK_REGISTER_F(AtomicReordering_Benchmark, StoreLoad_SeqCst);//->Threads(1);

BENCHMARK_DEFINE_F(AtomicReordering_Benchmark, StoreLoad_SeqCst_RelaxedLoads)(benchmark::State& State)
{
	auto ThreadFunc1 = &Run_StoreLoad_Thread1<Aeon::EMemoryOrder::SeqCst, Aeon::EMemoryOrder::Relaxed>;
	auto ThreadFunc2 = &Run_StoreLoad_Thread2<Aeon::EMemoryOrder::SeqCst, Aeon::EMemoryOrder::Relaxed>;
	Run_Test(ThreadFunc1, ThreadFunc2, State);
}
BENCHMARK_REGISTER_F(AtomicReordering_Benchmark, StoreLoad_SeqCst_RelaxedLoads);//->Threads(1);

BENCHMARK_DEFINE_F(AtomicReordering_Benchmark, StoreLoad_SeqCst_RelaxedStores)(benchmark::State& State)
{
	auto ThreadFunc1 = &Run_StoreLoad_Thread1<Aeon::EMemoryOrder::Relaxed, Aeon::EMemoryOrder::SeqCst>;
	auto ThreadFunc2 = &Run_StoreLoad_Thread2<Aeon::EMemoryOrder::Relaxed, Aeon::EMemoryOrder::SeqCst>;
	Run_Test(ThreadFunc1, ThreadFunc2, State);
}
BENCHMARK_REGISTER_F(AtomicReordering_Benchmark, StoreLoad_SeqCst_RelaxedStores);//->Threads(1);

BENCHMARK_DEFINE_F(AtomicReordering_Benchmark, ExchangeLoad_AcqRel)(benchmark::State& State)
{
	auto ThreadFunc1 = &Run_ExchangeLoad_Thread1<Aeon::EMemoryOrder::Release, Aeon::EMemoryOrder::Acquire>;
	auto ThreadFunc2 = &Run_ExchangeLoad_Thread2<Aeon::EMemoryOrder::Release, Aeon::EMemoryOrder::Acquire>;
	Run_Test(ThreadFunc1, ThreadFunc2, State);
}
BENCHMARK_REGISTER_F(AtomicReordering_Benchmark, ExchangeLoad_AcqRel);//->Threads(1);

BENCHMARK_DEFINE_F(AtomicReordering_Benchmark, StoreExchange_AcqRel)(benchmark::State& State)
{
	auto ThreadFunc1 = &Run_StoreExchange_Thread1<Aeon::EMemoryOrder::Release, Aeon::EMemoryOrder::Acquire>;
	auto ThreadFunc2 = &Run_StoreExchange_Thread2<Aeon::EMemoryOrder::Release, Aeon::EMemoryOrder::Acquire>;
	Run_Test(ThreadFunc1, ThreadFunc2, State);
}
BENCHMARK_REGISTER_F(AtomicReordering_Benchmark, StoreExchange_AcqRel);//->Threads(1);

BENCHMARK_DEFINE_F(AtomicReordering_Benchmark, RMW_Coherence_Relaxed)(benchmark::State& State)
{
	auto ThreadFunc1 = &Run_RMW_Coherence_Thread1<Aeon::EMemoryOrder::Relaxed, Aeon::EMemoryOrder::Relaxed>;
	auto ThreadFunc2 = &Run_RMW_Coherence_Thread2<Aeon::EMemoryOrder::Relaxed, Aeon::EMemoryOrder::Relaxed>;
	Run_Test(ThreadFunc1, ThreadFunc2, State);
}
BENCHMARK_REGISTER_F(AtomicReordering_Benchmark, RMW_Coherence_Relaxed);//->Threads(1);
