#include <benchmark/benchmark.h>

#include "Aeon/Engine/Platform/Launcher/WindowsLauncherHelpers.hpp"

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR pCmdLine, int nCmdShow)
{
	AEON_UNUSED(hInstance, hPrevInstance, pCmdLine, nCmdShow);

	int32 Argc;
	char** Argv;
	wchar** WArgv;

	Aeon::WindowsLauncherHelpers::AllocCommandLineArgs(Argc, Argv, WArgv);

//	extern szint AtomicReordering_Benchmarks2();
//	szint NumReorders = AtomicReordering_Benchmarks2();
//	AEON_DEBUGGER_FORMAT_PRINT(Warn, "NumReorders={}", NumReorders);

	// googlebenchmark specific code
	{
		using namespace ::benchmark;

		Initialize(&Argc, Argv);
		ReportUnrecognizedArguments(Argc, Argv);
		RunSpecifiedBenchmarks();
		Shutdown();
	}

	Aeon::WindowsLauncherHelpers::DeallocCommandLineArgs(Argc, Argv, WArgv);

	return EXIT_SUCCESS;
}
