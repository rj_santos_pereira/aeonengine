#include <gtest/gtest.h>

#include "Aeon/Framework/Framework.hpp"

using namespace Aeon;

TEST(CyclicArray_Tests, CtorDefault_ReservesMinimum)
{
	GDearray<int32>::AllocatorType MockAlloc;
	GDearray<int32> MockArr;
	
	uint32 AllocCount = /*AeonMath::NextPowerOfTwo*/(MockAlloc.RecommendedStartingAllocationSize());

	EXPECT_EQ(MockArr.AllocatedCount(), AllocCount);
}

TEST(CyclicArray_Tests, CtorDefault_CountIsZero)
{
	GDearray<int32> MockArr;

	EXPECT_EQ(MockArr.Count(), 0_sz);
}

TEST(CyclicArray_Tests, CtorReserve_ReservesZero)
{
	GDearray<int32> MockArr(TReserveStorage::Tag, 0);

	EXPECT_EQ(MockArr.AllocatedCount(), 0_sz);
}

TEST(CyclicArray_Tests, CtorReserve_ReservesZero_CountIsZero)
{
	GDearray<int32> MockArr(TReserveStorage::Tag, 0);

	EXPECT_EQ(MockArr.Count(), 0_sz);
}

TEST(CyclicArray_Tests, CtorReserve_ReservesNonZero)
{
	GDearray<int32> MockArr(TReserveStorage::Tag, 13);

	uint32 AllocCount = /*AeonMath::NextPowerOfTwo*/(13_u32);

	EXPECT_EQ(MockArr.AllocatedCount(), AllocCount);
}

TEST(CyclicArray_Tests, CtorReserve_ReservesNonZero_CountIsZero)
{
	GDearray<int32> MockArr(TReserveStorage::Tag, 13);

	EXPECT_EQ(MockArr.Count(), 0_sz);
}

TEST(CyclicArray_Tests, CtorReserve_ReservesBelowMinimum)
{
	GDearray<int32>::AllocatorType MockAlloc;

	uint32 MinAllocCount = MockAlloc.RecommendedStartingAllocationSize();
	uint32 AllocCount = (MinAllocCount >> 1) - 1;

	GDearray<int32> MockArr(TReserveStorage::Tag, AllocCount);

//	AllocCount = AeonMath::NextPowerOfTwo(AllocCount);

	EXPECT_EQ(MockArr.AllocatedCount(), AllocCount);
}

TEST(CyclicArray_Tests, CtorReserve_ReservesAboveMinimum)
{
	GDearray<int32>::AllocatorType MockAlloc;

	uint32 MinAllocCount = MockAlloc.RecommendedStartingAllocationSize();
	uint32 AllocCount = (MinAllocCount << 1) - 1;

	GDearray<int32> MockArr(TReserveStorage::Tag, AllocCount);

//	AllocCount = AeonMath::NextPowerOfTwo(AllocCount);

	EXPECT_EQ(MockArr.AllocatedCount(), AllocCount);
}

TEST(CyclicArray_Tests, CtorCreate_ReservesZero)
{
	uint32 Count = 0;

	GDearray<int32> MockArr(TCreateElements::Tag, Count);

	EXPECT_EQ(MockArr.AllocatedCount(), Count);
}

TEST(CyclicArray_Tests, CtorCreate_ReservesZero_CountMatchesReserve)
{
	uint32 Count = 0;

	GDearray<int32> MockArr(TCreateElements::Tag, Count);

	EXPECT_EQ(MockArr.Count(), Count);
}

TEST(CyclicArray_Tests, CtorCreate_ReservesNonZero)
{
	uint32 Count = 13;

	GDearray<int32> MockArr(TCreateElements::Tag, Count);

	uint32 AllocCount = /*AeonMath::NextPowerOfTwo*/(Count);

	EXPECT_EQ(MockArr.AllocatedCount(), AllocCount);
	EXPECT_EQ(MockArr.Count(), Count);
}

TEST(CyclicArray_Tests, CtorCreate_CreatesBelowMinimum)
{
	GDearray<int32>::AllocatorType MockAlloc;

	uint32 Count = (MockAlloc.RecommendedStartingAllocationSize() >> 1) - 1;

	GDearray<int32> MockArr(TCreateElements::Tag, Count);

	EXPECT_EQ(MockArr.Count(), Count);
}

TEST(CyclicArray_Tests, CtorCreate_CreatesAboveMinimum)
{
	GDearray<int32>::AllocatorType MockAlloc;

	uint32 Count = (MockAlloc.RecommendedStartingAllocationSize() << 1) - 1;

	GDearray<int32> MockArr(TCreateElements::Tag, Count);

	EXPECT_EQ(MockArr.Count(), Count);
}

TEST(CyclicArray_Tests, CtorCreateElem_AllElementsEqual)
{
	uint32 Count = 13;

	int32 InitElem = 42;

	GDearray<int32> MockArr(TCreateElements::Tag, Count, InitElem);

	for (auto const& Elem : MockArr)
	{
		EXPECT_EQ(Elem, InitElem);
	}
}

TEST(CyclicArray_Tests, CtorInitList)
{
	GDearray<int32> MockArr = { 3, 1, 4 };

	constexpr uint32 ElemCount = 3;
	constexpr int32 ElemList[ElemCount] = { 3, 1, 4 };
	uint32 Idx = 0;
	for (auto const& Elem : MockArr)
	{
		EXPECT_EQ(Elem, ElemList[Idx++]);
	}

	uint32 AllocCount = /*AeonMath::NextPowerOfTwo*/(ElemCount);

	EXPECT_EQ(MockArr.AllocatedCount(), AllocCount);
	EXPECT_EQ(MockArr.Count(), ElemCount);
}

TEST(CyclicArray_Tests, CtorInitList_RvalRef)
{
	struct MOnlyInt
	{
		MOnlyInt(int32 NewVal) : Val{ NewVal } {}

		AEON_MAKE_NON_COPYABLE(MOnlyInt);
		AEON_MAKE_MOVEABLE(MOnlyInt);

		// Make it non-trivially copyable
		~MOnlyInt() {}

		operator int32() const { return Val; }

		int32 Val;
	};

	// Will select RvalRefInitList ctor
	GDearray<MOnlyInt> MockArr = { MOnlyInt(3), MOnlyInt(1), MOnlyInt(4) };

	constexpr uint32 ElemCount = 3;
	constexpr int32 ElemList[ElemCount] = { 3, 1, 4 };
	uint32 Idx = 0;
	for (auto const& Elem : MockArr)
	{
		EXPECT_EQ(int32(Elem), ElemList[Idx++]);
	}

	uint32 AllocCount = /*AeonMath::NextPowerOfTwo*/(ElemCount);

	EXPECT_EQ(MockArr.AllocatedCount(), AllocCount);
	EXPECT_EQ(MockArr.Count(), ElemCount);
}

TEST(CyclicArray_Tests, CopyCtor)
{
	GDearray<int32> MockArr = { 3, 1, 4 };

	GDearray<int32> MockArrCopy{ MockArr };

	constexpr uint32 ElemCount = 3;
	uint32 Idx = 0;
	for (auto const& Elem : MockArrCopy)
	{
		EXPECT_EQ(Elem, MockArr[Idx++]);
	}

	uint32 AllocCount = /*AeonMath::NextPowerOfTwo*/(ElemCount);

	EXPECT_EQ(MockArr.AllocatedCount(), AllocCount);
	EXPECT_EQ(MockArr.Count(), ElemCount);

	EXPECT_EQ(MockArrCopy.AllocatedCount(), AllocCount);
	EXPECT_EQ(MockArrCopy.Count(), ElemCount);
}

TEST(CyclicArray_Tests, MoveCtor)
{
	GDearray<int32> MockArr = { 3, 1, 4 };

	GDearray<int32> MockArrCopy{ Move(MockArr) };

	constexpr uint32 ElemCount = 3;
	constexpr int32 ElemList[ElemCount] = { 3, 1, 4 };
	uint32 Idx = 0;
	for (auto const& Elem : MockArrCopy)
	{
		EXPECT_EQ(Elem, ElemList[Idx++]);
	}

	uint32 AllocCount = /*AeonMath::NextPowerOfTwo*/(ElemCount);

	EXPECT_EQ(MockArr.AllocatedCount(), 0_sz);
	EXPECT_EQ(MockArr.Count(), 0_sz);

	EXPECT_EQ(MockArrCopy.AllocatedCount(), AllocCount);
	EXPECT_EQ(MockArrCopy.Count(), ElemCount);
}

TEST(CyclicArray_Tests, IsEmpty_TrueWhenCountIsZero)
{
	GDearray<int32> MockArr;

	EXPECT_TRUE(MockArr.IsEmpty());
}

TEST(CyclicArray_Tests, IsEmpty_FalseWhenCountIsNonZero)
{
	GDearray<int32> MockArr = { 3, 1, 4 };

	EXPECT_FALSE(MockArr.IsEmpty());
}

TEST(CyclicArray_Tests, AssignOptor_Copy)
{
	GDearray<int32> MockArr = { 3, 1, 4 };

	GDearray<int32> MockArrCopy;

	EXPECT_FALSE(MockArr.IsEmpty());
	EXPECT_TRUE(MockArrCopy.IsEmpty());

	MockArrCopy = MockArr;

	EXPECT_FALSE(MockArr.IsEmpty());
	EXPECT_FALSE(MockArrCopy.IsEmpty());

	constexpr uint32 ElemCount = 3;
	uint32 Idx = 0;
	for (auto const& Elem : MockArrCopy)
	{
		EXPECT_EQ(Elem, MockArr[Idx++]);
	}

	uint32 AllocCount = /*AeonMath::NextPowerOfTwo*/(ElemCount);

	EXPECT_EQ(MockArr.AllocatedCount(), AllocCount);
	EXPECT_EQ(MockArr.Count(), ElemCount);

	EXPECT_EQ(MockArrCopy.AllocatedCount(), AllocCount);
	EXPECT_EQ(MockArrCopy.Count(), ElemCount);
}

TEST(CyclicArray_Tests, AssignOptor_Move)
{
	GDearray<int32> MockArr = { 3, 1, 4 };

	GDearray<int32> MockArrCopy;

	EXPECT_FALSE(MockArr.IsEmpty());
	EXPECT_TRUE(MockArrCopy.IsEmpty());

	MockArrCopy = Move(MockArr);

	EXPECT_TRUE(MockArr.IsEmpty());
	EXPECT_FALSE(MockArrCopy.IsEmpty());

	constexpr uint32 ElemCount = 3;
	constexpr int32 ElemList[ElemCount] = { 3, 1, 4 };
	uint32 Idx = 0;
	for (auto const& Elem : MockArrCopy)
	{
		EXPECT_EQ(Elem, ElemList[Idx++]);
	}

	uint32 AllocCount = /*AeonMath::NextPowerOfTwo*/(ElemCount);

	EXPECT_EQ(MockArr.AllocatedCount(), 0_sz);
	EXPECT_EQ(MockArr.Count(), 0_sz);

	EXPECT_EQ(MockArrCopy.AllocatedCount(), AllocCount);
	EXPECT_EQ(MockArrCopy.Count(), ElemCount);
}

TEST(CyclicArray_Tests, Append_Copy)
{
	GDearray<int32> MockArr = { 3, 1, 4 };

	GDearray<int32> MockArrOther = { 1, 5, 9 };

	EXPECT_FALSE(MockArr.IsEmpty());
	EXPECT_FALSE(MockArrOther.IsEmpty());

	MockArr.Append(MockArrOther);

	EXPECT_FALSE(MockArr.IsEmpty());
	EXPECT_FALSE(MockArrOther.IsEmpty());

	constexpr uint32 ElemCount = 6;
	constexpr uint32 ElemCountOther = 3;
	constexpr int32 ElemList[ElemCount] = { 3, 1, 4, 1, 5, 9 };
	uint32 Idx = 0;
	for (auto const& Elem : MockArr)
	{
		EXPECT_EQ(Elem, ElemList[Idx++]);
	}

	uint32 AllocCount = /*AeonMath::NextPowerOfTwo*/(ElemCount);

	EXPECT_EQ(MockArr.AllocatedCount(), AllocCount);
	EXPECT_EQ(MockArr.Count(), ElemCount);

	uint32 AllocCountOther = /*AeonMath::NextPowerOfTwo*/(ElemCountOther);

	EXPECT_EQ(MockArrOther.AllocatedCount(), AllocCountOther);
	EXPECT_EQ(MockArrOther.Count(), ElemCountOther);
}

TEST(CyclicArray_Tests, Append_Move)
{
	GDearray<int32> MockArr = { 3, 1, 4 };

	GDearray<int32> MockArrOther = { 1, 5, 9 };

	EXPECT_FALSE(MockArr.IsEmpty());
	EXPECT_FALSE(MockArrOther.IsEmpty());

	MockArr.Append(Move(MockArrOther));

	EXPECT_FALSE(MockArr.IsEmpty());
	EXPECT_TRUE(MockArrOther.IsEmpty());

	constexpr uint32 ElemCount = 6;
	constexpr int32 ElemList[ElemCount] = { 3, 1, 4, 1, 5, 9 };
	uint32 Idx = 0;
	for (auto const& Elem : MockArr)
	{
		EXPECT_EQ(Elem, ElemList[Idx++]);
	}

	uint32 AllocCount = /*AeonMath::NextPowerOfTwo*/(ElemCount);

	EXPECT_EQ(MockArr.AllocatedCount(), AllocCount);
	EXPECT_EQ(MockArr.Count(), ElemCount);

	EXPECT_EQ(MockArrOther.AllocatedCount(), 0_sz);
	EXPECT_EQ(MockArrOther.Count(), 0_sz);
}

TEST(CyclicArray_Tests, Resize_IncreaseCount)
{
	GDearray<int32> MockArr = { 3, 1, 4 };

	constexpr uint32 ElemCount = 7;

	MockArr.Resize(ElemCount);

	constexpr int32 ElemList[ElemCount] = { 3, 1, 4, int32{}, int32{}, int32{}, int32{} };
	uint32 Idx = 0;
	for (auto const& Elem : MockArr)
	{
		EXPECT_EQ(Elem, ElemList[Idx++]);
	}
}

TEST(CyclicArray_Tests, Resize_DecreaseCount_KeepsStorage)
{
	GDearray<int32> MockArr = { 3, 1, 4, 1, 5, 9 };

	uint32 AllocCount = MockArr.AllocatedCount();

	constexpr uint32 ElemCount = 3;

	MockArr.Resize(ElemCount);

	constexpr int32 ElemList[ElemCount] = { 3, 1, 4 };
	uint32 Idx = 0;
	for (auto const& Elem : MockArr)
	{
		EXPECT_EQ(Elem, ElemList[Idx++]);
	}

	EXPECT_EQ(MockArr.AllocatedCount(), AllocCount);
	EXPECT_EQ(MockArr.Count(), ElemCount);
}

TEST(CyclicArray_Tests, Resize_DecreaseCount_ReclaimsStorage)
{
	constexpr GDearray<int32>::AllocatorType MockAlloc;
	GDearray<int32> MockArr = { 3, 1, 4, 1, 5, 9, 2, 6,
									5, 3, 5, 8, 9, 7, 9, 3,
									2, 3, 8, 4, 6, 2, 6, 4,
									3, 3, 8, 3, 2, 7, 9, 5 };

	constexpr uint32 ElemCount = 6;

	// Ensure we reduce the array enough to force storage to be reclaimed
	AEON_ASSERT(ElemCount <= MockAlloc.RecommendedStartingAllocationSize());

	uint32 AllocCount = MockAlloc.RecommendedStartingAllocationSize();

	MockArr.Resize(ElemCount);

	constexpr int32 ElemList[ElemCount] = { 3, 1, 4, 1, 5, 9 };
	uint32 Idx = 0;
	for (auto const& Elem : MockArr)
	{
		EXPECT_EQ(Elem, ElemList[Idx++]);
	}

	EXPECT_EQ(MockArr.AllocatedCount(), AllocCount);
	EXPECT_EQ(MockArr.Count(), ElemCount);
}

TEST(CyclicArray_Tests, Reserve)
{
	uint32 AllocCount = 3;

	GDearray<int32> MockArr(TReserveStorage::Tag, AllocCount);

	// AllocCount =  AeonMath::NextPowerOfTwo(AllocCount);

	EXPECT_TRUE(MockArr.IsEmpty());
	EXPECT_EQ(MockArr.AllocatedCount(), AllocCount);

	AllocCount = 13;

	MockArr.Reserve(AllocCount);

	// AllocCount =  AeonMath::NextPowerOfTwo(AllocCount);

	EXPECT_TRUE(MockArr.IsEmpty());
	EXPECT_EQ(MockArr.AllocatedCount(), AllocCount);
}

TEST(CyclicArray_Tests, Reserve_MaintainsStorage)
{
	uint32 AllocCount = 13;

	GDearray<int32> MockArr(TReserveStorage::Tag, AllocCount);

	// AllocCount =  AeonMath::NextPowerOfTwo(AllocCount);

	EXPECT_TRUE(MockArr.IsEmpty());
	EXPECT_EQ(MockArr.AllocatedCount(), AllocCount);

	MockArr.Reserve(3);

	EXPECT_TRUE(MockArr.IsEmpty());
	EXPECT_EQ(MockArr.AllocatedCount(), AllocCount);
}

TEST(CyclicArray_Tests, Empty_KeepsStorage)
{
	GDearray<int32> MockArr = { 3, 1, 4 };

	MockArr.Empty();

	uint32 AllocCount = /*AeonMath::NextPowerOfTwo*/(3_u32);

	EXPECT_TRUE(MockArr.IsEmpty());
	EXPECT_EQ(MockArr.AllocatedCount(), AllocCount);
}

TEST(CyclicArray_Tests, Empty_ReclaimsStorage_ForcesZero)
{
	GDearray<int32> MockArr = { 3, 1, 4 };

	MockArr.Empty(TReclaimStorage::Tag);

	EXPECT_TRUE(MockArr.IsEmpty());
	EXPECT_EQ(MockArr.AllocatedCount(), 0_sz);
}

TEST(CyclicArray_Tests, AddToFront_IncreasesCount)
{
	GDearray<int32> MockArr;

	EXPECT_EQ(MockArr.Count(), 0_sz);

	MockArr.AddToFront(0);

	EXPECT_EQ(MockArr.Count(), 1_sz);
}

TEST(CyclicArray_Tests, RemoveFromFront_DecreasesCount)
{
	GDearray<int32> MockArr = { 42 };

	EXPECT_EQ(MockArr.Count(), 1_sz);

	MockArr.RemoveFromFront();

	EXPECT_EQ(MockArr.Count(), 0_sz);
}

TEST(CyclicArray_Tests, AddToBack_IncreasesCount)
{
	GDearray<int32> MockArr;

	EXPECT_EQ(MockArr.Count(), 0_sz);

	MockArr.AddToBack(0);

	EXPECT_EQ(MockArr.Count(), 1_sz);
}

TEST(CyclicArray_Tests, RemoveFromBack_DecreasesCount)
{
	GDearray<int32> MockArr = { 42 };

	EXPECT_EQ(MockArr.Count(), 1_sz);

	MockArr.RemoveFromBack();

	EXPECT_EQ(MockArr.Count(), 0_sz);
}

TEST(CyclicArray_Tests, AddToBackThenRemoveFromFront_BelowReserved_KeepsMinimum)
{
	GDearray<int32>::AllocatorType MockAlloc;

	uint32 AllocCount = MockAlloc.RecommendedStartingAllocationSize();

	GDearray<int32> MockArr(TReserveStorage::Tag, 0);

	// Increases allocation to minimum after first add
	{
		MockArr.AddToBack(0);
		EXPECT_EQ(MockArr.AllocatedCount(), AllocCount);
	}

	// Keeps allocation equal to minimum after fully filling storage
	{
		while (MockArr.Count() < AllocCount)
		{
			MockArr.AddToBack(0);
		}
		EXPECT_EQ(MockArr.AllocatedCount(), AllocCount);
	}

	// Keeps allocation equal to minimum after fully emptying storage
	{
		while (MockArr.Count() > 0)
		{
			MockArr.RemoveFromFront(TReclaimStorage::Tag);
		}
		EXPECT_EQ(MockArr.AllocatedCount(), AllocCount);
	}
}

TEST(CyclicArray_Tests, AddToBackThenRemoveFromFront_AboveReserved_KeepsMinimum)
{
	GDearray<int32>::AllocatorType MockAlloc;

	uint32 MinAllocCount = MockAlloc.RecommendedStartingAllocationSize();
	uint32 NextAllocCount = MockAlloc.ComputeAllocationSizeIncrease(MinAllocCount);
	uint32 Count = MinAllocCount + 1;

	GDearray<int32> MockArr(TReserveStorage::Tag, MinAllocCount);

	// Increases allocation above minimum after filling storage until Count elements
	{
		while (MockArr.Count() < Count)
		{
			MockArr.AddToBack(0);
		}
		EXPECT_EQ(MockArr.AllocatedCount(), NextAllocCount);
	}

	// Keeps allocation above minimum after emptying storage until MinAllocCount elements
	{
		while (MockArr.Count() > MinAllocCount)
		{
			MockArr.RemoveFromFront(TReclaimStorage::Tag);
		}
		EXPECT_EQ(MockArr.AllocatedCount(), NextAllocCount);
	}

	// Decreases allocation but keeps at minimum after fully emptying storage
	{
		while (MockArr.Count() > 0)
		{
			MockArr.RemoveFromFront(TReclaimStorage::Tag);
		}
		EXPECT_EQ(MockArr.AllocatedCount(), MinAllocCount);
	}
}

TEST(CyclicArray_Tests, AddToFrontThenRemoveFromBack_BelowReserved_KeepsMinimum)
{
	GDearray<int32>::AllocatorType MockAlloc;

	uint32 AllocCount = MockAlloc.RecommendedStartingAllocationSize();

	GDearray<int32> MockArr(TReserveStorage::Tag, 0);

	// Increases allocation to minimum after first add
	{
		MockArr.AddToFront(0);
		EXPECT_EQ(MockArr.AllocatedCount(), AllocCount);
	}

	// Keeps allocation equal to minimum after fully filling storage
	{
		while (MockArr.Count() < AllocCount)
		{
			MockArr.AddToFront(0);
		}
		EXPECT_EQ(MockArr.AllocatedCount(), AllocCount);
	}

	// Keeps allocation equal to minimum after fully emptying storage
	{
		while (MockArr.Count() > 0)
		{
			MockArr.RemoveFromBack(TReclaimStorage::Tag);
		}
		EXPECT_EQ(MockArr.AllocatedCount(), AllocCount);
	}
}

TEST(CyclicArray_Tests, AddToFrontThenRemoveFromBack_AboveReserved_KeepsMinimum)
{
	GDearray<int32>::AllocatorType MockAlloc;

	uint32 MinAllocCount = MockAlloc.RecommendedStartingAllocationSize();
	uint32 NextAllocCount = MockAlloc.ComputeAllocationSizeIncrease(MinAllocCount);
	uint32 Count = MinAllocCount + 1;

	GDearray<int32> MockArr(TReserveStorage::Tag, MinAllocCount);

	// Increases allocation above minimum after filling storage until Count elements
	{
		while (MockArr.Count() < Count)
		{
			MockArr.AddToFront(0);
		}
		EXPECT_EQ(MockArr.AllocatedCount(), NextAllocCount);
	}

	// Keeps allocation above minimum after emptying storage until MinAllocCount elements
	{
		while (MockArr.Count() > MinAllocCount)
		{
			MockArr.RemoveFromBack(TReclaimStorage::Tag);
		}
		EXPECT_EQ(MockArr.AllocatedCount(), NextAllocCount);
	}

	// Decreases allocation but keeps at minimum after fully emptying storage
	{
		while (MockArr.Count() > 0)
		{
			MockArr.RemoveFromBack(TReclaimStorage::Tag);
		}
		EXPECT_EQ(MockArr.AllocatedCount(), MinAllocCount);
	}
}

TEST(CyclicArray_Tests, AddToBack_KeepsOrderOfAdd)
{
	GDearray<int32> MockArr;

	MockArr.AddToBack(3);
	MockArr.AddToBack(1);
	MockArr.AddToBack(4);
	MockArr.AddToBack(1);
	MockArr.AddToBack(5);
	MockArr.AddToBack(9);

	constexpr uint32 ElemCount = 6;
	constexpr int32 ElemList[ElemCount] = { 3, 1, 4, 1, 5, 9 };
	uint32 Idx = 0;
	for (auto const& Elem : MockArr)
	{
		EXPECT_EQ(Elem, ElemList[Idx++]);
	}
}

TEST(CyclicArray_Tests, AddToFront_KeepsReverseOrderOfAdd)
{
	GDearray<int32> MockArr;

	MockArr.AddToFront(9);
	MockArr.AddToFront(5);
	MockArr.AddToFront(1);
	MockArr.AddToFront(4);
	MockArr.AddToFront(1);
	MockArr.AddToFront(3);

	constexpr uint32 ElemCount = 6;
	constexpr int32 ElemList[ElemCount] = { 3, 1, 4, 1, 5, 9 };
	uint32 Idx = 0;
	for (auto const& Elem : MockArr)
	{
		EXPECT_EQ(Elem, ElemList[Idx++]);
	}
}

TEST(CyclicArray_Tests, Front)
{
	GDearray<int32> MockArr1 = { 3, 1, 4, 1, 5, 9 };

	EXPECT_EQ(MockArr1.Front(), 3);

	GDearray<int32> MockArr2 = { 2, 6, 5, 3, 5, 8 };

	EXPECT_EQ(MockArr2.Front(), 2);

	MockArr1.Append(MockArr2);

	EXPECT_EQ(MockArr1.Front(), 3);
}

TEST(CyclicArray_Tests, Back)
{
	GDearray<int32> MockArr1 = { 3, 1, 4, 1, 5, 9 };

	EXPECT_EQ(MockArr1.Back(), 9);

	GDearray<int32> MockArr2 = { 2, 6, 5, 3, 5, 8 };

	EXPECT_EQ(MockArr2.Back(), 8);

	MockArr1.Append(MockArr2);

	EXPECT_EQ(MockArr1.Back(), 8);
}

TEST(CyclicArray_Tests, AddAt_FrontWhenIndexIsZero)
{
	GDearray<int32> MockArr;

	MockArr.AddToBack(3);
	MockArr.AddToBack(1);
	MockArr.AddToBack(4);

	MockArr.AddAt(0, 42);

	constexpr uint32 ElemCount = 4;
	constexpr int32 ElemList[ElemCount] = { 42, 3, 1, 4 };
	uint32 Idx = 0;
	for (auto const& Elem : MockArr)
	{
		EXPECT_EQ(Elem, ElemList[Idx++]);
	}
}

TEST(CyclicArray_Tests, AddAt_BackWhenIndexIsCount)
{
	GDearray<int32> MockArr;

	MockArr.AddToBack(3);
	MockArr.AddToBack(1);
	MockArr.AddToBack(4);

	MockArr.AddAt(3, 42);

	constexpr uint32 ElemCount = 4;
	constexpr int32 ElemList[ElemCount] = { 3, 1, 4, 42 };
	uint32 Idx = 0;
	for (auto const& Elem : MockArr)
	{
		EXPECT_EQ(Elem, ElemList[Idx++]);
	}
}

TEST(CyclicArray_Tests, AddAt_MiddleOtherwise)
{
	GDearray<int32> MockArr;

	MockArr.AddToBack(3);
	MockArr.AddToBack(1);
	MockArr.AddToBack(4);

	MockArr.AddAt(2, 42);

	constexpr uint32 ElemCount = 4;
	constexpr int32 ElemList[ElemCount] = { 3, 1, 42, 4 };
	uint32 Idx = 0;
	for (auto const& Elem : MockArr)
	{
		EXPECT_EQ(Elem, ElemList[Idx++]);
	}
}

TEST(CyclicArray_Tests, IsSorted)
{
	{
		GDearray<int32> MockArr;

		MockArr.AddToBack(6);
		MockArr.AddToBack(3);
		MockArr.AddToBack(5);
		MockArr.AddToBack(1);
		MockArr.AddToBack(8);

		EXPECT_FALSE(MockArr.IsSorted());
	}

	{
		GDearray<int32> MockArr;

		MockArr.AddToBack(1);
		MockArr.AddToBack(3);
		MockArr.AddToBack(5);
		MockArr.AddToBack(6);
		MockArr.AddToBack(8);

		EXPECT_TRUE(MockArr.IsSorted());
	}
}

TEST(CyclicArray_Tests, AddSorted_IsOrderedAfterEachAdd)
{
	GDearray<int32> MockArr;

	MockArr.AddSorted(6);
	EXPECT_TRUE(MockArr.IsSorted());

	MockArr.AddSorted(3);
	EXPECT_TRUE(MockArr.IsSorted());

	MockArr.AddSorted(5);
	EXPECT_TRUE(MockArr.IsSorted());

	MockArr.AddSorted(1);
	EXPECT_TRUE(MockArr.IsSorted());

	MockArr.AddSorted(8);
	EXPECT_TRUE(MockArr.IsSorted());
}

TEST(CyclicArray_Tests, Sort_IsOrderedAfterSort)
{
	GDearray<int32> MockArr;

	MockArr.AddToBack(6);
	MockArr.AddToBack(3);
	MockArr.AddToBack(5);
	MockArr.AddToBack(1);
	MockArr.AddToBack(8);

	EXPECT_FALSE(MockArr.IsSorted());

	MockArr.Sort();

	EXPECT_TRUE(MockArr.IsSorted());
}

TEST(CyclicArray_Tests, Reverse)
{
	GDearray<int32> MockArr;

	MockArr.AddToBack(1);
	MockArr.AddToBack(3);
	MockArr.AddToBack(5);
	MockArr.AddToBack(6);
	MockArr.AddToBack(8);

	EXPECT_TRUE(MockArr.IsSorted());

	MockArr.Reverse();

	EXPECT_FALSE(MockArr.IsSorted<GLessThan<int32>>());
	EXPECT_TRUE(MockArr.IsSorted<GGreaterThan<int32>>());
}

// TODO test Find, Contains, Filter, Map, Reduce

//TEST(CyclicArray_Tests, )
//{
//	GDearray<int32> MockArr;
//
//}