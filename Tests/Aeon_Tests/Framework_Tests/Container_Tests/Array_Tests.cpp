#include <gtest/gtest.h>

#include "Aeon/Framework/Framework.hpp"

using namespace Aeon;

TEST(Array_Tests, CtorDefault_ReservesMinimum)
{
	GArray<int32>::AllocatorType MockAlloc;
	GArray<int32> MockArr;

	EXPECT_EQ(MockArr.AllocatedCount(), MockAlloc.RecommendedStartingAllocationSize());
}

TEST(Array_Tests, CtorDefault_CountIsZero)
{
	GArray<int32> MockArr;

	EXPECT_EQ(MockArr.Count(), 0_sz);
}

TEST(Array_Tests, CtorReserve_ReservesZero)
{
	GArray<int32> MockArr(TReserveStorage::Tag, 0);

	EXPECT_EQ(MockArr.AllocatedCount(), 0_sz);
}

TEST(Array_Tests, CtorReserve_ReservesZero_CountIsZero)
{
	GArray<int32> MockArr(TReserveStorage::Tag, 0);

	EXPECT_EQ(MockArr.Count(), 0_sz);
}

TEST(Array_Tests, CtorReserve_ReservesNonZero)
{
	GArray<int32> MockArr(TReserveStorage::Tag, 13);

	EXPECT_EQ(MockArr.AllocatedCount(), 13_sz);
}

TEST(Array_Tests, CtorReserve_ReservesNonZero_CountIsZero)
{
	GArray<int32> MockArr(TReserveStorage::Tag, 13);

	EXPECT_EQ(MockArr.Count(), 0_sz);
}

TEST(Array_Tests, CtorReserve_ReservesBelowMinimum)
{
	GArray<int32>::AllocatorType MockAlloc;

	uint32 MinAllocCount = MockAlloc.RecommendedStartingAllocationSize();
	uint32 AllocCount = (MinAllocCount >> 1) - 1;

	GArray<int32> MockArr(TReserveStorage::Tag, AllocCount);

	EXPECT_EQ(MockArr.AllocatedCount(), AllocCount);
}

TEST(Array_Tests, CtorReserve_ReservesAboveMinimum)
{
	GArray<int32>::AllocatorType MockAlloc;

	uint32 MinAllocCount = MockAlloc.RecommendedStartingAllocationSize();
	uint32 AllocCount = (MinAllocCount << 1) - 1;

	GArray<int32> MockArr(TReserveStorage::Tag, AllocCount);

	EXPECT_EQ(MockArr.AllocatedCount(), AllocCount);
}

TEST(Array_Tests, CtorCreate_ReservesZero)
{
	uint32 Count = 0;

	GArray<int32> MockArr(TCreateElements::Tag, Count);

	EXPECT_EQ(MockArr.AllocatedCount(), Count);
}

TEST(Array_Tests, CtorCreate_ReservesZero_CountMatchesReserve)
{
	uint32 Count = 0;

	GArray<int32> MockArr(TCreateElements::Tag, Count);

	EXPECT_EQ(MockArr.Count(), Count);
}

TEST(Array_Tests, CtorCreate_ReservesNonZero)
{
	uint32 Count = 13;

	GArray<int32> MockArr(TCreateElements::Tag, Count);

	EXPECT_EQ(MockArr.AllocatedCount(), Count);
}

TEST(Array_Tests, CtorCreate_ReservesNonZero_CountMatchesReserve)
{
	uint32 Count = 13;

	GArray<int32> MockArr(TCreateElements::Tag, Count);

	EXPECT_EQ(MockArr.Count(), Count);
}

TEST(Array_Tests, CtorCreate_CreatesBelowMinimum)
{
	GArray<int32>::AllocatorType MockAlloc;

	uint32 Count = (MockAlloc.RecommendedStartingAllocationSize() >> 1) - 1;

	GArray<int32> MockArr(TCreateElements::Tag, Count);

	EXPECT_EQ(MockArr.Count(), Count);
}

TEST(Array_Tests, CtorCreate_CreatesAboveMinimum)
{
	GArray<int32>::AllocatorType MockAlloc;

	uint32 Count = (MockAlloc.RecommendedStartingAllocationSize() << 1) - 1;

	GArray<int32> MockArr(TCreateElements::Tag, Count);

	EXPECT_EQ(MockArr.Count(), Count);
}

TEST(Array_Tests, CtorCreateElem_AllElementsEqual)
{
	uint32 Count = 13;

	int32 InitElem = 42;

	GArray<int32> MockArr(TCreateElements::Tag, Count, InitElem);

	for (auto const& Elem : MockArr)
	{
		EXPECT_EQ(Elem, InitElem);
	}
}

TEST(Array_Tests, CtorInitList)
{
	GArray<int32> MockArr = { 3, 1, 4 };

	constexpr uint32 ElemCount = 3;
	constexpr int32 ElemList[ElemCount] = { 3, 1, 4 };
	uint32 Idx = 0;
	for (auto const& Elem : MockArr)
	{
		EXPECT_EQ(Elem, ElemList[Idx++]);
	}

	EXPECT_EQ(MockArr.AllocatedCount(), ElemCount);
	EXPECT_EQ(MockArr.Count(), ElemCount);
}

TEST(Array_Tests, CtorInitList_RvalRef)
{
	struct MOnlyInt
	{
		MOnlyInt(int32 NewVal) : Val{ NewVal } {}

		AEON_MAKE_NON_COPYABLE(MOnlyInt);
		AEON_MAKE_MOVEABLE(MOnlyInt);

		// Make it non-trivially copyable
		~MOnlyInt() {}

		operator int32() const { return Val; }

		int32 Val;
	};

	// Invokes RvalRefInitList ctor
	GArray<MOnlyInt> MockArr = { MOnlyInt(3), MOnlyInt(1), MOnlyInt(4) };

	constexpr uint32 ElemCount = 3;
	constexpr int32 ElemList[ElemCount] = { 3, 1, 4 };
	uint32 Idx = 0;
	for (auto const& Elem : MockArr)
	{
		EXPECT_EQ(int32(Elem), ElemList[Idx++]);
	}

	EXPECT_EQ(MockArr.AllocatedCount(), ElemCount);
	EXPECT_EQ(MockArr.Count(), ElemCount);
}

TEST(Array_Tests, CopyCtor)
{
	GArray<int32> MockArr = { 3, 1, 4 };

	GArray<int32> MockArrCopy{ MockArr };

	constexpr uint32 ElemCount = 3;
	uint32 Idx = 0;
	for (auto const& Elem : MockArrCopy)
	{
		EXPECT_EQ(Elem, MockArr[Idx++]);
	}

	EXPECT_EQ(MockArr.AllocatedCount(), ElemCount);
	EXPECT_EQ(MockArr.Count(), ElemCount);

	EXPECT_EQ(MockArrCopy.AllocatedCount(), ElemCount);
	EXPECT_EQ(MockArrCopy.Count(), ElemCount);
}

TEST(Array_Tests, MoveCtor)
{
	GArray<int32> MockArr = { 3, 1, 4 };

	GArray<int32> MockArrCopy{ Move(MockArr) };

	constexpr uint32 ElemCount = 3;
	constexpr int32 ElemList[ElemCount] = { 3, 1, 4 };
	uint32 Idx = 0;
	for (auto const& Elem : MockArrCopy)
	{
		EXPECT_EQ(Elem, ElemList[Idx++]);
	}

	EXPECT_EQ(MockArr.AllocatedCount(), 0_sz);
	EXPECT_EQ(MockArr.Count(), 0_sz);

	EXPECT_EQ(MockArrCopy.AllocatedCount(), ElemCount);
	EXPECT_EQ(MockArrCopy.Count(), ElemCount);
}

TEST(Array_Tests, IsEmpty_TrueWhenCountIsZero)
{
	GArray<int32> MockArr;

	EXPECT_TRUE(MockArr.IsEmpty());
}

TEST(Array_Tests, IsEmpty_FalseWhenCountIsNonZero)
{
	GArray<int32> MockArr = { 3, 1, 4 };

	EXPECT_FALSE(MockArr.IsEmpty());
}

TEST(Array_Tests, AssignOptor_Copy)
{
	GArray<int32> MockArr = { 3, 1, 4 };

	GArray<int32> MockArrCopy;

	EXPECT_FALSE(MockArr.IsEmpty());
	EXPECT_TRUE(MockArrCopy.IsEmpty());

	MockArrCopy = MockArr;

	EXPECT_FALSE(MockArr.IsEmpty());
	EXPECT_FALSE(MockArrCopy.IsEmpty());

	constexpr uint32 ElemCount = 3;
	uint32 Idx = 0;
	for (auto const& Elem : MockArrCopy)
	{
		EXPECT_EQ(Elem, MockArr[Idx++]);
	}

	EXPECT_EQ(MockArr.AllocatedCount(), ElemCount);
	EXPECT_EQ(MockArr.Count(), ElemCount);

	EXPECT_EQ(MockArrCopy.AllocatedCount(), ElemCount);
	EXPECT_EQ(MockArrCopy.Count(), ElemCount);
}

TEST(Array_Tests, AssignOptor_Move)
{
	GArray<int32> MockArr = { 3, 1, 4 };

	GArray<int32> MockArrCopy;

	EXPECT_FALSE(MockArr.IsEmpty());
	EXPECT_TRUE(MockArrCopy.IsEmpty());

	MockArrCopy = Move(MockArr);

	EXPECT_TRUE(MockArr.IsEmpty());
	EXPECT_FALSE(MockArrCopy.IsEmpty());

	constexpr uint32 ElemCount = 3;
	constexpr int32 ElemList[ElemCount] = { 3, 1, 4 };
	uint32 Idx = 0;
	for (auto const& Elem : MockArrCopy)
	{
		EXPECT_EQ(Elem, ElemList[Idx++]);
	}

	EXPECT_EQ(MockArr.AllocatedCount(), 0_sz);
	EXPECT_EQ(MockArr.Count(), 0_sz);

	EXPECT_EQ(MockArrCopy.AllocatedCount(), ElemCount);
	EXPECT_EQ(MockArrCopy.Count(), ElemCount);
}

TEST(Array_Tests, Append_Copy)
{
	GArray<int32> MockArr = { 3, 1, 4 };

	GArray<int32> MockArrOther = { 1, 5, 9 };

	EXPECT_FALSE(MockArr.IsEmpty());
	EXPECT_FALSE(MockArrOther.IsEmpty());

	MockArr.Append(MockArrOther);

	EXPECT_FALSE(MockArr.IsEmpty());
	EXPECT_FALSE(MockArrOther.IsEmpty());

	constexpr uint32 ElemCount = 6;
	constexpr uint32 ElemCountOther = 3;
	constexpr int32 ElemList[ElemCount] = { 3, 1, 4, 1, 5, 9 };
	uint32 Idx = 0;
	for (auto const& Elem : MockArr)
	{
		EXPECT_EQ(Elem, ElemList[Idx++]);
	}

	EXPECT_EQ(MockArr.AllocatedCount(), ElemCount);
	EXPECT_EQ(MockArr.Count(), ElemCount);

	EXPECT_EQ(MockArrOther.AllocatedCount(), ElemCountOther);
	EXPECT_EQ(MockArrOther.Count(), ElemCountOther);
}

TEST(Array_Tests, Append_Move)
{
	GArray<int32> MockArr = { 3, 1, 4 };

	GArray<int32> MockArrOther = { 1, 5, 9 };

	EXPECT_FALSE(MockArr.IsEmpty());
	EXPECT_FALSE(MockArrOther.IsEmpty());

	MockArr.Append(Move(MockArrOther));

	EXPECT_FALSE(MockArr.IsEmpty());
	EXPECT_TRUE(MockArrOther.IsEmpty());

	constexpr uint32 ElemCount = 6;
	constexpr int32 ElemList[ElemCount] = { 3, 1, 4, 1, 5, 9 };
	uint32 Idx = 0;
	for (auto const& Elem : MockArr)
	{
		EXPECT_EQ(Elem, ElemList[Idx++]);
	}

	EXPECT_EQ(MockArr.AllocatedCount(), ElemCount);
	EXPECT_EQ(MockArr.Count(), ElemCount);

	EXPECT_EQ(MockArrOther.AllocatedCount(), 0_sz);
	EXPECT_EQ(MockArrOther.Count(), 0_sz);
}

TEST(Array_Tests, Resize_IncreaseCount)
{
	GArray<int32> MockArr = { 3, 1, 4 };

	constexpr uint32 ElemCount = 7;

	MockArr.Resize(ElemCount);

	constexpr int32 ElemList[ElemCount] = { 3, 1, 4, int32{}, int32{}, int32{}, int32{} };
	uint32 Idx = 0;
	for (auto const& Elem : MockArr)
	{
		EXPECT_EQ(Elem, ElemList[Idx++]);
	}
}

TEST(Array_Tests, Resize_DecreaseCount_KeepsStorage)
{
	GArray<int32> MockArr = { 3, 1, 4, 1, 5, 9 };

	uint32 AllocCount = MockArr.AllocatedCount();

	constexpr uint32 ElemCount = 3;

	MockArr.Resize(ElemCount);

	constexpr int32 ElemList[ElemCount] = { 3, 1, 4 };
	uint32 Idx = 0;
	for (auto const& Elem : MockArr)
	{
		EXPECT_EQ(Elem, ElemList[Idx++]);
	}

	EXPECT_EQ(MockArr.AllocatedCount(), AllocCount);
	EXPECT_EQ(MockArr.Count(), ElemCount);
}

TEST(Array_Tests, Resize_DecreaseCount_ReclaimsStorage)
{
	constexpr GArray<int32>::AllocatorType MockAlloc;
	GArray<int32> MockArr = { 3, 1, 4, 1, 5, 9, 2, 6,
								5, 3, 5, 8, 9, 7, 9, 3,
								2, 3, 8, 4, 6, 2, 6, 4,
								3, 3, 8, 3, 2, 7, 9, 5 };

	constexpr uint32 ElemCount = 6;

	// Ensure we reduce the array enough to force storage to be reclaimed
	AEON_ASSERT(ElemCount <= MockAlloc.RecommendedStartingAllocationSize());

	uint32 AllocCount = MockAlloc.RecommendedStartingAllocationSize();

	MockArr.Resize(ElemCount);

	constexpr int32 ElemList[ElemCount] = { 3, 1, 4, 1, 5, 9 };
	uint32 Idx = 0;
	for (auto const& Elem : MockArr)
	{
		EXPECT_EQ(Elem, ElemList[Idx++]);
	}

	EXPECT_EQ(MockArr.AllocatedCount(), AllocCount);
	EXPECT_EQ(MockArr.Count(), ElemCount);
}

TEST(Array_Tests, Reserve)
{
	uint32 AllocCount = 3;

	GArray<int32> MockArr(TReserveStorage::Tag, AllocCount);

	EXPECT_TRUE(MockArr.IsEmpty());
	EXPECT_EQ(MockArr.AllocatedCount(), AllocCount);

	AllocCount = 13;

	MockArr.Reserve(AllocCount);

	EXPECT_TRUE(MockArr.IsEmpty());
	EXPECT_EQ(MockArr.AllocatedCount(), AllocCount);
}

TEST(Array_Tests, Reserve_MaintainsStorage)
{
	uint32 AllocCount = 13;

	GArray<int32> MockArr(TReserveStorage::Tag, AllocCount);

	EXPECT_TRUE(MockArr.IsEmpty());
	EXPECT_EQ(MockArr.AllocatedCount(), AllocCount);

	MockArr.Reserve(3);

	EXPECT_TRUE(MockArr.IsEmpty());
	EXPECT_EQ(MockArr.AllocatedCount(), AllocCount);
}

TEST(Array_Tests, Empty_KeepsStorage)
{
	GArray<int32> MockArr = { 3, 1, 4 };

	MockArr.Empty();

	EXPECT_TRUE(MockArr.IsEmpty());
	EXPECT_EQ(MockArr.AllocatedCount(), 3_sz);
}

TEST(Array_Tests, Empty_ReclaimsStorage_ForcesZero)
{
	GArray<int32> MockArr = { 3, 1, 4 };

	MockArr.Empty(TReclaimStorage::Tag);

	EXPECT_TRUE(MockArr.IsEmpty());
	EXPECT_EQ(MockArr.AllocatedCount(), 0_sz);
}

TEST(Array_Tests, Add_IncreasesCount)
{
	GArray<int32> MockArr;

	EXPECT_EQ(MockArr.Count(), 0_sz);

	MockArr.Add(0);

	EXPECT_EQ(MockArr.Count(), 1_sz);
}

TEST(Array_Tests, Remove_DecreasesCount)
{
	GArray<int32> MockArr;

	MockArr.Add(0);

	EXPECT_EQ(MockArr.Count(), 1_sz);

	MockArr.Remove();

	EXPECT_EQ(MockArr.Count(), 0_sz);
}

TEST(Array_Tests, AddThenRemove_BelowReserved_KeepsMinimum)
{
	GArray<int32>::AllocatorType MockAlloc;

	uint32 AllocCount = MockAlloc.RecommendedStartingAllocationSize();

	GArray<int32> MockArr(TReserveStorage::Tag, 0);

	// Increases allocation to minimum after first add
	{
		MockArr.Add(0);
		EXPECT_EQ(MockArr.AllocatedCount(), AllocCount);
	}

	// Keeps allocation equal to minimum after fully filling storage
	{
		while (MockArr.Count() < AllocCount)
		{
			MockArr.Add(0);
		}
		EXPECT_EQ(MockArr.AllocatedCount(), AllocCount);
	}

	// Keeps allocation equal to minimum after fully emptying storage
	{
		while (MockArr.Count() > 0)
		{
			MockArr.Remove(TReclaimStorage::Tag);
		}
		EXPECT_EQ(MockArr.AllocatedCount(), AllocCount);
	}
}

TEST(Array_Tests, AddThenRemove_AboveReserved_KeepsMinimum)
{
	GArray<int32>::AllocatorType MockAlloc;

	uint32 MinAllocCount = MockAlloc.RecommendedStartingAllocationSize();
	uint32 NextAllocCount = MockAlloc.ComputeAllocationSizeIncrease(MinAllocCount);
	uint32 Count = MinAllocCount + 1;

	GArray<int32> MockArr(TReserveStorage::Tag, MinAllocCount);

	// Increases allocation above minimum after filling storage until Count elements
	{
		while (MockArr.Count() < Count)
		{
			MockArr.Add(0);
		}
		EXPECT_EQ(MockArr.AllocatedCount(), NextAllocCount);
	}

	// Keeps allocation above minimum after emptying storage until MinAllocCount elements
	{
		while (MockArr.Count() > MinAllocCount)
		{
			MockArr.Remove(TReclaimStorage::Tag);
		}
		EXPECT_EQ(MockArr.AllocatedCount(), NextAllocCount);
	}

	// Decreases allocation but keeps at minimum after fully emptying storage
	{
		while (MockArr.Count() > 0)
		{
			MockArr.Remove(TReclaimStorage::Tag);
		}
		EXPECT_EQ(MockArr.AllocatedCount(), MinAllocCount);
	}
}

TEST(Array_Tests, Add_KeepsOrderOfAdd)
{
	GArray<int32> MockArr;

	MockArr.Add(3);
	MockArr.Add(1);
	MockArr.Add(4);

	constexpr uint32 ElemCount = 3;
	constexpr int32 ElemList[ElemCount] = { 3, 1, 4 };
	uint32 Idx = 0;
	for (auto const& Elem : MockArr)
	{
		EXPECT_EQ(Elem, ElemList[Idx++]);
	}
}

TEST(Array_Tests, AddThenRemove_KeepsOrderOfAdd)
{
	GArray<int32> MockArr;

	MockArr.Add(3);
	MockArr.Add(1);
	MockArr.Add(4);
	MockArr.Add(1);
	MockArr.Add(5);
	MockArr.Add(9);

	MockArr.Remove();
	MockArr.Remove();
	MockArr.Remove();

	constexpr uint32 ElemCount = 3;
	constexpr int32 ElemList[ElemCount] = { 3, 1, 4 };
	uint32 Idx = 0;
	for (auto const& Elem : MockArr)
	{
		EXPECT_EQ(Elem, ElemList[Idx++]);
	}
}

TEST(Array_Tests, Front)
{
	GArray<int32> MockArr1 = { 3, 1, 4, 1, 5, 9 };

	EXPECT_EQ(MockArr1.Front(), 3);

	GArray<int32> MockArr2 = { 2, 6, 5, 3, 5, 8 };

	EXPECT_EQ(MockArr2.Front(), 2);

	MockArr1.Append(MockArr2);

	EXPECT_EQ(MockArr1.Front(), 3);
}

TEST(Array_Tests, Back)
{
	GArray<int32> MockArr1 = { 3, 1, 4, 1, 5, 9 };

	EXPECT_EQ(MockArr1.Back(), 9);

	GArray<int32> MockArr2 = { 2, 6, 5, 3, 5, 8 };

	EXPECT_EQ(MockArr2.Back(), 8);

	MockArr1.Append(MockArr2);

	EXPECT_EQ(MockArr1.Back(), 8);
}

TEST(Array_Tests, AddAt_FrontWhenIndexIsZero)
{
	GArray<int32> MockArr;

	MockArr.Add(3);
	MockArr.Add(1);
	MockArr.Add(4);

	MockArr.AddAt(0, 42);

	constexpr uint32 ElemCount = 4;
	constexpr int32 ElemList[ElemCount] = { 42, 3, 1, 4 };
	uint32 Idx = 0;
	for (auto const& Elem : MockArr)
	{
		EXPECT_EQ(Elem, ElemList[Idx++]);
	}
}

TEST(Array_Tests, AddAt_BackWhenIndexIsCount)
{
	GArray<int32> MockArr;

	MockArr.Add(3);
	MockArr.Add(1);
	MockArr.Add(4);

	MockArr.AddAt(3, 42);

	constexpr uint32 ElemCount = 4;
	constexpr int32 ElemList[ElemCount] = { 3, 1, 4, 42 };
	uint32 Idx = 0;
	for (auto const& Elem : MockArr)
	{
		EXPECT_EQ(Elem, ElemList[Idx++]);
	}
}

TEST(Array_Tests, AddAt_MiddleOtherwise)
{
	GArray<int32> MockArr;

	MockArr.Add(3);
	MockArr.Add(1);
	MockArr.Add(4);

	MockArr.AddAt(2, 42);

	constexpr uint32 ElemCount = 4;
	constexpr int32 ElemList[ElemCount] = { 3, 1, 42, 4 };
	uint32 Idx = 0;
	for (auto const& Elem : MockArr)
	{
		EXPECT_EQ(Elem, ElemList[Idx++]);
	}
}

TEST(Array_Tests, IsSorted)
{
	{
		GArray<int32> MockArr;

		MockArr.Add(6);
		MockArr.Add(3);
		MockArr.Add(5);
		MockArr.Add(1);
		MockArr.Add(8);

		EXPECT_FALSE(MockArr.IsSorted());
	}

	{
		GArray<int32> MockArr;

		MockArr.Add(1);
		MockArr.Add(3);
		MockArr.Add(5);
		MockArr.Add(6);
		MockArr.Add(8);

		EXPECT_TRUE(MockArr.IsSorted());
	}
}

TEST(Array_Tests, AddSorted_IsOrderedAfterEachAdd)
{
	GArray<int32> MockArr;

	MockArr.AddSorted(6);
	EXPECT_TRUE(MockArr.IsSorted());

	MockArr.AddSorted(3);
	EXPECT_TRUE(MockArr.IsSorted());

	MockArr.AddSorted(5);
	EXPECT_TRUE(MockArr.IsSorted());

	MockArr.AddSorted(1);
	EXPECT_TRUE(MockArr.IsSorted());

	MockArr.AddSorted(8);
	EXPECT_TRUE(MockArr.IsSorted());
}

TEST(Array_Tests, Sort_IsOrderedAfterSort)
{
	GArray<int32> MockArr;

	MockArr.Add(6);
	MockArr.Add(3);
	MockArr.Add(5);
	MockArr.Add(1);
	MockArr.Add(8);

	EXPECT_FALSE(MockArr.IsSorted());

	MockArr.Sort();

	EXPECT_TRUE(MockArr.IsSorted());
}

TEST(Array_Tests, Reverse)
{
	GArray<int32> MockArr;

	MockArr.Add(1);
	MockArr.Add(3);
	MockArr.Add(5);
	MockArr.Add(6);
	MockArr.Add(8);

	EXPECT_TRUE(MockArr.IsSorted());

	MockArr.Reverse();

	EXPECT_FALSE(MockArr.IsSorted<GLessThan<int32>>());
	EXPECT_TRUE(MockArr.IsSorted<GGreaterThan<int32>>());
}

// TODO test Emplace(At), Find, Contains, Filter, Map, Reduce

//TEST(Array_Tests, )
//{
//	ZzInternal_GArray<int32> MockArr;
//
//}