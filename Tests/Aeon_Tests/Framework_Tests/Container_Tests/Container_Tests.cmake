#add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/)

target_sources(${PROJECT_NAME}
	PRIVATE
		${CMAKE_CURRENT_SOURCE_DIR}/Allocator_Tests.cpp
		${CMAKE_CURRENT_SOURCE_DIR}/Array_Tests.cpp
		${CMAKE_CURRENT_SOURCE_DIR}/Dearray_Tests.cpp
	)