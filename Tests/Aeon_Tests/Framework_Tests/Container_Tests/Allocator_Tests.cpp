#include <gtest/gtest.h>

#include "Aeon/Framework/Framework.hpp"

using namespace Aeon;

TEST(Allocator_Tests, Dynamic_MinimumCount_NonZero)
{
	GDynamicAllocator<int32, uint32> MockAlloc;

	EXPECT_NE(MockAlloc.RecommendedStartingAllocationSize(), 0_sz);
}

TEST(Allocator_Tests, Dynamic_ShouldIncrease_MaintainsNotAboveThreshold)
{
	GDynamicAllocator<int32, uint32> MockAlloc;

	uint32 AllocCount = MockAlloc.RecommendedStartingAllocationSize();

	{
		uint32 UpdatedCount = AllocCount;
		EXPECT_FALSE(MockAlloc.ShouldIncreaseAllocationSize(AllocCount, UpdatedCount));
	}
}

TEST(Allocator_Tests, Dynamic_ShouldIncrease_IncreasesJustAboveThreshold)
{
	GDynamicAllocator<int32, uint32> MockAlloc;

	uint32 AllocCount = MockAlloc.RecommendedStartingAllocationSize();

	{
		uint32 UpdatedCount = AllocCount + 1;
		EXPECT_TRUE(MockAlloc.ShouldIncreaseAllocationSize(AllocCount, UpdatedCount));
	}
}

TEST(Allocator_Tests, Dynamic_ShouldDecrease_MaintainsJustBelowThreshold)
{
	GDynamicAllocator<int32, uint32> MockAlloc;

	uint32 AllocCount = MockAlloc.RecommendedStartingAllocationSize() << 1;

	{
		uint32 UpdatedCount = AllocCount - 1;
		EXPECT_FALSE(MockAlloc.ShouldDecreaseAllocationSize(AllocCount, UpdatedCount));
	}
}

TEST(Allocator_Tests, Dynamic_ShouldDecrease_MaintainsBelowThreshold)
{
	GDynamicAllocator<int32, uint32> MockAlloc;

	uint32 AllocCount = MockAlloc.RecommendedStartingAllocationSize() << 1;

	{
		uint32 UpdatedCount = (AllocCount >> 1) + 1;
		EXPECT_FALSE(MockAlloc.ShouldDecreaseAllocationSize(AllocCount, UpdatedCount));
	}

	{
		uint32 UpdatedCount = AllocCount >> 1;
		EXPECT_FALSE(MockAlloc.ShouldDecreaseAllocationSize(AllocCount, UpdatedCount));
	}
}

TEST(Allocator_Tests, Dynamic_ShouldDecrease_DecreasesWellBelowThreshold)
{
	GDynamicAllocator<int32, uint32> MockAlloc;

	uint32 AllocCount = MockAlloc.RecommendedStartingAllocationSize() << 1;

	{
		uint32 UpdatedCount = (AllocCount >> 2) + 1;
		EXPECT_FALSE(MockAlloc.ShouldDecreaseAllocationSize(AllocCount, UpdatedCount));
	}

	{
		uint32 UpdatedCount = AllocCount >> 2;
		EXPECT_TRUE(MockAlloc.ShouldDecreaseAllocationSize(AllocCount, UpdatedCount));
	}
}

TEST(Allocator_Tests, Dynamic_ComputeIncrease_EnsuresMinimum)
{
	GDynamicAllocator<int32, uint32> MockAlloc;

	uint32 AllocCount = 0;
	uint32 UpdatedAllocCount = MockAlloc.ComputeAllocationSizeIncrease(AllocCount);

	EXPECT_EQ(UpdatedAllocCount, MockAlloc.RecommendedStartingAllocationSize());
}

TEST(Allocator_Tests, Dynamic_ComputeDecrease_EnsuresMinimum)
{
	GDynamicAllocator<int32, uint32> MockAlloc;

	uint32 AllocCount = MockAlloc.RecommendedStartingAllocationSize();
	uint32 UpdatedAllocCount = MockAlloc.ComputeAllocationSizeDecrease(AllocCount);

	EXPECT_EQ(UpdatedAllocCount, MockAlloc.RecommendedStartingAllocationSize());
}

TEST(Allocator_Tests, Dynamic_ComputeIncrease_EnsuresConsistent)
{
	GDynamicAllocator<int32, uint32> MockAlloc;

	uint32 InitialAllocCount = MockAlloc.RecommendedStartingAllocationSize();

	// First Increase is above initial
	uint32 UpdatedAllocCount_FirstInc = MockAlloc.ComputeAllocationSizeIncrease(InitialAllocCount);
	EXPECT_GT(UpdatedAllocCount_FirstInc, InitialAllocCount);

	// Decrease of first Increase is equal to initial
	uint32 UpdatedAllocCount_Dec = MockAlloc.ComputeAllocationSizeDecrease(UpdatedAllocCount_FirstInc);
	EXPECT_EQ(UpdatedAllocCount_Dec, InitialAllocCount);

	// Second Increase is above initial
	uint32 UpdatedAllocCount_SecondInc = MockAlloc.ComputeAllocationSizeIncrease(UpdatedAllocCount_Dec);
	EXPECT_GT(UpdatedAllocCount_SecondInc, InitialAllocCount);

	// First Increase is equal to second Increase
	EXPECT_EQ(UpdatedAllocCount_FirstInc, UpdatedAllocCount_SecondInc);
}

TEST(Allocator_Tests, Dynamic_ComputeDecrease_EnsuresConsistent)
{
	GDynamicAllocator<int32, uint32> MockAlloc;

	uint32 InitialAllocCount = MockAlloc.RecommendedStartingAllocationSize() << 1;

	// First Decrease is below initial
	uint32 UpdatedAllocCount_FirstDec = MockAlloc.ComputeAllocationSizeDecrease(InitialAllocCount);
	EXPECT_LT(UpdatedAllocCount_FirstDec, InitialAllocCount);

	// Increase of first Decrease is equal to initial
	uint32 UpdatedAllocCount_Inc = MockAlloc.ComputeAllocationSizeIncrease(UpdatedAllocCount_FirstDec);
	EXPECT_EQ(UpdatedAllocCount_Inc, InitialAllocCount);

	// Second Decrease is below initial
	uint32 UpdatedAllocCount_SecondInc = MockAlloc.ComputeAllocationSizeDecrease(UpdatedAllocCount_Inc);
	EXPECT_LT(UpdatedAllocCount_SecondInc, InitialAllocCount);

	// First Decrease is equal to second Decrease
	EXPECT_EQ(UpdatedAllocCount_FirstDec, UpdatedAllocCount_SecondInc);
}