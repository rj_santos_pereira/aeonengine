project(AeonFrameworkTests CXX)

enable_testing()

add_executable(${PROJECT_NAME})

include_directories(${CMAKE_SOURCE_DIR}/Source)

include(${CMAKE_SOURCE_DIR}/DefaultProjectConfig.cmake)

# Based on: https://matgomes.com/integrate-google-test-into-cmake/

# FIXME at some point when reworking the tests, make this depend on Framework only
target_link_libraries(${PROJECT_NAME}
	PUBLIC
		gtest
		AeonEngine
	)

add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Framework_Tests)

add_test(${PROJECT_NAME} ${PROJECT_NAME})

target_sources(${PROJECT_NAME}
	PRIVATE
		${CMAKE_CURRENT_SOURCE_DIR}/LaunchTests.cpp
	)

#include(GoogleTest)
#gtest_discover_tests(${PROJECT_NAME})
