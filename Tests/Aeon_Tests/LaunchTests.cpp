#include <gtest/gtest.h>

#include "Aeon/Engine/Platform/Launcher/WindowsLauncherHelpers.hpp"

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR pCmdLine, int nCmdShow)
{
	AEON_UNUSED(hInstance, hPrevInstance, pCmdLine, nCmdShow);

	int32 Argc;
	char** Argv;
	wchar** WArgv;

	Aeon::WindowsLauncherHelpers::AllocCommandLineArgs(Argc, Argv, WArgv);

	// googletest specific code
	{
		using namespace ::testing;

		InitGoogleTest(&Argc, Argv);
		UnitTest::GetInstance()->Run();
	}

	Aeon::WindowsLauncherHelpers::DeallocCommandLineArgs(Argc, Argv, WArgv);

	return EXIT_SUCCESS;
}
