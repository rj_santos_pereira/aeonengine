# AeonEngine
---

## Dev notes (as of 07/2024):

Currently nearing the end of step 0: creating a generic library (nicknamed AeonFramework) to support the rest of the project, as well as any future projects.

Already started step 1: creating the renderer, along with logic to display and interact with stuff on the screen (e.g. window manager, window, I/O etc).