project(utf8proc VERSION 2.8.0 LANGUAGES C)

message(STATUS "Library: ${PROJECT_NAME}")

add_library(${PROJECT_NAME} STATIC IMPORTED GLOBAL)

# These are identical, both compiled as Release builds,
# but are linked to different versions of the CRT (debug and release, respectively),
# to avoid warnings/errors during linking the engine.
if (CMAKE_BUILD_TYPE MATCHES Debug)
	set(UTF8PROC_LIB_FILE utf8proc_static_debug.lib)
#	set(UTF8PROC_IMPLIB_FILE utf8proc_static_debug.lib)
else()
	set(UTF8PROC_LIB_FILE utf8proc_static.lib)
#	set(UTF8PROC_IMPLIB_FILE utf8proc_static.lib)
endif()

# Need to include the compile definition in the CMakeLists of the project,
# otherwise we'll get linker errors.
set_target_properties(
	${PROJECT_NAME} PROPERTIES
	INTERFACE_INCLUDE_DIRECTORIES "${CMAKE_CURRENT_SOURCE_DIR}/include"
	INTERFACE_COMPILE_DEFINITIONS "UTF8PROC_STATIC"
	IMPORTED_LOCATION "${CMAKE_CURRENT_SOURCE_DIR}/lib/${UTF8PROC_LIB_FILE}"
)
