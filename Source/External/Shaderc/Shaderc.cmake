project(Shaderc VERSION 2023.6 LANGUAGES CXX)

message(STATUS "Library: ${PROJECT_NAME}")

# FIXME See: https://stackoverflow.com/questions/48396863/how-to-include-external-shared-library-with-several-dlls-dylibs
# and try to use INTERFACE IMPORTED to include all .libs under a single interface

# Toggle between machine-local SDK path or project lib path
set( VULKAN_SDK_PATH $ENV{VULKAN_SDK} )
#set( VULKAN_SDK_PATH ${CMAKE_CURRENT_SOURCE_DIR} )

add_library(${PROJECT_NAME} SHARED IMPORTED GLOBAL)

if (CMAKE_BUILD_TYPE MATCHES Debug)
#	set(SHADERC_LIB_FILE shadercd.lib)
	set(SHADERC_LIB_FILE shaderc_sharedd.dll)
	set(SHADERC_IMPLIB_FILE shaderc_sharedd.lib)
else ()
#	set(SHADERC_LIB_FILE shaderc.lib)
	set(SHADERC_LIB_FILE shaderc_shared.dll)
	set(SHADERC_IMPLIB_FILE shaderc_shared.lib)
endif ()

set_target_properties(
	${PROJECT_NAME} PROPERTIES
	INTERFACE_INCLUDE_DIRECTORIES "${VULKAN_SDK_PATH}/Include"
#	INTERFACE_COMPILE_DEFINITIONS ""
	# NOTE The dlls in VulkanSDK are in the Bin folder instead of Lib
	IMPORTED_LOCATION "${VULKAN_SDK_PATH}/Bin/${SHADERC_LIB_FILE}"
	IMPORTED_IMPLIB "${VULKAN_SDK_PATH}/Lib/${SHADERC_IMPLIB_FILE}"
)
