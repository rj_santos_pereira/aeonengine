project(GLFW VERSION 3.4 LANGUAGES C)

message(STATUS "Library: ${PROJECT_NAME}")

# TODO See https://stackoverflow.com/questions/10671916 and rpath on linux

add_library(${PROJECT_NAME} SHARED IMPORTED GLOBAL)

if (CMAKE_BUILD_TYPE MATCHES Debug)
	set(GLFW_LIB_FILE glfw3.dll)
	set(GLFW_IMPLIB_FILE glfw3dll.lib)
else ()
	set(GLFW_LIB_FILE glfw3.dll)
	set(GLFW_IMPLIB_FILE glfw3dll.lib)
endif ()

set_target_properties(
	${PROJECT_NAME} PROPERTIES
	INTERFACE_INCLUDE_DIRECTORIES "${CMAKE_CURRENT_SOURCE_DIR}/include"
#	INTERFACE_COMPILE_DEFINITIONS ""
	IMPORTED_LOCATION "${CMAKE_CURRENT_SOURCE_DIR}/lib/${GLFW_LIB_FILE}"
	IMPORTED_IMPLIB "${CMAKE_CURRENT_SOURCE_DIR}/lib/${GLFW_IMPLIB_FILE}"
)
