project(VulkanSDK VERSION 1.3.261.1 LANGUAGES CXX)

message(STATUS "Library: ${PROJECT_NAME}")

# TODO eventually dl includes from https://github.com/KhronosGroup/Vulkan-Headers/tree/main
#  and use loader from platform (or compile it from https://github.com/KhronosGroup/Vulkan-Loader/tree/main,
#  see https://github.com/KhronosGroup/Vulkan-Loader/blob/main/BUILD.md),
#  on Windows, loader is '$env:SystemRoot\System32\vulkan-1.dll' (use Tools/Get-ItemMetadata.ps1 to obtain correct version using PropertyId 166);
#  on Linux, loader is named 'libvulkan.so.1' (need to find location);
#  on macOS, loader is named 'libvulkan.1.dylib' (need to find location);

# NOTE In reality, this is a shared library disguised as a static one,
#  the lib file is simply an import library for the installed Vulkan loader (which is a dynamic library in most/all systems).
add_library(${PROJECT_NAME} STATIC IMPORTED GLOBAL)

# Toggle between machine-local SDK path or project lib path
set( VULKAN_SDK_PATH $ENV{VULKAN_SDK} )
#set( VULKAN_SDK_PATH ${CMAKE_CURRENT_SOURCE_DIR} )

message(STATUS "VulkanSDK path: ${VULKAN_SDK_PATH}")

# FIXME add check to ensure VulkanSDK is installed

#	"${VULKAN_SDK_PATH}/Lib/dxcompiler.lib"
#	"${VULKAN_SDK_PATH}/Lib/GenericCodeGend.lib"
#	"${VULKAN_SDK_PATH}/Lib/glslang-default-resource-limitsd.lib"
#	"${VULKAN_SDK_PATH}/Lib/glslangd.lib"
#	"${VULKAN_SDK_PATH}/Lib/HLSLd.lib"
#	"${VULKAN_SDK_PATH}/Lib/MachineIndependentd.lib"
#	"${VULKAN_SDK_PATH}/Lib/OGLCompilerd.lib"
#	"${VULKAN_SDK_PATH}/Lib/OSDependentd.lib"
#	"${VULKAN_SDK_PATH}/Lib/shadercd.lib"
#	"${VULKAN_SDK_PATH}/Lib/shaderc_combinedd.lib"
#	"${VULKAN_SDK_PATH}/Lib/shaderc_sharedd.lib"
#	"${VULKAN_SDK_PATH}/Lib/shaderc_utild.lib"
#	"${VULKAN_SDK_PATH}/Lib/spirv-cross-c-sharedd.lib"
#	"${VULKAN_SDK_PATH}/Lib/spirv-cross-cd.lib"
#	"${VULKAN_SDK_PATH}/Lib/spirv-cross-cored.lib"
#	"${VULKAN_SDK_PATH}/Lib/spirv-cross-cppd.lib"
#	"${VULKAN_SDK_PATH}/Lib/spirv-cross-glsld.lib"
#	"${VULKAN_SDK_PATH}/Lib/spirv-cross-hlsld.lib"
#	"${VULKAN_SDK_PATH}/Lib/spirv-cross-msld.lib"
#	"${VULKAN_SDK_PATH}/Lib/spirv-cross-reflectd.lib"
#	"${VULKAN_SDK_PATH}/Lib/spirv-cross-utild.lib"
#	"${VULKAN_SDK_PATH}/Lib/SPIRV-Tools-diffd.lib"
#	"${VULKAN_SDK_PATH}/Lib/SPIRV-Tools-linkd.lib"
#	"${VULKAN_SDK_PATH}/Lib/SPIRV-Tools-lintd.lib"
#	"${VULKAN_SDK_PATH}/Lib/SPIRV-Tools-optd.lib"
#	"${VULKAN_SDK_PATH}/Lib/SPIRV-Tools-reduced.lib"
#	"${VULKAN_SDK_PATH}/Lib/SPIRV-Tools-sharedd.lib"
#	"${VULKAN_SDK_PATH}/Lib/SPIRV-Toolsd.lib"
#	"${VULKAN_SDK_PATH}/Lib/SPIRVd.lib"
#	"${VULKAN_SDK_PATH}/Lib/SPVRemapperd.lib"
#	"${VULKAN_SDK_PATH}/Lib/VkLayer_utils.lib"
#	"${VULKAN_SDK_PATH}/Lib/vulkan-1.lib"

set(VULKANSDK_LIB_FILE vulkan-1.lib)

# Need to include the compile definition in the CMakeLists of the project,
# otherwise we'll get linker errors.
set_target_properties(
	${PROJECT_NAME} PROPERTIES
	INTERFACE_INCLUDE_DIRECTORIES "${VULKAN_SDK_PATH}/Include"
#	INTERFACE_COMPILE_DEFINITIONS "<add-macro-defs-here>"
	IMPORTED_LOCATION "${VULKAN_SDK_PATH}/Lib/${VULKANSDK_LIB_FILE}"
)


