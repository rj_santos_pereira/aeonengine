project(fmt VERSION 10.0.0 LANGUAGES CXX)

message(STATUS "Library: ${PROJECT_NAME}")

add_library(${PROJECT_NAME} STATIC IMPORTED GLOBAL)

#include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include)

# These are identical, both compiled as Release builds,
# but are linked to different versions of the CRT (debug and release, respectively),
# to avoid warnings/errors during linking the engine.
if (CMAKE_BUILD_TYPE MATCHES Debug)
	set(FMT_LIBRARY_NAME fmt_debug.lib)
else()
	set(FMT_LIBRARY_NAME fmt.lib)
endif()

# Need to include the compile definition in the CMakeLists of the project,
# otherwise we'll get linker errors.
set_target_properties(
	${PROJECT_NAME} PROPERTIES
	INTERFACE_INCLUDE_DIRECTORIES "${CMAKE_CURRENT_SOURCE_DIR}/include"
#	INTERFACE_COMPILE_DEFINITIONS "UTF8PROC_STATIC"
	IMPORTED_LOCATION "${CMAKE_CURRENT_SOURCE_DIR}/lib/${FMT_LIBRARY_NAME}"
)
