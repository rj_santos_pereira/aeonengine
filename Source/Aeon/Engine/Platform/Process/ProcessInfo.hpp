#ifndef ZZINTERNAL_INCLUDE_AEON_ENGINE_PLATFORM_PROCESS_PROCESSINFO
#define ZZINTERNAL_INCLUDE_AEON_ENGINE_PLATFORM_PROCESS_PROCESSINFO

#include "Aeon/Framework/Framework.hpp"

namespace Aeon
{
	enum struct EDebuggerName : uint8
	{
		None = 0x00,

		VSRemoteMonitor = 0x01,
		LLDB = 0x02,

		Unknown = 0xFF,
	};

	class SProcessInfo
	{
	public:
	//	explicit SProcessInfo(...);

		static EDebuggerName DetectRunningDebugger();

	private:
	};
}

#endif