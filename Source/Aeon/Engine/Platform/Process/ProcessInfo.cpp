#include "Aeon/Engine/Platform/Process/ProcessInfo.hpp"

namespace Aeon
{
	EDebuggerName SProcessInfo::DetectRunningDebugger()
	{
		if (!AEON_HAS_DEBUGGER())
		{
			return EDebuggerName::None;
		}

		constexpr SStringView DebuggerProcessArray[] =
			{
				"msvsmon.exe", // Visual Studio 2022 Remote Debugger
				"LLDBFrontend.exe", // Windows LLVM Debugger
			};
		constexpr EDebuggerName DebuggerNameMap[] =
			{
				EDebuggerName::VSRemoteMonitor,
				EDebuggerName::LLDB,
			};

		EDebuggerName Debugger = EDebuggerName::Unknown;

#	if AEON_PLATFORM_WINDOWS
		DWORD ProcessArray[1024];
		DWORD ProcessArrayByteSize;

		AEON_FAILSAFE(EnumProcesses(ProcessArray, sizeof ProcessArray, &ProcessArrayByteSize),
					  return EDebuggerName::Unknown,
					  "Failed to enumerate processes!");

		szint NumProcesses = ProcessArrayByteSize / sizeof(DWORD);
		for (szint ProcIdx = 0; ProcIdx < NumProcesses; ++ProcIdx)
		{
			DWORD ProcessId = ProcessArray[ProcIdx];
			HANDLE ProcessHandle = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, ProcessId);

			if (ProcessHandle == NULL)
			{
				// Process may have terminated in the meantime, or we don't have permissions, ignore it regardless
				continue;
			}

			HMODULE ModuleHandle;
			DWORD Dummy; // RequiredModuleArrayByteSize

			constexpr szint MaxProcessNameLength = 1024;
			char ProcessNameBuffer[MaxProcessNameLength];

			if (!EnumProcessModules(ProcessHandle, &ModuleHandle, sizeof ModuleHandle, &Dummy))
			{
				// Couldn't enumerate modules (possibly due to lack of permission?), so probably not what we are looking for
				continue;
			}

			szint ProcessNameLength = GetModuleBaseNameA(ProcessHandle, ModuleHandle, ProcessNameBuffer,
														 MaxProcessNameLength);

			AEON_FAILSAFE(0 < ProcessNameLength && ProcessNameLength < MaxProcessNameLength,
						  continue,
						  "Invalid process name");

			SStringView ProcessName{ ProcessNameBuffer, uint32(ProcessNameLength) };

		//	AEON_DBGLOG("Process ID: {}; Name: {}", ProcessId, ProcessName);

			szint DbgIdx = 0;
			for (auto DebuggerName : DebuggerProcessArray)
			{
				// NOTE This is imperfect, as it assumes you only have one debugger process running,
				//  which is attached to the current process, but that's good enough for now
				if (ProcessName == DebuggerName)
				{
				//	AEON_DBGLOG("Found debugger: {}", ProcessName);
					Debugger = DebuggerNameMap[DbgIdx];
					break;
				}

				++DbgIdx;
			}

			if (Debugger != EDebuggerName::Unknown)
			{
				break;
			}
		}
#	else
		// TODO implement this
#		error
#	endif

		return Debugger;
	}
}
