# Add sources to MainModule library
# FIXME currently these need to be in MainModule since Tests/Benchmarks use these
if (AEON_PLATFORM_WINDOWS)
	target_sources(${PROJECT_NAME}
		PUBLIC
			${CMAKE_CURRENT_SOURCE_DIR}/WindowsLauncherHelpers.hpp
		PRIVATE
			${CMAKE_CURRENT_SOURCE_DIR}/WindowsLauncherHelpers.cpp
		)
else()
	message(FATAL_ERROR "Not implemented")
endif()

project(AeonStudio CXX)

add_executable(${PROJECT_NAME})

include(${CMAKE_SOURCE_DIR}/DefaultProjectConfig.cmake)

target_link_libraries(${PROJECT_NAME}
	PUBLIC
		AeonEngine
	)

# This is used to copy any dlls from External dependencies into the executable folder
# See https://cmake.org/cmake/help/latest/manual/cmake-generator-expressions.7.html#genex:TARGET_RUNTIME_DLLS
# FIXME eventually make most dependencies (of MainModule) be static libs (except system dependencies, like Vulkan), or make MainModule be a dynamic lib (or both)
# TODO convert MainModule to a dynamic lib, as to simplify linking game modules to the engine, as the game will be the main exec
add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD
	COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_RUNTIME_DLLS:${PROJECT_NAME}> $<TARGET_FILE_DIR:${PROJECT_NAME}>
	COMMAND_EXPAND_LISTS
	)

# TODO Do this in Dev builds instead, so we can continue to iterate on the shaders without changing folders (needs symlink support in AeonFramework)
#add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD
#	COMMAND ${CMAKE_COMMAND} -E create_symlink ${AEON_SOURCE_DIR}/Shader $<TARGET_FILE_DIR:${PROJECT_NAME}>/Shader
#	)
add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD
	COMMAND ${CMAKE_COMMAND} -E copy_directory_if_different ${AEON_SOURCE_DIR}/Shader $<TARGET_FILE_DIR:${PROJECT_NAME}>/Shader
	)

# TODO select sources based on platform
if (AEON_PLATFORM_WINDOWS)
	target_sources(${PROJECT_NAME}
	#	PUBLIC
	#		${CMAKE_CURRENT_SOURCE_DIR}/WindowsLauncherHelpers.hpp
		PRIVATE
	#		${CMAKE_CURRENT_SOURCE_DIR}/WindowsLauncherHelpers.cpp
			${CMAKE_CURRENT_SOURCE_DIR}/WindowsLauncher.cpp
		)
else()
	message(FATAL_ERROR "Not implemented")
endif()
