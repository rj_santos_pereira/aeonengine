#include "Aeon/Framework/Framework.hpp"

#include "Aeon/Engine/Logging/Log.hpp"

#include "Aeon/Engine/Graphics/Rendering/Vulkan/VulkanRenderer.hpp"
#include "Aeon/Engine/System/Display/WindowManager.hpp"

#include "Aeon/Engine/System/Filesystem/EnginePaths.hpp"

#include "Aeon/Engine/Platform/Launcher/WindowsLauncherHelpers.hpp"

// TODO move this elsewhere
ZZINTERNAL_AEON_OVERRIDE_STANDARD_ALLOCATION_FUNCTIONS
// TODO likewise ?
AEON_DECLARE_LOG_FAMILY(ProcessLaunch, ETerminalColor::PaleGreen,
					    ELogSourceInfo::FileName | ELogSourceInfo::LineNum | ELogSourceInfo::FuncName,
					    ELogDateTimeInfo::Date | ELogDateTimeInfo::Time);

struct SConstructorLogger
{
	SConstructorLogger()
	{
		AEON_LOG(Info, ProcessLaunch, "Ctor: {}", bit_cast<ptrint>(this));
	}

	SConstructorLogger(SConstructorLogger const&)
	{
		AEON_LOG(Info, ProcessLaunch, "Copy: {}", bit_cast<ptrint>(this));
	}
	SConstructorLogger(SConstructorLogger&&)
	{
		AEON_LOG(Info, ProcessLaunch, "Move: {}", bit_cast<ptrint>(this));
	}

	SConstructorLogger& operator=(SConstructorLogger Other)
	{
		AEON_UNUSED(Other);
		return *this;
	}

	~SConstructorLogger()
	{
		AEON_LOG(Info, ProcessLaunch, "Dtor: {}", bit_cast<ptrint>(this));
	}
};

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR pCmdLine, int nCmdShow)
{
	AEON_UNUSED(hInstance, hPrevInstance, pCmdLine, nCmdShow);

#if 0
	HANDLE ProcessHandle;
	HANDLE ProcessPseudoHandle = GetCurrentProcess();
	DuplicateHandle(/* hSourceProcessHandle */ ProcessPseudoHandle,
					/* hSourceHandle */ ProcessPseudoHandle,
					/* hTargetProcessHandle */ ProcessPseudoHandle,
					/* lpTargetHandle */ addressof(ProcessHandle),
					/* dwDesiredAccess */ 0,
					/* bInheritHandle */ FALSE,
					/* dwOption */ DUPLICATE_SAME_ACCESS);
#endif

	HINSTANCE ProcInstance;
	GetModuleHandleExW(GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT,
					   NULL,
					   addressof(ProcInstance));

	int32 Argc;
	char** Argv;
	wchar** WArgv;

	Aeon::WindowsLauncherHelpers::AllocCommandLineArgs(Argc, Argv, WArgv);
	Aeon::WindowsLauncherHelpers::AllocConsole();

	// TODO move this elsewhere
	::timeBeginPeriod(1);

	Aeon::InitPlatformLoggerDevice();

	using namespace Aeon;

#if 0
	int AtExitRetVal = std::atexit([] {
		// Flush all open streams
		_flushall();
	});

	if (AtExitRetVal)
	{
		AEON_LOG(Error, ProcessLaunch, "Failed to setup atexit function");
		return EXIT_FAILURE;
	}
#endif

#if 0 // TODO move this to benchmark
	GHashMap<int32, SString> HashTest1{ DHashMapParams{ .InitialElementCount = 15, .LoadFactorLimit = 0.95f } };

	HashTest1.Add(1, "Hi");
	HashTest1.Add(2, "my");
	HashTest1.Add(3, "name");
	HashTest1.Add(5, "is");
	HashTest1.Add(8, "Morty");
	HashTest1.Add(13, "from");
	HashTest1.Add(21, "the");
	HashTest1.Add(34, "hit");
	HashTest1.Add(55, "show");
	HashTest1.Add(89, "Rick");
	HashTest1.Add(144, "and");
	HashTest1.Add(233, "Morty");
	HashTest1.Add(377, "Oh");
	HashTest1.Add(610, "Geez");
	HashTest1.Add(987, "Rick");

	HashTest1.Relink();

	for (auto& Elem : HashTest1)
	{
		AEON_LOG(Info, ProcessLaunch, "Key: {} Value: {}", Elem.Key(), Elem.Value());
	}
#endif

#if 0 // TODO move this to benchmark
	SDirectory FileDir = SDirectory::GetWorkingDirectory().Parent();

	AEON_CONDITIONAL_SCOPE_GUARD(SDirectory::SwitchWorkingContext(FileDir))
	{
		GRandomGenerator<uint32> RNG;
		GArray<uint32> Data{ TCreateElements::Tag, 1'000'000/*'000*/ };

		{
			auto Start = STimePoint::Now();
			for (auto& Elem : Data)
			{
				Elem = RNG();
			}
			auto End = STimePoint::Now();

			AEON_LOG(Info, ProcessLaunch, "Time generating elements: {:%T}", STimeSpan::Difference(End, Start));
		}

		SPath FilePath = SDirectory::GetWorkingDirectory().Path() / pstr("TestBytes.txt");
		SFile File = SFile::Create(FilePath, EFileAccessMode::DiscardWrite);

		if (File.IsValid() && File.IsOpen())
		{
			auto Start = STimePoint::Now();
			File.WriteBytes(reinterpret_cast<cbyteptr>(Data.Buffer()), Aeon::SizeOf(uint32, Data.Count()));
			auto End = STimePoint::Now();

			AEON_LOG(Info, ProcessLaunch, "Time writing to file: {:%T}", STimeSpan::Difference(End, Start));
		}
	}
#endif

	SPath EnginePath = *SPath{ WArgv[0] };

	SEnginePaths::SetupEnginePaths(EnginePath);

	IWindowManager& WindowManager = IWindowManager::Instance();
	WindowManager.Initialize();

	DWindowInitParams WindowParams;
	WindowParams.Title = "Aeon Studio";
//	WindowParams.CursorMode = ECursorMode::CapturedVirtual;
	HWindow WindowHnd = WindowManager.CreateWindow(WindowParams);

	auto& Window = WindowManager.GetWindow(WindowHnd);

	SVulkanRenderer& Renderer = SVulkanRenderer::Instance();

	Renderer.Initialize();

	SVulkanShaderManager& ShaderManager = Renderer.GetShaderManager();

	ShaderManager.RefreshShaderList();

	ShaderManager.ReloadShaders();

	if (ShaderManager.NeedsCompileShaders())
	{
		ShaderManager.CompileShaders();
	}

	Renderer.DetectDevices();

	SVulkanDeviceFeatureSet DeviceFeatures;
#if AEON_BUILD_DEVELOPMENT
	DeviceFeatures.SetBoundsCheckedBufferAccessSupport(true);
	DeviceFeatures.SetBoundsCheckedImageAccessSupport(true);
#endif
	DeviceFeatures.SetFullInt32RangeIndexSupport(true);
	DeviceFeatures.SetGeometryShaderSupport(true);
	DeviceFeatures.SetTessellationShaderSupport(true);
//	DeviceFeatures.SetAnisotropicFilteringSupport(true);
//	DeviceFeatures.SetWireframeFillModeSupport(true);

//	DeviceFeatures.SetPrivateDataSupport(true);
	DeviceFeatures.SetDynamicRenderingSupport(true);
	DeviceFeatures.SetSynchronizationCommands2Support(true);
	DeviceFeatures.SetMaintenance4Support(true);

	if (Renderer.SelectDevice(DeviceFeatures))
	{
		SVulkanDevice& Device = Renderer.GetSelectedDevice();

		SVulkanDeviceQueueRequirementList QueueConfigRequirements;

		// Typical deferred-rendering pipeline is sequential in its stages, so request a single queue for now.
		QueueConfigRequirements.AddRequirement(EVulkanDeviceQueueCapability::Graphics
											   | EVulkanDeviceQueueCapability::Transfer, 1,
											   EVulkanDeviceQueuePresentationSupport::True);
		// CHECKME Request a single compute queue for now, for simple async jobs,
		//  but eventually change this if necessary (some devices, e.g. AMD, may not have that many queues).
		QueueConfigRequirements.AddRequirement(EVulkanDeviceQueueCapability::Compute
											   | EVulkanDeviceQueueCapability::Transfer, 1,
											   EVulkanDeviceQueuePresentationSupport::False);
		// Two (exclusively) transfer queues typically represent a duplex host-device connection,
		//  ideal for copying data from CPU to GPU and back.
		QueueConfigRequirements.AddRequirement(EVulkanDeviceQueueCapability::Transfer, 2,
											   EVulkanDeviceQueuePresentationSupport::False);

		GOptional<SVulkanDeviceQueueConfig> QueueConfig = Device.MakeQueueConfig(QueueConfigRequirements);

		if (!QueueConfig)
		{
			// TODO Log Error
		}

		bool HasInitLogical = Device.SetupLogicalDevice(DeviceFeatures, *QueueConfig);
		if (!HasInitLogical)
		{
			// TODO Log Error
		}

		bool HasInitSurfaces = Device.SetupSurfaces();
		if (!HasInitSurfaces)
		{
			// TODO Log Error
		}

		SVulkanSurface& Surface = Device.GetSurface(WindowHnd);

		SVulkanSurfaceSwapchainConfig SwapchainConfig;

		SwapchainConfig.SetSurfaceFormat(VK_FORMAT_B8G8R8A8_SRGB);
		SwapchainConfig.SetSurfaceColorSpace(VK_COLOR_SPACE_SRGB_NONLINEAR_KHR);
		SwapchainConfig.SetPresentationMode(VK_PRESENT_MODE_FIFO_RELAXED_KHR);
		SwapchainConfig.SetImageCount(3);

		bool HasInitSwapchain = Surface.SetupSwapchain(SwapchainConfig);
		if (!HasInitSwapchain)
		{
			// TODO Log Error
		}

		// TODO Do this once shaders are compiled (might need to do this on a loop)
		//  also probably better to do this within SVulkanDevice, so that the device keeps a ref (SharedPtr) to each shader that it is using
	//	auto ShaderList = ShaderManager.QueryShaderList();
	//
	//	for (auto& Shader : ShaderList)
	//	{
	//		Shader->SetupModule(Device);
	//	}
	}

	struct MainWindowManager : AEON_EVENT_HANDLER
	{
		MainWindowManager(MainWindowManager const& Other)
			: WindowManager(Other.WindowManager)
			, Window(Other.Window)
		{
		}

		MainWindowManager(IWindowManager* NewWindowManager, IWindow* NewWindow)
			: WindowManager(NewWindowManager)
			, Window(NewWindow)
		{
		}

		void HandleKeyEvent(EVirtualKey Key, EKeyEvent Event, EKeyModifierState)
		{
			AEON_LOG(Info, ProcessLaunch, "Key {} Event: {}", uint32(Key), MakeKeyEventString(Event));

			if (Key == EVirtualKey::Key_Escape && Aeon::HasFlag(Event, EKeyEvent::Press))
			{
				WindowManager->RequestCloseWindow(Window->GetHandle());
			}
			if (Key == EVirtualKey::Key_NumPad0 && Aeon::HasFlag(Event, EKeyEvent::Press))
			{
				Window->SetPosition(SIntVector2{ 20, 40 });
			}
			if (Key == EVirtualKey::Key_Q && Aeon::HasFlag(Event, EKeyEvent::Press))
			{
				SVulkanRenderer& Renderer = SVulkanRenderer::Instance();
				SVulkanSurface& Surface = Renderer.GetSelectedDevice().GetSurface(Window->GetHandle());

				for (uint32 Index = 0; Index < Surface.GetSupportedFormatCount(); ++Index)
				{
					auto Format = Surface.GetSupportedFormat(Index);
					AEON_LOG(Info, ProcessLaunch, "Format: {} Colorspace: {}",
							 MakeFormatString(Format.format), MakeColorspaceString(Format.colorSpace));
				}
			}
		}
		void HandleFullscreenEvent(EVirtualKey, EKeyEvent Event, EKeyModifierState)
		{
			if (Aeon::HasFlag(Event, EKeyEvent::Press))
			{
				bool IsFS = Window->IsFullscreen();
				Window->ToggleFullscreen(!IsFS);
			}
		}
		void HandleCursorEvent(ECursorButton Button, ECursorEvent Event)
		{
			AEON_LOG(Info, ProcessLaunch, "Button {} Event: {}", uint32(Button), MakeCursorEventString(Event));
		}

		//void HandleCursorScroll(SVector2 ScrollDelta)
		//{
		//	AEON_LOG(Info, ProcessLaunch, "Cursor scroll=({},{})", ScrollDelta.X, ScrollDelta.Y);
		//}

		//void HandleCursorPosition(SVector2 CursorPosition, SVector2 CursorDelta)
		//{
		//	AEON_UNUSED(CursorDelta);
		//	AEON_LOG(Info, ProcessLaunch, "Cursor position=({},{}) delta=({},{})", CursorPosition.X, CursorPosition.Y, CursorDelta.X, CursorDelta.Y);
		//}

		IWindowManager* WindowManager;
		IWindow* Window;
	}
	MWM{ &WindowManager, &Window };

	Window.OnKeyEvent.Bind(&MWM, &MainWindowManager::HandleKeyEvent);
	Window.OnKeyEvent.BindWithMask(EVirtualKey::Key_F11, &MWM, &MainWindowManager::HandleFullscreenEvent);
	Window.OnCursorEvent.Bind(&MWM, &MainWindowManager::HandleCursorEvent);
//	Window.OnCursorScrollEvent.Bind(&MWM, &MainWindowManager::HandleCursorScroll);
//	Window.OnCursorPositionChanged.Bind(&MWM, &MainWindowManager::HandleCursorPosition);

//	bool HasPrinted = false;
//	STimePoint PrintTimer = STimePoint::Now();

	while (!WindowManager.ShouldCloseWindow(WindowHnd))
	{
		WindowManager.ProcessEvents();

	//	if (!HasPrinted && STimeSpan::Difference(STimePoint::Now(), PrintTimer) > STimeSpan::MakeSeconds(5))
	//	{
	//		HasPrinted = true;
	//	}

		if (ShaderManager.IsCompilingShaders())
		{
			ShaderManager.ReloadFinishedCompilingShaders();
		}
	}

	Renderer.Terminate();

	WindowManager.DestroyWindow(WindowHnd);

	WindowManager.Terminate();

	// TODO move this elsewhere
	::timeEndPeriod(1);

	Aeon::WindowsLauncherHelpers::DeallocConsole();
	Aeon::WindowsLauncherHelpers::DeallocCommandLineArgs(Argc, Argv, WArgv);

	return EXIT_SUCCESS;
}
