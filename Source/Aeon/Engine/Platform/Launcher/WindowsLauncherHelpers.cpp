#include "Aeon/Engine/Platform/Launcher/WindowsLauncherHelpers.hpp"

namespace
{
// Adapted from https://stackoverflow.com/a/25927081

	[[maybe_unused]]
	decltype(stdin) DupCrtIn;
	[[maybe_unused]]
	decltype(stdout) DupCrtOut;
	[[maybe_unused]]
	decltype(stderr) DupCrtErr;

	[[maybe_unused]]
	std::FILE* BindCrtToStdHandle(std::FILE* CrtHandle, HANDLE StdHandle, const char* Mode)
	{
		// Re-initialize the C runtime "FILE" handles with clean handles bound to "nul". We do this because it has been
		// observed that the file number of our standard handle file objects can be assigned internally to a value of -2
		// when not bound to a valid target, which represents some kind of unknown internal invalid state. In this state our
		// call to "_dup2" fails, as it specifically tests to ensure that the target file number isn't equal to this value
		// before allowing the operation to continue. We can resolve this issue by first "re-opening" the target files to
		// use the "nul" device, which will place them into a valid state, after which we can redirect them to our target
		// using the "_dup2" function.

		std::FILE* NulCrtHandle;
		freopen_s(&NulCrtHandle, "nul", Mode, CrtHandle);

		// We need to duplicate the handle so that _open_osfhandle doesn't make the original one invalid,
		// causing problems on shutdown when the CRT tries to destroy them.
		HANDLE DupStdHandle = INVALID_HANDLE_VALUE;
		DuplicateHandle(GetCurrentProcess(), StdHandle, GetCurrentProcess(), &DupStdHandle, 0, FALSE, DUPLICATE_SAME_ACCESS);
		if (DupStdHandle == INVALID_HANDLE_VALUE)
		{
			return nullptr;
		}

		// See https://docs.microsoft.com/en-us/cpp/c-runtime-library/reference/open-osfhandle?view=msvc-160
		int StdFd = _open_osfhandle(bit_cast<std::intptr_t>(DupStdHandle), _O_U8TEXT);
		if (StdFd == -1)
		{
			return nullptr;
		}

		std::FILE* NewCrtHandle = _fdopen(StdFd, Mode);
		if (NewCrtHandle == nullptr)
		{
			// If _fdopen fails, we need to close the file using the descriptor
			_close(StdFd);
			return nullptr;
		}

		if (_dup2(_fileno(NewCrtHandle), _fileno(CrtHandle)) == 0)
		{
			if (setvbuf(CrtHandle, nullptr, _IOFBF, BUFSIZ) == 0) // TODO might want to adjust these params
			{
				return NewCrtHandle;
			}
		}

		// Otherwise, if _dup2 or setvbuf fails, we need to close the file using the pointer
		fclose(NewCrtHandle);
		return nullptr;
	}

	[[maybe_unused]]
	void UnbindCrtHandle(std::FILE* DupCrtHandle, std::FILE* CrtHandle, const char* Mode)
	{
		if (DupCrtHandle)
		{
			fclose(DupCrtHandle);
		}

		std::FILE* NulCrtHandle;
		freopen_s(&NulCrtHandle, "nul", Mode, CrtHandle);
	}
}

namespace Aeon::WindowsLauncherHelpers
{
	void AllocCommandLineArgs(int32& NewArgc, char**& NewArgv, wchar**& NewWArgv)
	{
		wchar* CmdLine = ::GetCommandLineW();
		szint CmdLineLen = Aeon::StringOps::Length(CmdLine);

		int32 Argc;
		wchar** WArgv = ::CommandLineToArgvW(CmdLine, addressof(Argc));
		char** Argv = new char*[Argc + 1];

		// We'll use the same buffer for all strings of Argv,
		// which makes the pointers in Argv just offsets into the same buffer
		char* ArgvBuf = new char[CmdLineLen + Argc];

		NewArgc = Argc;
		NewArgv = Argv;
		NewWArgv = WArgv;

		char* CurrArgvBuf = ArgvBuf;
		for (int32 Index = 0; Index < Argc; ++Index)
		{
			std::mbstate_t State{};

			wchar const* CurrWArgv = WArgv[Index];
			char* CurrArgv = Argv[Index] = CurrArgvBuf;

			// Currently holds the size of the wchar string...
			szint CurrArgvLen = Aeon::StringOps::Length(CurrWArgv);
			// ... But now it's overwritten with the size of the resulting char string
			CurrArgvLen = std::wcsrtombs(CurrArgv, addressof(CurrWArgv), CurrArgvLen, addressof(State));

			CurrArgv[CurrArgvLen++] = Aeon::StringOps::NullTerminator<char>;
			CurrArgvBuf += CurrArgvLen;
		}

		Argv[Argc] = nullptr;
	}

	void DeallocCommandLineArgs(int32 Argc, char** Argv, wchar** WArgv)
	{
		AEON_UNUSED(Argc);

		// First delete the underlying string buffer, which will always be pointed to by the first element of argv
		delete[] *Argv;
		// Then just delete the argv buffer itself
		delete[] Argv;

		// Now free the wargv buffer with LocalFree,
		// which is the WinAPI-sanctioned way to free memory obtained from CommandLineToArgvW
		::LocalFree(WArgv);
	}

	// TODO move this to a DeviceLogger
	void AllocConsole()
	{
#	if AEON_BUILD_DISTRIBUTION
		::AllocConsole();

		// CHECKME CLion does things differently?
		freopen_s(&DupCrtIn, "CONIN$", "r", stdin);
		freopen_s(&DupCrtOut, "CONOUT$", "w", stdout);
		freopen_s(&DupCrtErr, "CONOUT$", "w", stderr);

	//	DupCrtIn = BindCrtToStdHandle(stdin, GetStdHandle(STD_INPUT_HANDLE), "r");
	//	DupCrtOut = BindCrtToStdHandle(stdout, GetStdHandle(STD_OUTPUT_HANDLE), "w");
	//	DupCrtErr = BindCrtToStdHandle(stderr, GetStdHandle(STD_ERROR_HANDLE), "w");
#	endif

		// TODO ensure BUFSIZ is the ideal size for the platform
		std::setvbuf(stdout, nullptr, _IOFBF, BUFSIZ);
		std::setvbuf(stderr, nullptr, _IONBF, BUFSIZ);

	//	std::ios::sync_with_stdio(false);

		std::cin.clear();
		std::cout.clear();
		std::cerr.clear();

		std::wcin.clear();
		std::wcout.clear();
		std::wcerr.clear();
	}

	void DeallocConsole()
	{
#	if AEON_BUILD_DISTRIBUTION
	//	UnbindCrtHandle(DupCrtIn, stdin, "r");
	//	UnbindCrtHandle(DupCrtOut, stdout, "w");
	//	UnbindCrtHandle(DupCrtErr, stderr, "w");

		::fflush(stdout);
		::fflush(stderr);

		::FreeConsole();
#	endif
	}
}
