#ifndef ZZINTERNAL_INCLUDE_AEON_ENGINE_PLATFORM_LAUNCH_WINDOWSLAUNCHERHELPERS
#define ZZINTERNAL_INCLUDE_AEON_ENGINE_PLATFORM_LAUNCH_WINDOWSLAUNCHERHELPERS

#include "Aeon/Framework/Framework.hpp"

namespace Aeon::WindowsLauncherHelpers
{
	void AllocCommandLineArgs(int32& NewArgc, char**& NewArgv, wchar**& NewWArgv);

	void DeallocCommandLineArgs(int32 Argc, char** Argv, wchar** WArgv);

	void AllocConsole();

	void DeallocConsole();
}

#endif