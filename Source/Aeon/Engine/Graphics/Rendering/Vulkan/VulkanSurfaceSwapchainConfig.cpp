#include "Aeon/Engine/Graphics/Rendering/Vulkan/VulkanSurfaceSwapchainConfig.hpp"

namespace Aeon
{
	VkFormat SVulkanSurfaceSwapchainConfig::GetSurfaceFormat() const
	{
		return SurfaceFormat;
	}

	VkColorSpaceKHR SVulkanSurfaceSwapchainConfig::GetSurfaceColorSpace() const
	{
		return SurfaceColorSpace;
	}

	VkPresentModeKHR SVulkanSurfaceSwapchainConfig::GetPresentationMode() const
	{
		return PresentationMode;
	}

	uint32 SVulkanSurfaceSwapchainConfig::GetImageCount() const
	{
		return ImageCount;
	}

	void SVulkanSurfaceSwapchainConfig::SetSurfaceFormat(VkFormat Format)
	{
		SurfaceFormat = Format;
	}

	void SVulkanSurfaceSwapchainConfig::SetSurfaceColorSpace(VkColorSpaceKHR ColorSpace)
	{
		SurfaceColorSpace = ColorSpace;
	}

	void SVulkanSurfaceSwapchainConfig::SetPresentationMode(VkPresentModeKHR Mode)
	{
		PresentationMode = Mode;
	}

	void SVulkanSurfaceSwapchainConfig::SetImageCount(uint32 Count)
	{
		ImageCount = Count;
	}
}
