#ifndef ZZINTERNAL_INCLUDE_AEON_ENGINE_GRAPHICS_RENDERING_VULKAN_VULKANDEVICE
#define ZZINTERNAL_INCLUDE_AEON_ENGINE_GRAPHICS_RENDERING_VULKAN_VULKANDEVICE

#include "Aeon/Framework/Framework.hpp"

#include "Aeon/Engine/Graphics/Rendering/Vulkan/VulkanCommon.hpp"
#include "Aeon/Engine/Graphics/Rendering/Vulkan/VulkanDeviceFeatureSet.hpp"
#include "Aeon/Engine/Graphics/Rendering/Vulkan/VulkanDeviceQueueConfig.hpp"
#include "Aeon/Engine/Graphics/Rendering/Vulkan/VulkanSurface.hpp"
#include "Aeon/Engine/Graphics/Rendering/Vulkan/Shader/VulkanShader.hpp"

#include "Aeon/Engine/System/Display/Window.hpp"

namespace Aeon
{
	class SVulkanRenderer;

	// NOTE See https://github.com/KhronosGroup/Vulkan-Docs/issues/569
	//  for notes about queue families and limitations of the abstraction,
	//  as well as notes about expected performance of using multiple queues for different capabilities

	AEON_DECLARE_FLAG_ENUM(EVulkanDevice, uint32)
	(
		AEON_FLAG_ENUM_RULE_AS_IS,
		Other = 0x00,
		CPU = VK_PHYSICAL_DEVICE_TYPE_CPU,
		iGPU = VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU,
		dGPU = VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU,
		vGPU = VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU,
	);

	class SVulkanDevice : AEON_EVENT_HANDLER
	{
	//	AEON_GENERATE_EVENT_HANDLER_OVERRIDES();

	public:
		AEON_DECLARE_ACCESS_CLASS(ConstructorAccess, class SVulkanRenderer);

		explicit SVulkanDevice(ConstructorAccess, SVulkanRenderer* NewOwningRenderer, VkPhysicalDevice NewDeviceHandle);

		bool Initialize();

		void Terminate();

		bool SetupLogicalDevice(SVulkanDeviceFeatureSet const& RequiredFeatures,
								SVulkanDeviceQueueConfig const& RequiredQueueConfig);

		bool SetupSurfaces();

		void RegisterShaders(GArray<GSharedPtr<SVulkanShader>> ShaderList); // TODO

		GOptional<SVulkanDeviceQueueConfig> MakeQueueConfig(SVulkanDeviceQueueRequirementList const& RequirementList) const;

		VkPhysicalDevice GetPhysicalDeviceHandle() const;
		VkDevice GetLogicalDeviceHandle() const;

		SStringView GetName() const;
		VkPhysicalDeviceType GetType() const; // TODO maybe replace with EVulkanDevice?

		SString GetAPIVersion() const;
		SString GetDriverVersion() const;

		// TODO expose remaining properties
		VkPhysicalDeviceLimits const& GetProperties() const;
		VkPhysicalDeviceSparseProperties const& GetSparseResourceProperties() const;

		SVulkanDeviceFeatureSet const& GetFeatures() const;
		SVulkanDeviceFeatureSet const& GetEnabledFeatures() const;

		VkQueueFamilyProperties const& GetQueueFamilyProperties(uint32 Index) const;
		uint32 GetQueueFamilyCount() const;

		bool DoesQueueFamilySupportPresentation(uint32 QueueFamilyIndex) const;

		VkQueue GetQueue(EVulkanDeviceQueueCapability QueueCapability, uint32 QueueIndex) const;
		uint32 GetQueueCount(EVulkanDeviceQueueCapability QueueCapability) const;

		SVulkanDeviceQueueConfig const& GetQueueConfig() const;

		SVulkanSurface& GetSurface(HWindow WindowHandle);

	private:
		void CreateSurfaceForWindow_Internal(IWindow& Window);
		void DestroySurfaceForWindow_Internal(IWindow& Window);

		SVulkanRenderer* OwningRenderer;
		VkPhysicalDevice PhysicalDeviceHandle;
		VkDevice LogicalDeviceHandle;

		// TODO maybe abstract this with specific class (e.g. SVulkanDeviceProperties)
		VkPhysicalDeviceProperties2 DeviceProperties;
		VkPhysicalDeviceVulkan11Properties DevicePropertiesEx_1_1;
		VkPhysicalDeviceVulkan12Properties DevicePropertiesEx_1_2;
		VkPhysicalDeviceVulkan13Properties DevicePropertiesEx_1_3;

		SVulkanDeviceFeatureSet DeviceFeatures;
		SVulkanDeviceFeatureSet EnabledFeatures;

		GHashSet<VkExtensionProperties> EnabledExtensionsSet;
		GArray<VkQueueFamilyProperties> QueueFamilyPropertiesArray;

		GHashMap<EVulkanDeviceQueueCapability, GArray<VkQueue>> CachedQueueMap;
		SVulkanDeviceQueueConfig CachedQueueConfig;

		GHashMap<HWindow, GUniquePtr<SVulkanSurface>> WindowSurfaceMap;

	};
}

#endif