#include "Aeon/Engine/Graphics/Rendering/Vulkan/VulkanDevice.hpp"

#include "Aeon/Engine/Graphics/Rendering/Vulkan/VulkanRenderer.hpp"

#include "Aeon/Engine/Logging/Log.hpp"

#if AEON_USING_GLFW_LIBRARY
//#	if AEON_PLATFORM_WINDOWS
//#		define GLFW_EXPOSE_NATIVE_WIN32
//#		include <vulkan/vulkan_win32.h>
//#	else
//#		error Not implemented yet!
//#	endif
//#	define GLFW_NATIVE_INCLUDE_NONE
#	include <GLFW/glfw3.h>
//#	include <GLFW/glfw3native.h>
#endif

#include "Aeon/Engine/System/Display/WindowManager.hpp"

AEON_DECLARE_LOG_FAMILY(VulkanDevice, ETerminalColor::Firebrick,
						ELogSourceInfo::FileName | ELogSourceInfo::FuncName | ELogSourceInfo::LineNum,
						ELogDateTimeInfo::None);

namespace
{
	bool TryAddExtension(Aeon::GArray<VkExtensionProperties> const& AvailableExtensions,
						 Aeon::GHashSet<VkExtensionProperties>& EnabledExtensions,
						 char const* ExtensionName)
	{
		auto Extension = AvailableExtensions.Find(
			[ExtensionName](VkExtensionProperties const& Element)
			{
				return Aeon::StringOps::Compare(Element.extensionName, ExtensionName) == 0;
			}
		);

		if (Extension)
		{
			EnabledExtensions.Add(*Extension);
		}

		return bool(Extension);
	}
}

namespace Aeon
{
	SVulkanDevice::SVulkanDevice(ConstructorAccess, SVulkanRenderer* NewOwningRenderer, VkPhysicalDevice NewDeviceHandle)
		: OwningRenderer{ NewOwningRenderer }
		, PhysicalDeviceHandle{ NewDeviceHandle }
		, LogicalDeviceHandle{ VK_NULL_HANDLE }
		, DeviceProperties{}
		, DevicePropertiesEx_1_1{}
		, DevicePropertiesEx_1_2{}
		, DevicePropertiesEx_1_3{}
	{
	}

	bool SVulkanDevice::Initialize()
	{
		DeviceProperties.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2;
		DeviceProperties.pNext = addressof(DevicePropertiesEx_1_1);
		DevicePropertiesEx_1_1.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_1_PROPERTIES;
		DevicePropertiesEx_1_1.pNext = addressof(DevicePropertiesEx_1_2);
		DevicePropertiesEx_1_2.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_PROPERTIES;
		DevicePropertiesEx_1_2.pNext = addressof(DevicePropertiesEx_1_3);
		DevicePropertiesEx_1_3.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_3_PROPERTIES;
		DevicePropertiesEx_1_3.pNext = nullptr;

		vkGetPhysicalDeviceProperties2(PhysicalDeviceHandle, &DeviceProperties);
		vkGetPhysicalDeviceFeatures2(PhysicalDeviceHandle, DeviceFeatures.VulkanData());

		QueueFamilyPropertiesArray = VulkanQueryArrayData<vkGetPhysicalDeviceQueueFamilyProperties>(PhysicalDeviceHandle);

		return true;
	}

	void SVulkanDevice::Terminate()
	{
		IWindowManager& WindowManager = IWindowManager::Instance();

		// TODO we could avoid this if SetupSurfaces() was not called
		WindowManager.OnPostWindowCreate.Unbind(this);
		WindowManager.OnPreWindowDestroy.Unbind(this);

		for (auto& Surface : WindowSurfaceMap)
		{
			Surface.Value()->Terminate();
		}

		WindowSurfaceMap.Empty(TReclaimStorage::Tag);

		if (LogicalDeviceHandle != VK_NULL_HANDLE)
		{
			vkDestroyDevice(LogicalDeviceHandle, nullptr);
		}

		LogicalDeviceHandle = VK_NULL_HANDLE;
	}

	bool SVulkanDevice::SetupLogicalDevice(SVulkanDeviceFeatureSet const& RequiredFeatures,
										   SVulkanDeviceQueueConfig const& RequiredQueueConfig)
	{
		AEON_ASSERT(LogicalDeviceHandle == VK_NULL_HANDLE);

		AEON_FAILSAFE(DeviceFeatures.IsSupersetOf(RequiredFeatures), return false);

		EnabledFeatures = RequiredFeatures;
		CachedQueueConfig = RequiredQueueConfig;

		VkDeviceCreateInfo CreateInfo{};
		CreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;

		GArray<VkDeviceQueueCreateInfo> QueueCreateInfoArray;

		uint32 MaxQueueCount =
			SVulkanDeviceQueueConfig::SupportedCapabilityList()
				.Map([this] (EVulkanDeviceQueueCapability Capability) -> uint32
					 { return CachedQueueConfig.GetCapabilityConfig(Capability).QueueCount; })
				.Fold([] (uint32 MaxQueueCount, uint32 QueueCount)
					  { return AeonMath::Max(MaxQueueCount, QueueCount); });

		// TODO at some point, we might want to give higher priority to e.g. Graphics, so we'll have to move the priorities to config

		// Currently, we simply make every queue have the same priority.
		// NOTE This array only needs to live until the vkCreateDevice call
		GArray<float32> QueuePriorityArray{ TCreateElements::Tag, MaxQueueCount, 1.0f };

		bool RequiresPresentationCapability = false;

		for (auto Capability : SVulkanDeviceQueueConfig::SupportedCapabilityList())
		{
			auto const& QueueConfig = CachedQueueConfig.GetCapabilityConfig(Capability);

			// Only update the CreateInfo array for capabilities that we are using
			if (QueueConfig.QueueCount)
			{
				uint32 QueueFamilyIndex = QueueConfig.QueueFamilyIndex;

				// If we already have a CreateInfo for this queue family (from some other capability), just update the queue count
				if (auto QueueCreateInfoOpt = QueueCreateInfoArray.Find([QueueFamilyIndex](VkDeviceQueueCreateInfo const& Info)
																		{ return Info.queueFamilyIndex == QueueFamilyIndex; }))
				{
					QueueCreateInfoOpt->queueCount += QueueConfig.QueueCount;
				}
				else
				{
					VkDeviceQueueCreateInfo& QueueCreateInfo = QueueCreateInfoArray.Add(VkDeviceQueueCreateInfo{});
					QueueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;

					QueueCreateInfo.pQueuePriorities = QueuePriorityArray.Buffer();

					QueueCreateInfo.queueFamilyIndex = QueueConfig.QueueFamilyIndex;
					QueueCreateInfo.queueCount = QueueConfig.QueueCount;
				}

				RequiresPresentationCapability = RequiresPresentationCapability || QueueConfig.SupportsPresentation;
			}
		}

		// See: https://registry.khronos.org/vulkan/specs/1.3-extensions/html/vkspec.html#VUID-VkDeviceCreateInfo-pNext-00373
		CreateInfo.pNext = EnabledFeatures.VulkanData();
		CreateInfo.pEnabledFeatures = nullptr;
	//	CreateInfo.pEnabledFeatures = &EnabledFeatures.VulkanData()->features;

		// TODO add device extensions using config file
		GArray<VkExtensionProperties> AvailableExtensionsArray
			= VulkanQueryArrayData<vkEnumerateDeviceExtensionProperties>(PhysicalDeviceHandle, nullptr);

		AEON_LOG(Info, VulkanDevice, "Available device extensions ({}):", AvailableExtensionsArray.Count());
		for (auto const& Extension : AvailableExtensionsArray)
		{
			AEON_LOG(Info, VulkanDevice, "  - {}", Extension.extensionName);
		}

		if (RequiresPresentationCapability)
		{
			constexpr char const* SwapchainExtensionName = VK_KHR_SWAPCHAIN_EXTENSION_NAME;

			bool HasExtension = TryAddExtension(AvailableExtensionsArray, EnabledExtensionsSet, SwapchainExtensionName);
			if (!HasExtension)
			{
				AEON_LOG(Error, VulkanDevice, "Required device extension '{}' is unavailable!", SwapchainExtensionName);
				return false;
			}
		}

		GArray<char const*> EnabledExtensionsArray{ TReserveStorage::Tag, EnabledExtensionsSet.Count() };

		AEON_LOG(Info, VulkanDevice, "Enabled extensions ({}):", EnabledExtensionsSet.Count());
		for (auto const& Extension : EnabledExtensionsSet)
		{
			auto ExtensionName = Extension.extensionName;

			AEON_LOG(Info, VulkanDevice, "  - {}", ExtensionName);

			EnabledExtensionsArray.Add(ExtensionName);
		}

		CreateInfo.enabledExtensionCount = EnabledExtensionsArray.Count();
		CreateInfo.ppEnabledExtensionNames = EnabledExtensionsArray.Buffer();

		auto const& EnabledLayersSet = OwningRenderer->GetEnabledLayers();

		GArray<char const*> EnabledLayersArray{ TReserveStorage::Tag, EnabledLayersSet.Count() };
		for (auto const& Layer : EnabledLayersSet)
		{
			EnabledLayersArray.Add(Layer.layerName);
		}

		CreateInfo.enabledLayerCount = EnabledLayersArray.Count();
		CreateInfo.ppEnabledLayerNames = EnabledLayersArray.Buffer();

		CreateInfo.pQueueCreateInfos = QueueCreateInfoArray.Buffer();
		CreateInfo.queueCreateInfoCount = QueueCreateInfoArray.Count();

		// TODO pass allocator from renderer
		if (VkResult Result = vkCreateDevice(PhysicalDeviceHandle, &CreateInfo, nullptr, &LogicalDeviceHandle))
		{
			AEON_LOG(Error, VulkanDevice, "Failed to create logical device for device '{}'. Error code: {}",
					 GetName(), int(Result));
			return false;
		}

		// CHECKME Something to note, we catch a similar message while initializing the device:
		//  https://github.com/KhronosGroup/Vulkan-ValidationLayers/issues/8048

		for (auto Capability : SVulkanDeviceQueueConfig::SupportedCapabilityList())
		{
			DVulkanDeviceQueueCapabilityConfig const& QueueConfig = CachedQueueConfig.GetCapabilityConfig(Capability);

			auto& [AEON_IGNORED, QueueArray] = CachedQueueMap.Add(Capability, GArray<VkQueue>{ TCreateElements::Tag, QueueConfig.QueueCount });

			for (uint32 QueueIndex = 0; QueueIndex < QueueConfig.QueueCount; ++QueueIndex)
			{
				auto& QueueHandle = QueueArray[QueueIndex];

				vkGetDeviceQueue(LogicalDeviceHandle, QueueConfig.QueueFamilyIndex, QueueIndex, &QueueHandle);
			}
		}

		return true;
	}

	bool SVulkanDevice::SetupSurfaces()
	{
		// NOTE This function is kept separate from SetupLogicalDevice because we might want headless rendering at some point,
		//  which precludes the need for surfaces

		AEON_ASSERT(LogicalDeviceHandle != VK_NULL_HANDLE);

		auto PresentationQueueIndexesArray = CachedQueueConfig.QueryPresentationQueueIndexes();

		bool SupportsPresentation = !PresentationQueueIndexesArray.IsEmpty();
		if (!SupportsPresentation)
		{
			AEON_LOG(Error, VulkanDevice, "Cannot create surfaces for device '{}', since no configured queue supports presentation.", GetName());
			return false;
		}

		IWindowManager& WindowManager = IWindowManager::Instance();

		WindowManager.OnPostWindowCreate.Bind(this, &SVulkanDevice::CreateSurfaceForWindow_Internal);
		WindowManager.OnPreWindowDestroy.Bind(this, &SVulkanDevice::DestroySurfaceForWindow_Internal);

		for (uint32 Index = 0; Index < WindowManager.GetWindowCount(); ++Index)
		{
			IWindow& Window = WindowManager.GetWindow(Index);

			CreateSurfaceForWindow_Internal(Window);
		}

		return true;
	}

	GOptional<SVulkanDeviceQueueConfig> SVulkanDevice::MakeQueueConfig(SVulkanDeviceQueueRequirementList const& RequirementList) const
	{
		// See: https://stackoverflow.com/questions/37575012/should-i-try-to-use-as-many-queues-as-possible

		GArray<DVulkanDeviceQueueFamilyData> QueueFamilyArray;

		AEON_LOG(Info, VulkanDevice, "Device '{}' queue family list:", GetName());

		for (uint32 QueueFamilyIndex = 0; QueueFamilyIndex < QueueFamilyPropertiesArray.Count(); ++QueueFamilyIndex)
		{
			VkQueueFamilyProperties const& QueueFamily = QueueFamilyPropertiesArray[QueueFamilyIndex];

			EVulkanDeviceQueueCapability QueueFamilyCapability = EVulkanDeviceQueueCapability(static_cast<int>(QueueFamily.queueFlags));
			uint32 QueueCount = QueueFamily.queueCount;

			bool SupportsTransfer = QueueFamilyCapability & EVulkanDeviceQueueCapability::Transfer;
			bool SupportsSparseBinding = QueueFamilyCapability & EVulkanDeviceQueueCapability::SparseBinding;
			bool SupportsProtectedAccess = QueueFamilyCapability & EVulkanDeviceQueueCapability::ProtectedAccess;
			bool SupportsPresentation = DoesQueueFamilySupportPresentation(QueueFamilyIndex);

			SString QueueFamilyCapabilityList;
			uint32 QueueFamilyCapabilityCount = 0;

#		define ZZINTERNAL_AEON_CHECK_HAS_CAPABILITY(Cap) \
            if (Aeon::HasFlag(QueueFamilyCapability, EVulkanDeviceQueueCapability:: Cap))\
            {\
                QueueFamilyCapabilityList.Concat(QueueFamilyCapabilityCount ? ", " : "")\
                                         .Concat(AEON_TOKEN_STR(Cap));\
                ++QueueFamilyCapabilityCount;\
            }

			ZZINTERNAL_AEON_CHECK_HAS_CAPABILITY(Graphics)
			ZZINTERNAL_AEON_CHECK_HAS_CAPABILITY(Compute)
			ZZINTERNAL_AEON_CHECK_HAS_CAPABILITY(Decode)
			ZZINTERNAL_AEON_CHECK_HAS_CAPABILITY(Encode)
			ZZINTERNAL_AEON_CHECK_HAS_CAPABILITY(Transfer)
			ZZINTERNAL_AEON_CHECK_HAS_CAPABILITY(SparseBinding)
			ZZINTERNAL_AEON_CHECK_HAS_CAPABILITY(ProtectedAccess)

			if (SupportsPresentation)
			{
				QueueFamilyCapabilityList.Concat(QueueFamilyCapabilityCount ? ", " : "")
                                         .Concat("Presentation");
				++QueueFamilyCapabilityCount;
			}

			if (QueueFamilyCapabilityCount == 0)
			{
				QueueFamilyCapabilityList = "Unknown";
			}

#		undef ZZINTERNAL_AEON_CHECK_HAS_CAPABILITY

			AEON_LOG(Info, VulkanDevice, " - Family {}: Capabilities = {}; QueueCount = {};",
					 QueueFamilyIndex, QueueFamilyCapabilityList, QueueCount);

			QueueFamilyArray.Add(DVulkanDeviceQueueFamilyData{ .Capability = QueueFamilyCapability,
															   .QueueCount = QueueCount,
															   .SupportsTransfer = SupportsTransfer,
															   .SupportsSparseBinding = SupportsSparseBinding,
															   .SupportsProtectedAccess = SupportsProtectedAccess,
															   .SupportsPresentation = SupportsPresentation, });
		}

		SVulkanDeviceQueueConfig QueueConfig;

		for (auto const& Requirement : RequirementList.QueryRequirements())
		{
			DVulkanDeviceQueueCapabilityConfig CapabilityConfig;

			uint32 SelectedQueueCount = 0;
			uint32 SelectedCapabilityCount = 8; // Capability count will never be greater than 7

			for (uint32 QueueFamilyIndex = 0; QueueFamilyIndex < QueueFamilyArray.Count(); ++QueueFamilyIndex)
			{
				DVulkanDeviceQueueFamilyData& QueueFamilyData = QueueFamilyArray[QueueFamilyIndex];

				// Exclude SparseBinding and Protected, as these refer to memory access capabilities and not operations.
				uint32 QueueFamilyCapabilityCount = AeonMath::Popcnt<uint32>(QueueFamilyData.Capability /* & VulkanOperationCapabilityMask*/);

				// NOTE If the config already has a queue family for a required capability,
				//  overwrite it if the current family has less capabilities (is more specialized for specific capabilities),
				//  or if it has otherwise more queues than the old one (can dedicate more queues to specific capabilities)

				if (Aeon::HasFlag(QueueFamilyData.Capability, Requirement.Capability)
					&& QueueFamilyData.QueueCount >= Requirement.QueueCount
					&& (!Requirement.SupportsTransfer || QueueFamilyData.SupportsTransfer)
					&& (!Requirement.SupportsSparseBinding || QueueFamilyData.SupportsSparseBinding)
					&& (!Requirement.SupportsProtectedAccess || QueueFamilyData.SupportsProtectedAccess)
					&& (!Requirement.SupportsPresentation || QueueFamilyData.SupportsPresentation)
					&&	// Use this family if it has less capabilities than the selected one (more restricted/specialized)
						(QueueFamilyCapabilityCount < SelectedCapabilityCount
						// Otherwise, use this family if it has more queues than the selected one
						|| QueueFamilyData.QueueCount > SelectedQueueCount))
				{
					CapabilityConfig.QueueFamilyIndex = QueueFamilyIndex;
					CapabilityConfig.QueueCount = Requirement.QueueCount;

					CapabilityConfig.SupportsTransfer = QueueFamilyData.SupportsTransfer;
					CapabilityConfig.SupportsSparseBinding = QueueFamilyData.SupportsSparseBinding;
					CapabilityConfig.SupportsProtectedAccess = QueueFamilyData.SupportsProtectedAccess;
					CapabilityConfig.SupportsPresentation = QueueFamilyData.SupportsPresentation;

					SelectedQueueCount = QueueFamilyData.QueueCount;
					SelectedCapabilityCount = QueueFamilyCapabilityCount;
				}
			}

			if (SelectedQueueCount > 0)
			{
				DVulkanDeviceQueueFamilyData& QueueFamilyData = QueueFamilyArray[CapabilityConfig.QueueFamilyIndex];
				QueueFamilyData.QueueCount -= CapabilityConfig.QueueCount;
			}
			else
			{
				AEON_LOG(Error, VulkanDevice, "Could not find a queue family configuration that fulfills the requirements for capability '{}'.",
						 MakeCapabilityEnumString(Requirement.Capability));
				return GOptional<SVulkanDeviceQueueConfig>{};
			}

			QueueConfig.SetQueueConfig(Requirement.Capability, CapabilityConfig);
		}

		return GOptional<SVulkanDeviceQueueConfig>{ TConstructInPlace::Tag, QueueConfig };
	}

	VkPhysicalDevice SVulkanDevice::GetPhysicalDeviceHandle() const
	{
		return PhysicalDeviceHandle;
	}
	VkDevice SVulkanDevice::GetLogicalDeviceHandle() const
	{
		return LogicalDeviceHandle;
	}

	SStringView SVulkanDevice::GetName() const
	{
		return DeviceProperties.properties.deviceName;
	}
	VkPhysicalDeviceType SVulkanDevice::GetType() const
	{
		return DeviceProperties.properties.deviceType;
	}

	SString SVulkanDevice::GetAPIVersion() const
	{
		return Aeon::SStringView(); // TODO implement this
	}
	SString SVulkanDevice::GetDriverVersion() const
	{
		return Aeon::SStringView(); // TODO implement this
	}

	VkPhysicalDeviceLimits const& SVulkanDevice::GetProperties() const
	{
		return DeviceProperties.properties.limits;
	}
	VkPhysicalDeviceSparseProperties const& SVulkanDevice::GetSparseResourceProperties() const
	{
		// See: https://registry.khronos.org/vulkan/specs/1.3-extensions/html/vkspec.html#sparsememory
		return DeviceProperties.properties.sparseProperties;
	}

	SVulkanDeviceFeatureSet const& SVulkanDevice::GetFeatures() const
	{
		return DeviceFeatures;
	}

	SVulkanDeviceFeatureSet const& SVulkanDevice::GetEnabledFeatures() const
	{
		return EnabledFeatures;
	}

	VkQueueFamilyProperties const& SVulkanDevice::GetQueueFamilyProperties(uint32 Index) const
	{
		return QueueFamilyPropertiesArray[Index];
	}
	uint32 SVulkanDevice::GetQueueFamilyCount() const
	{
		return QueueFamilyPropertiesArray.Count();
	}

	bool SVulkanDevice::DoesQueueFamilySupportPresentation(uint32 QueueFamilyIndex) const
	{
		// NOTE From the Vulkan docs:
		// > It should be noted that the availability of a presentation queue (...) implies that the swapchain extension must be supported.
		// > However, (...) the extension does have to be explicitly enabled.
#	if AEON_USING_GLFW_LIBRARY
		auto InstanceHandle = OwningRenderer->GetHandle();
		return glfwGetPhysicalDevicePresentationSupport(InstanceHandle, PhysicalDeviceHandle, QueueFamilyIndex);
#	else
#		error Not implemented yet!
		return false;
#	endif
	}

	VkQueue SVulkanDevice::GetQueue(EVulkanDeviceQueueCapability QueueCapability, uint32 QueueIndex) const
	{
		return CachedQueueMap[QueueCapability][QueueIndex];
	}
	uint32 SVulkanDevice::GetQueueCount(EVulkanDeviceQueueCapability QueueCapability) const
	{
		return CachedQueueMap[QueueCapability].Count();
	}

	SVulkanDeviceQueueConfig const& SVulkanDevice::GetQueueConfig() const
	{
		return CachedQueueConfig;
	}

	SVulkanSurface& SVulkanDevice::GetSurface(HWindow WindowHandle)
	{
		return *WindowSurfaceMap[WindowHandle];
	}

	void SVulkanDevice::CreateSurfaceForWindow_Internal(IWindow& Window)
	{
		auto SurfacePtr = MakeUniquePtr<SVulkanSurface>(SVulkanSurface::ConstructorAccess::Tag, OwningRenderer, this, Window);

		if (SurfacePtr->Initialize())
		{
			HWindow Handle = Window.GetHandle();

			WindowSurfaceMap.Add(Handle, Move(SurfacePtr));
		}
	}

	void SVulkanDevice::DestroySurfaceForWindow_Internal(IWindow& Window)
	{
		HWindow Handle = Window.GetHandle();

		auto& SurfacePtr = WindowSurfaceMap[Handle];

		SurfacePtr->Terminate();

		WindowSurfaceMap.Remove(Handle);
	}
}
