#ifndef ZZINTERNAL_INCLUDE_AEON_ENGINE_GRAPHICS_RENDERING_VULKAN_VULKANSURFACE
#define ZZINTERNAL_INCLUDE_AEON_ENGINE_GRAPHICS_RENDERING_VULKAN_VULKANSURFACE

#include "Aeon/Framework/Framework.hpp"

#include "Aeon/Engine/Graphics/Rendering/Vulkan/VulkanCommon.hpp"
#include "Aeon/Engine/Graphics/Rendering/Vulkan/VulkanSurfaceSwapchainConfig.hpp"

#include "Aeon/Engine/System/Display/Window.hpp"

namespace Aeon
{
	class SVulkanDevice;
	class SVulkanRenderer;

	extern SStringView MakeFormatString(VkFormat Format);
	extern SStringView MakeColorspaceString(VkColorSpaceKHR ColorSpace);

	class SVulkanSurface : AEON_EVENT_HANDLER
	{
	//	AEON_GENERATE_EVENT_HANDLER_OVERRIDES();

	public:
		AEON_DECLARE_ACCESS_CLASS(ConstructorAccess, class SVulkanDevice);

		explicit SVulkanSurface(ConstructorAccess, SVulkanRenderer* NewOwningRenderer, SVulkanDevice* NewOwningDevice, IWindow& NewOwningWindow);

		bool Initialize();

		void Terminate();

		bool SetupSwapchain(SVulkanSurfaceSwapchainConfig const& RequiredConfig);

		void RefreshSurfaceInfo();

		VkSurfaceKHR GetHandle() const;

		SStringView GetName() const;

		VkPresentModeKHR const& GetSupportedPresentationMode(uint32 Index) const;
		uint32 GetSupportedPresentationModeCount() const;

		VkSurfaceFormatKHR const& GetSupportedFormat(uint32 Index) const;
		uint32 GetSupportedFormatCount() const;

		VkSurfaceCapabilitiesKHR const& GetSupportedCharacteristics() const;

	private:
		bool IsConfigSupported_Internal(SVulkanSurfaceSwapchainConfig const& Config) const;

		bool RecreateSwapchain_Internal();

		void HandleFramebufferChanged_Internal(EFramebufferChange Change, SIntVector2 Size, SIntVector2 Position, SEulerRotation Rotation);

		SVulkanRenderer* OwningRenderer;
		SVulkanDevice* OwningDevice;
		IWindow* OwningWindow;
		VkSurfaceKHR SurfaceHandle;
		VkSwapchainKHR SwapchainHandle;

		GArray<VkPresentModeKHR> CachedPresentModeArray;

		GArray<VkSurfaceFormatKHR> CachedFormatArray;
		VkSurfaceCapabilitiesKHR CachedCharacteristics;

		GArray<VkImage> CachedSwapchainImageArray;
		GArray<VkImageView> CachedSwapchainImageViewArray;
		GStaticArray<uint32, 5> CachedSwapchainSharingQueueIndexesArray;

		VkSwapchainCreateInfoKHR CachedSwapchainInfo;
		VkImageViewCreateInfo CachedSwapchainImageViewInfo;

	//	SVulkanSurfaceSwapchainConfig CachedSwapchainConfig;

	};
}

#endif