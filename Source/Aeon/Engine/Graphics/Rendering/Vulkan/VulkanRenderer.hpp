#ifndef ZZINTERNAL_INCLUDE_AEON_ENGINE_GRAPHICS_RENDERING_VULKAN_VULKANRENDERER
#define ZZINTERNAL_INCLUDE_AEON_ENGINE_GRAPHICS_RENDERING_VULKAN_VULKANRENDERER

#include "Aeon/Framework/Framework.hpp"

#include "Aeon/Engine/Graphics/Rendering/Vulkan/VulkanCommon.hpp"
#include "Aeon/Engine/Graphics/Rendering/Vulkan/VulkanDeviceFeatureSet.hpp"
#include "Aeon/Engine/Graphics/Rendering/Vulkan/VulkanDevice.hpp"

#include "Aeon/Engine/Graphics/Rendering/Vulkan/Shader/VulkanShaderManager.hpp"

namespace Aeon
{
	class SVulkanRenderer
	{
		explicit SVulkanRenderer();

	public:
		static SVulkanRenderer& Instance(); // CHECKME Consider making this/renderer a non-singleton

		bool Initialize();

		void Terminate();

		VkInstance GetHandle();

		void DetectDevices();

		bool SelectDevice(SVulkanDeviceFeatureSet const& RequiredFeatures/*, SVulkanDeviceScoringFactorList const& ScoringFactors*/);

		bool HasSelectedDevice() const;

		SVulkanDevice& GetSelectedDevice();

		SVulkanDevice& GetDevice(uint32 Index);

		uint32 GetDeviceCount() const;

		SVulkanShaderManager& GetShaderManager();

		GHashSet<VkExtensionProperties> const& GetEnabledExtensions() const;
		GHashSet<VkLayerProperties> const& GetEnabledLayers() const;

	private:
		static VKAPI_ATTR VkBool32 VKAPI_CALL DebugMessenger_Internal(VkDebugUtilsMessageSeverityFlagBitsEXT MessageSeverity,
																	  VkDebugUtilsMessageTypeFlagsEXT MessageType,
																	  VkDebugUtilsMessengerCallbackDataEXT const* CallbackData,
																	  void* UserData);

		VkInstance InstanceHandle;
		VkDebugUtilsMessengerEXT DebugMessengerHandle;

		GHashSet<VkExtensionProperties> EnabledExtensionsSet;
		GHashSet<VkLayerProperties> EnabledLayersSet;

		GArray<GUniquePtr<SVulkanDevice>> DeviceArray;
		uint32 SelectedDeviceIndex;

		GStorage<SVulkanShaderManager> ShaderManager;

		// TODO use additional macro (or engine config) to control this
		static constexpr bool ShouldUseValidationLayers = bool(AEON_BUILD_DEVELOPMENT);

	};
}

#endif