#ifndef ZZINTERNAL_INCLUDE_AEON_ENGINE_GRAPHICS_RENDERING_VULKAN_VULKANSURFACESWAPCHAINCONFIG
#define ZZINTERNAL_INCLUDE_AEON_ENGINE_GRAPHICS_RENDERING_VULKAN_VULKANSURFACESWAPCHAINCONFIG

#include "Aeon/Framework/Framework.hpp"

#include "Aeon/Engine/Graphics/Rendering/Vulkan/VulkanCommon.hpp"

namespace Aeon
{
	class SVulkanSurface;

	class SVulkanSurfaceSwapchainConfig
	{
	public:
		explicit SVulkanSurfaceSwapchainConfig() = default;

		VkFormat GetSurfaceFormat() const;
		VkColorSpaceKHR GetSurfaceColorSpace() const;
		VkPresentModeKHR GetPresentationMode() const;
		uint32 GetImageCount() const;

		void SetSurfaceFormat(VkFormat Format);
		void SetSurfaceColorSpace(VkColorSpaceKHR ColorSpace);
		void SetPresentationMode(VkPresentModeKHR Mode);
		void SetImageCount(uint32 Count);

	private:
		VkFormat SurfaceFormat;
		VkColorSpaceKHR SurfaceColorSpace;
		VkPresentModeKHR PresentationMode;
		uint32 ImageCount;

	};
}

#endif