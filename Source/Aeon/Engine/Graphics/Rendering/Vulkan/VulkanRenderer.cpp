#include "Aeon/Engine/Graphics/Rendering/Vulkan/VulkanRenderer.hpp"

#include "Aeon/Engine/Logging/Log.hpp"
#include "Aeon/Engine/Logging/Terminal/TerminalTextStyler.hpp"

#include "Aeon/Engine/System/Filesystem/EnginePaths.hpp"

#if AEON_USING_GLFW_LIBRARY
//#	if AEON_PLATFORM_WINDOWS
//#		define GLFW_EXPOSE_NATIVE_WIN32
//#		include <vulkan/vulkan_win32.h>
//#	else
//#		error Not implemented yet!
//#	endif
//#	define GLFW_NATIVE_INCLUDE_NONE
#	include <GLFW/glfw3.h>
//#	include <GLFW/glfw3native.h>
#endif

#define ZZINTERNAL_AEON_VULKAN_LOG_EXTRA_INFO 1

AEON_DECLARE_LOG_FAMILY(VulkanRenderer, ETerminalColor::Firebrick,
						ELogSourceInfo::FileName | ELogSourceInfo::FuncName | ELogSourceInfo::LineNum,
						ELogDateTimeInfo::None);

// TODO maybe generate strings for vk enumeration values

namespace // TODO maybe move these methods elsewhere
{
	bool TryAddExtension(Aeon::GArray<VkExtensionProperties> const& AvailableExtensions,
						 Aeon::GHashSet<VkExtensionProperties>& EnabledExtensions,
						 char const* ExtensionName)
	{
		auto Extension = AvailableExtensions.Find(
			[ExtensionName](VkExtensionProperties const& Element)
			{
				return Aeon::StringOps::Compare(Element.extensionName, ExtensionName) == 0;
			}
		);

		if (Extension)
		{
			EnabledExtensions.Add(*Extension);
		}

		return bool(Extension);
	}

	[[maybe_unused]]
	bool TryAddLayer(Aeon::GArray<VkLayerProperties> const& AvailableLayers,
					 Aeon::GHashSet<VkLayerProperties>& EnabledLayers,
					 char const* LayerName)
	{
		auto Layer = AvailableLayers.Find(
			[LayerName](VkLayerProperties const& Element)
			{
				return Aeon::StringOps::Compare(Element.layerName, LayerName) == 0;
			}
		);

		if (Layer)
		{
			EnabledLayers.Add(*Layer);
		}

		return bool(Layer);
	}

	[[maybe_unused]]
	Aeon::SStringView MakeDeviceTypeString(VkPhysicalDeviceType DeviceType)
	{
		switch (DeviceType)
		{
			case VK_PHYSICAL_DEVICE_TYPE_CPU:
				return "CPU";
			case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU:
				return "iGPU";
			case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU:
				return "dGPU";
			case VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU:
				return "vGPU";
			case VK_PHYSICAL_DEVICE_TYPE_OTHER:
				return "Other";
			default:
				return "Unknown";
		};
	}
}

namespace Aeon
{
	SVulkanRenderer& SVulkanRenderer::Instance()
	{
		static SVulkanRenderer Renderer;
		return Renderer;
	}

	SVulkanRenderer::SVulkanRenderer()
		: InstanceHandle{ VK_NULL_HANDLE }
		, DebugMessengerHandle{ VK_NULL_HANDLE }
		, DeviceArray{ TReserveStorage::Tag, 0 }
		, SelectedDeviceIndex{ ~0_u32 }
	{
	}

	bool SVulkanRenderer::Initialize()
	{
		AEON_ASSERT(SThread::ThisId() == MainThread.Id());

		uint32 APIVersion;

		AEON_EVAL_ASSERT(vkEnumerateInstanceVersion(&APIVersion) == VK_SUCCESS);

		uint8 MajorVersion = VK_API_VERSION_MAJOR(APIVersion);
		uint16 MinorVersion = VK_API_VERSION_MINOR(APIVersion);
		uint16 PatchVersion = VK_API_VERSION_PATCH(APIVersion);
		uint8 VariantVersion = VK_API_VERSION_VARIANT(APIVersion);

		// From: https://vulkan.lunarg.com/doc/view/1.3.261.1/windows/1.3-extensions/vkspec.html §45.2.1
		// > The variant indicates the variant of the Vulkan API supported by the implementation.
		// > This is always 0 for the Vulkan API.
		//
		//   > A non-zero variant indicates the API is a variant of the Vulkan API and
		//   > applications will typically need to be modified to run against it. The variant field
		//   > was a later addition to the version number, added in version 1.2.175 of the
		//   > Specification. As Vulkan uses variant 0, this change is fully backwards compatible
		//   > with the previous version number format for Vulkan implementations. New
		//   > version number macros have been added for this change and the old macros
		//   > deprecated. For existing applications using the older format and macros, an
		//   > implementation with non-zero variant will decode as a very high Vulkan version.
		//   > The high version number should be detectable by applications performing suitable
		//   > version checking.
		AEON_FAILSAFE(VariantVersion == 0, return false, "Unsupported Vulkan API implementation!");

		AEON_LOG(Info, VulkanRenderer, "Vulkan API version: {}.{}.{}", MajorVersion, MinorVersion, PatchVersion);

		// Initialize ShaderManager class

		ShaderManager.Construct(SVulkanShaderManager::ConstructorAccess::Tag);

		if (!ShaderManager->Initialize())
		{
			return false;
		}

		// Initialize App info struct

		VkApplicationInfo AppInfo{};

		AppInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;

		AppInfo.pEngineName = "Aeon Engine";
		AppInfo.engineVersion = VK_MAKE_VERSION(0, 1, 0); // TODO set this from engine config

		// TODO set this from game config
		AppInfo.pApplicationName = "Aeon Engine";
		AppInfo.applicationVersion = VK_MAKE_VERSION(0, 1, 0);

		AppInfo.apiVersion = VK_API_VERSION_1_3;

		// Configure extensions for Vulkan instance

		GArray<VkExtensionProperties> AvailableExtensionsArray = VulkanQueryArrayData<vkEnumerateInstanceExtensionProperties>(nullptr);

#	if ZZINTERNAL_AEON_VULKAN_LOG_EXTRA_INFO
		AEON_LOG(Info, VulkanRenderer, "Available instance extensions ({}):", AvailableExtensionsArray.Count());
		for (auto const& Extension : AvailableExtensionsArray)
		{
			AEON_LOG(Info, VulkanRenderer, "  - {}", Extension.extensionName);
		}
#	endif

#	if AEON_USING_GLFW_LIBRARY
		uint32 GlfwReqExtensionsCount;
		char const** GlfwReqExtensionsArray = glfwGetRequiredInstanceExtensions(&GlfwReqExtensionsCount);

		for (szint Idx = 0; Idx < GlfwReqExtensionsCount; ++Idx)
		{
			char const* ExtensionName = GlfwReqExtensionsArray[Idx];
			bool HasExtension = TryAddExtension(AvailableExtensionsArray, EnabledExtensionsSet, ExtensionName);
			if (!HasExtension)
			{
				AEON_LOG(Error, VulkanRenderer, "Required instance extension '{}' (requested by GLFW) is unavailable!", ExtensionName);
				return false;
			}
		}
#	endif

	//	uint32 ReqExtensionsCount;
	//	GArray<char const*> ReqExtensionsArray = /* TODO get required extensions from config */

	//	uint32 OptExtensionsCount;
	//	GArray<char const*> OptExtensionsArray = /* TODO get optional extensions from config */

#	if AEON_PLATFORM_MACOS
		{
			// NOTE to self: MacOS likely implements Vulkan on top of Metal, and as such fails to be a conforming implementation of the Vulkan spec;
			//  see https://registry.khronos.org/vulkan/specs/1.3-extensions/man/html/VK_KHR_portability_enumeration.html
			//  and https://registry.khronos.org/vulkan/specs/1.3-extensions/man/html/VK_KHR_portability_subset.html

			constexpr char const* PortabilityExtensionName = VK_KHR_PORTABILITY_ENUMERATION_EXTENSION_NAME;

			bool HasExtension = TryAddExtension(AvailableExtensionsArray, EnabledExtensionsSet, PortabilityExtensionName);
			if (!HasExtension)
			{
				AEON_LOG(Error, VulkanRenderer, "Required instance extension '{}' is unavailable!", PortabilityExtensionName);
				return false;
			}
		}
#	endif

		// CHECKME should this remain required?
		if constexpr (ShouldUseValidationLayers)
		{
			constexpr char const* DbgUtilsExtensionName = VK_EXT_DEBUG_UTILS_EXTENSION_NAME;

			bool HasExtension = TryAddExtension(AvailableExtensionsArray, EnabledExtensionsSet, DbgUtilsExtensionName);
			if (!HasExtension)
			{
				AEON_LOG(Error, VulkanRenderer, "Required instance extension '{}' is unavailable!", DbgUtilsExtensionName);
				return false;
			}
		}

		GArray<char const*> EnabledExtensionsArray{ TReserveStorage::Tag, EnabledExtensionsSet.Count() };

		AEON_LOG(Info, VulkanRenderer, "Enabled extensions ({}):", EnabledExtensionsSet.Count());
		for (auto const& Extension : EnabledExtensionsSet)
		{
			auto ExtensionName = Extension.extensionName;

			AEON_LOG(Info, VulkanRenderer, "  - {}", ExtensionName);

			EnabledExtensionsArray.Add(ExtensionName);
		}

		// Configure validation layers for Vulkan instance, if enabled

		GArray<VkLayerProperties> AvailableLayersArray{ TReserveStorage::Tag, 0 };
		GArray<char const*> EnabledLayersArray{ TReserveStorage::Tag, 0 };

		if constexpr (ShouldUseValidationLayers)
		{
			AvailableLayersArray = VulkanQueryArrayData<vkEnumerateInstanceLayerProperties>();

#		if ZZINTERNAL_AEON_VULKAN_LOG_EXTRA_INFO
			AEON_LOG(Info, VulkanRenderer, "Available validation layers ({}):", AvailableLayersArray.Count());
			for (auto const& Layer : AvailableLayersArray)
			{
				AEON_LOG(Info, VulkanRenderer, "  - {}", Layer.layerName);
			}
#		endif

			// NOTE Despite the name, validations layers are not strictly required,
			//  and initialization will continue in the case of a failure
			GArray<char const*> ReqLayersArray; /* TODO get required layers from config */

			// See https://vulkan.lunarg.com/doc/view/latest/linux/khronos_validation_layer.html
			//  for usage information on validation layers
			constexpr char const* KhronosDiagLayer = "VK_LAYER_KHRONOS_validation";

			ReqLayersArray.Add(KhronosDiagLayer);

			for (auto ReqLayerName : ReqLayersArray)
			{
				bool HasLayer = TryAddLayer(AvailableLayersArray, EnabledLayersSet, ReqLayerName);
				if (!HasLayer)
				{
					AEON_LOG(Warning, VulkanRenderer, "Validation layer '{}' is unavailable!", ReqLayerName);
				}
			}

			EnabledLayersArray.Reserve(EnabledLayersSet.Count());

			AEON_LOG(Info, VulkanRenderer, "Enabled validation layers ({}):", EnabledLayersSet.Count());
			for (auto const& Layer : EnabledLayersSet)
			{
				auto LayerName = Layer.layerName;

				AEON_LOG(Info, VulkanRenderer, "  - {}", LayerName);

				EnabledLayersArray.Add(LayerName);
			}
		}

		// Initialize DebugUtils info struct

		VkDebugUtilsMessengerCreateInfoEXT DebugCallbackCreateInfo{};

		DebugCallbackCreateInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
		DebugCallbackCreateInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT
												  | VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT
												  | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT
												  | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
		DebugCallbackCreateInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT
											  | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT
											  | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT
											  | VK_DEBUG_UTILS_MESSAGE_TYPE_DEVICE_ADDRESS_BINDING_BIT_EXT;
		DebugCallbackCreateInfo.pfnUserCallback = DebugMessenger_Internal;
		DebugCallbackCreateInfo.pUserData = nullptr; // Optional // CHECKME maybe use 'this'?

		// Initialize InstanceCreate info struct

		VkInstanceCreateInfo CreateInfo{};

		CreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		CreateInfo.pApplicationInfo = &AppInfo;

		CreateInfo.enabledExtensionCount = EnabledExtensionsArray.Count();
		CreateInfo.ppEnabledExtensionNames = EnabledExtensionsArray.Buffer();

		if constexpr (ShouldUseValidationLayers)
		{
			CreateInfo.enabledLayerCount = EnabledLayersArray.Count();
			CreateInfo.ppEnabledLayerNames = EnabledLayersArray.Buffer();

			// Use same logger to log initialization
			CreateInfo.pNext = &DebugCallbackCreateInfo;
		}

#	if AEON_PLATFORM_MACOS
		CreateInfo.flags |= VK_INSTANCE_CREATE_ENUMERATE_PORTABILITY_BIT_KHR;
#	endif

		// TODO use an "allocator" at some point (maybe use VMA lib?)
		// https://github.com/GPUOpen-LibrariesAndSDKs/VulkanMemoryAllocator/tree/master
	//	[[maybe_unused]]
	//	VkAllocationCallbacks AllocatorData;

		if (VkResult Result = vkCreateInstance(&CreateInfo, nullptr, &InstanceHandle))
		{
			// FIXME Make VkResult to string conversion method
			AEON_LOG(Error, VulkanRenderer, "Failed to create Vulkan instance. Error code: {}", int(Result));
			return false;
		}

		if constexpr (ShouldUseValidationLayers)
		{
			if (VkResult Result = AEON_VULKAN_DYN_CALL_FUNC(vkCreateDebugUtilsMessengerEXT, InstanceHandle,
															&DebugCallbackCreateInfo, nullptr, &DebugMessengerHandle))
			{
				// Log and proceed regardless of the error
				AEON_LOG(Error, VulkanRenderer, "Failed to create Vulkan debug messenger. Error code: {}", int(Result));
			}
		}

		return true;
	}

	void SVulkanRenderer::Terminate()
	{
		AEON_ASSERT(SThread::ThisId() == MainThread.Id());
		AEON_ASSERT(InstanceHandle != VK_NULL_HANDLE);

		for (auto& Device : DeviceArray)
		{
			Device->Terminate();
		}

		DeviceArray.Empty(TReclaimStorage::Tag);

		if constexpr (ShouldUseValidationLayers)
		{
			if (VkResult Result = AEON_VULKAN_DYN_CALL_FUNC(vkDestroyDebugUtilsMessengerEXT, InstanceHandle,
															DebugMessengerHandle, nullptr))
			{
				// Log and proceed regardless of the error
				AEON_LOG(Error, VulkanRenderer, "Failed to destroy Vulkan debug messenger. Error code: {}", int(Result));
			}

			DebugMessengerHandle = VK_NULL_HANDLE;
		}

		vkDestroyInstance(InstanceHandle, nullptr);

		InstanceHandle = VK_NULL_HANDLE;
	}

	VkInstance SVulkanRenderer::GetHandle()
	{
		return InstanceHandle;
	}

	void SVulkanRenderer::DetectDevices()
	{
		// CHECKME Should we instead repopulate the array if we have called this previously? If so, ensure no device is selected/initialized
		AEON_FAILSAFE(DeviceArray.IsEmpty(), return);

		GArray<VkPhysicalDevice> DeviceHandleArray = VulkanQueryArrayData<vkEnumeratePhysicalDevices>(InstanceHandle);

		AEON_LOG(Info, VulkanRenderer, "Detected devices ({}):", DeviceHandleArray.Count());
		for (auto DeviceHandle : DeviceHandleArray)
		{
			auto Device = MakeUniquePtr<SVulkanDevice>(SVulkanDevice::ConstructorAccess::Tag, this, DeviceHandle);

			Device->Initialize();

#		if ZZINTERNAL_AEON_VULKAN_LOG_EXTRA_INFO
#			define AEON_VULKAN_DEVICE_FEATURE(FeatureName) "\n\t  - " AEON_TOKEN_STR(FeatureName) "={}"
			constexpr auto FormatString = "  - Name: {}\n\tType: {}\n\tFeatures:" AEON_ITERATE_VULKAN_DEVICE_FEATURE_SET;
#			undef AEON_VULKAN_DEVICE_FEATURE
#		else
			constexpr auto FormatString = "  - Name: {}\n\tType: {}";
#		endif

		// FIXME improve this (use a single formatted string "@fmtcode1Yes@fmtcode2No", and views for the log)
#if AEON_BUILD_DISTRIBUTION
			STerminalTextStyler Styler{ TClass<SNullTerminalTextStylerBackend>{} };
#else
			STerminalTextStyler Styler{ TClass<SANSITerminalTextStylerBackend>{}, EANSIColorSpecifier::Index };
#endif

			auto StyledFeatureSupport = [&Styler] (bool Value)
				{
					SString Result;
					Styler.BindString(Result);
					if (Value)
					{
						Styler.MakeColored(ETerminalColor::ForestGreen)
							  .MakeBold()
							  .FormatConcat("Yes")
							  .MakeRegular();
					}
					else
					{
						Styler.MakeColored(ETerminalColor::IndianRed)
							  .MakeSlanted()
							  .FormatConcat("No")
							  .MakeRegular();
					}
					return Result;
				};

#		if ZZINTERNAL_AEON_VULKAN_LOG_EXTRA_INFO
			auto const& DeviceFeatures = Device->GetFeatures();

#			define AEON_VULKAN_DEVICE_FEATURE(FeatureName) \
				,StyledFeatureSupport(DeviceFeatures.AEON_TOKEN_CONCAT(Get, FeatureName, Support)())
			AEON_LOG(Info, VulkanRenderer, FormatString,
					 Device->GetName(), MakeDeviceTypeString(Device->GetType())
					 AEON_ITERATE_VULKAN_DEVICE_FEATURE_SET);
#			undef AEON_VULKAN_DEVICE_FEATURE
#		else
			AEON_LOG(Info, VulkanRenderer, FormatString,
					 Device.GetName(), MakeDeviceTypeString(Device.GetType()));
#		endif

			DeviceArray.Add(Move(Device));
		}
	}

	bool SVulkanRenderer::SelectDevice(SVulkanDeviceFeatureSet const& RequiredFeatures)
	{
		AEON_ASSERT(InstanceHandle != VK_NULL_HANDLE);

		SelectedDeviceIndex = ~0_u32;

		AEON_LOG(Info, VulkanRenderer, "Device selection:");

		uint64 SelectedDeviceScore = 0;
		uint32 DeviceIndex = 0;
		for (auto& Device : DeviceArray)
		{
			// First, check if feature set matches the required features

			// TODO move this elsewhere (maybe cmake flag?)
			constexpr bool ShouldSkipDevicesWithMissingFeatures = true;

			AEON_LOG(Info, VulkanRenderer, " - Device '{}'", Device->GetName());

			SVulkanDeviceFeatureSet const& DeviceFeatures = Device->GetFeatures();

			GFunction MissingFeatureLambda = [] (SStringView FeatureName, [[maybe_unused]] szint FeatureIndex)
			{
				AEON_LOG(Info, VulkanRenderer, "   Feature '{}' is not supported by this device!", FeatureName);
			//	RequiredFeatures.SetFeatureSupportByIndex(FeatureIndex, false);
			};

			if (!DeviceFeatures.CheckFeatureSupport(RequiredFeatures, Move(MissingFeatureLambda)))
			{
				if constexpr (ShouldSkipDevicesWithMissingFeatures)
				{
					AEON_LOG(Info, VulkanRenderer, "   Skipping device...");

					++DeviceIndex;
					continue;
				}
			}

			// Then, attribute a score to this device

			uint64 DeviceScore = 0;

			// TODO maybe attribute score depending on feature set

			VkPhysicalDeviceLimits const& Properties = Device->GetProperties();

			// FIXME use ScoringFactorList to adjust the score of each property

			// Higher score if device supports larger textures
			DeviceScore += Properties.maxImageDimension2D;

			// Higher score if device supports larger volumetric textures
			DeviceScore += Properties.maxImageDimension3D;

			// Lastly, check if score surpasses the selected device, and replace it if so
			if (DeviceScore > SelectedDeviceScore)
			{
				SelectedDeviceScore = DeviceScore;
				SelectedDeviceIndex = DeviceIndex;
			}

			AEON_LOG(Info, VulkanRenderer, "   Attributed score: {}u", DeviceScore);

			++DeviceIndex;
		}

		// TODO add log indicating which device was selected

		AEON_FAILSAFE(SelectedDeviceIndex != ~0_u32, return false);

		return true;
	}

	bool SVulkanRenderer::HasSelectedDevice() const
	{
		return SelectedDeviceIndex != ~0_u32;
	}

	SVulkanDevice& SVulkanRenderer::GetSelectedDevice()
	{
		AEON_ASSERT(HasSelectedDevice());
		return *DeviceArray[SelectedDeviceIndex];
	}

	SVulkanDevice& SVulkanRenderer::GetDevice(uint32 Index)
	{
		AEON_ASSERT(InstanceHandle != VK_NULL_HANDLE);
		return *DeviceArray[Index];
	}

	uint32 SVulkanRenderer::GetDeviceCount() const
	{
		AEON_ASSERT(InstanceHandle != VK_NULL_HANDLE);
		return DeviceArray.Count();
	}

	SVulkanShaderManager& SVulkanRenderer::GetShaderManager()
	{
		AEON_ASSERT(InstanceHandle != VK_NULL_HANDLE);
		return *ShaderManager;
	}

	GHashSet<VkExtensionProperties> const& SVulkanRenderer::GetEnabledExtensions() const
	{
		return EnabledExtensionsSet;
	}

	GHashSet<VkLayerProperties> const& SVulkanRenderer::GetEnabledLayers() const
	{
		return EnabledLayersSet;
	}

	VkBool32 SVulkanRenderer::DebugMessenger_Internal(VkDebugUtilsMessageSeverityFlagBitsEXT MessageSeverity,
													  VkDebugUtilsMessageTypeFlagsEXT MessageType,
													  VkDebugUtilsMessengerCallbackDataEXT const* CallbackData,
													  void* UserData)
	{
		AEON_UNUSED(UserData);

		// TODO See: https://github.com/KhronosGroup/Vulkan-ValidationLayers/issues/2509
		//  for info about UNASSIGNED-BestPractices-vkCreateInstance-specialuse-extension-debugging

		// See: https://vulkan.lunarg.com/doc/view/latest/linux/khronos_validation_layer.html
		// for more info on validation layer logs
		SStringView EventPrefix;
		SString EventMessage = CallbackData->pMessage;

		switch (MessageType)
		{
			case VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT:
			default:
				EventPrefix = "Debug: ";
				break;
			case VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT:
				// NOTE Validation messages already include the prefix string "Validation <msg-sev>:",
				//  where <msg-sev> is the severity of the message.
				EventPrefix = "Validation";
				EventMessage = EventMessage.SubstringToBack(EventMessage.FindFirstOf(": "));
				break;
			case VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT:
				EventPrefix = "Optimization";
				EventMessage = EventMessage.SubstringToBack(EventMessage.FindFirstOf(": "));
				break;
			case VK_DEBUG_UTILS_MESSAGE_TYPE_DEVICE_ADDRESS_BINDING_BIT_EXT:
				EventPrefix = ""; // TODO figure out if we need some prefix here
				AEON_DEBUGGER_BREAK();
				break;
		}

		switch (MessageSeverity)
		{
			case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT:
			case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT:
			default:
				AEON_LOG(Info, VulkanRenderer, "{}{}", EventPrefix, EventMessage);
				break;
			case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
				// This is always of either type Validation or Performance
				AEON_LOG(Warning, VulkanRenderer, "{}{}", EventPrefix, EventMessage);
				break;
			case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
				// This is always of type Validation
				AEON_LOG(Error, VulkanRenderer, "{}{}", EventPrefix, EventMessage);
				break;
		}

		// > The callback returns a boolean that indicates if the Vulkan call that triggered the validation layer message should be aborted.
		// > If the callback returns true, then the call is aborted with the VK_ERROR_VALIDATION_FAILED_EXT error.
		// TODO add logic to determine what value to return
		return VK_FALSE;
	}

}
