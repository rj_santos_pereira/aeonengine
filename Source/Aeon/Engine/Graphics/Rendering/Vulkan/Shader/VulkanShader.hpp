#ifndef ZZINTERNAL_INCLUDE_AEON_ENGINE_GRAPHICS_RENDERING_VULKAN_SHADER_VULKANSHADER
#define ZZINTERNAL_INCLUDE_AEON_ENGINE_GRAPHICS_RENDERING_VULKAN_SHADER_VULKANSHADER

#include "Aeon/Framework/Framework.hpp"

#include "Aeon/Engine/Graphics/Rendering/Vulkan/VulkanCommon.hpp"

namespace Aeon
{
	class SVulkanDevice;

	enum struct EVulkanShaderType : uint8
	{
		Unknown,

		// General compute
		Compute,

		// Primitive shading
		Vertex,
		TessellationCtrl, // D3D Hull Shader
		TessellationEval, // D3D Domain Shader
		Geometry,
		Fragment, // D3D Pixel Shader

		// TODO enable more types once necessary (update logic in SVulkanShaderManager::RefreshShaderList)
#if 0
		// Mesh shading
		Task,
		Mesh,

		// Ray-tracing
		RayGeneration,
		RayIntersection,
		RayAnyHit,
		RayClosestHit,
		RayMiss,
		RayCallable,
#endif
	};

	AEON_DECLARE_FLAG_ENUM(EVulkanShaderStatus, uint8)
	(
		AEON_FLAG_ENUM_RULE_AS_IS,
		Unloaded		= 0b00,
		Compiling		= 0b01,
		Loaded			= 0b10,
		Recompiling 	= 0b11,
	);

	class SVulkanShader
	{
	public:
		AEON_DECLARE_ACCESS_CLASS(ConstructorAccess, class SVulkanShaderManager);

		explicit SVulkanShader(ConstructorAccess, SPath const& NewSourcePath, SPath const& NewBinaryPath, EVulkanShaderType NewShaderType);

		bool SetupModule(SVulkanDevice& Device);

		void TeardownModule();

		bool ReloadBinary();

		void UpdateBinaryMetadata();
		void UpdateBinary(uint32 const* DataPtr, szint DataSize);

		bool LoadSource();
		void UnloadSource();

		bool HasLoadedBinary() const;
		bool IsUpdatingBinary() const;

		bool HasLoadedSource() const;

		bool IsTimestampUpToDate() const;
		bool IsHashUpToDate() const;

		void RefreshMetadata();

		void MarkUpdatingBinary(bool NewIsUpdatingBinary);

		GDuple<char const*, szint> GetSourceData() const;
		GDuple<uint32 const*, szint> GetBytecodeData() const;

		EVulkanShaderStatus GetStatus() const;

		STextStringView GetName() const;

		EVulkanShaderType GetShaderType() const;

	private:
		void UpdateBinaryMetadata_Internal(SFile& BinaryFile);

		struct DShaderMetadata
		{
			uint64 Timestamp;
			uint64 Hash;
		};

		SVulkanDevice* OwningDevice;
		VkShaderModule ModuleHandle;

		SFile SourceFile;
		SPath BinaryPath;
		STextString ShaderName;

		// NOTE This effectively limits the size of the file to 4GB, which is fine for a single source/binary file

		DShaderMetadata BinaryMetadata;
		GArray<uint32> BinaryData;

		mutable DShaderMetadata CachedSourceMetadata;
		GArray<char> CachedSourceData;

		EVulkanShaderType ShaderType;
		EVulkanShaderStatus Status;

		mutable uint8 IsCachedTimestampRefreshed : 1;
		mutable uint8 IsCachedHashRefreshed : 1;

		static constexpr szint MetadataSize = sizeof(DShaderMetadata);
	};
}

#endif