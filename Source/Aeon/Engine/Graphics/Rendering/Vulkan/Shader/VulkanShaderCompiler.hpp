#ifndef ZZINTERNAL_INCLUDE_AEON_ENGINE_GRAPHICS_RENDERING_VULKAN_SHADER_VULKANSHADERCOMPILER
#define ZZINTERNAL_INCLUDE_AEON_ENGINE_GRAPHICS_RENDERING_VULKAN_SHADER_VULKANSHADERCOMPILER

#include "Aeon/Framework/Framework.hpp"

#include "Aeon/Engine/Graphics/Rendering/Vulkan/Shader/VulkanShader.hpp"

#include "shaderc/status.h"

namespace Aeon
{
	enum struct EShaderCompileResult	: uint8
	{
		Unknown							= 0b1111,
		Success 						= shaderc_compilation_status_success,
		InvalidStage 					= shaderc_compilation_status_invalid_stage,
		InvalidAssembly 				= shaderc_compilation_status_invalid_assembly,
		CompilationError 				= shaderc_compilation_status_compilation_error,
		ValidationError 				= shaderc_compilation_status_validation_error,
		TransformationError 			= shaderc_compilation_status_transformation_error,
		ConfigurationError 				= shaderc_compilation_status_configuration_error,
		InternalError 					= shaderc_compilation_status_internal_error,
	};

	inline SStringView MakeCompileResultString(EShaderCompileResult Value)
	{
		switch (Value)
		{
			case EShaderCompileResult::Unknown: return "Unknown";
			case EShaderCompileResult::Success: return "Success";
			case EShaderCompileResult::InvalidStage: return "InvalidStage";
			case EShaderCompileResult::InvalidAssembly: return "InvalidAssembly";
			case EShaderCompileResult::CompilationError: return "CompilationError";
			case EShaderCompileResult::ValidationError: return "ValidationError";
			case EShaderCompileResult::TransformationError: return "TransformationError";
			case EShaderCompileResult::ConfigurationError: return "ConfigurationError";
			case EShaderCompileResult::InternalError: return "InternalError";
		}
	}

	struct DShaderCompileResult
	{
		GSharedPtr<SVulkanShader> Shader;
		GArray<uint32> BinaryData{ TReserveStorage::Tag, 0 };
		SString CompileMessage;
		EShaderCompileResult CompileResult : 4 = EShaderCompileResult::Unknown;
		uint16 NumWarnings : 14 = 0;
		uint16 NumErrors : 14 = 0;
	};

	struct DShaderCompileRequest
	{
		GPromise<DShaderCompileResult> ResultPromise;
		GSharedPtr<SVulkanShader> Shader;
	};

	class SVulkanShaderCompiler
	{
	public:
		explicit SVulkanShaderCompiler(uint32 RequestedThreadCount);
		~SVulkanShaderCompiler();

		GFuture<DShaderCompileResult> RequestCompile(GSharedPtr<SVulkanShader> const& Shader);
		GArray<GFuture<DShaderCompileResult>> RequestCompileBatch(GArray<GSharedPtr<SVulkanShader>> const& ShaderArray);

		static uint32 SupportedWorkerCount();

	private:
		void WorkerLoop_Internal(uint32 ThreadIdx);
		void CompileShader_Internal(uint32 ThreadIdx, DShaderCompileRequest&& Request);

		GConcurrentQueue<DShaderCompileRequest> RequestQueue;
		GAtomicUint32<EMemoryOrder::Relaxed> RequestCounter;

		uint32 ThreadCount;
		GArray<SThread> ThreadArray;
	};
}

#endif