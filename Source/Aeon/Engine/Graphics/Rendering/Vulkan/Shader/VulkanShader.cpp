#include "Aeon/Engine/Graphics/Rendering/Vulkan/Shader/VulkanShader.hpp"

#include "Aeon/Engine/Graphics/Rendering/Vulkan/VulkanDevice.hpp"

#include "Aeon/Engine/Logging/Log.hpp"

AEON_DECLARE_LOG_FAMILY(VulkanShader, ETerminalColor::BlueViolet,
						ELogSourceInfo::FileName | ELogSourceInfo::FuncName | ELogSourceInfo::LineNum,
						ELogDateTimeInfo::None);

// FIXME Add more logs and specify shader name in these

namespace Aeon
{
	SVulkanShader::SVulkanShader(ConstructorAccess, SPath const& NewSourcePath, SPath const& NewBinaryPath, EVulkanShaderType NewShaderType)
		: OwningDevice{ nullptr }
		, ModuleHandle{ VK_NULL_HANDLE }
		, SourceFile{ SFile::At(NewSourcePath) }
		, BinaryPath{ NewBinaryPath }
		, ShaderName{}
		, BinaryMetadata{}
		, BinaryData{ TReserveStorage::Tag, 0 }
		, CachedSourceMetadata{}
		, CachedSourceData{ TReserveStorage::Tag, 0 }
		, ShaderType{ NewShaderType }
		, Status{ EVulkanShaderStatus::Unloaded }
		, IsCachedTimestampRefreshed{ false }
		, IsCachedHashRefreshed{ false }
	{
		// FIXME
		auto ShaderName_Platform = SourceFile.GetName();

		std::wstring_convert<std::codecvt_utf8_utf16<wchar>> NameConverter;
		auto ShaderNameString = NameConverter.to_bytes(ShaderName_Platform.CharBuffer());

		ShaderName.Concat(STextStringView{ ShaderNameString.c_str(), uint32(ShaderNameString.length()) });
	}

	bool SVulkanShader::SetupModule(SVulkanDevice& Device)
	{
		AEON_ASSERT(ModuleHandle == VK_NULL_HANDLE);

		VkShaderModuleCreateInfo ModuleCreateInfo{};
		ModuleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;

		uint32 const* BytecodePtr = AeonMemory::StartObjectArrayLifetime<uint32[]>(BinaryData.Buffer(), BinaryData.Count());
		szint BytecodeSize = Aeon::SizeOf(uint32, BinaryData.Count());

		ModuleCreateInfo.pCode = BytecodePtr;
		ModuleCreateInfo.codeSize = BytecodeSize;

		VkDevice DeviceHandle = Device.GetLogicalDeviceHandle();

		if (VkResult Result = vkCreateShaderModule(DeviceHandle, &ModuleCreateInfo, nullptr, &ModuleHandle))
		{
			AEON_LOG(Error, VulkanShader, "Failed to create module for shader '{}' with device '{}'. Error code: {}", 0, Device.GetName(), int(Result));
			return false;
		}

		OwningDevice = addressof(Device);

		// CHECKME The memory for the binary data can be reclaimed here (maybe preserve the metadata only?),
		//  once the engine logic for loading/reloading shaders is in place, review this

		return true;
	}

	void SVulkanShader::TeardownModule()
	{
		AEON_ASSERT(ModuleHandle != VK_NULL_HANDLE);

		VkDevice DeviceHandle = OwningDevice->GetLogicalDeviceHandle();

		vkDestroyShaderModule(DeviceHandle, ModuleHandle, nullptr);
	}

	bool SVulkanShader::ReloadBinary()
	{
		AEON_FAILSAFE(!IsUpdatingBinary(), return false);

		if (!BinaryPath.Exists())
		{
			return false;
		}

		SFile BinaryFile = SFile::At(BinaryPath);

		szint BinaryFileSize = BinaryFile.SizeInBytes();
		szint BinaryDataSize = BinaryFileSize - MetadataSize;

		if (!AeonMath::IsClamped(BinaryFileSize, MetadataSize, szint(TNumericProperties<uint32>::Maximum)))
		{
			AEON_LOG(Error, VulkanShader, "Failed to reload shader binary. Invalid file size!");
			return false;
		}

		DShaderMetadata NewBinaryMetadata;
		GArray<uint32> NewBinaryData{ TCreateElements::Tag, uint32(BinaryDataSize / sizeof(uint32)) };

		bool HasLoaded = BinaryFile.Open(EFileAccessMode::Read);
		HasLoaded = HasLoaded && BinaryFile.ReadBytes(addressof(NewBinaryMetadata), MetadataSize);
		HasLoaded = HasLoaded && BinaryFile.ReadBytes(NewBinaryData.Buffer(), BinaryDataSize);
		if (!HasLoaded)
		{
			AEON_LOG(Error, VulkanShader, "Failed to read binary data from shader file!");
			return false;
		}

		BinaryMetadata = Move(NewBinaryMetadata);
		BinaryData = Move(NewBinaryData);

		Status = EVulkanShaderStatus::Loaded;

		return true;
	}

	void SVulkanShader::UpdateBinaryMetadata()
	{
		AEON_FAILSAFE(IsCachedTimestampRefreshed && IsCachedHashRefreshed, return, "Failed to update binary: metadata not refreshed!");

		SFile BinaryFile = SFile::CreateAt(BinaryPath, EFileAccessMode::Write);

		AEON_FAILSAFE(BinaryFile.IsOpen(), return); // FIXME better error handling

		UpdateBinaryMetadata_Internal(BinaryFile);
	}

	void SVulkanShader::UpdateBinary(uint32 const* DataPtr, szint DataSize)
	{
		AEON_FAILSAFE(IsCachedTimestampRefreshed && IsCachedHashRefreshed, return, "Failed to update binary: metadata not refreshed!");

		SFile BinaryFile = SFile::CreateAt(BinaryPath, EFileAccessMode::Write);

		AEON_FAILSAFE(BinaryFile.IsOpen(), return); // FIXME better error handling

		UpdateBinaryMetadata_Internal(BinaryFile);

		szint DataByteSize = Aeon::SizeOf(uint32, DataSize);

		BinaryData.Resize(uint32(DataSize));
		AeonMemory::MemCopy(BinaryData.Buffer(), DataPtr, DataByteSize);

		BinaryFile.WriteBytes(BinaryData.Buffer(), DataByteSize);

		Status = EVulkanShaderStatus::Loaded;
	}

	bool SVulkanShader::LoadSource()
	{
		szint SourceFileSize = SourceFile.SizeInBytes();

		if (!AeonMath::IsClamped(SourceFileSize, 0_sz, szint(TNumericProperties<uint32>::Maximum)))
		{
			AEON_LOG(Error, VulkanShader, "Failed to load shader source. Invalid file size!");
			return false;
		}

		GArray<char> NewSourceData{ TCreateElements::Tag, uint32(SourceFileSize) };

		bool HasLoaded = SourceFile.Open(EFileAccessMode::Read);
		HasLoaded = HasLoaded && SourceFile.ReadBytes(NewSourceData.Buffer(), NewSourceData.Count());
		if (!HasLoaded)
		{
			AEON_LOG(Error, VulkanShader, "Failed to read source data from shader file!");
			return false;
		}

		CachedSourceData = Move(NewSourceData);

		return true;
	}

	void SVulkanShader::UnloadSource()
	{
		CachedSourceData.Empty(TReclaimStorage::Tag);

		IsCachedTimestampRefreshed = false;
		IsCachedHashRefreshed = false;
	}

	bool SVulkanShader::HasLoadedBinary() const
	{
		return Aeon::HasAnyFlags(Status, EVulkanShaderStatus::Loaded);
	}

	bool SVulkanShader::IsUpdatingBinary() const
	{
		return Aeon::HasAnyFlags(Status, EVulkanShaderStatus::Compiling);
	}

	bool SVulkanShader::HasLoadedSource() const
	{
		return !CachedSourceData.IsEmpty();
	}

	bool SVulkanShader::IsTimestampUpToDate() const
	{
		AEON_FAILSAFE(Aeon::HasAnyFlags(Status, EVulkanShaderStatus::Loaded), return false);

		SourceFile.RefreshStats();

		STimePoint BinaryModifyTime = STimePoint::FromTimestamp(BinaryMetadata.Timestamp, ESpecificTimezone::UTC);
		STimePoint SourceModifyTime = SourceFile.LastModifyTime(ESpecificTimezone::UTC);

		CachedSourceMetadata.Timestamp = SourceModifyTime.ToTimestamp();
		IsCachedTimestampRefreshed = true;

		AEON_FAILSAFE(BinaryModifyTime <= SourceModifyTime, void(),
					  "Reloading shader binary with a timestamp more recent than the corresponding source file. "
					  "Either the binary data corresponds to a different version of the source data, "
					  "or the source data needs to be reloaded.");

		return (BinaryModifyTime == SourceModifyTime);
	}

	bool SVulkanShader::IsHashUpToDate() const
	{
		AEON_FAILSAFE(Aeon::HasAnyFlags(Status, EVulkanShaderStatus::Loaded), return false);

		AEON_FAILSAFE(!CachedSourceData.IsEmpty(), return false);

		uint64 BinaryHash = BinaryMetadata.Hash;
		uint64 SourceHash = Aeon::FNV1aHash<uint64>(reinterpret_cast<cbyteptr>(CachedSourceData.Buffer()), CachedSourceData.Count());

		CachedSourceMetadata.Hash = SourceHash;
		IsCachedHashRefreshed = true;

//		if (IsTimestampUpToDate())
//		{
//			AEON_FAILSAFE(BinaryHash == SourceHash,
//						  return false,
//						  "Reloading shader binary with the same timestamp as the corresponding source file, but the hash differs!"
//						  "Either the binary data corresponds to a different version of the source data, "
//						  "or the source data needs to be reloaded.");
//		}

		return (BinaryHash == SourceHash);
	}

	void SVulkanShader::RefreshMetadata()
	{
		AEON_FAILSAFE(!CachedSourceData.IsEmpty(), return);

		SourceFile.RefreshStats();

		uint64 NewTimestamp = SourceFile.LastModifyTime(ESpecificTimezone::UTC).ToTimestamp();
		uint64 NewHash = Aeon::FNV1aHash<uint64>(reinterpret_cast<cbyteptr>(CachedSourceData.Buffer()), CachedSourceData.Count());

		CachedSourceMetadata = { NewTimestamp, NewHash };
		IsCachedTimestampRefreshed = true;
		IsCachedHashRefreshed = true;
	}

	void SVulkanShader::MarkUpdatingBinary(bool NewIsUpdatingBinary)
	{
		if (NewIsUpdatingBinary)
		{
			Status |= EVulkanShaderStatus::Compiling;
		}
		else
		{
			Status &= ~EVulkanShaderStatus::Compiling;
		}
	}

	GDuple<char const*, szint> SVulkanShader::GetSourceData() const
	{
		AEON_FAILSAFE(!CachedSourceData.IsEmpty(), return {}, "Invalid source data!");

		szint DataSize = CachedSourceData.Count();
		return { AeonMemory::StartObjectArrayLifetime<char[]>(CachedSourceData.Buffer(), DataSize), DataSize };
	}

	GDuple<uint32 const*, szint> SVulkanShader::GetBytecodeData() const
	{
		AEON_FAILSAFE(!BinaryData.IsEmpty(), return {}, "Invalid binary data!");

		szint DataSize = BinaryData.Count();
		return { AeonMemory::StartObjectArrayLifetime<uint32[]>(BinaryData.Buffer(), DataSize), DataSize };
	}

	EVulkanShaderStatus SVulkanShader::GetStatus() const
	{
		return Status;
	}

	STextStringView SVulkanShader::GetName() const
	{
		return ShaderName;
	}

	EVulkanShaderType SVulkanShader::GetShaderType() const
	{
		return ShaderType;
	}

	void SVulkanShader::UpdateBinaryMetadata_Internal(SFile& BinaryFile)
	{
		BinaryMetadata = CachedSourceMetadata;

		BinaryFile.WriteBytes(addressof(BinaryMetadata), MetadataSize);

		IsCachedTimestampRefreshed = false;
		IsCachedHashRefreshed = false;
	}
}
