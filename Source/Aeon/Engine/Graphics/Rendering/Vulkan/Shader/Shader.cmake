#add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/)

target_sources(${PROJECT_NAME}
	PUBLIC
		${CMAKE_CURRENT_SOURCE_DIR}/VulkanShaderManager.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/VulkanShader.hpp
	PRIVATE
		${CMAKE_CURRENT_SOURCE_DIR}/VulkanShaderCompiler.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/VulkanShaderCompiler.cpp
		${CMAKE_CURRENT_SOURCE_DIR}/VulkanShaderManager.cpp
		${CMAKE_CURRENT_SOURCE_DIR}/VulkanShader.cpp
	)