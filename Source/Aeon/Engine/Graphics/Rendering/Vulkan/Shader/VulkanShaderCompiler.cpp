#include "Aeon/Engine/Graphics/Rendering/Vulkan/Shader/VulkanShaderCompiler.hpp"

#include "Aeon/Engine/Logging/Log.hpp"

#include "shaderc/shaderc.hpp"

AEON_DECLARE_LOG_FAMILY(VulkanShaderCompiler, ETerminalColor::BlueViolet,
						ELogSourceInfo::FileName | ELogSourceInfo::FuncName | ELogSourceInfo::LineNum,
						ELogDateTimeInfo::None);

namespace Aeon
{
	SVulkanShaderCompiler::SVulkanShaderCompiler(uint32 RequestedWorkerCount)
		: RequestQueue{}
		, RequestCounter{ 0 }
		, ThreadCount{ RequestedWorkerCount }
		, ThreadArray{ TReserveStorage::Tag, ThreadCount }
	{
		AEON_ASSERT(RequestedWorkerCount <= SupportedWorkerCount());

		for (uint32 ThreadIdx = 0; ThreadIdx < ThreadCount; ++ThreadIdx)
		{
			SString ThreadName = SString::Format("ShaderWorker_{:0>2}", ThreadIdx);

			auto& Thread = ThreadArray.Emplace(DThreadParams{ .Name = ThreadName });

			Thread.Start(&SVulkanShaderCompiler::WorkerLoop_Internal, this, ThreadIdx);
		}
	}

	SVulkanShaderCompiler::~SVulkanShaderCompiler()
	{
		for (auto& ThreadPtr : ThreadArray)
		{
			ThreadPtr.RequestStop();
		}

		RequestCounter.StoreNotifyAll(~0_u32);

		while (auto Request = RequestQueue.Pop())
		{
			// Mark remaining requests as incomplete, this ensures promise is not broken
			Request->ResultPromise.SetResult(DShaderCompileResult{});
		}
	}

	GFuture<DShaderCompileResult> SVulkanShaderCompiler::RequestCompile(GSharedPtr<SVulkanShader> const& Shader)
	{
		DShaderCompileRequest Request;

		auto ResultFuture = Request.ResultPromise.MakeFuture();
		Request.Shader = Shader;

		RequestQueue.Push(Move(Request));

		RequestCounter.FetchIncrement();
		RequestCounter.Notify();

		return ResultFuture;
	}

	GArray<GFuture<DShaderCompileResult>> SVulkanShaderCompiler::RequestCompileBatch(GArray<GSharedPtr<SVulkanShader>> const& ShaderArray)
	{
		GArray<GFuture<DShaderCompileResult>> ResultFutureArray{ TReserveStorage::Tag, ShaderArray.Count() };

		for (auto& Shader : ShaderArray)
		{
			DShaderCompileRequest Request;

			auto ResultFuture = Request.ResultPromise.MakeFuture();
			Request.Shader = Shader;

			RequestQueue.Push(Move(Request));

			ResultFutureArray.Add(ResultFuture);
		}

		RequestCounter.FetchAdd(ShaderArray.Count());
		RequestCounter.NotifyAll();

		return ResultFutureArray;
	}

	uint32 SVulkanShaderCompiler::SupportedWorkerCount()
	{
		// NOTE Subtract one to account for the producer thread
		return GConcurrentQueue<DShaderCompileRequest>::SupportedThreadCount() - 1;
	}

	void SVulkanShaderCompiler::WorkerLoop_Internal(uint32 ThreadIdx)
	{
		while (!SThread::ShouldStop())
		{
			if (auto Request = RequestQueue.Pop())
			{
				CompileShader_Internal(ThreadIdx, Move(*Request));

				RequestCounter.FetchDecrement();
			}
			else
			{
				RequestCounter.WaitLoad(0);
			}
		}
	}

	void SVulkanShaderCompiler::CompileShader_Internal(uint32 ThreadIdx, DShaderCompileRequest&& Request)
	{
		GSharedPtr<SVulkanShader>& Shader = Request.Shader;

		STextStringView ShaderName = Shader->GetName();

		AEON_LOG(Info, VulkanShaderCompiler, "ShaderWorker_{:0>2} compiling shader: '{}'", ThreadIdx, ShaderName);

		auto [SourceBuffer, SourceSize] = Shader->GetSourceData();

		shaderc_shader_kind ShaderKind;
		switch (Shader->GetShaderType())
		{
			default:
			case EVulkanShaderType::Unknown: ShaderKind = shaderc_glsl_infer_from_source; break;
			case EVulkanShaderType::Compute: ShaderKind = shaderc_compute_shader; break;
			case EVulkanShaderType::Vertex: ShaderKind = shaderc_vertex_shader; break;
			case EVulkanShaderType::TessellationCtrl: ShaderKind = shaderc_tess_control_shader; break;
			case EVulkanShaderType::TessellationEval: ShaderKind = shaderc_tess_evaluation_shader; break;
			case EVulkanShaderType::Geometry: ShaderKind = shaderc_geometry_shader; break;
			case EVulkanShaderType::Fragment: ShaderKind = shaderc_fragment_shader; break;
		}

		shaderc::CompileOptions ShaderOptions;
		ShaderOptions.SetTargetEnvironment(shaderc_target_env::shaderc_target_env_vulkan, shaderc_env_version_vulkan_1_3);

		shaderc_optimization_level OptimizationLevel =
#	if AEON_BUILD_DEVELOPMENT
			shaderc_optimization_level_zero
#	else
			shaderc_optimization_level_performance
#	endif
		;

		ShaderOptions.SetOptimizationLevel(OptimizationLevel);

#	if AEON_BUILD_DEVELOPMENT or AEON_BUILD_DISTRIBUTION_DEBUG
		ShaderOptions.SetGenerateDebugInfo();
#	endif

		shaderc::Compiler Compiler;
		shaderc::CompilationResult<uint32> CompilationResult
			= Compiler.CompileGlslToSpv(SourceBuffer, SourceSize, ShaderKind, ShaderName.CharBuffer(), ShaderOptions);

		DShaderCompileResult Result;

		Result.Shader = Move(Request.Shader);

		Result.CompileMessage = CompilationResult.GetErrorMessage().c_str();
		Result.CompileResult = EShaderCompileResult(CompilationResult.GetCompilationStatus());
		Result.NumWarnings = uint16(CompilationResult.GetNumWarnings());
		Result.NumErrors = uint16(CompilationResult.GetNumErrors());

		if (Result.CompileResult == EShaderCompileResult::Success)
		{
			uint32 const* DataPtr = CompilationResult.begin();
			szint DataSize = std::distance(CompilationResult.begin(), CompilationResult.end());

			szint DataByteSize = Aeon::SizeOf(uint32, DataSize);

			Result.BinaryData.Resize(uint32(DataSize));
			AeonMemory::MemCopy(Result.BinaryData.Buffer(), DataPtr, DataByteSize);
		}

		Request.ResultPromise.SetResult(Move(Result));
	}
}
