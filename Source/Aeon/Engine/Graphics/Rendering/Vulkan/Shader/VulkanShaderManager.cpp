#include "Aeon/Engine/Graphics/Rendering/Vulkan/Shader/VulkanShaderManager.hpp"

#include "Aeon/Engine/Logging/Log.hpp"
#include "Aeon/Engine/System/Filesystem/EnginePaths.hpp"

AEON_DECLARE_LOG_FAMILY(VulkanShaderManager, ETerminalColor::BlueViolet,
						ELogSourceInfo::FileName | ELogSourceInfo::FuncName | ELogSourceInfo::LineNum,
						ELogDateTimeInfo::None);
namespace Aeon
{
	SVulkanShaderManager::SVulkanShaderManager(ConstructorAccess)
	{
	}

	bool SVulkanShaderManager::Initialize()
	{
		// TODO This is the default, load value from config file
		uint32 ShaderWorkerCount = AeonMath::Clamp(SThread::SystemThreadCount() - 2, 1_u32, SVulkanShaderCompiler::SupportedWorkerCount());

		if (!AeonMath::IsClamped(ShaderWorkerCount, 1_u32, SVulkanShaderCompiler::SupportedWorkerCount()))
		{
			AEON_LOG(Error, VulkanShaderManager, "Failed to construct shader compiler with worker count={}", ShaderWorkerCount);
			return false;
		}

		ShaderCompiler.Construct(ShaderWorkerCount);

		SPath ShaderDirPath = SEnginePaths::GetShaderDirectory();
		SPath BinaryDirPath = ShaderDirPath / PATHSTR("Binaries");

		if (!ShaderDirPath.Exists())
		{
			// FIXME use SPlatformStringView once AEON_LOG supports it
			AEON_LOG(Error, VulkanShaderManager, "Failed to find shader directory at '{}'", ShaderDirPath.AsNative().string<char>().c_str());
			return false;
		}

		ShaderDir = SDirectory::At(ShaderDirPath); // FIXME figure out how to check if we have read/write permissions
		BinaryDir = SDirectory::Create(BinaryDirPath);

		return true;
	}

	void SVulkanShaderManager::Terminate()
	{
		// Tell the compiler workers to finish up and terminate the instance
		ShaderCompiler.Destruct();

		// Reload/update this last batch of compiled shaders, just to avoid re-doing work.
		ReloadFinishedCompilingShaders();

		PendingRecompileShaderArray.Empty();
		RecompilingShaderArray.Empty();

		ShaderArray.Empty();

		ShaderDir.Reset();
		BinaryDir.Reset();
	}

	GArray<GSharedPtr<SVulkanShader>> SVulkanShaderManager::QueryShaderList() const
	{
		return ShaderArray;
	}

	void SVulkanShaderManager::RefreshShaderList()
	{
		AEON_ASSERT(ShaderDir.IsValid());

		for (auto DirEntry : ShaderDir)
		{
			if (auto ShaderPath = DirEntry.AsPath())
			{
				auto ShaderExt = ShaderPath.GetLeafExtension();

				// TODO update logic to detect other shader types

				EVulkanShaderType ShaderType = EVulkanShaderType::Unknown;
				if (ShaderExt == PATHSTR(".comp") || ShaderExt == PATHSTR(".csh"))
				{
					ShaderType = EVulkanShaderType::Compute;
				}
				else if (ShaderExt == PATHSTR(".vert") || ShaderExt == PATHSTR(".vsh"))
				{
					ShaderType = EVulkanShaderType::Vertex;
				}
				else if (ShaderExt == PATHSTR(".tesc") || ShaderExt == PATHSTR(".tcsh"))
				{
					ShaderType = EVulkanShaderType::TessellationCtrl;
				}
				else if (ShaderExt == PATHSTR(".tese") || ShaderExt == PATHSTR(".tesh"))
				{
					ShaderType = EVulkanShaderType::TessellationEval;
				}
				else if (ShaderExt == PATHSTR(".geom") || ShaderExt == PATHSTR(".gsh"))
				{
					ShaderType = EVulkanShaderType::Geometry;
				}
				else if (ShaderExt == PATHSTR(".frag") || ShaderExt == PATHSTR(".fsh"))
				{
					ShaderType = EVulkanShaderType::Fragment;
				}
				else
				{
					continue;
				}

				auto BinaryPath = BinaryDir.Path() / ShaderPath.GetLeafName();
				BinaryPath.Append(PATHSTR(".aespv"));

				auto Shader = MakeSharedPtr<SVulkanShader>(SVulkanShader::ConstructorAccess::Tag, ShaderPath, BinaryPath, ShaderType);

				ShaderArray.AddUnique(Move(Shader), [&Shader] (GSharedPtr<SVulkanShader> const& ExistingShader)
													{ return Shader->GetName() == ExistingShader->GetName(); });
			}
		}
	}

	void SVulkanShaderManager::ReloadShaders(bool ReloadAllShaders)
	{
		for (auto& Shader : ShaderArray)
		{
			bool NeedsRecompile = false;
			if (!Shader->HasLoadedBinary())
			{
				if (Shader->ReloadBinary())
				{
					NeedsRecompile = DoesShaderNeedRecompile_Internal(Shader);
					AEON_LOG(Info, VulkanShaderManager, "Loaded binary for shader '{}'. Needs recompile={}", Shader->GetName(), NeedsRecompile);
				}
				else if (Shader->LoadSource())
				{
					Shader->RefreshMetadata();
					NeedsRecompile = true;
					AEON_LOG(Info, VulkanShaderManager, "Loaded source for shader '{}'. Needs compile", Shader->GetName());
				}
				else
				{
					AEON_LOG(Error, VulkanShaderManager, "Failed to load binary and/or source data for shader '{}'", Shader->GetName());
				}
			}
			else
			{
				if (ReloadAllShaders && Shader->ReloadBinary())
				{
					NeedsRecompile = DoesShaderNeedRecompile_Internal(Shader);
					AEON_LOG(Info, VulkanShaderManager, "Reloaded binary for shader '{}'. Needs recompile={}", Shader->GetName(), NeedsRecompile);
				}
				else
				{
					AEON_LOG(Error, VulkanShaderManager, "Failed to reload binary data for shader '{}'", Shader->GetName());
				}
			}

			if (NeedsRecompile)
			{
				PendingRecompileShaderArray.Add(Shader);
			}
		}
	}

	void SVulkanShaderManager::CompileShaders()
	{
		for (auto& Shader : PendingRecompileShaderArray)
		{
			Shader->MarkUpdatingBinary(true);

			RecompilingShaderArray.Add(ShaderCompiler->RequestCompile(Shader));
		}

		PendingRecompileShaderArray.Empty();
	}

	void SVulkanShaderManager::RecompileAllShaders()
	{
		for (auto& Shader : ShaderArray)
		{
			Shader->MarkUpdatingBinary(true);

			RecompilingShaderArray.Add(ShaderCompiler->RequestCompile(Shader));
		}

		PendingRecompileShaderArray.Empty();
	}

	void SVulkanShaderManager::ReloadFinishedCompilingShaders()
	{
		GArray<GFuture<DShaderCompileResult>> StillRecompilingShaderArray{ TReserveStorage::Tag, RecompilingShaderArray.Count() };

		for (auto& CompileFuture : RecompilingShaderArray)
		{
			if (CompileFuture.HasResult())
			{
				DShaderCompileResult Result = CompileFuture.TakeResult();

				if (Result.CompileResult == EShaderCompileResult::Success)
				{
					AEON_LOG(Info, VulkanShaderManager, "Finished compiling shader '{}' [Warnings={} Errors={}]",
							 Result.Shader->GetName(), uint32(Result.NumWarnings), uint32(Result.NumErrors));

					if (Result.NumWarnings > 0)
					{
						AEON_LOG(Warning, VulkanShaderManager, "\tshaderc: {}",
								 Result.CompileMessage);
					}

					Result.Shader->UpdateBinary(Result.BinaryData.Buffer(), Result.BinaryData.Count());
				}
				else
				{
					AEON_LOG(Error, VulkanShaderManager, "Failed to compile shader '{}' with reason '{}' [Warnings={} Errors={}]",
							 Result.Shader->GetName(), MakeCompileResultString(Result.CompileResult),
							 uint32(Result.NumWarnings), uint32(Result.NumErrors));

					AEON_LOG(Error, VulkanShaderManager, "\tshaderc: {}",
							 Result.CompileMessage);

					Result.Shader->MarkUpdatingBinary(false);
				}

				Result.Shader->UnloadSource();
			}
			else
			{
				StillRecompilingShaderArray.Add(Move(CompileFuture));
			}
		}

		RecompilingShaderArray = Move(StillRecompilingShaderArray);
	}

	bool SVulkanShaderManager::NeedsCompileShaders() const
	{
		return !PendingRecompileShaderArray.IsEmpty();
	}

	bool SVulkanShaderManager::IsCompilingShaders() const
	{
		return !RecompilingShaderArray.IsEmpty();
	}

	bool SVulkanShaderManager::DoesShaderNeedRecompile_Internal(GSharedPtr<SVulkanShader>& Shader) const
	{
		if (!Shader->IsTimestampUpToDate())
		{
			if (!Shader->LoadSource())
			{
				// Can't recompile if there's no source, so warn the user, and report the binary as up-to-date.
				AEON_LOG(Error, VulkanShaderManager, "Could not load source data for shader '{}'!", Shader->GetName());
				return false;
			}

			if (!Shader->IsHashUpToDate())
			{
				// NOTE We want to keep the source loaded in this case, so just return immediately
				return true;
			}

			// Only the timestamp was outdated, so update it here to avoid loading the source spuriously on subsequent queries.
			Shader->UpdateBinaryMetadata();

			Shader->UnloadSource();
		}

		return false;
	}
}
