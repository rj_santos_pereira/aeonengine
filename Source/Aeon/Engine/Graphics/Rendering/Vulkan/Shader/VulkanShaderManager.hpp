#ifndef ZZINTERNAL_INCLUDE_AEON_ENGINE_GRAPHICS_RENDERING_VULKAN_SHADER_VULKANSHADERMANAGER
#define ZZINTERNAL_INCLUDE_AEON_ENGINE_GRAPHICS_RENDERING_VULKAN_SHADER_VULKANSHADERMANAGER

#include "Aeon/Framework/Framework.hpp"

#include "Aeon/Engine/Graphics/Rendering/Vulkan/Shader/VulkanShader.hpp"
#include "Aeon/Engine/Graphics/Rendering/Vulkan/Shader/VulkanShaderCompiler.hpp"

namespace Aeon
{
	class SVulkanShaderManager
	{
	public:
		AEON_DECLARE_ACCESS_CLASS(ConstructorAccess, class SVulkanRenderer);

		explicit SVulkanShaderManager(ConstructorAccess);

		bool Initialize();

		void Terminate();

		GArray<GSharedPtr<SVulkanShader>> QueryShaderList() const;
	//	GSharedPtr<SVulkanShader> QueryShader(STextStringView ShaderName) const; // TODO implement this once shaders are stored in a map

		void RefreshShaderList();

		void ReloadShaders(bool ReloadAllShaders = false);

		void CompileShaders();

		void RecompileAllShaders();

		void ReloadFinishedCompilingShaders();

		bool NeedsCompileShaders() const;

		bool IsCompilingShaders() const;

	private:
		bool DoesShaderNeedRecompile_Internal(GSharedPtr<SVulkanShader>& Shader) const;

		GStorage<SVulkanShaderCompiler> ShaderCompiler;

		// FIXME make this a hashmap instead, String (name) -> Shader
		GArray<GSharedPtr<SVulkanShader>> ShaderArray;

		GArray<GSharedPtr<SVulkanShader>> PendingRecompileShaderArray;
		GArray<GFuture<DShaderCompileResult>> RecompilingShaderArray;

		SDirectory ShaderDir;
		SDirectory BinaryDir;
	};
}

#endif