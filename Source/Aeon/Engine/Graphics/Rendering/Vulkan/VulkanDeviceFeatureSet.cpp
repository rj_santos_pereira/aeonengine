#include "Aeon/Engine/Graphics/Rendering/Vulkan/VulkanDeviceFeatureSet.hpp"

namespace Aeon
{
	namespace ZzInternal
	{
		class SVulkanDeviceFeatureData
		{
		public:
			SVulkanDeviceFeatureData();

			SVulkanDeviceFeatureData(SVulkanDeviceFeatureData const& Other);
			SVulkanDeviceFeatureData& operator=(SVulkanDeviceFeatureData const& Other);

			void CopyData(SVulkanDeviceFeatureData const& Other);

			VkPhysicalDeviceFeatures2 Features;
			VkPhysicalDeviceFeatures& Features1_0;
			VkPhysicalDeviceVulkan11Features Features1_1;
			VkPhysicalDeviceVulkan12Features Features1_2;
			VkPhysicalDeviceVulkan13Features Features1_3;
		};

		SVulkanDeviceFeatureData::SVulkanDeviceFeatureData()
			: Features{}
			, Features1_0{ Features.features }
			, Features1_1{}
			, Features1_2{}
			, Features1_3{}
		{
			Features.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
			Features1_1.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_1_FEATURES;
			Features1_2.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_FEATURES;
			Features1_3.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_3_FEATURES;

			Features.pNext = addressof(Features1_1);
			Features1_1.pNext = addressof(Features1_2);
			Features1_2.pNext = addressof(Features1_3);
			Features1_3.pNext = nullptr;
		}

		SVulkanDeviceFeatureData::SVulkanDeviceFeatureData(SVulkanDeviceFeatureData const& Other)
			: SVulkanDeviceFeatureData()
		{
			CopyData(Other);
		}
		SVulkanDeviceFeatureData& SVulkanDeviceFeatureData::operator=(SVulkanDeviceFeatureData const& Other)
		{
			CopyData(Other);
			return *this;
		}

		void SVulkanDeviceFeatureData::CopyData(SVulkanDeviceFeatureData const& Other)
		{
			AeonMemory::MemCopy(addressof(Features1_0),
								addressof(Other.Features1_0),
								sizeof(VkPhysicalDeviceFeatures));
			AeonMemory::MemCopy(addressof(Features1_1.storageBuffer16BitAccess),
								addressof(Other.Features1_1.storageBuffer16BitAccess),
								sizeof(VkPhysicalDeviceVulkan11Features) - offsetof(VkPhysicalDeviceVulkan11Features, storageBuffer16BitAccess));
			AeonMemory::MemCopy(addressof(Features1_2.samplerMirrorClampToEdge),
								addressof(Other.Features1_2.samplerMirrorClampToEdge),
								sizeof(VkPhysicalDeviceVulkan12Features) - offsetof(VkPhysicalDeviceVulkan12Features, samplerMirrorClampToEdge));
			AeonMemory::MemCopy(addressof(Features1_3.robustImageAccess),
								addressof(Other.Features1_3.robustImageAccess),
								sizeof(VkPhysicalDeviceVulkan13Features) - offsetof(VkPhysicalDeviceVulkan13Features, robustImageAccess));
		}
	}

	SVulkanDeviceFeatureSet::SVulkanDeviceFeatureSet()
		: Data{ MakeUniquePtr<ZzInternal::SVulkanDeviceFeatureData>() }
	{
	}

	SVulkanDeviceFeatureSet::SVulkanDeviceFeatureSet(SVulkanDeviceFeatureSet const& Other)
		: Data{ MakeUniquePtr<ZzInternal::SVulkanDeviceFeatureData>(*Other.Data) }
	{
	}
	SVulkanDeviceFeatureSet::SVulkanDeviceFeatureSet(SVulkanDeviceFeatureSet&& Other)
		: Data{ Move(Other.Data) }
	{
	}

	SVulkanDeviceFeatureSet& SVulkanDeviceFeatureSet::operator=(SVulkanDeviceFeatureSet const& Other)
	{
		*Data = *Other.Data;
		return *this;
	}
	SVulkanDeviceFeatureSet& SVulkanDeviceFeatureSet::operator=(SVulkanDeviceFeatureSet&& Other)
	{
		Data = Move(Other.Data);
		return *this;
	}

	SVulkanDeviceFeatureSet::~SVulkanDeviceFeatureSet()
	{
	}

#pragma region Feature Getters/Setters
	bool SVulkanDeviceFeatureSet::GetBoundsCheckedBufferAccessSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.robustBufferAccess;
	}
	void SVulkanDeviceFeatureSet::SetBoundsCheckedBufferAccessSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.robustBufferAccess = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetFullInt32RangeIndexSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.fullDrawIndexUint32;
	}
	void SVulkanDeviceFeatureSet::SetFullInt32RangeIndexSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.fullDrawIndexUint32 = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetCubeArrayImageViewSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.imageCubeArray;
	}
	void SVulkanDeviceFeatureSet::SetCubeArrayImageViewSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.imageCubeArray = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetIndependentBlendSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.independentBlend;
	}
	void SVulkanDeviceFeatureSet::SetIndependentBlendSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.independentBlend = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetGeometryShaderSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.geometryShader;
	}
	void SVulkanDeviceFeatureSet::SetGeometryShaderSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.geometryShader = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetTessellationShaderSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.tessellationShader;
	}
	void SVulkanDeviceFeatureSet::SetTessellationShaderSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.tessellationShader = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetSampleShadingSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.sampleRateShading;
	}
	void SVulkanDeviceFeatureSet::SetSampleShadingSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.sampleRateShading = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetDualSourceBlendSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.dualSrcBlend;
	}
	void SVulkanDeviceFeatureSet::SetDualSourceBlendSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.dualSrcBlend = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetLogicOperationBlendSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.logicOp;
	}
	void SVulkanDeviceFeatureSet::SetLogicOperationBlendSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.logicOp = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetMultiDrawIndirectSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.multiDrawIndirect;
	}
	void SVulkanDeviceFeatureSet::SetMultiDrawIndirectSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.multiDrawIndirect = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetDrawIndirectFirstInstanceControlSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.drawIndirectFirstInstance;
	}
	void SVulkanDeviceFeatureSet::SetDrawIndirectFirstInstanceControlSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.drawIndirectFirstInstance = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetDepthClampingSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.depthClamp;
	}
	void SVulkanDeviceFeatureSet::SetDepthClampingSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.depthClamp = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetDepthBiasClampingSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.depthBiasClamp;
	}
	void SVulkanDeviceFeatureSet::SetDepthBiasClampingSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.depthBiasClamp = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetWireframeFillModeSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.fillModeNonSolid;
	}
	void SVulkanDeviceFeatureSet::SetWireframeFillModeSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.fillModeNonSolid = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetDepthBoundsTestsSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.depthBounds;
	}
	void SVulkanDeviceFeatureSet::SetDepthBoundsTestsSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.depthBounds = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetLineWidthControlSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.wideLines;
	}
	void SVulkanDeviceFeatureSet::SetLineWidthControlSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.wideLines = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetPointSizeControlSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.largePoints;
	}
	void SVulkanDeviceFeatureSet::SetPointSizeControlSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.largePoints = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetAlphaToOneSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.alphaToOne;
	}
	void SVulkanDeviceFeatureSet::SetAlphaToOneSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.alphaToOne = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetMultiViewportSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.multiViewport;
	}
	void SVulkanDeviceFeatureSet::SetMultiViewportSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.multiViewport = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetAnisotropicFilteringSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.samplerAnisotropy;
	}
	void SVulkanDeviceFeatureSet::SetAnisotropicFilteringSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.samplerAnisotropy = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetETC2TextureCompressionSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.textureCompressionETC2;
	}
	void SVulkanDeviceFeatureSet::SetETC2TextureCompressionSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.textureCompressionETC2 = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetASTC_LDRTextureCompressionSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.textureCompressionASTC_LDR;
	}
	void SVulkanDeviceFeatureSet::SetASTC_LDRTextureCompressionSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.textureCompressionASTC_LDR = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetBCTextureCompressionSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.textureCompressionBC;
	}
	void SVulkanDeviceFeatureSet::SetBCTextureCompressionSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.textureCompressionBC = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetPreciseOcclusionQuerySupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.occlusionQueryPrecise;
	}
	void SVulkanDeviceFeatureSet::SetPreciseOcclusionQuerySupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.occlusionQueryPrecise = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetPipelineStatisticsQuerySupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.pipelineStatisticsQuery;
	}
	void SVulkanDeviceFeatureSet::SetPipelineStatisticsQuerySupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.pipelineStatisticsQuery = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetPrimitiveStorageBufferAndImageStoreAndAtomicOpsSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.vertexPipelineStoresAndAtomics;
	}
	void SVulkanDeviceFeatureSet::SetPrimitiveStorageBufferAndImageStoreAndAtomicOpsSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.vertexPipelineStoresAndAtomics = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetFragmentStorageBufferAndImageStoreAndAtomicOpsSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.fragmentStoresAndAtomics;
	}
	void SVulkanDeviceFeatureSet::SetFragmentStorageBufferAndImageStoreAndAtomicOpsSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.fragmentStoresAndAtomics = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetTessellationAndGeometryPointSizeControlSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.shaderTessellationAndGeometryPointSize;
	}
	void SVulkanDeviceFeatureSet::SetTessellationAndGeometryPointSizeControlSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.shaderTessellationAndGeometryPointSize = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderImageGatherExtendedOpsSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.shaderImageGatherExtended;
	}
	void SVulkanDeviceFeatureSet::SetShaderImageGatherExtendedOpsSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.shaderImageGatherExtended = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetImageExtendedFormatSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.shaderStorageImageExtendedFormats;
	}
	void SVulkanDeviceFeatureSet::SetImageExtendedFormatSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.shaderStorageImageExtendedFormats = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetImageMultisamplingSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.shaderStorageImageMultisample;
	}
	void SVulkanDeviceFeatureSet::SetImageMultisamplingSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.shaderStorageImageMultisample = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderImageReadWithoutFormatSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.shaderStorageImageReadWithoutFormat;
	}
	void SVulkanDeviceFeatureSet::SetShaderImageReadWithoutFormatSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.shaderStorageImageReadWithoutFormat = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderImageWriteWithoutFormatSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.shaderStorageImageWriteWithoutFormat;
	}
	void SVulkanDeviceFeatureSet::SetShaderImageWriteWithoutFormatSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.shaderStorageImageWriteWithoutFormat = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetUniformBufferArrayDynamicIndexingSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.shaderUniformBufferArrayDynamicIndexing;
	}
	void SVulkanDeviceFeatureSet::SetUniformBufferArrayDynamicIndexingSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.shaderUniformBufferArrayDynamicIndexing = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetSamplerArrayDynamicIndexingSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.shaderSampledImageArrayDynamicIndexing;
	}
	void SVulkanDeviceFeatureSet::SetSamplerArrayDynamicIndexingSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.shaderSampledImageArrayDynamicIndexing = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetStorageBufferArrayDynamicIndexingSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.shaderStorageBufferArrayDynamicIndexing;
	}
	void SVulkanDeviceFeatureSet::SetStorageBufferArrayDynamicIndexingSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.shaderStorageBufferArrayDynamicIndexing = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetImageArrayDynamicIndexingSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.shaderStorageImageArrayDynamicIndexing;
	}
	void SVulkanDeviceFeatureSet::SetImageArrayDynamicIndexingSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.shaderStorageImageArrayDynamicIndexing = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderClipDistanceSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.shaderClipDistance;
	}
	void SVulkanDeviceFeatureSet::SetShaderClipDistanceSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.shaderClipDistance = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderCullDistanceSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.shaderCullDistance;
	}
	void SVulkanDeviceFeatureSet::SetShaderCullDistanceSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.shaderCullDistance = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderFloat64Support() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.shaderFloat64;
	}
	void SVulkanDeviceFeatureSet::SetShaderFloat64Support(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.shaderFloat64 = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderInt64Support() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.shaderInt64;
	}
	void SVulkanDeviceFeatureSet::SetShaderInt64Support(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.shaderInt64 = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderInt16Support() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.shaderInt16;
	}
	void SVulkanDeviceFeatureSet::SetShaderInt16Support(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.shaderInt16 = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderResourceResidencyOpsSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.shaderResourceResidency;
	}
	void SVulkanDeviceFeatureSet::SetShaderResourceResidencyOpsSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.shaderResourceResidency = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderResourceMinimumLODSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.shaderResourceMinLod;
	}
	void SVulkanDeviceFeatureSet::SetShaderResourceMinimumLODSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.shaderResourceMinLod = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetSparseBindingSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.sparseBinding;
	}
	void SVulkanDeviceFeatureSet::SetSparseBindingSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.sparseBinding = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetSparseResidencyBufferAccessSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.sparseResidencyBuffer;
	}
	void SVulkanDeviceFeatureSet::SetSparseResidencyBufferAccessSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.sparseResidencyBuffer = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetSparseResidencyImage2DAccessSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.sparseResidencyImage2D;
	}
	void SVulkanDeviceFeatureSet::SetSparseResidencyImage2DAccessSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.sparseResidencyImage2D = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetSparseResidencyImage3DAccessSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.sparseResidencyImage3D;
	}
	void SVulkanDeviceFeatureSet::SetSparseResidencyImage3DAccessSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.sparseResidencyImage3D = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetSparseResidency2SPPImage2DAccessSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.sparseResidency2Samples;
	}
	void SVulkanDeviceFeatureSet::SetSparseResidency2SPPImage2DAccessSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.sparseResidency2Samples = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetSparseResidency4SPPImage2DAccessSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.sparseResidency4Samples;
	}
	void SVulkanDeviceFeatureSet::SetSparseResidency4SPPImage2DAccessSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.sparseResidency4Samples = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetSparseResidency8SPPImage2DAccessSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.sparseResidency8Samples;
	}
	void SVulkanDeviceFeatureSet::SetSparseResidency8SPPImage2DAccessSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.sparseResidency8Samples = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetSparseResidency16SPPImage2DAccessSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.sparseResidency16Samples;
	}
	void SVulkanDeviceFeatureSet::SetSparseResidency16SPPImage2DAccessSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.sparseResidency16Samples = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetSparseResidencyAliasedAccessSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.sparseResidencyAliased;
	}
	void SVulkanDeviceFeatureSet::SetSparseResidencyAliasedAccessSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.sparseResidencyAliased = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetVariableMultisampleRateSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.variableMultisampleRate;
	}
	void SVulkanDeviceFeatureSet::SetVariableMultisampleRateSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.variableMultisampleRate = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetInheritedQuerySupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_0.inheritedQueries;
	}
	void SVulkanDeviceFeatureSet::SetInheritedQuerySupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_0.inheritedQueries = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderIntAndFloat16StorageBufferMemberSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_1.storageBuffer16BitAccess;
	}
	void SVulkanDeviceFeatureSet::SetShaderIntAndFloat16StorageBufferMemberSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_1.storageBuffer16BitAccess = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderIntAndFloat16UniformAndStorageBufferMemberSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_1.uniformAndStorageBuffer16BitAccess;
	}
	void SVulkanDeviceFeatureSet::SetShaderIntAndFloat16UniformAndStorageBufferMemberSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_1.uniformAndStorageBuffer16BitAccess = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderIntAndFloat16PushConstantMemberSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_1.storagePushConstant16;
	}
	void SVulkanDeviceFeatureSet::SetShaderIntAndFloat16PushConstantMemberSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_1.storagePushConstant16 = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderIntAndFloat16InputOutputMemberSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_1.storageInputOutput16;
	}
	void SVulkanDeviceFeatureSet::SetShaderIntAndFloat16InputOutputMemberSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_1.storageInputOutput16 = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetMultiviewRenderingSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_1.multiview;
	}
	void SVulkanDeviceFeatureSet::SetMultiviewRenderingSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_1.multiview = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetMultiviewRenderingWithGeometryShaderSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_1.multiviewGeometryShader;
	}
	void SVulkanDeviceFeatureSet::SetMultiviewRenderingWithGeometryShaderSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_1.multiviewGeometryShader = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetMultiviewRenderingWithTessellationShaderSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_1.multiviewTessellationShader;
	}
	void SVulkanDeviceFeatureSet::SetMultiviewRenderingWithTessellationShaderSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_1.multiviewTessellationShader = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderStorageBufferVariablePointerSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_1.variablePointersStorageBuffer;
	}
	void SVulkanDeviceFeatureSet::SetShaderStorageBufferVariablePointerSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_1.variablePointersStorageBuffer = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderVariablePointerSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_1.variablePointers;
	}
	void SVulkanDeviceFeatureSet::SetShaderVariablePointerSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_1.variablePointers = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetProtectedMemorySupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_1.protectedMemory;
	}
	void SVulkanDeviceFeatureSet::SetProtectedMemorySupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_1.protectedMemory = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetSamplerYCBCRConversionSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_1.samplerYcbcrConversion;
	}
	void SVulkanDeviceFeatureSet::SetSamplerYCBCRConversionSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_1.samplerYcbcrConversion = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderDrawParameterSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_1.shaderDrawParameters;
	}
	void SVulkanDeviceFeatureSet::SetShaderDrawParameterSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_1.shaderDrawParameters = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetSamplerMirrorClampToEdgeSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.samplerMirrorClampToEdge;
	}
	void SVulkanDeviceFeatureSet::SetSamplerMirrorClampToEdgeSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.samplerMirrorClampToEdge = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetDrawIndirectCountSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.drawIndirectCount;
	}
	void SVulkanDeviceFeatureSet::SetDrawIndirectCountSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.drawIndirectCount = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderInt8StorageBufferMemberSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.storageBuffer8BitAccess;
	}
	void SVulkanDeviceFeatureSet::SetShaderInt8StorageBufferMemberSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.storageBuffer8BitAccess = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderInt8UniformAndStorageBufferMemberSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.uniformAndStorageBuffer8BitAccess;
	}
	void SVulkanDeviceFeatureSet::SetShaderInt8UniformAndStorageBufferMemberSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.uniformAndStorageBuffer8BitAccess = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderInt8PushConstantMemberSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.storagePushConstant8;
	}
	void SVulkanDeviceFeatureSet::SetShaderInt8PushConstantMemberSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.storagePushConstant8 = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderBufferInt64AtomicOpsSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.shaderBufferInt64Atomics;
	}
	void SVulkanDeviceFeatureSet::SetShaderBufferInt64AtomicOpsSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.shaderBufferInt64Atomics = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderSharedInt64AtomicOpsSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.shaderSharedInt64Atomics;
	}
	void SVulkanDeviceFeatureSet::SetShaderSharedInt64AtomicOpsSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.shaderSharedInt64Atomics = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderFloat16Support() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.shaderFloat16;
	}
	void SVulkanDeviceFeatureSet::SetShaderFloat16Support(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.shaderFloat16 = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderInt8Support() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.shaderInt8;
	}
	void SVulkanDeviceFeatureSet::SetShaderInt8Support(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.shaderInt8 = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetDescriptorIndexingSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.descriptorIndexing;
	}
	void SVulkanDeviceFeatureSet::SetDescriptorIndexingSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.descriptorIndexing = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderInputAttachmentArrayDynamicIndexingSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.shaderInputAttachmentArrayDynamicIndexing;
	}
	void SVulkanDeviceFeatureSet::SetShaderInputAttachmentArrayDynamicIndexingSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.shaderInputAttachmentArrayDynamicIndexing = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderSamplerBufferArrayDynamicIndexingSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.shaderUniformTexelBufferArrayDynamicIndexing;
	}
	void SVulkanDeviceFeatureSet::SetShaderSamplerBufferArrayDynamicIndexingSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.shaderUniformTexelBufferArrayDynamicIndexing = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderImageBufferArrayDynamicIndexingSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.shaderStorageTexelBufferArrayDynamicIndexing;
	}
	void SVulkanDeviceFeatureSet::SetShaderImageBufferArrayDynamicIndexingSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.shaderStorageTexelBufferArrayDynamicIndexing = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderUniformBufferArrayNonUniformIndexingSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.shaderUniformBufferArrayNonUniformIndexing;
	}
	void SVulkanDeviceFeatureSet::SetShaderUniformBufferArrayNonUniformIndexingSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.shaderUniformBufferArrayNonUniformIndexing = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderSamplerArrayNonUniformIndexingSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.shaderSampledImageArrayNonUniformIndexing;
	}
	void SVulkanDeviceFeatureSet::SetShaderSamplerArrayNonUniformIndexingSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.shaderSampledImageArrayNonUniformIndexing = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderStorageBufferArrayNonUniformIndexingSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.shaderStorageBufferArrayNonUniformIndexing;
	}
	void SVulkanDeviceFeatureSet::SetShaderStorageBufferArrayNonUniformIndexingSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.shaderStorageBufferArrayNonUniformIndexing = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderImageArrayNonUniformIndexingSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.shaderStorageImageArrayNonUniformIndexing;
	}
	void SVulkanDeviceFeatureSet::SetShaderImageArrayNonUniformIndexingSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.shaderStorageImageArrayNonUniformIndexing = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderInputAttachmentArrayNonUniformIndexingSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.shaderInputAttachmentArrayNonUniformIndexing;
	}
	void SVulkanDeviceFeatureSet::SetShaderInputAttachmentArrayNonUniformIndexingSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.shaderInputAttachmentArrayNonUniformIndexing = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderSamplerBufferArrayNonUniformIndexingSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.shaderUniformTexelBufferArrayNonUniformIndexing;
	}
	void SVulkanDeviceFeatureSet::SetShaderSamplerBufferArrayNonUniformIndexingSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.shaderUniformTexelBufferArrayNonUniformIndexing = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderImageBufferArrayNonUniformIndexingSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.shaderStorageTexelBufferArrayNonUniformIndexing;
	}
	void SVulkanDeviceFeatureSet::SetShaderImageBufferArrayNonUniformIndexingSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.shaderStorageTexelBufferArrayNonUniformIndexing = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetUpdateBoundUniformBufferDescriptorSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.descriptorBindingUniformBufferUpdateAfterBind;
	}
	void SVulkanDeviceFeatureSet::SetUpdateBoundUniformBufferDescriptorSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.descriptorBindingUniformBufferUpdateAfterBind = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetUpdateBoundSamplerDescriptorSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.descriptorBindingSampledImageUpdateAfterBind;
	}
	void SVulkanDeviceFeatureSet::SetUpdateBoundSamplerDescriptorSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.descriptorBindingSampledImageUpdateAfterBind = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetUpdateBoundImageDescriptorSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.descriptorBindingStorageImageUpdateAfterBind;
	}
	void SVulkanDeviceFeatureSet::SetUpdateBoundImageDescriptorSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.descriptorBindingStorageImageUpdateAfterBind = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetUpdateBoundStorageBufferDescriptorSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.descriptorBindingStorageBufferUpdateAfterBind;
	}
	void SVulkanDeviceFeatureSet::SetUpdateBoundStorageBufferDescriptorSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.descriptorBindingStorageBufferUpdateAfterBind = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetUpdateBoundSamplerBufferDescriptorSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.descriptorBindingUniformTexelBufferUpdateAfterBind;
	}
	void SVulkanDeviceFeatureSet::SetUpdateBoundSamplerBufferDescriptorSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.descriptorBindingUniformTexelBufferUpdateAfterBind = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetUpdateBoundImageBufferDescriptorSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.descriptorBindingStorageTexelBufferUpdateAfterBind;
	}
	void SVulkanDeviceFeatureSet::SetUpdateBoundImageBufferDescriptorSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.descriptorBindingStorageTexelBufferUpdateAfterBind = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetUpdateBoundDescriptorPendingExecutionSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.descriptorBindingUpdateUnusedWhilePending;
	}
	void SVulkanDeviceFeatureSet::SetUpdateBoundDescriptorPendingExecutionSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.descriptorBindingUpdateUnusedWhilePending = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetDescriptorBindingPartiallyBoundSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.descriptorBindingPartiallyBound;
	}
	void SVulkanDeviceFeatureSet::SetDescriptorBindingPartiallyBoundSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.descriptorBindingPartiallyBound = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetDescriptorBindingVariableCountSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.descriptorBindingVariableDescriptorCount;
	}
	void SVulkanDeviceFeatureSet::SetDescriptorBindingVariableCountSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.descriptorBindingVariableDescriptorCount = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetRuntimeDescriptorArraySupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.runtimeDescriptorArray;
	}
	void SVulkanDeviceFeatureSet::SetRuntimeDescriptorArraySupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.runtimeDescriptorArray = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetSamplerMinMaxFilteringSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.samplerFilterMinmax;
	}
	void SVulkanDeviceFeatureSet::SetSamplerMinMaxFilteringSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.samplerFilterMinmax = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetResourceScalarAlignmentSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.scalarBlockLayout;
	}
	void SVulkanDeviceFeatureSet::SetResourceScalarAlignmentSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.scalarBlockLayout = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetImagelessFramebufferSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.imagelessFramebuffer;
	}
	void SVulkanDeviceFeatureSet::SetImagelessFramebufferSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.imagelessFramebuffer = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderUniformStandardLayoutSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.uniformBufferStandardLayout;
	}
	void SVulkanDeviceFeatureSet::SetShaderUniformStandardLayoutSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.uniformBufferStandardLayout = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderSubgroupExtendedTypesSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.shaderSubgroupExtendedTypes;
	}
	void SVulkanDeviceFeatureSet::SetShaderSubgroupExtendedTypesSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.shaderSubgroupExtendedTypes = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetSeparateDepthStencilLayoutsSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.separateDepthStencilLayouts;
	}
	void SVulkanDeviceFeatureSet::SetSeparateDepthStencilLayoutsSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.separateDepthStencilLayouts = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetHostQueryResetSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.hostQueryReset;
	}
	void SVulkanDeviceFeatureSet::SetHostQueryResetSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.hostQueryReset = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetTimelineSemaphoreSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.timelineSemaphore;
	}
	void SVulkanDeviceFeatureSet::SetTimelineSemaphoreSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.timelineSemaphore = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetBufferDeviceAddressQuerySupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.bufferDeviceAddress;
	}
	void SVulkanDeviceFeatureSet::SetBufferDeviceAddressQuerySupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.bufferDeviceAddress = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetBufferDeviceAddressCaptureReplaySupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.bufferDeviceAddressCaptureReplay;
	}
	void SVulkanDeviceFeatureSet::SetBufferDeviceAddressCaptureReplaySupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.bufferDeviceAddressCaptureReplay = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetBufferDeviceAddressMultiDeviceSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.bufferDeviceAddressMultiDevice;
	}
	void SVulkanDeviceFeatureSet::SetBufferDeviceAddressMultiDeviceSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.bufferDeviceAddressMultiDevice = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderVulkanMemoryModelSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.vulkanMemoryModel;
	}
	void SVulkanDeviceFeatureSet::SetShaderVulkanMemoryModelSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.vulkanMemoryModel = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderVulkanMemoryModelDeviceScopeSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.vulkanMemoryModelDeviceScope;
	}
	void SVulkanDeviceFeatureSet::SetShaderVulkanMemoryModelDeviceScopeSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.vulkanMemoryModelDeviceScope = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderVulkanMemoryModelAvailabilityVisibilityChainsSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.vulkanMemoryModelAvailabilityVisibilityChains;
	}
	void SVulkanDeviceFeatureSet::SetShaderVulkanMemoryModelAvailabilityVisibilityChainsSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.vulkanMemoryModelAvailabilityVisibilityChains = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderOutputViewportIndexSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.shaderOutputViewportIndex;
	}
	void SVulkanDeviceFeatureSet::SetShaderOutputViewportIndexSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.shaderOutputViewportIndex = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderOutputLayerSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.shaderOutputViewportIndex;
	}
	void SVulkanDeviceFeatureSet::SetShaderOutputLayerSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.shaderOutputViewportIndex = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderSubgroupBroadcastWithDynamicIdSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_2.subgroupBroadcastDynamicId;
	}
	void SVulkanDeviceFeatureSet::SetShaderSubgroupBroadcastWithDynamicIdSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_2.subgroupBroadcastDynamicId = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetBoundsCheckedImageAccessSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_3.robustImageAccess;
	}
	void SVulkanDeviceFeatureSet::SetBoundsCheckedImageAccessSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_3.robustImageAccess = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetInlineUniformDataSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_3.inlineUniformBlock;
	}
	void SVulkanDeviceFeatureSet::SetInlineUniformDataSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_3.inlineUniformBlock = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetUpdateBoundInlineUniformDescriptorSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_3.descriptorBindingInlineUniformBlockUpdateAfterBind;
	}
	void SVulkanDeviceFeatureSet::SetUpdateBoundInlineUniformDescriptorSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_3.descriptorBindingInlineUniformBlockUpdateAfterBind = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetPipelineCreationCacheControlSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_3.pipelineCreationCacheControl;
	}
	void SVulkanDeviceFeatureSet::SetPipelineCreationCacheControlSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_3.pipelineCreationCacheControl = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetPrivateDataSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_3.privateData;
	}
	void SVulkanDeviceFeatureSet::SetPrivateDataSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_3.privateData = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderDemoteToHelperInvocationSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_3.shaderDemoteToHelperInvocation;
	}
	void SVulkanDeviceFeatureSet::SetShaderDemoteToHelperInvocationSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_3.shaderDemoteToHelperInvocation = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderTerminateInvocationSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_3.shaderTerminateInvocation;
	}
	void SVulkanDeviceFeatureSet::SetShaderTerminateInvocationSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_3.shaderTerminateInvocation = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetPipelineStageCreationSubgroupSizeControlSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_3.subgroupSizeControl;
	}
	void SVulkanDeviceFeatureSet::SetPipelineStageCreationSubgroupSizeControlSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_3.subgroupSizeControl = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetPipelineStageCreationRequireFullSubgroupSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_3.computeFullSubgroups;
	}
	void SVulkanDeviceFeatureSet::SetPipelineStageCreationRequireFullSubgroupSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_3.computeFullSubgroups = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetSynchronizationCommands2Support() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_3.synchronization2;
	}
	void SVulkanDeviceFeatureSet::SetSynchronizationCommands2Support(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_3.synchronization2 = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetASTC_HDRTextureCompressionSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_3.textureCompressionASTC_HDR;
	}
	void SVulkanDeviceFeatureSet::SetASTC_HDRTextureCompressionSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_3.textureCompressionASTC_HDR = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderZeroInitWorkgroupVariableSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_3.shaderZeroInitializeWorkgroupMemory;
	}
	void SVulkanDeviceFeatureSet::SetShaderZeroInitWorkgroupVariableSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_3.shaderZeroInitializeWorkgroupMemory = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetDynamicRenderingSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_3.dynamicRendering;
	}
	void SVulkanDeviceFeatureSet::SetDynamicRenderingSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_3.dynamicRendering = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetShaderIntDotProductOpSupport() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_3.shaderIntegerDotProduct;
	}
	void SVulkanDeviceFeatureSet::SetShaderIntDotProductOpSupport(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_3.shaderIntegerDotProduct = NewValue;
	};

	bool SVulkanDeviceFeatureSet::GetMaintenance4Support() const
	{
		AEON_ASSERT(Data);
		return Data->Features1_3.maintenance4;
	}
	void SVulkanDeviceFeatureSet::SetMaintenance4Support(bool NewValue)
	{
		AEON_ASSERT(Data);
		Data->Features1_3.maintenance4 = NewValue;
	}
#pragma endregion Feature Getters/Setters

	bool SVulkanDeviceFeatureSet::GetFeatureSupportByIndex(szint FeatureIndex) const
	{
		AEON_ASSERT(FeatureIndex < FeatureCount());
		return (this->*FeatureGetterArray[FeatureIndex])();
	}
	void SVulkanDeviceFeatureSet::SetFeatureSupportByIndex(szint FeatureIndex, bool NewValue)
	{
		AEON_ASSERT(FeatureIndex < FeatureCount());
		(this->*FeatureSetterArray[FeatureIndex])(NewValue);
	}

	bool SVulkanDeviceFeatureSet::CheckFeatureSupport(SVulkanDeviceFeatureSet const& Collection,
															 GFunction<void(SStringView, szint)> MissingFeatureCallback) const
	{
		bool SupportsFeatures = true;
		for (szint Index = 0; Index < FeatureCount(); ++Index)
		{
			if (GetFeatureSupportByIndex(Index) == false && Collection.GetFeatureSupportByIndex(Index) == true)
			{
				MissingFeatureCallback(FeatureName(Index), Index);
				SupportsFeatures = false;
			}
		}
		return SupportsFeatures;
	}
	
	bool SVulkanDeviceFeatureSet::IsSupersetOf(SVulkanDeviceFeatureSet const& Other) const
	{
		for (szint Index = 0; Index < FeatureCount(); ++Index)
		{
			if (GetFeatureSupportByIndex(Index) == false && Other.GetFeatureSupportByIndex(Index) == true)
			{
				return false;
			}
		}
		return true;
	}
	bool SVulkanDeviceFeatureSet::IsSubsetOf(SVulkanDeviceFeatureSet const& Other) const
	{
		return Other.IsSupersetOf(*this);
	}

	VkPhysicalDeviceFeatures2 const* SVulkanDeviceFeatureSet::VulkanData() const
	{
		AEON_ASSERT(Data);
		return addressof(Data->Features);
	}
	VkPhysicalDeviceFeatures2* SVulkanDeviceFeatureSet::VulkanData()
	{
		return AEON_INVOKE_CONST_OVERLOAD(VulkanData);
	}

	constexpr SStringView SVulkanDeviceFeatureSet::FeatureName(szint FeatureIndex)
	{
		AEON_ASSERT(FeatureIndex < FeatureCount());
		return FeatureNameArray[FeatureIndex];
	}
	constexpr szint SVulkanDeviceFeatureSet::FeatureCount()
	{
		constexpr szint Count = TArrayDimensionSize<decltype(FeatureNameArray)>::Value;
		return Count;
	}
}
