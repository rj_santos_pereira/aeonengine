#ifndef ZZINTERNAL_INCLUDE_AEON_ENGINE_GRAPHICS_RENDERING_VULKAN_VULKANCOMMON
#define ZZINTERNAL_INCLUDE_AEON_ENGINE_GRAPHICS_RENDERING_VULKAN_VULKANCOMMON

#include "Aeon/Framework/Framework.hpp"

#define VK_ENABLE_BETA_EXTENSIONS
//#if AEON_PLATFORM_WINDOWS
//#	define VK_USE_PLATFORM_WIN32_KHR
//#endif
#include <vulkan/vulkan.hpp>

//#include "Aeon/Engine/Logging/LoggingUtility.hpp"

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  AEON_VULKAN_DYN_LOAD_FUNC, AEON_VULKAN_DYN_LOAD_DEVICE_FUNC

#define AEON_VULKAN_DYN_LOAD_FUNC(FuncName, InstHnd) \
	[] (VkInstance Instance)\
	{\
        auto FuncPtr = reinterpret_cast<AEON_TOKEN_CONCAT(PFN_, FuncName)>(vkGetInstanceProcAddr(Instance, AEON_TOKEN_STR(FuncName)));\
		if (!FuncPtr)\
        {\
			AEON_DEBUGGER_FORMAT_PRINT(Error, "Failed to load Vulkan function '{}'", AEON_TOKEN_STR(FuncName));\
        }\
        return FuncPtr;\
	}\
	(InstHnd)

#define AEON_VULKAN_DYN_LOAD_DEVICE_FUNC(FuncName, DevHnd) \
	[] (VkDevice Device)\
	{\
        auto FuncPtr = reinterpret_cast<AEON_TOKEN_CONCAT(PFN_, FuncName)>(vkGetDeviceProcAddr(Device, AEON_TOKEN_STR(FuncName)));\
		if (!FuncPtr)\
        {\
			AEON_DEBUGGER_FORMAT_PRINT(Error, "Failed to load Vulkan function '{}'", AEON_TOKEN_STR(FuncName));\
        }\
        return FuncPtr;\
	}\
	(DevHnd)

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  AEON_VULKAN_DYN_CALL_FUNC, AEON_VULKAN_DYN_CALL_DEVICE_FUNC

#define AEON_VULKAN_DYN_CALL_FUNC(FuncName, InstHnd, ...) \
	[] <class ... ArgTypes> (VkInstance Instance, ArgTypes&&... Args) -> VkResult\
	{\
		if (auto LoadedFuncPtr = AEON_VULKAN_DYN_LOAD_FUNC(FuncName, Instance))\
        {\
            if constexpr (::Aeon::VIsSameClass<typename ::Aeon::TBreakFunctionSignature<decltype(LoadedFuncPtr)>::ReturnType, VkResult>)\
            {\
                return LoadedFuncPtr(Instance, Forward<ArgTypes>(Args)...);\
            }\
            else\
            {\
				LoadedFuncPtr(Instance, Forward<ArgTypes>(Args)...);\
				return VK_SUCCESS;\
        	}\
		}\
		return VK_ERROR_EXTENSION_NOT_PRESENT;\
	}\
	(InstHnd __VA_OPT__(,) __VA_ARGS__)

#define AEON_VULKAN_DYN_CALL_DEVICE_FUNC(FuncName, DevHnd, ...) \
	[] <class ... ArgTypes> (VkDevice Device, ArgTypes&&... Args) -> VkResult\
	{\
		if (auto LoadedFuncPtr = AEON_VULKAN_DYN_LOAD_DEVICE_FUNC(FuncName, Device))\
        {\
            if constexpr (::Aeon::VIsSameClass<typename ::Aeon::TBreakFunctionSignature<decltype(LoadedFuncPtr)>::ReturnType, VkResult>)\
            {\
                return LoadedFuncPtr(Device, Forward<ArgTypes>(Args)...);\
            }\
            else\
            {\
				LoadedFuncPtr(Device, Forward<ArgTypes>(Args)...);\
				return VK_SUCCESS;\
        	}\
		}\
		return VK_ERROR_EXTENSION_NOT_PRESENT;\
	}\
	(DevHnd __VA_OPT__(,) __VA_ARGS__)

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  VulkanQueryData, VulkanQueryArrayData

	template <auto FuncValue,
			  class DataType = TRemovePointer<typename TBreakFunctionSignature<TDecayType<decltype(FuncValue)>>::ParametersPackType::Back>,
			  class ... ArgTypes>
	DataType VulkanQueryData(ArgTypes&& ... FuncArgs)
	requires CFunctionPointer<TDecayType<decltype(FuncValue)>> && CCallable<TDecayType<decltype(FuncValue)>, ArgTypes ..., DataType*>
	{
		DataType Data;

		FuncValue(Forward<ArgTypes>(FuncArgs) ..., addressof(Data));

		return Data;
	}

	template <auto FuncValue,
			  class DataType = TRemovePointer<typename TBreakFunctionSignature<TDecayType<decltype(FuncValue)>>::ParametersPackType::Back>,
			  class ... ArgTypes>
	GArray<DataType> VulkanQueryArrayData(ArgTypes&& ... FuncArgs)
	requires CFunctionPointer<TDecayType<decltype(FuncValue)>> && CCallable<TDecayType<decltype(FuncValue)>, ArgTypes ..., uint32*, DataType*>
	{
		GArray<DataType> DataArray{ TReserveStorage::Tag, 0 };

		uint32 DataCount = 0;
		FuncValue(Forward<ArgTypes>(FuncArgs) ..., addressof(DataCount), nullptr);

		if (DataCount != 0)
		{
			DataArray.Resize(DataCount);
			FuncValue(Forward<ArgTypes>(FuncArgs) ..., addressof(DataCount), DataArray.Buffer());
		}

		return DataArray;
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GEqualTo<VkExtensionProperties>, GNotEqualTo<VkExtensionProperties>

	template <>
	struct GEqualTo<VkExtensionProperties>
	{
		constexpr bool operator()(VkExtensionProperties const& Value1, VkExtensionProperties const& Value2)
		{
			return Aeon::StringOps::Compare(Value1.extensionName, Value2.extensionName) == 0;
		}
	};
	template <>
	struct GNotEqualTo<VkExtensionProperties>
	{
		constexpr bool operator()(VkExtensionProperties const& Value1, VkExtensionProperties const& Value2)
		{
			return Aeon::StringOps::Compare(Value1.extensionName, Value2.extensionName) != 0;
		}
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GEqualTo<VkLayerProperties>, GNotEqualTo<VkLayerProperties>

	template <>
	struct GEqualTo<VkLayerProperties>
	{
		constexpr bool operator()(VkLayerProperties const& Value1, VkLayerProperties const& Value2)
		{
			return Aeon::StringOps::Compare(Value1.layerName, Value2.layerName) == 0;
		}
	};
	template <>
	struct GNotEqualTo<VkLayerProperties>
	{
		constexpr bool operator()(VkLayerProperties const& Value1, VkLayerProperties const& Value2)
		{
			return Aeon::StringOps::Compare(Value1.layerName, Value2.layerName) != 0;
		}
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GDefaultObjectHasher<VkExtensionProperties>

	template <CUnsignedInteger HshType>
	struct GDefaultObjectHasher<VkExtensionProperties, HshType>
	{
		using ObjectType = VkExtensionProperties;
		using HashType = HshType;

		AEON_FORCEINLINE static HashType Hash(ObjectType const& Object)
		{
			return Aeon::FNV1aHash<HashType>(reinterpret_cast<cbyteptr>(Object.extensionName),
											 Aeon::StringOps::Length(Object.extensionName));
		}
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GDefaultObjectHasher<VkLayerProperties>

	template <CUnsignedInteger HshType>
	struct GDefaultObjectHasher<VkLayerProperties, HshType>
	{
		using ObjectType = VkLayerProperties;
		using HashType = HshType;

		AEON_FORCEINLINE static HashType Hash(ObjectType const& Object)
		{
			return Aeon::FNV1aHash<HashType>(reinterpret_cast<cbyteptr>(Object.layerName),
											 Aeon::StringOps::Length(Object.layerName));
		}
	};

}

#endif