#ifndef ZZINTERNAL_INCLUDE_AEON_ENGINE_GRAPHICS_RENDERING_VULKAN_VULKANDEVICEQUEUECONFIG
#define ZZINTERNAL_INCLUDE_AEON_ENGINE_GRAPHICS_RENDERING_VULKAN_VULKANDEVICEQUEUECONFIG

#include "Aeon/Framework/Framework.hpp"

#include "Aeon/Engine/Graphics/Rendering/Vulkan/VulkanCommon.hpp"

namespace Aeon
{
	AEON_DECLARE_FLAG_ENUM(EVulkanDeviceQueueCapability, uint32)
	(
		AEON_FLAG_ENUM_RULE_AS_IS,
		Unknown = 0x00,

		Graphics = VK_QUEUE_GRAPHICS_BIT,
		Compute = VK_QUEUE_COMPUTE_BIT,
		Decode = VK_QUEUE_VIDEO_DECODE_BIT_KHR,
		Encode = VK_QUEUE_VIDEO_ENCODE_BIT_KHR,
		Transfer = VK_QUEUE_TRANSFER_BIT,

		SparseBinding = VK_QUEUE_SPARSE_BINDING_BIT,
		ProtectedAccess = VK_QUEUE_PROTECTED_BIT,

	//	OperationMask = (Graphics | Compute | Decode | Encode | Transfer),
	//	ResourceMask = (SparseBinding | ProtectedAccess),
	);

	// TODO remove this at some point
	inline constexpr SStringView MakeCapabilityEnumString(EVulkanDeviceQueueCapability Capability)
	{
		switch (Capability)
		{
			default:
			case EVulkanDeviceQueueCapability::Unknown:
				return "Unknown";

			case EVulkanDeviceQueueCapability::Graphics:
				return "Graphics";
			case EVulkanDeviceQueueCapability::Compute:
				return "Compute";
			case EVulkanDeviceQueueCapability::Decode:
				return "Decode";
			case EVulkanDeviceQueueCapability::Encode:
				return "Encode";
			case EVulkanDeviceQueueCapability::Transfer:
				return "Transfer";

			case EVulkanDeviceQueueCapability::SparseBinding:
				return "SparseBinding";
			case EVulkanDeviceQueueCapability::ProtectedAccess:
				return "ProtectedAccess";
		}
	}

	struct DVulkanDeviceQueueFamilyData
	{
		EVulkanDeviceQueueCapability Capability : 8;
		uint32 QueueCount : 20;

		uint32 SupportsTransfer : 1;
		uint32 SupportsSparseBinding : 1;
		uint32 SupportsProtectedAccess : 1;
		uint32 SupportsPresentation : 1;
	};

	struct DVulkanDeviceQueueCapabilityConfig
	{
		// NOTE This is an inversion of DVulkanDeviceQueueFamilyData,
		//  that stores the index of the queue family to which this group of queues (which will be associated with some capability) belong.

		// Currently limiting the number of families to 255, which seems reasonable
		uint32 QueueFamilyIndex : 8 = ~0_u8;
		uint32 QueueCount : 20 = 0;

		uint32 SupportsTransfer : 1 = false;
		uint32 SupportsSparseBinding : 1 = false;
		uint32 SupportsProtectedAccess : 1 = false;
		uint32 SupportsPresentation : 1 = false;
	};

	AEON_DECLARE_BOOL_ENUM(EVulkanDeviceQueuePresentationSupport);

	class SVulkanDeviceQueueRequirementList
	{
	public:
		explicit SVulkanDeviceQueueRequirementList() = default;

		bool AddRequirement(EVulkanDeviceQueueCapability Capability, uint32 QueueCount,
							EVulkanDeviceQueuePresentationSupport RequiresPresentationSupport);

		void RemoveRequirement(uint32 Index);

		void ClearRequirements();

		GArray<DVulkanDeviceQueueFamilyData> const& QueryRequirements() const;
		uint32 RequirementCount() const;

	private:
		GArray<DVulkanDeviceQueueFamilyData> RequirementArray;

	};

	class SVulkanDeviceQueueConfig
	{
	public:
		explicit SVulkanDeviceQueueConfig();

		DVulkanDeviceQueueCapabilityConfig const& GetCapabilityConfig(EVulkanDeviceQueueCapability Capability) const;
		void SetQueueConfig(EVulkanDeviceQueueCapability Capability, DVulkanDeviceQueueCapabilityConfig const& QueueConfig);

		GStaticArray<uint32, 5> QueryPresentationQueueIndexes() const;

		static GStaticArray<EVulkanDeviceQueueCapability, 5> SupportedCapabilityList();

	private:
		GHashMap<EVulkanDeviceQueueCapability, DVulkanDeviceQueueCapabilityConfig> CapabilityQueueConfigMap;

	};
}

#endif