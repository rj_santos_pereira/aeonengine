#include "Aeon/Engine/Graphics/Rendering/Vulkan/VulkanDeviceQueueConfig.hpp"

namespace Aeon
{
	bool SVulkanDeviceQueueRequirementList::AddRequirement(EVulkanDeviceQueueCapability Capability, uint32 QueueCount,
														   EVulkanDeviceQueuePresentationSupport RequiresPresentationSupport)
	{
		AEON_ASSERT(QueueCount > 0);

		constexpr EVulkanDeviceQueueCapability OperationCapabilityMask = (EVulkanDeviceQueueCapability::Graphics
																		  | EVulkanDeviceQueueCapability::Compute
																		  | EVulkanDeviceQueueCapability::Decode
																		  | EVulkanDeviceQueueCapability::Encode);

		// Modern devices' queue families for Graphics/Compute usually support Transfer operations,
		// but we want to be able to query for support on those queues regardless, to potentially report outliers.
		bool RequiresTransferSupport = Capability & EVulkanDeviceQueueCapability::Transfer;
		bool RequiresSparseBindingSupport = Capability & EVulkanDeviceQueueCapability::SparseBinding;
		bool RequiresProtectedAccessSupport = Capability & EVulkanDeviceQueueCapability::ProtectedAccess;

		// Exclude Transfer if this requirement refers to another operation-type capability,
		// as that will be the primary capability for this requirement
		Capability &= OperationCapabilityMask;
		uint32 CapabilityCount = AeonMath::Popcnt<uint32>(Capability);

		// Otherwise, if this requires only Transfer capability (among operation-type capabilities), restore it as the primary capability
		if (CapabilityCount == 0 && RequiresTransferSupport)
		{
			Capability = EVulkanDeviceQueueCapability::Transfer;
			CapabilityCount = 1;
		}

		// NOTE Currently, this limits the requirements to one (operation) capability per queue family, excluding transfer
		AEON_FAILSAFE(CapabilityCount == 1, return false);

		RequirementArray.AddUnique(DVulkanDeviceQueueFamilyData{ .Capability = Capability,
																 .QueueCount = QueueCount,
																 .SupportsTransfer = RequiresTransferSupport,
																 .SupportsSparseBinding = RequiresSparseBindingSupport,
																 .SupportsProtectedAccess = RequiresProtectedAccessSupport,
																 .SupportsPresentation = RequiresPresentationSupport },
								   [Capability](DVulkanDeviceQueueFamilyData const& Element)
								   { return Element.Capability == Capability; });

		return true;
	}

	void SVulkanDeviceQueueRequirementList::RemoveRequirement(uint32 Index)
	{
		RequirementArray.RemoveAt(Index);
	}

	void SVulkanDeviceQueueRequirementList::ClearRequirements()
	{
		RequirementArray.Empty();
	}

	GArray<DVulkanDeviceQueueFamilyData> const& SVulkanDeviceQueueRequirementList::QueryRequirements() const
	{
		return RequirementArray;
	}

	uint32 SVulkanDeviceQueueRequirementList::RequirementCount() const
	{
		return RequirementArray.Count();
	}

	SVulkanDeviceQueueConfig::SVulkanDeviceQueueConfig()
		: CapabilityQueueConfigMap{}
	{
		for (auto Capability : SupportedCapabilityList())
		{
			CapabilityQueueConfigMap.AddDefault(Capability);
		}
	}

	DVulkanDeviceQueueCapabilityConfig const& SVulkanDeviceQueueConfig::GetCapabilityConfig(EVulkanDeviceQueueCapability Capability) const
	{
		return CapabilityQueueConfigMap[Capability];
	}

	void SVulkanDeviceQueueConfig::SetQueueConfig(EVulkanDeviceQueueCapability Capability, DVulkanDeviceQueueCapabilityConfig const& QueueConfig)
	{
		CapabilityQueueConfigMap[Capability] = QueueConfig;
	}

	GStaticArray<uint32, 5> SVulkanDeviceQueueConfig::QueryPresentationQueueIndexes() const
	{
		return SVulkanDeviceQueueConfig::SupportedCapabilityList()
			.Filter([this] (EVulkanDeviceQueueCapability Capability) -> bool
					{ return CapabilityQueueConfigMap[Capability].SupportsPresentation; })
			.Map([this] (EVulkanDeviceQueueCapability Capability) -> uint32
				 { return CapabilityQueueConfigMap[Capability].QueueFamilyIndex; });
	}

	GStaticArray<EVulkanDeviceQueueCapability, 5> SVulkanDeviceQueueConfig::SupportedCapabilityList()
	{
		return { EVulkanDeviceQueueCapability::Graphics,
				 EVulkanDeviceQueueCapability::Compute,
				 EVulkanDeviceQueueCapability::Transfer,
				 EVulkanDeviceQueueCapability::Decode,
				 EVulkanDeviceQueueCapability::Encode, };
	}
}
