project(AeonEngine CXX)

# FIXME Make this a dll
add_library(${PROJECT_NAME} STATIC)

# TODO For the love of God, document this... set_target_properties in DefaultProjectConfig seems to depend on this somehow
set(AEON_BASE_PROJECT_NAME AeonEngine)

include(${CMAKE_SOURCE_DIR}/DefaultProjectConfig.cmake)

# TODO add fmt only if ${AEON_USING_FMT_LIBRARY} is true
# TODO compile 32-bit versions of these libs
target_link_libraries(${PROJECT_NAME}
	PUBLIC
		AeonFramework
#		fmt
#		utf8proc
		GLFW
		Shaderc
		VulkanSDK
)

#add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Library)

add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Graphics)
#add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Physics)
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Logging)
#add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Memory)
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Platform)
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/System)

#target_sources(${PROJECT_NAME}
#	PUBLIC
#		${CMAKE_CURRENT_SOURCE_DIR}/
#	PRIVATE
#		${CMAKE_CURRENT_SOURCE_DIR}/
#	)