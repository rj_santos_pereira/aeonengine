#ifndef ZZINTERNAL_INCLUDE_AEON_ENGINE_SYSTEM_DISPLAY_SCREEN
#define ZZINTERNAL_INCLUDE_AEON_ENGINE_SYSTEM_DISPLAY_SCREEN

#include "Aeon/Framework/Framework.hpp"

namespace Aeon
{
	class IWindow;

	struct SScreenHandle;

	using HScreen = GOpaque<SScreenHandle>;

	class IScreen
	{
	protected:
		IScreen() = default;

		IScreen(IScreen const&) = delete;
		IScreen(IScreen&&) = delete;

		IScreen& operator=(IScreen const&) = delete;
		IScreen& operator=(IScreen&&) = delete;

		~IScreen() = default;

	public:
		virtual HScreen GetHandle() const = 0;
		virtual voidptr GetNativeHandle() const = 0;

		virtual SIntVector2 GetWorkareaPosition() const = 0;
		virtual SIntVector2 GetWorkareaSize() const = 0;
		virtual SVector2 GetContentScale() const = 0;

		virtual SIntVector2 GetResolution() const = 0;
		virtual SIntVector3 GetBitDepth() const = 0;
	//	virtual EBitFormat GetBitFormat() const = 0;
		virtual int32 GetRefreshRate() const = 0;
	//	virtual float32 GetGammaRamp() const = 0;

		virtual bool ContainsCoord(SIntVector2 Coord) const = 0;
		virtual bool ContainsRect(SIntVector2 CoordMin, SIntVector2 CoordMax) const = 0;

		// Check if window is contained in this screen, by checking whether the screen contains at least half of the window area
		virtual bool ContainsWindow(SIntVector2 WindowCoordMin, SIntVector2 WindowCoordMax) const = 0;
		virtual bool ContainsWindow(IWindow const& Window) const = 0;

		virtual bool IsMainScreen() const = 0;

	};
}

#endif