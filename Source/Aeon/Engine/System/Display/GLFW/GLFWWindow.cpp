#include "Aeon/Engine/System/Display/GLFW/GLFWWindow.hpp"

#include "Aeon/Engine/System/Display/GLFW/GLFWCommon.hpp"

#include "Aeon/Engine/System/Display/GLFW/GLFWScreen.hpp"
#include "Aeon/Engine/System/Display/GLFW/GLFWManager.hpp"

namespace Aeon
{
	SGLFWWindow::SGLFWWindow(ConstructorAccess, SGLFWManager* NewOwningManager, IScreen* NewContainingScreen, DWindowInitParams const& Params)
		: OwningManager{ NewOwningManager }
		, CachedContainingScreen{ NewContainingScreen }
		, WindowHandle{ nullptr }
		, KeyStateMap{ TCreateElements::Tag, KeyStateMapSize }
		, CursorStateMap{ TCreateElements::Tag, CursorStateMapSize }
	{
		IsFocusedFlag = Params.IsFocused;
		glfwWindowHint(GLFW_FOCUSED, IsFocusedFlag);

		IsMaximizedFlag = false;
		IsMinimizedFlag = false;
		IsFullscreenFlag = false;
		glfwWindowHint(GLFW_MAXIMIZED, GLFW_FALSE);

		IsVisibleFlag = Params.IsVisible;
		glfwWindowHint(GLFW_VISIBLE, IsVisibleFlag);

		IsResizableFlag = Params.IsResizable;
		glfwWindowHint(GLFW_RESIZABLE, IsResizableFlag);

		HasTitleBarFlag = Params.HasTitleBar;
		glfwWindowHint(GLFW_DECORATED, HasTitleBarFlag);

		IsAlwaysOnTopFlag = Params.IsAlwaysOnTop;
		glfwWindowHint(GLFW_FLOATING, IsAlwaysOnTopFlag);

		// NOTE GLFW_FOCUS_ON_SHOW affects whether the window is given focus when shown/toggled visible
		glfwWindowHint(GLFW_FOCUS_ON_SHOW, GLFW_TRUE); // CHECKME parameterize this if necessary

		// NOTE On some platforms, whatever logic is used to scale automatically computes some weird/wrong values
		// so this is disabled so that the window can be scaled manually using the callback
		glfwWindowHint(GLFW_SCALE_TO_MONITOR, GLFW_FALSE);
		// CHECKME Validate how this works
		// Supposedly, this automatically adjusts the framebuffer size when window is moved between screens with different content scale,
		// on platforms/screens where the ratio between pixel/coords is not 1:1
		glfwWindowHint(GLFW_SCALE_FRAMEBUFFER, GLFW_TRUE);

		glfwWindowHint(GLFW_POSITION_X, Params.Position.X);
		glfwWindowHint(GLFW_POSITION_Y, Params.Position.Y);

	//	glfwWindowHint(GLFW_CENTER_CURSOR, GLFW_TRUE); // FIXME this seems to not be working...

		// NOTE For more info on fullscreen/borderless fullscreen and video modes,
		//  start here: https://www.glfw.org/docs/latest/window_guide.html#window_creation

		// If enabled, will minimize fullscreen windows when focus is lost // CHECKME parameterize this if necessary
		glfwWindowHint(GLFW_AUTO_ICONIFY, GLFW_FALSE);

		// TODO add "Creating window" log

		WindowHandle = glfwCreateWindow(Params.Size.X,
										Params.Size.Y,
										Params.Title.CharBuffer(),
										nullptr, /* Fullscreen is configured after creating the window */
										nullptr /* TODO share resources omitted for now */);

		glfwSetWindowUserPointer(WindowHandle, this);

		CachedTitle = { glfwGetWindowTitle(WindowHandle) };

		CachedPosition = Params.Position;
		glfwSetWindowPosCallback(WindowHandle, &UpdatePositionForWindow_Internal);

		CachedSize = Params.Size;
		glfwSetWindowSizeCallback(WindowHandle, &UpdateSizeForWindow_Internal);

	//	glfwGetFramebufferSize(WindowHandle, &CachedFramebufferSize.X, &CachedFramebufferSize.Y);

		// NOTE This depends on the containing screen
		glfwGetWindowContentScale(WindowHandle, &CachedContentScale.X, &CachedContentScale.Y);
		glfwSetWindowContentScaleCallback(WindowHandle, &UpdateContentScaleForWindow_Internal);

		// Adjust window size to account for the screen content scale
		UseContentScalingFlag = Params.UseContentScaling;
		if (UseContentScalingFlag)
		{
			if (!CachedContentScale.IsApproxEqual(SVector2::One()))
			{
				CachedSize *= CachedContentScale;

				glfwSetWindowSize(WindowHandle, CachedSize.X, CachedSize.Y);
			}
		}

		IsUpdatingContentScale = false;
		IsUpdatingPosition = false;
		IsUpdatingSize = false;

		IsUpdatingIconifyState = false;
		IsSettingUpFullscreen_HACK = false;
		IsUpdatingPosSize_HACK = false;

		glfwSetWindowFocusCallback(WindowHandle, &UpdateFocusForWindow_Internal);
		glfwSetWindowMaximizeCallback(WindowHandle, &UpdateMaximizedForWindow_Internal);
		glfwSetWindowIconifyCallback(WindowHandle, &UpdateMinimizedForWindow_Internal);

		// Configure fullscreen now
		ToggleFullscreen(Params.IsFullscreen);

		// TODO At some point, allow locking window to some user-selected aspect ratio
	//	glfwSetWindowAspectRatio(WindowHandle, 16, 9);

		// On glfwGetKey:
		// > This function only returns cached key event state.
		// > It does not poll the system for the current state of the physical key.
		// > It also does not provide any key repeat information.
		// > Whenever you poll state, you risk missing the state change you are looking for.
		// > If a pressed key is released again before you poll its state, you will have missed the key press.
		// > The recommended solution for this is to use a key callback, but there is also the GLFW_STICKY_KEYS input mode.
		// > When sticky keys mode is enabled,
		// > the pollable state of a key will remain GLFW_PRESS until the state of that key is polled with glfwGetKey.
		// > Once it has been polled, if a key release event had been processed in the meantime, the state will reset to GLFW_RELEASE,
		// > otherwise it will remain GLFW_PRESS.
		// In our case, we don't care about "missed" events, since we are listening for all events and dispatch them to whoever wants,
		// so we at most only want to query the current state of a key.
		glfwSetInputMode(WindowHandle, GLFW_STICKY_KEYS, GLFW_FALSE);

		// > If you wish to know what the state of the Caps Lock and Num Lock keys was when input events were generated,
		// > set the GLFW_LOCK_KEY_MODS input mode.
		// > When this input mode is enabled, any callback that receives modifier bits will have the GLFW_MOD_CAPS_LOCK bit set
		// > if Caps Lock was on when the event occurred and the GLFW_MOD_NUM_LOCK bit set if Num Lock was on.
		glfwSetInputMode(WindowHandle, GLFW_LOCK_KEY_MODS, GLFW_TRUE);

		glfwSetKeyCallback(WindowHandle, &UpdateKeyStateForWindow_Internal);
	//	glfwSetCharCallback(WindowHandle, &UpdateCharEventForWindow_Internal); // TODO implement this

		glfwSetCursorEnterCallback(WindowHandle, &UpdateCursorEnterLeaveForWindow_Internal);

		SetCursorMode(Params.CursorMode);

		// TODO make cursor image configurable
		//  see https://www.glfw.org/docs/latest/input_guide.html#cursor_object
#if 0
		byte Pixels[1 * 1 * 4];
		::std::memset(pixels, 0xFF, sizeof(pixels));
		GLFWimage image;
		image.width = 1;
		image.height = 1;
		image.pixels = reinterpret_cast<unsigned char*>(pixels);

	//	GLFWcursor* CursorImage = glfwCreateStandardCursor(GLFW_CROSSHAIR_CURSOR);
		GLFWcursor* CursorImage = glfwCreateCursor(&image, 0, 0);
		glfwSetCursor(WindowHandle, CursorImage);
#endif

		UseRawInputForCursorFlag = Params.UseRawInputForCursor;
		if (UseRawInputForCursorFlag)
		{
			// CHECKME Should scaling and acceleration be disabled in general using the platform's API?
			//  For Windows, see: https://learn.microsoft.com/en-us/windows/win32/inputdev/about-raw-input
			if (glfwRawMouseMotionSupported())
			{
				AEON_LOG(Info, GLFWManager, "Enabling raw cursor input for window {}", CachedTitle);
				// > Raw mouse motion is closer to the actual motion of the mouse across a surface.
				// > It is not affected by the scaling and acceleration applied to the motion of the desktop cursor.
				// > That processing is suitable for a cursor while raw motion is better for controlling for example a 3D camera.
				// > Because of this, raw mouse motion is only provided when the cursor is disabled.
				// In AeonEngine terms, 'disabled' means 'CapturedVirtual'; we enable it regardless of the current input mode.
				glfwSetInputMode(WindowHandle, GLFW_RAW_MOUSE_MOTION, GLFW_TRUE);
			}
			else
			{
				AEON_LOG(Warning, GLFWManager, "Raw cursor input is not available!");
			}
		}

		glfwSetMouseButtonCallback(WindowHandle, &UpdateCursorStateForWindow_Internal);
		glfwSetScrollCallback(WindowHandle, &UpdateCursorScrollForWindow_Internal);
		glfwSetCursorPosCallback(WindowHandle, &UpdateCursorPositionForWindow_Internal);
	}

	HWindow SGLFWWindow::GetHandle() const
	{
		return const_cast<SWindowHandle&>(WindowHandle);
	}
	voidptr SGLFWWindow::GetNativeHandle() const
	{
#	if AEON_PLATFORM_WINDOWS
		return glfwGetWin32Window(WindowHandle);
#	else
#		error Not implemented yet!
#	endif
	}

	IScreen& SGLFWWindow::GetContainingScreen() const
	{
		if (IsFullscreenFlag)
		{
			SScreenHandle Handle = glfwGetWindowMonitor(WindowHandle);
			return OwningManager->GetScreen(Handle);
		}

		return OwningManager->FindScreenContainingWindow(*this);
	}

	SStringView SGLFWWindow::GetTitle() const
	{
		return CachedTitle;
	}

	SIntVector2 SGLFWWindow::GetPosition() const
	{
		return CachedPosition;
	}
	SIntVector2 SGLFWWindow::GetSize() const
	{
		return CachedSize;
	}
	SIntVector2 SGLFWWindow::GetFramebufferSize() const
	{
		return CachedSize;
	}

	bool SGLFWWindow::IsFocused() const
	{
		return IsFocusedFlag;
	}

	bool SGLFWWindow::IsMaximized() const
	{
		return IsMaximizedFlag;
	}

	bool SGLFWWindow::IsMinimized() const
	{
		return IsMinimizedFlag;
	}

	bool SGLFWWindow::IsFullscreen() const
	{
		return IsFullscreenFlag;
	}

	bool SGLFWWindow::IsVisible() const
	{
		return IsVisibleFlag;
	}

	bool SGLFWWindow::IsResizable() const
	{
		return IsResizableFlag;
	}

	bool SGLFWWindow::HasTitleBar() const
	{
		return HasTitleBarFlag;
	}

	bool SGLFWWindow::IsAlwaysOnTop() const
	{
		return IsAlwaysOnTopFlag;
	}

	bool SGLFWWindow::UsesContentScaling() const
	{
		return UseContentScalingFlag;
	}

	bool SGLFWWindow::UsesRawInputForCursor() const
	{
		// CHECKME should we return this conditionally based on cursor mode? i.e.
		//	return CachedCursorMode == ECursorMode::CapturedVirtual && UseRawInputForCursorFlag
		return UseRawInputForCursorFlag;
	}

	void SGLFWWindow::SetTitle(SStringView NewTitle)
	{
		AEON_ASSERT(SThread::ThisId() == MainThread.Id());
		glfwSetWindowTitle(WindowHandle, NewTitle.CharBuffer());
		CachedTitle = NewTitle;
	}

	void SGLFWWindow::SetPosition(SIntVector2 NewPosition)
	{
		AEON_ASSERT(SThread::ThisId() == MainThread.Id());
		if (!(IsMinimizedFlag || IsMaximizedFlag || IsFullscreenFlag))
		{
			glfwSetWindowPos(WindowHandle, NewPosition.X, NewPosition.Y);
		}
	}
	void SGLFWWindow::SetSize(SIntVector2 NewSize)
	{
		AEON_ASSERT(SThread::ThisId() == MainThread.Id());

		// CHECKME Should calling this method be allowed even if window is non-resizable?
	//	AEON_FAILSAFE(IsResizableFlag, return);

		if (!(IsMinimizedFlag || IsMaximizedFlag || IsFullscreenFlag))
		{
			glfwSetWindowSize(WindowHandle, NewSize.X, NewSize.Y);
		}
	}

	void SGLFWWindow::ToggleFocus()
	{
		AEON_ASSERT(SThread::ThisId() == MainThread.Id());
		glfwFocusWindow(WindowHandle);
	}

	void SGLFWWindow::ToggleMaximized(bool NewMaximized)
	{
		AEON_ASSERT(SThread::ThisId() == MainThread.Id());
		if (NewMaximized != IsMaximizedFlag)
		{
			if (NewMaximized)
			{
				glfwMaximizeWindow(WindowHandle);
			}
			else
			{
				glfwRestoreWindow(WindowHandle);
			}

			IsMaximizedFlag = NewMaximized;
		}
	}

	void SGLFWWindow::ToggleMinimized(bool NewMinimized)
	{
		AEON_ASSERT(SThread::ThisId() == MainThread.Id());
		if (NewMinimized != IsMinimizedFlag)
		{
			if (NewMinimized)
			{
				glfwIconifyWindow(WindowHandle);
			}
			else
			{
				glfwRestoreWindow(WindowHandle);
			}

			IsMinimizedFlag = NewMinimized;
		}
	}

	void SGLFWWindow::ToggleFullscreen(bool NewFullscreen)
	{
		AEON_ASSERT(SThread::ThisId() == MainThread.Id());
		if (NewFullscreen != IsFullscreenFlag)
		{
			IsFullscreenFlag = NewFullscreen;

			if (NewFullscreen)
			{
				CachedWindowedPosition = CachedPosition;
				CachedWindowedSize = CachedSize;

				IsSettingUpFullscreen_HACK = true;

				SetupFullscreen_Internal();
			}
			else
			{
				glfwSetWindowMonitor(WindowHandle, nullptr,
									 CachedWindowedPosition.X, CachedWindowedPosition.Y,
									 CachedWindowedSize.X, CachedWindowedSize.Y, 0);
			}
		}
	}

	void SGLFWWindow::ToggleVisible(bool NewVisible)
	{
		AEON_ASSERT(SThread::ThisId() == MainThread.Id());
		if (NewVisible != IsVisibleFlag)
		{
			if (NewVisible)
			{
				glfwShowWindow(WindowHandle);
			}
			else
			{
				glfwHideWindow(WindowHandle);
			}

			IsVisibleFlag = NewVisible;
		}
	}

	void SGLFWWindow::ToggleResizable(bool NewResizable)
	{
		AEON_ASSERT(SThread::ThisId() == MainThread.Id());
		if (NewResizable != IsResizableFlag)
		{
			glfwSetWindowAttrib(WindowHandle, GLFW_RESIZABLE, NewResizable);
			IsResizableFlag = NewResizable;
		}
	}

	void SGLFWWindow::ProcessEvents(ManagerAccess)
	{
		EFramebufferChange FramebufferChange = EFramebufferChange::None;

		// First update the cached content scale, and adjust the cached size (windowed size if fullscreen),
		// and apply various hacks due to the system scaling misbehaving in certain situations
		if (IsUpdatingContentScale)
		{
			SVector2 OldContentScale = CachedContentScale;
			SVector2 NewContentScale;
			glfwGetWindowContentScale(WindowHandle, &NewContentScale.X, &NewContentScale.Y);

			CachedContentScale = NewContentScale;

			AEON_LOG(Info, GLFWManager, "Updating window '{}': content scale=({},{})",
					 CachedTitle, CachedContentScale.X, CachedContentScale.Y);

			// HACK On Windows platforms, moving the window between screens with different content scales using a keyboard shortcut
			//  (e.g. Win+Shift+NavArrow) causes the window to resize due to scaling even when SCALE_TO_MONITOR is disabled.
			//  If the window is using content scaling, compute the correct new size (otherwise system computes wrong values);
			//  otherwise, reset the window to the old size.
			//  ---
			//  The only consistent way we found of detecting this is by checking whether a particular window message, WM_EXITSIZEMOVE,
			//  is received by an attached window procedure hook.
			//  This message is received when the user finishes dragging the window or its border, which never happens when using the shortcut,
			//  as the repositioning is immediate (only WM_MOVE is called in that case).

			if (UseContentScalingFlag)
			{
				// We only want to resize the window if we are moving the window between screens, and not resizing across screens

				if (IsUpdatingPosition && (!IsUpdatingSize || !IsUpdatingPosSize_HACK))
				{
					SVector2 ContentScaleRatio = NewContentScale / OldContentScale;

					if (!IsFullscreenFlag)
					{
						CachedSize *= ContentScaleRatio;

						glfwSetWindowSize(WindowHandle, CachedSize.X, CachedSize.Y);

						IsUpdatingSize = true;
					}
					else
					{
						// NOTE When in fullscreen, content scaling rules don't apply,
						// so simply update the cached windowed size instead so that when we leave fullscreen, we set the previous size correctly.
						CachedWindowedSize *= ContentScaleRatio;
					}
				}
			}
			else
			{
#			if AEON_PLATFORM_WINDOWS
				if (!IsFullscreenFlag && !IsUpdatingPosSize_HACK)
				{
					AEON_LOG(Warning, GLFWManager, "HACK: Resetting size due to move between screens with different content scales.");

					glfwSetWindowSize(WindowHandle, CachedSize.X, CachedSize.Y);

					IsUpdatingSize = false;
				}
#			endif
			}
		}

		// Then update the cached position (windowed position if fullscreen), and re-setup fullscreen when the containing screen changes,
		// which will also conditionally trigger a resize.
		if (IsUpdatingPosition)
		{
			// Don't update the cached position if the window is just changing iconify state
			if (!IsUpdatingIconifyState)
			{
				SIntVector2 OldPosition = CachedPosition;
				SIntVector2 NewPosition;
				glfwGetWindowPos(WindowHandle, &NewPosition.X, &NewPosition.Y);

				CachedPosition = NewPosition;

				AEON_LOG(Info, GLFWManager, "Updating window '{}': position=({},{})", CachedTitle, CachedPosition.X, CachedPosition.Y);

				// Check if window moved to a different containing screen
				IScreen& CurrentContainingScreen = OwningManager->FindScreenContainingWindow(*this);
				if (CurrentContainingScreen.GetHandle() != CachedContainingScreen->GetHandle())
				{
					CachedContainingScreen = addressof(CurrentContainingScreen);

					// When moving window between screens, the current containing screen may support different surface formats,
					// so this must update the cached format list
					FramebufferChange |= EFramebufferChange::Position;
				}

				SIntVector2 PositionDelta = (NewPosition - OldPosition);

				CachedCursorPosition += PositionDelta;
				CachedCursorPositionDelta = SVector2::Zero();

				// HACK GLFW does not update owning monitor if system moves the window, so do it here instead;
				//  see https://github.com/glfw/glfw/issues/2137
				if (IsFullscreenFlag && !IsSettingUpFullscreen_HACK)
				{
					AEON_LOG(Warning, GLFWManager, "HACK: Re-setting up fullscreen due to window reposition event triggered by system.");

					// Also update the windowed position with the delta between the old and the new positions,
					// so that when exiting fullscreen, we can present the window in the new position instead of the old one
					CachedWindowedPosition += PositionDelta;

					// NOTE This might call UpdateSizeForWindow_Internal if the new containing screen has a different resolution,
					//  so no need to set IsUpdatingSize explicitly.
					SetupFullscreen_Internal();
				}
			}
		}

		// Lastly update the cached size, which depends on all the previous checks.
		if (IsUpdatingSize)
		{
			// Don't update the cached size if the window is just changing iconify state
			if (!IsUpdatingIconifyState)
			{
				glfwGetWindowSize(WindowHandle, &CachedSize.X, &CachedSize.Y);

				AEON_LOG(Info, GLFWManager, "Updating window '{}': size={}x{}", CachedTitle, CachedSize.X, CachedSize.Y);

			//	glfwGetFramebufferSize(WindowHandle, &CachedFramebufferSize.X, &CachedFramebufferSize.Y);

				FramebufferChange |= EFramebufferChange::Size;
			}
		}

		if (FramebufferChange != EFramebufferChange::None)
		{
			BroadcastFramebufferUpdated(FramebufferChange, CachedSize, CachedPosition, SEulerRotation::Identity());
		}

		IsUpdatingContentScale = false;
		IsUpdatingPosition = false;
		IsUpdatingSize = false;

		IsUpdatingIconifyState = false;
		IsSettingUpFullscreen_HACK = false;
		IsUpdatingPosSize_HACK = false;

		UpdatePendingKeyState_Internal();
		UpdatePendingCursorState_Internal();
	}

	void SGLFWWindow::MarkWindowUpdatingPosSize_HACK()
	{
		IsUpdatingPosSize_HACK = true;
	}

	EKeyEvent SGLFWWindow::QueryKeyEvent(EVirtualKey Key) const
	{
		auto& KeyState = KeyStateMap[MapVirtualKeyToIndex_Internal(Key)];
		return KeyState.CachedEvent;
	}
	EKeyDirectEvent SGLFWWindow::QueryKeyState(EVirtualKey Key) const
	{
		auto& KeyState = KeyStateMap[MapVirtualKeyToIndex_Internal(Key)];
		return KeyState.CachedDirectEvent;
	}
	EKeyModifierState SGLFWWindow::QueryKeyModifierState() const
	{
		return CachedKeyModifierState;
	}

	ECursorEvent SGLFWWindow::QueryCursorEvent(ECursorButton Button) const
	{
		auto& CursorState = CursorStateMap[Button];
		return CursorState.CachedEvent;
	}
	ECursorDirectEvent SGLFWWindow::QueryCursorState(ECursorButton Button) const
	{
		auto& CursorState = CursorStateMap[Button];
		return CursorState.CachedDirectEvent;
	}

	ECursorMode SGLFWWindow::GetCursorMode() const
	{
		return CachedCursorMode;
	}

	SVector2 SGLFWWindow::QueryCursorPosition() const
	{
		return CachedCursorPosition;
	}
	SVector2 SGLFWWindow::QueryCursorPositionDelta() const
	{
		return CachedCursorPositionDelta;
	}
	SVector2 SGLFWWindow::QueryCursorScrollDelta() const
	{
		return CachedCursorScrollDelta;
	}

	DKeyEventParams SGLFWWindow::GetKeyEventParams() const
	{
		return KeyEventParams;
	}
	DCursorEventParams SGLFWWindow::GetCursorEventParams() const
	{
		return CursorEventParams;
	}

	void SGLFWWindow::SetCursorMode(ECursorMode NewCursorMode)
	{
		if (NewCursorMode != CachedCursorMode)
		{
			if (IsFocusedFlag && NewCursorMode == ECursorMode::CapturedVirtual)
			{
				float32 CenterX = CachedSize.X / 2.f;
				float32 CenterY = CachedSize.Y / 2.f;

				glfwSetCursorPos(WindowHandle, CenterX, CenterY);
				CachedCursorPosition = SVector2{ CenterX, CenterY };
			}

			int32 GLFWCursorMode = GLFW_CURSOR_NORMAL;
			switch (NewCursorMode)
			{
				case ECursorMode::Free: GLFWCursorMode = GLFW_CURSOR_NORMAL; break;
				case ECursorMode::FreeHidden: GLFWCursorMode = GLFW_CURSOR_HIDDEN; break;
				case ECursorMode::CapturedHidden: AEON_FAILSAFE(false, void(), "ECursorMode::CapturedHidden not supported, using ECursorMode::Captured");
				case ECursorMode::Captured: GLFWCursorMode = GLFW_CURSOR_CAPTURED; break;
				case ECursorMode::CapturedVirtual: GLFWCursorMode = GLFW_CURSOR_DISABLED; break;
			//	default: AEON_FAILSAFE(false, void(), "Invalid ECursorMode value");
			}

			glfwSetInputMode(WindowHandle, GLFW_CURSOR, GLFWCursorMode);

			CachedCursorMode = NewCursorMode;
		}
	}

	void SGLFWWindow::SetKeyEventParams(DKeyEventParams NewParams)
	{
		KeyEventParams = NewParams;
	}
	void SGLFWWindow::SetCursorEventParams(DCursorEventParams NewParams)
	{
		CursorEventParams = NewParams;
	}

	void SGLFWWindow::SetupFullscreen_Internal()
	{
		SScreenHandle ScreenHandle = CachedContainingScreen->GetHandle();

		SIntVector2 Resolution = CachedContainingScreen->GetResolution();
		int32 RefreshRate = CachedContainingScreen->GetRefreshRate();

		AEON_LOG(Info, GLFWManager, "Setting up fullscreen: resolution={}x{} refresh rate={}", Resolution.X, Resolution.Y, RefreshRate);

		glfwSetWindowMonitor(WindowHandle, ScreenHandle, 0, 0,
							 Resolution.X, Resolution.Y, RefreshRate);
	}

	void SGLFWWindow::UpdateKeyState_Internal(EVirtualKey Key, EKeyDirectEvent NewDirectEvent)
	{
		// CHECKME See https://github.com/glfw/glfw/blob/master/src/win32_window.c#L658
		//  on how GLFW detects and changes key names on keyboard layout change

		auto& KeyState = KeyStateMap[MapVirtualKeyToIndex_Internal(Key)];

		EKeyEvent NewEvent = EKeyEvent::None;
		if (NewDirectEvent == EKeyDirectEvent::Down)
		{
			if (KeyState.RepeatEventCount != 0)
			{
				uint8 RepeatTapCount = KeyState.RepeatEventCount;

				++RepeatTapCount;

				float64 TapDelayDuration = KeyState.EventStopwatch.Current().AsSeconds();
				float64 RepeatTapDuration = KeyState.RepeatEventStopwatch.Current().AsSeconds();

				if (TapDelayDuration >= KeyEventParams.TapDeadline
					|| RepeatTapDuration >= KeyEventParams.RepeatedTapDeadline
					|| RepeatTapCount > KeyEventParams.RepeatedTapLimit)
				{
					KeyState.RepeatEventCount = 0;
				}
			}

			KeyState.EventStopwatch.Restart();

			if (KeyState.RepeatEventCount == 0)
			{
				KeyState.RepeatEventStopwatch = KeyState.EventStopwatch;
			}

			if (PendingKeyStateArray.Count() < PendingKeyStateArray.AllocatedCount())
			{
				PendingKeyStateArray.AddSorted(Key);
			}
		}
		else
		{
			// NOTE When a window loses focus, it generates key (and cursor button) release events;
			//  > When a window loses input focus, it will generate synthetic key release events
			//  > for all pressed keys with associated key tokens.
			//  > You can tell these events from user-generated events
			//  > by the fact that the synthetic ones are generated after the focus loss event has been processed,
			//  > i.e. after the window focus callback has been called.

			bool RemovePendingState = true;

			float64 EventDuration = KeyState.EventStopwatch.Current().AsSeconds();

			if (EventDuration < KeyEventParams.TapDeadline)
			{
				// Emit Tap / RepeatedTap

				uint8 RepeatTapCount = KeyState.RepeatEventCount;

				++RepeatTapCount;

				if (RepeatTapCount > 1)
				{
					float64 RepeatTapDuration = KeyState.RepeatEventStopwatch.Current().AsSeconds();

					if (RepeatTapDuration < KeyEventParams.RepeatedTapDeadline
						&& RepeatTapCount <= KeyEventParams.RepeatedTapLimit)
					{
						NewEvent = AEON_WRITE_KEY_EVENT_DATA(EKeyEvent::RepeatedTap, RepeatTapCount);
					}
					else
					{
						RepeatTapCount = 1;
					}
				}

				if (RepeatTapCount == 1)
				{
					NewEvent = AEON_WRITE_KEY_EVENT_DATA(EKeyEvent::Tap, RepeatTapCount);
				}

				KeyState.RepeatEventCount = RepeatTapCount;

				// Restart event stopwatch here, so we can measure delay between taps
				KeyState.EventStopwatch.Restart();
			}
			else if (EventDuration < KeyEventParams.PressDeadline)
			{
				// Emit Press

				NewEvent = EKeyEvent::Press;

				KeyState.RepeatEventCount = 0;
			}
			else
			{
				// Emit Release

				if (AEON_READ_KEY_EVENT(KeyState.CachedEvent) == EKeyEvent::Hold)
				{
					NewEvent = EKeyEvent::Release;
				}
				else
				{
					AEON_LOG(Warning, GLFWManager, "Failed to emit Hold (and now Release) event for key {}", uint32(Key));
					NewEvent = EKeyEvent::Press;
				}

				KeyState.RepeatEventCount = 0;

				RemovePendingState = false;
			}

			if (RemovePendingState)
			{
				if (auto Index = PendingKeyStateArray.FindIndexSorted(Key))
				{
					PendingKeyStateArray.RemoveAt(*Index);
				}
			}
		}

		KeyState.CachedDirectEvent = NewDirectEvent;
		BroadcastKeyDirectEvent(Key, NewDirectEvent, CachedKeyModifierState);

		if (NewEvent != EKeyEvent::None)
		{
			KeyState.CachedEvent = NewEvent;
			BroadcastKeyEvent(Key, NewEvent, CachedKeyModifierState);
		}
	}

	void SGLFWWindow::UpdatePendingKeyState_Internal()
	{
		bool HasUpdatedState = false;

		for (EVirtualKey& Key : PendingKeyStateArray)
		{
			auto& KeyState = KeyStateMap[MapVirtualKeyToIndex_Internal(Key)];

			float64 EventDuration = KeyState.EventStopwatch.Current().AsSeconds();

			// Emit Hold

			if (EventDuration >= KeyEventParams.PressDeadline)
			{
				auto NewEvent = KeyState.CachedEvent = EKeyEvent::Hold;
				BroadcastKeyEvent(Key, NewEvent, CachedKeyModifierState);

				Key = EVirtualKey::Null;
				HasUpdatedState = true;
			}
		}

		if (HasUpdatedState)
		{
			while (auto Index = PendingKeyStateArray.FindIndex(EVirtualKey::Null))
			{
				PendingKeyStateArray.RemoveAt(*Index);
			}
		}
	}

	void SGLFWWindow::UpdateCursorState_Internal(ECursorButton Button, ECursorDirectEvent NewDirectEvent)
	{
		auto& CursorState = CursorStateMap[Button];

		ECursorEvent NewEvent = ECursorEvent::None;
		if (NewDirectEvent == ECursorDirectEvent::Down)
		{
			// If we are counting repeating clicks, check if this potential next click can be a repeated click,
			// or if we should stop counting
			if (CursorState.RepeatEventCount != 0)
			{
				uint8 RepeatClickCount = CursorState.RepeatEventCount;

				++RepeatClickCount;

				float64 ClickDelayDuration = CursorState.EventStopwatch.Current().AsSeconds();
				float64 RepeatClickDuration = CursorState.RepeatEventStopwatch.Current().AsSeconds();

				if (ClickDelayDuration >= CursorEventParams.ClickDeadline
					|| RepeatClickDuration >= CursorEventParams.RepeatedClickDeadline
					|| RepeatClickCount > CursorEventParams.RepeatedClickLimit)
				{
					CursorState.RepeatEventCount = 0;
				}
			}

			// Restart event stopwatch to determine whether it's a click or a drag
			CursorState.EventStopwatch.Restart();

			// If we are not counting repeating clicks, also restart the repeat stopwatch
			if (CursorState.RepeatEventCount == 0)
			{
				CursorState.RepeatEventStopwatch = CursorState.EventStopwatch;
			}

			if (PendingCursorStateArray.Count() < PendingCursorStateArray.AllocatedCount())
			{
				PendingCursorStateArray.AddSorted(Button);
			}
		}
		else
		{
			// HACK This branch is a workaround for two "bugs" in GLFW:
			// - When a window loses focus, GLFW emits a button-up direct event (for all pressed buttons),
			//   but it also (erroneously) emits a button-up direct event if detected within the window's bounds while out of focus;
			// - When maximizing a window through double-click on the title bar, a button-up direct event might be emitted during the last click,
			//   because the window changes size and the button-up then happens within the bounds;
			// For both of these "bugs", the workaround is to check if the last processed direct event is a button-down event,
			// as each button-up event must have exactly one associated button-down event.
			if (CursorState.CachedDirectEvent != ECursorDirectEvent::Down)
			{
				AEON_LOG(Warning, GLFWManager, "HACK: Detected erroneous cursor button-up direct event, skipping event dispatch.");
				return;
			}

			bool RemovePendingState = true;

			float64 EventDuration = CursorState.EventStopwatch.Current().AsSeconds();

			if (EventDuration < CursorEventParams.ClickDeadline)
			{
				// Emit Click / DoubleClick / TripleClick

				uint8 RepeatClickCount = CursorState.RepeatEventCount;

				++RepeatClickCount;

				if (RepeatClickCount > 1)
				{
					float64 RepeatClickDuration = CursorState.RepeatEventStopwatch.Current().AsSeconds();

					if (RepeatClickDuration < CursorEventParams.RepeatedClickDeadline
						&& RepeatClickCount <= CursorEventParams.RepeatedClickLimit)
					{
						NewEvent = RepeatClickCount > 2 ? ECursorEvent::TripleClick : ECursorEvent::DoubleClick;
					}
					else
					{
						RepeatClickCount = 1;
					}
				}

				if (RepeatClickCount == 1)
				{
					NewEvent = ECursorEvent::Click;
				}

				CursorState.RepeatEventCount = RepeatClickCount;

				// Restart event stopwatch here, so we can measure delay between clicks
				CursorState.EventStopwatch.Restart();
			}
			else
			{
				// Emit Release

				if (CursorState.CachedEvent == ECursorEvent::Drag)
				{
					NewEvent = ECursorEvent::Release;
				}
				else
				{
					AEON_LOG(Warning, GLFWManager, "Failed to emit Drag (and now Release) event for cursor button {}", uint32(Button));
					NewEvent = ECursorEvent::Click;
				}

				CursorState.RepeatEventCount = 0;

				RemovePendingState = false;
			}

			if (RemovePendingState)
			{
				if (auto Index = PendingCursorStateArray.FindIndexSorted(Button))
				{
					PendingCursorStateArray.RemoveAt(*Index);
				}
			}
		}

		CursorState.CachedDirectEvent = NewDirectEvent;
		BroadcastCursorDirectEvent(Button, NewDirectEvent);

		if (NewEvent != ECursorEvent::None)
		{
			CursorState.CachedEvent = NewEvent;
			BroadcastCursorEvent(Button, NewEvent);
		}
	}

	void SGLFWWindow::UpdatePendingCursorState_Internal()
	{
		bool HasUpdatedState = false;

		for (ECursorButton& Button : PendingCursorStateArray)
		{
			auto& CursorState = CursorStateMap[Button];

			float64 EventDuration = CursorState.EventStopwatch.Current().AsSeconds();

			// Emit Drag

			if (EventDuration >= CursorEventParams.ClickDeadline)
			{
				auto NewEvent = CursorState.CachedEvent = ECursorEvent::Drag;
				BroadcastCursorEvent(Button, NewEvent);

				Button = ECursorButton::Null;
				HasUpdatedState = true;
			}
		}

		if (HasUpdatedState)
		{
			while (auto Index = PendingCursorStateArray.FindIndex(ECursorButton::Null))
			{
				PendingCursorStateArray.RemoveAt(*Index);
			}
		}
	}

	void SGLFWWindow::UpdateContentScaleForWindow_Internal(GLFWwindow* Handle, float32 ScaleX, float32 ScaleY)
	{
		AEON_UNUSED(ScaleX, ScaleY);

		auto& Window = *static_cast<SGLFWWindow*>(glfwGetWindowUserPointer(Handle));

		Window.IsUpdatingContentScale = true;
	}

	void SGLFWWindow::UpdatePositionForWindow_Internal(GLFWwindow* Handle, int32 PositionX, int32 PositionY)
	{
		AEON_UNUSED(PositionX, PositionY);

		auto& Window = *static_cast<SGLFWWindow*>(glfwGetWindowUserPointer(Handle));

		Window.IsUpdatingPosition = true;
	}

	void SGLFWWindow::UpdateSizeForWindow_Internal(GLFWwindow* Handle, int32 SizeX, int32 SizeY)
	{
		AEON_UNUSED(SizeX, SizeY);

		auto& Window = *static_cast<SGLFWWindow*>(glfwGetWindowUserPointer(Handle));

		Window.IsUpdatingSize = true;
	}

	void SGLFWWindow::UpdateFocusForWindow_Internal(GLFWwindow* Handle, int32 NewFocused)
	{
		auto& Window = *static_cast<SGLFWWindow*>(glfwGetWindowUserPointer(Handle));

		AEON_LOG(Info, GLFWManager, "Updating window '{}': focused={}", Window.CachedTitle, NewFocused);

		Window.IsFocusedFlag = NewFocused;
	}

	void SGLFWWindow::UpdateMaximizedForWindow_Internal(GLFWwindow* Handle, int32 NewMaximized)
	{
		auto& Window = *static_cast<SGLFWWindow*>(glfwGetWindowUserPointer(Handle));

		AEON_LOG(Info, GLFWManager, "Updating window '{}': maximized={}", Window.CachedTitle, NewMaximized);

		Window.IsMaximizedFlag = NewMaximized;
	}

	void SGLFWWindow::UpdateMinimizedForWindow_Internal(GLFWwindow* Handle, int32 NewMinimized)
	{
		auto& Window = *static_cast<SGLFWWindow*>(glfwGetWindowUserPointer(Handle));

		AEON_LOG(Info, GLFWManager, "Updating window '{}': minimized={}", Window.CachedTitle, NewMinimized);

		Window.IsMinimizedFlag = NewMinimized;
		Window.IsUpdatingIconifyState = true;
	}

	void SGLFWWindow::UpdateKeyStateForWindow_Internal(GLFWwindow* Handle, int32 Key, int32 Scancode, int32 NewAction, int32 NewModifiers)
	{
		AEON_UNUSED(Scancode);

		auto& Window = *static_cast<SGLFWWindow*>(glfwGetWindowUserPointer(Handle));

		// First update the cached current modifiers

		EKeyModifierState KeyModifierState = EKeyModifierState::None;
		if (NewModifiers & GLFW_MOD_SHIFT)
		{
			KeyModifierState |= EKeyModifierState::Shift;
		}
		if (NewModifiers & GLFW_MOD_CONTROL)
		{
			KeyModifierState |= EKeyModifierState::Control;
		}
		if (NewModifiers & GLFW_MOD_ALT)
		{
			KeyModifierState |= EKeyModifierState::Alt;
		}
		if (NewModifiers & GLFW_MOD_CAPS_LOCK)
		{
			KeyModifierState |= EKeyModifierState::CapsLock;
		}
		if (NewModifiers & GLFW_MOD_NUM_LOCK)
		{
			KeyModifierState |= EKeyModifierState::NumLock;
		}
		Window.CachedKeyModifierState = KeyModifierState;

		// Now run the logic to update the key state, ignoring GLFW_REPEAT events

		if (NewAction == GLFW_PRESS || NewAction == GLFW_RELEASE)
		{
			Window.UpdateKeyState_Internal(EVirtualKey(Key), EKeyDirectEvent(NewAction));
		}
	}

	void SGLFWWindow::UpdateCursorStateForWindow_Internal(GLFWwindow* Handle, int32 Button, int32 NewAction, int32 NewModifiers)
	{
		AEON_UNUSED(NewModifiers);

		auto& Window = *static_cast<SGLFWWindow*>(glfwGetWindowUserPointer(Handle));

		if (NewAction == GLFW_PRESS || NewAction == GLFW_RELEASE)
		{
			Window.UpdateCursorState_Internal(ECursorButton(Button + 1), ECursorDirectEvent(NewAction));
		}
	}

	void SGLFWWindow::UpdateCursorScrollForWindow_Internal(GLFWwindow* Handle, float64 DeltaX, float64 DeltaY)
	{
		auto& Window = *static_cast<SGLFWWindow*>(glfwGetWindowUserPointer(Handle));

		if (Window.IsFocusedFlag)
		{
			SVector2 NewScrollDelta{ float32(DeltaX), float32(DeltaY) };

			Window.CachedCursorScrollDelta = NewScrollDelta;
			Window.BroadcastCursorScrollChanged(NewScrollDelta);
		}
	}

	void SGLFWWindow::UpdateCursorPositionForWindow_Internal(GLFWwindow* Handle, float64 PositionX, float64 PositionY)
	{
		auto& Window = *static_cast<SGLFWWindow*>(glfwGetWindowUserPointer(Handle));

		// FIXME when window is changed programmatically, the cursor position might change without actual movement, resulting in very high deltas;
		//  detect this and zero the delta in this case (idea: maybe set flag in SetPosition/SetSize (and unset it in EnterLeave))

		if (Window.IsFocusedFlag)
		{
			SVector2 NewPosition{ float32(PositionX), float32(PositionY) };
			SVector2 NewPositionDelta = NewPosition - Window.CachedCursorPosition;

			if (Window.CachedCursorMode == ECursorMode::CapturedVirtual)
			{
			//	AEON_LOG(Info, GLFWManager, "Virtual cursor position=({}, {})", PositionX, PositionY);

				auto WrappedPositionX = AeonMath::Wrap<float32>(NewPosition.X, 0.f, float32(Window.CachedSize.X));
				auto WrappedPositionY = AeonMath::Wrap<float32>(NewPosition.Y, 0.f, float32(Window.CachedSize.Y));

				SVector2 WrappedPosition{ WrappedPositionX, WrappedPositionY };
				if (!WrappedPosition.IsApproxEqual(NewPosition))
				{
					AEON_LOG(Info, GLFWManager, "Updating window '{}': wrapping virtual cursor position=({}, {})",
							 Window.CachedTitle, WrappedPositionX, WrappedPositionY);

					// NOTE This won't re-trigger this callback, so we should update the cached position now
					glfwSetCursorPos(Window.WindowHandle, WrappedPositionX, WrappedPositionY);
					NewPosition = WrappedPosition;
				}
			}

			Window.CachedCursorPosition = NewPosition;
			Window.CachedCursorPositionDelta = NewPositionDelta;
			Window.BroadcastCursorPositionChanged(NewPosition, NewPositionDelta);
		}
	}

	void SGLFWWindow::UpdateCursorEnterLeaveForWindow_Internal(GLFWwindow* Handle, int32 HasEntered)
	{
		auto& Window = *static_cast<SGLFWWindow*>(glfwGetWindowUserPointer(Handle));

		if (Window.IsFocusedFlag)
		{
			// Cursor has entered the content area
			if (HasEntered)
			{
				// This avoids huge deltas when cursor leaves and enters in a different part of the window
				// CHECKME This relies on this event being triggered before UpdateCursorPositionForWindow_Internal; ensure this is reliable

				float64 PositionX, PositionY;
				glfwGetCursorPos(Window.WindowHandle, &PositionX, &PositionY);

				AEON_LOG(Info, GLFWManager, "Updating window '{}': Cursor entering at position=({}, {})",
						 Window.CachedTitle, PositionX, PositionY);

				Window.CachedCursorPosition = { float32(PositionX), float32(PositionY) };
			}
			// Cursor has left the content area
			else
			{
				// Nothing to do for now
			}
		}
	}

	uint32 SGLFWWindow::MapVirtualKeyToIndex_Internal(EVirtualKey Key)
	{
#	if AEON_PLATFORM_WINDOWS or AEON_PLATFORM_MACOS or AEON_PLATFORM_LINUX
		uint32 Index = (Key > 0xFF) ? (Key - 0x5D) : Key;
		AEON_ASSERT(Index < KeyStateMapSize);
		return Index;
#	else
#		error Not implemented yet!
#	endif
	}
}
