#include "Aeon/Engine/System/Display/GLFW/GLFWManager.hpp"

#include "Aeon/Engine/System/Display/GLFW/GLFWCommon.hpp"

#include "Aeon/Engine/System/Display/GLFW/GLFWScreen.hpp"
#include "Aeon/Engine/System/Display/GLFW/GLFWWindow.hpp"

namespace Aeon
{
#if AEON_PLATFORM_WINDOWS
	LRESULT CALLBACK GLFWWndProcHook(_In_ int nCode, _In_ WPARAM wParam, _In_ LPARAM lParam)
	{
		if (nCode == HC_ACTION)
		{
			auto& Manager = static_cast<SGLFWManager&>(IWindowManager::Instance());

			auto WndProcInfo = reinterpret_cast<CWPSTRUCT*>(lParam);
		//	AEON_LOG(Info, GLFWManager, "WndMessage: {}", ZzInternal::MakeWindowsMessageString(WndProcInfo->message));
			switch (WndProcInfo->message)
			{
				case WM_INPUTLANGCHANGE:
					AEON_LOG(Info, GLFWManager, "Input lang change!");
					// TODO update current KeyLayout
				//	SInputDeviceManager::
					break;
				case WM_EXITSIZEMOVE:
					if (SGLFWWindow* Wnd = Manager.GetWindowByNativeHandle(WndProcInfo->hwnd))
					{
						Wnd->MarkWindowUpdatingPosSize_HACK();
					}
					break;
			}
		}
		return CallNextHookEx(NULL, nCode, wParam, lParam);
	}
#endif

#if AEON_USING_GLFW_LIBRARY
	IWindowManager& IWindowManager::Instance()
	{
		static SGLFWManager Manager{ SGLFWManager::ConstructorAccess::Tag };
		return Manager;
	}
#endif

	SGLFWManager::SGLFWManager(SGLFWManager::ConstructorAccess)
	{
		// No-op, for now
	}

	bool SGLFWManager::Initialize()
	{
		// Ensure this is run from the main thread
		AEON_ASSERT(SThread::ThisId() == MainThread.Id());

		// Function is idempotent
		if (IsInit)
		{
			return true;
		}

		// TODO check init hints for GLFW here: https://www.glfw.org/docs/latest/intro_guide.html#init_hints

		bool HasInit = glfwInit();

		if (!HasInit)
		{
			char const* Description;
			int ErrorCode = glfwGetError(&Description);

			AEON_LOG(Error, GLFWManager, "Failed to initialize GLFW with error code {:X}: '{}'", ErrorCode, Description);

			return false;
		}

		// Specify that no OpenGL context should be created when creating windows
		// CHECKME this might be different on different platforms, check Vulkan docs and GLFW_CONTEXT_CREATION_API
		glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

		glfwWindowHint(GLFW_DOUBLEBUFFER, GLFW_TRUE);
		// CHECKME do we ever want to limit the refresh rate for a window? (note this is only valid for fullscreen windows)
		glfwWindowHint(GLFW_REFRESH_RATE, GLFW_DONT_CARE);

		glfwSetMonitorCallback(&DetectMonitorConnectionChanged_Internal);

		int ScreenCount;
		GLFWmonitor** ScreenHandleList = glfwGetMonitors(&ScreenCount);

		AEON_LOG(Info, GLFWManager, "Detected screens ({}):", ScreenCount);
		for (int Idx = 0; Idx < ScreenCount; ++Idx)
		{
			GLFWmonitor* GLFWScreenHandle = ScreenHandleList[Idx];

			AddScreen_Internal(GLFWScreenHandle);
		}

#	if AEON_PLATFORM_WINDOWS
		SetWindowsHookExA(WH_CALLWNDPROC, GLFWWndProcHook, NULL, ::GetCurrentThreadId());
#	endif

		IsInit = true;

		return true;
	}

	void SGLFWManager::Terminate()
	{
		AEON_ASSERT(SThread::ThisId() == MainThread.Id());

		// Function is idempotent
		if (!IsInit)
		{
			return;
		}

		glfwTerminate();

		IsInit = false;
	}

	HWindow SGLFWManager::CreateWindow(DWindowInitParams const& Params)
	{
		AEON_ASSERT(SThread::ThisId() == MainThread.Id());
		AEON_ASSERT(IsInit);

		SIntVector2 const& WindowPos = Params.Position;
		SIntVector2 const& WindowSize = Params.Size;

		SIntVector2 WindowCoordMin = WindowPos;
		SIntVector2 WindowCoordMax = WindowPos + WindowSize;

		IScreen* ContainingScreen = nullptr;
		for (auto& ScreenHandle : ScreenHandleArray)
		{
			auto& [_, Screen] = *ScreenMap.Find(ScreenHandle);

			if (Screen->ContainsWindow(WindowCoordMin, WindowCoordMax))
			{
				SIntVector3 BitDepth = Screen->GetBitDepth();
				uint32 RefreshRate = Screen->GetRefreshRate();

				// This is the only chance of configuring the framebuffer through GLFW
				glfwWindowHint(GLFW_RED_BITS, BitDepth.X);
				glfwWindowHint(GLFW_GREEN_BITS, BitDepth.Y);
				glfwWindowHint(GLFW_BLUE_BITS, BitDepth.Z);

				glfwWindowHint(GLFW_REFRESH_RATE, RefreshRate);

				ContainingScreen = Screen.Get();
				break;
			}
		}

		auto WindowPtr = MakeUniquePtr<SGLFWWindow>(SGLFWWindow::ConstructorAccess::Tag, this, ContainingScreen, Params);
		auto WindowHandle = WindowPtr->GetHandle();

		auto& Window = WindowMap.Add(WindowHandle, Move(WindowPtr));
		WindowHandleArray.Add(WindowHandle);

		BroadcastPostWindowCreate(*Window.Value());

		return WindowHandle;
	}

	void SGLFWManager::DestroyWindow(HWindow Handle)
	{
		AEON_ASSERT(SThread::ThisId() == MainThread.Id());
		AEON_ASSERT(IsInit);

		AEON_ASSERT(Handle.IsValid());

		if (auto WindowOpt = WindowMap.Find(Handle))
		{
			SWindowHandle WindowHandle = Handle;
			IWindow& Window = *WindowOpt->Value();

			BroadcastPreWindowDestroy(Window);

			WindowMap.Remove(Handle);

			glfwDestroyWindow(WindowHandle);
		}
	}

	IWindow& SGLFWManager::GetWindow(HWindow Handle)
	{
		AEON_ASSERT(IsInit);

		AEON_ASSERT(Handle.IsValid());

		return *WindowMap[Handle];
	}

	IWindow& SGLFWManager::GetWindow(szint Index)
	{
		AEON_ASSERT(IsInit);

		auto Handle = WindowHandleArray[uint32(Index)];
		return GetWindow(Handle);
	}

	szint SGLFWManager::GetWindowCount() const
	{
		AEON_ASSERT(IsInit);

		return WindowHandleArray.Count();
	}

	SGLFWWindow* SGLFWManager::GetWindowByNativeHandle(HWND NativeHandle)
	{
		auto Handle = WindowHandleArray.Find([NativeHandle] (const HWindow& Handle)
											 { return glfwGetWin32Window(Handle.Access()) == NativeHandle; });
		return Handle ? WindowMap[*Handle].Get() : nullptr;
	}

	bool SGLFWManager::IsWindowValid(HWindow Handle) const
	{
		AEON_ASSERT(IsInit);

		return WindowMap.Contains(Handle);
	}

	IScreen& SGLFWManager::GetScreen(HScreen Handle)
	{
		AEON_ASSERT(IsInit);

		AEON_ASSERT(Handle.IsValid());

		return *ScreenMap[Handle];
	}

	IScreen& SGLFWManager::GetScreen(szint Index)
	{
		AEON_ASSERT(IsInit);

		auto Handle = ScreenHandleArray[uint32(Index)];
		return GetScreen(Handle);
	}

	szint SGLFWManager::GetScreenCount() const
	{
		AEON_ASSERT(IsInit);

		return ScreenHandleArray.Count();
	}

	bool SGLFWManager::IsScreenValid(HScreen Handle) const
	{
		AEON_ASSERT(IsInit);

		return ScreenMap.Contains(Handle);
	}

	IScreen& SGLFWManager::FindMainScreen()
	{
		for (auto& ScreenHandle : ScreenHandleArray)
		{
			auto& Screen = *ScreenMap[ScreenHandle];

			if (Screen.IsMainScreen())
			{
				return Screen;
			}
		}

		AEON_LOG(Warning, GLFWManager, "Could not find main screen!");
		// FIXME This breaks if no screen is connected (maybe add a dummy screen object that is always valid)
		return GetScreen(0);
	}

	IScreen& SGLFWManager::FindScreenContainingWindow(IWindow const& Window)
	{
		for (auto& ScreenHandle : ScreenHandleArray)
		{
			auto& Screen = *ScreenMap[ScreenHandle];

			if (Screen.ContainsWindow(Window))
			{
				return Screen;
			}
		}

		AEON_LOG(Warning, GLFWManager, "Could not find screen containing window '{}'!", Window.GetTitle());
		return FindMainScreen();
	}

	void SGLFWManager::RequestCloseWindow(HWindow Handle)
	{
		// NOTE This is not a requirement: glfwSetWindowShouldClose can be called from any thread, but access is not sync'ed
	//	AEON_ASSERT(SThread::ThisId() == MainThread.Id());
		AEON_ASSERT(IsInit);

		AEON_ASSERT(Handle.IsValid());
		SWindowHandle WindowHandle = Handle;

		glfwSetWindowShouldClose(WindowHandle, GLFW_TRUE);
	}

	bool SGLFWManager::ShouldCloseWindow(HWindow Handle) const
	{
		// NOTE This is not a requirement: glfwWindowShouldClose can be called from any thread, but access is not sync'ed
	//	AEON_ASSERT(SThread::ThisId() == MainThread.Id());
		AEON_ASSERT(IsInit);

		AEON_ASSERT(Handle.IsValid());
		SWindowHandle WindowHandle = Handle;

		return glfwWindowShouldClose(WindowHandle);
	}

	void SGLFWManager::ProcessEvents(EWindowEventsProcessMode ProcessMode)
	{
		AEON_ASSERT(SThread::ThisId() == MainThread.Id());
		AEON_ASSERT(IsInit);

		// CHECKME prob don't want to flush messages every update
#	if AEON_BUILD_DEVELOPMENT or AEON_BUILD_DISTRIBUTION_DEBUG
		auto& LoggerDeviceManager = Aeon::SLoggerDeviceManager::Instance();
		LoggerDeviceManager.FlushLogDevices();
#	endif

		switch (ProcessMode)
		{
			case EWindowEventsProcessMode::NonBlocking:
				// FIXME This can still cause blocks during window moving/resizing, see:
				// https://www.gamedev.net/forums/topic/488074-win32-message-pump-and-opengl---rendering-pauses-while-draggingresizing/
				glfwPollEvents();
				break;
			case EWindowEventsProcessMode::Blocking:
				glfwWaitEvents();
				break;
		//	case EWindowEventsProcessMode::Blocking:
		//		glfwWaitEventsTimeout(Timeout);
		//		break;
		}

		for (auto WindowHandle : WindowHandleArray)
		{
			auto& [_, Window] = *WindowMap.Find(WindowHandle);

			ProcessEventsForWindow(*Window);
		}
	}

	void SGLFWManager::AddScreen_Internal(GLFWmonitor* GLFWScreenHandle)
	{
		auto ScreenPtr = MakeUniquePtr<SGLFWScreen>(SGLFWScreen::ConstructorAccess::Tag, this, GLFWScreenHandle);
		auto ScreenHandle = ScreenPtr->GetHandle();

		auto Resolution = ScreenPtr->GetResolution();
		auto RefreshRate = ScreenPtr->GetRefreshRate();
		auto Pos = ScreenPtr->GetWorkareaPosition();
		auto Size = ScreenPtr->GetWorkareaSize();

		uint32 Index = ScreenHandleArray.Count();

		AEON_LOG(Info, GLFWManager, "Adding screen: Index={} Resolution={}x{} RefreshRate={}Hz | Workarea: Size={}x{} Position=({},{})",
				 Index, Resolution.X, Resolution.Y, RefreshRate, Size.X, Size.Y, Pos.X, Pos.Y);

		ScreenMap.Add(ScreenHandle, Move(ScreenPtr));
		ScreenHandleArray.Add(ScreenHandle);
	}
	void SGLFWManager::RemoveScreen_Internal(GLFWmonitor* GLFWScreenHandle)
	{
		SScreenHandle ScreenHandle{ GLFWScreenHandle };

		if (auto Index = ScreenHandleArray.FindIndex(ScreenHandle))
		{
			AEON_LOG(Info, GLFWManager, "Removing screen: Index={}", *Index);

			// TODO add logic to move windows or do whatever else needed

			ScreenHandleArray.RemoveAt(*Index);
			ScreenMap.Remove(ScreenHandle);
		}
	}

	void SGLFWManager::DetectMonitorConnectionChanged_Internal(GLFWmonitor* Handle, int32 ConnectionStatus)
	{
		auto& Manager = static_cast<SGLFWManager&>(IWindowManager::Instance());

		if (ConnectionStatus == GLFW_CONNECTED)
		{
			Manager.AddScreen_Internal(Handle);
		}
		else // if (ConnectionStatus == GLFW_DISCONNECTED)
		{
			Manager.RemoveScreen_Internal(Handle);
		}
	}
}
