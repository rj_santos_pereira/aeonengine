#include "Aeon/Engine/System/Display/GLFW/GLFWScreen.hpp"

#include "Aeon/Engine/System/Display/GLFW/GLFWManager.hpp"

#include "Aeon/Engine/System/Display/GLFW/GLFWCommon.hpp"

namespace Aeon
{
	SGLFWScreen::SGLFWScreen(ConstructorAccess, SGLFWManager* NewOwningManager, GLFWmonitor* NewScreenHandle)
		: OwningManager{ NewOwningManager }
		, ScreenHandle{ NewScreenHandle }
	{
		glfwGetMonitorWorkarea(ScreenHandle, &CachedWorkareaPosition.X, &CachedWorkareaPosition.Y, &CachedWorkareaSize.X, &CachedWorkareaSize.Y);

		auto VideoMode = glfwGetVideoMode(ScreenHandle);

		Resolution = { VideoMode->width, VideoMode->height };
		BitDepth = { VideoMode->redBits, VideoMode->greenBits, VideoMode->blueBits };
		RefreshRate = VideoMode->refreshRate;
	}

	HScreen SGLFWScreen::GetHandle() const
	{
		return const_cast<SScreenHandle&>(ScreenHandle);
	}
	voidptr SGLFWScreen::GetNativeHandle() const
	{
#	if AEON_PLATFORM_WINDOWS
		// NOTE glfwGetWin32Monitor only returns the name of the monitor, which is not very useful as a handle, so we rather return null instead.
		return nullptr;
#	else
#		error Not implemented yet!
#	endif
	}

	SIntVector2 SGLFWScreen::GetWorkareaPosition() const
	{
		return CachedWorkareaPosition;
	}

	SIntVector2 SGLFWScreen::GetWorkareaSize() const
	{
		return CachedWorkareaSize;
	}

	SVector2 SGLFWScreen::GetContentScale() const
	{
		// NOTE Screen does not have any callback to detect when content scale changes (through the platform),
		// so we never cache it.
		SVector2 ContentScale;
		glfwGetMonitorContentScale(ScreenHandle, &ContentScale.X, &ContentScale.Y);
		return ContentScale;
	}

	SIntVector2 SGLFWScreen::GetResolution() const
	{
		return Resolution;
	}

	SIntVector3 SGLFWScreen::GetBitDepth() const
	{
		return BitDepth;
	}

	int32 SGLFWScreen::GetRefreshRate() const
	{
		return RefreshRate;
	}

	bool SGLFWScreen::ContainsCoord(SIntVector2 Coord) const
	{
		auto const&	ScreenCoordMin = CachedWorkareaPosition;
		auto const	ScreenCoordMax = CachedWorkareaPosition + CachedWorkareaSize;

		return AeonMath::IsClamped(Coord.X, ScreenCoordMin.X, ScreenCoordMax.X)
			&& AeonMath::IsClamped(Coord.Y, ScreenCoordMin.Y, ScreenCoordMax.Y);
	}

	bool SGLFWScreen::ContainsRect(SIntVector2 CoordMin, SIntVector2 CoordMax) const
	{
		AEON_ASSERT(CoordMin.X <= CoordMax.X && CoordMin.Y <= CoordMax.Y);

		auto const&	ScreenCoordMin = CachedWorkareaPosition;
		auto const	ScreenCoordMax = CachedWorkareaPosition + CachedWorkareaSize;

		return ScreenCoordMin.X <= CoordMin.X && CoordMax.X <= ScreenCoordMax.X
			&& ScreenCoordMin.Y <= CoordMin.Y && CoordMax.Y <= ScreenCoordMax.Y;
	}

	bool SGLFWScreen::ContainsWindow(SIntVector2 WindowCoordMin, SIntVector2 WindowCoordMax) const
	{
		AEON_ASSERT(WindowCoordMin.X <= WindowCoordMax.X && WindowCoordMin.Y <= WindowCoordMax.Y);

		auto const& ScreenCoordMin = CachedWorkareaPosition;
		auto const	ScreenCoordMax = CachedWorkareaPosition + CachedWorkareaSize;

		// Compute only if window coords overlap with screen;
		// note that this is subtly different from the check in ContainsRect
		if (ScreenCoordMin.X <= WindowCoordMax.X && WindowCoordMin.X <= ScreenCoordMax.X
			&& ScreenCoordMin.Y <= WindowCoordMax.Y && WindowCoordMin.Y <= ScreenCoordMax.Y)
		{
			auto const WindowSize = WindowCoordMax - WindowCoordMin;

			int32 HalfWindowArea = (WindowSize.X * WindowSize.Y) / 2;

			int32 OverlapAreaX = AeonMath::Min(ScreenCoordMax.X, WindowCoordMax.X) - AeonMath::Max(ScreenCoordMin.X, WindowCoordMin.X);
			int32 OverlapAreaY = AeonMath::Min(ScreenCoordMax.Y, WindowCoordMax.Y) - AeonMath::Max(ScreenCoordMin.Y, WindowCoordMin.Y);

			int32 OverlapArea = OverlapAreaX * OverlapAreaY;

			return OverlapArea >= HalfWindowArea;
		}

		return false;
	}

	bool SGLFWScreen::ContainsWindow(IWindow const& Window) const
	{
		auto const& WindowCoordMin = Window.GetPosition();
		auto const	WindowCoordMax = WindowCoordMin + Window.GetSize();

		return ContainsWindow(WindowCoordMin, WindowCoordMax);
	}

	bool SGLFWScreen::IsMainScreen() const
	{
		// NOTE This is not cached since the system may change it at any point, and GLFW is never informed about it.
		return ScreenHandle.Handle == glfwGetPrimaryMonitor();
	}
}
