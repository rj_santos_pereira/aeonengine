#ifndef ZZINTERNAL_INCLUDE_AEON_ENGINE_SYSTEM_DISPLAY_GLFW_GLFWMANAGER
#define ZZINTERNAL_INCLUDE_AEON_ENGINE_SYSTEM_DISPLAY_GLFW_GLFWMANAGER

#include "Aeon/Framework/Framework.hpp"

#include "Aeon/Engine/System/Display/WindowManager.hpp"

struct GLFWmonitor;
struct GLFWwindow;

namespace Aeon
{
	class SGLFWScreen;
	class SGLFWWindow;

	class SGLFWManager final : public IWindowManager
	{
	public:
		AEON_DECLARE_ACCESS_CLASS(ConstructorAccess, class IWindowManager);

		explicit SGLFWManager(ConstructorAccess);

		virtual bool Initialize() override;
		virtual void Terminate() override;

		virtual HWindow CreateWindow(DWindowInitParams const& Params) override;
		virtual void DestroyWindow(HWindow Handle) override;

		virtual IWindow& GetWindow(HWindow Handle) override;
		virtual IWindow& GetWindow(szint Index) override;
		virtual szint GetWindowCount() const override;
		virtual bool IsWindowValid(HWindow Handle) const override;

		SGLFWWindow* GetWindowByNativeHandle(HWND Handle);

		virtual IScreen& GetScreen(HScreen Handle) override;
		virtual IScreen& GetScreen(szint Index) override;
		virtual szint GetScreenCount() const override;
		virtual bool IsScreenValid(HScreen Handle) const override;

		virtual IScreen& FindMainScreen() override;
		virtual IScreen& FindScreenContainingWindow(IWindow const& Window) override;

		virtual void RequestCloseWindow(HWindow Handle) override;
		virtual bool ShouldCloseWindow(HWindow Handle) const override;

		virtual void ProcessEvents(EWindowEventsProcessMode ProcessMode = EWindowEventsProcessMode::NonBlocking) override;

	private:
		void AddScreen_Internal(GLFWmonitor* GLFWScreenHandle);
		void RemoveScreen_Internal(GLFWmonitor* GLFWScreenHandle);

		static void DetectMonitorConnectionChanged_Internal(GLFWmonitor* Handle, int32 ConnectionStatus);

		// TODO remove these arrays
		GArray<HScreen> ScreenHandleArray;
		GArray<HWindow> WindowHandleArray;

		GHashMap<HScreen, GUniquePtr<SGLFWScreen>> ScreenMap;
		GHashMap<HWindow, GUniquePtr<SGLFWWindow>> WindowMap;

		bool IsInit : 1 = false;
	};
}

#endif