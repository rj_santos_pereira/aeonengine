#ifndef ZZINTERNAL_INCLUDE_AEON_ENGINE_SYSTEM_DISPLAY_GLFW_GLFWSCREEN
#define ZZINTERNAL_INCLUDE_AEON_ENGINE_SYSTEM_DISPLAY_GLFW_GLFWSCREEN

#include "Aeon/Framework/Framework.hpp"

#include "Aeon/Engine/System/Display/Screen.hpp"

struct GLFWmonitor;

namespace Aeon
{
	class SGLFWManager;

	struct SScreenHandle
	{
		explicit SScreenHandle() = default;
		SScreenHandle(GLFWmonitor* NewHandle)
			: Handle{ NewHandle }
		{
		}

		bool operator==(SScreenHandle const&) const = default;
		strong_ordering operator<=>(SScreenHandle const&) const = default;

		operator GLFWmonitor*() const
		{
			return Handle;
		}

		GLFWmonitor* Handle = nullptr;
	};

#if 0
	template <class HshType>
	struct GDefaultObjectHasher<SScreenHandle, HshType>
	{
		using ObjectType = SScreenHandle;
		using HashType = HshType;

		AEON_FORCEINLINE static HashType Hash(ObjectType const& Object)
		{
			GLFWmonitor* GLFWHandle = Object;
			return Aeon::FNV1aHash<HashType>(reinterpret_cast<cbyteptr>(addressof(GLFWHandle)), sizeof(GLFWHandle));
		}
	};
#endif

	class SGLFWScreen final : public IScreen
	{
	public:
		AEON_DECLARE_ACCESS_CLASS(ConstructorAccess, class SGLFWManager);

		explicit SGLFWScreen(ConstructorAccess, SGLFWManager* NewOwningManager, GLFWmonitor* NewScreenHandle);

		virtual HScreen GetHandle() const override;
		virtual voidptr GetNativeHandle() const override;

		virtual SIntVector2 GetWorkareaPosition() const override;
		virtual SIntVector2 GetWorkareaSize() const override;
		virtual SVector2 GetContentScale() const override;

		virtual SIntVector2 GetResolution() const override;
		virtual SIntVector3 GetBitDepth() const override;
		virtual int32 GetRefreshRate() const override;

		virtual bool ContainsCoord(SIntVector2 Coord) const override;
		virtual bool ContainsRect(SIntVector2 CoordMin, SIntVector2 CoordMax) const override;

		// Check if window is contained in this screen, by checking whether the screen contains at least half of the window area
		virtual bool ContainsWindow(SIntVector2 WindowCoordMin, SIntVector2 WindowCoordMax) const override;
		virtual bool ContainsWindow(IWindow const& Window) const override;

		virtual bool IsMainScreen() const override;

	private:
		SGLFWManager* OwningManager;
		SScreenHandle ScreenHandle;

		SIntVector2 CachedWorkareaPosition;
		SIntVector2 CachedWorkareaSize;

		SIntVector2 Resolution;
		SIntVector3 BitDepth;
		int32 RefreshRate;

	};
}

#endif