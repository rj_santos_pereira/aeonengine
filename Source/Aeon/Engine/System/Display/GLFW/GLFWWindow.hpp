#ifndef ZZINTERNAL_INCLUDE_AEON_ENGINE_SYSTEM_DISPLAY_GLFW_GLFWWINDOW
#define ZZINTERNAL_INCLUDE_AEON_ENGINE_SYSTEM_DISPLAY_GLFW_GLFWWINDOW

#include "Aeon/Framework/Framework.hpp"

#include "Aeon/Engine/System/Display/Window.hpp"
#include "Aeon/Engine/System/Display/GLFW/GLFWScreen.hpp"

struct GLFWwindow;

namespace Aeon
{
	class SGLFWManager;

	struct SWindowHandle
	{
		explicit SWindowHandle() = default;
		SWindowHandle(GLFWwindow* NewHandle)
			: Handle{NewHandle}
		{
		}

		bool operator==(SWindowHandle const&) const = default;
		strong_ordering operator<=>(SWindowHandle const&) const = default;

		operator GLFWwindow*() const
		{
			return Handle;
		}

		GLFWwindow* Handle = nullptr;
	};

#if 0
	template <CUnsignedInteger HshType>
	struct GDefaultObjectHasher<SWindowHandle, HshType>
	{
		using ObjectType = SWindowHandle;
		using HashType = HshType;

		AEON_FORCEINLINE static HashType Hash(ObjectType const& Object)
		{
			GLFWwindow* GLFWHandle = Object;
			return Aeon::FNV1aHash<HashType>(reinterpret_cast<cbyteptr>(addressof(GLFWHandle)), sizeof(GLFWHandle));
		}
	};
#endif

	struct DKeyStateData
	{
		// TODO ideally, instead of this, we would query some engine-global class that manages frame time,
		//  as that likely will have enough resolution for what we want
		SLightweightStopwatch EventStopwatch;
		SLightweightStopwatch RepeatEventStopwatch;

		EKeyEvent CachedEvent : 8 = EKeyEvent::None;
		EKeyDirectEvent CachedDirectEvent : 1 = EKeyDirectEvent::Up;

		uint8 RepeatEventCount : 4 = 0;
	};

	struct DCursorStateData
	{
		SLightweightStopwatch EventStopwatch;
		SLightweightStopwatch RepeatEventStopwatch;

		ECursorEvent CachedEvent : 4 = ECursorEvent::None;
		ECursorDirectEvent CachedDirectEvent : 1 = ECursorDirectEvent::Up;

		uint8 RepeatEventCount : 2 = 0;
	};

	class SGLFWWindow final : public IWindow
	{
	public:
		AEON_DECLARE_ACCESS_CLASS(ConstructorAccess, class SGLFWManager);

		explicit SGLFWWindow(ConstructorAccess, SGLFWManager* NewOwningManager, IScreen* NewContainingScreen, DWindowInitParams const& Params);

		// IWindow interface

		virtual HWindow GetHandle() const override;
		virtual voidptr GetNativeHandle() const override;

		virtual IScreen& GetContainingScreen() const override;

		virtual SStringView GetTitle() const override;

		virtual SIntVector2 GetPosition() const override;
		virtual SIntVector2 GetSize() const override;

		virtual SIntVector2 GetFramebufferSize() const override;

		virtual bool IsFocused() const override;
		virtual bool IsMaximized() const override;
		virtual bool IsMinimized() const override;
		virtual bool IsFullscreen() const override;

		virtual bool IsVisible() const override;
		virtual bool IsResizable() const override;

		virtual bool HasTitleBar() const override;
		virtual bool IsAlwaysOnTop() const override;

		virtual bool UsesContentScaling() const override;
		virtual bool UsesRawInputForCursor() const override;

		virtual void SetTitle(SStringView NewTitle) override;

		virtual void SetPosition(SIntVector2 NewPosition) override;
		virtual void SetSize(SIntVector2 NewSize) override;

		virtual void ToggleFocus() override;

		virtual void ToggleMaximized(bool NewMaximized) override;
		virtual void ToggleMinimized(bool NewMinimized) override;

		virtual void ToggleFullscreen(bool NewFullscreen) override;

		virtual void ToggleVisible(bool NewVisible) override;
		virtual void ToggleResizable(bool NewResizable) override;

		virtual void ProcessEvents(ManagerAccess) override;

		void MarkWindowUpdatingPosSize_HACK();

		// IInputDispatcher interface

		virtual EKeyEvent QueryKeyEvent(EVirtualKey Key) const override;
		virtual EKeyDirectEvent QueryKeyState(EVirtualKey Key) const override;
		virtual EKeyModifierState QueryKeyModifierState() const override;

		virtual ECursorEvent QueryCursorEvent(ECursorButton Button) const override;
		virtual ECursorDirectEvent QueryCursorState(ECursorButton Button) const override;

		virtual ECursorMode GetCursorMode() const override;

		virtual SVector2 QueryCursorPosition() const override;
		virtual SVector2 QueryCursorPositionDelta() const override;

		virtual SVector2 QueryCursorScrollDelta() const override;

		virtual DKeyEventParams GetKeyEventParams() const override;
		virtual DCursorEventParams GetCursorEventParams() const override;

		virtual void SetCursorMode(ECursorMode NewCursorMode) override;

		virtual void SetKeyEventParams(DKeyEventParams NewParams) override;
		virtual void SetCursorEventParams(DCursorEventParams NewParams) override;

	private:
		void SetupFullscreen_Internal();

		void UpdateKeyState_Internal(EVirtualKey Key, EKeyDirectEvent NewDirectEvent);
		void UpdatePendingKeyState_Internal();

		void UpdateCursorState_Internal(ECursorButton Button, ECursorDirectEvent NewDirectEvent);
		void UpdatePendingCursorState_Internal();

		static void UpdateContentScaleForWindow_Internal(GLFWwindow* Handle, float32 ScaleX, float32 ScaleY);
		static void UpdatePositionForWindow_Internal(GLFWwindow* Handle, int32 PositionX, int32 PositionY);
		static void UpdateSizeForWindow_Internal(GLFWwindow* Handle, int32 SizeX, int32 SizeY);

		static void UpdateFocusForWindow_Internal(GLFWwindow* Handle, int32 NewFocused);

		static void UpdateMaximizedForWindow_Internal(GLFWwindow* Handle, int32 NewMaximized);
		static void UpdateMinimizedForWindow_Internal(GLFWwindow* Handle, int32 NewMinimized);

		static void UpdateKeyStateForWindow_Internal(GLFWwindow* Handle, int32 Key, int32 Scancode, int32 NewAction, int32 NewModifiers);

		static void UpdateCursorStateForWindow_Internal(GLFWwindow* Handle, int32 Button, int32 NewAction, int32 NewModifiers);

		static void UpdateCursorScrollForWindow_Internal(GLFWwindow* Handle, float64 DeltaX, float64 DeltaY);
		static void UpdateCursorPositionForWindow_Internal(GLFWwindow* Handle, float64 PositionX, float64 PositionY);

		static void UpdateCursorEnterLeaveForWindow_Internal(GLFWwindow* Handle, int32 HasEntered);

		static uint32 MapVirtualKeyToIndex_Internal(EVirtualKey Key);

		SGLFWManager* OwningManager;
		IScreen* CachedContainingScreen;
		SWindowHandle WindowHandle;

#	if AEON_PLATFORM_WINDOWS or AEON_PLATFORM_MACOS or AEON_PLATFORM_LINUX
		// NOTE Based on https://www.glfw.org/docs/latest/group__keys.html
		//  This is compressed from 350 to 256,
		//	which we do by offsetting values equal to and greater than 0x100 (Escape) by 0x5D:
		//  this value depends on Backslash (0xA2) having the highest value until Escape (0x100);
		//  it's thus computed as 0xFF - 0xA2 == 0x5D
		static constexpr szint KeyStateMapSize = 256;
#	else
#		error Not implemented yet!
#	endif

		// NOTE Based on https://www.glfw.org/docs/latest/group__buttons.html
		//  This is expanded from 8 to 16; this allows some additional inputs for some specialized devices
		static constexpr szint CursorStateMapSize = 16;

		// NOTE Setting limits of 20 simultaneous key events and 10 simultaneous cursor events;
		//  as there are only (typically) 5 fingers in each human hand (or foot), this gives plenty of leeway on both events.

		GStaticArray<DKeyStateData, KeyStateMapSize> KeyStateMap;
		GStaticArray<EVirtualKey, 20> PendingKeyStateArray;

		GStaticArray<DCursorStateData, CursorStateMapSize> CursorStateMap;
		GStaticArray<ECursorButton, 10> PendingCursorStateArray;

		SVector2 CachedCursorPosition;
		SVector2 CachedCursorPositionDelta;

		SVector2 CachedCursorScrollDelta;

		// TODO Gamepad state map

		DKeyEventParams KeyEventParams;
		DCursorEventParams CursorEventParams;

		EKeyModifierState CachedKeyModifierState : 6 = EKeyModifierState::None;
		ECursorMode CachedCursorMode : 3 = ECursorMode::Free;

		SStringView CachedTitle;

		SVector2 CachedContentScale;
		SIntVector2 CachedPosition;
		SIntVector2 CachedSize;
	//	SIntVector2 CachedFramebufferSize;

		SIntVector2 CachedWindowedPosition;
		SIntVector2 CachedWindowedSize;

		bool IsUpdatingContentScale : 1;
		bool IsUpdatingPosition : 1;
		bool IsUpdatingSize : 1;

		bool IsUpdatingIconifyState : 1;
		bool IsSettingUpFullscreen_HACK : 1;
		bool IsUpdatingPosSize_HACK : 1;

		bool IsFocusedFlag : 1;
		bool IsMaximizedFlag : 1;
		bool IsMinimizedFlag : 1;
		bool IsFullscreenFlag : 1;

		bool IsVisibleFlag : 1;
		bool IsResizableFlag : 1;

		bool HasTitleBarFlag : 1;
		bool IsAlwaysOnTopFlag : 1;
		bool UseContentScalingFlag : 1;
		bool UseRawInputForCursorFlag : 1;

	};
}

#endif