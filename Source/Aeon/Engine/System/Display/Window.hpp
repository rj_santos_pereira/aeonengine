#ifndef ZZINTERNAL_INCLUDE_AEON_ENGINE_SYSTEM_DISPLAY_WINDOW
#define ZZINTERNAL_INCLUDE_AEON_ENGINE_SYSTEM_DISPLAY_WINDOW

#include "Aeon/Framework/Framework.hpp"

#include "Aeon/Engine/System/Input/InputDispatcher.hpp"

namespace Aeon
{
	class IScreen;

	struct SWindowHandle;

	using HWindow = GOpaque<SWindowHandle>;

	struct DWindowInitParams
	{
		SStringView Title 				= "Aeon Window";

		// TODO add icon texture reference

		SIntVector2 Position			= { 20, 40 };
		SIntVector2 Size				= { 1280, 720 };

		ECursorMode CursorMode : 3		= ECursorMode::Free;

		bool IsFocused : 1 				= true;
		bool IsFullscreen : 1 			= false;

		bool IsVisible : 1 				= true;
		bool IsResizable : 1 			= true;
	//	bool ShouldAutoIconify : 1		= false;

		// NOTE The following parameters are only configurable during init.

		bool HasTitleBar : 1 			= true;
		bool IsAlwaysOnTop : 1 			= false;

		bool UseContentScaling : 1		= false;
		// This only applies to cursor mode 'CapturedVirtual'
		bool UseRawInputForCursor : 1	= true;

	};

	AEON_DECLARE_FLAG_ENUM(EFramebufferChange, uint8)
	(
		AEON_FLAG_ENUM_RULE_AS_IS,
		None 							= 0b000,
		Size 							= 0b001,
		Position						= 0b010,
		Orientation						= 0b100,
	);

	class IWindow : public IInputDispatcher
	{
	protected:
		IWindow() = default;

		IWindow(IWindow const&) = delete;
		IWindow(IWindow&&) = delete;

		IWindow& operator=(IWindow const&) = delete;
		IWindow& operator=(IWindow&&) = delete;

		~IWindow() = default;

	public:
		AEON_DECLARE_ACCESS_CLASS(ManagerAccess, class IWindowManager);

		AEON_DECLARE_EVENT_DELEGATE(OnFramebufferChangedDelegate, IWindow, void(EFramebufferChange, SIntVector2, SIntVector2, SEulerRotation));
		OnFramebufferChangedDelegate OnFramebufferChanged;

		// TODO make delegates for OnPositionChanged, OnSizeChanged, OnFocused, OnMinimized, OnMaximized, OnFullscreenToggled, OnVisibilityChanged, etc

		virtual HWindow GetHandle() const = 0;
		virtual voidptr GetNativeHandle() const = 0;

		virtual IScreen& GetContainingScreen() const = 0;

		virtual SStringView GetTitle() const = 0;

		virtual SIntVector2 GetPosition() const = 0;
		virtual SIntVector2 GetSize() const = 0;

		virtual SIntVector2 GetFramebufferSize() const = 0;

		virtual bool IsFocused() const = 0;
		virtual bool IsMaximized() const = 0;
		virtual bool IsMinimized() const = 0;
		virtual bool IsFullscreen() const = 0;

		virtual bool IsVisible() const = 0;
		virtual bool IsResizable() const = 0;

		virtual bool HasTitleBar() const = 0;
		virtual bool IsAlwaysOnTop() const = 0;

	//	virtual bool ShouldAutoIconify() const = 0;
	//	virtual bool ShouldFocusOnShow() const = 0;

		virtual bool UsesContentScaling() const = 0;
		virtual bool UsesRawInputForCursor() const = 0;

		virtual void SetTitle(SStringView NewTitle) = 0;

		virtual void SetPosition(SIntVector2 NewPosition) = 0;
		virtual void SetSize(SIntVector2 NewSize) = 0;

		virtual void ToggleFocus() = 0;
		virtual void ToggleMaximized(bool NewMaximized) = 0;
		virtual void ToggleMinimized(bool NewMinimized) = 0;
		virtual void ToggleFullscreen(bool NewFullscreen) = 0;

		virtual void ToggleVisible(bool NewVisible) = 0;
		virtual void ToggleResizable(bool NewResizable) = 0;
	//	virtual void ToggleTitleBar(bool NewDecorated) = 0;
	//	virtual void ToggleAlwaysOnTop(bool NewAlwaysOnTop) = 0;

	//	virtual void ToggleAutoIconify(bool NewAutoIconify) = 0;
	//	virtual void ToggleFocusOnShow(bool NewFocusOnShow) = 0;

		// CHECKME Maybe pass delta time as parameter in the future
		virtual void ProcessEvents(ManagerAccess) = 0;

		// IInputDispatcher interface

		virtual EKeyEvent QueryKeyEvent(EVirtualKey Key) const = 0;
		virtual EKeyDirectEvent QueryKeyState(EVirtualKey Key) const = 0;
		virtual EKeyModifierState QueryKeyModifierState() const = 0;

		virtual ECursorEvent QueryCursorEvent(ECursorButton Button) const = 0;
		virtual ECursorDirectEvent QueryCursorState(ECursorButton Button) const = 0;

		virtual ECursorMode GetCursorMode() const = 0;

		virtual SVector2 QueryCursorPosition() const = 0;
		virtual SVector2 QueryCursorPositionDelta() const = 0;
		virtual SVector2 QueryCursorScrollDelta() const = 0;

		virtual DKeyEventParams GetKeyEventParams() const = 0;
		virtual DCursorEventParams GetCursorEventParams() const = 0;

		virtual void SetCursorMode(ECursorMode NewCursorMode) = 0;

		virtual void SetKeyEventParams(DKeyEventParams NewParams) = 0;
		virtual void SetCursorEventParams(DCursorEventParams NewParams) = 0;

	protected:
		void BroadcastFramebufferUpdated(EFramebufferChange Change, SIntVector2 NewSize, SIntVector2 NewPosition, SEulerRotation NewOrientation)
		{
			OnFramebufferChanged.Broadcast(OnFramebufferChangedDelegate::BroadcasterAccess::Tag, Change, NewSize, NewPosition, NewOrientation);
		}

	};
}

#endif