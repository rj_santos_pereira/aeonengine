#ifndef ZZINTERNAL_INCLUDE_AEON_ENGINE_SYSTEM_DISPLAY_WINDOWMANAGER
#define ZZINTERNAL_INCLUDE_AEON_ENGINE_SYSTEM_DISPLAY_WINDOWMANAGER

#include "Aeon/Framework/Framework.hpp"

#include "Aeon/Engine/System/Display/Screen.hpp"
#include "Aeon/Engine/System/Display/Window.hpp"

#if AEON_PLATFORM_WINDOWS
#	undef CreateWindow
#endif

namespace Aeon
{
	enum struct EWindowEventsProcessMode
	{
		NonBlocking,
		Blocking,
	};

	class IWindowManager
	{
	protected:
		IWindowManager() = default;

		IWindowManager(IWindowManager const&) = delete;
		IWindowManager(IWindowManager&&) = delete;

		IWindowManager& operator=(IWindowManager const&) = delete;
		IWindowManager& operator=(IWindowManager&&) = delete;

		~IWindowManager() = default;

	public:
		AEON_DECLARE_EVENT_DELEGATE(OnPostWindowCreateDelegate, IWindowManager, void(IWindow&));
		OnPostWindowCreateDelegate OnPostWindowCreate;
		AEON_DECLARE_EVENT_DELEGATE(OnPreWindowDestroyDelegate, IWindowManager, void(IWindow&));
		OnPreWindowDestroyDelegate OnPreWindowDestroy;

		static IWindowManager& Instance();

		virtual bool Initialize() = 0;
		virtual void Terminate() = 0;

		virtual HWindow CreateWindow(DWindowInitParams const& Params) = 0;
		virtual void DestroyWindow(HWindow Handle) = 0;

		virtual IWindow& GetWindow(HWindow Handle) = 0;
		virtual IWindow& GetWindow(szint Index) = 0;
		virtual szint GetWindowCount() const = 0;
		virtual bool IsWindowValid(HWindow Handle) const = 0;

		virtual IScreen& GetScreen(HScreen Handle) = 0;
		virtual IScreen& GetScreen(szint Index) = 0;
		virtual szint GetScreenCount() const = 0;
		virtual bool IsScreenValid(HScreen Handle) const = 0;

		virtual IScreen& FindMainScreen() = 0;
		virtual IScreen& FindScreenContainingWindow(IWindow const& Window) = 0;

		virtual void RequestCloseWindow(HWindow Handle) = 0;
		virtual bool ShouldCloseWindow(HWindow Handle) const = 0;

		virtual void ProcessEvents(EWindowEventsProcessMode ProcessMode = EWindowEventsProcessMode::NonBlocking) = 0;

	protected:
		void BroadcastPostWindowCreate(IWindow& Window)
		{
			OnPostWindowCreate.Broadcast(OnPostWindowCreateDelegate::BroadcasterAccess::Tag, Window);
		}
		void BroadcastPreWindowDestroy(IWindow& Window)
		{
			OnPreWindowDestroy.Broadcast(OnPreWindowDestroyDelegate::BroadcasterAccess::Tag, Window);
		}

		// FIXME Pass delta time as parameter in the future
		void ProcessEventsForWindow(IWindow& Window)
		{
			Window.ProcessEvents(IWindow::ManagerAccess::Tag);
		}

	};
}

#endif