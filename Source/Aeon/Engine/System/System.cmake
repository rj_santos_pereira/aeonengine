add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Display)
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Filesystem)
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Input)

#target_sources(${PROJECT_NAME}
#	PUBLIC
#		${CMAKE_CURRENT_SOURCE_DIR}/
#	PRIVATE
#		${CMAKE_CURRENT_SOURCE_DIR}/
#	)