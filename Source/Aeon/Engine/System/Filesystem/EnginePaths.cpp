#include "Aeon/Engine/System/Filesystem/EnginePaths.hpp"

namespace Aeon
{
	void SEnginePaths::SetupEnginePaths(SPath const& NewBasePath)
	{
		AEON_FAILSAFE(!ExecDirPath.IsValid(), return, "Engine paths already setup!");

		AEON_FAILSAFE(NewBasePath.IsValid(), return, "Invalid base path!");

		ExecDirPath = NewBasePath;
		ShaderDirPath = ExecDirPath / PATHSTR("Shader");
	}

	SPath const& SEnginePaths::GetExecDirectory()
	{
		return ExecDirPath;
	}

	SPath const& SEnginePaths::GetShaderDirectory()
	{
		return ShaderDirPath;
	}
}
