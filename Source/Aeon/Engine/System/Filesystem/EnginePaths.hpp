#ifndef ZZINTERNAL_INCLUDE_AEON_ENGINE_SYSTEM_DIRECTORY_ENGINEPATHS
#define ZZINTERNAL_INCLUDE_AEON_ENGINE_SYSTEM_DIRECTORY_ENGINEPATHS

#include "Aeon/Framework/Framework.hpp"

namespace Aeon
{
	class SEnginePaths
	{
	public:
		static void SetupEnginePaths(SPath const& NewBasePath);

		static SPath const& GetExecDirectory();

		static SPath const& GetShaderDirectory();

	private:
		static inline SPath ExecDirPath;
	//	static inline SPath AssetPath;
		static inline SPath ShaderDirPath;
	};
}

#endif