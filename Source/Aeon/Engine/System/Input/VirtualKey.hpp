#ifndef ZZINTERNAL_INCLUDE_AEON_ENGINE_SYSTEM_INPUT_VIRTUALKEY
#define ZZINTERNAL_INCLUDE_AEON_ENGINE_SYSTEM_INPUT_VIRTUALKEY

#include "Aeon/Framework/Framework.hpp"

namespace Aeon
{

#if 0
	enum struct EVirtualKey 	: uint8
	{
		// Enumerators are named after the primary key in a US layout (QWERTY) keyboard obeying ISO 9995,
		// reference: https://en.wikipedia.org/wiki/Keyboard_layout#/media/File:Qwerty.svg
		// Keys have values corresponding to the key's position in the ISO layout,
		// with respect to the reference grid, ranging from 30 to 3F, from 50 to 5F, and from A0 to FF;
		// note that key positions range from 99 to 14 (wrapping to 0) on the alphanumeric section,
		// we use F to represent 99 instead; for keys occupying more than one position in the grid,
		// we use the smallest number in the range (with 99 being considered the smallest);
		// for more info, see: https://en.wikipedia.org/wiki/ISO/IEC_9995#ISO/IEC_9995-2

		// Control section

		Key_Escape 				= 0xF0,

		Key_F1 					= 0xF1,
		Key_F2 					= 0xF2,
		Key_F3 					= 0xF3,
		Key_F4 					= 0xF4,

		Key_F5 					= 0xF5,
		Key_F6 					= 0xF6,
		Key_F7 					= 0xF7,
		Key_F8 					= 0xF8,

		Key_F9 					= 0xF9,
		Key_F10 				= 0xFA,
		Key_F11 				= 0xFB,
		Key_F12 				= 0xFC,

		Key_PrintScreen			= 0xFD,
		Key_ScreenLock 			= 0xFE,
		Key_Pause				= 0xFF,

		// Row E per ISO 9995 (alphanumeric section, number row)

		Key_Grave				= 0xE0,
		Key_1					= 0xE1,
		Key_2					= 0xE2,
		Key_3					= 0xE3,
		Key_4					= 0xE4,
		Key_5					= 0xE5,
		Key_6					= 0xE6,
		Key_7					= 0xE7,
		Key_8					= 0xE8,
		Key_9					= 0xE9,
		Key_0					= 0xEA,
		Key_Hyphen				= 0xEB,
		Key_Equals				= 0xEC,
	//	Key_Backslash			= 0xED,
		Key_Backspace			= 0xED,

		// Row D per ISO 9995 (alphanumeric section, letter top row)

		Key_Tab					= 0xD0,
		Key_Q					= 0xD1,
		Key_W					= 0xD2,
		Key_E					= 0xD3,
		Key_R					= 0xD4,
		Key_T					= 0xD5,
		Key_Y					= 0xD6,
		Key_U					= 0xD7,
		Key_I					= 0xD8,
		Key_O					= 0xD9,
		Key_P					= 0xDA,
		Key_LeftBracket 		= 0xDB,
		Key_RightBracket 		= 0xDC,
		Key_RESERVED_DD			= 0xDD,

		// Row C per ISO 9995 (alphanumeric section, letter home row)

		Key_CapsLock			= 0xC0,
		Key_A					= 0xC1,
		Key_S					= 0xC2,
		Key_D					= 0xC3,
		Key_F					= 0xC4,
		Key_G					= 0xC5,
		Key_H					= 0xC6,
		Key_J					= 0xC7,
		Key_K					= 0xC8,
		Key_L					= 0xC9,
		Key_Semicolon			= 0xCA,
		Key_Apostrophe			= 0xCB,
		Key_NumberSign 			= 0xCC,
		Key_Enter				= 0xCD,

		// Row B per ISO 9995 (alphanumeric section, letter bottom row)

		Key_LeftShift 			= 0xBF,
		Key_Backslash 			= 0xB0,
		Key_Z 					= 0xB1,
		Key_X 					= 0xB2,
		Key_C 					= 0xB3,
		Key_V 					= 0xB4,
		Key_B 					= 0xB5,
		Key_N 					= 0xB6,
		Key_M 					= 0xB7,
		Key_Comma				= 0xB8,
		Key_Period				= 0xB9,
		Key_Slash				= 0xBA,
		Key_RESERVED_BB			= 0xBB,
		Key_RightShift			= 0xBC,

		// Row A per ISO 9995 (alphanumeric section, function row)

		Key_LeftControl			= 0xAF,
		Key_LeftSystem 			= 0xA0,
		Key_Alt 				= 0xA1,
		Key_RESERVED_A2 		= 0xA2,
		Key_Space 				= 0xA3,
		Key_RESERVED_A8 		= 0xA8,
		Key_AltGraph 			= 0xA9,
		Key_RightSystem 		= 0xAA,
		Key_Menu 				= 0xAB,
		Key_RightControl 		= 0xAC,

		// Editing section (grid 30 to 32)

		Key_Insert				= 0x30,
		Key_Home				= 0x31,
		Key_PageUp				= 0x32,
		Key_Delete				= 0x33,
		Key_End					= 0x34,
		Key_PageDown			= 0x35,
	//	Key_EMPTY 				= 0x36,
	//	Key_EMPTY 				= 0x37,
	//	Key_EMPTY 				= 0x38,

	//	Key_EMPTY 				= 0x39,
		Key_Up					= 0x3A,
	//	Key_EMPTY 				= 0x3B,
		Key_Left				= 0x3C,
		Key_Down				= 0x3D,
		Key_Right				= 0x3E,

		// Numeric section (grid 51 to 54)

		Key_NumLock				= 0x3F,

		Key_Num0				= 0x50,
		Key_Num1				= 0x51,
		Key_Num2				= 0x52,
		Key_Num3				= 0x53,
		Key_Num4				= 0x54,
		Key_Num5				= 0x55,
		Key_Num6				= 0x56,
		Key_Num7				= 0x57,
		Key_Num8				= 0x58,
		Key_Num9				= 0x59,

		Key_NumSeparator 		= 0x5A,
		Key_NumPlus				= 0x5B,
		Key_NumMinus			= 0x5C,
		Key_NumMultiply			= 0x5D,
		Key_NumDivide 			= 0x5E,
		Key_NumEnter 			= 0x5F,

	};
#endif

	// NOTE Enumerators are named after the primary key in a 105-keys keyboard (QWERTY/UK layout) obeying ISO 9995,
	//  and the alphanumeric section is organized from top to bottom row;
	//  References: https://en.wikipedia.org/wiki/ISO/IEC_9995
	//  https://en.wikipedia.org/wiki/Keyboard_layout#/media/File:ISO_keyboard_(105)_QWERTY_UK.svg

	// NOTE Based on GLFW values for virtual keys: https://www.glfw.org/docs/latest/group__keys.html
	//  Virtual keys are platform-dependent, but layout-independent
	// CHECKME Ensure this works on all platforms that use GLFW, currently Windows, macOS and Linux (Wayland and X11)

#if AEON_PLATFORM_WINDOWS or AEON_PLATFORM_MACOS or AEON_PLATFORM_LINUX
	// FIXME might need to check which display server is being used when in Linux
	struct ZzInternal_EGLFWVirtualKey
	{
		// NOTE If any value is ever updated, review SGLFWWindow::MapVirtualKeyToIndex_Internal

		enum Type 												: uint32
		{
			Null												= 0x0,

			// Control section

			Key_Escape 											= 0x100,

			Key_F1 												= 0x122,
			Key_F2 												= 0x123,
			Key_F3 												= 0x124,
			Key_F4 												= 0x125,

			Key_F5 												= 0x126,
			Key_F6 												= 0x127,
			Key_F7 												= 0x128,
			Key_F8 												= 0x129,

			Key_F9 												= 0x12A,
			Key_F10 											= 0x12B,
			Key_F11 											= 0x12C,
			Key_F12 											= 0x12D,

			Key_PrintScreen										= 0x11B,
			Key_ScreenLock 										= 0x119,
			Key_Pause											= 0x11C,

			// Alphanumeric section, number row (Row E per ISO 9995)

			Key_Grave											= 0x60, // ASCII
			Key_1												= 0x31, // ASCII
			Key_2												= 0x32, // ASCII
			Key_3												= 0x33, // ASCII
			Key_4												= 0x34, // ASCII
			Key_5												= 0x35, // ASCII
			Key_6												= 0x36, // ASCII
			Key_7												= 0x37, // ASCII
			Key_8												= 0x38, // ASCII
			Key_9												= 0x39, // ASCII
			Key_0												= 0x30, // ASCII
			Key_Hyphen											= 0x2D, // ASCII
			Key_Equals											= 0x3D, // ASCII
		//	Key_Backslash										= 0x5C, // ASCII // TODO test this
			Key_Backspace										= 0x103,

			// Alphanumeric section, letter top row (Row D per ISO 9995)

			Key_Tab												= 0x102,
			Key_Q												= 0x51, // ASCII
			Key_W												= 0x57, // ASCII
			Key_E												= 0x45, // ASCII
			Key_R												= 0x52, // ASCII
			Key_T												= 0x54, // ASCII
			Key_Y												= 0x59, // ASCII
			Key_U												= 0x55, // ASCII
			Key_I												= 0x49, // ASCII
			Key_O												= 0x4F, // ASCII
			Key_P												= 0x50, // ASCII
			Key_LeftBracket 									= 0x5B, // ASCII
			Key_RightBracket 									= 0x5D, // ASCII
		//	Key_Tilde											= 0x7E, // ASCII // TODO test this

			// Alphanumeric section, letter home row (Row C per ISO 9995)

			Key_CapsLock										= 0x118,
			Key_A												= 0x41, // ASCII
			Key_S												= 0x53, // ASCII
			Key_D												= 0x44, // ASCII
			Key_F												= 0x46, // ASCII
			Key_G												= 0x47, // ASCII
			Key_H												= 0x48, // ASCII
			Key_J												= 0x4A, // ASCII
			Key_K												= 0x4B, // ASCII
			Key_L												= 0x4C, // ASCII
			Key_Semicolon										= 0x3B, // ASCII
			Key_Apostrophe										= 0x27, // ASCII
			Key_NumberSign 										= 0x5C, // ASCII
			Key_Enter											= 0x101,

			// Alphanumeric section, letter bottom row (Row B per ISO 9995)

			Key_LeftShift										= 0x154,
			Key_Backslash 										= 0xA2,
			Key_Z												= 0x5A, // ASCII
			Key_X												= 0x58, // ASCII
			Key_C												= 0x43, // ASCII
			Key_V												= 0x56, // ASCII
			Key_B												= 0x42, // ASCII
			Key_N												= 0x4E, // ASCII
			Key_M												= 0x4D, // ASCII
			Key_Comma											= 0x2C, // ASCII
			Key_Period											= 0x2E, // ASCII
			Key_Slash											= 0x2F, // ASCII
			Key_RightShift										= 0x158,

			// Alphanumeric section, function row (Row A per ISO 9995)

			Key_LeftControl										= 0x155,
			Key_LeftSystem 										= 0x157,
			Key_Alt 											= 0x156,
		//	Key_RESERVED_A2 									= 0xA1, // ISO
			Key_Space 											= 0x20, // ASCII
		//	Key_RESERVED_A8 									= 0xA2, // ISO
			Key_AltGraph 										= 0x15A,
			Key_RightSystem 									= 0x15B,
			Key_Menu 											= 0x15C,
			Key_RightControl 									= 0x159,

			// Function section

			Key_Insert											= 0x104,
			Key_Home											= 0x10C,
			Key_PageUp											= 0x10A,
			Key_Delete											= 0x105,
			Key_End												= 0x10D,
			Key_PageDown										= 0x10B,

			Key_Up												= 0x109,
			Key_Left											= 0x107,
			Key_Down											= 0x108,
			Key_Right											= 0x106,

			// Numeric section

			Key_NumLock											= 0x11A,

			Key_NumPad0											= 0x140,
			Key_NumPad1											= 0x141,
			Key_NumPad2											= 0x142,
			Key_NumPad3											= 0x143,
			Key_NumPad4											= 0x144,
			Key_NumPad5											= 0x145,
			Key_NumPad6											= 0x146,
			Key_NumPad7											= 0x147,
			Key_NumPad8											= 0x148,
			Key_NumPad9											= 0x149,

			Key_NumPadSeparator 								= 0x14A,
			Key_NumPadDivide 									= 0x14B,
			Key_NumPadMultiply									= 0x14C,
			Key_NumPadSubtract 									= 0x14D,
			Key_NumPadAdd 										= 0x14E,
			Key_NumPadEnter 									= 0x14F,
		};
	};

	using EVirtualKey = ZzInternal_EGLFWVirtualKey::Type;
#endif
}

#endif