#ifndef ZZINTERNAL_INCLUDE_AEON_ENGINE_SYSTEM_INPUT_KEYBOARDINPUT
#define ZZINTERNAL_INCLUDE_AEON_ENGINE_SYSTEM_INPUT_KEYBOARDINPUT

#include "Aeon/Framework/Framework.hpp"

#include "Aeon/Engine/System/Input/VirtualKey.hpp"

namespace Aeon
{
	enum struct EKeyType : uint8
	{
		Unknown,
		Glyph,
		Dead,
		Modifier, // Ctrl, Shift, Alt, AltGr/Option
		System, // Esc, Fn keys, Win (Windows), Command (macOS), Menu, SysRq/PrtSc, Pause/Break
		Editing, // Enter, Backspace, Insert, Delete
		Navigation, // Nav arrows, Home, End, PageUp, PageDown
		Lock, // Caps Lock, Num Lock, Scroll Lock
	};

	struct DKey
	{
		STextStringView KeyName;
	//	STextString KeyName; // CHECKME Should this be a TextString?

		uint32 CodePoint : 21;
		EVirtualKey VirtKey : 9; // TODO ensure VirtualKeys always fit here

		EKeyType KeyType : 3;
	};

	// CHECKME more info about keyboard state https://learn.microsoft.com/en-us/windows/win32/learnwin32/keyboard-input#keyboard-state
	class SKeyboardLayout
	{
	public:
		SStringView GetName() const;

		DKey GetKey(EVirtualKey VirtKey) const;

		void SetupName(SStringView NewName);

	//	void SetupAlias(SStringView NewAlias);

		void SetupKey(EVirtualKey VirtKey, DKey Key);

	private:
		SString Name;
	//	ZzInternal_GArray<SString> NameAliases;
		GHashMap<EVirtualKey, DKey> VirtKeyMap;

	};
}

#endif