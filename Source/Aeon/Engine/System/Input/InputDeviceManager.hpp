#ifndef ZZINTERNAL_INCLUDE_AEON_ENGINE_SYSTEM_INPUT_INPUTDEVICEMANAGER
#define ZZINTERNAL_INCLUDE_AEON_ENGINE_SYSTEM_INPUT_INPUTDEVICEMANAGER

#include "Aeon/Framework/Framework.hpp"

#include "Aeon/Engine/System/Input/KeyboardInput.hpp"

namespace Aeon
{

	class SInputDeviceManager // TODO maybe rename to SInputManager ?
	{
	public:
		SKeyboardLayout const& GetCurrentKeyboardLayout() const;

		GOptionalRef<SKeyboardLayout const> FindKeyboardLayout(SStringView Alias) const;

		SKeyboardLayout& SetupNewKeyboardLayout(SStringView NewName);

	private:
		GArray<SKeyboardLayout> KeyboardArray;

	};
}

#endif