#ifndef ZZINTERNAL_INCLUDE_AEON_ENGINE_SYSTEM_INPUT_CURSORINPUT
#define ZZINTERNAL_INCLUDE_AEON_ENGINE_SYSTEM_INPUT_CURSORINPUT

#include "Aeon/Framework/Framework.hpp"

namespace Aeon
{
	// NOTE Cursor button values are based on GLFW buttons,
	//  though incremented by 1 to allow existence of Null value
	//  - GLFW: https://www.glfw.org/docs/latest/group__buttons.html

#if AEON_PLATFORM_WINDOWS or AEON_PLATFORM_MACOS or AEON_PLATFORM_LINUX
	struct ZzInternal_ECursorButton
	{
		enum Type 												: uint8
		{
			Null 												= 0x0,

			Button_Left											= 0x01,
			Button_Right										= 0x02,
			Button_Middle										= 0x03,

			Button_Back											= 0x04,
			Button_Forward										= 0x05,

		//	Button_1											= 0x01,
		//	Button_2											= 0x02,
		//	Button_3											= 0x03,
		//	Button_4											= 0x04,

		//	Button_5											= 0x05,
		//	Button_6											= 0x06,
		//	Button_7											= 0x07,
		//	Button_8											= 0x08,
		};
	};

	using ECursorButton = ZzInternal_ECursorButton::Type;
#endif
}

#endif