#add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/)

target_sources(${PROJECT_NAME}
	PUBLIC
		${CMAKE_CURRENT_SOURCE_DIR}/InputDispatcher.hpp

		${CMAKE_CURRENT_SOURCE_DIR}/InputDeviceManager.hpp

		${CMAKE_CURRENT_SOURCE_DIR}/KeyboardInput.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/CursorInput.hpp

	PRIVATE
		${CMAKE_CURRENT_SOURCE_DIR}/InputDeviceManager.cpp

		${CMAKE_CURRENT_SOURCE_DIR}/VirtualKey.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/KeyboardInput.cpp
		${CMAKE_CURRENT_SOURCE_DIR}/CursorInput.cpp

	)