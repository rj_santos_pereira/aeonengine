#ifndef ZZINTERNAL_INCLUDE_AEON_ENGINE_SYSTEM_INPUT_INPUTDISPATCHER
#define ZZINTERNAL_INCLUDE_AEON_ENGINE_SYSTEM_INPUT_INPUTDISPATCHER

#include "Aeon/Framework/Framework.hpp"

#include "Aeon/Engine/System/Input/KeyboardInput.hpp"
#include "Aeon/Engine/System/Input/CursorInput.hpp"

// HACK Clang bug causes erroneous warning here, see LogFlags.hpp for more info;
//  See: https://github.com/llvm/llvm-project/issues/73844
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wshadow"

namespace Aeon
{
#define AEON_READ_KEY_EVENT(Event) (static_cast<::Aeon::EKeyEvent>(static_cast<uint8>(Event) & 0x0F))
#define AEON_READ_KEY_EVENT_DATA(Event) ((static_cast<uint8>(Event) & 0xF0) >> 4)
#define AEON_WRITE_KEY_EVENT_DATA(Event, Data) static_cast<::Aeon::EKeyEvent>(static_cast<uint8>(Event) | ((Data & 0x0F) << 4))

	AEON_DECLARE_FLAG_ENUM(EKeyDirectEvent, uint8) // 1 bit
	(
		AEON_FLAG_ENUM_RULE_AS_IS,
		Up = 0,
		Down = 1,
	);

	AEON_DECLARE_FLAG_ENUM(EKeyEvent, uint8) // 8 bits
	(
		AEON_FLAG_ENUM_RULE_AS_IS,
		None							= 0b0000'0000,
		Press							= 0b0000'0001,
		Tap								= 0b0000'0011, // A Tap is a Press
		RepeatedTap						= 0b0000'0111, // A RepeatedTap is a Tap, and also a Press
		SingleTap						= 0b0001'0011, // Tap and RepeatedTap use the 4 higher bits to count the number of taps (Tap is always 1)
		DoubleTap						= 0b0010'0111,
		TripleTap						= 0b0011'0111,
		Hold							= 0b0000'1001, // A Hold is a Press
		Release							= 0b0000'1000,
	);

	AEON_DECLARE_FLAG_ENUM(EKeyModifierState, uint8) // 6 bits
	(
		AEON_FLAG_ENUM_RULE_POWER,
		None,
		Shift,
		Control,
		Alt,
		CapsLock,
		NumLock,
		ScrollLock
	);

	AEON_DECLARE_FLAG_ENUM(ECursorDirectEvent, uint8) // 1 bit
	(
		AEON_FLAG_ENUM_RULE_AS_IS,
		Up = 0,
		Down = 1,
	);

	AEON_DECLARE_FLAG_ENUM(ECursorEvent, uint8) // 4 bits
	(
		AEON_FLAG_ENUM_RULE_AS_IS,
		None							= 0b0000,
		Click							= 0b0001,
		DoubleClick						= 0b0011,
		TripleClick						= 0b0111,
		Drag							= 0b1001,
		Release							= 0b1000,
	);

	AEON_DECLARE_FLAG_ENUM(ECursorMode, uint8) // 3 bits
	(
		AEON_FLAG_ENUM_RULE_AS_IS,
		Free							= 0b000,
		FreeHidden	 					= 0b001,
		Captured 						= 0b010,
		CapturedHidden					= 0b011,
		CapturedVirtual					= 0b111,

	//	Mode_Hidden						= 0b001,
	//	Mode_Captured					= 0b010,
	//	Mode_Virtual					= 0b100,
	);

	// TODO refactor this
	inline SStringView MakeKeyEventString(EKeyEvent Event)
	{
		switch (Event)
		{
			default:
			case EKeyEvent::None: return "None";
			case EKeyEvent::Press: return "Press";
			case EKeyEvent::Tap: return "Tap";
			case EKeyEvent::SingleTap: return "SingleTap";
			case EKeyEvent::DoubleTap: return "DoubleTap";
			case EKeyEvent::TripleTap: return "TripleTap";
			case EKeyEvent::RepeatedTap: return "RepeatedTap";
			case EKeyEvent::Hold: return "Hold";
			case EKeyEvent::Release: return "Release";
		}
	}
	inline SStringView MakeCursorEventString(ECursorEvent Event)
	{
		switch (Event)
		{
			default:
			case ECursorEvent::None: return "None";
			case ECursorEvent::Click: return "Click";
			case ECursorEvent::DoubleClick: return "DoubleClick";
			case ECursorEvent::TripleClick: return "TripleClick";
			case ECursorEvent::Drag: return "Drag";
			case ECursorEvent::Release: return "Release";
		}
	}

	struct DKeyEventParams
	{
		// Maximum time, in seconds, for a key press to be considered a Tap event; anything beyond is considered a Press event
		float32 TapDeadline = 0.125f; // 0.10f // 0.15f
		// Maximum time, in seconds, for a key press to be considered a Press event; anything beyond is considered a Hold event
		float32 PressDeadline = 0.25f; // 0.3f
		// Maximum time since a Tap event during which pressing the same key will emit RepeatedTap events
		float32 RepeatedTapDeadline = 0.5f;
		// Maximum number of times a RepeatedTap event will be emitted when the same key is pressed within the deadline
		uint8 RepeatedTapLimit : 4 = 3;

		// TODO implement this
	//	bool ShouldResetCachedEventOnQuery : 1 = false;
	};

	struct DCursorEventParams
	{
		// Maximum time, in seconds, for a cursor button press to be considered a Click event; anything beyond is considered a Drag event
		float32 ClickDeadline = 0.125f;
		// Maximum time since a Click event during which pressing the same button will emit a DoubleClick/TripleClick event
		float32 RepeatedClickDeadline = 0.5f;

		// TODO implement this
	//	bool ShouldResetCachedEventOnQuery : 1 = false;

		static constexpr uint8 RepeatedClickLimit = 3;
	};

	// NOTE Notes about implementation:
	//  On key-down direct event, measure time until key-up,
	//  if before TapDeadline, emit Tap event,
	//  if before PressDeadline, emit Press event,
	//  else emit Hold event immediately, and when key-up is detected, emit Release event;
	//  if Tap event happens following another Tap event, emit RepeatedTap instead and for every consecutive Tap instead,
	//  until RepeatedTapDeadline or number of consecutive RepeatedTap (plus the initial Tap) events reaches RepeatedTapLimit.

	class IInputDispatcher
	{
	protected:
		~IInputDispatcher() = default;

	public:
		// CHECKME should we replace occurrences of EVirtKey with DKey in delegates? (window checks mapping in keyboard layout)
		//  should both be an option instead?

		AEON_DECLARE_MASKING_EVENT_DELEGATE(OnKeyEventDelegate, IInputDispatcher, void(EVirtualKey, EKeyEvent, EKeyModifierState));
		OnKeyEventDelegate OnKeyEvent;
		AEON_DECLARE_MASKING_EVENT_DELEGATE(OnKeyDirectEventDelegate, IInputDispatcher, void(EVirtualKey, EKeyDirectEvent, EKeyModifierState));
		OnKeyDirectEventDelegate OnKeyDirectEvent;

		AEON_DECLARE_MASKING_EVENT_DELEGATE(OnCursorEventDelegate, IInputDispatcher, void(ECursorButton, ECursorEvent));
		OnCursorEventDelegate OnCursorEvent;
		AEON_DECLARE_MASKING_EVENT_DELEGATE(OnCursorDirectEventDelegate, IInputDispatcher, void(ECursorButton, ECursorDirectEvent));
		OnCursorDirectEventDelegate OnCursorDirectEvent;

		AEON_DECLARE_EVENT_DELEGATE(OnCursorScrollEventDelegate, IInputDispatcher, void(SVector2));
		OnCursorScrollEventDelegate OnCursorScrollEvent;
		AEON_DECLARE_EVENT_DELEGATE(OnCursorPositionChangedDelegate, IInputDispatcher, void(SVector2, SVector2));
		OnCursorPositionChangedDelegate OnCursorPositionChanged;

		virtual EKeyEvent QueryKeyEvent(EVirtualKey Key) const = 0;
		virtual EKeyDirectEvent QueryKeyState(EVirtualKey Key) const = 0;
		virtual EKeyModifierState QueryKeyModifierState() const = 0;

		virtual ECursorEvent QueryCursorEvent(ECursorButton Button) const = 0;
		virtual ECursorDirectEvent QueryCursorState(ECursorButton Button) const = 0;

		virtual ECursorMode GetCursorMode() const = 0;

		virtual SVector2 QueryCursorPosition() const = 0;
		virtual SVector2 QueryCursorPositionDelta() const = 0;
		virtual SVector2 QueryCursorScrollDelta() const = 0;

		virtual DKeyEventParams GetKeyEventParams() const = 0;
		virtual DCursorEventParams GetCursorEventParams() const = 0;

		virtual void SetCursorMode(ECursorMode NewCursorMode) = 0;

		virtual void SetKeyEventParams(DKeyEventParams NewParams) = 0;
		virtual void SetCursorEventParams(DCursorEventParams NewParams) = 0;

		// TODO get text input

	protected:
		void BroadcastKeyEvent(EVirtualKey Key, EKeyEvent KeyEvent, EKeyModifierState KeyModifierState)
		{
			OnKeyEvent.Broadcast(OnKeyEventDelegate::BroadcasterAccess::Tag, Key, KeyEvent, KeyModifierState);
		}
		void BroadcastKeyDirectEvent(EVirtualKey Key, EKeyDirectEvent KeyEvent, EKeyModifierState KeyModifierState)
		{
			OnKeyDirectEvent.Broadcast(OnKeyDirectEventDelegate::BroadcasterAccess::Tag, Key, KeyEvent, KeyModifierState);
		}

		void BroadcastCursorEvent(ECursorButton Button, ECursorEvent CursorEvent)
		{
			OnCursorEvent.Broadcast(OnCursorEventDelegate::BroadcasterAccess::Tag, Button, CursorEvent);
		}
		void BroadcastCursorDirectEvent(ECursorButton Button, ECursorDirectEvent CursorEvent)
		{
			OnCursorDirectEvent.Broadcast(OnCursorDirectEventDelegate::BroadcasterAccess::Tag, Button, CursorEvent);
		}

		void BroadcastCursorScrollChanged(SVector2 ScrollDelta)
		{
			OnCursorScrollEvent.Broadcast(OnCursorScrollEventDelegate::BroadcasterAccess::Tag, ScrollDelta);
		}
		void BroadcastCursorPositionChanged(SVector2 Position, SVector2 PositionDelta)
		{
			OnCursorPositionChanged.Broadcast(OnCursorPositionChangedDelegate::BroadcasterAccess::Tag, Position, PositionDelta);
		}

	};
}

#pragma clang diagnostic pop

#endif