#include "Aeon/Engine/System/Input/KeyboardInput.hpp"

namespace Aeon
{
#if 0
	char const* Buffer = glfwGetKeyName(GLFW_KEY_UNKNOWN, Scancode);
	char const* Buffer = glfwGetKeyName(Key, 0);
	szint BufferLength = StringOps::Length(Buffer);

	uint32 CodePoint;
	utf8proc_iterate(reinterpret_cast<uint8 const*>(Buffer), BufferLength, reinterpret_cast<int32*>(&CodePoint));

	// TODO maybe use ICU and replace this with codepoint name
	// TODO move this elsewhere
	constexpr STextStringView KeyNames[128]
		{
			"Null Character",
			"Start of Heading",
			"Start of Text",
			"End of Text",
			"End of Transmission",
			"Enquiry",
			"Acknowledge",
			"Bell",
			"Backspace",
			"Horizontal Tab",
			"Line Feed",
			"Vertical Tabulation",
			"Form Feed",
			"Carriage Return",
			"Shift Out",
			"Shift In",
			"Data Link Escape",
			"Device Control One",
			"Device Control Two",
			"Device Control Three",
			"Device Control Four",
			"Negative Acknowledge",
			"Synchronous Idle",
			"End of Transmission Block",
			"Cancel",
			"End of Medium",
			"Substitute",
			"Escape",
			"File Separator",
			"Group Separator",
			"Record Separator",
			"Unit Separator",
			"Space",
			"Exclamation mark",
			"Double quotes",
			"Number sign",
			"Dollar sign",
			"Percent sign",
			"Ampersand",
			"Single quote",
			"Left parenthesis",
			"Right parenthesis",
			"Asterisk",
			"Plus",
			"Comma",
			"Hyphen",
			"Period",
			"Slash",
			"0", //"Zero",
			"1", //"One",
			"2", //"Two",
			"3", //"Three",
			"4", //"Four",
			"5", //"Five",
			"6", //"Six",
			"7", //"Seven",
			"8", //"Eight",
			"9", //"Nine",
			"Colon",
			"Semicolon",
			"Less-than sign",
			"Equals sign",
			"Greater-than sign",
			"Question mark",
			"At sign",
			"A", //"Uppercase A",
			"B", //"Uppercase B",
			"C", //"Uppercase C",
			"D", //"Uppercase D",
			"E", //"Uppercase E",
			"F", //"Uppercase F",
			"G", //"Uppercase G",
			"H", //"Uppercase H",
			"I", //"Uppercase I",
			"J", //"Uppercase J",
			"K", //"Uppercase K",
			"L", //"Uppercase L",
			"M", //"Uppercase M",
			"N", //"Uppercase N",
			"O", //"Uppercase O",
			"P", //"Uppercase P",
			"Q", //"Uppercase Q",
			"R", //"Uppercase R",
			"S", //"Uppercase S",
			"T", //"Uppercase T",
			"U", //"Uppercase U",
			"V", //"Uppercase V",
			"W", //"Uppercase W",
			"X", //"Uppercase X",
			"Y", //"Uppercase Y",
			"Z", //"Uppercase Z",
			"Left square bracket",
			"Backslash",
			"Right square bracket",
			"Circumflex accent",
			"Underscore",
			"Grave accent",
			"A", //"Lowercase a",
			"B", //"Lowercase b",
			"C", //"Lowercase c",
			"D", //"Lowercase d",
			"E", //"Lowercase e",
			"F", //"Lowercase f",
			"G", //"Lowercase g",
			"H", //"Lowercase h",
			"I", //"Lowercase i",
			"J", //"Lowercase j",
			"K", //"Lowercase k",
			"L", //"Lowercase l",
			"M", //"Lowercase m",
			"N", //"Lowercase n",
			"O", //"Lowercase o",
			"P", //"Lowercase p",
			"Q", //"Lowercase q",
			"R", //"Lowercase r",
			"S", //"Lowercase s",
			"T", //"Lowercase t",
			"U", //"Lowercase u",
			"V", //"Lowercase v",
			"W", //"Lowercase w",
			"X", //"Lowercase x",
			"Y", //"Lowercase y",
			"Z", //"Lowercase z",
			"Left curly bracket",
			"Vertical bar",
			"Right curly bracket",
			"Tilde",
			"Delete",
		};

	bool IsASCII = CodePoint < 128;
	auto KeyName = IsASCII ? KeyNames[CodePoint] : Buffer;

	AEON_LOG(Info, GLFWManager, "Key state change on window '{}': VK: {} Name: '{}'{} CodePoint: U+{:04X} State: {} Modifiers: {}",
			 Window.CachedTitle, Key, KeyName, IsASCII ? " (ASCII)" : "", CodePoint, NewAction, NewModifiers);
#endif
}
