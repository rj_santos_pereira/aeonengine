#ifndef ZZINTERNAL_INCLUDE_AEON_ENGINE_LOGGING_TERMINAL_TERMINALCOLOR
#define ZZINTERNAL_INCLUDE_AEON_ENGINE_LOGGING_TERMINAL_TERMINALCOLOR

#include "Aeon/Framework/Framework.hpp"

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ETerminalColor

	enum struct ETerminalColor		: uint32
	{
		// Using X11 terminal colors
		// Colors are encoded as 0xRR'GG'BB'NN,
		// where R, G, B correspond to the value of the Red, Green and Blue channels, respectively,
		// and N corresponds to the identifier of the closest color (distance measured through red-mean) in 256-color mode terminals.

		AliceBlue               	= 0xf0'f8'ff'0f, // Distance: 29.3859
		AntiqueWhite            	= 0xfa'eb'd7'ff, // Distance: 39.2681
		Aqua                    	= 0x00'ff'ff'0e, // Distance: 0
		Aquamarine              	= 0x7f'ff'd4'7a, // Distance: 13.5318
		Azure                   	= 0xf0'ff'ff'0f, // Distance: 25.8366
		Beige                   	= 0xf5'f5'dc'e6, // Distance: 27.3522
		Bisque                  	= 0xff'e4'c4'e0, // Distance: 37.3898
		Black                   	= 0x00'00'00'00, // Distance: 0
		BlanchedAlmond          	= 0xff'eb'cd'e0, // Distance: 42.4264
		Blue                    	= 0x00'00'ff'0c, // Distance: 0
		BlueViolet              	= 0x8a'2b'e2'5c, // Distance: 87.8454
		Brown                   	= 0xa5'2a'2a'7c, // Distance: 106.94
		Burlywood               	= 0xde'b8'87'b4, // Distance: 21.5365
		CadetBlue               	= 0x5f'9e'a0'49, // Distance: 41.7926
		Chartreuse              	= 0x7f'ff'00'76, // Distance: 12.6787
		Chocolate               	= 0xd2'69'1e'a6, // Distance: 49.1952
		Coral                   	= 0xff'7f'50'd1, // Distance: 26.5707
		CornflowerBlue          	= 0x64'95'ed'45, // Distance: 41.12
		Cornsilk                	= 0xff'f8'dc'e6, // Distance: 15.6844
		Crimson                 	= 0xdc'14'3c'a1, // Distance: 65.5796
		Cyan                    	= 0x00'ff'ff'0e, // Distance: 0
		DarkBlue                	= 0x00'00'8b'12, // Distance: 6.92369
		DarkCyan                	= 0x00'8b'8b'1e, // Distance: 10.5801
		DarkGoldenrod           	= 0xb8'86'0b'88, // Distance: 22.3714
		DarkGray                	= 0xa9'a9'a9'f8, // Distance: 2.99935
		DarkGreen               	= 0x00'64'00'16, // Distance: 10
		DarkKhaki               	= 0xbd'b7'6b'8f, // Distance: 33.4127
		DarkMagenta             	= 0x8b'00'8b'5a, // Distance: 8.94078
		DarkOliveGreen          	= 0x55'6b'2f'f0, // Distance: 77.0291
		DarkOrange              	= 0xff'8c'00'd0, // Distance: 10
		DarkOrchid              	= 0x99'32'cc'62, // Distance: 96.0454
		DarkRed                 	= 0x8b'00'00'58, // Distance: 6.36887
		DarkSalmon              	= 0xe9'96'7a'ae, // Distance: 46.7971
		DarkSeaGreen            	= 0x8f'bc'8f'6c, // Distance: 31.5555
		DarkSlateBlue           	= 0x48'3d'8b'3c, // Distance: 76.7936
		DarkSlateGray           	= 0x2f'4f'4f'ee, // Distance: 42.4311
		DarkTurquoise           	= 0x00'ce'd1'2c, // Distance: 20.7812
		DarkViolet              	= 0x94'00'd3'5c, // Distance: 21.6911
		DeepPink                	= 0xff'14'93'c6, // Distance: 43.4511
		DeepSkyBlue             	= 0x00'bf'ff'27, // Distance: 32
		DimGray                 	= 0x69'69'69'f2, // Distance: 8.99805
		DodgerBlue              	= 0x1e'90'ff'21, // Distance: 46.6555
		Firebrick               	= 0xb2'22'22'7c, // Distance: 85.5259
		FloralWhite             	= 0xff'fa'f0'0f, // Distance: 23.4521
		ForestGreen             	= 0x22'8b'22'1c, // Distance: 76.4165
		Fuchsia                 	= 0xff'00'ff'0d, // Distance: 0
		Gainsboro               	= 0xdc'dc'dc'fd, // Distance: 5.9987
		GhostWhite              	= 0xf8'f8'ff'0f, // Distance: 18.497
		Gold                    	= 0xff'd7'00'dc, // Distance: 0
		Goldenrod               	= 0xda'a5'20'b2, // Distance: 51.2602
		Gray                    	= 0xbe'be'be'07, // Distance: 5.9987
		ANSIGray                	= 0x80'80'80'08, // Distance: 0
		Green                   	= 0x00'ff'00'0a, // Distance: 0
		ANSIGreen               	= 0x00'80'00'02, // Distance: 0
		GreenYellow             	= 0xad'ff'2f'9a, // Distance: 71.6077
		Honeydew                	= 0xf0'ff'f0'0f, // Distance: 33.5279
		HotPink                 	= 0xff'69'b4'cd, // Distance: 21.2132
		IndianRed               	= 0xcd'5c'5c'a7, // Distance: 18.3743
		Indigo                  	= 0x4b'00'82'36, // Distance: 31.6135
		Ivory                   	= 0xff'ff'f0'0f, // Distance: 21.2132
		Khaki                   	= 0xf0'e6'8c'de, // Distance: 40.2276
		Lavender                	= 0xe6'e6'fa'ff, // Distance: 27.2454
		LavenderBlush           	= 0xff'f0'f5'ff, // Distance: 31.1753
		LawnGreen               	= 0x7c'fc'00'76, // Distance: 18.4176
		LemonChiffon            	= 0xff'fa'cd'e6, // Distance: 17.3205
		LightBlue               	= 0xad'd8'e6'98, // Distance: 23.1497
		LightCoral              	= 0xf0'80'80'd2, // Distance: 31.0317
		LightCyan               	= 0xe0'ff'ff'c3, // Distance: 15.2135
		LightGoldenrod          	= 0xfa'fa'd2'e6, // Distance: 14.9967
		LightGray               	= 0xd3'd3'd3'fc, // Distance: 8.99805
		LightGreen              	= 0x90'ee'90'78, // Distance: 39.5055
		LightPink               	= 0xff'b6'c1'd9, // Distance: 29.0517
		LightSalmon             	= 0xff'a0'7a'd8, // Distance: 35.1852
		LightSeaGreen           	= 0x20'b2'aa'25, // Distance: 47.1311
		LightSkyBlue            	= 0x87'ce'fa'75, // Distance: 19.6397
		LightSlateGray          	= 0x77'88'99'66, // Distance: 38.1182
		LightSteelBlue          	= 0xb0'c4'de'98, // Distance: 39.4956
		LightYellow             	= 0xff'ff'e0'e6, // Distance: 12.7279
		Lime                    	= 0x00'ff'00'0a, // Distance: 0
		LimeGreen               	= 0x32'cd'32'4d, // Distance: 102.553
		Linen                   	= 0xfa'f0'e6'ff, // Distance: 23.9165
		Magenta                 	= 0xff'00'ff'0d, // Distance: 0
		Maroon                  	= 0xb0'30'60'83, // Distance: 94.0266
		ANSIMaroon              	= 0x80'00'00'01, // Distance: 0
		MediumAquamarine        	= 0x66'cd'aa'4f, // Distance: 24.1275
		MediumBlue              	= 0x00'00'cd'14, // Distance: 17.3092
		MediumOrchid            	= 0xba'55'd3'86, // Distance: 27.64
		MediumPurple            	= 0x93'70'db'62, // Distance: 39.5277
		MediumSeaGreen          	= 0x3c'b3'71'47, // Distance: 61.2984
		MediumSlateBlue         	= 0x7b'68'ee'63, // Distance: 37.4807
		MediumSpringGreen       	= 0x00'fa'9a'30, // Distance: 34.3743
		MediumTurquoise         	= 0x48'd1'cc'50, // Distance: 41.202
		MediumVioletRed         	= 0xc7'15'85'a2, // Distance: 49.9174
		MidnightBlue            	= 0x19'19'70'04, // Distance: 67.3425
		MintCream               	= 0xf5'ff'fa'0f, // Distance: 18.6586
		MistyRose               	= 0xff'e4'e1'e0, // Distance: 29.5973
		Moccasin                	= 0xff'e4'b5'df, // Distance: 27.3496
		NavajoWhite             	= 0xff'de'ad'df, // Distance: 14.2829
		NavyBlue                	= 0x00'00'80'04, // Distance: 0
		OldLace                 	= 0xfd'f5'e6'e6, // Distance: 29.3743
		Olive                   	= 0x80'80'00'03, // Distance: 0
		OliveDrab               	= 0x6b'8e'23'40, // Distance: 61.0551
		Orange                  	= 0xff'a5'00'd6, // Distance: 20
		OrangeRed               	= 0xff'45'00'ca, // Distance: 52
		Orchid                  	= 0xda'70'd6'aa, // Distance: 34.4058
		PaleGoldenrod           	= 0xee'e8'aa'df, // Distance: 45.4214
		PaleGreen               	= 0x98'fb'98'78, // Distance: 38.8313
		PaleTurquoise           	= 0xaf'ee'ee'9f, // Distance: 42.712
		PaleVioletRed           	= 0xdb'70'93'a8, // Distance: 38.8708
		PapayaWhip              	= 0xff'ef'd5'e6, // Distance: 32.1248
		PeachPuff               	= 0xff'da'b9'df, // Distance: 15.3623
		Peru                    	= 0xcd'85'3f'ad, // Distance: 50.2596
		Pink                    	= 0xff'c0'cb'da, // Distance: 38
		Plum                    	= 0xdd'a0'dd'b6, // Distance: 32.8612
		PowderBlue              	= 0xb0'e0'e6'98, // Distance: 29.0957
		Purple                  	= 0xa0'20'f0'81, // Distance: 72.2504
		ANSIPurple              	= 0x80'00'80'05, // Distance: 0
		RebeccaPurple           	= 0x66'33'99'3c, // Distance: 93.3109
		Red                     	= 0xff'00'00'09, // Distance: 0
		RosyBrown               	= 0xbc'8f'8f'8a, // Distance: 29.3291
		RoyalBlue               	= 0x41'69'e1'3e, // Distance: 52.4367
		SaddleBrown             	= 0x8b'45'13'5e, // Distance: 60.274
		Salmon                  	= 0xfa'80'72'd1, // Distance: 31.5624
		SandyBrown              	= 0xf4'a4'60'd7, // Distance: 29.0852
		SeaGreen                	= 0x2e'8b'57'1d, // Distance: 68.3528
		Seashell                	= 0xff'f5'ee'0f, // Distance: 31.273
		Sienna                  	= 0xa0'52'2d'82, // Distance: 77.5587
		Silver                  	= 0xc0'c0'c0'07, // Distance: 0
		SkyBlue                 	= 0x87'ce'eb'74, // Distance: 36.2146
		SlateBlue               	= 0x6a'5a'cd'3e, // Distance: 25.4922
		SlateGray               	= 0x70'80'90'42, // Distance: 33.178
		Snow                    	= 0xff'fa'fa'0f, // Distance: 12.2474
		SpringGreen             	= 0x00'ff'7f'30, // Distance: 13.8474
		SteelBlue               	= 0x46'82'b4'43, // Distance: 40.2276
		Tan                     	= 0xd2'b4'8c'b4, // Distance: 14.9967
		Teal                    	= 0x00'80'80'06, // Distance: 0
		Thistle                 	= 0xd8'bf'd8'b6, // Distance: 32.078
		Tomato                  	= 0xff'63'47'cb, // Distance: 34.8712
		Turquoise               	= 0x40'e0'd0'50, // Distance: 51.7303
		Violet                  	= 0xee'82'ee'd5, // Distance: 39.2921
		Wheat                   	= 0xf5'de'b3'df, // Distance: 22.934
		White                   	= 0xff'ff'ff'0f, // Distance: 0
		WhiteSmoke              	= 0xf5'f5'f5'ff, // Distance: 20.9954
		Yellow                  	= 0xff'ff'00'0b, // Distance: 0
		YellowGreen             	= 0x9a'cd'32'71, // Distance: 79.0559
	};

	enum struct EANSI256Color		: uint8
	{
		// TODO
	};

//	struct ETerminalColor
//	{
//		using enum EX11TerminalColor;
//	};
}

#endif