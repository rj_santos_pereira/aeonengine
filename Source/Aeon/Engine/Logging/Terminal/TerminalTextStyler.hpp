#ifndef ZZINTERNAL_INCLUDE_AEON_ENGINE_LOGGING_TERMINAL_TERMINALTEXTSTYLER
#define ZZINTERNAL_INCLUDE_AEON_ENGINE_LOGGING_TERMINAL_TERMINALTEXTSTYLER

#include "Aeon/Framework/Framework.hpp"

#include "Aeon/Engine/Logging/Terminal/TerminalColor.hpp"

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ITerminalTextStylerBackend

	class ITerminalTextStylerBackend
	{
	protected:
		~ITerminalTextStylerBackend() = default;

	public:
		virtual void MakeRegular(SString& Text) = 0;

		virtual void MakeBold(SString& Text) = 0;

		virtual void MakeFaint(SString& Text) = 0;

		virtual void MakeSlanted(SString& Text) = 0;

		virtual void MakeUnderlined(SString& Text) = 0;

		virtual void MakeStrikethrough(SString& Text) = 0;

		virtual void MakeFramed(SString& Text) = 0;

		virtual void MakeColored(SString& Text, ETerminalColor Color) = 0;
	};

#define ZZINTERNAL_AEON_OVERRIDE_TERMINAL_TEXT_STYLER_BACKEND_INTERFACE \
	virtual void MakeRegular(SString& Text) override;\
	virtual void MakeBold(SString& Text) override;\
	virtual void MakeFaint(SString& Text) override;\
	virtual void MakeSlanted(SString& Text) override;\
	virtual void MakeUnderlined(SString& Text) override;\
	virtual void MakeStrikethrough(SString& Text) override;\
	virtual void MakeFramed(SString& Text) override;\
	virtual void MakeColored(SString& Text, ETerminalColor Color) override

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SNullTerminalTextStylerBackend

	class SNullTerminalTextStylerBackend final : public ITerminalTextStylerBackend
	{
	public:
		explicit SNullTerminalTextStylerBackend() = default;

		ZZINTERNAL_AEON_OVERRIDE_TERMINAL_TEXT_STYLER_BACKEND_INTERFACE;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// EANSIColorSpecifier, SANSITerminalTextStylerBackend

	// TODO move this backend elsewhere

	enum struct EANSIColorSpecifier
	{
		Index,
		Value,
	};

	class SANSITerminalTextStylerBackend final : public ITerminalTextStylerBackend
	{
	public:
		explicit SANSITerminalTextStylerBackend(EANSIColorSpecifier NewColorSpec);

		ZZINTERNAL_AEON_OVERRIDE_TERMINAL_TEXT_STYLER_BACKEND_INTERFACE;

	private:
		EANSIColorSpecifier ColorSpec;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// STerminalTextStyler

	class STerminalTextStyler
	{
	public:
		template <class BackendType, class ... ArgTypes>
		STerminalTextStyler(TClass<BackendType>, ArgTypes&& ... CtorArgs)
		requires CBaseClass<ITerminalTextStylerBackend, BackendType>
			: BackendPtr(MakeUniquePtr<BackendType>(Forward<ArgTypes>(CtorArgs)...))
		{
		}

		AEON_MAKE_NON_COPYABLE(STerminalTextStyler);
		AEON_MAKE_NON_MOVEABLE(STerminalTextStyler);

		STerminalTextStyler& BindString(SString& Text);

		STerminalTextStyler& MakeRegular();

		STerminalTextStyler& MakeBold();

		STerminalTextStyler& MakeFaint();

		STerminalTextStyler& MakeSlanted();

		STerminalTextStyler& MakeUnderlined();

		STerminalTextStyler& MakeStrikethrough();

		STerminalTextStyler& MakeFramed();

		STerminalTextStyler& MakeColored(ETerminalColor Color);

		// These methods simulate (and defer to) the GString class methods for simplicity of use

		template <class ... FormatDataTypes>
		STerminalTextStyler& FormatConcat(SStringView const& FormatString, FormatDataTypes&& ... FormatData)
		{
			AEON_FAILSAFE(BoundStringPtr, return *this, "BindString must be called before calling this method!");
			BoundStringPtr->FormatConcat(FormatString, Forward<FormatDataTypes>(FormatData) ...);
			return *this;
		}

		STerminalTextStyler& Concat(SStringView const& ConcatString)
		{
			AEON_FAILSAFE(BoundStringPtr, return *this, "BindString must be called before calling this method!");
			BoundStringPtr->Concat(ConcatString);
			return *this;
		}
	private:
		GUniquePtr<ITerminalTextStylerBackend> BackendPtr;
		SString* BoundStringPtr = nullptr; // TODO change this to STextString
	};
}

#endif