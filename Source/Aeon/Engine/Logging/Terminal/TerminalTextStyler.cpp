#include "Aeon/Engine/Logging/Terminal/TerminalTextStyler.hpp"

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SNullTerminalTextStylerBackend

	void SNullTerminalTextStylerBackend::MakeRegular(SString& Text)
	{
		AEON_UNUSED(Text);
	}
	
	void SNullTerminalTextStylerBackend::MakeBold(SString& Text)
	{
		AEON_UNUSED(Text);
	}
	
	void SNullTerminalTextStylerBackend::MakeFaint(SString& Text)
	{
		AEON_UNUSED(Text);
	}
	
	void SNullTerminalTextStylerBackend::MakeSlanted(SString& Text)
	{
		AEON_UNUSED(Text);
	}
	
	void SNullTerminalTextStylerBackend::MakeUnderlined(SString& Text)
	{
		AEON_UNUSED(Text);
	}
	
	void SNullTerminalTextStylerBackend::MakeStrikethrough(SString& Text)
	{
		AEON_UNUSED(Text);
	}
	
	void SNullTerminalTextStylerBackend::MakeFramed(SString& Text)
	{
		AEON_UNUSED(Text);
	}
	
	void SNullTerminalTextStylerBackend::MakeColored(SString& Text, ETerminalColor Color)
	{
		AEON_UNUSED(Text, Color);
	}
		
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SANSITerminalTextStyler

	SANSITerminalTextStylerBackend::SANSITerminalTextStylerBackend(EANSIColorSpecifier NewColorSpec)
		: ColorSpec(NewColorSpec)
	{
	}
	
	void SANSITerminalTextStylerBackend::MakeRegular(SString& Text)
	{
		Text.Concat("\x1B[0m");
	}

	void SANSITerminalTextStylerBackend::MakeBold(SString& Text)
	{
		Text.Concat("\x1B[1m");
	}

	void SANSITerminalTextStylerBackend::MakeFaint(SString& Text)
	{
		Text.Concat("\x1B[2m");
	}

	void SANSITerminalTextStylerBackend::MakeSlanted(SString& Text)
	{
		Text.Concat("\x1B[3m");
	}

	void SANSITerminalTextStylerBackend::MakeUnderlined(SString& Text)
	{
		Text.Concat("\x1B[4m");
	}

	void SANSITerminalTextStylerBackend::MakeStrikethrough(SString& Text)
	{
		Text.Concat("\x1B[9m");
	}

	void SANSITerminalTextStylerBackend::MakeFramed(SString& Text)
	{
		Text.Concat("\x1B[51m");
	}

	void SANSITerminalTextStylerBackend::MakeColored(SString& Text, ETerminalColor Color)
	{
		if (ColorSpec == EANSIColorSpecifier::Index)
		{
			auto ColorIndex = static_cast<uint8>(Color);
			Text.FormatConcat("\x1B[38;5;{}m", ColorIndex);
		}
		else
		{
			struct DColorValues
			{
#			if AEON_IS_LITTLE_ENDIAN_ARCH
				uint8 I; uint8 B; uint8 G; uint8 R;
#			else
#				error Not implemented yet!
#			endif
			};

			auto ColorValues = bit_cast<DColorValues>(Color);
			Text.FormatConcat("\x1B[38;2;{};{};{}m", ColorValues.R, ColorValues.G, ColorValues.B);
		}
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// STerminalTextStyler

//	STerminalTextStyler::STerminalTextStyler(ITerminalTextStylerBackend* Backend)
//	{
//		AEON_ASSERT(Backend);
//		BackendPtr = MakeManagedPtr<ITerminalTextStylerBackend>(Backend);
//	}

	STerminalTextStyler& STerminalTextStyler::BindString(SString& Text)
	{
		BoundStringPtr = addressof(Text);
		return *this;
	}
	
	STerminalTextStyler& STerminalTextStyler::MakeRegular()
	{
		AEON_FAILSAFE(BoundStringPtr, return *this, "BindString must be called before calling this method!");
		BackendPtr->MakeRegular(*BoundStringPtr);
		return *this;
	}

	STerminalTextStyler& STerminalTextStyler::MakeBold()
	{
		AEON_FAILSAFE(BoundStringPtr, return *this, "BindString must be called before calling this method!");
		BackendPtr->MakeBold(*BoundStringPtr);
		return *this;
	}

	STerminalTextStyler& STerminalTextStyler::MakeFaint()
	{
		AEON_FAILSAFE(BoundStringPtr, return *this, "BindString must be called before calling this method!");
		BackendPtr->MakeFaint(*BoundStringPtr);
		return *this;
	}

	STerminalTextStyler& STerminalTextStyler::MakeSlanted()
	{
		AEON_FAILSAFE(BoundStringPtr, return *this, "BindString must be called before calling this method!");
		BackendPtr->MakeSlanted(*BoundStringPtr);
		return *this;
	}

	STerminalTextStyler& STerminalTextStyler::MakeUnderlined()
	{
		AEON_FAILSAFE(BoundStringPtr, return *this, "BindString must be called before calling this method!");
		BackendPtr->MakeUnderlined(*BoundStringPtr);
		return *this;
	}

	STerminalTextStyler& STerminalTextStyler::MakeStrikethrough()
	{
		AEON_FAILSAFE(BoundStringPtr, return *this, "BindString must be called before calling this method!");
		BackendPtr->MakeStrikethrough(*BoundStringPtr);
		return *this;
	}

	STerminalTextStyler& STerminalTextStyler::MakeFramed()
	{
		AEON_FAILSAFE(BoundStringPtr, return *this, "BindString must be called before calling this method!");
		BackendPtr->MakeFramed(*BoundStringPtr);
		return *this;
	}

	STerminalTextStyler& STerminalTextStyler::MakeColored(ETerminalColor Color)
	{
		AEON_FAILSAFE(BoundStringPtr, return *this, "BindString must be called before calling this method!");
		BackendPtr->MakeColored(*BoundStringPtr, Color);
		return *this;
	}

}
