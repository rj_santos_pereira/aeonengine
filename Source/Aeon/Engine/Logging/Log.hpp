#ifndef ZZINTERNAL_INCLUDE_AEON_ENGINE_LOGGING_LOG
#define ZZINTERNAL_INCLUDE_AEON_ENGINE_LOGGING_LOG

#include "Aeon/Framework/Framework.hpp"

#include "Aeon/Engine/Logging/LoggerDeviceManager.hpp"
#include "Aeon/Engine/Logging/LoggerDevice.hpp"

namespace Aeon::ZzInternal
{
	// FIXME generalize this to support STextString somehow;
	//  idea: use STextString internally, and convert passed strings to TextString

	// These function templates are instantiated with unique hashes
	// associated with the source file/line of the AEON_LOG instance that invokes them,
	// so that certain static information is cached in order to avoid redundant processing in subsequent calls.
	// This caching is designed to be compatible with hot-reload tools.

	template <uint64 FileNameHashValue>
	SStringView PreprocessFilePath(SStringView FilePath)
	{
		AEON_UNUSED(FileNameHashValue);

		// Per-file instance to avoid redundant setup
		static SString PreprocessedFilePath;
		if (PreprocessedFilePath.IsEmpty())
		{
			PreprocessedFilePath = FilePath;

			// Make path relative to Source folder
			constexpr SStringView SourceFolderName = "Source";
			SString::IndexType SourceFilePathIdx = PreprocessedFilePath.FindFirstOf(SourceFolderName);

			AEON_ASSERT(SourceFilePathIdx != SString::NullIndex,
						"[CoreLogging/FatalError] Expected file to be within 'Source' folder");

			PreprocessedFilePath = PreprocessedFilePath.SubstringToBack(SourceFilePathIdx + SourceFolderName.CharCount() + 1);

#			if AEON_PLATFORM_WINDOWS
			// Replace backslashes by slashes
			PreprocessedFilePath.ReplaceAllOf("\\", "/");
#			endif

			PreprocessedFilePath.Prune();
		}

		return PreprocessedFilePath;
	}

	template <uint64 FileLineHashValue>
	SStringView PreprocessFuncName(SStringView FuncSig)
	{
		AEON_UNUSED(FileLineHashValue);

		// Per-line instance to avoid redundant setup
		static SString PreprocessedFuncName;
		if (PreprocessedFuncName.IsEmpty())
		{
			PreprocessedFuncName = FuncSig;

			// Transform any lambda signature into a mix of symbols that indicate an anonymous function
			SString::IndexType PastRBracketIndex = 0;
			while (true)
			{
#				if AEON_COMPILER_MSVC
				auto LBracketIndex = PreprocessedFuncName.FindFirstOf("<", PastRBracketIndex);
#				elif AEON_COMPILER_CLANG
				auto LBracketIndex = PreprocessedFuncName.FindFirstOf("(anonymous class)", PastRBracketIndex);
#				else
#					error Not implemented!
#				endif
				if (LBracketIndex == SString::NullIndex)
				{
					break;
				}

				// The first scope operator will precede the function call operator of the lambda,
				// so find the second scope operator.
				PastRBracketIndex = PreprocessedFuncName.FindNthOf("::", 1, LBracketIndex);
				if (PastRBracketIndex == SString::NullIndex)
				{
					PastRBracketIndex = PreprocessedFuncName.LengthChars();
				}

				PreprocessedFuncName.ReplaceAt(LBracketIndex, PastRBracketIndex - LBracketIndex, "<lambda>");
			}

			// Remove parameter type lists.
			PastRBracketIndex = 0;
			while (true)
			{
				auto LBracketIndex = PreprocessedFuncName.FindFirstOf("(", PastRBracketIndex);
				if (LBracketIndex == SString::NullIndex)
				{
					break;
				}

				PastRBracketIndex = PreprocessedFuncName.FindFirstOf("::", LBracketIndex);
				if (PastRBracketIndex == SString::NullIndex)
				{
					PastRBracketIndex = PreprocessedFuncName.LengthChars();
				}

				PreprocessedFuncName.ReplaceAt(LBracketIndex, PastRBracketIndex - LBracketIndex, "");
			}

			// Remove the return type and any qualifiers.
			PreprocessedFuncName.ReplaceAt(0, PreprocessedFuncName.FindLastOf(" ") + 1, "");

			PreprocessedFuncName.Prune();
		}

		return PreprocessedFuncName;
	}

	template <uint64 SourceFileHashValue, uint64 SourceLineHashValue, class ... FormatDataTypes>
	void Log(AeonDebugger::ISourceDebugInfo const& SourceInfo, SDateTime const& DateTime,
			 DLogCategory LogCategory, DLogFamily LogFamily,
			 SStringView const& FormatString, FormatDataTypes&& ... FormatData)
	{
		// Per-log hash check to avoid redundant setup
		static GAtomicUint64<EMemoryOrder::AcqRel, EMemoryOrder::Relaxed> DeviceHash[SLoggerDeviceManager::MaxDeviceCount] = { };

		constexpr uint64 DeviceNeedsSetupFlag = AeonMath::MakeBitMask<uint64, 64>();

		// CHECKME We may need to avoid accessing the constant so that during recompilation, old threads still use the old value
	//	uint64 volatile SourceLineHash = SourceLineHashValue;

		// TODO Consider making logging async (or configurable per device),
		//  and flush on DEBUG_BREAK (and/or configurable per log category, e.g. error and fatal flush immediately)

		SString FormattedMessage;

		FormattedMessage.FormatConcat(FormatString, Aeon::Forward<FormatDataTypes>(FormatData) ...);

		auto& DeviceManager = SLoggerDeviceManager::Instance();
		for (uint8 Index = 0; Index < DeviceManager.GetLoggerCount(); ++Index)
		{
			auto Device = DeviceManager.GetLogger(Index);

			bool NeedsSetup;
			// While the current hash is different from SourceLineHashValue, try to mark it with the DeviceSetup mask
			uint64 CurrentHash = 0;
			do
			{
				NeedsSetup = DeviceHash[Index].CompareExchange<EMemoryOrder::Relaxed>(CurrentHash, DeviceNeedsSetupFlag);
				NeedsSetup = NeedsSetup || CurrentHash == DeviceNeedsSetupFlag;
			}
			while (!NeedsSetup && CurrentHash != SourceLineHashValue);

			// If the device does need setup, and we managed to mark it, then setup the device
			if (NeedsSetup && CurrentHash != DeviceNeedsSetupFlag)
			{
				Device->SetupNameInfo(SourceLineHashValue, LogFamily.Name, LogCategory.ShortName);

				// CHECKME eventually use additional color defined in family by default?
				ETerminalColor MessageColor = ETerminalColor::Black;
				if (Aeon::HasAnyFlags(LogCategory.Behavior, ELogBehavior::UseCategoryColor))
				{
					MessageColor = LogCategory.Color;
				}

				Device->SetupColorInfo(SourceLineHashValue, LogFamily.Color, LogCategory.Color, MessageColor);

				// Normally the category's flags are used to define what info should be shown for that particular category,
				// and the group defines which subset of that info is actually shown (by bit-wise AND'ing those flags).
				// When this flag is set, however, the group flags are ignored, and the flags of the category are used directly.
				// This allows, for instance, for the Debug category to always show all the info,
				// regardless of which flags are defined for the group.

				ELogSourceInfo SourceInfoFlags = LogCategory.SourceInfo;
				ELogDateTimeInfo DateTimeInfoFlags = LogCategory.DateTimeInfo;
				if (!Aeon::HasAnyFlags(LogCategory.Behavior, ELogBehavior::UseCategoryFlags))
				{
					SourceInfoFlags &= LogFamily.SourceInfo;
					DateTimeInfoFlags &= LogFamily.DateTimeInfo;
				}

				Device->SetupFlagInfo(SourceLineHashValue, SourceInfoFlags, DateTimeInfoFlags);

				if (Aeon::HasAnyFlags(SourceInfoFlags, ELogSourceInfo::FileName, ELogSourceInfo::FilePath))
				{
					SStringView FilePath = PreprocessFilePath<SourceFileHashValue>(SourceInfo.FilePath());
					SStringView FileName{ StringOps::FindLastOf(FilePath.CharBuffer(), "/") + 1 };

					Device->SetupFileInfo(SourceLineHashValue, FileName, FilePath);
				}

				if (Aeon::HasAnyFlags(SourceInfoFlags, ELogSourceInfo::LineNum, ELogSourceInfo::LineChar))
				{
					Device->SetupLineInfo(SourceLineHashValue, SourceInfo.LineNum(), SourceInfo.LineChar());
				}

				if (Aeon::HasAnyFlags(SourceInfoFlags, ELogSourceInfo::FuncName, ELogSourceInfo::FuncSig))
				{
					SStringView FuncName = PreprocessFuncName<SourceLineHashValue>(SourceInfo.FuncSig());

					Device->SetupFuncInfo(SourceLineHashValue, FuncName, SourceInfo.FuncSig());
				}

				Device->FinishSetup(SourceLineHashValue);

				// Mark the device as setup, and notify waiting threads
				DeviceHash[Index].StoreNotifyAll<EMemoryOrder::Release>(SourceLineHashValue);
			}

			// While the device is being setup, loop and wait;
			// this should never be a busy-loop unless a hot-reload happened in the meantime
			do
			{
				CurrentHash = DeviceHash[Index].WaitLoad<EMemoryOrder::Acquire>(DeviceNeedsSetupFlag);
			}
			while (CurrentHash != SourceLineHashValue);

			Device->LogMessage(SourceLineHashValue, DateTime, Move(FormattedMessage));

			if (Aeon::HasAnyFlags(LogCategory.Behavior, ELogBehavior::ShouldFlush))
			{
				Device->FlushMessages(SourceLineHashValue);
			}
		}

		// CHECKME should this behavior exist here?
		if (Aeon::HasAnyFlags(LogCategory.Behavior, ELogBehavior::ShouldCrash))
		{
			AeonDebugger::CrashProgram(true, 1);
		}
	}
}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// AEON_LOG

#define AEON_LOG(LogCategory, LogFamily, LogMessage, ...) \
    ;{                                                                                                                                               \
        constexpr ::Aeon::SStringView ZzInternal_LogMessage = LogMessage;                                                                            \
        if constexpr (::Aeon::ZzInternal:: LogCategory.Level <= AEON_BUILD_LOG_LEVEL)                                                                \
        {                                                                                                                                            \
            ::Aeon::ZzInternal::Log<AEON_MAKE_SOURCE_FILE_HASH(),                                                                                    \
                                    AEON_MAKE_SOURCE_LINE_HASH()>(AEON_SOURCE_LINE_INFO(), ::Aeon::SDateTime::Now(),                                 \
                                                                  ::Aeon::ZzInternal:: LogCategory, ::Aeon::ZzInternal:: LogFamily,                  \
                                                                  ZzInternal_LogMessage __VA_OPT__(,) __VA_ARGS__);                                  \
        }                                                                                                                                            \
	}                                                                                                                                                \

#if 0
#define AEON_INFO(LogFamily, LogMessage, ...) AEON_LOG(Info, LogFamily, LogMessage, __VA_ARGS__)
#define AEON_WARN(LogFamily, LogMessage, ...) AEON_LOG(Warning, LogFamily, LogMessage, __VA_ARGS__)
#define AEON_ERROR(LogFamily, LogMessage, ...) AEON_LOG(Error, LogFamily, LogMessage, __VA_ARGS__)
#define AEON_FATAL(LogFamily, LogMessage, ...) AEON_LOG(FatalError, LogFamily, LogMessage, __VA_ARGS__)
#endif

AEON_DECLARE_LOG_FAMILY(DebugLog, ETerminalColor::MediumBlue, ELogSourceInfo::None, ELogDateTimeInfo::None);

#define AEON_DBGLOG(LogMessage, ...) AEON_LOG(Debug, DebugLog, LogMessage, __VA_ARGS__)

// TODO #define AEON_LOGARG for fmt named/indexed args

#endif