#include "Aeon/Engine/Logging/LoggerDeviceManager.hpp"

#include "Aeon/Engine/Logging/LoggerDevice.hpp"

namespace Aeon
{
	SLoggerDeviceManager& SLoggerDeviceManager::Instance()
	{
		static SLoggerDeviceManager Manager;
//		DeviceManager = addressof(Manager);
//		return DeviceManager;
		return Manager;
	}

	void SLoggerDeviceManager::RegisterLogger(ILoggerDevice* Device)
	{
		// TODO check thread

		if (DeviceCount < MaxDeviceCount)
		{
			uint8 NewIndex = DeviceCount++;

			DeviceArray[NewIndex] = Device;
			Device->SetRegistryIndex(ILoggerDevice::DeviceManagerAccess::Tag, NewIndex);
		}
	}
	void SLoggerDeviceManager::UnregisterLogger(ILoggerDevice* Device)
	{
		// TODO check thread

		uint8 Index = Device->GetRegistryIndex(ILoggerDevice::DeviceManagerAccess::Tag);
		if (Index != MaxDeviceCount)
		{
			uint8 OldLastIndex = --DeviceCount;

			// At worst, this is a no-op (if RegistryIndex == OldLastIndex), and it will be null'ed afterwards
			DeviceArray[Index] = DeviceArray[OldLastIndex];
			DeviceArray[OldLastIndex] = nullptr;
		}
	}
	ILoggerDevice* SLoggerDeviceManager::GetLogger(uint8 Index) const
	{
		return DeviceArray[Index];
	}
	uint8 SLoggerDeviceManager::GetLoggerCount() const
	{
		return DeviceCount;
	}

	void SLoggerDeviceManager::FlushLogDevices()
	{
		for (uint8 Index = 0; Index < DeviceCount; ++Index)
		{
			auto Device = DeviceArray[Index];

			Device->FlushMessages(0);
		}
	}
}
