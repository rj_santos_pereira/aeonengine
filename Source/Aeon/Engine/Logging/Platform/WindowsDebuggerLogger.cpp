#include "Aeon/Engine/Logging/Log.hpp"

#include "Aeon/Engine/Logging/Terminal/TerminalTextStyler.hpp"
#include "Aeon/Engine/Platform/Process/ProcessInfo.hpp"

namespace Aeon
{
	class SWindowsDebuggerLogger final : public ILoggerDevice
	{
	public:
		SWindowsDebuggerLogger()
		{
			// FIXME move this out of the ctor, and initialize conditionally
			DebuggerName = SProcessInfo::DetectRunningDebugger();
			AEON_FAILSAFE(DebuggerName != EDebuggerName::None, void(),
						  "No debugger is attached! Initialization of this device will finish, but configuration may be incorrect.")
		}

		virtual SStringView LoggerName() const override
		{
			return AEON_TOKEN_STR(SWindowsDebuggerLogger);
		}

		virtual void SetupNameInfo(uint64 LogId, SStringView const& FamilyName, SStringView const& CategoryName) override
		{
			auto& [Key, LogData] = CachedLogDataMap.FindElseAddDefault(LogId);

			LogData.FamilyName = FamilyName;
			LogData.CategoryName = CategoryName;
		}
		virtual void SetupColorInfo(uint64 LogId, ETerminalColor FamilyColor, ETerminalColor CategoryColor, ETerminalColor MessageColor) override
		{
			auto& [Key, LogData] = CachedLogDataMap.FindElseAddDefault(LogId);

			LogData.FamilyColor = FamilyColor;
			LogData.CategoryColor = CategoryColor;
			LogData.MessageColor = MessageColor;
		}
		virtual void SetupFlagInfo(uint64 LogId, ELogSourceInfo SourceInfo, ELogDateTimeInfo DateTimeInfo) override
		{
			auto& [Key, LogData] = CachedLogDataMap.FindElseAddDefault(LogId);

			LogData.SourceInfo = SourceInfo;
			LogData.DateTimeInfo = DateTimeInfo;
		}
		virtual void SetupFileInfo(uint64 LogId, SStringView const& FileName, SStringView const& FilePath) override
		{
			AEON_UNUSED(FilePath);

			auto& [Key, LogData] = CachedLogDataMap.FindElseAddDefault(LogId);

			LogData.FileName = FileName;
		}
		virtual void SetupLineInfo(uint64 LogId, uint32 LineNum, uint32 LineChar) override
		{
			AEON_UNUSED(LineChar);

			auto& [Key, LogData] = CachedLogDataMap.FindElseAddDefault(LogId);

			LogData.LineNum = LineNum;
		}
		virtual void SetupFuncInfo(uint64 LogId, SStringView const& FuncName, SStringView const& FuncSig) override
		{
			AEON_UNUSED(FuncSig);

			auto& [Key, LogData] = CachedLogDataMap.FindElseAddDefault(LogId);

			LogData.FuncName = FuncName;
		}
		virtual void FinishSetup(uint64 LogId) override
		{
			auto& [Key, LogData] = CachedLogDataMap.FindElseAddDefault(LogId);

			STerminalTextStyler FormattedLogStyler =
				[this]{
					switch (DebuggerName)
					{
						case EDebuggerName::LLDB:
							return STerminalTextStyler(TClass<SANSITerminalTextStylerBackend>{}, EANSIColorSpecifier::Index);
						case EDebuggerName::VSRemoteMonitor:
						default:
							return STerminalTextStyler(TClass<SNullTerminalTextStylerBackend>{});
					}
				}();

			FormattedLogStyler.BindString(LogData.CachedFormattedLogBuffer)
							  .MakeBold()
							  .MakeColored(LogData.CategoryColor)
							  .FormatConcat(LogCategoryPaddedFormat, SString::Format(LogCategoryInfoFormat, LogData.CategoryName))
							  .MakeColored(LogData.FamilyColor)
							  .FormatConcat(LogFamilyPaddedFormat, SString::Format(LogFamilyInfoFormat, LogData.FamilyName))
							  .MakeRegular()
							  .MakeColored(LogData.MessageColor)
							  .Concat(MessageFormat);

			ELogSourceInfo SourceInfoFlag = LogData.SourceInfo;
			ELogDateTimeInfo DateTimeInfoFlag = LogData.DateTimeInfo;

			if (Aeon::HasAnyFlags(SourceInfoFlag, ELogSourceInfo::FileName, ELogSourceInfo::FuncName))
			{
				FormattedLogStyler.Concat(" ")
								  .MakeBold()
								  .MakeUnderlined()
			//					  .MakeSlanted()
								  .MakeColored(LogData.CategoryColor);

				if (Aeon::HasAllFlags(SourceInfoFlag, ELogSourceInfo::FileName, ELogSourceInfo::FuncName))
				{
					FormattedLogStyler.FormatConcat(FileFuncInfoFormat, LogData.FileName, LogData.LineNum, LogData.FuncName);
				}
				else if (Aeon::HasAnyFlags(SourceInfoFlag, ELogSourceInfo::FileName))
				{
					FormattedLogStyler.FormatConcat(FileInfoFormat, LogData.FileName, LogData.LineNum);
				}
				else if (Aeon::HasAnyFlags(SourceInfoFlag, ELogSourceInfo::FuncName))
				{
					FormattedLogStyler.FormatConcat(FuncInfoFormat, LogData.FuncName);
				}

				FormattedLogStyler.MakeRegular();
			}

			if (Aeon::HasAnyFlags(DateTimeInfoFlag, ELogDateTimeInfo::Date, ELogDateTimeInfo::Time))
			{
				FormattedLogStyler.Concat(" ")
								  .MakeBold()
								  .MakeUnderlined()
								  .MakeSlanted()
								  .MakeColored(LogData.CategoryColor);

				if (Aeon::HasAllFlags(DateTimeInfoFlag, ELogDateTimeInfo::Date, ELogDateTimeInfo::Time))
				{
					FormattedLogStyler.Concat(DateTimeInfoFormat);
				}
				else if (Aeon::HasAnyFlags(DateTimeInfoFlag, ELogDateTimeInfo::Date))
				{
					FormattedLogStyler.Concat(DateInfoFormat);
				}
				else if (Aeon::HasAnyFlags(DateTimeInfoFlag, ELogDateTimeInfo::Time))
				{
					FormattedLogStyler.Concat(TimeInfoFormat);
				}

				FormattedLogStyler.MakeRegular();
			}

			FormattedLogStyler.Concat("\n");

			LogData.CachedFormattedLogBuffer.Prune();
		}

		virtual void LogMessage(uint64 LogId, SDateTime const& DateTime, SStringView const& Message) override
		{
			auto LogDataOpt = CachedLogDataMap.Find(LogId);

			AEON_FAILSAFE(LogDataOpt, return, "LogData not configured for requested ID");

			auto& [Key, LogData] = *LogDataOpt;

			SString MessageBuffer = Message;
			MessageBuffer.ReplaceAllOf("\n", PaddedNewline);

			SString LogBuffer;

			if (Aeon::HasAnyFlags(LogData.DateTimeInfo, ELogDateTimeInfo::Date, ELogDateTimeInfo::Time))
			{
				LogBuffer.FormatConcat(LogData.CachedFormattedLogBuffer, MessageBuffer, DateTime);
			}
			else
			{
				LogBuffer.FormatConcat(LogData.CachedFormattedLogBuffer, MessageBuffer);
			}

			switch (DebuggerName)
			{
				case EDebuggerName::VSRemoteMonitor:
					OutputDebugStringA(LogBuffer.CharBuffer());
					break;
				case EDebuggerName::LLDB:
				default:
					std::fwrite(LogBuffer.CharBuffer(), sizeof(char), LogBuffer.LengthChars(), stdout);
					break;
			}
		}
		virtual void FlushMessages(uint64 LogId) override
		{
			AEON_UNUSED(LogId);

			switch (DebuggerName)
			{
				case EDebuggerName::VSRemoteMonitor:
					break;
				case EDebuggerName::LLDB:
				default:
					std::fflush(stdout);
					break;
			}
		}

	private:
		struct DLogData
		{
			DLogData() = default;

			SString CachedFormattedLogBuffer;

			SStringView FamilyName;
			SStringView CategoryName;

			SStringView FileName;
			SStringView FuncName;
			uint32 LineNum;

			ETerminalColor FamilyColor;
			ETerminalColor CategoryColor;
			ETerminalColor MessageColor;

			ELogSourceInfo SourceInfo;
			ELogDateTimeInfo DateTimeInfo;
		};

	public:
		GHashMap<uint64, DLogData> CachedLogDataMap;

		EDebuggerName DebuggerName;

		// See: https://en.cppreference.com/w/cpp/utility/format/formatter#Standard_format_specification
		// and https://en.cppreference.com/w/cpp/chrono/system_clock/formatter#Format_specification

		// TODO limit group name size to ensure aligned messages
		//  move time info to before the message once text is aligned

		static constexpr SStringView LogCategoryInfoFormat 		= "[{}]";
		static constexpr SStringView LogCategoryPaddedFormat	= "{:<7}";
		static constexpr SStringView LogFamilyInfoFormat		= "({})";
		static constexpr SStringView LogFamilyPaddedFormat		= "{:<25}";
	//	static constexpr SStringView LogThreadInfoFormat		= "[#{:0<3}]"; // TODO maybe adapt to support thread id (numeric and/or text)
		static constexpr SStringView PaddedNewlineFormat 		= "{:<33}";
		static inline SString const PaddedNewline				= SString::Format(SWindowsDebuggerLogger::PaddedNewlineFormat, '\n');

		static constexpr SStringView MessageFormat				= "{0:}";

		// NOTE need to use two pairs of curve brackets because the string will be subject to format twice
		static constexpr SStringView FileFuncInfoFormat			= "{{{{#{}:{} ${}}}}}";
		static constexpr SStringView FileInfoFormat 			= "{{{{#{}:{}}}}}";
		static constexpr SStringView FuncInfoFormat				= "{{{{${}}}}}";

		static constexpr SStringView DateTimeInfoFormat			= "<%{1:%F} @{1:%T}>";
		static constexpr SStringView DateInfoFormat				= "<%{1:%F}>";
		static constexpr SStringView TimeInfoFormat 			= "<@{1:%T}>";
	};

	SWindowsDebuggerLogger* ZzInternal_PlatformLogger = nullptr;

	// TODO refactor this into DetectInitDebuggerLogger (move it elsewhere? LoggerDeviceManager)
	bool InitPlatformLoggerDevice()
	{
		static SWindowsDebuggerLogger Logger;
		ZzInternal_PlatformLogger = addressof(Logger);
		return true;
	}

	ILoggerDevice* GetPlatformLoggerDevice()
	{
		// FIXME eventually a different logger (non-debugger) might be better; maybe make it with WinAPI/Vulkan
		AEON_ASSERT(ZzInternal_PlatformLogger);
		return ZzInternal_PlatformLogger;
	}
}
