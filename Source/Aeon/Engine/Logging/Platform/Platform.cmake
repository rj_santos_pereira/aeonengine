#add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/)

if (${AEON_PLATFORM_WINDOWS})
	target_sources(${PROJECT_NAME}
		PRIVATE
		${CMAKE_CURRENT_SOURCE_DIR}/WindowsDebuggerLogger.cpp
		)
endif()