#ifndef ZZINTERNAL_INCLUDE_AEON_ENGINE_LOGGING_LOGGERDEVICEMANAGER
#define ZZINTERNAL_INCLUDE_AEON_ENGINE_LOGGING_LOGGERDEVICEMANAGER

#include "Aeon/Framework/Framework.hpp"

namespace Aeon
{
	class ILoggerDevice;

	class SLoggerDeviceManager
	{
	public:
		static SLoggerDeviceManager& Instance();

		AEON_MAKE_NON_COPYABLE(SLoggerDeviceManager);
		AEON_MAKE_NON_MOVEABLE(SLoggerDeviceManager);

		void RegisterLogger(ILoggerDevice* Device);
		void UnregisterLogger(ILoggerDevice* Device);

		ILoggerDevice* GetLogger(uint8 Index) const;
		uint8 GetLoggerCount() const;

		void FlushLogDevices();

		static constexpr uint8 MaxDeviceCount = 255;

	private:
		explicit SLoggerDeviceManager() = default;

		// TODO should PIMPL be used to abstract implementation?

		ILoggerDevice* DeviceArray[MaxDeviceCount] = {};
		uint8 DeviceCount = 0; // TODO make atomic
	};

	// TODO move these methods to device manager class and adapt the interface

	extern bool InitPlatformLoggerDevice();

	extern ILoggerDevice* GetPlatformLoggerDevice();
}

#endif