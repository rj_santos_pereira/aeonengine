#ifndef ZZINTERNAL_INCLUDE_AEON_ENGINE_LOGGING_LOGGERDEVICE
#define ZZINTERNAL_INCLUDE_AEON_ENGINE_LOGGING_LOGGERDEVICE

#include "Aeon/Framework/Framework.hpp"

#include "Aeon/Engine/Logging/LogCategory.hpp"
#include "Aeon/Engine/Logging/LogFamily.hpp"
#include "Aeon/Engine/Logging/LoggerDeviceManager.hpp"

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ILoggerDevice, SLoggerDeviceManager

namespace Aeon
{
	class ILoggerDevice
	{
	protected:
		ILoggerDevice() { SLoggerDeviceManager::Instance().RegisterLogger(this); }
		~ILoggerDevice() { SLoggerDeviceManager::Instance().UnregisterLogger(this); }
		
	public:
		AEON_DECLARE_ACCESS_CLASS(DeviceManagerAccess, class SLoggerDeviceManager);

	//	AEON_MAKE_COPYABLE(ILoggerDevice);
		AEON_MAKE_NON_COPYABLE(ILoggerDevice);

		virtual SStringView LoggerName() const = 0;

		virtual void SetupNameInfo(uint64 LogId, SStringView const& FamilyName, SStringView const& CategoryName) = 0;
		virtual void SetupColorInfo(uint64 LogId, ETerminalColor FamilyColor, ETerminalColor CategoryColor, ETerminalColor MessageColor) = 0;
		virtual void SetupFlagInfo(uint64 LogId, ELogSourceInfo SourceInfo, ELogDateTimeInfo DateTimeInfo) = 0;
		virtual void SetupFileInfo(uint64 LogId, SStringView const& FileName, SStringView const& FilePath) = 0;
		virtual void SetupLineInfo(uint64 LogId, uint32 LineNumber, uint32 LineChar) = 0;
		virtual void SetupFuncInfo(uint64 LogId, SStringView const& FuncName, SStringView const& FuncSig) = 0;
		virtual void FinishSetup(uint64 LogId) = 0;

		virtual void LogMessage(uint64 LogId, SDateTime const& DateTime, SStringView const& Message) = 0;
		// FIXME maybe deprecate/remove LogId param
		virtual void FlushMessages(uint64 LogId) = 0;

		void SetRegistryIndex(DeviceManagerAccess, uint8 NewIndex)
		{
			RegistryIndex = NewIndex;
		}
		uint8 GetRegistryIndex(DeviceManagerAccess) const
		{
			return RegistryIndex;
		}

	private:
		uint8 RegistryIndex = uint8(-1);
	};
}

#endif