#ifndef ZZINTERNAL_INCLUDE_AEON_ENGINE_LOGGING_LOGCATEGORY
#define ZZINTERNAL_INCLUDE_AEON_ENGINE_LOGGING_LOGCATEGORY

#include "Aeon/Framework/Framework.hpp"

#include "Aeon/Engine/Logging/LogFlags.hpp"
#include "Aeon/Engine/Logging/Terminal/TerminalColor.hpp"

namespace Aeon
{
	struct DLogCategory
	{
		constexpr DLogCategory(SStringView NewName,
							   SStringView NewShortName,
							   uint8 NewLevel,
							   ETerminalColor NewColor,
							   ELogSourceInfo NewSourceInfo,
							   ELogDateTimeInfo NewDateTimeInfo,
							   ELogBehavior NewBehavior)
							   // TODO re-add Level; will be used to enable certain logs per build
							   //  E.g.:
							   //   Level of Error/Fatal < Warn < Info < Debug
							   //   Dev builds -> level 3, ProdDebug builds -> level 2, Prod builds -> 1
			: Name(NewName)
			, ShortName(NewShortName)
			, Color(NewColor)
			, SourceInfo(NewSourceInfo)
			, DateTimeInfo(NewDateTimeInfo)
			, Behavior(NewBehavior)
			, Level(NewLevel)
		{
		}

		SStringView Name; // TODO currently unused, maybe remove?
		SStringView ShortName;
		ETerminalColor Color;
		ELogSourceInfo SourceInfo;
		ELogDateTimeInfo DateTimeInfo;
		ELogBehavior Behavior;
		uint8 Level;
	};
}

#define AEON_DECLARE_LOG_CATEGORY(Id, ShortId, Color, SourceInfo, DateTimeInfo, BehaviorInfo, Level) \
    namespace Aeon::ZzInternal                                                                                                                       \
    {                                                                                                                                                \
		inline constexpr ::Aeon::DLogCategory Id{ AEON_TOKEN_STR(Id), AEON_TOKEN_STR(ShortId),                                                       \
												  Color, SourceInfo, DateTimeInfo, BehaviorInfo, Level };                                            \
		static_assert(Id.ShortName.CharCount() == 4,                                                                                                 \
			  		  "'AEON_DECLARE_LOG_CATEGORY(Id, ShortId, Level, Color, SourceInfo, DateTimeInfo, BehaviorInfo)': "                             \
                      "ShortId must have exactly 4 characters");                                                                                     \
    }                                                                                                                                                \
    static_assert(::Aeon::VAlwaysTrue<decltype(::Aeon::ZzInternal::Id)>, "AEON_DECLARE_LOG_CATEGORY: Macro must be used in the global scope");

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Default log categories

AEON_DECLARE_LOG_CATEGORY(Debug, DBUG, 4,
						  ETerminalColor::ForestGreen,
						  ELogSourceInfo::FileName | ELogSourceInfo::FilePath
						  	| ELogSourceInfo::LineNum | ELogSourceInfo::LineChar
							| ELogSourceInfo::FuncName | ELogSourceInfo::FuncSig,
						  ELogDateTimeInfo::Date | ELogDateTimeInfo::Time,
						  ELogBehavior::UseCategoryFlags | ELogBehavior::UseCategoryColor | ELogBehavior::ShouldFlush); // TODO review flags for this
AEON_DECLARE_LOG_CATEGORY(Info, INFO, 3,
						  ETerminalColor::Silver,
						  ELogSourceInfo::None,
						  ELogDateTimeInfo::None,
						  ELogBehavior::None);
AEON_DECLARE_LOG_CATEGORY(InfoVerbose, /*INFV*/INFO, 3,
						  ETerminalColor::Silver,
						  ELogSourceInfo::FileName | ELogSourceInfo::FilePath
							| ELogSourceInfo::LineNum | ELogSourceInfo::LineChar,
						  ELogDateTimeInfo::Date | ELogDateTimeInfo::Time,
						  ELogBehavior::None);
AEON_DECLARE_LOG_CATEGORY(Warning, WARN, 2,
						  ETerminalColor::Goldenrod,
						  ELogSourceInfo::None,
						  ELogDateTimeInfo::None,
						  ELogBehavior::UseCategoryColor);
AEON_DECLARE_LOG_CATEGORY(WarningVerbose, /*WRNV*/WARN, 2,
						  ETerminalColor::Goldenrod,
						  ELogSourceInfo::FileName | ELogSourceInfo::FilePath
						  	| ELogSourceInfo::LineNum | ELogSourceInfo::LineChar,
						  ELogDateTimeInfo::Date | ELogDateTimeInfo::Time,
						  ELogBehavior::UseCategoryColor);
AEON_DECLARE_LOG_CATEGORY(Error, ERRR, 1,
						  ETerminalColor::Firebrick,
						  ELogSourceInfo::None,
						  ELogDateTimeInfo::None,
						  ELogBehavior::UseCategoryColor
							| ELogBehavior::ShouldFlush);
AEON_DECLARE_LOG_CATEGORY(ErrorVerbose, /*ERRV*/ERRR, 1,
						  ETerminalColor::Firebrick,
						  ELogSourceInfo::FileName | ELogSourceInfo::FilePath
							| ELogSourceInfo::LineNum | ELogSourceInfo::LineChar,
						  ELogDateTimeInfo::Date | ELogDateTimeInfo::Time,
						  ELogBehavior::UseCategoryColor
							| ELogBehavior::ShouldFlush);
AEON_DECLARE_LOG_CATEGORY(FatalError, FATL, 0,
						  ETerminalColor::Firebrick,
						  ELogSourceInfo::FileName | ELogSourceInfo::FilePath
							| ELogSourceInfo::LineNum | ELogSourceInfo::LineChar
							| ELogSourceInfo::FuncName | ELogSourceInfo::FuncSig,
						  ELogDateTimeInfo::Date | ELogDateTimeInfo::Time,
						  ELogBehavior::UseCategoryColor
							| ELogBehavior::ShouldFlush | ELogBehavior::ShouldCrash);

#endif