#ifndef ZZINTERNAL_INCLUDE_AEON_ENGINE_LOGGING_LOGFLAGS
#define ZZINTERNAL_INCLUDE_AEON_ENGINE_LOGGING_LOGFLAGS

#include "Aeon/Framework/Framework.hpp"

// HACK Clang bug causes erroneous warning here (which is promoted to error with current build flags),
//  because of None being an enumerator of both of these types;
//  See: https://github.com/llvm/llvm-project/issues/73844
//  Strangely, if the enum definition is within anywhere else other than global scope,
//  the enumerators are NOT introduced in the wrong scope, despite the shadowing warnings,
//  so we only need to silence the warnings.
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wshadow"

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ELogSourceInfo

	AEON_DECLARE_FLAG_ENUM(ELogSourceInfo, uint8)
	(
		AEON_FLAG_ENUM_RULE_POWER,
		None,
		FileName,
		FilePath,
		FuncName,
		FuncSig,
		LineNum,
		LineChar
	);

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ELogDateTimeInfo

	AEON_DECLARE_FLAG_ENUM(ELogDateTimeInfo, uint8)
	(
		AEON_FLAG_ENUM_RULE_POWER,
		None,
		Date,
		Time
	//	DateTime = Date | Time
	);

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ELogBehavior

// TODO add thread_id at some point (prob to LogBehavior)
	AEON_DECLARE_FLAG_ENUM(ELogBehavior, uint8)
	(
		AEON_FLAG_ENUM_RULE_POWER,
		None,
		UseCategoryFlags,
		UseCategoryColor,
		ShouldFlush,
		ShouldCrash
	);

}

#pragma clang diagnostic pop

#endif