add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Platform)
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Terminal)

target_sources(${PROJECT_NAME}
	PUBLIC
		${CMAKE_CURRENT_SOURCE_DIR}/LogFlags.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/LogCategory.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/LogFamily.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/LoggerDevice.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/LoggerDeviceManager.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/Log.hpp
	PRIVATE
		${CMAKE_CURRENT_SOURCE_DIR}/LoggerDeviceManager.cpp
	)