#ifndef ZZINTERNAL_INCLUDE_AEON_ENGINE_LOGGING_LOGFAMILY
#define ZZINTERNAL_INCLUDE_AEON_ENGINE_LOGGING_LOGFAMILY

#include "Aeon/Framework/Framework.hpp"

#include "Aeon/Engine/Logging/LogFlags.hpp"
#include "Aeon/Engine/Logging/Terminal/TerminalColor.hpp"

namespace Aeon
{
	struct DLogFamily
	{
		constexpr DLogFamily(SStringView NewName,
							ETerminalColor NewColor,
							ELogSourceInfo NewSourceInfo,
							ELogDateTimeInfo NewDateTimeInfo)
			: Name(NewName)
			, Color(NewColor)
			, SourceInfo(NewSourceInfo)
			, DateTimeInfo(NewDateTimeInfo)
		{
		}

		SStringView Name;
		ETerminalColor Color;
		ELogSourceInfo SourceInfo;
		ELogDateTimeInfo DateTimeInfo;
	};
}

#define AEON_DECLARE_LOG_FAMILY(Id, Color, SourceInfo, DateTimeInfo) \
    namespace Aeon::ZzInternal                                                                                                                       \
    {                                                                                                                                                \
		inline constexpr ::Aeon::DLogFamily Id{ AEON_TOKEN_STR(Id), Color, SourceInfo, DateTimeInfo };                                               \
		static_assert(Id.Name.CharCount() <= 20,                                                                                                     \
			  		  "'AEON_DECLARE_LOG_FAMILY(Id, Color, SourceInfo, DateTimeInfo)': "                                                             \
                      "Name must have at most 20 characters");                                                                                       \
    }                                                                                                                                                \
	static_assert(::Aeon::VAlwaysTrue<decltype(::Aeon::ZzInternal::Id)>, "AEON_DECLARE_LOG_FAMILY: Macro must be used in the global scope");

#endif