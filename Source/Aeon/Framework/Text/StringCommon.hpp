#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_TEXT_STRINGCOMMON
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_TEXT_STRINGCOMMON

#include "Aeon/Framework/Core.hpp"

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	ECharacterSet

	enum struct ECharacterSet
	{
		ASCII,
		Unicode,
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	ECharacterEncoding

	enum struct ECharacterEncoding
	{
		Unknown,

		ASCII,
//		UCS2,

		UTF8,
		UTF16,
		UTF32,

#	if AEON_PLATFORM_WINDOWS
		Platform_Narrow = UTF8,
		Platform_Wide = UTF16, // CHECKME should this be something like UCS2?
#	elif AEON_PLATFORM_LINUX
		Platform_Narrow = UTF8,
		Platform_Wide = UTF32,
#	else
#		error Not implemented yet!
		// TODO other platforms
		Platform_Narrow = Unknown,
		Platform_Wide = Unknown,
#	endif
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	EStringJoinBehavior, EStringSplitBehavior

	struct TJoinBehaviorTag {};
	struct TSplitBehaviorTag {};

	// This is templated only to be able to alias different enums with the same definition
	template <class>
	struct ZzInternal_EStringJoinSplitBehavior
	{
		enum Flag : bool
		{
			IgnoreEmpty = false,
			IncludeEmpty = true,
		};
	};

	using EStringJoinBehavior = ZzInternal_EStringJoinSplitBehavior<TJoinBehaviorTag>;
	using EStringSplitBehavior = ZzInternal_EStringJoinSplitBehavior<TSplitBehaviorTag>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	StringOps

	struct StringOps // TODO rename this (maybe to CharOps?)
	{
		enum struct EOutOfBoundsBehavior : szint
		{
#		pragma warning(suppress: 4146) // TODO maybe disable this warning for the whole project?
			ReturnNullIndex = -1_sz,
			ReturnZeroIndex = 0,
			ReturnBufferSize = 1,
		};

		template <CCharacter CharType>
		static constexpr CharType NullTerminator = CharType('\x00');

		template <CCharacter CharType>
		static consteval bool IsNarrow()
		{
			return TIsSameClass<TRemoveConstVolatile<CharType>, char>::Value;
		}
		template <CCharacter CharType>
		static consteval bool IsWide()
		{
			return TIsSameClass<TRemoveConstVolatile<CharType>, wchar>::Value;
		}

		// TODO reimplement this using AeonSIMD
		// TODO move this to cpp with CPU dispatcher
#if AEON_USING_SIMD
		template <CCharacter CharType>
		static constexpr szint Length(CharType const* StrPtr)
		{
			szint StrLen = 0;

			if_consteval
			{
				while (*StrPtr++ != NullTerminator<CharType>)
				{
					++StrLen;
				}
			}
			else
			{
				// This implementation is based on Agner Fog's strlen implementation.

				constexpr szint CharSize = Aeon::SizeOf(CharType);
				constexpr szint RegSize = Aeon::SizeOf(__m256i);
				constexpr szint NumCharsPerWord = RegSize / CharSize;

				// Search through the first N chars until StrPtr points to an address aligned an 32-byte word (ymm/AVX register);
				// At the same time, count the number of chars and check if we find the null terminator.
				szint StrCharsWordOffset = (RegSize - (bit_cast<ptrint>(StrPtr) % RegSize)) / CharSize;
				while (StrCharsWordOffset > 0 && *StrPtr++ != NullTerminator<CharType>)
				{
					++StrLen;
					--StrCharsWordOffset;
				}

				// If we haven't found the null terminator yet, we'll continue searching, but 32-byte words at a time.
				bool HasZero = StrCharsWordOffset != 0;
				while (!HasZero)
				{
					constexpr __m256i ZeroMaskReg = { 0, 0, 0, 0 };

					__m256i StrReg = _mm256_load_si256(reinterpret_cast<__m256i const*>(StrPtr));

					if constexpr (CharSize == 4)
					{
						StrReg = _mm256_cmpeq_epi32(StrReg, ZeroMaskReg);
					}
					else if constexpr (CharSize == 2)
					{
						StrReg = _mm256_cmpeq_epi16(StrReg, ZeroMaskReg);
					}
					else // (CharSize == 1)
					{
						StrReg = _mm256_cmpeq_epi8(StrReg, ZeroMaskReg);
					}

					uint32 ZeroMask = _mm256_movemask_epi8(StrReg);
					HasZero = bool(ZeroMask);

					if (HasZero)
					{
						// std::countr_zero (bsf/tzcnt) computes the index of the zero from the current position,
						// as the mask is made from which character compared equal to zero;
						// this has to be further divided by the size of the word, as movemask operates on bytes (pi8),
						// and so it produces CharSize bits per character.
#					if AEON_IS_LITTLE_ENDIAN_ARCH
						StrLen += std::countr_zero(ZeroMask) / CharSize;
#					else
						// CHECKME this probably breaks in big-endian archs (prob need to use lzcnt instead and do some arithmetic)
#						error Not implemented yet
#					endif
					}
					else
					{
						StrPtr += NumCharsPerWord;
						StrLen += NumCharsPerWord;
					}
				}
			}

			return StrLen;
		}

		template <CCharacter CharType>
		static constexpr szint Length_Bounded(CharType const* StrPtr, szint StrBufSize,
											  EOutOfBoundsBehavior Behavior = EOutOfBoundsBehavior::ReturnNullIndex)
		{
			szint StrLen = 0;

			if_consteval
			{
				while (StrLen < StrBufSize && *StrPtr++ != NullTerminator<CharType>)
				{
					++StrLen;
				}

				if (StrLen >= StrBufSize)
				{
					switch (Behavior)
					{
						case EOutOfBoundsBehavior::ReturnNullIndex:
						case EOutOfBoundsBehavior::ReturnZeroIndex:
							return szint(Behavior);
						case EOutOfBoundsBehavior::ReturnBufferSize:
							return StrBufSize;
					}
				}
			}
			else
			{
				// This implementation is based on Agner Fog's strlen implementation.

				constexpr szint CharSize = Aeon::SizeOf(CharType);
				constexpr szint RegSize = Aeon::SizeOf(__m256i);
				constexpr szint NumCharsPerWord = RegSize / CharSize;

				// Search through the first N chars until StrPtr points to an address aligned an 32-byte word (ymm/AVX register);
				// At the same time, count the number of chars and check if we find the null terminator.
				szint StrCharsWordOffset = (RegSize - (bit_cast<ptrint>(StrPtr) % RegSize)) / CharSize;
				while (StrCharsWordOffset > 0 && StrLen < StrBufSize && *StrPtr++ != NullTerminator<CharType>)
				{
					++StrLen;
					--StrCharsWordOffset;
				}

				// If we haven't found the null terminator yet, we'll continue searching, but 32-byte words at a time.
				// If we would go over the size of the buffer, don't loop and go immediately to the exit.
				bool HasZero = StrCharsWordOffset != 0;
				while (!HasZero && StrLen < StrBufSize)
				{
					constexpr __m256i ZeroMaskReg = { 0, 0, 0, 0 };

					__m256i StrReg = _mm256_load_si256(reinterpret_cast<__m256i const*>(StrPtr));

					if constexpr (CharSize == 4)
					{
						StrReg = _mm256_cmpeq_epi32(StrReg, ZeroMaskReg);
					}
					else if constexpr (CharSize == 2)
					{
						StrReg = _mm256_cmpeq_epi16(StrReg, ZeroMaskReg);
					}
					else // (CharSize == 1)
					{
						StrReg = _mm256_cmpeq_epi8(StrReg, ZeroMaskReg);
					}

					uint32 ZeroMask = _mm256_movemask_epi8(StrReg);
					HasZero = bool(ZeroMask);

					// If one of the chars is zero, we'll check each one until we find it and finish
					if (HasZero)
					{
						// BitScanForward (tzcnt) computes the index of the zero from the current position,
						// as the mask is made from which character compared equal to zero;
						// this has to be further divided by the size of the word, as movemask operates on bytes (pi8),
						// and so it produces CharSize bits per character.
#					if AEON_IS_LITTLE_ENDIAN_ARCH
						StrLen += std::countr_zero(ZeroMask) / CharSize;
#					else
						// CHECKME this probably breaks in big-endian archs (prob need to use lzcnt instead and do some arithmetic)
#						error Not implemented yet
#					endif
					}
					else
					{
						StrPtr += NumCharsPerWord;
						StrLen += NumCharsPerWord;
					}
				}

				if (StrLen >= StrBufSize)
				{
					switch (Behavior)
					{
						case EOutOfBoundsBehavior::ReturnNullIndex:
						case EOutOfBoundsBehavior::ReturnZeroIndex:
							return szint(Behavior);
						case EOutOfBoundsBehavior::ReturnBufferSize:
							return StrBufSize;
					}
				}
			}

			return StrLen;
		}
#if 0 // TODO remove this
		template <CCharacter CharType>
		static constexpr ptrdffint Compare(CharType const* StrPtr1, CharType const* StrPtr2)
		{
			ptrdffint StrCmp = 0;
			if_consteval
			{
				CharType Char1, Char2;
				bool IsEqual, IsNull;
				do
				{
					Char1 = *StrPtr1++;
					Char2 = *StrPtr2++;
					IsEqual = Char1 == Char2;
					IsNull = Char1 == NullTerminator<CharType>;
				}
				while (IsEqual && !IsNull);

				if (!IsEqual)
				{
					if (IsNull)
					{
						StrCmp = -1;
					}
					else if (Char2 == NullTerminator<CharType>)
					{
						StrCmp = +1;
					}
					else
					{
						StrCmp = Char1 < Char2 ? -1 : +1;
					}
				}
			}
			else
			{
				constexpr __m256i ZeroMaskReg = { 0, 0, 0, 0 };

				uint32 CmpEqual;
				uint32 CmpNull;
				__m256i StrReg1;
				__m256i StrReg2;
				__m256i CmpReg;
				do
				{
					StrReg1 = _mm256_load_si256(reinterpret_cast<__m256i const*>(StrPtr1));
					StrReg2 = _mm256_load_si256(reinterpret_cast<__m256i const*>(StrPtr2));

					CmpReg = _mm256_cmpeq_epi8(StrReg1, StrReg2);
					StrReg1 = _mm256_cmpeq_epi8(StrReg1, ZeroMaskReg);

					CmpEqual = ~_mm256_movemask_epi8(CmpReg);
					CmpNull = _mm256_movemask_epi8(StrReg1);
				}
				while (!bool(CmpEqual) && bool(CmpNull));

			//	uint32 NequalIdx = CmpEqual ? std::countr_zero(CmpEqual) : /* / CharSize*/;
				uint32 ZeroIdx1 = std::countr_zero(CmpNull)/* / CharSize*/;

				if (!CmpEqual)
				{
					if (!CmpNull)
					{
						StrReg2 = _mm256_cmpeq_epi8(StrReg2, ZeroMaskReg);
						CmpNull = _mm256_movemask_epi8(StrReg2);

						if (CmpNull)
						{
							StrCmp = +1;
						}
						else
						{
							Index = std::countr_zero(CmpEqual)/* / CharSize*/;
							StrCmp = Char1 < Char2 ? -1 : +1;
						}
					}
					else
					{

						StrReg2 = _mm256_cmpeq_epi8(StrReg2, ZeroMaskReg);
						CmpNull = _mm256_movemask_epi8(StrReg2);

						StrCmp = -1;
					}
				}
			}
			return StrCmp;
		}
#endif
#else
		template <CCharacter CharType>
		static constexpr szint Length(CharType const* StrPtr)
		{
			szint StrLen = 0;

			if_consteval
			{
				while (*StrPtr++ != NullTerminator<CharType>)
				{
					++StrLen;
				}
			}
			else
			{
				// This implementation is based on glibc strlen implementation.

				// Search through the first N chars until StrPtr points to an address aligned to uint64 words (8 bytes);
				// At the same time, count the number of chars and check if we find the null terminator.
				szint StrCharsWordOffset = (Aeon::SizeOf(uint64) - (bit_cast<ptrint>(StrPtr) % Aeon::SizeOf(uint64)))
											  / (Aeon::SizeOf(CharType));
				while (StrCharsWordOffset > 0 && *StrPtr++ != NullTerminator<CharType>)
				{
					++StrLen;
					--StrCharsWordOffset;
				}

				// If we haven't found the null terminator yet, we'll continue searching, but uint64 words at a time.
				bool HasZero = StrCharsWordOffset != 0;
				while (!HasZero)
				{
					constexpr szint NumCharsPerWord = Aeon::SizeOf(uint64) / Aeon::SizeOf(CharType);

					// Reinterpret the next NumCharsPerWord as an uint64 word
					uint64 StrCharsWord = bitseq_cast<uint64>(StrPtr);

					// Bitwise magic to determine if any of the chars in the next 8 bytes are zero.
					// https://graphics.stanford.edu/~seander/bithacks.html#ZeroInWord
					if constexpr (Aeon::SizeOf(CharType) == 4)
					{
						HasZero = (StrCharsWord - 0x0000'0001'0000'0001_u64) & ~StrCharsWord & 0x8000'0000'8000'0000_u64;
					}
					else if constexpr (Aeon::SizeOf(CharType) == 2)
					{
						HasZero = (StrCharsWord - 0x0001'0001'0001'0001_u64) & ~StrCharsWord & 0x8000'8000'8000'8000_u64;
					}
					else // Aeon::SizeOf(CharType) == 1
					{
						HasZero = (StrCharsWord - 0x0101'0101'0101'0101_u64) & ~StrCharsWord & 0x8080'8080'8080'8080_u64;
					}

					// If one of the chars is zero, we'll check each one until we find it and finish
					if (HasZero)
					{
						// HACK this helps the compiler unroll the loop
						StrCharsWordOffset = NumCharsPerWord;
						while (StrCharsWordOffset > 0 && *StrPtr++ != NullTerminator<CharType>)
						{
							++StrLen;
							--StrCharsWordOffset;
						}
					}
					else
					{
						StrPtr += NumCharsPerWord;
						StrLen += NumCharsPerWord;
					}
				}
			}

			return StrLen;
		}

		template <CCharacter CharType>
		static constexpr szint Length_Bounded(CharType const* StrPtr, szint StrBufSize,
												 EOutOfBoundsBehavior Behavior = EOutOfBoundsBehavior::ReturnNullIndex)
		{
			szint StrLen = 0;

			if_consteval
			{
				while (StrLen < StrBufSize && *StrPtr++ != NullTerminator<CharType>)
				{
					++StrLen;
				}

				if (StrLen >= StrBufSize)
				{
					switch (Behavior)
					{
						case EOutOfBoundsBehavior::ReturnNullIndex:
						case EOutOfBoundsBehavior::ReturnZeroIndex:
							return szint(Behavior);
						case EOutOfBoundsBehavior::ReturnBufferSize:
							return StrBufSize;
					}
				}
			}
			else
			{
				// This implementation is based on glibc strlen implementation.

				// Search through the first N chars until StrPtr points to an address aligned to uint64 words (8 bytes);
				// At the same time, count the number of chars and check if we find the null terminator.
				szint StrCharsWordOffset = (Aeon::SizeOf(uint64) - (bit_cast<ptrint>(StrPtr) % Aeon::SizeOf(uint64)))
											  / (Aeon::SizeOf(CharType));
				while (StrCharsWordOffset > 0 && StrLen < StrBufSize && *StrPtr++ != NullTerminator<CharType>)
				{
					++StrLen;
					--StrCharsWordOffset;
				}

				// If we haven't found the null terminator yet, we'll continue searching, but uint64 words at a time.
				// If we went over the size of the buffer, don't loop and go immediately to the exit
				bool HasZero = StrLen >= StrBufSize || StrCharsWordOffset != 0;
				while (!HasZero)
				{
					constexpr szint NumCharsPerWord = Aeon::SizeOf(uint64) / Aeon::SizeOf(CharType);

					// Reinterpret the next NumCharsPerWord as an uint64 word
					uint64 StrCharsWord = StrLen + NumCharsPerWord <= StrBufSize ? bitseq_cast<uint64>(StrPtr) : 0;

					// Bitwise magic to determine if any of the chars in the next 8 bytes are zero.
					// https://graphics.stanford.edu/~seander/bithacks.html#ZeroInWord
					if constexpr (Aeon::SizeOf(CharType) == 4)
					{
						HasZero = (StrCharsWord - 0x0000'0001'0000'0001_u64) & ~StrCharsWord & 0x8000'0000'8000'0000_u64;
					}
					else if constexpr (Aeon::SizeOf(CharType) == 2)
					{
						HasZero = (StrCharsWord - 0x0001'0001'0001'0001_u64) & ~StrCharsWord & 0x8000'8000'8000'8000_u64;
					}
					else // Aeon::SizeOf(CharType) == 1
					{
						HasZero = (StrCharsWord - 0x0101'0101'0101'0101_u64) & ~StrCharsWord & 0x8080'8080'8080'8080_u64;
					}

					// If one of the chars is zero, we'll check each one until we find it and finish
					if (HasZero)
					{
						// HACK this helps the compiler unroll the loop
						StrCharsWordOffset = NumCharsPerWord;
						while (StrCharsWordOffset > 0 && StrLen < StrBufSize && *StrPtr++ != NullTerminator<CharType>)
						{
							++StrLen;
							--StrCharsWordOffset;
						}
					}
					else
					{
						StrPtr += NumCharsPerWord;
						StrLen += NumCharsPerWord;
					}
				}

				if (StrLen >= StrBufSize)
				{
					switch (Behavior)
					{
						case EOutOfBoundsBehavior::ReturnNullIndex:
						case EOutOfBoundsBehavior::ReturnZeroIndex:
							return szint(Behavior);
						case EOutOfBoundsBehavior::ReturnBufferSize:
							return StrBufSize;
					}
				}
			}

			return StrLen;
		}
#endif

		template <CCharacter CharType>
		static constexpr diffint Compare(CharType const* StrPtr1, CharType const* StrPtr2) // TODO remove this
		{
			diffint StrCmp = 0;
			if_consteval
			{
				CharType Char1, Char2;
				bool IsEqual, IsNull;
				do
				{
					Char1 = *StrPtr1++;
					Char2 = *StrPtr2++;
					IsEqual = Char1 == Char2;
					IsNull = Char1 == NullTerminator<CharType>;
				}
				while (IsEqual && !IsNull);

				if (!IsEqual)
				{
					if (IsNull)
					{
						StrCmp = -1;
					}
					else if (Char2 == NullTerminator<CharType>)
					{
						StrCmp = +1;
					}
					else
					{
						StrCmp = Char1 < Char2 ? -1 : +1;
					}
				}
			}
			else
			{
				// TODO implement this manually (with SIMD)?
				if constexpr (IsNarrow<CharType>())
				{
					StrCmp = std::strcmp(StrPtr1, StrPtr2);
				}
				else if constexpr (IsWide<CharType>())
				{
					StrCmp = std::wcscmp(StrPtr1, StrPtr2);
				}
				else
				{
					// Unsupported
					static_assert(VAlwaysFalse<CharType>);
				}
			}
			return StrCmp;
		}

#	pragma warning(push)
#	pragma warning(disable: 4996)

		template <CCharacter CharType>
		static void Copy(CharType* DstPtr, TAddConst<CharType>* SrcPtr, szint StrLen)
		{
			// Buffer must have at least size of StrLen + 1
			AEON_ASSERT(StrLen > 0);

			// Copies string until and including NT
			if constexpr (IsNarrow<CharType>())
			{
				std::strncpy(DstPtr, SrcPtr, StrLen);
			}
			else if constexpr (IsWide<CharType>())
			{
				std::wcsncpy(DstPtr, SrcPtr, StrLen);
			}
			else
			{
				// Unsupported
				static_assert(VAlwaysFalse<CharType>);
			}

			// Ensure null terminator is appended at the end
			DstPtr[StrLen] = NullTerminator<CharType>;
		}

		template <CCharacter CharType>
		static void Concat(CharType* DstPtr, TAddConst<CharType>* SrcPtr, szint StrLen)
		{
			// Buffer must have at least size of Length(DstPtr) + Length(SrcPtr) + 1
			AEON_ASSERT(StrLen > 0);

			if constexpr (IsNarrow<CharType>())
			{
				std::strncat(DstPtr, SrcPtr, StrLen);
			}
			else if constexpr (IsWide<CharType>())
			{
				std::wcsncat(DstPtr, SrcPtr, StrLen);
			}
			else
			{
				// Unsupported
				static_assert(VAlwaysFalse<CharType>);
			}
		}

#	pragma warning(pop)

		template <CCharacter CharType, class ... FormatDataTypes>
		static bool Format(CharType* BufStrPtr, szint& BufStrLen, TAddConst<CharType>* FmtStrPtr, FormatDataTypes&& ... FmtData)
	//	requires (TConjunction<TIsTriviallyCopyable<FormatDataTypes> ...>::Value)
		{
			diffint FormattedStrLen;

			if constexpr (IsNarrow<CharType>())
			{
				FormattedStrLen = std::snprintf(BufStrPtr, BufStrLen, FmtStrPtr, Aeon::Forward<FormatDataTypes>(FmtData) ...);
			}
			else if constexpr (IsWide<CharType>())
			{
				FormattedStrLen = std::swprintf(BufStrPtr, BufStrLen, FmtStrPtr, Aeon::Forward<FormatDataTypes>(FmtData) ...);
			}
			else
			{
				// Unsupported
				static_assert(VAlwaysFalse<CharType>);
			}

			bool IsFormatted = FormattedStrLen > 0 && szint(FormattedStrLen) < BufStrLen;
			if (IsFormatted)
			{
				BufStrLen = szint(FormattedStrLen);
			}

			return IsFormatted;
		}

		template <CCharacter CharType>
		static bool FormatDateTime(CharType* BufStrPtr, szint& BufStrLen, TAddConst<CharType>* FmtStrPtr, std::tm const& FmtDateTime)
		{
			szint FormattedStrLen;

			if constexpr (IsNarrow<CharType>())
			{
				FormattedStrLen = std::strftime(BufStrPtr, BufStrLen, FmtStrPtr, addressof(FmtDateTime));
			}
			else if constexpr (IsWide<CharType>())
			{
				FormattedStrLen = std::wcsftime(BufStrPtr, BufStrLen, FmtStrPtr, addressof(FmtDateTime));
			}
			else
			{
				// Unsupported
				static_assert(VAlwaysFalse<CharType>);
			}

			bool IsFormatted = FormattedStrLen > 0;
			if (IsFormatted)
			{
				BufStrLen = FormattedStrLen;
			}

			return IsFormatted;
		}

		// TODO maybe generalize this using the Two-Way algorithm (used in glibc)
		// See: http://www-igm.univ-mlv.fr/~lecroq/string/
		template <CCharacter CharType>
		static CharType* FindFirstOf(CharType* TargetStrPtr, TAddConst<CharType>* SubStrPtr)
		{
			if constexpr (IsNarrow<CharType>())
			{
				return std::strstr(TargetStrPtr, SubStrPtr);
			}
			else if constexpr (IsWide<CharType>())
			{
				return std::wcsstr(TargetStrPtr, SubStrPtr);
			}
			else
			{
				// Unsupported
				static_assert(VAlwaysFalse<CharType>);
			}
		}

		template <CCharacter CharType>
		static CharType* FindLastOf(CharType* TargetStrPtr, TAddConst<CharType>* SubStrPtr)
		{
			// TODO reimplement this in terms of strrchr and strncmp

			CharType* ResultStrPtr = nullptr;
			if constexpr (IsNarrow<CharType>())
			{
				while (CharType* LastStrPtr = std::strstr(TargetStrPtr, SubStrPtr))
				{
					ResultStrPtr = LastStrPtr;
					TargetStrPtr = LastStrPtr + 1;
				}
			}
			else if constexpr (IsWide<CharType>())
			{
				while (CharType* LastStrPtr = std::wcsstr(TargetStrPtr, SubStrPtr))
				{
					ResultStrPtr = LastStrPtr;
					TargetStrPtr = LastStrPtr + 1;
				}
			}
			else
			{
				// Unsupported
				static_assert(VAlwaysFalse<CharType>);
			}
			return ResultStrPtr;
		}

	};
}

#endif