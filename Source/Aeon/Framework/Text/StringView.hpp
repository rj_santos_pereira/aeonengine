#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_TEXT_STRINGVIEW
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_TEXT_STRINGVIEW

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Text/StringCommon.hpp"

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	GStringView

	// TODO make an implicit constructor that takes some internal class template type
	//  which can be specialized for different types to provide a string-like form of the type

	template <CCharacter CharType, ECharacterSet CharSetValue>
	class GStringView
	{
		// This is simply used by GString to determine the character encoding of the underlying string.
		// Also helps with avoiding mix-ups in encoding when passing views around.
		AEON_UNUSED(CharSetValue);

		static constexpr bool IsUsingNarrowChar = StringOps::IsNarrow<CharType>();
		static constexpr bool IsUsingWideChar = StringOps::IsWide<CharType>();

		// For now, limit the class to only narrow and wide char types (and ensure exclusivity)
		static_assert(IsUsingNarrowChar != IsUsingWideChar);

	public:
		using CharacterType = CharType;
		using IndexType = uint32; // TODO define this in terms of GString somehow

		constexpr GStringView() = default;

		explicit constexpr GStringView(CharacterType const* CharString, IndexType CharStringBufferSize)
		{
			AEON_ASSERT(CharString && CharStringBufferSize);
			BufferPtr = CharString;
			BufferCount = IndexType(StringOps::Length_Bounded(CharString, CharStringBufferSize,
															  StringOps::EOutOfBoundsBehavior::ReturnBufferSize));
		}
		constexpr GStringView(nullptr_type, IndexType) = delete;

		constexpr GStringView(CharacterType const* CharString)
		{
			AEON_ASSERT(CharString);
			BufferPtr = CharString;
			BufferCount = IndexType(StringOps::Length(CharString));
		}
		constexpr GStringView(nullptr_type) = delete;

		template <IndexType CharStringBufferSizeValue>
		constexpr GStringView(CharacterType const(& CharString)[CharStringBufferSizeValue])
		{
			BufferPtr = CharString;
			BufferCount = IndexType(StringOps::Length_Bounded(CharString, CharStringBufferSizeValue,
															  StringOps::EOutOfBoundsBehavior::ReturnBufferSize));
		}

		AEON_MAKE_COPYABLE(GStringView, constexpr);
		AEON_MAKE_MOVEABLE(GStringView, constexpr);

		// TODO rename methods
		AEON_FORCEINLINE constexpr CharacterType const* CharBuffer() const
		{
			return BufferPtr;
		}

		// TODO rename methods
		AEON_FORCEINLINE constexpr IndexType CharCount() const
		{
			return BufferCount;
		}

		AEON_FORCEINLINE constexpr bool IsEmpty() const
		{
			return BufferCount == 0;
		}

		AEON_FORCEINLINE constexpr bool IsValid() const
		{
			return BufferPtr != nullptr;
		}

		AEON_FORCEINLINE constexpr bool operator==(GStringView const& Other) const
		{
			return CharCount() == Other.CharCount()
				&& StringOps::Compare(CharBuffer(), Other.CharBuffer()) == 0;
		}
		AEON_FORCEINLINE constexpr strong_ordering operator<=>(GStringView const& Other) const
		{
			return StringOps::Compare(CharBuffer(), Other.CharBuffer()) <=> 0;
		}

		// CONVERSION OPERATORS FOR INTERNAL USE
		AEON_FORCEINLINE explicit operator std::string_view() const
		requires IsUsingNarrowChar
		{
			return std::string_view{ BufferPtr, BufferCount };
		}

		AEON_FORCEINLINE explicit operator std::wstring_view() const
		requires IsUsingWideChar
		{
			return std::wstring_view{ BufferPtr, BufferCount };
		}

	private:
		CharacterType const* BufferPtr = nullptr;
		IndexType BufferCount = 0;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	SStringView, STextStringView, SPlatformStringView

	// String view type encoding characters in ASCII.
	using SStringView = Aeon::GStringView<char, Aeon::ECharacterSet::ASCII>;

	// String view type encoding characters in UTF-8.
	using STextStringView = Aeon::GStringView<char, Aeon::ECharacterSet::Unicode>;

	// String view type encoding characters in a platform-specific Unicode encoding.
	using SPlatformStringView = Aeon::GStringView<wchar, Aeon::ECharacterSet::Unicode>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	Literal operators

	inline namespace Literals
	{
		AEON_FORCEINLINE inline auto operator""_strv(char const* Literal, szint Length)
			-> GStringView<char, Aeon::ECharacterSet::ASCII>
		{
			using StringViewType = GStringView<char, Aeon::ECharacterSet::ASCII>;
			return StringViewType{ Literal, StringViewType::IndexType(Length) };
		}

		AEON_FORCEINLINE inline auto operator""_tstrv(char const* Literal, szint Length)
			-> GStringView<char, Aeon::ECharacterSet::Unicode>
		{
			using StringViewType = GStringView<char, Aeon::ECharacterSet::Unicode>;
			return StringViewType{ Literal, StringViewType::IndexType(Length) };
		}

		AEON_FORCEINLINE inline auto operator""_pstrv(wchar const* Literal, szint Length)
			-> GStringView<wchar, Aeon::ECharacterSet::Unicode>
		{
			using StringViewType = GStringView<wchar, Aeon::ECharacterSet::Unicode>;
			return StringViewType{ Literal, StringViewType::IndexType(Length) };
		}
	}

#if 0
	template <SStringView ViewValue>
	SStringView operator""_c_strv()
	{
		// NOTE requires all members in SStringView to be public,
		//  and for a ctor with signature SStringView(char const*) to exist
		return ViewValue;
	}
#endif
}

#endif