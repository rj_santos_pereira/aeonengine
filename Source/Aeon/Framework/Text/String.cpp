#include "Aeon/Framework/Text/String.hpp"

#include <utf8proc.h>

namespace Aeon
{
	template <CCharacter CharType, ECharacterSet CharSetValue>
	auto GString<CharType, CharSetValue>::Reserve(IndexType NewBufferSize)
		-> GString&
	{
		ReserveStorage_Internal(NewBufferSize);
		return *this;
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	auto GString<CharType, CharSetValue>::Prune()
		-> GString&
	{
		PruneStorage_Internal();
		return *this;
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	auto GString<CharType, CharSetValue>::Join(StringViewArrayType const& StringArray, ViewType const& Separator,
											   EStringJoinBehavior::Flag ShouldIncludeEmpty)
		-> GString
	{
		AEON_ASSERT(Separator.IsValid());

		GString ResultString;

		if (IndexType StringCount = StringArray.Count())
		{
			IndexType SeparatorCharCount = Separator.CharCount();

			// Preallocate enough memory for the resulting string by visiting each string view
			{
				IndexType ResultStringSize = 0;

				ResultStringSize += SeparatorCharCount * (StringCount - 1) + 1;
				for (ViewType const& String : StringArray)
				{
					ResultStringSize += String.CharCount();
				}

				ResultString.ReserveStorage_Internal(ResultStringSize);
			}

			// This is set to true once we have joined at least one (non-empty) string;
			// this is needed to ensure correct behavior when EStringJoinBehavior::IgnoreEmpty is passed, and the first string is empty
			bool IsNonEmpty = false;

			ViewType const& FirstString = StringArray.Front();
			AEON_ASSERT(FirstString.IsValid());

			if (IndexType FirstStrCharCount = FirstString.CharCount())
			{
				ResultString.Concat_Internal(FirstString.CharBuffer(), FirstStrCharCount);
				IsNonEmpty = true;
			}

			for (IndexType Index = 1; Index < StringCount; ++Index)
			{
				ViewType const& CurrString = StringArray[Index];
				AEON_ASSERT(CurrString.IsValid());

				IndexType CurrStrCharCount = CurrString.CharCount();
				if (SeparatorCharCount && (ShouldIncludeEmpty || (CurrStrCharCount && IsNonEmpty)))
				{
					ResultString.Concat_Internal(Separator.CharBuffer(), SeparatorCharCount);
				}
				if (CurrStrCharCount)
				{
					ResultString.Concat_Internal(CurrString.CharBuffer(), CurrStrCharCount);
					IsNonEmpty = true;
				}
			}
		}

		return ResultString;
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	auto GString<CharType, CharSetValue>::Split(ViewType const& Separator, EStringSplitBehavior::Flag ShouldIncludeEmpty)
		-> StringViewArrayType
	{
		AEON_ASSERT(Separator.IsValid() && !Separator.IsEmpty());

		StringViewArrayType ResultArray;

		CharacterType* StrCharPtr = GetBufferPtr_Internal();
		IndexType StrCharCount = GetBufferCharCount_Internal();

		IndexType SeparatorCharCount = Separator.CharCount();

		if (StrCharCount >= SeparatorCharCount)
		{
			CharacterType* CurrCharPtr = StrCharPtr;
			CharacterType const* SeparatorCharPtr = Separator.CharBuffer();

			while (CharacterType* NextCharPtr = StringOps::FindFirstOf(CurrCharPtr, SeparatorCharPtr))
			{
				IndexType CurrCharCount = IndexType(NextCharPtr - CurrCharPtr);
				if (ShouldIncludeEmpty || CurrCharCount)
				{
					ResultArray.Emplace(CurrCharPtr, CurrCharCount);
				}

				CurrCharPtr = NextCharPtr + SeparatorCharCount;
			}
		}
		else
		{
			ResultArray.Emplace(StrCharPtr, StrCharCount);
		}

		return ResultArray;
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	auto GString<CharType, CharSetValue>::AddAt(IndexType Offset, ViewType InsertString)
		-> GString&
	{
		IndexType BufferCharCount = GetBufferCharCount_Internal();

		AEON_ASSERT(Offset < BufferCharCount);

		if (InsertString.IsEmpty())
		{
			return *this;
		}

		// NOTE Make sure the substring remains valid by copying it to a string if needed,
		//  otherwise, if it points to this string, madness would ensue.
		GString InsertStringCopy;
		if (CheckIsWithinBuffer_Internal(InsertString.CharBuffer()))
		{
			InsertStringCopy = InsertString;
			InsertString = InsertStringCopy;
		}

		CharacterType const* InsStrCharPtr = InsertString.CharBuffer();
		IndexType InsStrCharCount = InsertString.CharCount();

		ReserveStorage_Internal(BufferCharCount + InsStrCharCount);

		CharacterType* BufferCharPtr = GetBufferPtr_Internal() + Offset;

		AeonMemory::MemMove(BufferCharPtr + InsStrCharCount, BufferCharPtr, Aeon::SizeOf(CharacterType, BufferCharCount - Offset));
		AeonMemory::MemCopy(BufferCharPtr, InsStrCharPtr, Aeon::SizeOf(CharacterType, InsStrCharCount));

		SetBufferCharCount_Internal(BufferCharCount + InsStrCharCount);

		return *this;
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	auto GString<CharType, CharSetValue>::RemoveAt(IndexType Offset, IndexType Length)
		-> GString&
	{
		return ReplaceAt(Offset, Length, EmptyString);
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	auto GString<CharType, CharSetValue>::FindFirstOf(ViewType const& Substring, IndexType Offset) const
		-> IndexType
	{
		AEON_ASSERT(Substring.IsValid() && !Substring.IsEmpty());

		IndexType SubstrCharCount = Substring.CharCount();
		if (Offset + SubstrCharCount <= GetBufferCharCount_Internal())
		{
			CharacterType const* SubstrCharPtr = Substring.CharBuffer();
			CharacterType* StrCharPtr = GetBufferPtr_Internal() + Offset;

			SubstrCharPtr = StringOps::FindFirstOf(StrCharPtr, SubstrCharPtr);
			if (SubstrCharPtr)
			{
				return IndexType(SubstrCharPtr - StrCharPtr) + Offset;
			}
		}

		return NullIndex;
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	auto GString<CharType, CharSetValue>::FindNthOf(ViewType const& Substring, IndexType Index, IndexType Offset) const
		-> IndexType
	{
		AEON_ASSERT(Substring.IsValid() && !Substring.IsEmpty());

		IndexType SubstrCharCount = Substring.CharCount();
		if (Offset + SubstrCharCount <= GetBufferCharCount_Internal())
		{
			CharacterType const* SubstrCharPtr = Substring.CharBuffer();
			CharacterType* StrCharPtr = GetBufferPtr_Internal() + Offset;

			CharacterType* CurrCharPtr = StrCharPtr;
			CharacterType* NextCharPtr = CurrCharPtr;
			do
			{
				CurrCharPtr = StringOps::FindFirstOf(NextCharPtr, SubstrCharPtr);
				NextCharPtr = CurrCharPtr + SubstrCharCount;
			}
			while (CurrCharPtr && Index-- != 0);

			if (CurrCharPtr)
			{
				return IndexType(CurrCharPtr - StrCharPtr) + Offset;
			}
		}

		return NullIndex;
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	auto GString<CharType, CharSetValue>::FindLastOf(ViewType const& Substring, IndexType Offset) const
		-> IndexType
	{
		AEON_ASSERT(Substring.IsValid() && !Substring.IsEmpty());

		IndexType SubstrCharCount = Substring.CharCount();
		if (SubstrCharCount <= GetBufferCharCount_Internal())
		{
			CharacterType const* SubstrCharPtr = Substring.CharBuffer();
			CharacterType* StrCharPtr = GetBufferPtr_Internal() + Offset;

			SubstrCharPtr = StringOps::FindLastOf(StrCharPtr, SubstrCharPtr);
			if (SubstrCharPtr)
			{
				return IndexType(SubstrCharPtr - StrCharPtr) + Offset;
			}
		}

		return NullIndex;
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	bool GString<CharType, CharSetValue>::Contains(ViewType const& Substring) const
	{
		return FindFirstOf(Substring) != NullIndex;
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	auto GString<CharType, CharSetValue>::ReplaceAt(IndexType OldSubstringOffset, IndexType OldSubstringLength, ViewType NewSubstring)
		-> GString&
	{
		AEON_ASSERT(NewSubstring.IsValid());

		IndexType RemainingSubstrOffset = OldSubstringOffset + OldSubstringLength;
		IndexType OldBufCharCount = GetBufferCharCount_Internal();

		// Ensure that chars to replace fall within the buffer
		AEON_ASSERT(RemainingSubstrOffset < OldBufCharCount);

		if (OldSubstringLength == 0)
		{
			return *this;
		}

		GString NewSubstringCopy;
		if (CheckIsWithinBuffer_Internal(NewSubstring.CharBuffer()))
		{
			NewSubstringCopy = NewSubstring;
			NewSubstring = NewSubstringCopy;
		}

		CharacterType const* NewSubstrCharPtr = NewSubstring.CharBuffer();
		IndexType NewSubstrCharCount = NewSubstring.CharCount();

		diffint CharCountDiff = diffint(NewSubstrCharCount) - diffint(OldSubstringLength);
		IndexType NewBufCharCount = IndexType(OldBufCharCount + CharCountDiff);

		// Reserve storage if the new substring is longer than the substring we are replacing
		if (CharCountDiff > 0)
		{
			ReserveStorage_Internal(NewBufCharCount);
		}

		CharacterType* OldSubstrCharPtr = GetBufferPtr_Internal() + OldSubstringOffset;

		// First we need to reposition the remaining chars (chars after the old substring) to accommodate for the new substring
		CharacterType* OldRemainingCharPtr = OldSubstrCharPtr + OldSubstringLength;
		CharacterType* NewRemainingCharPtr = OldRemainingCharPtr + CharCountDiff;

		IndexType RemainingCharCount = OldBufCharCount - RemainingSubstrOffset;

		// Move the remaining chars
		AeonMemory::MemMove(NewRemainingCharPtr, OldRemainingCharPtr, Aeon::SizeOf(CharacterType, RemainingCharCount));
		// Now, replace the old substring with the new one
		AeonMemory::MemCopy(OldSubstrCharPtr, NewSubstrCharPtr, Aeon::SizeOf(CharacterType, NewSubstrCharCount));

		SetBufferCharCount_Internal(NewBufCharCount);

		return *this;
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	auto GString<CharType, CharSetValue>::ReplaceAllOf(ViewType OldSubstring, ViewType NewSubstring)
		-> GString&
	{
		AEON_ASSERT(OldSubstring.IsValid() && !OldSubstring.IsEmpty());
		AEON_ASSERT(NewSubstring.IsValid());

		// Subtracting the null terminator from the count here simplifies a tiny bit of logic
		IndexType OldBufferCharCount = GetBufferCharCount_Internal() - 1;
		diffint NewBufferCharCount = OldBufferCharCount + 1;

		GString OldSubstringCopy;
		if (CheckIsWithinBuffer_Internal(OldSubstring.CharBuffer()))
		{
			OldSubstringCopy = OldSubstring;
			OldSubstring = OldSubstringCopy;
		}

		GString NewSubstringCopy;
		if (CheckIsWithinBuffer_Internal(NewSubstring.CharBuffer()))
		{
			NewSubstringCopy = NewSubstring;
			NewSubstring = NewSubstringCopy;
		}

		CharacterType const* OldSubstrCharPtr = OldSubstring.CharBuffer();
		CharacterType const* NewSubstrCharPtr = NewSubstring.CharBuffer();

		IndexType OldSubstrCharCount = OldSubstring.CharCount();
		IndexType NewSubstrCharCount = NewSubstring.CharCount();

		// Execute different logic, depending on whether we need to accommodate for new characters or not.
		// We could use the same logic for both branches (in particular, the logic from the CharCountDiff > 0 branch),
		// but for some extra complexity, we can avoid allocating new memory altogether.
		auto CharCountDiff = diffint(NewSubstrCharCount) - diffint(OldSubstrCharCount);
		if (CharCountDiff > 0)
		{
			// Compute an estimate of the number of chars to preallocate enough memory for the new string;
			// Assume that the string comprises a sequence of N old substrings plus some extra chars at the end.
			IndexType MaxSubstrCount = OldBufferCharCount / OldSubstrCharCount;
			IndexType MaxNonSubstrCharCount = OldBufferCharCount % OldSubstrCharCount;

			IndexType MaxBufferCharCount = NewSubstrCharCount * MaxSubstrCount + MaxNonSubstrCharCount + 1;

			// NOTE Always change to dynamic storage, to simplify logic
			DDynAllocStorage NewStorageData;

			NewStorageData.Allocate(MaxBufferCharCount);

			CharacterType* CurrOldBufPtr = GetBufferPtr_Internal();
			CharacterType* CurrNewBufPtr = NewStorageData.BufferPtr;

			diffint CharOffset = -1;
			while (CharacterType* NextOldBufPtr = StringOps::FindFirstOf(CurrOldBufPtr, OldSubstrCharPtr))
			{
				IndexType OldNonSubstrCharCount = IndexType(NextOldBufPtr - CurrOldBufPtr);

				// First copy the part of the string that is not the substring (since the last substring we found,
				// or the start of the string on the first iteration) to the new buffer.
				AeonMemory::MemCopy(CurrNewBufPtr, CurrOldBufPtr, Aeon::SizeOf(CharacterType, OldNonSubstrCharCount));
				CurrOldBufPtr = NextOldBufPtr + OldSubstrCharCount;
				CurrNewBufPtr += OldNonSubstrCharCount;

				// Then, copy the substring itself to the new buffer
				AeonMemory::MemCopy(CurrNewBufPtr, NewSubstrCharPtr, Aeon::SizeOf(CharacterType, NewSubstrCharCount));
				CurrNewBufPtr += NewSubstrCharCount;

				CharOffset += OldNonSubstrCharCount + OldSubstrCharCount;
				NewBufferCharCount += CharCountDiff;
			}

			// Now copy the rest of the string, if there is anything to copy, to the new buffer
			AeonMemory::MemCopy(CurrNewBufPtr, CurrOldBufPtr,
								Aeon::SizeOf(CharacterType, OldBufferCharCount - CharOffset));

			CheckDestruct_Internal();
			ReconstructStorage_Internal(EStorageMode::Dynamic);

			Storage.AllocData = NewStorageData;
		}
		else
		{
			CharacterType* CurrBufPtr = GetBufferPtr_Internal();
			diffint CharOffset = 0;

			while (CharacterType* NextBufPtr = StringOps::FindFirstOf(CurrBufPtr, OldSubstrCharPtr))
			{
				diffint OldNonSubstrCharCount = NextBufPtr - CurrBufPtr;

				// Move chars between the previous substring that was replaced and the next substring that will be replaced
				//  next to the end of the previous substring;
				// This move is only necessary when CharCountDiff < 0: it does nothing in the first iteration,
				//	and on each subsequent iteration moves the chars further
				//	by accumulating the difference of chars between the old and the new substrings.
				AeonMemory::MemMove(CurrBufPtr + CharOffset, CurrBufPtr, Aeon::SizeOf(CharacterType, OldNonSubstrCharCount));
				// Now copy the chars of the new substring to the place of the old substring,
				//  taking into account the offset of the chars from each iteration.
				AeonMemory::MemCopy(NextBufPtr + CharOffset, NewSubstrCharPtr, Aeon::SizeOf(CharacterType, NewSubstrCharCount));

				CurrBufPtr = NextBufPtr + OldSubstrCharCount;
				CharOffset += CharCountDiff;

				NewBufferCharCount += CharCountDiff;
			}

			// If we had to replace even one substring, then we need to move the rest of the chars next to it (if CharCountDiff < 0).
			AeonMemory::MemMove(CurrBufPtr + CharOffset, CurrBufPtr,
								Aeon::SizeOf(CharacterType, OldBufferCharCount - NewBufferCharCount));
		}

		SetBufferCharCount_Internal(IndexType(NewBufferCharCount));

		return *this;
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	auto GString<CharType, CharSetValue>::ReplaceFirstOf(ViewType const& OldSubstring, ViewType const& NewSubstring)
		-> GString&
	{
	//	AEON_ASSERT(OldSubstring.IsValid() && !OldSubstring.IsEmpty());
	//	AEON_ASSERT(NewSubstring.IsValid());

		IndexType OldSubstringIdx = FindFirstOf(OldSubstring);
		if (OldSubstringIdx != NullIndex)
		{
			ReplaceAt(OldSubstringIdx, OldSubstring.CharCount(), NewSubstring);
		}

		return *this;
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	auto GString<CharType, CharSetValue>::ReplaceNthOf(ViewType const& OldSubstring, ViewType const& NewSubstring, IndexType Index)
		-> GString&
	{
	//	AEON_ASSERT(OldSubstring.IsValid() && !OldSubstring.IsEmpty());
	//	AEON_ASSERT(NewSubstring.IsValid());

		IndexType OldSubstringIdx = FindNthOf(OldSubstring, Index);
		if (OldSubstringIdx != NullIndex)
		{
			ReplaceAt(OldSubstringIdx, OldSubstring.CharCount(), NewSubstring);
		}

		return *this;
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	auto GString<CharType, CharSetValue>::ReplaceLastOf(ViewType const& OldSubstring, ViewType const& NewSubstring)
		-> GString&
	{
	//	AEON_ASSERT(OldSubstring.IsValid() && !OldSubstring.IsEmpty());
	//	AEON_ASSERT(NewSubstring.IsValid());

		IndexType OldSubstringIdx = FindLastOf(OldSubstring);
		if (OldSubstringIdx != NullIndex)
		{
			ReplaceAt(OldSubstringIdx, OldSubstring.CharCount(), NewSubstring);
		}

		return *this;
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	auto GString<CharType, CharSetValue>::ToUppercase(IndexType Offset, IndexType Length)
		-> GString&
	{
		ToCharacterCase_Internal(ECharacterCaseConversion::Upper, Offset, Length);
		return *this;
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	auto GString<CharType, CharSetValue>::ToUppercase()
		-> GString&
	{
		ToCharacterCase_Internal(ECharacterCaseConversion::Upper, 0, GetBufferCharCount_Internal() - 1);
		return *this;
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	auto GString<CharType, CharSetValue>::ToLowercase(IndexType Offset, IndexType Length)
		-> GString&
	{
		ToCharacterCase_Internal(ECharacterCaseConversion::Lower, Offset, Length);
		return *this;
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	auto GString<CharType, CharSetValue>::ToLowercase()
		-> GString&
	{
		ToCharacterCase_Internal(ECharacterCaseConversion::Lower, 0, GetBufferCharCount_Internal() - 1);
		return *this;
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	bool GString<CharType, CharSetValue>::Normalize()
	{
		return Normalize_Internal(UTF8PROC_COMPOSE);
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	bool GString<CharType, CharSetValue>::NormalizeForCompatibility()
	{
		return Normalize_Internal(UTF8PROC_COMPOSE | UTF8PROC_COMPAT);
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	bool GString<CharType, CharSetValue>::NormalizeForCompatibility_Casefold()
	{
		return Normalize_Internal(UTF8PROC_COMPOSE | UTF8PROC_COMPAT | UTF8PROC_CASEFOLD);
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	bool GString<CharType, CharSetValue>::Validate()
	{
		return Validate_Internal();
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	void GString<CharType, CharSetValue>::ReconstructStorage_Internal(EStorageMode NewMode)
	{
		switch (NewMode)
		{
			case EStorageMode::Dynamic:
				AeonMemory::MemConstruct<DDynAllocStorage>(Aeon::AddressOf(Storage.AllocData));
				break;
			case EStorageMode::Static:
				AeonMemory::MemConstruct<DSSOStorage>(Aeon::AddressOf(Storage.SSOData));
				break;
		}
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	void GString<CharType, CharSetValue>::ReserveStorage_Internal(IndexType NewElemCount)
	{
		// If we need to change the buffer (from static to dynamic), then allocate it immediately with the right size
		bool IsUsingSSO = IsUsingSSO_Internal();
		if (IsUsingSSO && NewElemCount > DSSOStorage::BufferCount)
		{
			// We are increasing the size of the string, so it can only go to dynamic storage
			DDynAllocStorage NewStorageData;

			// Allocate buffer with enough size to accommodate the new elements
			NewStorageData.Allocate(NewElemCount);

			// Copy the old string to the new buffer; copy the entire buffer, this simplifies/optimizes code in SBackInsertWrapper,
			// and happens rarely enough that it will probably not make a difference.
			AeonMemory::MemCopy(NewStorageData.BufferPtr, Storage.SSOData.BufferArr, Aeon::SizeOf(CharacterType, DSSOStorage::BufferCount));

#		if ZZINTERNAL_AEON_STRING_STORAGE_LAYOUT_WORKAROUND
			// No need to assign the rest of them, the static buffer element count won't ever be greater than 7 bits
			NewStorageData.BufferCharCount_LoWord16_LoByte7 = Storage.SSOData.BufferCharCount;
#		else
			NewStorageData.BufferCharCount = Storage.SSOData.BufferCharCount;
#		endif

			ReconstructStorage_Internal(EStorageMode::Dynamic);

			Storage.AllocData = NewStorageData;
		}
		// Otherwise, if we are using a dynamic buffer, check if we need to reallocate
		else if (!IsUsingSSO && NewElemCount > Storage.AllocData.BufferCount)
		{
			Storage.AllocData.Reallocate(NewElemCount);
		}
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	void GString<CharType, CharSetValue>::PruneStorage_Internal()
	{
		IndexType BufferCharCount = GetBufferCharCount_Internal();
		IndexType BufferCount = GetBufferCount_Internal();

		// We don't want to do anything if we are already using the static buffer,
		// and there's nothing we can do if the number of characters is exactly the size of the buffer
		if (!IsUsingSSO_Internal() && BufferCharCount < BufferCount)
		{
			if (BufferCharCount <= DSSOStorage::BufferCount)
			{
				DSSOStorage NewStorageData;

				// NOTE It's technically possible for the dynamic buffer to be smaller than DSSOStorage::BufferCount (see e.g. Normalize);
				//  Ideally we would solve this at some point so we could benefit from SSO in those cases;
				//  then we could always copy exactly DSSOStorage::BufferCount characters here.
				AeonMemory::MemCopy(NewStorageData.BufferArr, Storage.AllocData.BufferPtr,
									Aeon::SizeOf(CharacterType, AeonMath::Min(Storage.AllocData.BufferCount, DSSOStorage::BufferCount)));

				NewStorageData.BufferCharCount = BufferCharCount;

				Storage.AllocData.Deallocate();

				ReconstructStorage_Internal(EStorageMode::Static);

				Storage.SSOData = NewStorageData;
			}
			else
			{
				Storage.AllocData.Reallocate(BufferCharCount);
			}
		}
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	void GString<CharType, CharSetValue>::Construct_Internal(CharacterType const* CharStrPtr, IndexType CharStrLen)
	{
		IndexType CharCount = CharStrLen + 1;
		if (CharCount > DSSOStorage::BufferCount)
		{
			ReconstructStorage_Internal(EStorageMode::Dynamic);

			Storage.AllocData.Allocate(CharCount);
		}

		CharacterType* BufferPtr = GetBufferPtr_Internal();
		AeonMemory::MemCopy(BufferPtr, CharStrPtr, Aeon::SizeOf(CharacterType, CharStrLen));

		// Ensure last character is a null terminator
		BufferPtr[CharStrLen] = NullTerminatorChar;

		SetBufferCharCount_Internal(CharCount);
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	void GString<CharType, CharSetValue>::CheckDestruct_Internal()
	{
		if (!IsUsingSSO_Internal())
		{
			Storage.AllocData.Deallocate();
		}
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	void GString<CharType, CharSetValue>::ToCharacterCase_Internal(ECharacterCaseConversion CaseConversion, IndexType Offset, IndexType Length)
	{
		IndexType OldBufferCharCount = GetBufferCharCount_Internal();

		AEON_ASSERT(Offset + Length < OldBufferCharCount);

		if (Length == 0)
		{
			return;
		}

		if constexpr (CharacterEncoding != ECharacterEncoding::ASCII)
		{
			diffint NewBufferCharCount = OldBufferCharCount;

			auto CharCaseConversionLambda
				= CaseConversion == ECharacterCaseConversion::Upper
				? [](int32 Char) { return utf8proc_toupper(Char); }
				: [](int32 Char) { return utf8proc_tolower(Char); };

			// Preallocate enough memory in case we potentially always need more characters than the ones we are currently occupying;
			// This assumes that for each character that we need to convert, the character has minimal byte size in the encoding,
			// and the resulting character would have maximal byte size in the encoding.
			IndexType MaxBufferCharCount = OldBufferCharCount + Length * IndexType(4 / Aeon::SizeOf(CharacterType) - 1);

			// NOTE We'll use similar logic to ReplaceAllOf to place the new characters,
			//  but since we don't know ahead of time whether we need more or less code units for each character that we convert,
			//  we'll always change to dynamic storage, to simplify logic
			DDynAllocStorage NewStorageData;

			NewStorageData.Allocate(MaxBufferCharCount);

			CharacterType* OldBufferPtr = GetBufferPtr_Internal();
			CharacterType* NewBufferPtr = NewStorageData.BufferPtr;

			// Copy the front of the string that we don't want to convert
			AeonMemory::MemCopy(NewBufferPtr, OldBufferPtr, Aeon::SizeOf(CharacterType, Offset));

			OldBufferPtr += Offset;
			NewBufferPtr += Offset;

			// Now iterate for Length characters while converting the string;
			// note that we may convert more characters than Length since we iterate by code-point.
			IndexType Index = 0;
			while (Index < Length)
			{
				// To convert the characters, we'll need to iterate by code-point, do the conversion,
				// then re-encode the new code-point while placing it in the string.
				int32 CurrCodePoint;
				diffint OldCodeUnitCount;
				DecodeCodePoint_Internal(OldBufferPtr, CurrCodePoint, OldCodeUnitCount);

				CurrCodePoint = CharCaseConversionLambda(CurrCodePoint);

				diffint NewCodeUnitCount;
				EncodeCodePoint_Internal(NewBufferPtr, CurrCodePoint, NewCodeUnitCount);

				OldBufferPtr += OldCodeUnitCount;
				NewBufferPtr += NewCodeUnitCount;

				NewBufferCharCount += NewCodeUnitCount - OldCodeUnitCount;
				Index += IndexType(OldCodeUnitCount);
			}

			Offset += Index;

			// Copy the back of the string that we don't want to convert
			AeonMemory::MemCopy(NewBufferPtr, OldBufferPtr, Aeon::SizeOf(CharacterType, OldBufferCharCount - Offset));

			NewStorageData.SetBufferCharCount(IndexType(NewBufferCharCount));

			CheckDestruct_Internal();
			ReconstructStorage_Internal(EStorageMode::Dynamic);

			Storage.AllocData = NewStorageData;
		}
		else
		{
			auto CharCaseConversionLambda
				= CaseConversion == ECharacterCaseConversion::Upper
				? [](char Char) { return std::toupper(Char, std::locale::classic()); }
				: [](char Char) { return std::tolower(Char, std::locale::classic()); };

			CharacterType* BufferPtr = GetBufferPtr_Internal() + Offset;

			IndexType Index = 0;
			while (Index < Length)
			{
				BufferPtr[Index] = CharCaseConversionLambda(BufferPtr[Index]);
				++Index;
			}
		}
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	bool GString<CharType, CharSetValue>::Normalize_Internal(uint64 Flags)
	{
		if constexpr (CharacterEncoding != ECharacterEncoding::ASCII)
		{
			CharacterType* OldBufferPtr = GetBufferPtr_Internal();
			IndexType OldCharCount = GetBufferCharCount_Internal() - 1;

			// Assume for now that we need twice the amount of characters during normalization;
			IndexType EstimateCodePointCount = OldCharCount << 1;

			// NOTE always change to dynamic storage, to simplify logic
			DDynAllocStorage NewStorageData;

			// We multiply by 4 because we need an int32 buffer for the conversion.
			// We also enforce the alignment in this allocation to be able to use this as a buffer of int32,
			// with no risk of misaligned accesses.
			// We lastly add an extra character because decompose ignores the null terminator, but re-encode always adds one.
			// Note that this works because we still define the buffer size as EstimateCodePointCount (in int32 objects),
			// so utf8proc_decompose will never try to write to the char dedicated to the null terminator even when Actual == Estimate.
			IndexType EstimateCodeUnitCount = (EstimateCodePointCount << 2) + 1;
			NewStorageData.Allocate(EstimateCodeUnitCount, Aeon::AlignOf(int32));

			auto NewBufferPtr = AeonMemory::StartObjectArrayLifetime<int32[]>(NewStorageData.BufferPtr, EstimateCodePointCount);

			diffint ActualCodePointCount = Decompose_Internal(OldBufferPtr, NewBufferPtr, EstimateCodePointCount, Flags);
			if (ActualCodePointCount > EstimateCodePointCount)
			{
				EstimateCodePointCount = IndexType(ActualCodePointCount);

				// Reallocate enforces the alignment we defined during allocation
				EstimateCodeUnitCount = (EstimateCodePointCount << 2) + 1;
				NewStorageData.Reallocate(EstimateCodeUnitCount);

				NewBufferPtr = AeonMemory::StartObjectArrayLifetime<int32[]>(NewStorageData.BufferPtr, EstimateCodePointCount);

				// Let's try this again, should work now
				ActualCodePointCount = Decompose_Internal(OldBufferPtr, NewBufferPtr, EstimateCodePointCount, Flags);
			}

			// TODO maybe log errors
			AEON_FAILSAFE(ActualCodePointCount >= 0 && ActualCodePointCount <= EstimateCodePointCount, return false);

			diffint NewCharCount = Compose_Internal(NewBufferPtr, reinterpret_cast<CharacterType*>(NewBufferPtr), ActualCodePointCount, Flags);

			AEON_FAILSAFE(NewCharCount > 0, return false);

			// We are done, reinterpret the buffer back and reconstruct the storage with the new data
			NewStorageData.BufferPtr = AeonMemory::StartObjectArrayLifetime<CharacterType[]>(NewBufferPtr, EstimateCodeUnitCount);
			NewStorageData.SetBufferCharCount(IndexType(NewCharCount));

			CheckDestruct_Internal();
			ReconstructStorage_Internal(EStorageMode::Dynamic);

			Storage.AllocData = NewStorageData;
		}

		return true;
	}

	template <class CharType>
	struct TMakeReplacementChar;

	template <>
	struct TMakeReplacementChar<char>
	{
		static constexpr char Value[3]{ '\xEF', '\xBF', '\xBD' };
	};

#if 0
	template <>
	struct TMakeReplacementChar<utf8char>
	{
		static constexpr utf8char Value[3]{ u8'\xEF', u8'\xBF', u8'\xBD' };
	};

	template <>
	struct TMakeReplacementChar<utf16char>
	{
		static constexpr utf16char Value[1]{ u'\uFFFD' };
	};

	template <>
	struct TMakeReplacementChar<utf32char>
	{
		static constexpr utf32char Value[1]{ U'\uFFFD' };
	};
#endif

	template <>
	struct TMakeReplacementChar<wchar>
	{
		static constexpr wchar Value[1]{ L'\uFFFD' };
	};

	template <CCharacter CharType, ECharacterSet CharSetValue>
	bool GString<CharType, CharSetValue>::Validate_Internal()
	{
		bool IsValid = true;
		if constexpr (CharacterEncoding != ECharacterEncoding::ASCII)
		{
			constexpr int32 InvalidCodePoint = -1;

			constexpr auto&& ReplacementCharBuf = TMakeReplacementChar<CharacterType>::Value;
			constexpr diffint ReplacementCharCount = sizeof ReplacementCharBuf / sizeof(CharacterType);

			CharacterType* BufferPtr = GetBufferPtr_Internal();
			diffint BufferOffset = 0;

			diffint BufCharCount = GetBufferCharCount_Internal();

			diffint InvalidCodeUnitCount = 0;

			int32 CurrCodePoint;
			do
			{
				diffint CurrCodeUnitCount;
				DecodeCodePoint_Internal(BufferPtr, CurrCodePoint, CurrCodeUnitCount);

				if (CurrCodePoint <= InvalidCodePoint)
				{
					++InvalidCodeUnitCount;
				}
				else if (InvalidCodeUnitCount)
				{
					CharacterType* InvalidBufPtr = BufferPtr - InvalidCodeUnitCount;

					diffint CharCountDiff = ReplacementCharCount - InvalidCodeUnitCount;
					if (CharCountDiff > 0)
					{
						ReserveStorage_Internal(IndexType(BufCharCount + CharCountDiff));
					}

					AeonMemory::MemMove(BufferPtr + CharCountDiff, BufferPtr,
										Aeon::SizeOf(CharacterType, BufCharCount - BufferOffset));

					AeonMemory::MemCopy(InvalidBufPtr, ReplacementCharBuf, Aeon::SizeOf(CharacterType, ReplacementCharCount));

					BufCharCount += CharCountDiff;

					SetBufferCharCount_Internal(IndexType(BufCharCount));

					InvalidCodeUnitCount = 0;
					IsValid = false;
				}

				BufferPtr += CurrCodeUnitCount;
				BufferOffset += CurrCodeUnitCount;
			}
			while (CurrCodePoint != NullTerminatorChar);
		}
		else
		{
			CharacterType* BufferCharPtr = GetBufferPtr_Internal();
			IndexType BufferCharCount = GetBufferCharCount_Internal() - 1;

			for (IndexType Index = 0; Index < BufferCharCount; ++Index)
			{
				if (static_cast<uint8>(BufferCharPtr[Index]) > 0x7F)
				{
					BufferCharPtr[Index] = '\x7F';
					IsValid = false;
				}
			}
		}

		return IsValid;
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	auto GString<CharType, CharSetValue>::GetGraphemeAt_Internal(IndexType Index) const
		-> ViewType
	{
		if constexpr (CharacterEncoding != ECharacterEncoding::ASCII)
		{
			CharacterType* BufferPtr = GetBufferPtr_Internal();

			int32 CurrCodePoint;
			diffint CurrCodeUnitCount = 0;
			do
			{
				BufferPtr += CurrCodeUnitCount;
				DecodeCodePoint_Internal(BufferPtr, CurrCodePoint, CurrCodeUnitCount);
			}
			while (Index-- != 0 && CurrCodePoint != NullTerminatorChar);

			return CurrCodePoint != NullTerminatorChar
				   ? ViewType{ BufferPtr, IndexType(CurrCodeUnitCount) }
				   : ViewType{ BufferPtr, 0 };
		}
		else
		{
			IndexType BufferCharCount = GetBufferCharCount_Internal() - 1;
			return Index < BufferCharCount
				   ? ViewType{ GetBufferPtr_Internal() + Index, 1 }
				   : ViewType{ GetBufferPtr_Internal() + BufferCharCount, 0 };
		}
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	auto GString<CharType, CharSetValue>::GetGraphemeLength_Internal() const
		-> IndexType
	{
		// Initialize to -1 to pre-emptively discard the null character
		IndexType GraphemeCount = IndexType(-1);

		if constexpr (CharacterEncoding != ECharacterEncoding::ASCII)
		{
			CharacterType* BufferPtr = GetBufferPtr_Internal();

			int32 CurrCodePoint;
			do
			{
				diffint CodeUnitCount;
				DecodeCodePoint_Internal(BufferPtr, CurrCodePoint, CodeUnitCount);
				BufferPtr += CodeUnitCount;
				++GraphemeCount;
			}
			while (CurrCodePoint != NullTerminatorChar);
		}
		else
		{
			GraphemeCount += GetBufferCharCount_Internal();
		}

		return GraphemeCount;
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	auto GString<CharType, CharSetValue>::GetGraphemeClusterAt_Internal(IndexType Index) const
		-> ViewType
	{
		if constexpr (CharacterEncoding != ECharacterEncoding::ASCII)
		{
			constexpr int32 InvalidCodePoint = -1;

			CharacterType* BufferPtr = GetBufferPtr_Internal();

			int32 CurrCodePoint;
			int32 NextCodePoint;
			diffint CurrCodeUnitCount;
			DecodeCodePoint_Internal(BufferPtr, CurrCodePoint, CurrCodeUnitCount);

			CharacterType* ClusterCharPtr = nullptr;
			IndexType ClusterCharCount = 0;

			int32 GraphemeClusterState = 0;
			while (CurrCodePoint != NullTerminatorChar)
			{
				bool IsAtCodePointSequence = Index == 0;

				// If we are at the code point sequence required by the index, process the code units
				if (IsAtCodePointSequence)
				{
					ClusterCharPtr = ClusterCharPtr ? ClusterCharPtr : BufferPtr;
					ClusterCharCount += IndexType(CurrCodeUnitCount);
				}

				BufferPtr += CurrCodeUnitCount;
				DecodeCodePoint_Internal(BufferPtr, NextCodePoint, CurrCodeUnitCount);

				bool IsAtSequenceBoundary = NextCodePoint > InvalidCodePoint
										 && utf8proc_grapheme_break_stateful(CurrCodePoint, NextCodePoint, &GraphemeClusterState);

				// If we are at a sequence boundary, either we want to advance the index,
				// or (if we are already at the index we want) finish processing the sequence
				if (IsAtSequenceBoundary)
				{
					if (!IsAtCodePointSequence)
					{
						--Index;
					}
					else
					{
						break;
					}
				}

				CurrCodePoint = NextCodePoint;
			}

			return ClusterCharPtr
				   ? ViewType{ ClusterCharPtr, ClusterCharCount }
				   : ViewType{ BufferPtr, 0 };
		}
		else
		{
			IndexType BufferCharCount = GetBufferCharCount_Internal() - 1;
			return Index < BufferCharCount
				   ? ViewType{ GetBufferPtr_Internal() + Index, 1 }
				   : ViewType{ GetBufferPtr_Internal() + BufferCharCount, 0 };
		}
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	auto GString<CharType, CharSetValue>::GetGraphemeClusterLength_Internal() const
		-> IndexType
	{
		// Initialize to -1 to pre-emptively discard the null character
		IndexType GraphemeClusterCount = IndexType(-1);

		if constexpr (CharacterEncoding != ECharacterEncoding::ASCII)
		{
			constexpr int32 InvalidCodePoint = -1;

			CharacterType* BufferPtr = GetBufferPtr_Internal();

			int32 CurrCodePoint;
			int32 NextCodePoint;
			diffint CurrCodeUnitCount;
			DecodeCodePoint_Internal(BufferPtr, CurrCodePoint, CurrCodeUnitCount);

			int32 GraphemeClusterState = 0;
			while (CurrCodePoint != NullTerminatorChar)
			{
				BufferPtr += CurrCodeUnitCount;
				DecodeCodePoint_Internal(BufferPtr, NextCodePoint, CurrCodeUnitCount);

				bool IsAtSequenceBoundary = NextCodePoint > InvalidCodePoint
										 && utf8proc_grapheme_break_stateful(CurrCodePoint, NextCodePoint, &GraphemeClusterState);

				if (IsAtSequenceBoundary)
				{
					++GraphemeClusterCount;
				}

				CurrCodePoint = NextCodePoint;
			}

			++GraphemeClusterCount;
		}
		else
		{
			GraphemeClusterCount += GetBufferCharCount_Internal();
		}

		return GraphemeClusterCount;
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	diffint GString<CharType, CharSetValue>::Decompose_Internal(CharacterType* OldBufferPtr, int32* NewBufferPtr, diffint NewBufferSize,
																uint64 Flags)
	{
		auto ConversionFlags = utf8proc_option_t(Flags | UTF8PROC_NULLTERM);

		diffint NewCodePointCount = 0;
		if constexpr (CharacterEncoding == ECharacterEncoding::UTF8)
		{
			NewCodePointCount = utf8proc_decompose(reinterpret_cast<uint8 const*>(OldBufferPtr), 0, NewBufferPtr, NewBufferSize, ConversionFlags);
		}
		else
		{
			while (true)
			{
				int32 CurrCodePoint;
				diffint CurrCodeUnitCount;
				DecodeCodePoint_Internal(OldBufferPtr, CurrCodePoint, CurrCodeUnitCount);

				if (CurrCodePoint == NullTerminatorChar)
				{
					break;
				}

				diffint CurrCodePointCount = utf8proc_decompose_char(CurrCodePoint, NewBufferPtr, NewBufferSize, ConversionFlags, nullptr);

				AEON_FAILSAFE(CurrCodePointCount > 0, return CurrCodePointCount);

				OldBufferPtr += CurrCodeUnitCount;

				NewBufferSize -= CurrCodePointCount;
				NewBufferPtr += CurrCodePointCount;

				NewCodePointCount += CurrCodePointCount;
			}

			// Now sort adjacent code-points per their combining class property.
			// CHECKME does composition (utf8proc_normalize_utf32) need this (insertion) sort to function properly?
			// TODO test this properly
			if (NewCodePointCount <= NewBufferSize)
			{
				diffint Index = 1, SortedIndex = 1;
				while (Index < NewCodePointCount)
				{
					int32 CodePoint1 = NewBufferPtr[Index - 1];
					int32 CodePoint2 = NewBufferPtr[Index];

					utf8proc_property_t const* Property1 = utf8proc_get_property(CodePoint1);
					utf8proc_property_t const* Property2 = utf8proc_get_property(CodePoint2);

					if (Property1->combining_class > Property2->combining_class && Property2->combining_class > 0)
					{
						NewBufferPtr[Index - 1] = CodePoint2;
						NewBufferPtr[Index] = CodePoint1;

						// Backtrack to compare with any previous combining class code-points that may exist.
						if (Index > 1)
						{
							--Index;
							continue;
						}
					}

					SortedIndex = Index = AeonMath::Max(Index, SortedIndex) + 1;
				}
			}
		}

		return NewCodePointCount;
	}

	// TODO better name; Normalize currently does, but does not necessarily "compose" (e.g.: NFD, NFKD)
	template <CCharacter CharType, ECharacterSet CharSetValue>
	diffint GString<CharType, CharSetValue>::Compose_Internal(int32* OldBufferPtr, CharacterType* NewBufferPtr, diffint OldBufferSize,
															  uint64 Flags)
	{
		auto ConversionFlags = utf8proc_option_t(Flags | UTF8PROC_STABLE);

		// The resulting code-point string should never be longer than the equivalent decomposed string
		diffint NewCodePointCount = utf8proc_normalize_utf32(OldBufferPtr, OldBufferSize, ConversionFlags);

		AEON_FAILSAFE(NewCodePointCount >= 0, return NewCodePointCount);

		// NOTE This is a workaround to preserve well-defined behavior: we use a temporary buffer to store the intermediate result of the encoding,
		//  and then just memcopy it to the storage; this has a measurable runtime cost, but we'll eat it for now.
		//  This is because NewBufferPtr is not technically a CharacterType[] at this point...
		//  we could get away for the 'char' case simply because it is allowed for 'char' to alias any object (in this case, int32), but meh.
		CharacterType TempBuffer[4];
		diffint NewCharCount = 0;

		for (diffint Index = 0; Index < NewCodePointCount; ++Index)
		{
			int32 CurrCodePoint = OldBufferPtr[Index];
			diffint CurrCodeUnitCount;
			EncodeCodePoint_Internal(TempBuffer, CurrCodePoint, CurrCodeUnitCount);

			AeonMemory::MemCopy(NewBufferPtr, TempBuffer, Aeon::SizeOf(CharacterType, CurrCodeUnitCount));

			NewBufferPtr += CurrCodeUnitCount;
			NewCharCount += CurrCodeUnitCount;
		}

		*NewBufferPtr = NullTerminatorChar;

		return NewCharCount + 1;
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	bool GString<CharType, CharSetValue>::DecodeCodePoint_Internal(CharacterType* BufferPtr, int32& CodePoint, diffint& CharsToAdvance)
	{
		constexpr int32 UnicodeLimit 		= 0x0010'FFFF_i32;
		constexpr int32 ASCIILimit 			= 0x0000'007F_i32;

		constexpr uint16 HighSurrogateMin 	= 0xD800_u16;
		constexpr uint16 HighSurrogateMax 	= 0xDBFF_u16;
		constexpr uint16 LowSurrogateMin 	= 0xDC00_u16;
		constexpr uint16 LowSurrogateMax 	= 0xDFFF_u16;

		// NOTE we always want to advance at least one character even when the string is invalid,
		//  so that code that is iterating through the string doesn't live-lock (if it does no error-checking);
		//  in particular, we advance exactly once when there is an error, which is the most conservative approach:
		//  this allows the most flexibility for error-checking code to handle the invalid string as it sees fit (see e.g. Validate)
		CharsToAdvance = 1;
		if constexpr (CharacterEncoding == ECharacterEncoding::UTF8)
		{
			// This is well-defined per strict-aliasing rules, but then again, we are interacting with a C library, so who cares...
			diffint CharCount = utf8proc_iterate(reinterpret_cast<uint8*>(BufferPtr), -1, addressof(CodePoint));

			// When negative, this is an error code, assign it to the CodePoint so that the caller can inspect it.
			if (CharCount < 0)
			{
				CodePoint = int32(CharCount);
			}

			AEON_FAILSAFE(CharCount > 0, return false);

			CharsToAdvance = CharCount;
		}
		else if constexpr (CharacterEncoding == ECharacterEncoding::UTF16)
		{
			constexpr int32 SurrogateBase 			= 0x0001'0000_i32;

			constexpr uint16 HighSurrogateOffset 	= HighSurrogateMin;
			constexpr uint16 LowSurrogateOffset 	= LowSurrogateMin;

			constexpr uint32 SurrogateShift 		= 0x0000'000A_u32;

			CodePoint = UTF8PROC_ERROR_INVALIDUTF8;

			uint16 CodeUnit = bit_cast<uint16>(*BufferPtr++);

			bool IsSurrogatePair = HighSurrogateMin <= CodeUnit && CodeUnit <= HighSurrogateMax;
			if (IsSurrogatePair)
			{
				int32 HighSurrogate = CodeUnit;

				CodeUnit = bit_cast<uint16>(*BufferPtr++);
				AEON_FAILSAFE(LowSurrogateMin <= CodeUnit && CodeUnit <= LowSurrogateMax, return false);

				int32 LowSurrogate = CodeUnit;

				HighSurrogate -= HighSurrogateOffset;
				LowSurrogate -= LowSurrogateOffset;

				CodePoint = SurrogateBase + (HighSurrogate << SurrogateShift) + LowSurrogate;
				CharsToAdvance = 2;
			}
			else
			{
				AEON_FAILSAFE(CodeUnit < HighSurrogateMin || LowSurrogateMax < CodeUnit, return false);

				CodePoint = CodeUnit;
				CharsToAdvance = 1;
			}
		}
		else if constexpr (CharacterEncoding == ECharacterEncoding::UTF32)
		{
			CodePoint = UTF8PROC_ERROR_INVALIDUTF8;

			uint32 CodeUnit = bit_cast<uint32>(*BufferPtr++);
			AEON_FAILSAFE(CodeUnit <= UnicodeLimit && (CodeUnit < HighSurrogateMin || LowSurrogateMax < CodeUnit), return false);

			CodePoint = CodeUnit;
			CharsToAdvance = 1;
		}
		else // ASCII
		{
			CodePoint = -1;

			uint8 CodeUnit = static_cast<uint8>(*BufferPtr++);
			AEON_FAILSAFE(CodeUnit <= ASCIILimit, return false);

			CodePoint = CodeUnit;
			CharsToAdvance = 1;
		}

		return true;
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	bool GString<CharType, CharSetValue>::EncodeCodePoint_Internal(CharacterType* BufferPtr, int32 CodePoint, diffint& CharsToAdvance)
	{
		constexpr int32 UnicodeLimit 	= 0x0010'FFFF_i32;
		constexpr int32 ASCIILimit 		= 0x0000'007F_i32;

		// Indicates an error of some sort, we should process it somehow
		AEON_FAILSAFE(CodePoint >= 0, return false);

		CharsToAdvance = 0;
		if constexpr (CharacterEncoding == ECharacterEncoding::UTF8)
		{
			diffint CharCount = utf8proc_encode_char(CodePoint, reinterpret_cast<uint8*>(BufferPtr));

			AEON_FAILSAFE(CharCount > 0, return false);

			CharsToAdvance = CharCount;
		}
		else if constexpr (CharacterEncoding == ECharacterEncoding::UTF16)
		{
			constexpr int32 SurrogateBase 			= 0x0001'0000_i32;

			constexpr uint16 HighSurrogateOffset 	= 0xD800_u16;
			constexpr uint16 LowSurrogateOffset 	= 0xDC00_u16;

			constexpr uint32 SurrogateShift 		= 0x0000'000A_u32;
			constexpr uint32 SurrogateRange 		= 0x0000'03FF_u32;

			AEON_FAILSAFE(CodePoint <= UnicodeLimit, return false);

			if (CodePoint >= SurrogateBase)
			{
				CodePoint -= SurrogateBase;

				uint16 HighSurrogate = static_cast<uint16>(uint32(CodePoint >> SurrogateShift) & SurrogateRange) + HighSurrogateOffset;
				uint16 LowSurrogate = static_cast<uint16>(uint32(CodePoint) & SurrogateRange) + LowSurrogateOffset;

				*BufferPtr++ = bit_cast<CharacterType>(HighSurrogate);
				*BufferPtr++ = bit_cast<CharacterType>(LowSurrogate);

				CharsToAdvance = 2;
			}
			else
			{
				*BufferPtr++ = bit_cast<CharacterType>(static_cast<uint16>(CodePoint));

				CharsToAdvance = 1;
			}
		}
		else if constexpr (CharacterEncoding == ECharacterEncoding::UTF32)
		{
			AEON_FAILSAFE(CodePoint <= UnicodeLimit, return false);

			*BufferPtr++ = bit_cast<CharacterType>(CodePoint);

			CharsToAdvance = 1;
		}
		else // ASCII
		{
			AEON_FAILSAFE(CodePoint <= ASCIILimit, return false);

			*BufferPtr++ = static_cast<uint8>(CodePoint);

			CharsToAdvance = 1;
		}

		return true;
	}

	// These are currently the only allowed instantiations
	template class GString<char, ECharacterSet::ASCII>;
	template class GString<char, ECharacterSet::Unicode>;
	template class GString<wchar, ECharacterSet::Unicode>;
}
