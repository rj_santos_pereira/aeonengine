#include "Aeon/Framework/Text/Localization/LocaleManager.hpp"

namespace Aeon
{
	SLocaleManager::SLocaleManager()
		: InvariantLocale(SLocale::LocaleManagerAccess::Tag, std::locale::classic())
		, CachedSystemLocale(SLocale::LocaleManagerAccess::Tag)
		, CurrentLocale(InvariantLocale)
	{
		// FIXME hackish way to get the list of installed locales
		//  see https://learn.microsoft.com/en-us/windows/win32/intl/retrieving-and-setting-locale-information
		//  and https://learn.microsoft.com/en-us/windows/win32/api/winnls/nf-winnls-setthreadlocale
		auto CurrentWorkingDirectory = stdfs::path("D:/AeonEngine/Resource");
		auto LocaleListFilePath = CurrentWorkingDirectory / "SystemLocales.txt";

		if (!exists(LocaleListFilePath))
		{
			// TODO log error (no locale list)
			return;
		}

		std::ifstream LocaleListFile{ LocaleListFilePath };
		std::string LocaleLine;

		while (std::getline(LocaleListFile, LocaleLine))
		{
			InstalledLocalesArray.Add(SLocale(SLocale::LocaleManagerAccess::Tag, LocaleLine.c_str()));
		}

		if (InstalledLocalesArray.IsEmpty())
		{
			// TODO log error (no default locale)
			return;
		}

		CachedSystemLocale = InstalledLocalesArray.Front();
		CurrentLocale = CachedSystemLocale;
	}

	SLocaleManager* SLocaleManager::Instance()
	{
		static SLocaleManager Manager;
		return addressof(Manager);
	}

	GArray<SLocale> const& SLocaleManager::GetAvailableLocales() const
	{
		return InstalledLocalesArray;
	}

	SLocale const& SLocaleManager::GetInvariantLocale() const
	{
		return InvariantLocale;
	}

	SLocale const& SLocaleManager::GetSystemLocale() const
	{
		return CachedSystemLocale;
	}

	SLocale const& SLocaleManager::GetCurrentLocale() const
	{
		return CurrentLocale;
	}

	void SLocaleManager::SetCurrentLocale(SLocale const& NewLocale)
	{
		CurrentLocale = NewLocale;
	}
}
