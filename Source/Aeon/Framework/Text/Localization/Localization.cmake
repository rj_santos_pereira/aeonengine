#add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/)

target_sources(${PROJECT_NAME}
	PUBLIC
		${CMAKE_CURRENT_SOURCE_DIR}/Locale.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/LocaleManager.hpp
	PRIVATE
		${CMAKE_CURRENT_SOURCE_DIR}/Locale.cpp
		${CMAKE_CURRENT_SOURCE_DIR}/LocaleManager.cpp
	)