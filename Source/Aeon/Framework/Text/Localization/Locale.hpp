#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_TEXT_LOCALIZATION_LOCALE
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_TEXT_LOCALIZATION_LOCALE

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Text/StringView.hpp"

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	SLocale

	class SLocale
	{
	public:
		AEON_DECLARE_ACCESS_CLASS(LocaleManagerAccess, class SLocaleManager);

		explicit SLocale(LocaleManagerAccess);
		explicit SLocale(LocaleManagerAccess, std::locale const& NewLocale);
		explicit SLocale(LocaleManagerAccess, char const* NewLocaleName);

		SStringView GetName() const;

		// TODO expose locale functionality through this class

		template <CCharacter CharType, ECharacterSet CharSetValue>
		diffint Collate(GStringView<CharType, CharSetValue> const& String1, GStringView<CharType, CharSetValue> const& String2) const;

		std::locale AsNative() const;

	private:
		std::locale Locale;
	};
}

#endif