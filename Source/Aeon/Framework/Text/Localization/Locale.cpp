#include "Aeon/Framework/Text/Localization/Locale.hpp"

namespace Aeon
{
	SLocale::SLocale(SLocale::LocaleManagerAccess)
		: Locale(std::locale::classic())
	{
	}
	SLocale::SLocale(SLocale::LocaleManagerAccess, std::locale const& NewLocale)
		: Locale(NewLocale)
	{
	}
	SLocale::SLocale(SLocale::LocaleManagerAccess, char const* NewLocaleName)
		: Locale(NewLocaleName)
	{
	}

	SStringView SLocale::GetName() const
	{
		return Locale.c_str();
	}

	std::locale SLocale::AsNative() const
	{
		return Locale;
	}

	template <CCharacter CharType, ECharacterSet CharSetValue>
	diffint SLocale::Collate(GStringView<CharType, CharSetValue> const& String1, GStringView<CharType, CharSetValue> const& String2) const
	{
		CharType const* StrCharPtr1 = String1.CharBuffer();
		CharType const* PastLastStrCharPtr1 = StrCharPtr1 + String1.CharCount();

		CharType const* StrCharPtr2 = String2.CharBuffer();
		CharType const* PastLastStrCharPtr2 = StrCharPtr2 + String2.CharCount();

		return diffint(std::use_facet<std::collate<CharType>>(Locale).compare(StrCharPtr1, PastLastStrCharPtr1,
																			  StrCharPtr2, PastLastStrCharPtr2));
	}

	template diffint SLocale::Collate<char, ECharacterSet::ASCII>(SStringView const&, SStringView const&) const;
	template diffint SLocale::Collate<char, ECharacterSet::Unicode>(STextStringView const&, STextStringView const&) const;
	template diffint SLocale::Collate<wchar, ECharacterSet::Unicode>(SPlatformStringView const&, SPlatformStringView const&) const;
}
