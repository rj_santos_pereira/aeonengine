#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_TEXT_LOCALIZATION_LOCALEMANAGER
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_TEXT_LOCALIZATION_LOCALEMANAGER

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Container/Array.hpp"
#include "Aeon/Framework/Text/Localization/Locale.hpp"

namespace Aeon
{
	class SLocaleManager
	{
		explicit SLocaleManager();

	public:
		static SLocaleManager* Instance();

		GArray<SLocale> const& GetAvailableLocales() const;

		SLocale const& GetInvariantLocale() const;

		SLocale const& GetSystemLocale() const;
	//	void SetSystemLocale(SLocale const& NewSystemLocale);

		SLocale const& GetCurrentLocale() const;
		void SetCurrentLocale(SLocale const& NewLocale);

	private:
		GArray<SLocale> InstalledLocalesArray;
		SLocale InvariantLocale;
		SLocale CachedSystemLocale;
		SLocale CurrentLocale;
	};
}

#endif