#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_TEXT_STRING
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_TEXT_STRING

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Container/Array.hpp"
#include "Aeon/Framework/Text/StringCommon.hpp"
#include "Aeon/Framework/Text/StringView.hpp"
#include "Aeon/Framework/Text/Localization/LocaleManager.hpp"

namespace Aeon
{
	class STimePoint;
	class SDateTime;

	template <CCharacter CharType, ECharacterSet CharSetValue /*, CAllocator AllocType*/>
	class GString;

	namespace ZzInternal
	{
		template <class, class = void>
		struct TEstimateStringStorageRequirements
		{
			static constexpr szint Value = 0;
		};

		template <>
		struct TEstimateStringStorageRequirements<bool>
		{
			static constexpr szint Value = 5;
		};

		template <class CharType>
		struct TEstimateStringStorageRequirements<CharType, TEnableIf<VIsCharacter<CharType>>>
		{
			static constexpr szint Value = 4 / sizeof(CharType);
		};
		template <class CharType>
		struct TEstimateStringStorageRequirements<CharType*, TEnableIf<VIsCharacter<CharType>>>
		{
			static constexpr szint Value = 16;
		};

		template <class IntType>
		struct TEstimateStringStorageRequirements<IntType, TEnableIf<VIsInteger<IntType>>>
		{
			static constexpr szint Value = 1 + TNumericProperties<IntType>::MinimumWritableDigits;
											 // 1 to account for the sign
		};
		template <class FloatType>
		struct TEstimateStringStorageRequirements<FloatType, TEnableIf<VIsFloatingPoint<FloatType>>>
		{
			static constexpr szint Value = 1 + TNumericProperties<FloatType>::MinimumWritableDigits + 1 + 5;
											 // 1 to account for the sign
																										// 1 to account for the decimal marker
																											// 5 to account for the exponent
		};

		template <>
		struct TEstimateStringStorageRequirements<STimePoint>
		{
			static constexpr szint Value = 20;
		};
		template <>
		struct TEstimateStringStorageRequirements<SDateTime>
		{
			static constexpr szint Value = 20;
		};

		template <CCharacter CharType, ECharacterSet CharSetValue>
		struct TEstimateStringStorageRequirements<GStringView<CharType, CharSetValue>>
		{
			static constexpr szint Value = 16;
		};
		template <CCharacter CharType, ECharacterSet CharSetValue>
		struct TEstimateStringStorageRequirements<GString<CharType, CharSetValue>>
		{
			static constexpr szint Value = 16;
		};
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	GString

	// TODO move const methods to some base class shared by GString and GStringView

	// FIXME add GDefaultObjectHasher specializations for GString and GStringView

	// TODO maybe add support for CAllocator eventually
	//  issue: Allocators currently do not support passing custom params to Allocate() (e.g. for passing flags to MemAlloc)
	template <CCharacter CharType, ECharacterSet CharSetValue/*, CAllocator AllocType*/>
	class GString
	{
		static constexpr bool IsUsingNarrowChar = StringOps::IsNarrow<CharType>();
		static constexpr bool IsUsingWideChar = StringOps::IsWide<CharType>();

		// For now, limit the class to only narrow and wide char types (and ensure exclusivity)
		static_assert(IsUsingNarrowChar != IsUsingWideChar);

		static constexpr ECharacterEncoding CharacterEncoding =
			IsUsingNarrowChar
			? (CharSetValue == ECharacterSet::ASCII
				? ECharacterEncoding::ASCII
				: ECharacterEncoding::Platform_Narrow)
			: ECharacterEncoding::Platform_Wide;

	public:
		using CharacterType = CharType;
		using IndexType = uint32; // TODO redefine this in terms of Allocator::IndexType

		using ViewType = GStringView<CharType, CharSetValue>;

		static constexpr CharacterType NullTerminatorChar = StringOps::NullTerminator<CharType>;
		static constexpr IndexType NullIndex = AeonMath::MakeBitMask<IndexType, Aeon::BitSizeOf(IndexType)>();

		static constexpr ViewType EmptyString = ViewType{ Aeon::AddressOf(NullTerminatorChar) };

		// Aliases for 'GArray<GString>' and 'GArray<GStringView>'
		// HACK Workaround for MSVC bug described here: https://developercommunity.visualstudio.com/t/C20-Error-C2027-when-instantiating-a/10393482
		//  Remove this workaround whenever possible (bug still present in version 19.35)
	//	using StringArrayType = GArray<GString, GDynamicAllocator<GString, IndexType>>;
		using StringViewArrayType = GArray<ViewType, GDynamicAllocator<ViewType, IndexType>>;

		GString() = default;

		explicit GString(CharacterType const* CharString, IndexType CharStringBufferSize)
		{
			AEON_ASSERT(CharString && CharStringBufferSize);
			if (auto CharStringLength = IndexType(StringOps::Length_Bounded(CharString, CharStringBufferSize,
																			StringOps::EOutOfBoundsBehavior::ReturnBufferSize)))
			{
				Construct_Internal(CharString, CharStringLength);
			}
		}
		explicit GString(utf8char const* CharString, IndexType CharStringBufferSize)
		requires (CharacterEncoding == ECharacterEncoding::UTF8 && IsUsingNarrowChar)
		{
			AEON_ASSERT(CharString && CharStringBufferSize);
			if (auto CharStringLength = IndexType(StringOps::Length_Bounded(CharString, CharStringBufferSize,
																			StringOps::EOutOfBoundsBehavior::ReturnBufferSize)))
			{
				Construct_Internal(AeonMemory::StartObjectArrayLifetime<CharacterType[]>(CharString, CharStringLength), CharStringLength);
			}
		}
		explicit GString(utf16char const* CharString, IndexType CharStringBufferSize)
		requires (CharacterEncoding == ECharacterEncoding::UTF16 && IsUsingWideChar)
		{
			AEON_ASSERT(CharString && CharStringBufferSize);
			if (auto CharStringLength = IndexType(StringOps::Length_Bounded(CharString, CharStringBufferSize,
																			StringOps::EOutOfBoundsBehavior::ReturnBufferSize)))
			{
				Construct_Internal(AeonMemory::StartObjectArrayLifetime<CharacterType[]>(CharString, CharStringLength), CharStringLength);
			}
		}
		explicit GString(utf32char const* CharString, IndexType CharStringBufferSize)
		requires (CharacterEncoding == ECharacterEncoding::UTF32 && IsUsingWideChar)
		{
			AEON_ASSERT(CharString && CharStringBufferSize);
			if (auto CharStringLength = IndexType(StringOps::Length_Bounded(CharString, CharStringBufferSize,
																			StringOps::EOutOfBoundsBehavior::ReturnBufferSize)))
			{
				Construct_Internal(AeonMemory::StartObjectArrayLifetime<CharacterType[]>(CharString, CharStringLength), CharStringLength);
			}
		}
		// Disallow directly initializing string from nullptr
		explicit GString(nullptr_type, IndexType) = delete;

		GString(CharacterType const* CharString)
		{
			AEON_ASSERT(CharString);
			if (auto CharStringLength = IndexType(StringOps::Length(CharString)))
			{
				Construct_Internal(CharString, CharStringLength);
			}
		}
		GString(utf8char const* CharString)
		requires (CharacterEncoding == ECharacterEncoding::UTF8 && IsUsingNarrowChar)
		{
			AEON_ASSERT(CharString);
			if (auto CharStringLength = IndexType(StringOps::Length(CharString)))
			{
				Construct_Internal(AeonMemory::StartObjectArrayLifetime<CharacterType[]>(CharString, CharStringLength), CharStringLength);
			}
		}
		GString(utf16char const* CharString)
		requires (CharacterEncoding == ECharacterEncoding::UTF16 && IsUsingWideChar)
		{
			AEON_ASSERT(CharString);
			if (auto CharStringLength = IndexType(StringOps::Length(CharString)))
			{
				Construct_Internal(AeonMemory::StartObjectArrayLifetime<CharacterType[]>(CharString, CharStringLength), CharStringLength);
			}
		}
		GString(utf32char const* CharString)
		requires (CharacterEncoding == ECharacterEncoding::UTF32 && IsUsingWideChar)
		{
			AEON_ASSERT(CharString);
			if (auto CharStringLength = IndexType(StringOps::Length(CharString)))
			{
				Construct_Internal(AeonMemory::StartObjectArrayLifetime<CharacterType[]>(CharString, CharStringLength), CharStringLength);
			}
		}
		// Disallow directly initializing string from nullptr
		GString(nullptr_type) = delete;

		template <IndexType CharStringBufferSizeValue>
		GString(CharacterType const(& CharString)[CharStringBufferSizeValue])
		{
			if (auto CharStringLength = IndexType(StringOps::Length_Bounded(CharString, CharStringBufferSizeValue,
																			StringOps::EOutOfBoundsBehavior::ReturnBufferSize)))
			{
				Construct_Internal(CharString, CharStringLength);
			}
		}
		template <IndexType CharStringBufferSizeValue>
		GString(utf8char const(& CharString)[CharStringBufferSizeValue])
		requires (CharacterEncoding == ECharacterEncoding::UTF8 && IsUsingNarrowChar)
		{
			if (auto CharStringLength = IndexType(StringOps::Length_Bounded(CharString, CharStringBufferSizeValue,
																			StringOps::EOutOfBoundsBehavior::ReturnBufferSize)))
			{
				Construct_Internal(AeonMemory::StartObjectArrayLifetime<CharacterType[]>(CharString, CharStringLength), CharStringLength);
			}
		}
		template <IndexType CharStringBufferSizeValue>
		GString(utf16char const(& CharString)[CharStringBufferSizeValue])
		requires (CharacterEncoding == ECharacterEncoding::UTF16 && IsUsingWideChar)
		{
			if (auto CharStringLength = IndexType(StringOps::Length_Bounded(CharString, CharStringBufferSizeValue,
																			StringOps::EOutOfBoundsBehavior::ReturnBufferSize)))
			{
				Construct_Internal(AeonMemory::StartObjectArrayLifetime<CharacterType[]>(CharString, CharStringLength), CharStringLength);
			}
		}
		template <IndexType CharStringBufferSizeValue>
		GString(utf32char const(& CharString)[CharStringBufferSizeValue])
		requires (CharacterEncoding == ECharacterEncoding::UTF32 && IsUsingWideChar)
		{
			if (auto CharStringLength = IndexType(StringOps::Length_Bounded(CharString, CharStringBufferSizeValue,
																			StringOps::EOutOfBoundsBehavior::ReturnBufferSize)))
			{
				Construct_Internal(AeonMemory::StartObjectArrayLifetime<CharacterType[]>(CharString, CharStringLength), CharStringLength);
			}
		}

		GString(GString const& Other)
		{
			Construct_Internal(Other.GetBufferPtr_Internal(), Other.GetBufferCharCount_Internal() - 1);
		}

		GString(GString&& Other)
		{
			if (Other.IsUsingSSO_Internal())
			{
				Storage.SSOData = Other.Storage.SSOData;
			}
			else
			{
				ReconstructStorage_Internal(EStorageMode::Dynamic);
				Storage.AllocData = Other.Storage.AllocData;
			}

			// Simulate move-like behavior
			Other.ReconstructStorage_Internal(EStorageMode::Static);
		}

		GString(ViewType const& StringView)
		{
			AEON_ASSERT(StringView.IsValid());
			if (!StringView.IsEmpty())
			{
				Construct_Internal(StringView.CharBuffer(), StringView.CharCount());
			}
		}

		~GString()
		{
			CheckDestruct_Internal();
		}

		AEON_FORCEINLINE GString& operator=(GString Other)
		{
			// Copy/Move-and-swap idiom
			using Aeon::Swap;
			Swap(*this, Other);

			return *this;
		}

		AEON_FORCEINLINE operator ViewType() const
		{
			return ViewType{ GetBufferPtr_Internal() };
		}

		GString& Reserve(IndexType NewBufferSize);
		GString& Prune();

		// TODO should these template methods be moved to an tpp file?

		template <class ... FormatDataTypes>
		static GString Format(ViewType const& FormatString, FormatDataTypes&& ... FormatData)
		{
			GString ResultString;
			if (IndexType FormatCharCount = FormatString.CharCount())
			{
				constexpr szint EstimateFormatDataLen
					= IndexType((1 // Account for the NT
								 + ...
								 + ZzInternal::TEstimateStringStorageRequirements<TRemoveConstVolatile<TRemoveReference<FormatDataTypes>>>::Value));

				CharacterType const* FormatCharPtr = FormatString.CharBuffer();

				ResultString.ReserveStorage_Internal(FormatCharCount + EstimateFormatDataLen);
				ResultString.FormatConcat_Internal(FormatCharPtr, FormatCharCount, Forward<FormatDataTypes>(FormatData) ...);
				ResultString.PruneStorage_Internal(); // CHECKME should we always Prune?
			}
			return ResultString;
		}

		template <class ... FormatDataTypes>
		GString& FormatConcat(ViewType FormatString, FormatDataTypes&& ... FormatData)
		{
			AEON_ASSERT(FormatString.IsValid());

			if (IndexType FormatCharCount = FormatString.CharCount())
			{
				// TODO ensure FormatData is not part of the buffer
			//	AEON_ASSERT((... && (!TIsSameClass<FormatDataTypes, ViewType>::Value || !CheckIsWithinBuffer_Internal(FormatData.CharBuffer()))));

				constexpr IndexType EstimateFormatDataLen
					= IndexType((0
								 + ...
								 + ZzInternal::TEstimateStringStorageRequirements<TRemoveConstVolatile<TRemoveReference<FormatDataTypes>>>::Value));

				GString FormatStringCopy;
				if (CheckIsWithinBuffer_Internal(FormatString.CharBuffer()))
				{
					FormatStringCopy = FormatString;
					FormatString = FormatStringCopy;
				}

				CharacterType const* FormatCharPtr = FormatString.CharBuffer();

				IndexType EstimateConcatStrLen = GetBufferCharCount_Internal() + FormatCharCount + EstimateFormatDataLen;

				IndexType OldBufferCount = GetBufferCount_Internal();
				IndexType NewBufferCount = EstimateConcatStrLen > OldBufferCount ? EstimateConcatStrLen << 1 : OldBufferCount;

				ReserveStorage_Internal(NewBufferCount);
				FormatConcat_Internal(FormatCharPtr, FormatCharCount, Forward<FormatDataTypes>(FormatData) ...);
			}
			return *this;
		}

		GString& Concat(ViewType ConcatString)
		{
			AEON_ASSERT(ConcatString.IsValid());

			if (IndexType ConcatCharCount = ConcatString.CharCount())
			{
				GString ConcatStringCopy;
				if (CheckIsWithinBuffer_Internal(ConcatString.CharBuffer()))
				{
					ConcatStringCopy = ConcatString;
					ConcatString = ConcatStringCopy;
				}

				CharacterType const* ConcatCharPtr = ConcatString.CharBuffer();

				IndexType ConcatStrLen = GetBufferCharCount_Internal() + ConcatCharCount;

				IndexType OldBufferCount = GetBufferCount_Internal();
				IndexType NewBufferCount = ConcatStrLen > OldBufferCount ? ConcatStrLen << 1 : OldBufferCount;

				ReserveStorage_Internal(NewBufferCount);
				Concat_Internal(ConcatCharPtr, ConcatCharCount);
			}
			return *this;
		}

		// TODO Make grapheme-indexed based versions of some methods (ReplaceAt, ToUppercase/ToLowercase, FindFirstOf),
		//  and differentiate them from buffer-indexed ones through some prefix/suffix (used for the latter overloads).
		//  Ideally, we would do this with as least code replication as possible; investigate feasibility.

		// CHECKME should Split return StringArrayType instead?

		static GString Join(StringViewArrayType const& StringArray, ViewType const& Separator,
							EStringJoinBehavior::Flag ShouldIncludeEmpty = EStringJoinBehavior::IncludeEmpty);

		StringViewArrayType Split(ViewType const& Separator,
								  EStringSplitBehavior::Flag ShouldIncludeEmpty = EStringSplitBehavior::IncludeEmpty);

		GString& AddAt(IndexType Offset, ViewType InsertString);
		GString& RemoveAt(IndexType Offset, IndexType Length);

		GString& ReplaceAt(IndexType OldSubstringOffset, IndexType OldSubstringLength, ViewType NewSubstring);
		GString& ReplaceAllOf(ViewType OldSubstring, ViewType NewSubstring);

		GString& ReplaceFirstOf(ViewType const& OldSubstring, ViewType const& NewSubstring);
		GString& ReplaceNthOf(ViewType const& OldSubstring, ViewType const& NewSubstring, IndexType Index);
		GString& ReplaceLastOf(ViewType const& OldSubstring, ViewType const& NewSubstring);

		// TODO
//		GString& FillFront(ViewType const& FillCharacters);
//		GString& FillBack(ViewType const& FillCharacters);
//
//		GString& TrimFront(ViewType const& TrimCharacters);
//		GString& TrimBack(ViewType const& TrimCharacters);

		GString& ToUppercase(IndexType Offset, IndexType Length = 1);
		GString& ToUppercase();

		GString& ToLowercase(IndexType Offset, IndexType Length = 1);
		GString& ToLowercase();

		// TODO
//		bool IsUpper(IndexType Offset, IndexType Length = 1) const;
//		bool IsUpper() const;
//
//		bool IsLower(IndexType Offset, IndexType Length = 1) const;
//		bool IsLower() const;

		// TODO better names for these methods (ToNFC, ToNFKC, ToCasefoldedNFKC ?)
		bool Normalize();
		bool NormalizeForCompatibility();
		bool NormalizeForCompatibility_Casefold();

		bool Validate();

		IndexType FindFirstOf(ViewType const& Substring, IndexType Offset = 0) const;
		IndexType FindNthOf(ViewType const& Substring, IndexType Index, IndexType Offset = 0) const;
		IndexType FindLastOf(ViewType const& Substring, IndexType Offset = 0) const;

		bool Contains(ViewType const& Substring) const;

		AEON_FORCEINLINE diffint Collate(GString const& Other) const
		{
			auto LocaleManager = SLocaleManager::Instance();
			if constexpr (CharacterEncoding == ECharacterEncoding::ASCII)
			{
				auto Locale = LocaleManager->GetInvariantLocale();
				return Locale.Collate(ViewType(*this), ViewType{ Other });
			}
			else
			{
				auto Locale = LocaleManager->GetSystemLocale();
				return Locale.Collate(ViewType(*this), ViewType{ Other });
			}
		}

		AEON_FORCEINLINE diffint Collate(GString const& Other, SLocale const& Locale) const
//		requires (CharacterEncoding != ECharacterEncoding::ASCII)
		{
			return Locale.Collate(ViewType(*this), ViewType{ Other });
		}

		AEON_FORCEINLINE ViewType Substring(IndexType Offset, IndexType Length) const
		{
			AEON_ASSERT(Offset + Length < GetBufferCharCount_Internal());
			return ViewType{ GetBufferPtr_Internal() + Offset, Length };
		}
		AEON_FORCEINLINE ViewType SubstringFromFront(IndexType Length) const
		{
			AEON_ASSERT(Length < GetBufferCharCount_Internal());
			return ViewType{ GetBufferPtr_Internal(), Length };
		}
		AEON_FORCEINLINE ViewType SubstringToBack(IndexType Offset) const
		{
			IndexType BufferCharCount = GetBufferCharCount_Internal();
			AEON_ASSERT(Offset < BufferCharCount);
			IndexType Length = BufferCharCount - Offset - 1;
			return ViewType{ GetBufferPtr_Internal() + Offset, Length };
		}

		/// Returns the character (Unicode: code-unit) at the specified index, relative to the number of characters in the string.
		/// @param Index The index (relative to the number of characters) of the character to retrieve.
		/// @return The character at the index, or a null character if out-of-bounds.
		AEON_FORCEINLINE CharacterType CharAt(IndexType Index) const
		{
			return Index < (GetBufferCharCount_Internal() - 1) ? GetBufferPtr_Internal()[Index] : NullTerminatorChar;
		}

		/// Counts the number of characters (Unicode: code-units) in the string.
		/// @return The number of characters in the string.
		AEON_FORCEINLINE IndexType LengthChars() const
		{
			return GetBufferCharCount_Internal() - 1;
		}

		/// Returns the grapheme (Unicode: code-point) at the specified index, relative to the number of graphemes in the string.
		/// This operation is mostly relevant for text edition purposes.
		/// Note: This is a linear-time operation for certain encodings.
		/// @param Index The index of the grapheme to retrieve (relative to the number of graphemes).
		/// @return A view to the portion of the string that contains the grapheme, or an empty view if out-of-bounds.
		AEON_FORCEINLINE ViewType GraphemeAt(IndexType Index) const
		{
			return GetGraphemeAt_Internal(Index);
		}

		/// Counts the number of graphemes (Unicode: code-points) in the string.
		/// This operation is mostly relevant for text edition purposes.
		/// Note: This is a linear-time operation for certain encodings.
		/// @return The number of graphemes in the string.
		AEON_FORCEINLINE IndexType LengthGraphemes() const
		{
			return GetGraphemeLength_Internal();
		}

		/// Returns the grapheme cluster (Unicode: user-perceived character) at the specified index,
		/// relative to the number of grapheme clusters in the string.
		/// This operation is mostly relevant for text representation purposes.
		/// Note: This is a linear-time operation for certain encodings.
		/// @param Index The index (relative to the number of graphemes) of the grapheme cluster to retrieve.
		/// @return A view to the portion of the string that contains the grapheme cluster, or an empty view if out-of-bounds.
		AEON_FORCEINLINE ViewType GraphemeClusterAt(IndexType Index) const
		{
			return GetGraphemeClusterAt_Internal(Index);
		}

		/// Counts the number of grapheme clusters (Unicode: user-perceived characters) in the string.
		/// This operation is mostly relevant for text representation purposes.
		/// Note: This is a linear-time operation for certain encodings.
		/// @return The number of grapheme clusters in the string.
		AEON_FORCEINLINE IndexType LengthGraphemeClusters() const
		{
			return GetGraphemeClusterLength_Internal();
		}

		AEON_FORCEINLINE bool IsEmpty() const
		{
			return GetBufferCharCount_Internal() == 1;
		}

		// Returns a pointer to the underlying buffer used as storage by the string.
		AEON_FORCEINLINE CharacterType const* CharBuffer() const
		{
			return GetBufferPtr_Internal();
		}

		// Returns the number of code units stored in the string. This includes the null terminator at the end of the buffer.
		AEON_FORCEINLINE IndexType CharCount() const
		{
			return GetBufferCharCount_Internal();
		}

		// Returns the number of code units supported by the underlying buffer used as storage by the string.
		AEON_FORCEINLINE IndexType AllocatedCharCount() const
		{
			return GetBufferCount_Internal();
		}

		// TODO conversion functions between encodings
		//  see std::codecvt and std::locale

		AEON_FORCEINLINE bool operator==(GString const& Other) const
		{
			return GetBufferCharCount_Internal() == Other.GetBufferCharCount_Internal()
				&& StringOps::Compare(GetBufferPtr_Internal(), Other.GetBufferPtr_Internal()) == 0;
		}
		AEON_FORCEINLINE strong_ordering operator<=>(GString const& Other) const
		{
		//	return Collate(Other) < 0;
			return StringOps::Compare(GetBufferPtr_Internal(), Other.GetBufferPtr_Internal()) <=> 0;
		}

		friend void Swap(GString& String1, GString& String2)
		{
			using Aeon::Swap;
			Swap(String1.Storage, String2.Storage);
		}

	private:
		// MSVC sucks sometimes... god forbid it would just align the field to the next byte and pack all bitfields in 4 bytes
#define ZZINTERNAL_AEON_STRING_STORAGE_LAYOUT_WORKAROUND 0 // (AEON_PLATFORM_WINDOWS /*&& (AEON_COMPILER_MSVC || AEON_COMPILER_CLANG)*/)

		enum struct ECharacterCaseConversion
		{
			Upper,
			Lower,
		};

		enum struct EStorageMode
		{
			// Alloc
			Dynamic = 0b01,
			// SSO
			Static = 0b10,
		};

		AEON_FORCEINLINE bool CheckIsWithinBuffer_Internal(CharacterType const* CharPtr) const
		{
			CharacterType* BufferCharPtr = GetBufferPtr_Internal();
			CharacterType* BufferLastCharPtr = BufferCharPtr + GetBufferCharCount_Internal() - 1;
			return BufferCharPtr <= CharPtr && CharPtr <= BufferLastCharPtr;
		}

		AEON_FORCEINLINE bool IsUsingSSO_Internal() const
		{
			// NOTE This is well-defined according to the standard, see N4849 (C++20), 11.4 §22-25 [class.mem]
			//  Accessing a subobject of a non-active member of a union is well-defined
			//  if that subobject is part of the common initial sequence of the member and the currently active member
			return Storage.AllocData.IsUsingSSO;
		}

		AEON_FORCEINLINE IndexType GetBufferCharCount_Internal() const
		{
			if (IsUsingSSO_Internal())
			{
				return Storage.SSOData.BufferCharCount;
			}
			else
			{
				return Storage.AllocData.GetBufferCharCount();
			}
		}

		AEON_FORCEINLINE void SetBufferCharCount_Internal(IndexType NewCharCount)
		{
			if (IsUsingSSO_Internal())
			{
				Storage.SSOData.BufferCharCount = NewCharCount;
			}
			else
			{
				Storage.AllocData.SetBufferCharCount(NewCharCount);
			}
		}

		AEON_FORCEINLINE IndexType GetBufferCount_Internal() const
		{
			return IsUsingSSO_Internal() ? DSSOStorage::BufferCount : Storage.AllocData.BufferCount;
		}

		AEON_FORCEINLINE CharacterType* GetBufferPtr_Internal() const
		{
			return IsUsingSSO_Internal() ? const_cast<CharacterType*>(Storage.SSOData.BufferArr) : Storage.AllocData.BufferPtr;
		}

		void ReconstructStorage_Internal(EStorageMode Mode);

		void ReserveStorage_Internal(IndexType NewElemCount);

		void PruneStorage_Internal();

		void Construct_Internal(CharacterType const* CharStrPtr, IndexType CharStrLen);

		void CheckDestruct_Internal();

		template <class ... FormatDataTypes>
		void FormatConcat_Internal(CharacterType const* FormatCharStrPtr, IndexType FormatCharStrLen, FormatDataTypes&& ... FormatData)
		{
			using FormatContextType = TSelectIf<IsUsingNarrowChar,
												ZZINTERNAL_AEON_FMT_NS::format_context,
#if AEON_USING_FMT_LIBRARY
												ZZINTERNAL_AEON_FMT_NS::buffer_context<wchar>
#else
												ZZINTERNAL_AEON_FMT_NS::wformat_context
#endif
												>;

			// This is intended to be used by std::back_insert_iterator only
			struct SBackInsertWrapper
			{
				using value_type = CharacterType;

				explicit SBackInsertWrapper(GString* NewStringPtr)
					: StringPtr(NewStringPtr)
					, CachedCharCount{ StringPtr->GetBufferCharCount_Internal() - 1 }
					, CachedBufCount{ StringPtr->GetBufferCount_Internal() }
				{
				}
				~SBackInsertWrapper()
				{
					push_back(NullTerminatorChar);
					StringPtr->SetBufferCharCount_Internal(CachedCharCount);
				}

				// NOTE std::back_insert_iterator expects a function with this name and signature
				void push_back(value_type NewChar)
				{
					IndexType Index = CachedCharCount++;
					if (CachedCharCount > CachedBufCount)
					{
						CachedBufCount <<= 1;
						StringPtr->ReserveStorage_Internal(CachedBufCount);
					}

					StringPtr->GetBufferPtr_Internal()[Index] = NewChar;
				}

				GString* StringPtr;
				IndexType CachedCharCount;
				IndexType CachedBufCount;
			}
			StringWrapper{ this };

			auto FormatIter = std::back_insert_iterator<SBackInsertWrapper>{ StringWrapper };
			auto FormatStrView = ZZINTERNAL_AEON_FMT_NS::basic_string_view<CharacterType>{ FormatCharStrPtr, FormatCharStrLen };
			auto FormatArgs = ZZINTERNAL_AEON_FMT_NS::make_format_args<FormatContextType>(Forward<FormatDataTypes>(FormatData) ...);

			try
			{
				ZZINTERNAL_AEON_FMT_NS::vformat_to(FormatIter, FormatStrView, FormatArgs);
			}
			// FIXME do we want to throw formatting exceptions?
			catch (...)
			{
				AEON_DEBUGGER_BREAK();
			}
		}

		void Concat_Internal(CharacterType const* ConcatCharStrPtr, IndexType ConcatCharStrLen)
		{
			// Account for the NT we are overwriting
			IndexType OldCharCount = GetBufferCharCount_Internal() - 1;
			IndexType NewCharCount = ConcatCharStrLen + OldCharCount + 1;

			// NOTE currently every caller already reserves storage (since they have more information on what follows)
		//	ReserveStorage_Internal(NewCharCount);

			// Obtain the (potentially new) buffer and offset it by the old string
			CharacterType* OffsetBufferPtr = GetBufferPtr_Internal() + OldCharCount;

			AeonMemory::MemCopy(OffsetBufferPtr, ConcatCharStrPtr, Aeon::SizeOf(CharacterType, ConcatCharStrLen));
			OffsetBufferPtr[ConcatCharStrLen] = NullTerminatorChar;

			SetBufferCharCount_Internal(NewCharCount);
		}

		void ToCharacterCase_Internal(ECharacterCaseConversion CaseConversion, IndexType Offset, IndexType Length);

		bool Normalize_Internal(uint64 Flags);

		bool Validate_Internal();

		ViewType GetGraphemeAt_Internal(IndexType Index) const;

		IndexType GetGraphemeLength_Internal() const;

		ViewType GetGraphemeClusterAt_Internal(IndexType Index) const;

		IndexType GetGraphemeClusterLength_Internal() const;

		static diffint Decompose_Internal(CharacterType* OldBufferPtr, int32* NewBufferPtr, diffint NewBufferSize, uint64 Flags);

		static diffint Compose_Internal(int32* OldBufferPtr, CharacterType* NewBufferPtr, diffint OldBufferSize, uint64 Flags);

		static bool DecodeCodePoint_Internal(CharacterType* BufferPtr, int32& CodePoint, diffint& CharsToAdvance);

		static bool EncodeCodePoint_Internal(CharacterType* BufferPtr, int32 CodePoint, diffint& CharsToAdvance);

		struct DDynAllocStorage
		{
			DDynAllocStorage()
				: IsUsingSSO(0)
#			if ZZINTERNAL_AEON_STRING_STORAGE_LAYOUT_WORKAROUND
				, BufferCharCount_LoWord16_LoByte7(0)
				, BufferCharCount_LoWord16_HiByte8(0)
				, BufferCharCount_HiWord16(0)
#			if 0
				, BufferCharCount_LoWord32_LoWord16_LoByte7(0)
				, BufferCharCount_LoWord32_LoWord16_HiByte8(0)
				, BufferCharCount_LoWord32_HiWord16(0)
				, BufferCharCount_HiWord32(0)
#			endif
#			else
				, BufferCharCount(0)
#			endif
				, BufferCount(0)
				, BufferPtr(nullptr)
			{
			}

			AEON_MAKE_COPYABLE(DDynAllocStorage);
			AEON_MAKE_MOVEABLE(DDynAllocStorage);

			AEON_FORCEINLINE void Allocate(IndexType InitialBufferCount, algnint BufferAlignment = Aeon::AlignOf(CharacterType))
			{
				szint BufferSize = Aeon::SizeOf(CharacterType, InitialBufferCount);

				// NOTE Alignment is currently only used by Normalize;
				//  This is somewhat overkill, since on most platforms, PlatformDefaultAllocAlignment >= alignof(int32),
				//  which ensures that Normalize cannot perform misaligned access(es) to int32 objects, but meh.
				BufferPtr = AeonMemory::MemAlloc_Ex<CharacterType[]>(BufferSize, BufferAlignment,
																	 AeonMemory::EMemAllocExOptions::RelaxAlignConstraint);
				BufferCount = InitialBufferCount;
			}
			AEON_FORCEINLINE void Reallocate(IndexType NewBufferCount)
			{
				szint BufferSize = Aeon::SizeOf(CharacterType, NewBufferCount);

				BufferPtr = AeonMemory::MemRealloc<CharacterType[]>(BufferPtr, BufferSize);
				BufferCount = NewBufferCount;
			}
			AEON_FORCEINLINE void Deallocate()
			{
				if (BufferPtr)
				{
					AeonMemory::MemDealloc(BufferPtr);

					BufferPtr = nullptr;
					BufferCount = 0;
				}
			}

			AEON_FORCEINLINE IndexType GetBufferCharCount() const
			{
#			if ZZINTERNAL_AEON_STRING_STORAGE_LAYOUT_WORKAROUND
#				if not AEON_IS_LITTLE_ENDIAN_ARCH
#					error Not implemented
#				endif
				IndexType CharCountMask;
				AeonMemory::MemCopy(Aeon::AddressOf(CharCountMask), this, Aeon::SizeOf(IndexType));
				return CharCountMask >> 1; // Remove the bit for IsUsingSSO
#			else
				return BufferCharCount;
#			endif
			}

			AEON_FORCEINLINE void SetBufferCharCount(IndexType NewCharCount)
			{
#			if ZZINTERNAL_AEON_STRING_STORAGE_LAYOUT_WORKAROUND
#				if not AEON_IS_LITTLE_ENDIAN_ARCH
#					error Not implemented
#				endif
				IndexType CharCountMask = NewCharCount << 1; // Add 0 as the bit for IsUsingSSO
				AeonMemory::MemCopy(this, Aeon::AddressOf(CharCountMask), Aeon::SizeOf(IndexType));
#			else
				BufferCharCount = NewCharCount;
#			endif
			}

			uint32 IsUsingSSO : 1;
#		if ZZINTERNAL_AEON_STRING_STORAGE_LAYOUT_WORKAROUND
			uint8 BufferCharCount_LoWord16_LoByte7 : 7;
			[[maybe_unused]]
			uint8 BufferCharCount_LoWord16_HiByte8;
			[[maybe_unused]]
			uint16 BufferCharCount_HiWord16;
#		if 0
			uint8 BufferCharCount_LoWord32_LoWord16_LoByte7 : 7;
			[[maybe_unused]]
			uint8 BufferCharCount_LoWord32_LoWord16_HiByte8;
			[[maybe_unused]]
			uint16 BufferCharCount_LoWord32_HiWord16;
			[[maybe_unused]]
			uint32 BufferCharCount_HiWord32;
#		endif
			static_assert(1 /*sizeof BufferCharCount_LoWord16_LoByte7*/
							+ sizeof BufferCharCount_LoWord16_HiByte8
							+ sizeof BufferCharCount_HiWord16 == Aeon::SizeOf(IndexType));
#		else
			uint32 BufferCharCount : Aeon::BitSizeOf(uint32) - 1;
#		endif
			uint32 BufferCount;
			// TODO adaptation to support Allocators
//			GCompressedDuple<AllocatorType, CharacterType*> BufferPtr;
			CharacterType* BufferPtr;
		};

		struct DSSOStorage
		{
			DSSOStorage()
				: IsUsingSSO(1)
				, BufferCharCount(1)
				, BufferArr{ NullTerminatorChar }
			{
			}

			AEON_MAKE_COPYABLE(DSSOStorage);
			AEON_MAKE_MOVEABLE(DSSOStorage);

			// NOTE Alignment and order of bitfields is implementation-defined,
			//  it just so happens that MSVC stringently aligns (and pads) bitfields of underlying type uint32 to 4-byte boundaries;
			//  note that this is done at the ABI level, so we need to check for the platform, and not the compiler
			static constexpr uint32 SSOIndexAlignment_Internal = uint32(
				#if AEON_PLATFORM_WINDOWS
					Aeon::SizeOf(uint32)
				#else
					Aeon::SizeOf(uint8)
				#endif
				);
			static constexpr uint32 BufferCount = uint32(
				(Aeon::SizeOf(DDynAllocStorage) - SSOIndexAlignment_Internal) / Aeon::SizeOf(CharacterType)
				);

			[[maybe_unused]]
			uint32 IsUsingSSO : 1;
			uint32 BufferCharCount : Aeon::BitSizeOf(uint8) - 1;
			CharacterType BufferArr[BufferCount];
		};

		// Ensure that storage data structures have equal size, if this breaks, check SSOIndexAlignment_Internal
		static_assert(Aeon::SizeOf(DDynAllocStorage) == Aeon::SizeOf(DSSOStorage));

		// Ensure we can read the common initial sequence of union elements
		static_assert(VIsStandardLayoutObject<DDynAllocStorage> && VIsStandardLayoutObject<DSSOStorage>);
		// Ensure we don't need to worry about explicitly destroying the union elements (simplifies some code)
		static_assert(VIsTriviallyDestructible<DDynAllocStorage> && VIsTriviallyDestructible<DSSOStorage>);

		// Main storage defined as a union so that we can use Small String Optimization (SSO);
		// By default starts as a static buffer, and the buffer is within the string object.
		union DDataStorage
		{
			// Defaults to static storage
			DDataStorage()
				: SSOData()
			{
			}

			DDynAllocStorage AllocData;
			DSSOStorage SSOData;
		}
		Storage;

	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	SString, STextString, SPlatformString

	// String type encoding characters in ASCII, ideal for composing strings with simple English text and/or internal data.
	using SString = Aeon::GString<char, Aeon::ECharacterSet::ASCII>;

	// String type encoding characters in UTF-8, ideal for composing strings with user-facing, international text.
	using STextString = Aeon::GString<char, Aeon::ECharacterSet::Unicode>; // TODO make a better name for this (and for StringView); SUserString? SUnicodeString?

	// String type encoding characters in a platform-specific Unicode encoding, ideal for interacting with certain platform-specific APIs.
#if AEON_PLATFORM_WINDOWS
	using SPlatformString = Aeon::GString<wchar, Aeon::ECharacterSet::Unicode>;
#elif AEON_PLATFORM_POSIX
	using SPlatformString = Aeon::GString<char, Aeon::ECharacterSet::Unicode>;
#else
#	error Not implemented yet!
#endif

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Literal operators

	// TODO clean this up (use string aliases); should we keep these literals?

	inline namespace Literals
	{
		AEON_FORCEINLINE inline auto operator""_str(char const* Literal, szint Length)
			-> GString<char, Aeon::ECharacterSet::ASCII>
		{
			using StringType = GString<char, Aeon::ECharacterSet::ASCII>;
			return StringType{ Literal, StringType::IndexType(Length) };
		}

		AEON_FORCEINLINE inline auto operator""_tstr(char const* Literal, szint Length)
			-> GString<char, Aeon::ECharacterSet::Unicode>
		{
			using StringType = GString<char, Aeon::ECharacterSet::Unicode>;
			return StringType{ Literal, StringType::IndexType(Length) };
		}
		AEON_FORCEINLINE inline auto operator""_tstr(utf8char const* Literal, szint Length)
			-> GString<char, Aeon::ECharacterSet::Unicode>
		{
			using StringType = GString<char, Aeon::ECharacterSet::Unicode>;
			return StringType{ Literal, StringType::IndexType(Length) };
		}

		AEON_FORCEINLINE inline auto operator""_pstr(wchar const* Literal, szint Length)
			-> GString<wchar, Aeon::ECharacterSet::Unicode>
		{
			using StringType = GString<wchar, Aeon::ECharacterSet::Unicode>;
			return StringType{ Literal, StringType::IndexType(Length) };
		}
#	if AEON_PLATFORM_WINDOWS
		AEON_FORCEINLINE inline auto operator""_pstr(utf16char const* Literal, szint Length)
			-> GString<wchar, Aeon::ECharacterSet::Unicode>
		{
			using StringType = GString<wchar, Aeon::ECharacterSet::Unicode>;
			return StringType{ Literal, StringType::IndexType(Length) };
		}
#	elif AEON_PLATFORM_LINUX
		AEON_FORCEINLINE inline auto operator""_pstr(utf32char const* Literal, szint Length)
			-> GString<wchar, Aeon::ECharacterSet::Unicode>
		{
			using StringType = GString<wchar, Aeon::ECharacterSet::Unicode>;
			return StringType{ Literal, StringType::IndexType(Length) };
		}
#	else
		// TODO other platforms
		AEON_COMPILER_WARNING("TODO implement string user literals for this platform");
#	endif
	}
}

namespace ZZINTERNAL_AEON_FMT_NS
{
	template <Aeon::CCharacter CharType, Aeon::ECharacterSet CharSetValue>
	struct formatter<Aeon::GStringView<CharType, CharSetValue>, CharType> : formatter<ZZINTERNAL_AEON_FMT_NS::basic_string_view<CharType>, CharType>
	{
		template <class ContextType>
		auto format(Aeon::GStringView<CharType, CharSetValue> const& View, ContextType& Context) const
		{
			return formatter<ZZINTERNAL_AEON_FMT_NS::basic_string_view<CharType>,
			    			 CharType>::format(ZZINTERNAL_AEON_FMT_NS::basic_string_view(View.CharBuffer(), View.CharCount()),
											   Context);
		}
	};

	template <Aeon::CCharacter CharType, Aeon::ECharacterSet CharSetValue>
	struct formatter<Aeon::GString<CharType, CharSetValue>, CharType> : formatter<ZZINTERNAL_AEON_FMT_NS::basic_string_view<CharType>, CharType>
	{
		template <class ContextType>
		auto format(Aeon::GString<CharType, CharSetValue> const& String, ContextType& Context) const
		{
			return formatter<ZZINTERNAL_AEON_FMT_NS::basic_string_view<CharType>,
			    			 CharType>::format(ZZINTERNAL_AEON_FMT_NS::basic_string_view(String.CharBuffer(), String.LengthChars()),
											   Context);
		}
	};

	// FIXME make formatter specializations for combinations of char and wchar (always treat the format string as Unicode)
}

#endif
