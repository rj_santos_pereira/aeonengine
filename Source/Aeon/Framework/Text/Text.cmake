add_subdirectory(Localization)

target_sources(${PROJECT_NAME}
	PUBLIC
		${CMAKE_CURRENT_SOURCE_DIR}/StringCommon.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/StringView.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/String.hpp
	PRIVATE
		${CMAKE_CURRENT_SOURCE_DIR}/String.cpp
	)