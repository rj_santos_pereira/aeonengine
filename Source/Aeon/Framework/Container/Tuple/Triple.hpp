#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_TUPLE_TRIPLE
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_TUPLE_TRIPLE

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Container/Tuple/TupleCommon.hpp"

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GTriple

	template <CObject FirstType, CObject SecondType, CObject ThirdType>
	class GTriple
	{
	public:
		template <szint IndexValue>
		using ObjectTypeAt = typename TTypePack<FirstType, SecondType, ThirdType>::template ElementAt<IndexValue>;

		template <class NewFirstType, class NewSecondType, class NewThirdType>
		constexpr GTriple(NewFirstType&& NewFirstObj, NewSecondType&& NewSecondObj, NewThirdType&& NewThirdObj)
		requires CConvertibleClass<NewFirstType, FirstType>
				 && CConvertibleClass<NewSecondType, SecondType>
				 && CConvertibleClass<NewThirdType, ThirdType>
			: FirstObj{ Forward<NewFirstType>(NewFirstObj) }
			, SecondObj{ Forward<NewSecondType>(NewSecondObj) }
			, ThirdObj{ Forward<NewThirdType>(NewThirdObj) }
		{
		}

		ZZINTERNAL_AEON_DECLARE_TRIPLE_CONSTRUCTORS(GTriple, FirstType, SecondType, ThirdType);

		template <szint IndexValue>
		AEON_FORCEINLINE constexpr ObjectTypeAt<IndexValue> const& ObjectAt() const
		{
			static_assert(IndexValue <= 2,
						  "'GTriple<FirstType, SecondType, ThirdType>::ObjectAt<IndexValue>()': "
						  "IndexValue must be 0, 1, or 2.");
			if constexpr (IndexValue == 0)
			{
				return FirstObj;
			}
			else if constexpr (IndexValue == 1)
			{
				return SecondObj;
			}
			else
			{
				return ThirdObj;
			}
		}
		template <szint IndexValue>
		AEON_FORCEINLINE constexpr ObjectTypeAt<IndexValue>& ObjectAt()
		{
			return AEON_INVOKE_CONST_OVERLOAD(template ObjectAt<IndexValue>);
		}
		
		AEON_FORCEINLINE constexpr FirstType const& First() const { return FirstObj; }
		AEON_FORCEINLINE constexpr FirstType& First() { return FirstObj; }

		AEON_FORCEINLINE constexpr SecondType const& Second() const { return SecondObj; }
		AEON_FORCEINLINE constexpr SecondType& Second() { return SecondObj; }

		AEON_FORCEINLINE constexpr ThirdType const& Third() const { return ThirdObj; }
		AEON_FORCEINLINE constexpr ThirdType& Third() { return ThirdObj; }

		// TODO implement Swap?

		// Implementation of get (found via ADL) to enable structured bindings with Aeon::GTriple
		template <std::size_t IndexValue>
		friend ObjectTypeAt<IndexValue> const& get(GTriple const& Triple)
		{
			return Triple.template ObjectAt<IndexValue>();
		}
		template <std::size_t IndexValue>
		friend ObjectTypeAt<IndexValue>& get(GTriple& Triple)
		{
			return Triple.template ObjectAt<IndexValue>();
		}
		template <std::size_t IndexValue>
		friend ObjectTypeAt<IndexValue> get(GTriple&& Triple)
		{
			return Triple.template ObjectAt<IndexValue>();
		}
		
	private:
		FirstType FirstObj;
		SecondType SecondObj;
		ThirdType ThirdObj;
	};

	template <class FirstType, class SecondType, class ThirdType>
	GTriple(FirstType&&, SecondType&&, ThirdType&&) -> GTriple<TDecayType<FirstType>,
	    													   TDecayType<SecondType>,
	    													   TDecayType<ThirdType>>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GCompressedTriple

	template <CObject FirstType, CObject SecondType, CObject ThirdType>
	class ZzInternal_GCompressedTriple
		: private ZzInternal_GCompressedDuple<ZzInternal_GCompressedDuple<FirstType, SecondType>, ThirdType>
	{
		using BaseType = ZzInternal_GCompressedDuple<ZzInternal_GCompressedDuple<FirstType, SecondType>, ThirdType>;
		using SubBaseType = ZzInternal_GCompressedDuple<FirstType, SecondType>;

	public:
		template <class NewFirstType, class NewSecondType, class NewThirdType>
		explicit constexpr ZzInternal_GCompressedTriple(NewFirstType&& NewFirstObj, NewSecondType&& NewSecondObj, NewThirdType&& NewThirdObj)
			: BaseType{ SubBaseType{ Forward<NewFirstType>(NewFirstObj), Forward<NewSecondType>(NewSecondObj) }, Forward<NewThirdType>(NewThirdObj) }
		{
		}

		ZZINTERNAL_AEON_DECLARE_TRIPLE_CONSTRUCTORS(ZzInternal_GCompressedTriple, FirstType, SecondType, ThirdType);

		AEON_FORCEINLINE constexpr FirstType const& First() const { return BaseType::First().SubBaseType::First(); }
		AEON_FORCEINLINE constexpr FirstType& First() { return BaseType::First().SubBaseType::First(); }

		AEON_FORCEINLINE constexpr SecondType const& Second() const { return BaseType::First().SubBaseType::Second(); }
		AEON_FORCEINLINE constexpr SecondType& Second() { return BaseType::First().SubBaseType::Second(); }

		AEON_FORCEINLINE constexpr ThirdType const& Third() const { return BaseType::Second(); }
		AEON_FORCEINLINE constexpr ThirdType& Third() { return BaseType::Second(); }

	};

#if 0
	// Base
	template <CObject FirstType, CObject SecondType, CObject ThirdType>
	class ZzInternal_GCompressedTriple
	{
	public:
		template <class NewFirstType, class NewSecondType, class NewThirdType>
		explicit constexpr ZzInternal_GCompressedTriple(NewFirstType&& NewFirstObj, NewSecondType&& NewSecondObj, NewThirdType&& NewThirdObj)
			: FirstObj{ Forward<NewFirstType>(NewFirstObj) }
			, SecondObj{ Forward<NewSecondType>(NewSecondObj) }
			, ThirdObj{ Forward<NewThirdType>(NewThirdObj) }
		{
		}

		ZZINTERNAL_AEON_DECLARE_TRIPLE_CONSTRUCTORS(ZzInternal_GCompressedTriple, FirstType, SecondType, ThirdType);

		AEON_FORCEINLINE constexpr FirstType const& First() const { return FirstObj; }
		AEON_FORCEINLINE constexpr FirstType& First() { return FirstObj; }

		AEON_FORCEINLINE constexpr SecondType const& Second() const { return SecondObj; }
		AEON_FORCEINLINE constexpr SecondType& Second() { return SecondObj; }

		AEON_FORCEINLINE constexpr ThirdType const& Third() const { return ThirdObj; }
		AEON_FORCEINLINE constexpr ThirdType& Third() { return ThirdObj; }

	private:
		FirstType FirstObj;
		SecondType SecondObj;
		ThirdType ThirdObj;

	};

	template <CObject FirstType, CObject SecondType, CObject ThirdType>
	requires CCompressibleClass<FirstType> && CCompressibleClass<SecondType> && CCompressibleClass<ThirdType>
	class ZzInternal_GCompressedTriple<FirstType, SecondType, ThirdType>
	    : protected ZzInternal::GCompressibleTupleElement<FirstType, 0>,
		  protected ZzInternal::GCompressibleTupleElement<SecondType, 1>,
		  protected ZzInternal::GCompressibleTupleElement<ThirdType, 2>
	{
		using BaseFirstType = ZzInternal::GCompressibleTupleElement<FirstType, 0>;
		using BaseSecondType = ZzInternal::GCompressibleTupleElement<SecondType, 1>;
		using BaseThirdType = ZzInternal::GCompressibleTupleElement<ThirdType, 2>;

	public:
		template <class NewFirstType, class NewSecondType, class NewThirdType>
		explicit constexpr ZzInternal_GCompressedTriple(NewFirstType&& NewFirstObj, NewSecondType&& NewSecondObj, NewThirdType&& NewThirdObj)
			: BaseFirstType{ Forward<NewFirstType>(NewFirstObj) }
			, BaseSecondType{ Forward<NewSecondType>(NewSecondObj) }
			, BaseThirdType{ Forward<NewThirdType>(NewThirdObj) }
		{
		}

		ZZINTERNAL_AEON_DECLARE_TRIPLE_CONSTRUCTORS(ZzInternal_GCompressedTriple, FirstType, SecondType, ThirdType);

		AEON_FORCEINLINE constexpr FirstType const& First() const { return *static_cast<BaseFirstType const*>(this); }
		AEON_FORCEINLINE constexpr FirstType& First() { return *static_cast<BaseFirstType*>(this); }

		AEON_FORCEINLINE constexpr SecondType const& Second() const { return *static_cast<BaseSecondType const*>(this); }
		AEON_FORCEINLINE constexpr SecondType& Second() { return *static_cast<BaseSecondType*>(this); }

		AEON_FORCEINLINE constexpr ThirdType const& Third() const { return *static_cast<BaseThirdType const*>(this); }
		AEON_FORCEINLINE constexpr ThirdType& Third() { return *static_cast<BaseThirdType*>(this); }

	};

	template <CObject FirstType, CObject SecondType, CObject ThirdType>
	requires CCompressibleClass<SecondType> && CCompressibleClass<ThirdType>
	class ZzInternal_GCompressedTriple<FirstType, SecondType, ThirdType>
	    : protected ZzInternal::GNonCompressibleTupleElement<FirstType>,
	      protected ZzInternal::GCompressibleTupleElement<SecondType, 0>,
		  protected ZzInternal::GCompressibleTupleElement<ThirdType, 1>
	{
		using BaseFirstType = ZzInternal::GNonCompressibleTupleElement<FirstType>;
		using BaseSecondType = ZzInternal::GCompressibleTupleElement<SecondType, 0>;
		using BaseThirdType = ZzInternal::GCompressibleTupleElement<ThirdType, 1>;

	public:
		template <class NewFirstType, class NewSecondType, class NewThirdType>
		explicit constexpr ZzInternal_GCompressedTriple(NewFirstType&& NewFirstObj, NewSecondType&& NewSecondObj, NewThirdType&& NewThirdObj)
			: BaseFirstType{ Forward<NewFirstType>(NewFirstObj) }
			, BaseSecondType{ Forward<NewSecondType>(NewSecondObj) }
			, BaseThirdType{ Forward<NewThirdType>(NewThirdObj) }
		{
		}

		ZZINTERNAL_AEON_DECLARE_TRIPLE_CONSTRUCTORS(ZzInternal_GCompressedTriple, FirstType, SecondType, ThirdType);

		AEON_FORCEINLINE constexpr FirstType const& First() const { return BaseFirstType::Object; }
		AEON_FORCEINLINE constexpr FirstType& First() { return BaseFirstType::Object; }

		AEON_FORCEINLINE constexpr SecondType const& Second() const { return static_cast<BaseSecondType const*>(this); }
		AEON_FORCEINLINE constexpr SecondType& Second() { return *static_cast<BaseSecondType*>(this); }

		AEON_FORCEINLINE constexpr ThirdType const& Third() const { return *static_cast<BaseThirdType const*>(this); }
		AEON_FORCEINLINE constexpr ThirdType& Third() { return *static_cast<BaseThirdType*>(this); }

	};

	template <CObject FirstType, CObject SecondType, CObject ThirdType>
	requires CCompressibleClass<ThirdType>
	class ZzInternal_GCompressedTriple<FirstType, SecondType, ThirdType>
	    : protected ZzInternal::GNonCompressibleTupleElement<FirstType, 0>,
	      protected ZzInternal::GNonCompressibleTupleElement<SecondType, 1>,
	      protected ZzInternal::GCompressibleTupleElement<ThirdType>
	{
		using BaseFirstType = ZzInternal::GNonCompressibleTupleElement<FirstType, 0>;
		using BaseSecondType = ZzInternal::GNonCompressibleTupleElement<SecondType, 1>;
		using BaseThirdType = ZzInternal::GCompressibleTupleElement<ThirdType>;

	public:
		template <class NewFirstType, class NewSecondType, class NewThirdType>
		explicit constexpr ZzInternal_GCompressedTriple(NewFirstType&& NewFirstObj, NewSecondType&& NewSecondObj, NewThirdType&& NewThirdObj)
			: BaseFirstType{ Forward<NewFirstType>(NewFirstObj) }
			, BaseSecondType{ Forward<NewSecondType>(NewSecondObj) }
			, BaseThirdType{ Forward<NewThirdType>(NewThirdObj) }
		{
		}

		ZZINTERNAL_AEON_DECLARE_TRIPLE_CONSTRUCTORS(ZzInternal_GCompressedTriple, FirstType, SecondType, ThirdType);

		AEON_FORCEINLINE constexpr FirstType const& First() const { return BaseFirstType::Object; }
		AEON_FORCEINLINE constexpr FirstType& First() { return BaseFirstType::Object; }

		AEON_FORCEINLINE constexpr SecondType const& Second() const { return BaseSecondType::Object; }
		AEON_FORCEINLINE constexpr SecondType& Second() { return BaseSecondType::Object; }

		AEON_FORCEINLINE constexpr ThirdType const& Third() const { return *static_cast<BaseThirdType const*>(this); }
		AEON_FORCEINLINE constexpr ThirdType& Third() { return *static_cast<BaseThirdType*>(this); }

	};
#endif

	template <CObject FirstType, CObject SecondType, CObject ThirdType>
	class GCompressedTriple : private ZzInternal_GCompressedTriple<FirstType, SecondType, ThirdType>
	{
		using Base = ZzInternal_GCompressedTriple<FirstType, SecondType, ThirdType>;

	public:
		using FirstObjectType = FirstType;
		using SecondObjectType = SecondType;
		using ThirdObjectType = ThirdType;

		template <class NewFirstType, class NewSecondType, class NewThirdType>
		explicit constexpr GCompressedTriple(NewFirstType&& NewFirstObj, NewSecondType&& NewSecondObj, NewThirdType&& NewThirdObj)
		requires CConvertibleClass<NewFirstType, FirstType>
				 && CConvertibleClass<NewSecondType, SecondType>
				 && CConvertibleClass<NewThirdType, ThirdType>
			: Base{ Forward<NewFirstType>(NewFirstObj), Forward<NewSecondType>(NewSecondObj), Forward<NewThirdType>(NewThirdObj) }
		{
		}

		ZZINTERNAL_AEON_DECLARE_TRIPLE_CONSTRUCTORS(GCompressedTriple, FirstType, SecondType, ThirdType);

		using Base::First;
		using Base::Second;
		using Base::Third;

	};

	template <class FirstType, class SecondType, class ThirdType>
	GCompressedTriple(FirstType&&, SecondType&&, ThirdType&&) -> GCompressedTriple<TDecayType<FirstType>,
																				   TDecayType<SecondType>,
																				   TDecayType<ThirdType>>;
}

namespace std
{
// Specialization of std::tuple_element to enable structured bindings with Aeon::GTriple
	template <std::size_t IndexValue, class FirstType, class SecondType, class ThirdType>
	struct tuple_element<IndexValue, Aeon::GTriple<FirstType, SecondType, ThirdType>>
	{
		using type = typename Aeon::TTypePack<FirstType, SecondType, ThirdType>::template ElementAt<IndexValue>;
	};

// Specialization of std::tuple_size to enable structured bindings with Aeon::GTriple
	template <class FirstType, class SecondType, class ThirdType>
	struct tuple_size<Aeon::GTriple<FirstType, SecondType, ThirdType>>
		: public std::integral_constant<std::size_t, 3>
	{
	};
}

#endif