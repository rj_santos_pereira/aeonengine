#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_TUPLE_TUPLECOMMON
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_TUPLE_TUPLECOMMON

#include "Aeon/Framework/Core.hpp"

namespace Aeon
{
#define ZZINTERNAL_AEON_DECLARE_TUPLE_CONSTRUCTORS(TupleType, ObjectType) \
	constexpr TupleType()\
	requires CDefaultConstructible<ObjectType> = default;\
    \
	constexpr TupleType(TupleType const&)\
	requires CCopyable<ObjectType> = default;\
	constexpr TupleType& operator=(TupleType const&)\
	requires CCopyable<ObjectType> = default;\
    \
	constexpr TupleType(TupleType&&)\
	requires CMoveable<ObjectType> = default;\
	constexpr TupleType& operator=(TupleType&&)\
	requires CMoveable<ObjectType> = default;

#define ZZINTERNAL_AEON_DECLARE_DUPLE_CONSTRUCTORS(DupleType, FirstType, SecondType) \
    constexpr DupleType()\
    requires CDefaultConstructible<FirstType> && CDefaultConstructible<SecondType> = default;\
    \
    constexpr DupleType(DupleType const&)\
    requires CCopyable<FirstType> && CCopyable<SecondType> = default;\
    constexpr DupleType& operator=(DupleType const&)\
    requires CCopyable<FirstType> && CCopyable<SecondType> = default;\
    \
    constexpr DupleType(DupleType&&)\
    requires CMoveable<FirstType> && CMoveable<SecondType> = default;\
    constexpr DupleType& operator=(DupleType&&)\
    requires CMoveable<FirstType> && CMoveable<SecondType> = default;

#define ZZINTERNAL_AEON_DECLARE_TRIPLE_CONSTRUCTORS(TripleType, FirstType, SecondType, ThirdType) \
    constexpr TripleType()\
    requires CDefaultConstructible<FirstType> && CDefaultConstructible<SecondType> && CDefaultConstructible<ThirdType> = default;\
    \
    constexpr TripleType(TripleType const&)\
    requires CCopyable<FirstType> && CCopyable<SecondType> && CCopyable<ThirdType> = default;\
    constexpr TripleType& operator=(TripleType const&)\
    requires CCopyable<FirstType> && CCopyable<SecondType> && CCopyable<ThirdType> = default;\
    \
    constexpr TripleType(TripleType&&)\
    requires CMoveable<FirstType> && CMoveable<SecondType> && CMoveable<ThirdType> = default;\
    constexpr TripleType& operator=(TripleType&&)\
    requires CMoveable<FirstType> && CMoveable<SecondType> && CMoveable<ThirdType> = default;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CCompressibleClass

	template <class ClassType>
	concept CCompressibleClass = CEmptyClass<ClassType> && CNotFinalClass<ClassType>;

	namespace ZzInternal
	{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GNonCompressibleTupleElement

		// This is used by the implementation of GCompressedDuple, GCompressedTriple, and GTuple
		// to ensure that subobjects are initialized in the same order that the classes are parameterized
		template <class ObjType, szint IdxValue = 0>
		class GNonCompressibleTupleElement
		{
		public:
			template <class NewObjType>
			explicit constexpr GNonCompressibleTupleElement(NewObjType&& NewNonCompressedObj)
				: Object{ Forward<NewObjType>(NewNonCompressedObj) }
			{
			}

			ZZINTERNAL_AEON_DECLARE_TUPLE_CONSTRUCTORS(GNonCompressibleTupleElement, ObjType);

			ObjType Object;
		};

#	if 0
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GCompressibleTupleElement

		// This is used by the implementation of tuple-like containers
		// to ensure that all subobjects are accessible, see https://stackoverflow.com/a/4118457
		template <class ObjType, szint IdxValue = 0>
		requires CCompressibleClass<ObjType>
		class GCompressibleTupleElement : protected ObjType
		{
		public:
			template <class NewObjType>
			explicit constexpr GCompressibleTupleElement(NewObjType&& NewCompressedObj)
				: ObjType{ Forward<NewObjType>(NewCompressedObj) }
			{
			}

			ZZINTERNAL_AEON_DECLARE_TUPLE_CONSTRUCTORS(GCompressibleTupleElement, ObjType);
		};
#	endif
	}
}

#endif