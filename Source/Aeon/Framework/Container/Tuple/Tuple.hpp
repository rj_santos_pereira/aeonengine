#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_TUPLE_TUPLE
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_TUPLE_TUPLE

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Container/Tuple/TupleCommon.hpp"
//#include "Aeon/Framework/Math/StaticIndex.hpp"

namespace Aeon
{
	namespace ZzInternal
	{
		template <szint ObjCountValue, CObject ... ObjTypes>
		class GTupleStorage
		{
			static_assert(sizeof...(ObjTypes) == 0);

		public:
			explicit constexpr GTupleStorage() = default;

			AEON_MAKE_COPYABLE(GTupleStorage, constexpr);
			AEON_MAKE_MOVEABLE(GTupleStorage, constexpr);

		};

		// CHECKME could we use ZzInternal::GCompressibleTupleElement here for compressible classes?
		//  (maybe specialize ZzInternal_GTupleElement based on requires clause)
		template <szint ObjIdxValue, CObject ObjType, CObject ... RemainingObjTypes>
		class GTupleStorage<ObjIdxValue, ObjType, RemainingObjTypes ...>
			: protected GNonCompressibleTupleElement<ObjType, ObjIdxValue>,
			  protected GTupleStorage<ObjIdxValue + 1, RemainingObjTypes ...>
		{
			using BaseObjType = GNonCompressibleTupleElement<ObjType, ObjIdxValue>;
			using BaseRemainingObjType = GTupleStorage<ObjIdxValue + 1, RemainingObjTypes ...>;

		public:
			template <class NewObjType, class ... NewRemainingObjTypes>
			explicit constexpr GTupleStorage(NewObjType&& NewObject, NewRemainingObjTypes&& ... NewRemainingObjects)
			requires CConvertibleClass<NewObjType, ObjType>
				: BaseObjType{ Forward<NewObjType>(NewObject) }
				, BaseRemainingObjType{ Forward<NewRemainingObjTypes>(NewRemainingObjects) ... }
			{
			}

			ZZINTERNAL_AEON_DECLARE_TUPLE_CONSTRUCTORS(GTupleStorage, ObjType);
		};
	}

	template <class ... ObjTypes>
	// HACK constraint applied through 'requires' clause instead of type parameter
	//  as a workaround for making CTAD work in older compiler versions (MSVC <= 19.36 / clang <= 15.0)
	requires (... && CObject<ObjTypes>)
	class GTuple : protected ZzInternal::GTupleStorage<0, ObjTypes ...>
	{
		using Base = ZzInternal::GTupleStorage<0, ObjTypes ...>;

	public:
		template <szint IndexValue>
		using ObjectTypeAt = typename TTypePack<ObjTypes ...>::template ElementAt<IndexValue>;

		using IndexSequenceType = TMakeIndexSequence<sizeof...(ObjTypes)>;

		template <class ... NewObjTypes>
		explicit constexpr GTuple(NewObjTypes&& ... NewObjects)
			: Base{ Forward<NewObjTypes>(NewObjects) ... }
		{
			constexpr szint ArgumentCount = sizeof...(NewObjTypes);
			static_assert(ArgumentCount <= ObjectCount_Internal,
						  "'GTuple<ObjTypes ...>::GTuple(NewObjTypes ...)': "
						  "NewObjTypes must be not greater than the number of elements");
		}

		constexpr GTuple()
		requires (... && CDefaultConstructible<ObjTypes>) = default;
		constexpr GTuple(GTuple const&)
		requires (... && CCopyable<ObjTypes>) = default;
		constexpr GTuple& operator=(GTuple const&)
		requires (... && CCopyable<ObjTypes>) = default;
		constexpr GTuple(GTuple&&)
		requires (... && CMoveable<ObjTypes>) = default;
		constexpr GTuple& operator=(GTuple&&)
		requires (... && CMoveable<ObjTypes>) = default;

		template <szint IndexValue>
		AEON_FORCEINLINE constexpr ObjectTypeAt<IndexValue> const& ObjectAt() const
		{
			static_assert(IndexValue < ObjectCount_Internal,
						  "'GTuple<ObjTypes ...>::ObjectAt<IndexValue>()': "
						  "IndexValue must be less than the number of elements.");

			using StorageType = ZzInternal::GNonCompressibleTupleElement<ObjectTypeAt<IndexValue>, IndexValue>;
			return StorageType::Object;
		}
		template <szint IndexValue>
		AEON_FORCEINLINE constexpr ObjectTypeAt<IndexValue>& ObjectAt()
		{
			return AEON_INVOKE_CONST_OVERLOAD(template ObjectAt<IndexValue>);
		}

		AEON_FORCEINLINE constexpr szint ObjectCount() const
		{
			return ObjectCount_Internal;
		}

		template <class ... OtherObjTypes>
		AEON_FORCEINLINE constexpr GTuple<ObjTypes ..., OtherObjTypes ...> Concat(GTuple<OtherObjTypes ...> const& Other) const
		requires (... && CCopyable<ObjTypes>) && (... && CCopyable<OtherObjTypes>)
		{
			return Concat_Internal(Other, Indexes(), Other.Indexes());
		}

		AEON_FORCEINLINE constexpr IndexSequenceType Indexes() const
		{
			return IndexSequenceType{};
		}

		// TODO implement Swap?

		// Implementation of get (found via ADL) to enable structured bindings with Aeon::GTuple
		template <std::size_t IndexValue>
		friend ObjectTypeAt<IndexValue> const& get(GTuple const& Tuple)
		{
			return Tuple.template ObjectAt<IndexValue>();
		}
		template <std::size_t IndexValue>
		friend ObjectTypeAt<IndexValue>& get(GTuple& Tuple)
		{
			return Tuple.template ObjectAt<IndexValue>();
		}
		template <std::size_t IndexValue>
		friend ObjectTypeAt<IndexValue> get(GTuple&& Tuple)
		{
			return Tuple.template ObjectAt<IndexValue>();
		}

	private:
		template <class ... OtherObjTypes, szint ... ThisIdxValues, szint ... OtherIdxValues>
		AEON_FORCEINLINE constexpr GTuple<ObjTypes ..., OtherObjTypes ...> Concat_Internal(GTuple<OtherObjTypes ...> const& Other,
																						   TIndexSequence<ThisIdxValues ...>,
																						   TIndexSequence<OtherIdxValues ...>) const
		{
			return GTuple<ObjTypes ..., OtherObjTypes ...>{ this->template ObjectAt<ThisIdxValues>() ...,
			    											Other.template ObjectAt<OtherIdxValues>() ... };
		}

		static constexpr szint ObjectCount_Internal = sizeof...(ObjTypes);

	};

	template <class ... Types>
	GTuple(Types&& ...) -> GTuple<TDecayType<Types> ...>;
}

namespace std
{
// Specialization of std::tuple_element to enable structured bindings with Aeon::GTuple
	template <std::size_t IndexValue, class ... ObjTypes>
	struct tuple_element<IndexValue, Aeon::GTuple<ObjTypes ...>>
	{
		using type = typename Aeon::TTypePack<ObjTypes ...>::template ElementAt<IndexValue>;
	};

// Specialization of std::tuple_size to enable structured bindings with Aeon::GTuple
	template <class ... ObjTypes>
	struct tuple_size<Aeon::GTuple<ObjTypes ...>>
		: public std::integral_constant<std::size_t, sizeof...(ObjTypes)>
	{
	};
}

#endif