#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_TUPLE_DUPLE
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_TUPLE_DUPLE

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Container/Tuple/TupleCommon.hpp"

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GDuple

	template <CObject FirstType, CObject SecondType>
	class GDuple
	{
	public:
		template <szint IndexValue>
		using ObjectTypeAt = typename TTypePack<FirstType, SecondType>::template ElementAt<IndexValue>;

		template <class NewFirstType, class NewSecondType>
		constexpr GDuple(NewFirstType&& NewFirstObj, NewSecondType&& NewSecondObj)
		requires CConvertibleClass<NewFirstType, FirstType> && CConvertibleClass<NewSecondType, SecondType>
			: FirstObj{ Forward<NewFirstType>(NewFirstObj) }
			, SecondObj{ Forward<NewSecondType>(NewSecondObj) }
		{
		}

		ZZINTERNAL_AEON_DECLARE_DUPLE_CONSTRUCTORS(GDuple, FirstType, SecondType);

		template <szint IndexValue>
		AEON_FORCEINLINE constexpr ObjectTypeAt<IndexValue> const& ObjectAt() const
		{
			static_assert(IndexValue <= 1,
						  "'GDuple<FirstType, SecondType>::ObjectAt<IndexValue>()': "
						  "IndexValue must be 0 or 1.");
			if constexpr (IndexValue == 0)
			{
				return FirstObj;
			}
			else
			{
				return SecondObj;
			}
		}
		template <szint IndexValue>
		AEON_FORCEINLINE constexpr ObjectTypeAt<IndexValue>& ObjectAt()
		{
			return AEON_INVOKE_CONST_OVERLOAD(template ObjectAt<IndexValue>);
		}

		AEON_FORCEINLINE constexpr FirstType const& First() const { return FirstObj; }
		AEON_FORCEINLINE constexpr FirstType& First() { return FirstObj; }

		AEON_FORCEINLINE constexpr SecondType const& Second() const { return SecondObj; }
		AEON_FORCEINLINE constexpr SecondType& Second() { return SecondObj; }

		// TODO implement Swap?

		// Implementation of get (found via ADL) to enable structured bindings with Aeon::GDuple
		template <std::size_t IndexValue>
		friend ObjectTypeAt<IndexValue> const& get(GDuple const& Duple)
		{
			return Duple.template ObjectAt<IndexValue>();
		}
		template <std::size_t IndexValue>
		friend ObjectTypeAt<IndexValue>& get(GDuple& Duple)
		{
			return Duple.template ObjectAt<IndexValue>();
		}
		template <std::size_t IndexValue>
		friend ObjectTypeAt<IndexValue> get(GDuple&& Duple)
		{
			return Duple.template ObjectAt<IndexValue>();
		}

	private:
		FirstType FirstObj;
		SecondType SecondObj;
	};

	template <class FirstType, class SecondType>
	GDuple(FirstType&&, SecondType&&) -> GDuple<TDecayType<FirstType>, TDecayType<SecondType>>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GCompressedDuple

	template <class FirstType, class SecondType>
	class ZzInternal_GCompressedDuple
	{
	public:
		template <class NewFirstType, class NewSecondType>
		explicit constexpr ZzInternal_GCompressedDuple(NewFirstType&& NewFirstObj, NewSecondType&& NewSecondObj)
			: FirstObj{ Forward<NewFirstType>(NewFirstObj) }
			, SecondObj{ Forward<NewSecondType>(NewSecondObj) }
		{
		}

		ZZINTERNAL_AEON_DECLARE_DUPLE_CONSTRUCTORS(ZzInternal_GCompressedDuple, FirstType, SecondType);

		AEON_FORCEINLINE constexpr FirstType const& First() const { return FirstObj; }
		AEON_FORCEINLINE constexpr FirstType& First() { return FirstObj; }

		AEON_FORCEINLINE constexpr SecondType const& Second() const { return SecondObj; }
		AEON_FORCEINLINE constexpr SecondType& Second() { return SecondObj; }

	private:
		FirstType FirstObj;
		SecondType SecondObj;

	};

#if 0
	template <class FirstType, class SecondType>
	requires CCompressibleClass<SecondType>
	class ZzInternal_GCompressedDuple<FirstType, SecondType>
		: private ZzInternal::GNonCompressibleTupleElement<FirstType>
		, private SecondType
	{
		using BaseFirstType = ZzInternal::GNonCompressibleTupleElement<FirstType>;
		using BaseSecondType = SecondType;

	public:
		template <class NewFirstType, class NewSecondType>
		explicit constexpr ZzInternal_GCompressedDuple(NewFirstType&& NewFirstObj, NewSecondType&& NewSecondObj)
			: BaseFirstType{ Forward<NewFirstType>(NewFirstObj) }
			, BaseSecondType{ Forward<NewSecondType>(NewSecondObj) }
		{
		}

		ZZINTERNAL_AEON_DECLARE_DUPLE_CONSTRUCTORS(ZzInternal_GCompressedDuple, FirstType, SecondType);

		AEON_FORCEINLINE constexpr FirstType const& First() const { return BaseFirstType::Object; }
		AEON_FORCEINLINE constexpr FirstType& First() { return BaseFirstType::Object; }

		AEON_FORCEINLINE constexpr SecondType const& Second() const { return *static_cast<SecondType const*>(this); }
		AEON_FORCEINLINE constexpr SecondType& Second() { return *static_cast<SecondType*>(this); }
	};
#endif

	template <class FirstType, class SecondType>
	requires CCompressibleClass<SecondType>
	class ZzInternal_GCompressedDuple<FirstType, SecondType>
	    : private SecondType
	{
	public:
		template <class NewFirstType, class NewSecondType>
		explicit constexpr ZzInternal_GCompressedDuple(NewFirstType&& NewFirstObj, NewSecondType&& NewSecondObj)
			: SecondType{ Forward<NewSecondType>(NewSecondObj) }
			, FirstObj{ Forward<NewFirstType>(NewFirstObj) }
		{
		}

		ZZINTERNAL_AEON_DECLARE_DUPLE_CONSTRUCTORS(ZzInternal_GCompressedDuple, FirstType, SecondType);

		AEON_FORCEINLINE constexpr FirstType const& First() const { return FirstObj; }
		AEON_FORCEINLINE constexpr FirstType& First() { return FirstObj; }

		AEON_FORCEINLINE constexpr SecondType const& Second() const { return *static_cast<SecondType const*>(this); }
		AEON_FORCEINLINE constexpr SecondType& Second() { return *static_cast<SecondType*>(this); }

	private:
		FirstType FirstObj;
	};

	template <class FirstType, class SecondType>
	requires CCompressibleClass<FirstType> && CCompressibleClass<SecondType>
	class ZzInternal_GCompressedDuple<FirstType, SecondType>
		: private FirstType
		, private SecondType
	{
	public:
		template <class NewFirstType, class NewSecondType>
		explicit constexpr ZzInternal_GCompressedDuple(NewFirstType&& NewFirstObj, NewSecondType&& NewSecondObj)
			: FirstType{ Forward<NewFirstType>(NewFirstObj) }
			, SecondType{ Forward<NewSecondType>(NewSecondObj) }
		{
		}

		ZZINTERNAL_AEON_DECLARE_DUPLE_CONSTRUCTORS(ZzInternal_GCompressedDuple, FirstType, SecondType);

		AEON_FORCEINLINE constexpr FirstType const& First() const { return *static_cast<FirstType const*>(this); }
		AEON_FORCEINLINE constexpr FirstType& First() { return *static_cast<FirstType*>(this); }

		AEON_FORCEINLINE constexpr SecondType const& Second() const { return *static_cast<SecondType const*>(this); }
		AEON_FORCEINLINE constexpr SecondType& Second() { return *static_cast<SecondType*>(this); }

	};

	template <CObject FirstType, CObject SecondType>
	class GCompressedDuple : private ZzInternal_GCompressedDuple<FirstType, SecondType>
	{
		using Base = ZzInternal_GCompressedDuple<FirstType, SecondType>;

	public:
		using FirstObjectType = FirstType;
		using SecondObjectType = SecondType;

		template <class NewFirstType, class NewSecondType>
		explicit constexpr GCompressedDuple(NewFirstType&& NewFirstObj, NewSecondType&& NewSecondObj)
		requires CConvertibleClass<NewFirstType, FirstType> && CConvertibleClass<NewSecondType, SecondType>
			: Base{ Forward<NewFirstType>(NewFirstObj), Forward<NewSecondType>(NewSecondObj) }
		{
		}

		ZZINTERNAL_AEON_DECLARE_DUPLE_CONSTRUCTORS(GCompressedDuple, FirstType, SecondType);

		using Base::First;
		using Base::Second;

	};

	template <class FirstType, class SecondType>
	GCompressedDuple(FirstType&&, SecondType&&) -> GCompressedDuple<TDecayType<FirstType>, TDecayType<SecondType>>;
}

namespace std
{
// Specialization of std::tuple_element to enable structured bindings with Aeon::GDuple
	template <std::size_t IndexValue, class FirstType, class SecondType>
	struct tuple_element<IndexValue, Aeon::GDuple<FirstType, SecondType>>
	{
		using type = typename Aeon::TTypePack<FirstType, SecondType>::template ElementAt<IndexValue>;
	};

// Specialization of std::tuple_size to enable structured bindings with Aeon::GDuple
	template <class FirstType, class SecondType>
	struct tuple_size<Aeon::GDuple<FirstType, SecondType>>
		: public std::integral_constant<std::size_t, 2>
	{
	};
}

#endif