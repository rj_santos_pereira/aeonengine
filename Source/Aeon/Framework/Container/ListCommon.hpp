#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_LISTCOMMON
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_LISTCOMMON

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Container/ContainerCommon.hpp"
#include "Aeon/Framework/Container/Tuple/Duple.hpp"
#include "Aeon/Framework/Allocator/DynamicAllocator.hpp"
#include "Aeon/Framework/Allocator/FixedAllocator.hpp"

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// INodeHandle

	template <class HandleType, class ObjectType>
	class INodeHandle
	{
	protected:
		INodeHandle()
		{
			static_assert(VIsBaseClass<INodeHandle, HandleType>);
		}
		~INodeHandle() = default;

		// Obtains a handle to the node that follows this one
		virtual HandleType NextHandle() const = 0;
		// Obtains a handle to the node that precedes this one
		virtual HandleType PrevHandle() const = 0;
		// Checks if this handle holds a valid node
		virtual bool IsValid() const = 0;
		// Checks if the node pointed to by this handle is accessible
		virtual bool CanAccess() const = 0;

	public:
		// Dereferences this node and returns the element to which it points
		virtual ObjectType& operator*() const = 0;
		virtual ObjectType* operator->() const { return addressof(this->operator*()); }

		// Checks if this handle references is equal to another node
		virtual bool operator==(HandleType const&) const = 0;
	//	virtual bool operator!=(HandleType const& Other) const { return !static_cast<HandleType const*>(this)->operator==(Other); }

		virtual explicit operator bool() const = 0;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GListIterator

	template <class ListType>
	class GListIterator final : public ISequentialAccessIterator<GListIterator<ListType>, TPropagateConst<ListType, typename ListType::ElementType>>
	{
		using Base = ISequentialAccessIterator<GListIterator<ListType>, TPropagateConst<ListType, typename ListType::ElementType>>;

		using ListHandleType = typename ListType::HandleType;

	public:
		AEON_DECLARE_ITERATOR_TRAITS(Base);

		GListIterator() = default;
		explicit GListIterator(ListHandleType NewNodeHandle)
			: NodeHandle{ NewNodeHandle }
		{
			AEON_ASSERT(NodeHandle.IsValid());
		}

		AEON_MAKE_COPYABLE(GListIterator);

		virtual ReferenceType operator*() const override
		{
			// Don't allow dereferencing the past-the-last element
			AEON_ASSERT(NodeHandle.CanAccess());
			// The ListType parameter already conveys const-qualification, so we can just cast it away here if needed
			return const_cast<ReferenceType>(*NodeHandle);
		}

		virtual GListIterator& operator++() override
		{
			auto NewNodeHandle = NodeHandle.NextHandle();
			// Don't increment beyond the past-the-last element
			AEON_ASSERT(NewNodeHandle.IsValid());
			NodeHandle = NewNodeHandle;
			return *this;
		}
		virtual GListIterator& operator--() override
		{
			auto NewNodeHandle = NodeHandle.PrevHandle();
			// Don't decrement beyond the first element
			AEON_ASSERT(NewNodeHandle.IsValid());
			NodeHandle = NewNodeHandle;
			return *this;
		}

		virtual bool operator==(GListIterator const& Other) const override
		{
			return NodeHandle == Other.NodeHandle;
		}

		ListHandleType AsHandle_Internal() const
		{
			return NodeHandle;
		}

	private:
		ListHandleType NodeHandle;
	};
}

#endif