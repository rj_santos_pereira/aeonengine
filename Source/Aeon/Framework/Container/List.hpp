#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_LIST
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_LIST

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Container/ListCommon.hpp"

namespace Aeon::ZzInternal
{
	template <class HandleType, class ListType, class NodeType, class ObjectType>
	class IListNodeHandle : public INodeHandle<HandleType, ObjectType>
	{
	protected:
		IListNodeHandle(ListType* NewListPtr, NodeType* NewNodePtr)
			: ListPtr{ NewListPtr }
			, NodePtr{ NewNodePtr }
		{
		}

		IListNodeHandle() = default;
		~IListNodeHandle() = default;

		virtual HandleType NextHandle() const override
		{
			return { ListPtr, NodePtr->NextNode };
		}
		virtual HandleType PrevHandle() const override
		{
			return { ListPtr, NodePtr->PrevNode };
		}
		virtual bool IsValid() const override
		{
			return NodePtr;
		}
		virtual bool CanAccess() const override
		{
			// Only non-past-the-last handles are considered dereferenceable;
			// sentinel node never has a non-null NextNode
			return NodePtr && NodePtr->NextNode;
		}

	public:
		virtual explicit operator bool() const override
		{
			return CanAccess();
		}

	protected:
		ListType* ListPtr;
		NodeType* NodePtr;
	};
}

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GList

	template <CScalarObject ElemType, CAllocatorForObject<TDecayType<ElemType>> AllocType = GDynamicAllocator<ElemType>>
	class GList
	{
		struct DNode
		{
			// NOTE By checking whether NextNode is null or not, we determine if a node is the past-the-last sentinel node
			DNode* PrevNode = nullptr;
			DNode* NextNode = nullptr;
		};

		struct DObjectNode : DNode
		{
			// HACK This is due to GStorage not being CMoveableElseCopyable
			DObjectNode(DObjectNode const& Other)
			{
				ObjStorage.Construct(*Other.ObjStorage);
			}
			DObjectNode(DObjectNode&& Other)
			{
				ObjStorage.Construct(Move(*Other.ObjStorage));
			}

			DObjectNode& operator=(DObjectNode const& Other)
			{
				*ObjStorage = *Other.ObjStorage;
				return *this;
			}
			DObjectNode& operator=(DObjectNode&& Other)
			{
				*ObjStorage = Move(*Other.ObjStorage);
				return *this;
			}

			GStorage<TRemoveConstVolatile<ElemType>> ObjStorage;
		};

		class SMutableListNodeHandle;

		class SListNodeHandle final : public ZzInternal::IListNodeHandle<SListNodeHandle, GList const, DNode const, TAddConst<ElemType>>
		{
			using Base = ZzInternal::IListNodeHandle<SListNodeHandle, GList const, DNode const, TAddConst<ElemType>>;

			SListNodeHandle(GList const* NewListPtr, DNode const* NewNodePtr = nullptr)
				: Base{ NewListPtr, NewNodePtr }
			{
			}

		public:
			SListNodeHandle() = default;

			AEON_MAKE_COPYABLE(SListNodeHandle);

		private:
			using Base::NextHandle;
			using Base::PrevHandle;
			using Base::IsValid;
			using Base::CanAccess;

		public:
			virtual TAddConst<ElemType>& operator*() const override
			{
				return *static_cast<DObjectNode const*>(this->NodePtr)->ObjStorage;
			}

			// HACK Override here instead of base class to suppress Clang warning -Wambiguous-reversed-operator, not too sure what is causing it...
			virtual bool operator==(SListNodeHandle const& Other) const override
			{
				AEON_ASSERT(this->ListPtr == Other.ListPtr);
				return this->NodePtr == Other.NodePtr;
			}

		private:
			// HACK This is only called within the context of GList
			operator SMutableListNodeHandle()
			{
				return { const_cast<GList*>(this->ListPtr), const_cast<DNode*>(this->NodePtr) };
			}

			friend Base;
			friend class SMutableListNodeHandle;

			friend class GList;
			friend class GListIterator<GList>;
			friend class GListIterator<GList const>;
		};

		class SMutableListNodeHandle final : public ZzInternal::IListNodeHandle<SMutableListNodeHandle, GList, DNode, ElemType>
		{
			using Base = ZzInternal::IListNodeHandle<SMutableListNodeHandle, GList, DNode, ElemType>;

			SMutableListNodeHandle(GList* NewListPtr, DNode* NewNodePtr = nullptr)
				: Base{ NewListPtr, NewNodePtr }
			{
			}

		public:
			SMutableListNodeHandle() = default;

			AEON_MAKE_COPYABLE(SMutableListNodeHandle);

			virtual ElemType& operator*() const override
			{
				return const_cast<ElemType&>(*static_cast<DObjectNode const*>(this->NodePtr)->ObjStorage);
			}

			// HACK Override here instead of base class to suppress Clang warning -Wambiguous-reversed-operator, not too sure what is causing it...
			virtual bool operator==(SMutableListNodeHandle const& Other) const override
			{
				AEON_ASSERT(this->ListPtr == Other.ListPtr);
				return this->NodePtr == Other.NodePtr;
			}

			operator SListNodeHandle() const
			{
				return { this->ListPtr, this->NodePtr };
			}

		private:
			friend Base;
			friend class SListNodeHandle;

			friend class GList;
		};

		using ObjectNodeType = DObjectNode;
		using NodeType = DNode;

		static_assert(TAllocatorTraits<AllocType>::IsChunkableResource,
					  "'GList<ElemType, AllocType>': List requires an allocator that can support multiple allocation requests.");

		static_assert(CRebindableAllocator<AllocType, ObjectNodeType>,
					  "'GList<ElemType, AllocType>': List requires an allocator that can rebind to internal DObjectNode type.");

		using NodeAllocatorType = TRebindAllocator<AllocType, ObjectNodeType>;

	public:
		using ElementType = ElemType;
		using ConstElementType = TAddConst<ElemType>;

		using AllocatorType = AllocType;

		using IndexType = typename AllocType::IndexType;

		using IteratorType = GListIterator<GList>;
		using ConstIteratorType = GListIterator<TAddConst<GList>>;

		using HandleType = SListNodeHandle;
		using MutableHandleType = SMutableListNodeHandle;

		template <class NewAllocatorType = AllocatorType>
		explicit GList(NewAllocatorType&& NewAllocator = NewAllocatorType{})
		// CHECKME Should this support receiving a NodeAllocator? (GetAllocator could return a ref then)
		requires CSameClass<TDecayType<NewAllocatorType>, AllocatorType>
			: Data(StorageType{}, Forward<NewAllocatorType>(NewAllocator))
		{
			InitStorage_Internal();
		}

		GList(InitList<ElementType> InitialElemList)
		requires CCopyable<ElementType>
		{
			InitStorage_Internal();

			auto InitialCount = InitListOps::Count(InitialElemList);
			auto ElemPtr = InitListOps::Data(InitialElemList);

			ConstructFromInitList_Internal(InitialCount, ElemPtr);
		}
		GList(RvalRefInitList<ElementType> InitialElemList)
		requires CMoveable<ElementType>
		{
			InitStorage_Internal();

			auto InitialCount = InitListOps::Count(InitialElemList);
			auto ElemPtr = InitListOps::Data(InitialElemList);

			ConstructFromInitList_Internal(InitialCount, ElemPtr);
		}

		GList(GList const& Other)
		requires CCopyable<ElementType>
			: Data(StorageType{}, Other.Data.Second().OnContainerCopy())
		{
			InitStorage_Internal();

			StorageType& Storage = Data.First();
			StorageType const& OtherStorage = Other.Data.First();

			IndexType OtherNodeCount = OtherStorage.NodeCount;
			NodeType* OtherNodePtr = OtherStorage.HeadNodePtr;

			NodeType** CurrNodeAddr = Aeon::AddressOf(Storage.HeadNodePtr);
			NodeType* PrevNode = nullptr;

			AppendCopyFromNodeList_Internal(CurrNodeAddr, PrevNode, OtherNodeCount, OtherNodePtr);
		}
		GList(GList&& Other)
		requires CMoveable<ElementType> || CSwappableResourceAllocator<AllocatorType>
			: Data(StorageType{}, Other.Data.Second().OnContainerMove())
		{
			InitStorage_Internal();

			StorageType& Storage = Data.First();
			StorageType& OtherStorage = Other.Data.First();

			if constexpr (CSwappableResourceAllocator<AllocatorType>)
			{
				Storage.HeadNodePtr = OtherStorage.HeadNodePtr;
				Storage.NodeCount = OtherStorage.NodeCount;

				// NOTE Using the overloaded Swap(DNode&, DNode&) to correctly change the pointers in the Tail nodes
				using Aeon::Swap;
				Swap(Storage.SentinelNode, OtherStorage.SentinelNode);

				Other.InitStorage_Internal();
			}
			else
			{
				IndexType OtherNodeCount = OtherStorage.NodeCount;
				NodeType* OtherNodePtr = OtherStorage.HeadNodePtr;

				NodeType** CurrNodeAddr = Aeon::AddressOf(Storage.HeadNodePtr);
				NodeType* PrevNode = nullptr;

				AppendMoveFromNodeList_Internal(CurrNodeAddr, PrevNode, OtherNodeCount, OtherNodePtr);

				// Simulate move-like behavior on Other
				Other.Destruct_Internal();
			}
		}

		~GList()
		{
			Destruct_Internal();
		}

		GList& operator=(GList const& Other)
		requires CCopyable<ElementType>
		{
			AEON_FAILSAFE(this != addressof(Other), return *this);

			StorageType& Storage = Data.First();
			StorageType& OtherStorage = Other.Data.First();

			NodeAllocatorType& Allocator = Data.Second();
			NodeAllocatorType const& OtherAllocator = Other.Data.Second();

			IndexType OtherNodeCount = OtherStorage.NodeCount;
			NodeType* OtherNodePtr = OtherStorage.HeadNodePtr;

			Destruct_Internal();

			if (Allocator != OtherAllocator)
			{
				Allocator = OtherAllocator.OnContainerCopy();
			}

			NodeType** CurrNodeAddr = Aeon::AddressOf(Storage.HeadNodePtr);
			NodeType* PrevNode = nullptr;

			AppendCopyFromNodeList_Internal(CurrNodeAddr, PrevNode, OtherNodeCount, OtherNodePtr);

			return *this;
		}
		GList& operator=(GList&& Other)
		requires CMoveable<ElementType> || CSwappableResourceAllocator<AllocatorType>
		{
			AEON_FAILSAFE(this != addressof(Other), return *this);

			StorageType& Storage = Data.First();
			StorageType& OtherStorage = Other.Data.First();

			NodeAllocatorType& Allocator = Data.Second();
			NodeAllocatorType& OtherAllocator = Other.Data.Second();

			IndexType OtherNodeCount = OtherStorage.NodeCount;
			NodeType* OtherNodePtr = OtherStorage.HeadNodePtr;

			Destruct_Internal();

			if (Allocator != OtherAllocator)
			{
				Allocator = OtherAllocator.OnContainerMove();
			}

			if constexpr (CSwappableResourceAllocator<AllocatorType>)
			{
				Storage.HeadNodePtr = OtherStorage.HeadNodePtr;
				Storage.NodeCount = OtherStorage.NodeCount;

				// NOTE Using the overloaded Swap(DNode&, DNode&) to correctly change the pointers in the Tail nodes
				using Aeon::Swap;
				Swap(Storage.SentinelNode, OtherStorage.SentinelNode);

				Other.InitStorage_Internal();
			}
			else
			{
				NodeType** CurrNodeAddr = Aeon::AddressOf(Storage.HeadNodePtr);
				NodeType* PrevNode = nullptr;

				AppendMoveFromNodeList_Internal(CurrNodeAddr, PrevNode, OtherNodeCount, OtherNodePtr);

				// Simulate move-like behavior on Other
				Other.Destruct_Internal();
			}

			return *this;
		}

		void Empty()
		{
			Destruct_Internal();
		}

		GList& Append(GList const& Other)
		requires CCopyable<ElementType>
		{
			AEON_FAILSAFE(this != addressof(Other), return *this);

			StorageType& Storage = Data.First();
			StorageType const& OtherStorage = Other.Data.First();

			if (OtherStorage.NodeCount > 0)
			{
				NodeType* OtherNodePtr = OtherStorage.HeadNodePtr;
				IndexType OtherNodeCount = OtherStorage.NodeCount;

				NodeType* OldTailNode = Storage.SentinelNode.PrevNode;

				NodeType** CurrNodeAddr = Aeon::AddressOf(OldTailNode ? OldTailNode->NextNode : Storage.HeadNodePtr);
				NodeType* PrevNode = OldTailNode;

				AppendCopyFromNodeList_Internal(CurrNodeAddr, PrevNode, OtherNodeCount, OtherNodePtr);
			}

			return *this;
		}
		GList& Append(GList&& Other)
		requires CMoveable<ElementType>
		{
			AEON_FAILSAFE(this != addressof(Other), return *this);

			StorageType& Storage = Data.First();
			StorageType& OtherStorage = Other.Data.First();

			NodeAllocatorType const& Allocator = Data.Second();
			NodeAllocatorType const& OtherAllocator = Other.Data.Second();

			if (OtherStorage.NodeCount > 0)
			{
				bool NeedsPiecewiseAppend = true;

				// NOTE Unfortunately we have to require CMoveable, because if the allocators aren't equal, even with a shareable resource,
				//  then we cannot simply append the nodes, and need to do a piecewise append.

				// FIXME Maybe separate this into two different methods
				if constexpr (CShareableResourceAllocator<AllocatorType>)
				{
					if (Allocator == OtherAllocator)
					{
						// Acquire the other list's head and 'tail' (node previous to the sentinel) pointers while resetting their values
						NodeType* OtherSentinelNode = Aeon::AddressOf(OtherStorage.SentinelNode);

						NodeType* OtherHeadNode = Exchange(OtherStorage.HeadNodePtr, OtherSentinelNode);
						NodeType* OtherTailNode = Exchange(OtherSentinelNode->PrevNode, nullptr);

						// Now append the nodes to this list, taking care when it is empty to update the head pointer of our list
						NodeType* SentinelNode = Aeon::AddressOf(Storage.SentinelNode);

						// Start with the tail of our list, replacing it with the head of the other list
						if (Storage.NodeCount > 0)
						{
							NodeType* OldTailNode = SentinelNode->PrevNode;

							OldTailNode->NextNode = OtherHeadNode;
							OtherHeadNode->PrevNode = OldTailNode;
						}
						else
						{
							// Note that in this case, OtherHeadNode->PrevNode will already be null
							Storage.HeadNodePtr = OtherHeadNode;
						}

						// Finish by updating our sentinel tail node, as well as the tail of the other list
						OtherTailNode->NextNode = SentinelNode;
						SentinelNode->PrevNode = OtherTailNode;

						// Update the count of nodes while resetting the other list's value
						Storage.NodeCount += Exchange(OtherStorage.NodeCount, 0);

						NeedsPiecewiseAppend = false;
					}
				}

				if (NeedsPiecewiseAppend)
				{
					NodeType* OtherNodePtr = OtherStorage.HeadNodePtr;
					IndexType OtherNodeCount = OtherStorage.NodeCount;

					NodeType* OldTailNode = Storage.SentinelNode.PrevNode;

					NodeType** CurrNodeAddr = Aeon::AddressOf(OldTailNode ? OldTailNode->NextNode : Storage.HeadNode);
					NodeType* PrevNode = OldTailNode;

					AppendMoveFromNodeList_Internal(CurrNodeAddr, PrevNode, OtherNodeCount, OtherNodePtr);

					// Simulate move-like behavior on Other
					Other.Destruct_Internal();
				}
			}

			return *this;
		}

		MutableHandleType AddToFront(ConstElementType& Element)
		requires CCopyable<ElementType>
		{
			auto Iter = begin();
			return AddBefore(Iter.AsHandle_Internal(), Element);
		}
		MutableHandleType AddToFront(ElementType&& Element)
		requires CMoveable<ElementType>
		{
			auto Iter = begin();
			return AddBefore(Iter.AsHandle_Internal(), Move(Element));
		}
		template <class ... ArgumentTypes>
		MutableHandleType EmplaceInFront(ArgumentTypes&& ... Arguments)
		requires CConstructible<ElementType, ArgumentTypes ...>
		{
			auto Iter = begin();
			return EmplaceBefore(Iter.AsHandle_Internal(), Forward<ArgumentTypes>(Arguments) ...);
		}
		void RemoveFromFront()
		{
			auto Iter = begin();
			RemoveAt(Iter.AsHandle_Internal());
		}

		MutableHandleType AddToBack(ConstElementType& Element)
		requires CCopyable<ElementType>
		{
			auto Iter = end();
			return AddBefore(Iter.AsHandle_Internal(), Element);
		}
		MutableHandleType AddToBack(ElementType&& Element)
		requires CMoveable<ElementType>
		{
			auto Iter = end();
			return AddBefore(Iter.AsHandle_Internal(), Move(Element));
		}
		template <class ... ArgumentTypes>
		MutableHandleType EmplaceInBack(ArgumentTypes&& ... Arguments)
		requires CConstructible<ElementType, ArgumentTypes ...>
		{
			auto Iter = end();
			return EmplaceBefore(Iter.AsHandle_Internal(), Forward<ArgumentTypes>(Arguments) ...);
		}
		void RemoveFromBack()
		{
			auto Iter = --end();
			RemoveAt(Iter.AsHandle_Internal());
		}

		MutableHandleType AddBefore(HandleType Handle, ConstElementType& Element)
		requires CCopyable<ElementType>
		{
			AEON_ASSERT(this == Handle.ListPtr);

			StorageType& Storage = Data.First();
			NodeAllocatorType& NodeAllocator = Data.Second();

			ObjectNodeType* NewNode = NodeAllocator.Allocate(1);
			NewNode = AeonMemory::MemConstruct<ObjectNodeType>(NewNode);

			auto NodeAfter = const_cast<NodeType*>(Handle.NodePtr);
			auto NodeBefore = Exchange(NodeAfter->PrevNode, NewNode);

			NewNode->ObjStorage.Construct(Element);
			NewNode->NextNode = NodeAfter;
			NewNode->PrevNode = NodeBefore;

			if (NodeAfter == Storage.HeadNodePtr)
			{
				Storage.HeadNodePtr = NewNode;
			}
			else
			{
				NodeBefore->NextNode = NewNode;
			}

			++Storage.NodeCount;

			return MutableHandleType{ this, NewNode };
		}
		MutableHandleType AddBefore(HandleType Handle, ElementType&& Element)
		requires CMoveable<ElementType>
		{
			AEON_ASSERT(this == Handle.ListPtr);

			StorageType& Storage = Data.First();
			NodeAllocatorType& NodeAllocator = Data.Second();

			ObjectNodeType* NewNode = NodeAllocator.Allocate(1);
			NewNode = AeonMemory::MemConstruct<ObjectNodeType>(NewNode);

			auto NodeAfter = const_cast<NodeType*>(Handle.NodePtr);
			auto NodeBefore = Exchange(NodeAfter->PrevNode, NewNode);

			NewNode->ObjStorage.Construct(Move(Element));
			NewNode->NextNode = NodeAfter;
			NewNode->PrevNode = NodeBefore;

			if (NodeAfter == Storage.HeadNodePtr)
			{
				Storage.HeadNodePtr = NewNode;
			}
			else
			{
				NodeBefore->NextNode = NewNode;
			}

			++Storage.NodeCount;

			return MutableHandleType{ this, NewNode };
		}
		template <class ... ArgumentTypes>
		MutableHandleType EmplaceBefore(HandleType Handle, ArgumentTypes&& ... Arguments)
		requires CConstructible<ElementType, ArgumentTypes ...>
		{
			AEON_ASSERT(this == Handle.ListPtr);

			StorageType& Storage = Data.First();
			NodeAllocatorType& NodeAllocator = Data.Second();

			ObjectNodeType* NewNode = NodeAllocator.Allocate(1);
			NewNode = AeonMemory::MemConstruct<ObjectNodeType>(NewNode);

			auto NodeAfter = const_cast<NodeType*>(Handle.NodePtr);
			auto NodeBefore = Exchange(NodeAfter->PrevNode, NewNode);

			NewNode->ObjStorage.Construct(Forward<ArgumentTypes>(Arguments) ...);
			NewNode->NextNode = NodeAfter;
			NewNode->PrevNode = NodeBefore;

			if (NodeAfter == Storage.HeadNodePtr)
			{
				Storage.HeadNodePtr = NewNode;
			}
			else
			{
				NodeBefore->NextNode = NewNode;
			}

			++Storage.NodeCount;

			return MutableHandleType{ this, NewNode };
		}

		MutableHandleType AddAfter(HandleType Handle, ConstElementType& Element)
		requires CCopyable<ElementType>
		{
			AEON_ASSERT(this == Handle.ListPtr);

			StorageType& Storage = Data.First();
			NodeAllocatorType& NodeAllocator = Data.Second();

			// Cannot add element after sentinel node
			AEON_ASSERT(Handle.CanAccess());

			ObjectNodeType* NewNode = NodeAllocator.Allocate(1);
			NewNode = AeonMemory::MemConstruct<ObjectNodeType>(NewNode);

			auto NodeBefore = const_cast<NodeType*>(Handle.NodePtr);
			auto NodeAfter = Exchange(NodeBefore->NextNode, NewNode);

			NewNode->ObjStorage.Construct(Element);
			NewNode->NextNode = NodeAfter;
			NewNode->PrevNode = NodeBefore;

			NodeAfter->PrevNode = NewNode;

			++Storage.NodeCount;

			return MutableHandleType{ this, NewNode };
		}
		MutableHandleType AddAfter(HandleType Handle, ElementType&& Element)
		requires CMoveable<ElementType>
		{
			AEON_ASSERT(this == Handle.ListPtr);

			StorageType& Storage = Data.First();
			NodeAllocatorType& NodeAllocator = Data.Second();

			// Cannot add element after sentinel node
			AEON_ASSERT(Handle.CanAccess());

			ObjectNodeType* NewNode = NodeAllocator.Allocate(1);
			NewNode = AeonMemory::MemConstruct<ObjectNodeType>(NewNode);

			auto NodeBefore = const_cast<NodeType*>(Handle.NodePtr);
			auto NodeAfter = Exchange(NodeBefore->NextNode, NewNode);

			NewNode->ObjStorage.Construct(Move(Element));
			NewNode->NextNode = NodeAfter;
			NewNode->PrevNode = NodeBefore;

			NodeAfter->PrevNode = NewNode;

			++Storage.NodeCount;

			return MutableHandleType{ this, NewNode };
		}
		template <class ... ArgumentTypes>
		MutableHandleType EmplaceAfter(HandleType Handle, ArgumentTypes&& ... Arguments)
		requires CConstructible<ElementType, ArgumentTypes ...>
		{
			AEON_ASSERT(this == Handle.ListPtr);

			StorageType& Storage = Data.First();
			NodeAllocatorType& NodeAllocator = Data.Second();

			// Cannot add element after sentinel node
			AEON_ASSERT(Handle.CanAccess());

			ObjectNodeType* NewNode = NodeAllocator.Allocate(1);
			NewNode = AeonMemory::MemConstruct<ObjectNodeType>(NewNode);

			auto NodeBefore = const_cast<NodeType*>(Handle.NodePtr);
			auto NodeAfter = Exchange(NodeBefore->NextNode, NewNode);

			NewNode->ObjStorage.Construct(Forward<ArgumentTypes>(Arguments) ...);
			NewNode->NextNode = NodeAfter;
			NewNode->PrevNode = NodeBefore;

			NodeAfter->PrevNode = NewNode;

			++Storage.NodeCount;

			return MutableHandleType{ this, NewNode };
		}

		void RemoveAt(HandleType Handle)
		{
			AEON_ASSERT(this == Handle.ListPtr);

			StorageType& Storage = Data.First();
			NodeAllocatorType& NodeAllocator = Data.Second();

			AEON_ASSERT(Storage.NodeCount > 0);
			// Cannot remove sentinel node
			AEON_ASSERT(Handle.CanAccess());

			auto OldNode = static_cast<ObjectNodeType*>(const_cast<NodeType*>(Handle.NodePtr));
			auto NodeAfter = OldNode->NextNode;
			auto NodeBefore = OldNode->PrevNode;

			OldNode->ObjStorage.Destruct();

			NodeAfter->PrevNode = NodeBefore;

			if (NodeBefore)
			{
				NodeBefore->NextNode = NodeAfter;
			}

			--Storage.NodeCount;

			if (Storage.NodeCount == 0)
			{
				Storage.HeadNodePtr = Aeon::AddressOf(Storage.SentinelNode);
			}

			AeonMemory::MemDestruct<ObjectNodeType>(OldNode);
			NodeAllocator.Deallocate(OldNode);
		}

		template <class PredicateType = GLessThan<ElementType>>
		MutableHandleType AddSorted(ConstElementType& NewElement, PredicateType&& Predicate = PredicateType{ })
		requires CCopyable<ElementType> && CCallableWithResult<PredicateType, bool, ConstElementType&, ConstElementType&>
		{
			auto Handle = Find([&] (ConstElementType& Element) { return !Predicate(Element, NewElement); });
			return AddBefore(Handle, NewElement);
		}
		template <class PredicateType = GLessThan<ElementType>>
		MutableHandleType AddSorted(ElementType&& NewElement, PredicateType&& Predicate = PredicateType{ })
		requires CMoveable<ElementType> && CCallableWithResult<PredicateType, bool, ConstElementType&, ConstElementType&>
		{
			auto Handle = Find([&] (ConstElementType& Element) { return !Predicate(Element, NewElement); });
			return AddBefore(Handle, Move(NewElement));
		}

		bool Contains(ConstElementType& Value) const
		{
			return Find(Value) != nullptr;
		}
		template <class PredicateType>
		bool Contains(PredicateType&& Predicate) const
		requires CCallableWithResult<PredicateType, bool, ConstElementType&>
		{
			return Find(Predicate) != nullptr;
		}

		HandleType Find(ConstElementType& Value) const
		{
			auto Iter = std::find(cbegin(), cend(), Value);
			return Iter != cend() ? Iter.AsHandle_Internal() : HandleType{};
		}
		MutableHandleType Find(ConstElementType& Value)
		{
			return AEON_INVOKE_CONST_OVERLOAD_UNCAST(Find, Value);
		}

		template <class PredicateType>
		HandleType Find(PredicateType&& Predicate) const
		requires CCallableWithResult<PredicateType, bool, ConstElementType&>
		{
			auto Iter = std::find_if(cbegin(), cend(), Predicate);
			return Iter != cend() ? Iter.AsHandle_Internal() : HandleType{};
		}
		template <class PredicateType>
		MutableHandleType Find(PredicateType&& Predicate)
		requires CCallableWithResult<PredicateType, bool, ConstElementType&>
		{
			return AEON_INVOKE_CONST_OVERLOAD_UNCAST(Find, Predicate);
		}

		template <class PredicateType = GLessThan<ElementType>>
		bool IsSorted(PredicateType&& Predicate = PredicateType{}) const
		requires CCallableWithResult<PredicateType, bool, ConstElementType&, ConstElementType&>
		{
			return std::is_sorted(cbegin(), cend(), Predicate);
		}

		template <class PredicateType = GLessThan<ElementType>>
		GList& Sort(PredicateType&& Predicate = PredicateType{})
		requires CCallableWithResult<PredicateType, bool, ConstElementType&, ConstElementType&>
		{
			StorageType& Storage = Data.First();
			ListOps::Sort(Storage.HeadNodePtr, Storage.NodeCount, Predicate);
			return *this;
		}

		template <class PredicateType = GLessThan<ElementType>>
		GList& StableSort(PredicateType&& Predicate = PredicateType{})
		requires CCallableWithResult<PredicateType, bool, ConstElementType&, ConstElementType&>
		{
			return Sort(Predicate);
		}

		GList& Reverse()
		{
			StorageType& Storage = Data.First();
			ListOps::Reverse(Storage.HeadNodePtr, Aeon::AddressOf(Storage.SentinelNode), Storage.NodeCount);
			return *this;
		}

		template <CInferableScalarObject MappedElementType = TInferTypeTag,
				  CInferableAllocator MappedAllocatorType = TInferTypeTag,
				  class PredicateType>
		auto Map(PredicateType&& Predicate) const
			-> TMapOperationResult<GList, ElementType, MappedElementType, AllocatorType, MappedAllocatorType, PredicateType>
		requires CCallable<PredicateType, ConstElementType&>
		{
			using MappedListType = TMapOperationResult<GList,
													   ElementType, MappedElementType,
													   AllocatorType, MappedAllocatorType,
													   PredicateType>;

			StorageType const& Storage = Data.First();

			MappedListType MappedList;
			auto CurrNode = Storage.HeadNodePtr;
			while (CurrNode != Aeon::AddressOf(Storage.SentinelNode))
			{
				ConstElementType& ElemPtr = *static_cast<ObjectNodeType*>(CurrNode)->ObjStorage;
				MappedList.EmplaceInBack(Predicate(ElemPtr));
				CurrNode = CurrNode->NextNode;
			}
			return MappedList;
		}
		template <CInferableScalarObject FilteredElementType = TInferTypeTag,
				  CInferableAllocator FilteredAllocatorType = TInferTypeTag,
				  class PredicateType>
		auto Filter(PredicateType&& Predicate) const
			-> TFilterOperationResult<GList, ElementType, FilteredElementType, AllocatorType, FilteredAllocatorType>
		requires CCallable<PredicateType, ConstElementType&>
		{
			using FilteredListType = TFilterOperationResult<GList,
															ElementType, FilteredElementType,
															AllocatorType, FilteredAllocatorType>;

			StorageType const& Storage = Data.First();

			FilteredListType FilteredList;
			auto CurrNode = Storage.HeadNodePtr;
			while (CurrNode != Aeon::AddressOf(Storage.SentinelNode))
			{
				ConstElementType& ElemPtr = *static_cast<ObjectNodeType*>(CurrNode)->ObjStorage;
				if (Predicate(ElemPtr))
				{
					FilteredList.EmplaceInBack(ElemPtr);
				}
				CurrNode = CurrNode->NextNode;
			}
			return FilteredList;
		}
		template <CScalarObject FoldedElementType = ElementType,
				  class InitialElementType = ElementType,
				  class PredicateType>
		FoldedElementType Fold(PredicateType&& Predicate, InitialElementType&& InitialValue = InitialElementType{}) const
		requires CCallableWithResult<PredicateType, ElementType, ConstElementType&, ConstElementType&>
				 && CConstructible<ElementType, InitialElementType>
				 && CConvertibleClass<ElementType, FoldedElementType>
		{
			StorageType const& Storage = Data.First();

			// Note: we'll implicitly convert the type to FoldedElementType in the return statement
			ElementType FoldedElement{ Forward<InitialElementType>(InitialValue) };
			auto CurrNode = Storage.HeadNodePtr;
			while (CurrNode != Aeon::AddressOf(Storage.SentinelNode))
			{
				ConstElementType& ElemPtr = *static_cast<ObjectNodeType*>(CurrNode)->ObjStorage;
				FoldedElement = Predicate(FoldedElement, ElemPtr);
				CurrNode = CurrNode->NextNode;
			}
			return FoldedElement;
		}

		HandleType Front() const
		{
			StorageType const& Storage = Data.First();
			AEON_ASSERT(Storage.NodeCount != 0);
			return HandleType{ this, Storage.HeadNodePtr };
		}
		MutableHandleType Front()
		{
			return AEON_INVOKE_CONST_OVERLOAD_UNCAST(Front);
		}

		HandleType Back() const
		{
			StorageType const& Storage = Data.First();
			AEON_ASSERT(Storage.NodeCount != 0);
			return HandleType{ this, Storage.SentinelNode.PrevNode };
		}
		MutableHandleType Back()
		{
        	return AEON_INVOKE_CONST_OVERLOAD_UNCAST(Back);
		}

		ConstElementType& operator[](HandleType Handle) const
		{
			AEON_ASSERT(Handle.CanAccess());
			return *static_cast<ObjectNodeType const*>(Handle.NodePtr)->ObjStorage;
		}
		ElementType& operator[](HandleType Handle)
		{
			return AEON_INVOKE_CONST_OVERLOAD(operator[], Handle);
		}

		ConstElementType& operator[](IndexType Index) const
		{
			StorageType const& Storage = Data.First();
			AEON_ASSERT(Index < Storage.NodeCount);
			auto CurrNode = Storage.HeadNodePtr;
			while (Index-- > 0)
			{
				CurrNode = CurrNode->NextNode;
			}
			return *static_cast<ObjectNodeType*>(CurrNode)->ObjStorage;
		}
		ElementType& operator[](IndexType Index)
		{
			return AEON_INVOKE_CONST_OVERLOAD(operator[], Index);
		}

		IndexType AllocatedCount() const
		{
			return Data.First().NodeCount;
		}
		IndexType Count() const
		{
			return Data.First().NodeCount;
		}

		bool IsEmpty() const
		{
			return Count() == 0;
		}

		AllocatorType GetAllocator() const
		{
			return AllocatorType{ Data.Second().OnContainerCopy() };
		}

#if 0
		ConstIteratorType BeginIterator() const
		{
			return begin();
		}
		IteratorType BeginIterator()
		{
			return begin();
		}

		ConstIteratorType EndIterator() const
		{
			return end();
		}
		IteratorType EndIterator()
		{
			return end();
		}
#endif

		AEON_FORCEINLINE ConstIteratorType begin() const
		{
			return ConstIteratorType{ HandleType{ this, Data.First().HeadNodePtr } };
		}
		AEON_FORCEINLINE IteratorType begin()
		{
			return IteratorType{ HandleType{ this, Data.First().HeadNodePtr } };
		}
		AEON_FORCEINLINE ConstIteratorType cbegin() const
		{
			return begin();
		}

		AEON_FORCEINLINE ConstIteratorType end() const
		{
			return ConstIteratorType{ HandleType{ this, Aeon::AddressOf(Data.First().SentinelNode) } };
		}
		AEON_FORCEINLINE IteratorType end()
		{
			return IteratorType{ HandleType{ this, Aeon::AddressOf(Data.First().SentinelNode) } };
		}
		AEON_FORCEINLINE ConstIteratorType cend() const
		{
			return end();
		}

		friend void Swap(GList& List1, GList& List2)
		requires CSwappableResourceAllocator<AllocatorType>
		{
			StorageType& Storage1 = List1.Data;
			StorageType& Storage2 = List2.Data;

			NodeAllocatorType& NodeAllocator1 = List1.Data.Second();
			NodeAllocatorType& NodeAllocator2 = List2.Data.Second();

			using Aeon::Swap;

			Swap(Storage1.HeadNodePtr, Storage2.HeadNodePtr);
			Swap(Storage1.SentinelNode, Storage2.SentinelNode);
			Swap(Storage1.NodeCount, Storage2.NodeCount);
			Swap(NodeAllocator1, NodeAllocator2);

			// NOTE This is necessary to correct the value of HeadNodePtr when the respective list is empty,
			//  otherwise it will remain pointing to the sentinel node of the other (respective) list.
			if (Storage1.NodeCount == 0)
			{
				Storage1.InitStorage_Internal();
			}
			if (Storage2.NodeCount == 0)
			{
				Storage2.InitStorage_Internal();
			}
		}

	private:
		void InitStorage_Internal()
		{
			StorageType& Storage = Data.First();

			Storage.HeadNodePtr = Aeon::AddressOf(Storage.SentinelNode);
			Storage.NodeCount = 0;
		}

		template <class NewElementType>
		void ConstructFromInitList_Internal(IndexType NodeCount, NewElementType const* ElemPtr)
		requires CSameClass<NewElementType, TDecayType<ElementType>>
				 || CSameClass<NewElementType, GRvalRefWrapper<TDecayType<ElementType>>>
		{
			StorageType& Storage = Data.First();
			NodeAllocatorType& NodeAllocator = Data.Second();

			NodeType** CurrNodeAddr = Aeon::AddressOf(Storage.HeadNodePtr);
			NodeType* PrevNode = nullptr;

			for (IndexType Index = 0; Index < NodeCount; ++Index)
			{
				NodeType*& CurrNodeRef = *CurrNodeAddr;

				CurrNodeRef = NodeAllocator.Allocate(1);
				ObjectNodeType* NewNode = AeonMemory::MemConstruct<ObjectNodeType>(CurrNodeRef);
				CurrNodeRef = NewNode;

				NewNode->ObjStorage.Construct(MoveElseCopy(*ElemPtr++));
				NewNode->PrevNode = PrevNode;

				PrevNode = CurrNodeRef;
				CurrNodeAddr = Aeon::AddressOf(CurrNodeRef->NextNode);
			}

			*CurrNodeAddr = Aeon::AddressOf(Storage.SentinelNode);

			Storage.SentinelNode.PrevNode = PrevNode;
			Storage.NodeCount = NodeCount;
		}

		void AppendCopyFromNodeList_Internal(NodeType** CurrNodeAddr, NodeType* PrevNode, IndexType NodeCount, NodeType* NodePtr)
		{
			StorageType& Storage = Data.First();
			NodeAllocatorType& NodeAllocator = Data.Second();

			for (IndexType Index = 0; Index < NodeCount; ++Index)
			{
				NodeType*& CurrNodeRef = *CurrNodeAddr;

				CurrNodeRef = NodeAllocator.Allocate(1);
				ObjectNodeType* NewNode = AeonMemory::MemConstruct<ObjectNodeType>(CurrNodeRef);
				CurrNodeRef = NewNode;

				NewNode->ObjStorage.Construct(Copy(*static_cast<ObjectNodeType*>(NodePtr)->ObjStorage));
				NewNode->PrevNode = PrevNode;

				PrevNode = CurrNodeRef;
				CurrNodeAddr = Aeon::AddressOf(CurrNodeRef->NextNode);

				NodePtr = NodePtr->NextNode;
			}

			*CurrNodeAddr = Aeon::AddressOf(Storage.SentinelNode);

			Storage.SentinelNode.PrevNode = PrevNode;
			Storage.NodeCount += NodeCount;
		}
		void AppendMoveFromNodeList_Internal(NodeType** CurrNodeAddr, NodeType* PrevNode, IndexType NodeCount, NodeType* NodePtr)
		{
			StorageType& Storage = Data.First();
			NodeAllocatorType& NodeAllocator = Data.Second();

			for (IndexType Index = 0; Index < NodeCount; ++Index)
			{
				NodeType*& CurrNodeRef = *CurrNodeAddr;

				CurrNodeRef = NodeAllocator.Allocate(1);
				ObjectNodeType* NewNode = AeonMemory::MemConstruct<ObjectNodeType>(CurrNodeRef);
				CurrNodeRef = NewNode;

				NewNode->ObjStorage.Construct(Move(*static_cast<ObjectNodeType*>(NodePtr)->ObjStorage));
				NewNode->PrevNode = PrevNode;

				PrevNode = CurrNodeRef;
				CurrNodeAddr = Aeon::AddressOf(CurrNodeRef->NextNode);

				NodePtr = NodePtr->NextNode;
			}

			*CurrNodeAddr = Aeon::AddressOf(Storage.SentinelNode);

			Storage.SentinelNode.PrevNode = PrevNode;
			Storage.NodeCount += NodeCount;
		}

		void Destruct_Internal()
		{
			StorageType& Storage = Data.First();
			NodeAllocatorType& NodeAllocator = Data.Second();

			auto CurrNode = Storage.HeadNodePtr;
			while (CurrNode != Aeon::AddressOf(Storage.SentinelNode))
			{
				ObjectNodeType* OldNode = static_cast<ObjectNodeType*>(CurrNode);

				OldNode->ObjStorage.Destruct();

				AeonMemory::MemDestruct<ObjectNodeType>(OldNode);
				NodeAllocator.Deallocate(OldNode);

				CurrNode = CurrNode->NextNode;
			}

			Storage.HeadNodePtr = Aeon::AddressOf(Storage.SentinelNode);
			Storage.SentinelNode.PrevNode = nullptr;
			Storage.NodeCount = 0;
		}

		friend void Swap(DNode& Node1, DNode& Node2)
		{
			if (Node1.PrevNode)
			{
				Node1.PrevNode->NextNode = Aeon::AddressOf(Node2);
			}
			if (Node1.NextNode)
			{
				Node1.NextNode->PrevNode = Aeon::AddressOf(Node2);
			}

			if (Node2.PrevNode)
			{
				Node2.PrevNode->NextNode = Aeon::AddressOf(Node1);
			}
			if (Node2.NextNode)
			{
				Node2.NextNode->PrevNode = Aeon::AddressOf(Node1);
			}

			using Aeon::Swap;
			Swap(Node1.PrevNode, Node2.PrevNode);
			Swap(Node1.NextNode, Node2.NextNode);
		}

		struct ListOps
		{
			// Merge sort implementation taken from MSVC (std::list<Type>::sort)
			template <class PredicateType>
			static NodeType* Sort(NodeType*& HeadNode, IndexType NodeCount, PredicateType&& Predicate)
			{
				// Order [_First, _First + _Size), return _First + _Size
				switch (NodeCount)
				{
					case 0:
						return HeadNode;
					case 1:
						return HeadNode->NextNode;
					default:
						break;
				}

				auto MidNode = Sort(HeadNode, NodeCount / 2, Predicate);
				auto TailNode = Sort(MidNode, NodeCount - NodeCount / 2, Predicate);
				HeadNode = Merge(HeadNode, MidNode, TailNode, Predicate);
				return TailNode;
			}
			template <class PredicateType>
			static NodeType* Merge(NodeType* HeadNode, NodeType* MidNode, NodeType* TailNode, PredicateType&& Predicate)
			{
				// Merge the sorted ranges [_First, _Mid) and [_Mid, _Last)
				// Returns the new beginning of the range (which won't be _First if it was spliced elsewhere)
				AEON_ASSERT(HeadNode != MidNode && MidNode != TailNode);

				NodeType* NewHeadNode;
				if (Predicate(*static_cast<ObjectNodeType*>(MidNode)->ObjStorage,
							  *static_cast<ObjectNodeType*>(HeadNode)->ObjStorage))
				{
					// _Mid will be spliced to the front of the range
					NewHeadNode = MidNode;
				}
				else
				{
					// Establish _Pred(_Mid->_Myval, _First->_Myval) by skipping over elements from the first
					// range already in position.
					NewHeadNode = HeadNode;
					do
					{
						HeadNode = HeadNode->NextNode;
						if (HeadNode == MidNode)
						{
							return NewHeadNode;
						}
					}
					while (!Predicate(*static_cast<ObjectNodeType*>(MidNode)->ObjStorage,
									  *static_cast<ObjectNodeType*>(HeadNode)->ObjStorage));
				}

				while (true)
				{
					// Process one run splice
					auto RunHeadNode = MidNode;
					do
					{
						// Find the end of the "run" of elements we need to splice from the second range into the first
						MidNode = MidNode->NextNode;
						if (MidNode == TailNode)
						{
							break;
						}
					}
					while (Predicate(*static_cast<ObjectNodeType*>(MidNode)->ObjStorage,
									 *static_cast<ObjectNodeType*>(HeadNode)->ObjStorage));

					// [_Run_start, _Mid) goes before _First->_Myval
					Splice(HeadNode, RunHeadNode, MidNode);
					if (MidNode == TailNode)
					{
						return NewHeadNode;
					}

					// Reestablish _Pred(_Mid->_Myval, _First->_Myval) by skipping over elements from the first
					// range already in position.
					do
					{
						HeadNode = HeadNode->NextNode;
						if (HeadNode == MidNode)
						{
							return NewHeadNode;
						}
					}
					while (!Predicate(*static_cast<ObjectNodeType*>(MidNode)->ObjStorage,
									  *static_cast<ObjectNodeType*>(HeadNode)->ObjStorage));
				}
			}
			static void Splice(NodeType* OldHeadNode, NodeType* HeadNode, NodeType* TailNode) noexcept
			{
				// Splice [_First, _Last) before _Before; returns _Last
				AEON_ASSERT(OldHeadNode != HeadNode && OldHeadNode != TailNode && HeadNode != TailNode);
				// 3 reads and 6 writes

				// Fixup the _Next values
				auto HeadPrevNode = HeadNode->PrevNode;
				HeadPrevNode->NextNode = TailNode;
				auto TailPrevNode = TailNode->PrevNode;
				TailPrevNode->NextNode = OldHeadNode;
				auto OldHeadPrevNode = OldHeadNode->PrevNode;
				// NOTE(AeonEngine): No (sentinel) node before head node, so this check is necessary
				if (OldHeadPrevNode)
				{
					OldHeadPrevNode->NextNode = HeadNode;
				}

				// Fixup the _Prev values
				OldHeadNode->PrevNode = TailPrevNode;
				TailNode->PrevNode = HeadPrevNode;
				HeadNode->PrevNode = OldHeadPrevNode;
			}

			static void Reverse(NodeType*& HeadNode, NodeType* TailNode, IndexType NodeCount)
			{
				switch (NodeCount)
				{
					case 0:
					case 1:
						return;
					default:
						break;
				}

				// Note that this CurrNode is the new TailNode
				auto OldTailNode = Exchange(TailNode->PrevNode, HeadNode);

				// This looks backwards, but each node's NextNode and PrevNode fields will be swapped inside the loop,
				// this avoids checking for the edge case of the head and tail nodes
				HeadNode->PrevNode = TailNode;
				OldTailNode->NextNode = nullptr;

				auto CurrNode = HeadNode;
				auto PrevNode = CurrNode;
				while (CurrNode)
				{
					PrevNode = Exchange(CurrNode->PrevNode, CurrNode->NextNode);
					CurrNode = Exchange(CurrNode->NextNode, Exchange(PrevNode, CurrNode));
				}

				HeadNode = PrevNode;
			}
		};

		struct DDataStorage
		{
			NodeType* HeadNodePtr;
			NodeType SentinelNode;
			IndexType NodeCount;
		};

		using StorageType = DDataStorage;

		GCompressedDuple<StorageType, NodeAllocatorType> Data;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GFixedList

	template <CScalarObject ElemType, szint ElemCountValue>
	using GFixedList = GList<ElemType, GFixedDynamicAllocator<ElemType, ElemCountValue>>;
}

#endif