#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_DEARRAY
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_DEARRAY

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Container/ArrayCommon.hpp"

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GDearray

	template <CScalarObject ElemType, CAllocatorForObject<TDecayType<ElemType>> AllocType = GDynamicAllocator<ElemType>>
	requires CMoveableElseCopyable<ElemType>
	class GDearray
	{
		// NOTE: Increasing/decreasing allocation will relocate all elements to be contiguous
		//  with the first element being at the start of the storage block

		using ElementPtrType = TAddPointer<typename AllocType::ObjectType>;

	public:
		using ElementType = ElemType;
		using ConstElementType = TAddConst<ElemType>;

		using AllocatorType = AllocType;

		using IndexType = typename AllocType::IndexType;

		using IteratorType = GDearrayIterator<GDearray>;
		using ConstIteratorType = GDearrayIterator<TAddConst<GDearray>>;

		template <class NewAllocatorType = AllocatorType>
		explicit GDearray(NewAllocatorType&& NewAllocator = NewAllocatorType{})
		requires CSameClass<TDecayType<NewAllocatorType>, AllocatorType>
			: Data(StorageType{}, Forward<NewAllocatorType>(NewAllocator))
		{
			AllocatorType& Allocator = Data.Second();

			IndexType InitialAllocatedCount = Allocator.RecommendedStartingAllocationSize();
			Allocate_Internal(InitialAllocatedCount);
		}
		template <class NewAllocatorType = AllocatorType>
		explicit GDearray(TReserveStorage, IndexType InitialAllocatedCount, NewAllocatorType&& NewAllocator = NewAllocatorType{})
		requires CSameClass<TDecayType<NewAllocatorType>, AllocatorType>
			: Data(StorageType{}, Forward<NewAllocatorType>(NewAllocator))
		{
			Allocate_Internal(InitialAllocatedCount);
		}

		template <class NewAllocatorType = AllocatorType>
		explicit GDearray(TCreateElements, IndexType InitialCount, NewAllocatorType&& NewAllocator = NewAllocatorType{})
		requires CDefaultConstructible<ElementType>
		    	 && CSameClass<TDecayType<NewAllocatorType>, AllocatorType>
			: Data(StorageType{}, Forward<NewAllocatorType>(NewAllocator))
		{
			Allocate_Internal(InitialCount);
			if (InitialCount > 0)
			{
				Construct_Internal(InitialCount);
			}
		}
		template <class NewAllocatorType = AllocatorType>
		explicit GDearray(TCreateElements, IndexType InitialCount, ConstElementType& InitialElement,
									 NewAllocatorType&& NewAllocator = NewAllocatorType{})
		requires CCopyable<ElementType>
				 && CSameClass<TDecayType<NewAllocatorType>, AllocatorType>
			: Data(StorageType{}, Forward<NewAllocatorType>(NewAllocator))
		{
			Allocate_Internal(InitialCount);
			if (InitialCount > 0)
			{
				ConstructFromValue_Internal(InitialCount, InitialElement);
			}
		}

		GDearray(InitList<ElementType> InitialElemList)
		requires CCopyable<ElementType>
		{
			auto InitialCount = IndexType(InitListOps::Count(InitialElemList));
			auto ElemPtr = InitListOps::Data(InitialElemList);

			Allocate_Internal(InitialCount);
			if (InitialCount > 0)
			{
				ConstructFromInitList_Internal(InitialCount, ElemPtr);
			}
		}
		GDearray(RvalRefInitList<ElementType> InitialElemList)
		requires CMoveable<ElementType>
		{
			auto InitialCount = IndexType(InitListOps::Count(InitialElemList));
			auto ElemPtr = InitListOps::Data(InitialElemList);

			Allocate_Internal(InitialCount);
			if (InitialCount > 0)
			{
				ConstructFromInitList_Internal(InitialCount, ElemPtr);
			}
		}

		GDearray(GDearray const& Other)
		requires CCopyable<ElementType>
			: Data(StorageType{}, Other.Data.Second().OnContainerCopy())
		{
			StorageType const& OtherStorage = Other.Data.First();

			IndexType OtherElemCount = OtherStorage.ElemCount;

			Allocate_Internal(OtherElemCount);
			if (OtherElemCount > 0)
			{
				AppendCopyFromArray_Internal(Other);
			}
		}
		GDearray(GDearray&& Other)
		requires CMoveable<ElementType> || CSwappableResourceAllocator<AllocatorType>
			: Data(StorageType{}, Other.Data.Second().OnContainerMove())
		{
			[[maybe_unused]]
			StorageType& Storage = Data.First();
			StorageType& OtherStorage = Other.Data.First();

			IndexType OtherElemCount = OtherStorage.ElemCount;

			if constexpr (CSwappableResourceAllocator<AllocatorType>)
			{
				Storage = Exchange(OtherStorage, StorageType{});
			}
			else
			{
				Allocate_Internal(OtherElemCount);

				if (OtherElemCount > 0)
				{
					AppendMoveFromArray_Internal(Other);
				}

				// Simulate move-like behavior on Other
				Other.Deallocate_Internal();
			}
		}

		~GDearray()
		{
			Destruct_Internal(0);
			Deallocate_Internal();
		}

		GDearray& operator=(GDearray const& Other)
		requires CCopyable<ElementType>
		{
			AEON_FAILSAFE(this != addressof(Other), return *this);

			StorageType const& OtherStorage = Other.Data.First();

			AllocatorType& Allocator = Data.Second();
			AllocatorType const& OtherAllocator = Other.Data.Second();

			IndexType OtherElemCount = OtherStorage.ElemCount;

			Destruct_Internal(0);

			if (Allocator != OtherAllocator)
			{
				Deallocate_Internal();
				Allocator = OtherAllocator.OnContainerCopy();
				Allocate_Internal(OtherElemCount);
			}
			else
			{
				Reallocate_Internal(OtherElemCount);
			}

			if (OtherElemCount > 0)
			{
				AppendCopyFromArray_Internal(Other);
			}

			return *this;
		}
		GDearray& operator=(GDearray&& Other)
		requires CMoveable<ElementType> || CSwappableResourceAllocator<AllocatorType>
		{
			AEON_FAILSAFE(this != addressof(Other), return *this);

			[[maybe_unused]]
			StorageType& Storage = Data.First();
			StorageType& OtherStorage = Other.Data.First();

			AllocatorType& Allocator = Data.Second();
			AllocatorType& OtherAllocator = Other.Data.Second();

			IndexType OtherElemCount = OtherStorage.ElemCount;

			Destruct_Internal(0);

			if constexpr (CSwappableResourceAllocator<AllocatorType>)
			{
				Deallocate_Internal();

				if (Allocator != OtherAllocator)
				{
					Allocator = OtherAllocator.OnContainerMove();
				}

				Storage = Exchange(OtherStorage, StorageType{});
			}
			else
			{
				if (Allocator != OtherAllocator)
				{
					Deallocate_Internal();
					Allocator = OtherAllocator.OnContainerMove();
					Allocate_Internal(OtherElemCount);
				}
				else
				{
					Reallocate_Internal(OtherElemCount);
				}

				if (OtherElemCount > 0)
				{
					AppendMoveFromArray_Internal(Other);
				}

				// Simulate move-like behavior on Other
				Other.Deallocate_Internal();
			}

			return *this;
		}

		void Reserve(IndexType NewAllocatedCount)
		{
			CheckIncreaseAllocation_Internal(NewAllocatedCount);
		}

		void Prune()
		{
			StorageType& Storage = Data.First();

			CheckDecreaseAllocation_Internal(Storage.ElemCount);
		}

		void Resize(IndexType NewElemCount)
		requires CDefaultConstructible<ElementType>
		{
			StorageType& Storage = Data.First();

			if (Storage.ElemCount < NewElemCount)
			{
				CheckIncreaseAllocation_Internal(NewElemCount);

				Construct_Internal(NewElemCount);
			}
			else if (Storage.ElemCount > NewElemCount)
			{
				Destruct_Internal(NewElemCount);

				CheckDecreaseAllocation_Internal(NewElemCount);
			}
		}
		void Resize(IndexType NewElemCount, ElementType const& NewElement)
		requires CCopyable<ElementType>
		{
			StorageType& Storage = Data.First();

			if (Storage.ElemCount < NewElemCount)
			{
				CheckIncreaseAllocation_Internal(NewElemCount);

				ConstructFromValue_Internal(NewElemCount, NewElement);
			}
			else if (Storage.ElemCount > NewElemCount)
			{
				Destruct_Internal(NewElemCount);

				CheckDecreaseAllocation_Internal(NewElemCount);
			}
		}

		void Empty()
		{
			Destruct_Internal(0);
		}
		void Empty(TReclaimStorage)
		{
			Destruct_Internal(0);

			// Forcefully deallocate all memory
			Deallocate_Internal();
		}

		GDearray& Append(GDearray const& Other)
		requires CCopyable<ElementType>
		{
			AEON_ASSERT(this != addressof(Other));

			StorageType& Storage = Data.First();
			StorageType const& OtherStorage = Other.Data.First();

			if (OtherStorage.ElemCount > 0)
			{
				IndexType NewElemCount = Storage.ElemCount + OtherStorage.ElemCount;

				CheckIncreaseAllocation_Internal(NewElemCount);
				AppendCopyFromArray_Internal(Other);
			}

			return *this;
		}
		GDearray& Append(GDearray&& Other)
		requires CMoveable<ElementType>
		{
			AEON_ASSERT(this != addressof(Other));

			StorageType& Storage = Data.First();
			StorageType& OtherStorage = Other.Data.First();

			if (OtherStorage.ElemCount > 0)
			{
				IndexType NewElemCount = Storage.ElemCount + OtherStorage.ElemCount;

				CheckIncreaseAllocation_Internal(NewElemCount);
				AppendMoveFromArray_Internal(Other);

				// Simulate move-like behavior on Other
				Other.Deallocate_Internal();
			}

			return *this;
		}

		ElementType& AddToFront(ConstElementType& NewElement)
		requires CCopyable<ElementType>
		{
			StorageType& Storage = Data.First();

			CheckIncreaseAllocation_Internal();

			++Storage.ElemCount;
			Storage.BufferFirstIdx = (Storage.BufferFirstIdx > 0 ? Storage.BufferFirstIdx : Storage.ElemCapacity) - 1;

			ElementType* ElemPtr = Storage.BufferPtr + Storage.BufferFirstIdx;
			return *AeonMemory::MemConstruct<ElementType>(ElemPtr, NewElement);
		}
		ElementType& AddToFront(ElementType&& NewElement)
		requires CMoveable<ElementType>
		{
			StorageType& Storage = Data.First();

			CheckIncreaseAllocation_Internal();

			++Storage.ElemCount;
			Storage.BufferFirstIdx = (Storage.BufferFirstIdx > 0 ? Storage.BufferFirstIdx : Storage.ElemCapacity) - 1;

			ElementType* ElemPtr = Storage.BufferPtr + Storage.BufferFirstIdx;
			return *AeonMemory::MemConstruct<ElementType>(ElemPtr, Move(NewElement));
		}

		template <class ... ArgumentTypes>
		ElementType& EmplaceInFront(ArgumentTypes&& ... Arguments)
		requires CConstructible<ElementType, ArgumentTypes ...>
		{
			StorageType& Storage = Data.First();

			CheckIncreaseAllocation_Internal();

			++Storage.ElemCount;
			Storage.BufferFirstIdx = (Storage.BufferFirstIdx > 0 ? Storage.BufferFirstIdx : Storage.ElemCapacity) - 1;

			ElementType* ElemPtr = Storage.BufferPtr + Storage.BufferFirstIdx;
			return *AeonMemory::MemConstruct<ElementType>(ElemPtr, Forward<ArgumentTypes>(Arguments) ...);
		}

		void RemoveFromFront()
		{
			StorageType& Storage = Data.First();
			AEON_ASSERT(Storage.ElemCount > 0);

			ElementType* ElemPtr = Storage.BufferPtr + Storage.BufferFirstIdx;
			AeonMemory::MemDestruct<ElementType>(ElemPtr);

			--Storage.ElemCount;
			Storage.BufferFirstIdx
				= ((Storage.BufferFirstIdx + 1) < Storage.ElemCapacity) && (Storage.ElemCount > 0)
				  ? (Storage.BufferFirstIdx + 1)
				  : 0;
		}
		void RemoveFromFront(TReclaimStorage)
		{
			StorageType& Storage = Data.First();
			AEON_ASSERT(Storage.ElemCount > 0);

			ElementType* ElemPtr = Storage.BufferPtr + Storage.BufferFirstIdx;
			AeonMemory::MemDestruct<ElementType>(ElemPtr);

			CheckDecreaseAllocation_Internal();

			--Storage.ElemCount;
			Storage.BufferFirstIdx
				= ((Storage.BufferFirstIdx + 1) < Storage.ElemCapacity) && (Storage.ElemCount > 0)
				? (Storage.BufferFirstIdx + 1)
				: 0;
		}

		ElementType& AddToBack(ConstElementType& NewElement)
		requires CCopyable<ElementType>
		{
			StorageType& Storage = Data.First();

			CheckIncreaseAllocation_Internal();

			IndexType BufferLastIdx = ComputeBufferIndex_Internal(Storage.ElemCount);

			++Storage.ElemCount;

			ElementType* ElemPtr = Storage.BufferPtr + BufferLastIdx;
			return *AeonMemory::MemConstruct<ElementType>(ElemPtr, NewElement);
		}
		ElementType& AddToBack(ElementType&& NewElement)
		requires CMoveable<ElementType>
		{
			StorageType& Storage = Data.First();

			CheckIncreaseAllocation_Internal();

			IndexType BufferLastIdx = ComputeBufferIndex_Internal(Storage.ElemCount);

			++Storage.ElemCount;

			ElementType* ElemPtr = Storage.BufferPtr + BufferLastIdx;
			return *AeonMemory::MemConstruct<ElementType>(ElemPtr, Move(NewElement));
		}

		template <class ... ArgumentTypes>
		ElementType& EmplaceInBack(ArgumentTypes&& ... Arguments)
		requires CConstructible<ElementType, ArgumentTypes ...>
		{
			StorageType& Storage = Data.First();

			CheckIncreaseAllocation_Internal();

			IndexType BufferLastIdx = ComputeBufferIndex_Internal(Storage.ElemCount);

			++Storage.ElemCount;

			ElementType* ElemPtr = Storage.BufferPtr + BufferLastIdx;
			return *AeonMemory::MemConstruct<ElementType>(ElemPtr, Forward<ArgumentTypes>(Arguments) ...);
		}

		void RemoveFromBack()
		{
			StorageType& Storage = Data.First();
			AEON_ASSERT(Storage.ElemCount != 0);

			IndexType BufferLastIdx = ComputeBufferIndex_Internal(Storage.ElemCount - 1);

			ElementType* ElemPtr = Storage.BufferPtr + BufferLastIdx;
			AeonMemory::MemDestruct<ElementType>(ElemPtr);

			--Storage.ElemCount;
			if (Storage.ElemCount == 0)
			{
				Storage.BufferFirstIdx = 0;
			}
		}
		void RemoveFromBack(TReclaimStorage)
		{
			StorageType& Storage = Data.First();
			AEON_ASSERT(Storage.ElemCount != 0);

			IndexType BufferLastIdx = ComputeBufferIndex_Internal(Storage.ElemCount - 1);

			ElementType* ElemPtr = Storage.BufferPtr + BufferLastIdx;
			AeonMemory::MemDestruct<ElementType>(ElemPtr);

			CheckDecreaseAllocation_Internal();

			--Storage.ElemCount;
			if (Storage.ElemCount == 0)
			{
				Storage.BufferFirstIdx = 0;
			}
		}

		ElementType& AddAt(IndexType Index, ConstElementType& NewElement)
		requires CCopyable<ElementType>
		{
			StorageType& Storage = Data.First();
			AEON_ASSERT(Index <= Storage.ElemCount);

			CheckIncreaseAllocation_Internal();
			RelocateAndOpenSlotAtIndex_Internal(Index);

			IndexType BlockFirstIdxDiff = Index;
			IndexType BlockLastIdxDiff = Storage.ElemCount++ - Index;
			if (BlockFirstIdxDiff < BlockLastIdxDiff)
			{
				Storage.BufferFirstIdx = (Storage.BufferFirstIdx > 0 ? Storage.BufferFirstIdx : Storage.ElemCapacity) - 1;
			}

			ElementType* ElemPtr = Storage.BufferPtr + ComputeBufferIndex_Internal(Index);
			return *AeonMemory::MemConstruct<ElementType>(ElemPtr, NewElement);
		}
		ElementType& AddAt(IndexType Index, ElementType&& NewElement)
		requires CMoveable<ElementType>
		{
			StorageType& Storage = Data.First();
			AEON_ASSERT(Index <= Storage.ElemCount);

			CheckIncreaseAllocation_Internal();
			RelocateAndOpenSlotAtIndex_Internal(Index);

			IndexType BlockFirstIdxDiff = Index;
			IndexType BlockLastIdxDiff = Storage.ElemCount++ - Index;
			if (BlockFirstIdxDiff < BlockLastIdxDiff)
			{
				Storage.BufferFirstIdx = (Storage.BufferFirstIdx > 0 ? Storage.BufferFirstIdx : Storage.ElemCapacity) - 1;
			}

			ElementType* ElemPtr = Storage.BufferPtr + ComputeBufferIndex_Internal(Index);
			return *AeonMemory::MemConstruct<ElementType>(ElemPtr, Move(NewElement));
		}

		template <class ... ArgumentTypes>
		ElementType& EmplaceAt(IndexType Index, ArgumentTypes&& ... Arguments)
		requires CConstructible<ElementType, ArgumentTypes ...>
		{
			StorageType& Storage = Data.First();
			AEON_ASSERT(Index <= Storage.ElemCount);

			CheckIncreaseAllocation_Internal();
			RelocateAndOpenSlotAtIndex_Internal(Index);

			IndexType BlockFirstIdxDiff = Index;
			IndexType BlockLastIdxDiff = Storage.ElemCount++ - Index;
			if (BlockFirstIdxDiff < BlockLastIdxDiff)
			{
				Storage.BufferFirstIdx = (Storage.BufferFirstIdx > 0 ? Storage.BufferFirstIdx : Storage.ElemCapacity) - 1;
			}

			ElementType* ElemPtr = Storage.BufferPtr + ComputeBufferIndex_Internal(Index);
			return *AeonMemory::MemConstruct<ElementType>(ElemPtr, Forward<ArgumentTypes>(Arguments) ...);
		}

		void RemoveAt(IndexType Index)
		{
			StorageType& Storage = Data.First();
			AEON_ASSERT(Index < Storage.ElemCount);

			ElementType* ElemPtr = Storage.BufferPtr + ComputeBufferIndex_Internal(Index);
			AeonMemory::MemDestruct<ElementType>(ElemPtr);

			RelocateAndCloseSlotAtIndex_Internal(Index);

			IndexType BlockFirstIdxDiff = Index;
			IndexType BlockLastIdxDiff = --Storage.ElemCount - Index;
			if (BlockFirstIdxDiff < BlockLastIdxDiff)
			{
				Storage.BufferFirstIdx = (Storage.BufferFirstIdx + 1) < Storage.ElemCapacity ? (Storage.BufferFirstIdx + 1) : 0;
			}

			if (Storage.ElemCount == 0)
			{
				Storage.BufferFirstIdx = 0;
			}
		}
		void RemoveAt(IndexType Index, TReclaimStorage)
		{
			StorageType& Storage = Data.First();
			AEON_ASSERT(Index < Storage.ElemCount);

			ElementType* ElemPtr = Storage.BufferPtr + ComputeBufferIndex_Internal(Index);
			AeonMemory::MemDestruct<ElementType>(ElemPtr);

			RelocateAndCloseSlotAtIndex_Internal(Index);
			CheckDecreaseAllocation_Internal();

			IndexType BlockFirstIdxDiff = Index;
			IndexType BlockLastIdxDiff = --Storage.ElemCount - Index;
			if (BlockFirstIdxDiff < BlockLastIdxDiff)
			{
				Storage.BufferFirstIdx = (Storage.BufferFirstIdx + 1) < Storage.ElemCapacity ? (Storage.BufferFirstIdx + 1) : 0;
			}

			if (Storage.ElemCount == 0)
			{
				Storage.BufferFirstIdx = 0;
			}
		}

		template <class PredicateType = GLessThan<ElementType>>
		ElementType& AddSorted(ConstElementType& NewElement, PredicateType&& Predicate = PredicateType{ })
		requires CCopyable<ElementType> && CCallableWithResult<PredicateType, bool, ConstElementType&, ConstElementType&>
		{
			auto Iter = std::lower_bound(cbegin(), cend(), NewElement, Predicate);
			auto Index = IndexType(Iter - cbegin());
			return AddAt(Index, NewElement);
		}
		template <class PredicateType = GLessThan<ElementType>>
		ElementType& AddSorted(ElementType&& NewElement, PredicateType&& Predicate = PredicateType{ })
		requires CMoveable<ElementType> && CCallableWithResult<PredicateType, bool, ConstElementType&, ConstElementType&>
		{
			auto Iter = std::lower_bound(cbegin(), cend(), NewElement, Predicate);
			auto Index = IndexType(Iter - cbegin());
			return AddAt(Index, Move(NewElement));
		}

		GOptionalRef<ElementType> AddUnique(ConstElementType& NewElement)
		requires CCopyable<ElementType>
		{
			if (!Contains(NewElement))
			{
				return GOptionalRef<ElementType>{ Add(NewElement) };
			}
			return GOptionalRef<ElementType>{};
		}
		GOptionalRef<ElementType> AddUnique(ElementType&& NewElement)
		requires CMoveable<ElementType>
		{
			if (!Contains(NewElement))
			{
				return GOptionalRef<ElementType>{ Add(Move(NewElement)) };
			}
			return GOptionalRef<ElementType>{};
		}
		template <class PredicateType>
		GOptionalRef<ElementType> AddUnique(ConstElementType& NewElement, PredicateType&& Predicate)
		requires CCopyable<ElementType> && CCallableWithResult<PredicateType, bool, ConstElementType&>
		{
			if (!Contains(Forward<PredicateType>(Predicate)))
			{
				return GOptionalRef<ElementType>{ Add(NewElement) };
			}
			return GOptionalRef<ElementType>{};
		}
		template <class PredicateType>
		GOptionalRef<ElementType> AddUnique(ElementType&& NewElement, PredicateType&& Predicate)
		requires CMoveable<ElementType> && CCallableWithResult<PredicateType, bool, ConstElementType&>
		{
			if (!Contains(Forward<PredicateType>(Predicate)))
			{
				return GOptionalRef<ElementType>{ Add(Move(NewElement)) };
			}
			return GOptionalRef<ElementType>{};
		}

		template <class PredicateType = GLessThan<ElementType>>
		GOptionalRef<ElementType> AddUniqueSorted(ConstElementType& NewElement, PredicateType&& Predicate = PredicateType{})
		requires CCopyable<ElementType> && CCallableWithResult<PredicateType, bool, ConstElementType&, ConstElementType&>
		{
			if (!ContainsSorted(Forward<PredicateType>(Predicate)))
			{
				return GOptionalRef<ElementType>{ AddSorted(NewElement) };
			}
			return GOptionalRef<ElementType>{};
		}
		template <class PredicateType = GLessThan<ElementType>>
		GOptionalRef<ElementType> AddUniqueSorted(ElementType&& NewElement, PredicateType&& Predicate = PredicateType{})
		requires CMoveable<ElementType> && CCallableWithResult<PredicateType, bool, ConstElementType&, ConstElementType&>
		{
			if (!ContainsSorted(Forward<PredicateType>(Predicate)))
			{
				return GOptionalRef<ElementType>{ AddSorted(Move(NewElement)) };
			}
			return GOptionalRef<ElementType>{};
		}

		bool Contains(ConstElementType& Value) const
		{
			return bool(Find(Value));
		}
		template <class PredicateType>
		bool Contains(PredicateType&& Predicate) const
		requires CCallableWithResult<PredicateType, bool, ConstElementType&>
		{
			return bool(Find(Forward<PredicateType>(Predicate)));
		}

		template <class PredicateType = GLessThan<ElementType>>
		bool ContainsSorted(ConstElementType& Value, PredicateType&& Predicate = PredicateType{}) const
		requires CCallableWithResult<PredicateType, bool, ConstElementType&, ConstElementType&>
		{
			return bool(FindSorted(Value, Forward<PredicateType>(Predicate)));
		}

		GOptional<IndexType> FindIndex(ConstElementType& Value) const
		{
			auto Iter = std::find(cbegin(), cend(), Value);
			if (Iter != cend())
			{
				return GOptional<IndexType>{ TConstructInPlace::Tag, IndexType(Iter - cbegin()) };
			}
			return GOptional<IndexType>{};
		}

		template <class PredicateType>
		GOptional<IndexType> FindIndex(PredicateType&& Predicate) const
		requires CCallableWithResult<PredicateType, bool, ConstElementType&>
		{
			auto Iter = std::find_if(cbegin(), cend(), Predicate);
			if (Iter != cend())
			{
				return GOptional<IndexType>{ TConstructInPlace::Tag, IndexType(Iter - cbegin()) };
			}
			return GOptional<IndexType>{};
		}

		template <class PredicateType = GLessThan<ElementType>>
		GOptional<IndexType> FindIndexSorted(ConstElementType& Value, PredicateType&& Predicate = PredicateType{}) const
		requires CCallableWithResult<PredicateType, bool, ConstElementType&, ConstElementType&>
		{
			auto Iter = std::lower_bound(cbegin(), cend(), Value, Predicate);
			if (Iter != cend())
			{
				ConstElementType& Element = *Iter;
				// Iter points to the first element for which Predicate(Element, Value) is false; if Predicate(Value, Element) is also false,
				// then Iter points to the element we want (with a few exceptions, e.g. floating-point NaN values)
				if (!Predicate(Value, Element))
				{
					return GOptional<IndexType>{ TConstructInPlace::Tag, IndexType(Iter - cbegin()) };
				}
			}
			return GOptional<IndexType>{};
		}

		GOptionalRef<ConstElementType> Find(ConstElementType& Value) const
		{
			auto Iter = std::find(cbegin(), cend(), Value);
			if (Iter != cend())
			{
				return { *Iter };
			}
			return {};
		}
		GOptionalRef<ElementType> Find(ConstElementType& Value)
		{
			return ZZINTERNAL_AEON_INVOKE_CONST_OPTIONALREF_OVERLOAD(Find, Value);
		}

		template <class PredicateType>
		GOptionalRef<ConstElementType> Find(PredicateType&& Predicate) const
		requires CCallableWithResult<PredicateType, bool, ConstElementType&>
		{
			auto Iter = std::find_if(cbegin(), cend(), Predicate);
			if (Iter != cend())
			{
				return { *Iter };
			}
			return {};
		}
		template <class PredicateType>
		GOptionalRef<ElementType> Find(PredicateType&& Predicate)
		requires CCallableWithResult<PredicateType, bool, ConstElementType&>
		{
			return ZZINTERNAL_AEON_INVOKE_CONST_OPTIONALREF_OVERLOAD(Find, Forward<PredicateType>(Predicate));
		}

		template <class PredicateType = GLessThan<ElementType>>
		GOptionalRef<ConstElementType> FindSorted(ConstElementType& Value, PredicateType&& Predicate = PredicateType{ }) const
		requires CCallableWithResult<PredicateType, bool, ConstElementType&, ConstElementType&>
		{
			auto Iter = std::lower_bound(cbegin(), cend(), Value, Predicate);
			if (Iter != cend())
			{
				ConstElementType& Element = *Iter;
				// Iter points to the first element for which Predicate(Element, Value) is false; if Predicate(Value, Element) is also false,
				// then Iter points to the element we want (with a few exceptions, e.g. floating-point NaN values)
				if (!Predicate(Value, Element))
				{
					return { Element };
				}
			}
			return {};
		}
		template <class PredicateType = GLessThan<ElementType>>
		GOptionalRef<ElementType> FindSorted(ConstElementType& Value, PredicateType&& Predicate = PredicateType{ })
		requires CCallableWithResult<PredicateType, bool, ConstElementType&, ConstElementType&>
		{
			return ZZINTERNAL_AEON_INVOKE_CONST_OPTIONALREF_OVERLOAD(FindSorted, Value, Forward<PredicateType>(Predicate));
		}

		template <class PredicateType = GLessThan<ElementType>>
		bool IsSorted(PredicateType&& Predicate = PredicateType{ }) const
		requires CCallableWithResult<PredicateType, bool, ConstElementType&, ConstElementType&>
		{
			return std::is_sorted(cbegin(), cend(), Predicate);
		}

		template <class PredicateType = GLessThan<ElementType>>
		GDearray& Sort(PredicateType&& Predicate = PredicateType{ })
		requires CCallableWithResult<PredicateType, bool, ConstElementType&, ConstElementType&>
		{
			std::sort(begin(), end(), Predicate);
			return *this;
		}

		template <class PredicateType = GLessThan<ElementType>>
		GDearray& StableSort(PredicateType&& Predicate = PredicateType{ })
		requires CCallableWithResult<PredicateType, bool, ConstElementType&, ConstElementType&>
		{
			std::stable_sort(begin(), end(), Predicate);
			return *this;
		}

		GDearray& Reverse()
		{
			std::reverse(begin(), end());
			return *this;
		}

		template <CInferableScalarObject MappedElementType = TInferTypeTag,
				  CInferableAllocator MappedAllocatorType = TInferTypeTag,
				  class PredicateType>
		auto Map(PredicateType&& Predicate) const
			-> TMapOperationResult<GDearray, ElementType, MappedElementType, AllocatorType, MappedAllocatorType, PredicateType>
		requires CCallable<PredicateType, ConstElementType&>
		{
			using MappedArrayType = TMapOperationResult<GDearray,
														ElementType, MappedElementType,
														AllocatorType, MappedAllocatorType,
														PredicateType>;

			StorageType const& Storage = Data.First();

			IndexType BufferIndex = Storage.BufferFirstIdx;
			IndexType BufferLastIndex = Storage.ElemCapacity - 1;

			MappedArrayType MappedArray{ TReserveStorage::Tag, Storage.ElemCount };
			for (IndexType Index = 0; Index < Storage.ElemCount; ++Index)
			{
				ConstElementType* ElemPtr = AeonMemory::MemAccess<ConstElementType>(Storage.BufferPtr + BufferIndex);
				MappedArray.EmplaceInBack(Predicate(*ElemPtr));

				BufferIndex = BufferIndex != BufferLastIndex ? BufferIndex + 1 : 0;
			}
			return MappedArray;
		}

		template <CInferableScalarObject FilteredElementType = TInferTypeTag,
				  CInferableAllocator FilteredAllocatorType = TInferTypeTag,
				  class PredicateType>
		auto Filter(PredicateType&& Predicate) const
			-> TFilterOperationResult<GDearray, ElementType, FilteredElementType, AllocatorType, FilteredAllocatorType>
		requires CCallable<PredicateType, ConstElementType&>
		{
			using FilteredArrayType = TFilterOperationResult<GDearray,
															 ElementType, FilteredElementType,
															 AllocatorType, FilteredAllocatorType>;

			StorageType const& Storage = Data.First();

			IndexType BufferIndex = Storage.BufferFirstIdx;
			IndexType BufferLastIndex = Storage.ElemCapacity - 1;

			FilteredArrayType FilteredArray{ TReserveStorage::Tag, Storage.ElemCount };
			for (IndexType Index = 0; Index < Storage.ElemCount; ++Index)
			{
				ConstElementType* ElemPtr = AeonMemory::MemAccess<ConstElementType>(Storage.BufferPtr + BufferIndex);
				if (Predicate(*ElemPtr))
				{
					FilteredArray.EmplaceInBack(*ElemPtr);
				}

				BufferIndex = BufferIndex != BufferLastIndex ? BufferIndex + 1 : 0;
			}
			FilteredArray.Prune();
			return FilteredArray;
		}

		template <CScalarObject FoldedElementType = ElementType,
				  class InitialElementType = ElementType,
				  class PredicateType>
		FoldedElementType Fold(PredicateType&& Predicate, InitialElementType&& InitialValue = InitialElementType{}) const
		requires CCallableWithResult<PredicateType, ElementType, ConstElementType&, ConstElementType&>
				 && CConstructible<ElementType, InitialElementType>
				 && CConvertibleClass<ElementType, FoldedElementType>
		{
			StorageType const& Storage = Data.First();

			IndexType BufferIndex = Storage.BufferFirstIdx;
			IndexType BufferLastIndex = Storage.ElemCapacity - 1;

			// Note: we'll implicitly convert the type to FoldedElementType in the return statement
			ElementType FoldedElement{ Forward<InitialElementType>(InitialValue) };
			for (IndexType Index = 0; Index < Storage.ElemCount; ++Index)
			{
				ConstElementType* ElemPtr = AeonMemory::MemAccess<ConstElementType>(Storage.BufferPtr + BufferIndex);
				FoldedElement = Predicate(FoldedElement, *ElemPtr);

				BufferIndex = BufferIndex != BufferLastIndex ? BufferIndex + 1 : 0;
			}
			return FoldedElement;
		}

		ConstElementType& Front() const
		{
			StorageType const& Storage = Data.First();
			AEON_ASSERT(Storage.ElemCount != 0);
			return *AeonMemory::MemAccess<ConstElementType>(Storage.BufferPtr + Storage.BufferFirstIdx);
		}
		ElementType& Front()
		{
			return AEON_INVOKE_CONST_OVERLOAD(Front);
		}

		ConstElementType& Back() const
		{
			StorageType const& Storage = Data.First();
			AEON_ASSERT(Storage.ElemCount != 0);
			return *AeonMemory::MemAccess<ConstElementType>(Storage.BufferPtr + ComputeBufferIndex_Internal(Storage.ElemCount - 1));
		}
		ElementType& Back()
		{
			return AEON_INVOKE_CONST_OVERLOAD(Back);
		}

		ConstElementType* Buffer() const
		{
			return Data.First().BufferPtr;
		}
		ElementType* Buffer()
		{
			return AEON_INVOKE_CONST_OVERLOAD(Buffer);
		}

		IndexType AllocatedCount() const
		{
			return Data.First().ElemCapacity;
		}

		IndexType Count() const
		{
			return Data.First().ElemCount;
		}

		bool IsEmpty() const
		{
			return Count() == 0;
		}

		AllocatorType const& GetAllocator() const
		{
			return Data.Second();
		}

		ConstElementType& operator[](IndexType Index) const
		{
			StorageType const& Storage = Data.First();
			AEON_ASSERT(Index < Storage.ElemCount);
			return *AeonMemory::MemAccess<ConstElementType>(Storage.BufferPtr + ComputeBufferIndex_Internal(Index));
		}
		ElementType& operator[](IndexType Index)
		{
			return AEON_INVOKE_CONST_OVERLOAD(operator[], Index);
		}

		voidptr DataBuffer_Internal(szint BufferIndex) const
		{
			AEON_ASSERT(BufferIndex == 0);
			return Data.First().BufferPtr;
		}
		szint DataBufferCount_Internal() const
		{
			return 1;
		}
		szint DataElementSize_Internal(szint BufferIndex) const
		{
			AEON_ASSERT(BufferIndex == 0);
			return Aeon::SizeOf(ElementType);
		}
		szint DataElementCount_Internal() const
		{
			return Count();
		}
		szint DataElementCapacity_Internal() const
		{
			return AllocatedCount();
		}

		AEON_FORCEINLINE ConstIteratorType begin() const
		{
			StorageType const& Storage = Data.First();
			return ConstIteratorType{ this, 0, Storage.BufferFirstIdx };
		}
		AEON_FORCEINLINE IteratorType begin()
		{
			StorageType const& Storage = Data.First();
			return IteratorType{ this, 0, Storage.BufferFirstIdx };
		}
		AEON_FORCEINLINE ConstIteratorType cbegin() const
		{
			return begin();
		}

		AEON_FORCEINLINE ConstIteratorType end() const
		{
			StorageType const& Storage = Data.First();
			return ConstIteratorType{ this, Storage.ElemCount, ComputeBufferIndex_Internal(Storage.ElemCount) };
		}
		AEON_FORCEINLINE IteratorType end()
		{
			StorageType const& Storage = Data.First();
			return IteratorType{ this, Storage.ElemCount, ComputeBufferIndex_Internal(Storage.ElemCount) };
		}
		AEON_FORCEINLINE ConstIteratorType cend() const
		{
			return end();
		}

		friend void Swap(GDearray& Array1, GDearray& Array2)
		requires CSwappableResourceAllocator<AllocatorType>
		{
			StorageType& Storage1 = Array1.Data.First();
			StorageType& Storage2 = Array2.Data.First();

			AllocatorType& Allocator1 = Array1.Data.Second();
			AllocatorType& Allocator2 = Array2.Data.Second();

			using Aeon::Swap;

			Swap(Storage1, Storage2);
			Swap(Allocator1, Allocator2);
		}

	private:
		AEON_FORCEINLINE IndexType ComputeBufferIndex_Internal(IndexType ElementIndex) const
		{

			StorageType const& Storage = Data.First();

		//	// NOTE Most of the time,
		//	//  iteration uses 'ElemBufferIdx = ElemBufferIdx != BufferLastIdx ? ElemBufferIdx + 1 : 0' (or some variant thereof)
		//	//  instead of calling this, because branch prediction will almost always be faster than the extra AND operation,
		//	//  especially for very big arrays.
		//	return AeonMath::ModPowerOfTwo(Storage.BufferFirstIdx + ElementIndex, Storage.ElemCapacity);

			IndexType BufferIndex = Storage.BufferFirstIdx + ElementIndex;
			if (BufferIndex >= Storage.ElemCapacity)
			{
				BufferIndex -= Storage.ElemCapacity;
			}
			return BufferIndex;
		}

		void Allocate_Internal(IndexType InitialElemCapacity)
		{
			StorageType& Storage = Data.First();
			AllocatorType& Allocator = Data.Second();

			// NOTE AllocatedCount is always adjusted to be a power of two to improve performance with ComputeBufferIndex_Internal
		//	InitialElemCapacity = AeonMath::NextPowerOfTwo(InitialElemCapacity);

			AEON_ASSERT(Allocator.IsAllocationSizeSupported(InitialElemCapacity));

			ElementPtrType InitialBufferPtr = Allocator.Allocate(InitialElemCapacity);

			AEON_ASSERT(InitialBufferPtr || (InitialElemCapacity == 0));

			Storage.BufferPtr = InitialBufferPtr;
			Storage.ElemCapacity = InitialElemCapacity;
		}
		void Reallocate_Internal(IndexType NewElemCapacity)
		{
			AllocatorType& Allocator = Data.Second();
			StorageType& Storage = Data.First();

		//	NewElemCapacity = AeonMath::NextPowerOfTwo(NewElemCapacity);

			AEON_ASSERT(Allocator.IsAllocationSizeSupported(NewElemCapacity));

			ElementPtrType NewBufferPtr = Allocator.Reallocate(Storage.BufferPtr, Storage.ElemCapacity, NewElemCapacity,
															   &GDearray::RelocateElements_Internal, this);

			AEON_ASSERT(NewBufferPtr || (NewElemCapacity == 0));

			Storage.BufferPtr = NewBufferPtr;
			Storage.ElemCapacity = NewElemCapacity;
		}
		void Deallocate_Internal()
		{
			StorageType& Storage = Data.First();
			AllocatorType& Allocator = Data.Second();

			if (Storage.BufferPtr)
			{
				Allocator.Deallocate(Storage.BufferPtr);

				Storage.BufferPtr = nullptr;
				Storage.ElemCapacity = 0;
			}
		}

		void Construct_Internal(IndexType NewElemCount)
		{
			StorageType& Storage = Data.First();

			IndexType OldElemCount = Exchange(Storage.ElemCount, NewElemCount);

			IndexType BufferIndex = ComputeBufferIndex_Internal(OldElemCount);
			IndexType BufferLastIndex = Storage.ElemCapacity - 1;

			for (IndexType Index = OldElemCount; Index < NewElemCount; ++Index)
			{
				AeonMemory::MemConstruct<ElementType>(Storage.BufferPtr + BufferIndex);

				BufferIndex = BufferIndex != BufferLastIndex ? BufferIndex + 1 : 0;
			}
		}
		void ConstructFromValue_Internal(IndexType NewElemCount, ConstElementType& Element)
		{
			StorageType& Storage = Data.First();

			IndexType OldElemCount = Exchange(Storage.ElemCount, NewElemCount);

			IndexType BufferIndex = ComputeBufferIndex_Internal(OldElemCount);
			IndexType BufferLastIndex = Storage.ElemCapacity - 1;

			for (IndexType Index = OldElemCount; Index < NewElemCount; ++Index)
			{
				AeonMemory::MemConstruct<ElementType>(Storage.BufferPtr + BufferIndex, Element);

				BufferIndex = BufferIndex != BufferLastIndex ? BufferIndex + 1 : 0;
			}
		}
		template <class NewElementType>
		void ConstructFromInitList_Internal(IndexType NewElemCount, NewElementType const* ElemPtr)
		requires CSameClass<NewElementType, TDecayType<ElementType>>
		    	 || CSameClass<NewElementType, GRvalRefWrapper<TDecayType<ElementType>>>
		{
			// Since this is only ever invoked by a constructor, we can assume BufferFirstIdx == 0

			StorageType& Storage = Data.First();

			Storage.ElemCount = NewElemCount;
			if constexpr (VIsTriviallyCopyable<NewElementType>)
			{
				IndexType BufferElemSize = NewElemCount * ElementSize;

				AeonMemory::MemCopy(Storage.BufferPtr, ElemPtr, BufferElemSize);
			}
			else
			{
				for (IndexType Index = 0; Index < NewElemCount; ++Index)
				{
					AeonMemory::MemConstruct<ElementType>(Storage.BufferPtr + Index, MoveElseCopy(*ElemPtr++));
				}
			}
		}

		void AppendCopyFromArray_Internal(GDearray const& Other)
		{
			StorageType& Storage = Data.First();
			StorageType const& OtherStorage = Other.Data.First();

			IndexType BufferIndex = ComputeBufferIndex_Internal(Storage.ElemCount);
			IndexType BufferLastIndex = Storage.ElemCapacity - 1;

			IndexType OtherBufferIndex = OtherStorage.BufferFirstIdx;
			IndexType OtherBufferLastIndex = OtherStorage.ElemCapacity - 1;

			IndexType OtherElemCount = OtherStorage.ElemCount;

			for (IndexType Index = 0; Index < OtherElemCount; ++Index)
			{
				ElementType* ElemPtr = Storage.BufferPtr + BufferIndex;
				ConstElementType* OtherElemPtr = AeonMemory::MemAccess<ConstElementType>(OtherStorage.BufferPtr + OtherBufferIndex);

				AeonMemory::MemConstruct<ElementType>(ElemPtr, *OtherElemPtr);

				BufferIndex = BufferIndex != BufferLastIndex ? BufferIndex + 1 : 0;
				OtherBufferIndex = OtherBufferIndex != OtherBufferLastIndex ? OtherBufferIndex + 1 : 0;
			}

			Storage.ElemCount += OtherElemCount;
		}
		void AppendMoveFromArray_Internal(GDearray& Other)
		{
			StorageType& Storage = Data.First();
			StorageType& OtherStorage = Other.Data.First();

			IndexType BufferIndex = ComputeBufferIndex_Internal(Storage.ElemCount);
			IndexType BufferLastIndex = Storage.ElemCapacity - 1;

			IndexType OtherBufferIndex = OtherStorage.BufferFirstIdx;
			IndexType OtherBufferLastIndex = OtherStorage.ElemCapacity - 1;

			IndexType OtherElemCount = OtherStorage.ElemCount;

			for (IndexType Index = 0; Index < OtherElemCount; ++Index)
			{
				ElementType* ElemPtr = Storage.BufferPtr + BufferIndex;
				ElementType* OtherElemPtr = AeonMemory::MemAccess<ElementType>(OtherStorage.BufferPtr + OtherBufferIndex);

				AeonMemory::MemConstruct<ElementType>(ElemPtr, Move(*OtherElemPtr));
				AeonMemory::MemDestruct<ElementType>(OtherElemPtr);

				BufferIndex = BufferIndex != BufferLastIndex ? BufferIndex + 1 : 0;
				OtherBufferIndex = OtherBufferIndex != OtherBufferLastIndex ? OtherBufferIndex + 1 : 0;
			}

			Storage.ElemCount += OtherElemCount;
			OtherStorage.ElemCount = 0;
		}

		void Destruct_Internal(IndexType NewElemCount)
		{
			StorageType& Storage = Data.First();

			IndexType OldElemCount = Exchange(Storage.ElemCount, NewElemCount);

			if constexpr (!VIsTriviallyDestructible<ElementType>)
			{
				IndexType BufferIndex = ComputeBufferIndex_Internal(NewElemCount);
				IndexType BufferLastIndex = Storage.ElemCapacity - 1;

				for (IndexType Index = NewElemCount; Index < OldElemCount; ++Index)
				{
					AeonMemory::MemDestruct<ElementType>(Storage.BufferPtr + BufferIndex);

					BufferIndex = BufferIndex != BufferLastIndex ? BufferIndex + 1 : 0;
				}
			}

			// Also reset the index to the first element if all elements have been destructed
			if (Storage.ElemCount == 0)
			{
				Storage.BufferFirstIdx = 0;
			}
		}

		void CheckIncreaseAllocation_Internal()
		{
			StorageType const& Storage = Data.First();
			AllocatorType const& Allocator = Data.Second();

			IndexType OldElemCapacity = Storage.ElemCapacity;
			if (Allocator.ShouldIncreaseAllocationSize(OldElemCapacity, Storage.ElemCount + 1))
			{
				IndexType NewElemCapacity = Allocator.ComputeAllocationSizeIncrease(OldElemCapacity);
				Reallocate_Internal(NewElemCapacity);
			}
		}
		void CheckIncreaseAllocation_Internal(IndexType NewElemCount)
		{
			StorageType const& Storage = Data.First();
			AllocatorType const& Allocator = Data.Second();

			if (Allocator.ShouldIncreaseAllocationSize(Storage.ElemCapacity, NewElemCount))
			{
				Reallocate_Internal(NewElemCount);
			}
		}

		void CheckDecreaseAllocation_Internal()
		{
			StorageType const& Storage = Data.First();
			AllocatorType const& Allocator = Data.Second();

			IndexType OldElemCapacity = Storage.ElemCapacity;
			if (Allocator.ShouldDecreaseAllocationSize(OldElemCapacity, Storage.ElemCount - 1))
			{
				IndexType NewElemCapacity = Allocator.ComputeAllocationSizeDecrease(OldElemCapacity);
				Reallocate_Internal(NewElemCapacity);
			}
		}
		void CheckDecreaseAllocation_Internal(IndexType NewElemCount)
		{
			StorageType const& Storage = Data.First();
			AllocatorType const& Allocator = Data.Second();

			if (Allocator.ShouldDecreaseAllocationSize(Storage.ElemCapacity, NewElemCount))
			{
				// Don't reduce more than the minimal allocation
				NewElemCount = AeonMath::Max(NewElemCount, Allocator.RecommendedStartingAllocationSize());
				Reallocate_Internal(NewElemCount);
			}
		}

		void RelocateAndOpenSlotAtIndex_Internal(IndexType ElementIndex)
		{
			// Moves elements, assuming ElementIndex refers to an existing element,
			// so as to empty it ("opening the slot"), moving the element to a different slot,
			// and making the array discontiguous.

			StorageType& Storage = Data.First();

			// ElementIndex may be equal to NewLastIndex, in which case either branch is a no-op.
			// Assumes this refers to the last element index with the empty element.
			IndexType BufferElemLastIdx = Storage.ElemCount;
			IndexType BufferLastIdx = Storage.ElemCapacity - 1;

			bool ShouldInvertRelocationIter = ElementIndex < (BufferElemLastIdx - ElementIndex);
			if (ShouldInvertRelocationIter)
			{
				IndexType OldBufferIndex = Storage.BufferFirstIdx;
				IndexType NewBufferIndex = OldBufferIndex != 0 ? OldBufferIndex - 1 : BufferLastIdx;

				for (IndexType Index = 0; Index < ElementIndex; ++Index)
				{
					ElementType* OldElemPtr = Storage.BufferPtr + OldBufferIndex;
					ElementType* NewElemPtr = Storage.BufferPtr + NewBufferIndex;

					ElementType* RelocateElemPtr = AeonMemory::MemAccess<ElementType>(OldElemPtr);
					AeonMemory::MemConstruct<ElementType>(NewElemPtr, MoveElseCopy(*RelocateElemPtr));
					AeonMemory::MemDestruct<ElementType>(RelocateElemPtr);

					NewBufferIndex = OldBufferIndex;
					OldBufferIndex = OldBufferIndex != BufferLastIdx ? OldBufferIndex + 1 : 0;
				}
			}
			else
			{
				IndexType OldBufferIndex = ComputeBufferIndex_Internal(BufferElemLastIdx - 1);
				IndexType NewBufferIndex = OldBufferIndex != BufferLastIdx ? OldBufferIndex + 1 : 0;

				for (IndexType Index = BufferElemLastIdx; Index > ElementIndex; --Index)
				{
					ElementType* OldElemPtr = Storage.BufferPtr + OldBufferIndex;
					ElementType* NewElemPtr = Storage.BufferPtr + NewBufferIndex;

					ElementType* RelocateElemPtr = AeonMemory::MemAccess<ElementType>(OldElemPtr);
					AeonMemory::MemConstruct<ElementType>(NewElemPtr, MoveElseCopy(*RelocateElemPtr));
					AeonMemory::MemDestruct<ElementType>(RelocateElemPtr);

					NewBufferIndex = OldBufferIndex;
					OldBufferIndex = OldBufferIndex != 0 ? OldBufferIndex - 1 : BufferLastIdx;
				}
			}
		}
		void RelocateAndCloseSlotAtIndex_Internal(IndexType ElementIndex)
		{
			// Moves elements, assuming ElementIndex refers to an empty slot,
			// so as to fill it ("closing the slot") with an existing element,
			// and making the array contiguous.
			
			StorageType& Storage = Data.First();

			// ElementIndex may be equal to NewLastIndex, in which case either branch is a no-op.
			// Assumes this refers to the last element index with the empty element.
			IndexType BufferElemLastIdx = Storage.ElemCount - 1;
			IndexType BufferLastIdx = Storage.ElemCapacity - 1;

			bool ShouldInvertRelocationIter = ElementIndex < (BufferElemLastIdx - ElementIndex);
			if (ShouldInvertRelocationIter)
			{
				IndexType NewBufferIndex = ComputeBufferIndex_Internal(ElementIndex);
				IndexType OldBufferIndex = NewBufferIndex != 0 ? NewBufferIndex - 1 : BufferLastIdx;

				for (IndexType Index = ElementIndex; Index > 0; --Index)
				{
					ElementType* NewElemPtr = Storage.BufferPtr + NewBufferIndex;
					ElementType* OldElemPtr = Storage.BufferPtr + OldBufferIndex;

					ElementType* RelocateElemPtr = AeonMemory::MemAccess<ElementType>(OldElemPtr);
					AeonMemory::MemConstruct<ElementType>(NewElemPtr, MoveElseCopy(*RelocateElemPtr));
					AeonMemory::MemDestruct<ElementType>(RelocateElemPtr);

					NewBufferIndex = OldBufferIndex;
					OldBufferIndex = OldBufferIndex != 0 ? OldBufferIndex - 1 : BufferLastIdx;
				}
			}
			else
			{
				IndexType NewBufferIndex = ComputeBufferIndex_Internal(ElementIndex);
				IndexType OldBufferIndex = NewBufferIndex != BufferLastIdx ? NewBufferIndex + 1 : 0;

				for (IndexType Index = ElementIndex; Index < BufferElemLastIdx; ++Index)
				{
					ElementType* NewElemPtr = Storage.BufferPtr + NewBufferIndex;
					ElementType* OldElemPtr = Storage.BufferPtr + OldBufferIndex;

					ElementType* RelocateElemPtr = AeonMemory::MemAccess<ElementType>(OldElemPtr);
					AeonMemory::MemConstruct<ElementType>(NewElemPtr, MoveElseCopy(*RelocateElemPtr));
					AeonMemory::MemDestruct<ElementType>(RelocateElemPtr);

					NewBufferIndex = OldBufferIndex;
					OldBufferIndex = OldBufferIndex != BufferLastIdx ? OldBufferIndex + 1 : 0;
				}
			}
		}

		static void RelocateElements_Internal(ElementPtrType OldStoragePtr, IndexType OldStorageCount,
											  ElementPtrType NewStoragePtr, IndexType NewStorageCount,
											  voidptr InstanceData)
		{
			AEON_UNUSED(OldStorageCount, NewStorageCount);

			auto InstancePtr = static_cast<GDearray*>(InstanceData);

			StorageType& Storage = InstancePtr->Data.First();

			IndexType ElemCount = Storage.ElemCount;

			IndexType OldBufferLastIdx = OldStorageCount - 1;
			IndexType NewBufferLastIdx = NewStorageCount - 1;

			IndexType ElemFirstIdx = Storage.BufferFirstIdx;
			IndexType ElemLastIdx = InstancePtr->ComputeBufferIndex_Internal(Storage.ElemCount - 1);

			diffint BufferDiff = diffint(NewStorageCount) - diffint(OldStorageCount);

			bool ShouldInvertRelocationIter = (OldStoragePtr < NewStoragePtr && NewStoragePtr < OldStoragePtr + OldStorageCount
											   && ElemFirstIdx < ElemLastIdx)
										   || (NewStoragePtr < OldStoragePtr && OldStoragePtr < NewStoragePtr + NewStorageCount
											   && ElemLastIdx < ElemFirstIdx);
			if (ShouldInvertRelocationIter)
			{
				IndexType OldStorageIdx = ElemLastIdx;
				IndexType NewStorageIdx = ElemFirstIdx < ElemLastIdx ? IndexType(ElemLastIdx + BufferDiff) : ElemLastIdx;

				for (IndexType Index = ElemCount; Index > 0; --Index)
				{
					ElementType* OldElemPtr = OldStoragePtr + OldStorageIdx;
					ElementType* NewElemPtr = NewStoragePtr + NewStorageIdx;

					ElementType* RelocateElemPtr = AeonMemory::MemAccess<ElementType>(OldElemPtr);
					AeonMemory::MemConstruct<ElementType>(NewElemPtr, MoveElseCopy(*RelocateElemPtr));
					AeonMemory::MemDestruct<ElementType>(RelocateElemPtr);

					OldStorageIdx = OldStorageIdx != 0 ? OldStorageIdx - 1 : OldBufferLastIdx;
					NewStorageIdx = NewStorageIdx != 0 ? NewStorageIdx - 1 : NewBufferLastIdx;
				}
			}
			else
			{
				IndexType OldStorageIdx = ElemFirstIdx;
				IndexType NewStorageIdx = ElemFirstIdx > ElemLastIdx ? IndexType(ElemFirstIdx + BufferDiff) : ElemFirstIdx;

				for (IndexType Index = 0; Index < ElemCount; ++Index)
				{
					ElementType* OldElemPtr = OldStoragePtr + OldStorageIdx;
					ElementType* NewElemPtr = NewStoragePtr + NewStorageIdx;

					ElementType* RelocateElemPtr = AeonMemory::MemAccess<ElementType>(OldElemPtr);
					AeonMemory::MemConstruct<ElementType>(NewElemPtr, MoveElseCopy(*RelocateElemPtr));
					AeonMemory::MemDestruct<ElementType>(RelocateElemPtr);

					OldStorageIdx = OldStorageIdx != OldBufferLastIdx ? OldStorageIdx + 1 : 0;
					NewStorageIdx = NewStorageIdx != NewBufferLastIdx ? NewStorageIdx + 1 : 0;
				}
			}
		}

		struct DDataStorage
		{
			ElementPtrType BufferPtr = nullptr; // Pointer to the bytes of the block of memory
			IndexType BufferFirstIdx = 0; // Index of the first element relative to the buffer
			IndexType ElemCount = 0; // Number of created elements
			IndexType ElemCapacity = 0; // Number of elements supported by allocated storage
		};

		using StorageType = DDataStorage;

		GCompressedDuple<StorageType, AllocatorType> Data;

		static constexpr szint ElementSize = Aeon::SizeOf(ElementType);
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GStaticDearray

	template <CScalarObject ElemType, szint ElemCountValue>
	using GStaticDearray = GDearray<ElemType, GStaticAllocator<ElemType, ElemCountValue>>;
}

#endif
