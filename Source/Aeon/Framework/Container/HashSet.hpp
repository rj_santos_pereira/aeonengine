#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_HASHSET
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_HASHSET

#include "Aeon/Framework/Core.hpp"

// TODO MapCommon
#include "Aeon/Framework/Allocator/DynamicAllocator.hpp"
#include "Aeon/Framework/Container/HashTable.hpp"

#pragma clang diagnostic push
#pragma ide diagnostic ignored "HidingNonVirtualFunction"

namespace Aeon::ZzInternal
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GHashSetElementAdapter

	template <CScalarObject ElemType>
	struct GHashSetElementAdapter
	{
		using ElementType = ElemType;
		using ConstElementType = TAddConst<ElemType>;

		using KeyType = ElemType;
		using ConstKeyType = TAddConst<ElemType>;

		// Add, subscript operator
		static ElementType* Construct(ElementType* ElemPtr, ElemType const& Key)
		{
			return AeonMemory::MemConstruct<ElementType>(ElemPtr, Key);
		}
		static ElementType* Construct(ElementType* ElemPtr, ElemType&& Key)
		{
			return AeonMemory::MemConstruct<ElementType>(ElemPtr, Move(Key));
		}

		// Emplace
		template <class ... KeyArgTypes>
		static ElementType* Construct(ElementType* ElemPtr, TConstructInPlace, KeyArgTypes&& ... KeyArgs)
		requires CConstructible<KeyType, KeyArgTypes ...>
		{
			return AeonMemory::MemConstruct<ElementType>(ElemPtr, Forward<KeyArgTypes>(KeyArgs)...);
		}

		// Copy/move constructors and assignment operators
		static ElementType* Construct(ElementType* ElemPtr, ConstElementType* OtherElemPtr)
		{
			return AeonMemory::MemConstruct<ElementType>(ElemPtr,
														 Copy(*OtherElemPtr));
		}
		static ElementType* Construct(ElementType* ElemPtr, ElementType* OtherElemPtr)
		{
			return AeonMemory::MemConstruct<ElementType>(ElemPtr,
														 Move(*OtherElemPtr));
		}

		// Rehash
		static ElementType* Construct(ElementType* ElemPtr, THashTableRehashElement, ElementType* OtherElemPtr)
		{
			return AeonMemory::MemConstruct<ElementType>(ElemPtr,
														 MoveElseCopy(*OtherElemPtr));
		}

		static void Destruct(ElementType* ElemPtr)
		{
			AeonMemory::MemDestruct<ElementType>(ElemPtr);
		}

		static ConstKeyType* AccessKey(ConstElementType* ElemPtr)
		{
			return AeonMemory::MemAccess<ConstElementType>(ElemPtr);
		}

		static ConstElementType* AccessElement(ConstElementType* ElemPtr)
		{
			return AeonMemory::MemAccess<ConstElementType>(ElemPtr);
		}
		static ElementType* AccessElement(ElementType* ElemPtr)
		{
			return AeonMemory::MemAccess<ElementType>(ElemPtr);
		}
	};
}

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GHashSet

	using DHashSetParams = ZzInternal::DHashTableParams;

	template <CScalarObject ElemType,
	    	  CObjectHasherForObject<TDecayType<ElemType>> ElemHasherType = GDefaultObjectHasher<ElemType>,
			  CAllocatorForObject<TDecayType<ElemType>> ElemAllocType = GDynamicAllocator<ElemType>>
	class GHashSet : protected ZzInternal::GHashTable<ZzInternal::GHashSetElementAdapter<ElemType>, ElemHasherType, ElemAllocType>
	{
		using Base = ZzInternal::GHashTable<ZzInternal::GHashSetElementAdapter<ElemType>, ElemHasherType, ElemAllocType>;

	public:
		using KeyType = ElemType;

		using ElementType = TAddConst<ElemType>; // NOTE HashSet elements are not modifiable
		using ConstElementType = TAddConst<ElemType>;

		using HasherType = Base::HasherType;
		using AllocatorType = Base::AllocatorType;

		using HashType = Base::HashType;
		using IndexType = Base::IndexType;

		using IteratorType = Base::ConstIteratorType; // NOTE HashSet elements are not modifiable
		using ConstIteratorType = Base::ConstIteratorType;

		template <class NewAllocatorType = AllocatorType>
		explicit GHashSet(NewAllocatorType&& NewAllocator = NewAllocatorType{})
		requires CSameClass<TDecayType<NewAllocatorType>, AllocatorType>
			: Base(Forward<NewAllocatorType>(NewAllocator))
		{
		}
		template <class NewAllocatorType = AllocatorType>
		explicit GHashSet(DHashSetParams const& Params, NewAllocatorType&& NewAllocator = NewAllocatorType{})
		requires CSameClass<TDecayType<NewAllocatorType>, AllocatorType>
			: Base(Params, Forward<NewAllocatorType>(NewAllocator))
		{
		}

		GHashSet(InitList<ElementType> InitElemList)
		requires CCopyable<ElementType>
			: Base(InitElemList)
		{
		}
		GHashSet(RvalRefInitList<ElementType> InitElemList)
		requires CMoveable<ElementType>
			: Base(InitElemList)
		{
		}

		GHashSet(GHashSet const& Other)
		requires CCopyable<ElementType>
			: Base(Other)
		{
		}
		GHashSet(GHashSet&& Other)
		requires CMoveable<ElementType> || CSwappableResourceAllocator<AllocatorType>
			: Base(Move(Other))
		{
		}

		GHashSet& operator=(GHashSet const& Other)
		requires CCopyable<ElementType>
		{
			Base::operator=(Other);
			return *this;
		}
		GHashSet& operator=(GHashSet&& Other)
		requires CMoveable<ElementType> || CSwappableResourceAllocator<AllocatorType>
		{
			Base::operator=(Move(Other));
			return *this;
		}

		using Base::Reserve;
		using Base::Prune;

		using Base::Empty;
		using Base::Append;

		// NOTE "Override" Add, Emplace, FindElseAdd, and Find to return a const lval-ref instead,
		//  as the elements in a set are not mutable (allowing mutation would potentially break hashing).

		ConstElementType& Add(KeyType const& Key)
		requires CCopyable<KeyType>
		{
			return Base::AddGeneric(Key);
		}
		ConstElementType& Add(KeyType&& Key)
		requires CMoveable<KeyType>
		{
			return Base::AddGeneric(Move(Key));
		}

		template <class ... KeyArgTypes>
		ConstElementType& Emplace(KeyArgTypes&&... KeyArgs)
		requires CConstructible<KeyType, KeyArgTypes ...> && CMoveableElseCopyable<KeyType>
		{
			return Base::AddGeneric(KeyType{ Forward<KeyArgTypes>(KeyArgs) ... });
		}

		ConstElementType& FindElseAdd(KeyType const& Key)
		requires CCopyable<KeyType>
		{
			return Base::FindElseAddGeneric(Key);
		}
		ConstElementType& FindElseAdd(KeyType&& Key)
		requires CMoveable<KeyType>
		{
			return Base::FindElseAddGeneric(Move(Key));
		}

		using Base::Remove;

		using Base::Contains;

		GOptionalRef<ConstElementType> Find(KeyType const& Key) const
		{
			return Base::Find(Key);
		}

		using Base::Relink;

		using Base::GetLoadFactor;
		using Base::GetLoadFactorLimit;
		using Base::SetLoadFactorLimit;

		ConstElementType* Buffer() const
		{
			return Base::Buffer();
		}

		using Base::AllocatedCount;
		using Base::Count;

		using Base::IsEmpty;

		using Base::GetAllocator;

		ElementType const& operator[](KeyType const& Key) const
		{
			AEON_ASSERT(Contains(Key));
			return *Find(Key);
		}

		using Base::cbegin;
		using Base::begin;
		using Base::cend;
		using Base::end;

		friend void Swap(GHashSet& Set1, GHashSet& Set2)
		requires CSwappableResourceAllocator<AllocatorType>
		{
			using Aeon::Swap;

			Swap(static_cast<Base&>(Set1), static_cast<Base&>(Set2));
		}
	};
}

#pragma clang diagnostic pop

#endif
