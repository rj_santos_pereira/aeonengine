#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_ITERATOR
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_ITERATOR

#include "Aeon/Framework/Core.hpp"

namespace Aeon
{
	// TODO maybe remove this
#if 0
	template <class Type>
	concept CIterator = requires
	{
		typename Type::PointerType;
		requires CObjectPointer<typename Type::PointerType>;

		typename Type::iterator_concept;
	//	requires CBaseClass<std::forward_iterator_tag, typename Type::iterator_concept>
	//		  || CBaseClass<std::output_iterator_tag, typename Type::iterator_concept>;

		requires CBaseClass<IIterator<TRemovePointer<typename Type::PointerType>, typename Type::iterator_concept>, Type>;
	};
#endif

	template <class ElementType, class IteratorTagType>
//	requires CBaseClass<std::input_iterator_tag, IteratorTagType> || CBaseClass<std::output_iterator_tag, IteratorTagType>
	requires CBaseClass<std::forward_iterator_tag, IteratorTagType>
	class IIterator
	{
	protected:
		~IIterator() = default;

		// These aliases are defined for compatibility with the language;
		// the iterators model the language's concepts closely but not exactly
		using iterator_concept = IteratorTagType;
		using iterator_category = IteratorTagType;

		using difference_type = diffint;

		using value_type = TRemoveConstVolatile<ElementType>;
		using pointer = TAddPointer<ElementType>;
		using reference = TAddLvalueReference<ElementType>;

		using IndexType = difference_type;

		using ValueType = value_type;
		using PointerType = pointer;
		using ReferenceType = reference;
		using ConstPointerType = TAddPointer<TAddConst<value_type>>;
		using ConstReferenceType = TAddLvalueReference<TAddConst<value_type>>;
	};

#define AEON_DECLARE_ITERATOR_TRAITS(Type) \
    using iterator_concept		= typename Type ::IteratorTraits::iterator_concept;                                                                  \
    using iterator_category		= typename Type ::IteratorTraits::iterator_category;                                                                 \
    using difference_type		= typename Type ::IteratorTraits::difference_type;                                                                   \
    using value_type			= typename Type ::IteratorTraits::value_type;                                                                        \
    using pointer				= typename Type ::IteratorTraits::pointer;                                                                           \
    using reference				= typename Type ::IteratorTraits::reference;                                                                         \
    using IndexType				= typename Type ::IteratorTraits::IndexType;                                                                         \
    using ValueType				= typename Type ::IteratorTraits::ValueType;                                                                         \
    using PointerType			= typename Type ::IteratorTraits::PointerType;                                                                       \
    using ReferenceType			= typename Type ::IteratorTraits::ReferenceType;                                                                     \
	using ConstPointerType		= typename Type ::IteratorTraits::ConstPointerType;                                                                  \
	using ConstReferenceType	= typename Type ::IteratorTraits::ConstReferenceType;

	// https://en.cppreference.com/w/cpp/named_req/InputIterator
	template <class IteratorType, class ElementType, class IteratorTagType = std::input_iterator_tag>
	class IReadableIterator : public IIterator<ElementType, IteratorTagType>
	{
	protected:
		using IteratorTraits = IIterator<ElementType, IteratorTagType>;

		AEON_DECLARE_ITERATOR_TRAITS(IReadableIterator);

		~IReadableIterator() = default;

	public:
		virtual ConstReferenceType operator*() const = 0;
		virtual ConstPointerType operator->() const { return addressof(*static_cast<IteratorType const&>(*this)); }

	};
	template <class IteratorType, class ElementType>
	class IReadableIterator<IteratorType, ElementType, std::random_access_iterator_tag>
		: public IIterator<ElementType, std::random_access_iterator_tag>
	{
	protected:
		using IteratorTraits = IIterator<ElementType, std::random_access_iterator_tag>;

		AEON_DECLARE_ITERATOR_TRAITS(IReadableIterator);

		~IReadableIterator() = default;

	public:
		virtual ConstReferenceType operator*() const = 0;
		virtual ConstPointerType operator->() const { return addressof(*static_cast<IteratorType const&>(*this)); }
		virtual ConstReferenceType operator[](IndexType Index) const { return *(static_cast<IteratorType const&>(*this) + Index); }

	};

	// https://en.cppreference.com/w/cpp/named_req/OutputIterator
	template <class IteratorType, class ElementType, class IteratorTagType = std::output_iterator_tag>
	class IWritableIterator : public IIterator<ElementType, IteratorTagType>
	{
	protected:
		using IteratorTraits = IIterator<ElementType, IteratorTagType>;

		AEON_DECLARE_ITERATOR_TRAITS(IWritableIterator);

		~IWritableIterator() = default;

	public:
		virtual ReferenceType operator*() const = 0;
		virtual PointerType operator->() const { return addressof(*static_cast<IteratorType const&>(*this)); }

	};
	template <class IteratorType, class ElementType>
	class IWritableIterator<IteratorType, ElementType, std::random_access_iterator_tag>
	    : public IIterator<ElementType, std::random_access_iterator_tag>
	{
	protected:
		using IteratorTraits = IIterator<ElementType, std::random_access_iterator_tag>;

		AEON_DECLARE_ITERATOR_TRAITS(IWritableIterator);

		~IWritableIterator() = default;

	public:
		virtual ReferenceType operator*() const = 0;
		virtual PointerType operator->() const { return addressof(*static_cast<IteratorType const&>(*this)); }
		virtual ReferenceType operator[](IndexType Index) const { return *(static_cast<IteratorType const&>(*this) + Index); }

	};

	// https://en.cppreference.com/w/cpp/named_req/ForwardIterator
	template <class IteratorType, class ElementType, class IteratorTagType = std::forward_iterator_tag>
	class IForwardAccessIterator
		: public TSelectIf<VIsConst<ElementType>,
						   IReadableIterator<IteratorType, ElementType, IteratorTagType>,
						   IWritableIterator<IteratorType, ElementType, IteratorTagType>>
	{
	protected:
		using IteratorTraits = IIterator<ElementType, IteratorTagType>;

		AEON_DECLARE_ITERATOR_TRAITS(IForwardAccessIterator);

		~IForwardAccessIterator() = default;

	public:
		virtual IteratorType& operator++() = 0;
		virtual IteratorType operator++(int)
		{
			IteratorType Temp{ static_cast<IteratorType&>(*this) };
			++static_cast<IteratorType&>(*this);
			return Temp;
		}

		virtual bool operator==(IteratorType const& Other) const = 0;
	//	virtual bool operator!=(IteratorType const& Other) const { return !(static_cast<IteratorType const&>(*this) == Other); }

		// TODO maybe make extension to standard iterator concept (alternative to Iter != Container.end())
//		virtual operator bool() const = 0;
	};

	// https://en.cppreference.com/w/cpp/named_req/BidirectionalIterator
	template <class IteratorType, class ElementType, class IteratorTagType = std::bidirectional_iterator_tag>
	class ISequentialAccessIterator : public IForwardAccessIterator<IteratorType, ElementType, IteratorTagType>
	{
	protected:
		using IteratorTraits = IIterator<ElementType, IteratorTagType>;

		AEON_DECLARE_ITERATOR_TRAITS(ISequentialAccessIterator);

		~ISequentialAccessIterator() = default;

	public:
		virtual IteratorType& operator--() = 0;
		virtual IteratorType operator--(int)
		{
			IteratorType Temp{ static_cast<IteratorType&>(*this) };
			--static_cast<IteratorType&>(*this);
			return Temp;
		}
	};

	// https://en.cppreference.com/w/cpp/named_req/RandomAccessIterator
	template <class IteratorType, class ElementType, class IteratorTagType = std::random_access_iterator_tag>
	class IRandomAccessIterator : public ISequentialAccessIterator<IteratorType, ElementType, IteratorTagType>
	{
	protected:
		using IteratorTraits = IIterator<ElementType, IteratorTagType>;

		AEON_DECLARE_ITERATOR_TRAITS(IRandomAccessIterator);

		~IRandomAccessIterator() = default;

	public:
		virtual IteratorType operator+(IndexType Index) const = 0;
		virtual IteratorType operator-(IndexType Index) const = 0;

		virtual IteratorType& operator+=(IndexType Index) = 0;
		virtual IteratorType& operator-=(IndexType Index) = 0;

		virtual IndexType operator-(IteratorType const& Other) const = 0;

		friend IteratorType operator+(IndexType Index, IteratorType const& Iter)
		{
			return Iter + Index;
		}

		virtual strong_ordering operator<=>(IteratorType const& Other) const = 0;
	};

}

#endif