#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_CONTAINERCOMMON
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_CONTAINERCOMMON

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Allocator/Allocator.hpp"
#include "Aeon/Framework/Container/Iterator.hpp"
#include "Aeon/Framework/Functional/Optional.hpp"
#include "Aeon/Framework/Functional/Relational.hpp"

// FIXME maybe make this ZzInternal
namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CInferableScalarObject, CInferableAllocator

	template <class Type>
	concept CInferableScalarObject = CScalarObject<Type> || CSameClass<Type, TInferTypeTag>;
	template <class Type>
	concept CInferableAllocator = CAllocator<Type> || CSameClass<Type, TInferTypeTag>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TFilterOperationResult

	template <template <class, class> class ContainerTemplate,
			  class ElementType, class FilterElementType,
			  class AllocatorType, class FilterAllocatorType>
	struct ZzInternal_TFilterOperationResult
	{
		using FilteredElementType = TSelectIf<VIsSameClass<FilterElementType, TInferTypeTag>,
											  ElementType,
											  FilterElementType>;

		static_assert(VIsConstructible<FilteredElementType, TAddLvalueReference<TAddConst<ElementType>>>);

		using FilteredAllocatorType =  TSelectIf<VIsSameClass<FilterAllocatorType, TInferTypeTag>,
												 TRebindAllocator<AllocatorType, FilteredElementType>,
												 FilterAllocatorType>;

		static_assert(VIsSameClass<FilteredElementType, typename FilteredAllocatorType::ObjectType>);

		using Type = ContainerTemplate<FilteredElementType, FilteredAllocatorType>;
	};

	template <template <class, class> class ContainerTemplate,
									  CScalarObject ElementType, CInferableScalarObject FilterElementType,
									  CAllocator AllocatorType, CInferableAllocator FilterAllocatorType>
	using TFilterOperationResult = typename ZzInternal_TFilterOperationResult<ContainerTemplate,
																			  ElementType, FilterElementType,
																			  AllocatorType, FilterAllocatorType>::Type;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TMapOperationResult

	template <template <class, class> class ContainerTemplate,
			  class ElementType, class MapElementType,
			  class AllocatorType, class MapAllocatorType,
			  class PredicateType>
	struct ZzInternal_TMapOperationResult
	{
		using PredicateReturnType = typename TCallableTraits<PredicateType, TAddLvalueReference<TAddConst<ElementType>>>::ReturnType;

		using MappedElementType = TSelectIf<VIsSameClass<MapElementType, TInferTypeTag>,
											PredicateReturnType,
											MapElementType>;

		// Ensure that, if we are coercing the return type of Map, that type is constructible from the return type of the predicate;
		// this is always valid when TInferTypeTag is passed.
		static_assert(VIsConstructible<MappedElementType, PredicateReturnType>);

		using MappedAllocatorType =  TSelectIf<VIsSameClass<MapAllocatorType, TInferTypeTag>,
											   TRebindAllocator<AllocatorType, MappedElementType>,
											   MapAllocatorType>;

		static_assert(VIsSameClass<MappedElementType, typename MappedAllocatorType::ObjectType>);

		using Type = ContainerTemplate<MappedElementType, MappedAllocatorType>;
	};


	template <template <class, class> class ContainerTemplate,
									  CScalarObject ElementType, CInferableScalarObject MapElementType,
									  CAllocator AllocatorType, CInferableAllocator MapAllocatorType,
									  class PredicateType>
	using TMapOperationResult = typename ZzInternal_TMapOperationResult<ContainerTemplate,
																		ElementType, MapElementType,
																		AllocatorType, MapAllocatorType,
																		PredicateType>::Type;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TReserveStorage, TCreateElements, TReclaimStorage

	AEON_DECLARE_TAG_CLASS(TReserveStorage); // NOLINT(google-explicit-constructor)
	AEON_DECLARE_TAG_CLASS(TCreateElements);
	AEON_DECLARE_TAG_CLASS(TReclaimStorage);

#if 0
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CIterableContainer

	template <class Type>
	concept CIterableContainer = requires
	{
		typename Type::ElementType;
		requires CScalarObject<typename Type::ElementType>;

		typename Type::IndexType;
		requires CUnsignedInteger<typename Type::IndexType>;

		requires requires (Type const Container, szint BufferIndex)
		{ { Container.DataBuffer_Internal(BufferIndex) } -> CSameClass<voidptr>; };
		requires requires (Type const Container)
		{ { Container.DataBufferCount_Internal() } -> CSameClass<szint>; };

		requires requires (Type const Container, szint BufferIndex)
		{ { Container.DataElementSize_Internal(BufferIndex) } -> CSameClass<szint>; };
		requires requires (Type const Container)
		{ { Container.DataElementCount_Internal() } -> CSameClass<szint>; };

		requires requires (Type const Container)
		{ { Container.DataElementCapacity_Internal() } -> CSameClass<szint>; };
	};
#endif
}

#endif