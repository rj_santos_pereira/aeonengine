#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_QUEUE
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_QUEUE

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Container/Dearray.hpp"

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GQueue

	template <CScalarObject ElemType, CAllocatorForObject<TDecayType<ElemType>> AllocType = GDynamicAllocator<ElemType>>
	class GQueue
	{
	public:
		using ElementType = ElemType;
		using ConstElementType = TAddConst<ElemType>;

		using AllocatorType = AllocType;

		using IndexType = typename AllocType::IndexType;

		template <class NewAllocatorType = AllocatorType>
		explicit GQueue(NewAllocatorType&& NewAllocator = NewAllocatorType{})
		requires CSameClass<TDecayType<NewAllocatorType>, AllocatorType>
			: Data(Forward<NewAllocatorType>(NewAllocator))
		{
		}
		template <class NewAllocatorType = AllocatorType>
		explicit GQueue(TReserveStorage, IndexType InitialAllocationCount, NewAllocatorType&& NewAllocator = NewAllocatorType{})
		requires CSameClass<TDecayType<NewAllocatorType>, AllocatorType>
			: Data(TReserveStorage::Tag, InitialAllocationCount, Forward<NewAllocatorType>(NewAllocator))
		{
		}

		GQueue(InitList<ElementType> InitialElemList)
		requires CCopyable<ElementType>
			: Data(InitialElemList)
		{
		}
		GQueue(RvalRefInitList<ElementType> InitialElemList)
		requires CMoveable<ElementType>
			: Data(InitialElemList)
		{
		}

		GQueue(GQueue const& Other)
		requires CCopyable<ElementType>
			: Data(Other.Data)
		{
		}
		GQueue(GQueue&& Other)
		requires CMoveable<ElementType> || CSwappableResourceAllocator<AllocatorType>
			: Data(Move(Other.Data))
		{
		}

		GQueue& operator=(GQueue const& Other)
		requires CCopyable<ElementType>
		{
			Data = Other.Data;
			return *this;
		}
		GQueue& operator=(GQueue&& Other)
		requires CMoveable<ElementType> || CSwappableResourceAllocator<AllocatorType>
		{
			Data = Move(Other.Data);
			return *this;
		}

		GQueue& Append(GQueue const& Other)
		requires CCopyable<ElementType>
		{
			Data.Append(Other.Data);
			return *this;
		}
		GQueue& Append(GQueue&& Other)
		requires CMoveable<ElementType>
		{
			Data.Append(Move(Other.Data));
			return *this;
		}

		void Push(ConstElementType& Element)
		requires CCopyable<ElementType>
		{
			Data.AddToBack(Element);
		}
		void Push(ElementType&& Element)
		requires CMoveable<ElementType>
		{
			Data.AddToBack(Move(Element));
		}

		template <class ... ArgumentTypes>
		void PushEmplace(ArgumentTypes&& ... Arguments)
		requires CConstructible<ElementType, ArgumentTypes ...>
		{
			Data.EmplaceInBack(Forward<ArgumentTypes>(Arguments) ...);
		}

		bool Pop()
		{
			bool HasElement = Count() > 0;
			if (HasElement)
			{
				Data.RemoveFromFront();
			}
			return HasElement;
		}
		bool Pop(TReclaimStorage)
		{
			bool HasElement = Count() > 0;
			if (HasElement)
			{
				Data.RemoveFromFront(TReclaimStorage::Tag);
			}
			return HasElement;
		}
		bool Pop(ElementType& Element)
		requires CMoveableElseCopyable<ElementType>
		{
			bool HasElement = Count() > 0;
			if (HasElement)
			{
				Element = MoveElseCopy(Peek());
				Data.RemoveFromFront();
			}
			return HasElement;
		}
		bool Pop(ElementType& Element, TReclaimStorage)
		requires CMoveableElseCopyable<ElementType>
		{
			bool HasElement = Count() > 0;
			if (HasElement)
			{
				Element = MoveElseCopy(Peek());
				Data.RemoveFromFront(TReclaimStorage::Tag);
			}
			return HasElement;
		}

		void Reserve(IndexType NewAllocatedCount)
		{
			Data.Reserve(NewAllocatedCount);
		}

		void Empty()
		{
			Data.Empty();
		}
		void Empty(TReclaimStorage)
		{
			Data.Empty(TReclaimStorage::Tag);
		}

		ConstElementType& Peek() const
		{
			return Data.Front();
		}
		ElementType& Peek()
		{
			return AEON_INVOKE_CONST_OVERLOAD(Peek);
		}

		IndexType AllocatedCount() const
		{
			return Data.AllocatedCount();
		}
		IndexType Count() const
		{
			return Data.Count();
		}

		bool IsEmpty() const
		{
			return Data.IsEmpty();
		}

		AllocatorType const& GetAllocator() const
		{
			return Data.Second();
		}

		friend void Swap(GQueue& Queue1, GQueue& Queue2)
		requires CSwappableResourceAllocator<AllocatorType>
		{
			using Aeon::Swap;
			Swap(Queue1.Data, Queue2.Data);
		};

	private:
		using StorageType = GDearray<ElemType, AllocType>;

		StorageType Data;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GStaticQueue

	template <CScalarObject ElemType, szint ElemCountValue>
	using GStaticQueue = GQueue<ElemType, GStaticAllocator<ElemType, ElemCountValue>>;
}

#endif
