#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_CONCURRENT_QUEUE
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_CONCURRENT_QUEUE

#include "Aeon/Framework/Core.hpp"

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GConcurrentQueue

	// This is a multiple-producer/multiple-consumer lock-free queue,
	// bounded on number of concurrent thread accesses, but unbounded on number of elements.
	//
	// The queue uses a single-linked list as the underlying data structure and supports two mutating operations:
	// Push() for enqueueing an element (by a producer), and Pop() for dequeueing an element (by a consumer);
	// both operations can be described by a 'work' phase, where the queue is mutated, followed by a conditional 'reclamation' phase,
	// where memory for elements that were dequeued (irrespective of the thread that dequeued the element) is reclaimed.
	// At no point does any thread wait for other threads to finish their operations/phases before it starts its own;
	// however, any 'reclamation' phase only starts after all 'work' phases across concurrent threads finish,
	// to ensure no other threads have access to the memory that will be reclaimed.
	// The thread achieves this by isolating the memory to reclaim with a cmpxchg operation (see FinishOp_Internal).
	// if a thread is not able to start the 'reclamation' phase because other threads are amidst operations,
	// it finishes its own operation and defers memory reclamation to another thread.
	// Note that multiple threads may concurrently execute separate 'reclamation' phases,
	// as the algorithm allows other threads to continue operating on the queue while a thread is reclaiming memory;
	// this can happen if a thread is preempted while reclaiming memory,
	// and other threads begin (and eventually finish) new operations on the queue.
	//
	// The queue achieves lock-freedom for the following definition of progress:
	// if a single thread is suspended while executing an operation, all other concurrent threads are able to finish their own operations,
	// and new operations may be started and finished with correct results and no additional runtime overhead*.
	//
	// *If a thread is suspended while in the 'work' phase, no memory in the queue is reclaimed until the thread is resumed,
	// as no other thread can start a 'reclamation' phase while a thread is completing its 'work' phase--in a situation with an hypothetical
	// adversarial scheduler that would preempt a thread indefinitely while it was executing an operation in this phase,
	// memory usage would grow unbounded with each subsequent Push/Pop pair of operations by other threads.
	// However, if a thread is suspended while in the 'reclamation' phase, only the memory being reclaimed by the suspended thread is unclaimed,
	// as other threads may start new 'reclamation' phases.

	template <CScalarObject ElemType, uint32 ReqThreadCountValue = 16>
	class alignas_atomic() GConcurrentQueue
	{
		// This implementation uses the LSBs of the head pointer/counter field to store additional state,
		// namely an access counter and a modify-bit, so that all state can be updated atomically;
		// compute the node alignment based on the requested number of threads,
		// so that there are enough unused bits in the address to represent the additional state.
		// E.g. for ThreadCount 							==  16 (0b0...0001'0000)
		//  -> ((ThreadCount << 1) + 1) 					==  33 (0b0...0010'0001)
		//  -> NextPowerOfTwo((ThreadCount << 1) + 1) 		==  64 (0b0...0100'0000) -> This is the NodeAlignment
		// Thus, for NodeAlignment							==  64 (0b0...0100'0000)
		//  -> (NodeAlignment >> 1) 						==  32 (0b0...0010'0000) -> This is the ModifyBitMask
		//  -> (ModifyBitMask - 1) 							==  31 (0b0...0001'1111) -> This is the CounterBitMask
		//  -> ~(ModifyBitMask | CounterBitMask)			== ~63 (0b1...1100'0000) -> This is the PointerBitMask
		// Note that for 16 <= ReqThreadCountValue < 32, NodeAlignment will be 64,
		// and thus the bit masks will have the same values as in the example.

		static constexpr szint NodeAlignment = AeonMath::Max(alignof(ElemType), szint(AeonMath::NextPowerOfTwo((ReqThreadCountValue << 1) + 1)));

		using PtrCtrBitType = ptrint;

		struct alignas_atomic(NodeAlignment) DNode
		{
			template <class ... ArgTypes>
			explicit DNode(ArgTypes&&... Args)
				: ObjStorage{ Forward<ArgTypes>(Args)... }
				, NextNodePtr{ nullptr }
				, LastPoppedNodePtr{ nullptr }
				, Popped{}
				, SeqNum{ 0 }
			{
			}

			ElemType ObjStorage;

			// CHECKME Figure out if we are able to improve performance by realigning and/or reordering these fields
			alignas_atomic() GAtomicPointer<DNode> NextNodePtr;
			GAtomicPointer<DNode> LastPoppedNodePtr;
			GAtomicFlag<EMemoryOrder::Relaxed> Popped;
			uint64 SeqNum = 0;
		};

	public:
		using ElementType = ElemType;

		GConcurrentQueue()
			: HeadPtrCtr{0}
		{
		}

		AEON_MAKE_NON_COPYABLE(GConcurrentQueue);
		AEON_MAKE_NON_MOVEABLE(GConcurrentQueue);

		~GConcurrentQueue()
		{
			PtrCtrBitType HeadPtrBits = HeadPtrCtr.Load();
			DNode* HeadPtr = bit_cast<DNode*>(HeadPtrBits & PointerBitMask);

			AEON_ASSERT((HeadPtrBits & CounterBitMask) == 0, "GConcurrentQueue still in use during destruction!");
			if (HeadPtr)
			{
				ReclaimMemory_Internal(HeadPtr, nullptr);
			}
		}

		template <class ... ArgumentTypes>
		void Push(ArgumentTypes&& ... Arguments)
		requires CConstructible<ElementType, ArgumentTypes ...>
		{
			// First create a node with the object;
			// Transform the value into a pointer/counter (PtrCtr), the counter is init'ed to zero.
			DNode* NewNodePtr = Aeon::New<DNode>(Forward<ArgumentTypes>(Arguments)...);
			PtrCtrBitType NewNodePtrCtrBits = bit_cast<PtrCtrBitType>(NewNodePtr);

			// Try to make the head of the queue point to our node, assuming an empty queue,
			// if this fails, we'll try to increment the access counter,
			// We'll loop until we either increment (and obtain the head node), or we write our node.
			PtrCtrBitType HeadPtrCtrBits;
			do
			{
				HeadPtrCtrBits = FetchIncrementHeadIfNotZero_Internal();
			}
			while (HeadPtrCtrBits == 0 && !HeadPtrCtr.CompareExchange(HeadPtrCtrBits, NewNodePtrCtrBits));

			// If we were able to insert the node as the head, we can just exit, no need to increment/decrement anything,
			// as we are not accessing any other nodes.
			if ((HeadPtrCtrBits & PointerBitMask) == 0)
			{
				return;
			}

			// If instead we obtained the head node (and incremented the access counter),
			// we have to insert our node at the end of the queue,
			// so we'll iterate the existing nodes until we find the last one.
			DNode* HeadPtr = bit_cast<DNode*>(HeadPtrCtrBits & PointerBitMask);

			// The SeqNum (sequence number) of our node is updated to the previous node's SeqNum + 1;
			// this SeqNum helps the consumer threads determine which node should be the new head node.
			DNode* CurrNodePtr;
			DNode* NextNodePtr = HeadPtr;
			do
			{
				CurrNodePtr = NextNodePtr;
				NewNodePtr->SeqNum = CurrNodePtr->SeqNum + 1;
				NextNodePtr = nullptr;
			}
			// NOTE This needs at least acq-rel ordering,
			//  due to reading the SeqNum of nodes in the queue, and potentially writing our node (with its updated SeqNum).
			while (!CurrNodePtr->NextNodePtr.CompareExchange(NextNodePtr, NewNodePtr));

			// Lastly, we need to decrement the access counter, and possibly update the head node ptr.
			FinishOp_Internal(false);
		}

		GOptional<ElementType> Pop()
		requires CMoveableElseCopyable<ElementType>
		{
			GOptional<ElementType> Object;

			// Start by trying to increment access counter if the queue is not empty;
			PtrCtrBitType HeadPtrCtrBits = FetchIncrementHeadIfNotZero_Internal();
			// If the queue is empty, just exit.
			if ((HeadPtrCtrBits & PointerBitMask) == 0)
			{
				return Object;
			}

			DNode* HeadPtr = bit_cast<DNode*>(HeadPtrCtrBits & PointerBitMask);

			// Now search for the first element that is still available,
			// stop the search if we reach the end of the queue without finding an available element.
			bool HasElement;
			DNode* CurrNodePtr;
			DNode* NextNodePtr = HeadPtr;
			do
			{
				CurrNodePtr = NextNodePtr;
				HasElement = !CurrNodePtr->Popped.TestAndSet();
				NextNodePtr = CurrNodePtr->NextNodePtr.Load();
			}
			while (!HasElement && NextNodePtr != nullptr);

			// If we found an element, try to update the last popped node pointer with the node we just popped
			// based on its sequence number.
			DNode* PoppedNodePtr = HasElement ? CurrNodePtr : nullptr;
			if (PoppedNodePtr)
			{
				Object.Set(MoveElseCopy(PoppedNodePtr->ObjStorage));

				DNode* LastPoppedNodePtr;
				do
				{
					LastPoppedNodePtr = HeadPtr->LastPoppedNodePtr.Load();
				}
				while ((LastPoppedNodePtr == nullptr || LastPoppedNodePtr->SeqNum < PoppedNodePtr->SeqNum)
					   && !HeadPtr->LastPoppedNodePtr.CompareExchange(LastPoppedNodePtr, PoppedNodePtr));
			}

			// Lastly, we need to decrement the access counter, and update the head node ptr.
			FinishOp_Internal(true);

			return Object;
		}

		bool IsEmpty() const
		{
			return HeadPtrCtr.Load<EMemoryOrder::Relaxed>() == 0;
		}

		static constexpr uint32 SupportedThreadCount()
		{
			static_assert(CounterBitMask >= ReqThreadCountValue);
			return CounterBitMask;
		}

	private:
		PtrCtrBitType FetchIncrementHeadIfNotZero_Internal()
		{
			PtrCtrBitType HeadPtrCtrBits = HeadPtrCtr.Load();

			bool HasIncremented = false;
			while ((HeadPtrCtrBits & PointerBitMask) != 0 && !HasIncremented)
			{
				PtrCtrBitType HeadPtrCtrModBits = (HeadPtrCtrBits + 1) | ModifyBitMask;

				AEON_ASSERT((HeadPtrCtrBits & CounterBitMask) != CounterBitMask);

				HasIncremented = HeadPtrCtr.CompareExchange(HeadPtrCtrBits, HeadPtrCtrModBits);
			}

			return HeadPtrCtrBits;
		}

		PtrCtrBitType FetchDecrementHeadIfNotOne_Internal()
		{
			PtrCtrBitType HeadPtrCtrBits = HeadPtrCtr.Load();

			bool HasDecremented = false;
			while ((HeadPtrCtrBits & CounterBitMask) != 1 && !HasDecremented)
			{
				PtrCtrBitType HeadPtrCtrModBits = (HeadPtrCtrBits - 1) | ModifyBitMask;

				HasDecremented = HeadPtrCtr.CompareExchange(HeadPtrCtrBits, HeadPtrCtrModBits);
			}

			return HeadPtrCtrBits;
		}

		void FinishOp_Internal(bool SkipPoppedNodeCheck)
		{
			bool HasDecremented;
			do
			{
				// First try to decrement if we are not the last thread accessing the structure.
				PtrCtrBitType HeadPtrCtrBits = FetchDecrementHeadIfNotOne_Internal();
				DNode* OldHeadPtr = bit_cast<DNode*>(HeadPtrCtrBits & PointerBitMask);

				HasDecremented = (HeadPtrCtrBits & CounterBitMask) != 1;

				// If we failed to decrement because we would be the last thread, we have to update the head pointer/counter.
				// We'll only run the last bit of logic if we successfully mark the ptr/ctr with an unset modify-bit;
				// We need this to detect the ABA problem, where a thread may enter, modify, and leave the structure
				// while this thread is computing the updated value of the head, which could completely invalidate the new value.
				PtrCtrBitType HeadPtrCtrNonModBits = HeadPtrCtrBits & ~ModifyBitMask;
				if (!HasDecremented && HeadPtrCtr.CompareExchange(HeadPtrCtrBits, HeadPtrCtrNonModBits))
				{
					// Load the last popped node from the current head node; at this point,
					// we should be able to sync with all past consumer threads and see the most up-to-date value;
					// the exception to this is "future" producer and consumer threads which might have started since the last cmpxchg in HeadPtrCtr;
					// changes by those will be detected by the following cmpxchg, as the threads will have set the modify bit.
					DNode* LastPoppedNodePtr = OldHeadPtr->LastPoppedNodePtr.Load();

					// The new head will be the node following the last popped node, which might be null.
					// NOTE that, when called from Push(), LastPoppedNodePtr can be null here,
					// indicative that no thread has popped an element in the meantime, and thus there is no new head node;
					// in that situation, we assign the old head pointer to NewHeadPtr,
					// which both preserves the head node in the cmpxchg and makes ReclaimNodes() skip node deletion.
					// However, when called from Pop(), LastPoppedNodePtr can NEVER be null here, because we either just popped an element,
					// or we tried to pop an element (queue was non-empty when we entered) but couldn't obtain one because other threads
					// popped all elements before this thread.
					AEON_ASSERT(!SkipPoppedNodeCheck || LastPoppedNodePtr);
					DNode* NewHeadPtr = (SkipPoppedNodeCheck || LastPoppedNodePtr) ? LastPoppedNodePtr->NextNodePtr.Load() : OldHeadPtr;

					// Lastly, update the head pointer/counter if the counter and modify bit were preserved, otherwise,
					// we retry all the decrement logic.
					PtrCtrBitType NewHeadPtrCtrBits = bit_cast<PtrCtrBitType>(NewHeadPtr);
					HasDecremented = HeadPtrCtr.CompareExchange(HeadPtrCtrNonModBits, NewHeadPtrCtrBits);

					// If we successfully decremented, then we have the guarantee that we are the last thread with access to the popped nodes,
					// if any, so reclaim the memory now.
					if (HasDecremented)
					{
						ReclaimMemory_Internal(OldHeadPtr, NewHeadPtr);
					}
				}
			}
			while (!HasDecremented);
		}

		static void ReclaimMemory_Internal(DNode* OldHeadPtr, DNode* NewHeadPtr)
		{
			DNode* CurrNodePtr = OldHeadPtr;
			while (CurrNodePtr != NewHeadPtr)
			{
				DNode* NextNodePtr = CurrNodePtr->NextNodePtr.Load();

				Aeon::Delete<DNode>(CurrNodePtr);

				CurrNodePtr = NextNodePtr;
			}
		}

		GAtomicScalar<PtrCtrBitType> HeadPtrCtr;

		static constexpr PtrCtrBitType ModifyBitMask = (NodeAlignment >> 1);
		static constexpr PtrCtrBitType CounterBitMask = ModifyBitMask - 1;
		static constexpr PtrCtrBitType PointerBitMask = ~(ModifyBitMask | CounterBitMask);

	};
}

#endif
