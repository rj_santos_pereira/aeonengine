add_subdirectory(Tuple)

target_sources(${PROJECT_NAME}
    PUBLIC
		${CMAKE_CURRENT_SOURCE_DIR}/Iterator.hpp # TODO should this be moved to its own subfolder?

	#	${CMAKE_CURRENT_SOURCE_DIR}/BitSet.hpp # TODO not ready

		${CMAKE_CURRENT_SOURCE_DIR}/ContainerCommon.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/ArrayCommon.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/ListCommon.hpp

		${CMAKE_CURRENT_SOURCE_DIR}/Array.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/Dearray.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/Stack.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/Queue.hpp

		${CMAKE_CURRENT_SOURCE_DIR}/ConcurrentQueue.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/List.hpp

	#	${CMAKE_CURRENT_SOURCE_DIR}/BinaryTree.hpp # TODO maybe rename to RedBlackTree? or AVLTree?

		${CMAKE_CURRENT_SOURCE_DIR}/HashTable.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/HashMap.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/HashSet.hpp
    )