#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_ARRAYCOMMON
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_ARRAYCOMMON

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Container/ContainerCommon.hpp"
#include "Aeon/Framework/Container/Tuple/Duple.hpp"
#include "Aeon/Framework/Allocator/StaticAllocator.hpp"
#include "Aeon/Framework/Allocator/DynamicAllocator.hpp"

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GArrayIterator

	// TODO move this to Array.hpp

	template <class ArrayType>
	class GArrayIterator final
		: public IRandomAccessIterator<GArrayIterator<ArrayType>, TPropagateConst<ArrayType, typename ArrayType::ElementType>>
	{
		using Base = IRandomAccessIterator<GArrayIterator<ArrayType>, TPropagateConst<ArrayType, typename ArrayType::ElementType>>;

		using ArrayElementType = typename ArrayType::ElementType;
		using ArrayIndexType = typename ArrayType::IndexType;

	public:
		AEON_DECLARE_ITERATOR_TRAITS(Base);

		GArrayIterator() = default;
		explicit GArrayIterator(ArrayType* NewArrayPtr, ArrayIndexType NewElemIdx)
			: ArrayPtr{ NewArrayPtr }
			, ElemIdx{ NewElemIdx }
		{
			AEON_ASSERT(ArrayPtr);
			AEON_ASSERT(0 <= ElemIdx && ElemIdx <= ArrayPtr->DataElementCount_Internal());
		}

		AEON_MAKE_COPYABLE(GArrayIterator);

		virtual ReferenceType operator*() const override
		{
			// Don't allow dereferencing the past-the-last "element"
			AEON_ASSERT(ElemIdx != ArrayPtr->DataElementCount_Internal());
			return *AeonMemory::MemAccess<ArrayElementType>(static_cast<ArrayElementType*>(ArrayPtr->DataBuffer_Internal(0)) + ElemIdx);
		}

		virtual GArrayIterator& operator++() override
		{
			// Don't increment beyond the past-the-last "element"
			AEON_ASSERT(ElemIdx < ArrayPtr->DataElementCount_Internal());
			++ElemIdx;
			return *this;
		}
		virtual GArrayIterator& operator--() override
		{
			// Don't decrement beyond the first element
			AEON_ASSERT(0 < ElemIdx);
			--ElemIdx;
			return *this;
		}

		virtual GArrayIterator operator+(IndexType Distance) const override
		{
			return GArrayIterator{ *this } += Distance;
		}
		virtual GArrayIterator operator-(IndexType Distance) const override
		{
			return GArrayIterator{ *this } -= Distance;
		}

		virtual GArrayIterator& operator+=(IndexType Distance) override
		{
			auto NewElemIdx = ArrayIndexType(ElemIdx + Distance);
			AEON_ASSERT(0 <= NewElemIdx && NewElemIdx <= ArrayPtr->DataElementCount_Internal());
			ElemIdx = NewElemIdx;
			return *this;
		}
		virtual GArrayIterator& operator-=(IndexType Distance) override
		{
			auto NewElemIdx = ArrayIndexType(ElemIdx - Distance);
			AEON_ASSERT(0 <= NewElemIdx && NewElemIdx <= ArrayPtr->DataElementCount_Internal());
			ElemIdx = NewElemIdx;
			return *this;
		}

		virtual IndexType operator-(GArrayIterator const& Other) const override
		{
			AEON_ASSERT(ArrayPtr == Other.ArrayPtr);
			return ArrayIndexType(ElemIdx) - ArrayIndexType(Other.ElemIdx);
		}

		virtual bool operator==(GArrayIterator const& Other) const override
		{
			AEON_ASSERT(ArrayPtr == Other.ArrayPtr);
			return ElemIdx == Other.ElemIdx;
		}
		virtual strong_ordering operator<=>(GArrayIterator const& Other) const override
		{
			AEON_ASSERT(ArrayPtr == Other.ArrayPtr);
			return ElemIdx <=> Other.ElemIdx;
		}

	private:
		ArrayType* ArrayPtr;
		ArrayIndexType ElemIdx;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GDearrayIterator

	// TODO move this to Dearray.hpp

	template <class ArrayType>
	class GDearrayIterator final
		: public IRandomAccessIterator<GDearrayIterator<ArrayType>, TPropagateConst<ArrayType, typename ArrayType::ElementType>>
	{
		using Base = IRandomAccessIterator<GDearrayIterator<ArrayType>, TPropagateConst<ArrayType, typename ArrayType::ElementType>>;

		using ArrayElementType = typename ArrayType::ElementType;
		using ArrayIndexType = typename ArrayType::IndexType;

	public:
		AEON_DECLARE_ITERATOR_TRAITS(Base);

		GDearrayIterator() = default;
		explicit GDearrayIterator(ArrayType* NewArrayPtr, ArrayIndexType NewElemIdx, ArrayIndexType NewElemBufferIdx)
			: ArrayPtr{ NewArrayPtr }
			, ElemIdx{ NewElemIdx }
			, ElemBufferIdx{ NewElemBufferIdx }
			, BufferLastIdx{ ArrayIndexType(ArrayPtr->DataElementCapacity_Internal() - 1) }
		{
		}

		AEON_MAKE_COPYABLE(GDearrayIterator);

		virtual ReferenceType operator*() const override
		{
			// Don't allow dereferencing the 'end' iterator
			AEON_ASSERT(ElemIdx != ArrayPtr->DataElementCount_Internal());
			return *AeonMemory::MemAccess<ArrayElementType>(static_cast<ArrayElementType*>(ArrayPtr->DataBuffer_Internal(0)) + ElemBufferIdx);
		}

		virtual GDearrayIterator& operator++() override
		{
			AEON_ASSERT(ElemIdx < ArrayPtr->DataElementCount_Internal());
			++ElemIdx;
			ElemBufferIdx = ElemBufferIdx != BufferLastIdx ? ElemBufferIdx + 1 : 0;
			return *this;
		}
		virtual GDearrayIterator& operator--() override
		{
			AEON_ASSERT(0 < ElemIdx);
			--ElemIdx;
			ElemBufferIdx = ElemBufferIdx != 0 ? ElemBufferIdx - 1 : BufferLastIdx;
			return *this;
		}

		virtual GDearrayIterator operator+(IndexType Distance) const override
		{
			return GDearrayIterator{ *this } += Distance;
		}
		virtual GDearrayIterator operator-(IndexType Distance) const override
		{
			return GDearrayIterator{ *this } -= Distance;
		}

		virtual GDearrayIterator& operator+=(IndexType Distance) override
		{
			auto NewElemIdx = ArrayIndexType(ElemIdx + Distance);
			AEON_ASSERT(0 <= NewElemIdx && NewElemIdx <= ArrayPtr->DataElementCount_Internal());
			ElemIdx = NewElemIdx;

			ArrayIndexType WrapDistance = BufferLastIdx - ElemBufferIdx;
			if (Distance <= WrapDistance)
			{
				ElemBufferIdx += Distance;
			}
			else
			{
				ElemBufferIdx = 0 + (Distance - WrapDistance - 1);
			}

			return *this;
		}
		virtual GDearrayIterator& operator-=(IndexType Distance) override
		{
			auto NewElemIdx = ArrayIndexType(ElemIdx - Distance);
			AEON_ASSERT(0 <= NewElemIdx && NewElemIdx <= ArrayPtr->DataElementCount_Internal());
			ElemIdx = NewElemIdx;

			ArrayIndexType WrapDistance = ElemBufferIdx - 0;
			if (Distance <= WrapDistance)
			{
				ElemBufferIdx -= Distance;
			}
			else
			{
				ElemBufferIdx = BufferLastIdx - (Distance - WrapDistance - 1);
			}

			return *this;
		}

		virtual IndexType operator-(GDearrayIterator const& Other) const override
		{
			AEON_ASSERT(ArrayPtr == Other.ArrayPtr);
			return ArrayIndexType(ElemIdx) - ArrayIndexType(Other.ElemIdx);
		}

		virtual bool operator==(GDearrayIterator const& Other) const override
		{
			AEON_ASSERT(ArrayPtr == Other.ArrayPtr);
			return ElemIdx == Other.ElemIdx;
		}
		virtual strong_ordering operator<=>(GDearrayIterator const& Other) const override
		{
			AEON_ASSERT(ArrayPtr == Other.ArrayPtr);
			return ElemIdx <=> Other.ElemIdx;
		}

	private:
		// Keep both index and offset because some operations are simpler with one than the other
		// Also ensures we don't dereference a past-the-last iterator even if it would be well-defined
		// (because the past-the-last iterator in a full ZzInternal_GDearray points to the first element)

		ArrayType* ArrayPtr;
		ArrayIndexType ElemIdx;
		ArrayIndexType ElemBufferIdx;
		ArrayIndexType BufferLastIdx;
	};

}

#endif