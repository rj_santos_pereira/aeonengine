#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_BITSET
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_BITSET

#include "Aeon/Framework/Core.hpp"

namespace Aeon
{
#if 0
	template <class BitSetType, szint BitCountValue>
	class GBitSetIterator
	{

	};
#endif

	template <class ElementType>
	class GBitRef
	{
		GBitRef(ElementType* NewBitPtr, ElementType NewBitIdx)
		requires CSameClass<TRemoveConst<ElementType>, uint8>
		    : BitPtrMask{ bit_cast<ptrint>(NewBitPtr) | static_cast<ptrint>(NewBitIdx) }
		{
			// NOTE BitPtr should always be a pointer aligned to at least a 16 byte boundary (to enable vectorization),
			//  and BitIdx should always be an integer of at most 3 bits of width (due to internal element representation as an 8-bit byte).
		}

	public:
		AEON_MAKE_NON_COPYABLE(GBitRef);
		AEON_MAKE_NON_MOVEABLE(GBitRef);
#if 0
		GBitRef& operator=(bool NewValue)
		{
			ElementType* BitPtr, BitIdx;

			ComputePtrIdx_Internal(BitPtr, BitIdx);

			if (NewValue)
			{
				(*BitPtr) |= (0x1 << BitIdx);
			}
			else
			{
				(*BitPtr) &= ~(0x1 << BitIdx);
			}

			return *this;
		}
#endif

		GBitRef& Set()
		requires CNotConst<ElementType>
		{
			ElementType* BitPtr, BitIdx;

			ComputeStoragePtrIdx_Internal(BitPtr, BitIdx);

			(*BitPtr) |= (0x1 << BitIdx);

			return *this;
		}

		GBitRef& Unset()
		requires CNotConst<ElementType>
		{
			ElementType* BitPtr, BitIdx;

			ComputeStoragePtrIdx_Internal(BitPtr, BitIdx);

			(*BitPtr) &= ~(0x1 << BitIdx);

			return *this;
		}

		GBitRef& Flip()
		requires CNotConst<ElementType>
		{
			ElementType* BitPtr, BitIdx;

			ComputeStoragePtrIdx_Internal(BitPtr, BitIdx);

			(*BitPtr) ^= (0x1 << BitIdx);

			return *this;
		}

		// TODO maybe make other bit-wise operators?
		AEON_FORCEINLINE bool operator~() const
		{
			return !operator bool();
		}

		explicit operator bool() const
		{
			ElementType* BitPtr, BitIdx;

			ComputeStoragePtrIdx_Internal(BitPtr, BitIdx);

			return bool(((*BitPtr) >> BitIdx) & 0x1);
		}

	private:
		AEON_FORCEINLINE void ComputeStoragePtrIdx_Internal(ElementType*& BitPtr, ElementType& BitIdx) const
		{
			BitPtr = bit_cast<ElementType*>(BitPtrMask & (AeonMath::MakeBitMask<ptrint, 60, 4>()));
			BitIdx = static_cast<ElementType>(BitPtrMask & AeonMath::MakeBitMask<ptrint, 4>());
		}

		ptrint BitPtrMask;

		template <szint BitCountValue>
		friend class GBitSet;
	};

	// TODO use SSE
	template <szint BitCountValue>
	class GBitSet
	{
	public:
		using ElementType = uint8;
		using IndexType = szint;

		GBitSet()
			: Storage{}
		{
		}
		GBitSet(Aeon::InitList<ElementType> InitList)
		{
			auto InitByteData = Aeon::InitListOps::Data(InitList);
			auto InitByteCount = Aeon::InitListOps::Count(InitList);

			for (IndexType Index = 0; Index < AeonMath::Min(InitByteCount, StorageElemCount); ++Index)
			{
				Storage[Index] = InitByteData[Index];
			}

			if constexpr (StorageLastElemBitSize > 0)
			{
				Storage[StorageElemCount - 1] &= StorageLastElemBitMask;
			}
		}

		AEON_MAKE_COPYABLE(GBitSet);
		AEON_MAKE_MOVEABLE(GBitSet);

		GBitSet& Set(IndexType BitIndex, IndexType BitCount = 1)
		{
			IndexType FirstElemIndex, LastElemIndex;
			ElementType FirstElemMask, LastElemMask;

			ComputeStorageIndexMask_Internal(BitIndex, BitCount, FirstElemIndex, FirstElemMask, LastElemIndex, LastElemMask);

			if (FirstElemIndex != LastElemIndex)
			{
				Storage[FirstElemIndex] |= FirstElemMask;
				for (auto Index = FirstElemIndex + 1; Index < LastElemIndex; ++Index)
				{
					Storage[Index] |= StorageElemBitMask;
				}
				if (LastElemMask)
				{
					Storage[LastElemIndex] |= LastElemMask;
				}
			}
			else
			{
				Storage[FirstElemIndex] |= (FirstElemMask & LastElemMask);
			}

			return *this;
		}

		GBitSet& Unset(IndexType BitIndex, IndexType BitCount = 1)
		{
			IndexType FirstElemIndex, LastElemIndex;
			ElementType FirstElemMask, LastElemMask;

			ComputeStorageIndexMask_Internal(BitIndex, BitCount, FirstElemIndex, FirstElemMask, LastElemIndex, LastElemMask);

			if (FirstElemIndex != LastElemIndex)
			{
				Storage[FirstElemIndex] &= ~FirstElemMask;
				for (auto Index = FirstElemIndex + 1; Index < LastElemIndex; ++Index)
				{
					Storage[Index] &= ~StorageElemBitMask;
				}
				if (LastElemMask)
				{
					Storage[LastElemIndex] &= ~LastElemMask;
				}
			}
			else
			{
				Storage[FirstElemIndex] &= ~(FirstElemMask & LastElemMask);
			}

			return *this;
		}

		GBitSet& Flip(IndexType BitIndex, IndexType BitCount = 1)
		{
			IndexType FirstElemIndex, LastElemIndex;
			ElementType FirstElemMask, LastElemMask;

			ComputeStorageIndexMask_Internal(BitIndex, BitCount, FirstElemIndex, FirstElemMask, LastElemIndex, LastElemMask);

			if (FirstElemIndex != LastElemIndex)
			{
				Storage[FirstElemIndex] ^= FirstElemMask;
				for (auto Index = FirstElemIndex + 1; Index < LastElemIndex; ++Index)
				{
					Storage[Index] ^= StorageElemBitMask;
				}
				if (LastElemMask)
				{
					Storage[LastElemIndex] ^= LastElemMask;
				}
			}
			else
			{
				Storage[FirstElemIndex] ^= (FirstElemMask & LastElemMask);
			}

			return *this;
		}

		bool All(IndexType BitIndex = 0, IndexType BitCount = BitCountValue) const
		{
			IndexType FirstElemIndex, LastElemIndex;
			ElementType FirstElemMask, LastElemMask;

			ComputeStorageIndexMask_Internal(BitIndex, BitCount, FirstElemIndex, FirstElemMask, LastElemIndex, LastElemMask);

			ElementType Result = StorageElemBitMask;

			if (FirstElemIndex != LastElemIndex)
			{
				Result &= (Storage[FirstElemIndex] | ~FirstElemMask);
				for (IndexType Index = FirstElemIndex + 1; Index < LastElemIndex && Result == StorageElemBitMask; ++Index)
				{
					Result &= Storage[Index];
				}
				if (LastElemMask)
				{
					Result &= (Storage[LastElemIndex] | ~LastElemMask);
				}
			}
			else
			{
				Result &= Storage[FirstElemIndex] | ~(FirstElemMask & LastElemMask);
			}

			return Result == StorageElemBitMask;
		}

		bool Any(IndexType BitIndex = 0, IndexType BitCount = BitCountValue) const
		{
			IndexType FirstElemIndex, LastElemIndex;
			ElementType FirstElemMask, LastElemMask;

			ComputeStorageIndexMask_Internal(BitIndex, BitCount, FirstElemIndex, FirstElemMask, LastElemIndex, LastElemMask);

			ElementType Result = 0x00;

			if (FirstElemIndex != LastElemIndex)
			{
				Result |= (Storage[FirstElemIndex] & FirstElemMask);
				for (IndexType Index = FirstElemIndex + 1; Index < LastElemIndex && Result == 0x00; ++Index)
				{
					Result |= Storage[Index];
				}
				if (LastElemMask)
				{
					Result |= (Storage[LastElemIndex] & LastElemMask);
				}
			}
			else
			{
				Result |= Storage[FirstElemIndex] & (FirstElemMask & LastElemMask);
			}

			return Result != 0x00;
		}

		bool None(IndexType BitIndex = 0, IndexType BitCount = BitCountValue) const
		{
			IndexType FirstElemIndex, LastElemIndex;
			ElementType FirstElemMask, LastElemMask;

			ComputeStorageIndexMask_Internal(BitIndex, BitCount, FirstElemIndex, FirstElemMask, LastElemIndex, LastElemMask);

			ElementType Result = 0x00;

			if (FirstElemIndex != LastElemIndex)
			{
				Result |= (Storage[FirstElemIndex] & FirstElemMask);
				for (IndexType Index = FirstElemIndex + 1; Index < LastElemIndex && Result != 0x00; ++Index)
				{
					Result |= Storage[Index];
				}
				if (LastElemMask)
				{
					Result |= (Storage[LastElemIndex] & LastElemMask);
				}
			}
			else
			{
				Result |= Storage[FirstElemIndex] & (FirstElemMask & LastElemMask);
			}

			return Result == 0x00;
		}

		// TODO remaining bit-wise operators

		GBitSet operator~() const
		{
			GBitSet Result{ *this };

			for (IndexType Index = 0; Index < StorageElemCount; ++Index)
			{
				Result.Storage[Index] ^= StorageElemBitMask;
			}

			if constexpr (StorageLastElemBitSize > 0)
			{
				Result.Storage[StorageElemCount - 1] &= StorageLastElemBitMask;
			}

			return Result;
		}

		AEON_FORCEINLINE GBitRef<ElementType const> operator[](IndexType Index) const
		{
			AEON_ASSERT(Index <= Count());
			return GBitRef<ElementType const>{ Storage + (Index / StorageElemBitSize),
											   ElementType(StorageElemIndexShift - (Index % StorageElemBitSize)) };
		}
		AEON_FORCEINLINE GBitRef<ElementType> operator[](IndexType Index)
		{
			AEON_ASSERT(Index <= Count());
			return GBitRef<ElementType>{ Storage + (Index / StorageElemBitSize),
										 ElementType(StorageElemIndexShift - (Index % StorageElemBitSize)) };
		}

		// TODO CountOnes, CountZeros, CountLeadingOnes/Zeros, CountTrailingOnes/Zeros

		AEON_FORCEINLINE constexpr IndexType Count() const
		{
			return BitCountValue;
		}

		AEON_FORCEINLINE voidptr DataBuffer_Internal() const
		{
			return Storage;
		}
		AEON_FORCEINLINE szint DataOffset_Internal() const
		{
			return 0;
		}

		// TODO iterators?

	private:
		void ComputeStorageIndexMask_Internal(IndexType BitIndex, IndexType BitCount,
											  IndexType& FirstElemIndex, ElementType& FirstElemMask,
											  IndexType& LastElemIndex, ElementType& LastElemMask) const
		{
			IndexType PastLastBitIndex = BitIndex + BitCount;

			AEON_ASSERT(BitCount > 0 && PastLastBitIndex <= Count());

			IndexType FirstElemBitSize = BitIndex % StorageElemBitSize;
			FirstElemIndex = BitIndex / StorageElemBitSize;
			ElementType FirstElemMaskCount = StorageElemBitSize - FirstElemBitSize;
			FirstElemMask = AeonMath::MakeBitMask<ElementType>(FirstElemMaskCount);

			IndexType LastElemBitSize = PastLastBitIndex % StorageElemBitSize;
			LastElemIndex = PastLastBitIndex / StorageElemBitSize;
			ElementType LastElemMaskCount = StorageElemBitSize - FirstElemBitSize;
			LastElemMask = ~AeonMath::MakeBitMask<ElementType>(LastElemMaskCount);
		}

		static constexpr IndexType StorageElemBitSize = Aeon::BitSizeOf(ElementType);
		static constexpr IndexType StorageElemIndexShift = StorageElemBitSize - 1;

		static constexpr IndexType StorageLastElemBitSize = (BitCountValue % StorageElemBitSize);

#pragma warning(push)
#pragma warning(disable: 4245)
#pragma warning(disable: 4309)
		static constexpr ElementType StorageElemBitMask = AeonMath::MakeBitMask<ElementType, StorageElemBitSize>();
		static constexpr ElementType StorageLastElemBitMask = ~AeonMath::MakeBitMask<ElementType, StorageElemBitSize - StorageLastElemBitSize>();
#pragma warning(pop)

		static constexpr IndexType StorageElemCount = (BitCountValue / StorageElemBitSize) + IndexType(StorageLastElemBitSize != 0);
		static constexpr IndexType StorageElemAlignment = IndexType(AeonMemory::PlatformVectorRegisterAlignment);

		// FIXME Enforce this on SSE platforms only
		static_assert(StorageElemAlignment >= 16);

		// TODO use allocator
		alignas(StorageElemAlignment) ElementType Storage[StorageElemCount];
	};
}

#endif