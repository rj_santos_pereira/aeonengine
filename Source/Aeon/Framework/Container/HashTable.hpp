#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_HASHTABLE
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_HASHTABLE

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Container/ContainerCommon.hpp"

#include "Aeon/Framework/Container/Tuple/Triple.hpp"

#pragma warning(push)
#pragma warning(disable: 4996)

namespace Aeon::ZzInternal
{
	template <class TableType>
	class GHashTableIterator;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CElementAdapter

	AEON_DECLARE_TAG_CLASS(THashTableRehashElement);

	template <class Type>
	concept CElementAdapter = requires
	{
		typename Type::KeyType;
		requires CScalarObject<typename Type::KeyType> && CDecayed<typename Type::KeyType>;

		typename Type::ConstKeyType;
		requires CScalarObject<typename Type::ConstKeyType> && CConst<typename Type::ConstKeyType>;

		requires CSameClass<TDecayType<typename Type::KeyType>, TDecayType<typename Type::ConstKeyType>>;

		typename Type::ElementType;
		requires CScalarObject<typename Type::ElementType>;

		typename Type::ConstElementType;
		requires CScalarObject<typename Type::ConstElementType> && CConst<typename Type::ConstElementType>;

		requires CSameClass<TDecayType<typename Type::ElementType>, TDecayType<typename Type::ConstElementType>>;

		// NOTE KeyType must be equality comparable, i.e.,
		//  the expressions 'K1 == K2' and 'K1 != K2' must be well-formed and have 'bool' type,
		//  where K1 and K2 are values of type KeyType.
		requires CEqualityComparable<typename Type::KeyType>;

		// NOTE Cannot test remaining constructors due to variadic parameters

		// Mainly used by subscript operator
		requires requires(typename Type::ElementType* ElementPtr, typename Type::KeyType const Key)
		{ { Type::Construct(ElementPtr, Key) } -> CSameClass<typename Type::ElementType*>; };
		requires requires(typename Type::ElementType* ElementPtr, typename Type::KeyType Key)
		{ { Type::Construct(ElementPtr, Move(Key)) } -> CSameClass<typename Type::ElementType*>; };

		// Mainly used by copy/move constructors and assignment operators
		requires requires(typename Type::ElementType* ElementPtr, typename Type::ConstElementType* OtherElementPtr)
		{ { Type::Construct(ElementPtr, OtherElementPtr) } -> CSameClass<typename Type::ElementType*>; };
		requires requires(typename Type::ElementType* ElementPtr, typename Type::ElementType* OtherElementPtr)
		{ { Type::Construct(ElementPtr, OtherElementPtr) } -> CSameClass<typename Type::ElementType*>; };

		// Mainly used by rehashing internals
		requires requires(typename Type::ElementType* ElementPtr, typename Type::ElementType* OtherElementPtr)
		{ { Type::Construct(ElementPtr, THashTableRehashElement::Tag, OtherElementPtr) } -> CSameClass<typename Type::ElementType*>; };

		requires requires(typename Type::ElementType* ElementPtr)
		{ { Type::Destruct(ElementPtr) } -> CVoid; };

		requires requires(typename Type::ConstElementType* ElementPtr)
		{ { Type::AccessKey(ElementPtr) } -> CSameClass<typename Type::ConstKeyType*>; };

		requires requires(typename Type::ConstElementType* ElementPtr)
		{ { Type::AccessElement(ElementPtr) } -> CSameClass<typename Type::ConstElementType*>; };
		requires requires(typename Type::ElementType* ElementPtr)
		{ { Type::AccessElement(ElementPtr) } -> CSameClass<typename Type::ElementType*>; };
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// DHashTableParams

	struct DHashTableParams
	{
		using IntParamType = uint64;
		using FloatParamType = float32;

		IntParamType InitialElementCount;
		FloatParamType LoadFactorLimit;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TFibonacciHashingFactor

	// Hash factor computed from 2^w / phi, where w is the word size, and phi is the golden ratio
	// We prefer odd numbers to preserve the first bit, so we round the factor to the nearest odd integer
	template <CUnsignedInteger HashType>
	struct TFibonacciHashingFactor;
	template <>
	struct TFibonacciHashingFactor<uint8>
	{
		static constexpr uint8 Value = 159_u8; /* 158.2167011199730811403742295976 */
	};
	template <>
	struct TFibonacciHashingFactor<uint16>
	{
		static constexpr uint16 Value = 40'503_u16; /* .475486713108771935802776986 */
	};
	template <>
	struct TFibonacciHashingFactor<uint32>
	{
		static constexpr uint32 Value = 2'654'435'769_u32; /* .4972302964775847707926 */
	};
	template <>
	struct TFibonacciHashingFactor<uint64>
	{
		static constexpr uint64 Value = 11'400'714'819'323'198'485_u64; /* .951610587622 */
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GHashTable

	template <CElementAdapter ElemAdapterType,
			  CObjectHasherForObject<TDecayType<typename ElemAdapterType::KeyType>> ElemHasherType,
			  CAllocatorForObject<TDecayType<typename ElemAdapterType::ElementType>> ElemAllocType>
	requires CMoveableElseCopyable<typename ElemAllocType::ObjectType>
	class GHashTable
	{
		// TODO experiment with SIMD for key lookup, compare performance before and after

	public:
		using ElementType = typename ElemAllocType::ObjectType;
		using ConstElementType = TAddConst<ElementType>;

		using KeyType = typename ElemHasherType::ObjectType;
		using HashType = typename ElemHasherType::HashType;
		using IndexType = typename ElemAllocType::IndexType;

		using AdapterType = ElemAdapterType;
		using HasherType = ElemHasherType;
		using AllocatorType = ElemAllocType;

		using IntParamType = uint32;
		using FloatParamType = float32;

		using IteratorType = GHashTableIterator<GHashTable>;
		using ConstIteratorType = GHashTableIterator<TAddConst<GHashTable>>;

		template <class NewAllocatorType = AllocatorType>
		explicit GHashTable(NewAllocatorType&& NewAllocator = NewAllocatorType{})
		requires CSameClass<TDecayType<NewAllocatorType>, AllocatorType>
			: Data(StorageType{}, Forward<NewAllocatorType>(NewAllocator), NewAllocator.OnContainerCopy())
		{
			ElementAllocatorType& Allocator = Data.Second();

			IndexType InitialAllocatedCount = Allocator.RecommendedStartingAllocationSize();
			Allocate_Internal(InitialAllocatedCount);
		}
		template <class NewAllocatorType = AllocatorType>
		explicit GHashTable(DHashTableParams const& Params, NewAllocatorType&& NewAllocator = NewAllocatorType{})
		requires CSameClass<TDecayType<NewAllocatorType>, AllocatorType>
			: Data(StorageType{}, Forward<NewAllocatorType>(NewAllocator), NewAllocator.OnContainerCopy())
		{
			SetLoadFactorLimit(Params.LoadFactorLimit);

			IndexType InitialAllocatedCount = IndexType(AeonMath::Ceil(Params.InitialElementCount / GetLoadFactorLimit()));
			Allocate_Internal(InitialAllocatedCount);
		}

#	if 0
		GHashTable(InitList<ElementType> InitElemList)
		requires CCopyable<ElementType>
		{
			ElementType* InitElemPtr = Aeon::InitListOps::Data(InitElemList);
			IndexType InitElemCount = Aeon::InitListOps::Count(InitElemList);

			IndexType InitialAllocatedCount = IndexType(AeonMath::Ceil(InitElemCount / GetLoadFactorLimit()));
			Allocate_Internal(InitialAllocatedCount);

			for (IndexType Index = 0; Index < InitElemCount; ++Index)
			{
				// TODO implement this somehow
			}
		}
		GHashTable(RvalRefInitList<ElementType> InitElemList)
		requires CMoveable<ElementType>
		{
			ElementType* InitElemPtr = Aeon::InitListOps::Data(InitElemList);
			IndexType InitElemCount = Aeon::InitListOps::Count(InitElemList);

			IndexType InitialAllocatedCount = IndexType(AeonMath::Ceil(InitElemCount / GetLoadFactorLimit()));
			Allocate_Internal(InitialAllocatedCount);

			for (IndexType Index = 0; Index < InitElemCount; ++Index)
			{
				// TODO implement this somehow
			}
		}
#	endif

		GHashTable(GHashTable const& Other)
		requires CCopyable<ElementType>
			: Data(StorageType{}, Other.Data.Second().OnContainerCopy(), Other.Data.Third().OnContainerCopy())
		{
			StorageType& Storage = Data.First();
			StorageType const& OtherStorage = Other.Data.First();

			IndexType OtherElemCount = OtherStorage.ElemCount;
			IndexType OtherCapCount = OtherStorage.ElemCapacity;

			Storage.LoadFactorLimitMantissa = OtherStorage.LoadFactorLimitMantissa;

			Allocate_Internal(OtherCapCount);
			if (OtherElemCount > 0)
			{
				ConstructCopyFromArray_Internal(Other);
			}
		}
		GHashTable(GHashTable&& Other)
		requires CMoveable<ElementType> || CSwappableResourceAllocator<AllocatorType>
			: Data(StorageType{}, Other.Data.Second().OnContainerMove(), Other.Data.Third().OnContainerMove())
		{
			StorageType& Storage = Data.First();
			StorageType& OtherStorage = Other.Data.First();

			if constexpr (CSwappableResourceAllocator<AllocatorType>)
			{
				Storage = Exchange(OtherStorage, StorageType{});
			}
			else
			{
				IndexType OtherElemCount = OtherStorage.ElemCount;
				IndexType OtherCapCount = OtherStorage.ElemCapacity;

				Storage.LoadFactorLimitMantissa = OtherStorage.LoadFactorLimitMantissa;

				Allocate_Internal(OtherCapCount);
				if (OtherElemCount > 0)
				{
					ConstructMoveFromArray_Internal(Other);
				}

				// Simulate move-like behavior on Other
				Other.Deallocate_Internal();
			}
		}

		~GHashTable()
		{
			Destruct_Internal();
			Deallocate_Internal();
		}

		GHashTable& operator=(GHashTable const& Other)
		requires CCopyable<ElementType>
		{
			AEON_FAILSAFE(this != addressof(Other), return *this);

			StorageType const& OtherStorage = Other.Data.First();

			AllocatorType& Allocator = Data.Second();
			AllocatorType const& OtherAllocator = Other.Data.Second();

			SlotAllocatorType& SlotAllocator = Data.Third();
			SlotAllocatorType const& OtherSlotAllocator = Other.Data.Third();

			IndexType OtherElemCount = OtherStorage.ElemCount;
			IndexType OtherCapCount = OtherStorage.ElemCapacity;

			Destruct_Internal();

			// NOTE Technically we could unconditionally always execute this branch and the logic would be correct,
			//  but perhaps less efficient in some cases
			if (Allocator != OtherAllocator)
			{
				Deallocate_Internal();
				Allocator = OtherAllocator.OnContainerCopy();
				SlotAllocator = OtherSlotAllocator.OnContainerCopy();
				Allocate_Internal(OtherCapCount);
			}
			else
			{
				Reallocate_Internal(OtherCapCount);
			}

			if (OtherElemCount > 0)
			{
				ConstructCopyFromArray_Internal(Other);
			}

			return *this;
		}
		GHashTable& operator=(GHashTable&& Other)
		requires CMoveable<ElementType> || CSwappableResourceAllocator<AllocatorType>
		{
			AEON_FAILSAFE(this != addressof(Other), return *this);

			[[maybe_unused]]
			StorageType& Storage = Data.First();
			StorageType& OtherStorage = Other.Data.First();

			AllocatorType& Allocator = Data.Second();
			AllocatorType& OtherAllocator = Other.Data.Second();

			SlotAllocatorType& SlotAllocator = Data.Third();
			SlotAllocatorType& OtherSlotAllocator = Other.Data.Third();

			IndexType OtherElemCount = OtherStorage.ElemCount;
			IndexType OtherCapCount = OtherStorage.ElemCapacity;

			Destruct_Internal();

			if constexpr (CSwappableResourceAllocator<AllocatorType>)
			{
				Deallocate_Internal();

				if (Allocator != OtherAllocator)
				{
					Allocator = OtherAllocator.OnContainerMove();
					SlotAllocator = OtherSlotAllocator.OnContainerMove();
				}

				Storage = Exchange(OtherStorage, StorageType{});
			}
			else
			{
				if (Allocator != OtherAllocator)
				{
					Deallocate_Internal();
					Allocator = OtherAllocator.OnContainerMove();
					SlotAllocator = OtherSlotAllocator.OnContainerMove();
					Allocate_Internal(OtherCapCount);
				}
				else
				{
					Reallocate_Internal(OtherCapCount);
				}

				if (OtherElemCount > 0)
				{
					ConstructMoveFromArray_Internal(Other);
				}

				// Simulate move-like behavior on Other
				Other.Deallocate_Internal();
			}

			return *this;
		}

		void Reserve(IndexType NewElementCount)
		{
			CheckIncreaseAllocation_Internal(NewElementCount);
		}
		void Prune()
		{
			CheckDecreaseAllocation_Internal();
		}

		void Empty()
		{
			Destruct_Internal();
		}
		void Empty(TReclaimStorage)
		{
			Destruct_Internal();

			// Forcefully deallocate all memory
			Deallocate_Internal();
		}

		GHashTable& Append(GHashTable const& Other)
		requires CCopyable<ElementType>
		{
			AEON_FAILSAFE(this != addressof(Other), return *this);

			StorageType& Storage = Data.First();
			StorageType const& OtherStorage = Other.Data.First();

			IndexType OtherElemCount = OtherStorage.ElemCount;

			if (OtherElemCount > 0)
			{
				IndexType NewElemCount = Storage.ElemCount + OtherElemCount;

				CheckIncreaseAllocation_Internal(NewElemCount);
				AppendCopyFromArray_Internal(Other);
			}

			return *this;
		}
		GHashTable& Append(GHashTable&& Other)
		requires CMoveable<ElementType>
		{
			AEON_FAILSAFE(this != addressof(Other), return *this);

			StorageType& Storage = Data.First();
			StorageType& OtherStorage = Other.Data.First();

			IndexType OtherElemCount = OtherStorage.ElemCount;

			if (OtherElemCount > 0)
			{
				IndexType NewElemCount = Storage.ElemCount + OtherElemCount;

				CheckIncreaseAllocation_Internal(NewElemCount);
				AppendMoveFromArray_Internal(Other);

				// Simulate move-like behavior on Other
				Other.Deallocate_Internal();
			}

			return *this;
		}

		template <class NewKeyType, class ... ArgTypes>
		ElementType& AddGeneric(NewKeyType&& Key, ArgTypes&&... ValArgs)
		requires CMoveableElseCopyable<KeyType> && CSameClass<KeyType, TDecayType<NewKeyType>>
		{
			CheckIncreaseAllocation_Internal();

			return *Add_Internal(Forward<NewKeyType>(Key), Forward<ArgTypes>(ValArgs)...);
		}

		template <class NewKeyType, class ... ArgTypes>
		ElementType& FindElseAddGeneric(NewKeyType&& Key, ArgTypes&&... ValArgs)
		requires CMoveableElseCopyable<KeyType> && CSameClass<KeyType, TDecayType<NewKeyType>>
		{
			StorageType const& Storage = Data.First();

			auto SlotInfo = FindSlot_Internal(Key);

			ElementType* ElemPtr;
			if (SlotInfo.HasRequestedElement)
			{
				ElemPtr = AdapterType::AccessElement(Storage.BufferPtr + SlotInfo.Index);
			}
			else
			{
				IndexType CachedElemCapacity = Storage.ElemCapacity;

				CheckIncreaseAllocation_Internal();

				// If the capacity did not change, we can reuse SlotInfo from the element search
				if (Storage.ElemCapacity == CachedElemCapacity)
				{
					ElemPtr = Add_Internal(SlotInfo,
										   Forward<NewKeyType>(Key), Forward<ArgTypes>(ValArgs)...);
				}
				// Otherwise, we'll have to search for the slot again
				else
				{
					ElemPtr = Add_Internal(FindSlot_Internal(Key, SlotInfo.Hash, 0),
										   Forward<NewKeyType>(Key), Forward<ArgTypes>(ValArgs)...);
				}
			}

			return *ElemPtr;
		}

		bool Remove(KeyType const& Key)
		{
			return Remove_Internal(Key);
		}
		bool Remove(KeyType const& Key, TReclaimStorage)
		{
			bool HasRemoved = Remove_Internal(Key);

			CheckDecreaseAllocation_Internal();

			return HasRemoved;
		}

		bool Contains(KeyType const& Key) const
		{
			return bool(Find(Key));
		}

		GOptionalRef<ConstElementType> Find(KeyType const& Key) const
		{
			auto SlotInfo = FindSlot_Internal(Key);

			if (SlotInfo.HasRequestedElement)
			{
				StorageType const& Storage = Data.First();

				auto ElemPtr = AdapterType::AccessElement(Storage.BufferPtr + SlotInfo.Index);
				return { *ElemPtr };
			}

			return {};
		}
		GOptionalRef<ElementType> Find(KeyType const& Key)
		{
			return ZZINTERNAL_AEON_INVOKE_CONST_OPTIONALREF_OVERLOAD(Find, Key);
		}

		void Relink()
		{
			// NOTE HashTable keeps an internal linked-list of elements for better performance in iteration:
			// Slot 0 is always the head of the list (it may or may not be an element itself, checked through PSL),
			// and every time an element is added to the table, its slot is added to the top of the list, after head (head points to it);
			// likewise, any time an element is removed from the table, after moving elements, the newly empty slot is unlinked from the list.
			// This list allows for potentially faster iteration for N << C, where N is the number of elements and C is the capacity,
			// but for tables where N approaches C, it may still have worse performance than a sequential search through the array,
			// as the elements are iterated in the order of insertion, rather than sequentially,
			// which may cause several cache misses depending on how the elements are arranged within the array.
			// In those situations, and/or if iteration is far more common than adding/removing elements,
			// Relink() may be used to link the existing elements following sequential indexes, which a O(C) operation.

			StorageType& Storage = Data.First();

			IndexType LastRelinkedSlotIndex = 0;

			SlotType& FirstSlot = Storage.SlotBufferPtr[0];
			FirstSlot.PrevSlotIdx = 0;

			for (IndexType SlotIdx = 0; SlotIdx < Storage.ElemCapacity; ++SlotIdx)
			{
				SlotType& Slot = Storage.SlotBufferPtr[SlotIdx];
				if (Slot.ProbeSeqLen != 0)
				{
					Slot.PrevSlotIdx = LastRelinkedSlotIndex;

					SlotType& LastRelinkedSlot = Storage.SlotBufferPtr[LastRelinkedSlotIndex];
					LastRelinkedSlot.NextSlotIdx = SlotIdx;

					LastRelinkedSlotIndex = SlotIdx;
				}
			}

			SlotType& LastSlot = Storage.SlotBufferPtr[LastRelinkedSlotIndex];
			LastSlot.NextSlotIdx = NullSlotIndex;
		}

		// TODO implement Filter, Map, Fold, AsArray, AsDearray

		FloatParamType GetLoadFactor() const
		{
			StorageType const& Storage = Data.First();

			return FloatParamType(Storage.ElemCount) / FloatParamType(Storage.ElemCapacity);
		}

		FloatParamType GetLoadFactorLimit() const
		{
			StorageType const& Storage = Data.First();

			//return Storage.LoadFactorLimit;

			// NOTE We want the actual exponent to be -1, so we subtract the bias from that value to determine what the biased exponent should be.
			FloatParamLayoutType LoadFactorLimit{ .Mantissa = Storage.LoadFactorLimitMantissa,
												  .Exponent = -1 - FloatParamLayoutType::ExponentBias,
												  .Sign = 0 };
			return AeonMath::ComposeFloat<FloatParamType>(LoadFactorLimit);
		}
		void SetLoadFactorLimit(FloatParamType NewLimit)
		{
			StorageType& Storage = Data.First();

			// This effectively clamps LoadFactorLimit to the range [0.5, 1.0[
			// Note that since all numbers in the range have the same exponent, this simply stores the mantissa (23 bits)

			// NOTE Lower bound is limited to 0.5 because at most, each allocation increase doubles the previous size,
			//  due to NextPowerOfTwo
			constexpr FloatParamType LimitLowerBound = 0.5f;
			// NOTE Subtracting by Epsilon/2 is equivalent to std::nextafter(1, 0),
			//  since Epsilon is the distance between 1 and the next representable number,
			//  and because 1 has mantissa equal to zero,
			//  the previous representable value (down to 0.5) has double the precision of values between 1 and 2
			constexpr FloatParamType LimitUpperBound = 1.0f - TNumericProperties<FloatParamType>::Epsilon / 2.0f;

			//Storage.LoadFactorLimit = AeonMath::Clamp(NewLimit, LimitLowerBound, LimitUpperBound);

			// CHECKME Should we conditionally rehash the map if over the new limit?

			AEON_FAILSAFE(AeonMath::IsClamped(NewLimit, LimitLowerBound, LimitUpperBound),
						  NewLimit = AeonMath::Clamp(NewLimit, LimitLowerBound, LimitUpperBound),
						  "GHashTable: LoadFactorLimit must be in the range of [0.5, 1.0[");
			Storage.LoadFactorLimitMantissa = AeonMath::DecomposeFloat(NewLimit).Mantissa;
		}

		ConstElementType* Buffer() const
		{
			return Data.First().BufferPtr;
		}
		ElementType* Buffer()
		{
			return AEON_INVOKE_CONST_OVERLOAD(Buffer);
		}
		
		IndexType AllocatedCount() const
		{
			return Data.First().ElemCapacity;
		}
		IndexType Count() const
		{
			return Data.First().ElemCount;
		}

		bool IsEmpty() const
		{
			return Count() == 0;
		}

		AllocatorType const& GetAllocator() const
		{
			return Data.Second();
		}

#	if 0
		voidptr DataBuffer_Internal(szint BufferIndex) const
		{
			AEON_FAILSAFE(BufferIndex <= 1, return nullptr);
			switch(BufferIndex)
			{
				case 0: return Data.First().SlotBufferPtr;
				case 1: return Data.First().BufferPtr;
			}
		}
		szint DataBufferCount_Internal() const
		{
			return 2;
		}
		szint DataElementSize_Internal(szint BufferIndex) const
		{
			AEON_FAILSAFE(BufferIndex <= 1, return 0);
			switch(BufferIndex)
			{
				case 0: return Aeon::SizeOf(SlotType);
				case 1: return Aeon::SizeOf(ElementType);
			}
		}
		szint DataElementCount_Internal() const
		{
			return Count();
		}
		szint DataElementCapacity_Internal() const
		{
			return AllocatedCount();
		}
#	endif

		AEON_FORCEINLINE ConstIteratorType begin() const
		{
			SlotType const& FirstSlot = Data.First().SlotBufferPtr[0];
			IndexType FirstIndex = FirstSlot.ProbeSeqLen ? 0 : FirstSlot.NextSlotIdx;
			return ConstIteratorType{ this, FirstIndex };
		}
		AEON_FORCEINLINE IteratorType begin()
		{
			SlotType const& FirstSlot = Data.First().SlotBufferPtr[0];
			IndexType FirstIndex = FirstSlot.ProbeSeqLen ? 0 : FirstSlot.NextSlotIdx;
			return IteratorType{ this, FirstIndex };
		}
		AEON_FORCEINLINE ConstIteratorType cbegin() const
		{
			return begin();
		}

		AEON_FORCEINLINE ConstIteratorType end() const
		{
			return ConstIteratorType{ this, NullSlotIndex };
		}
		AEON_FORCEINLINE IteratorType end()
		{
			return IteratorType{ this, NullSlotIndex };
		}
		AEON_FORCEINLINE ConstIteratorType cend() const
		{
			return end();
		}

		friend void Swap(GHashTable& Array1, GHashTable& Array2)
		requires CSwappableResourceAllocator<AllocatorType>
		{
			StorageType& Storage1 = Array1.Data.First();
			StorageType& Storage2 = Array2.Data.First();

			ElementAllocatorType& Allocator1 = Array1.Data.Second();
			ElementAllocatorType& Allocator2 = Array2.Data.Second();

			SlotAllocatorType& SlotAllocator1 = Array1.Data.Third();
			SlotAllocatorType& SlotAllocator2 = Array2.Data.Third();

			using Aeon::Swap;

			Swap(Storage1, Storage2);
			Swap(Allocator1, Allocator2);
			Swap(SlotAllocator1, SlotAllocator2);
		}

	private:
		// Internal type-aliases

		struct DSlotStorage;
		struct DContainerStorage;

		using SlotType = DSlotStorage;
		using StorageType = DContainerStorage;

		using ElementPtrType = TAddPointer<typename ElemAllocType::ObjectType>;
		using SlotPtrType = TAddPointer<SlotType>;

		using ElementAllocatorType = ElemAllocType;
		using SlotAllocatorType = TRebindAllocator<ElemAllocType, SlotType>;

		using FloatParamLayoutType = AeonMath::TDetectFloatNumLayout<float32>;

		// Internal types

		struct DFindSlotResult
		{
			HashType Hash;
			IndexType PSL;
			IndexType Index;
			bool HasRequestedElement : 1;
			bool IsEmpty : 1;
		};

#if 0
		// CHECKME Consider making this dynamic instead, with the log2(bits(IndexType)) value being the upper bound
		static constexpr szint ProbeSeqLenBitSize = AeonMath::LogPowerOfTwo(Aeon::BitSizeOf(typename ElemAllocType::IndexType));
		static constexpr szint HashSliceBitSize = Aeon::BitSizeOf(uint16) - ProbeSeqLenBitSize;

		struct alignas(uint16) DSlotSliceStorage
		{
			uint16 HashLoSlice : HashSliceBitSize;
			uint16 ProbeSeqLen : ProbeSeqLenBitSize;
		};

		static_assert(TIsImplicitLifetimeObject<SlotType>::Value);

		struct DSlotStorage
		{
			using HashType = typename ElemHasherType::HashType;
			using IndexType = typename ElemAllocType::IndexType;
			using ObjectType = typename ElemAllocType::ObjectType;

			AeonMemory::GStorage<ObjectType> ObjStorage;
			HashType HashHiSlice : Aeon::BitSizeOf(HashType) - HashSliceBitSize;
			IndexType ProbeSeqLen : ProbeSeqLenBitSize;
		};

		static_assert(TIsImplicitLifetimeObject<DSlotData>::Value);
#endif

		struct DSlotStorage
		{
			HashType Hash = 0;
			IndexType ProbeSeqLen = 0;
			IndexType NextSlotIdx = 0;
			IndexType PrevSlotIdx = 0;
		};

		struct DContainerStorage
		{
			SlotPtrType SlotBufferPtr = nullptr;
			ElementPtrType BufferPtr = nullptr;

			IndexType ElemCount = 0;
			IndexType ElemCapacity = 0;

		//	FloatParamType LoadFactorLimit = 0.7f;
			IntParamType LoadFactorLimitMantissa : FloatParamLayoutType::MantissaBitCount = AeonMath::DecomposeFloat(0.7f).Mantissa;
			IntParamType CachedHashShift : 6 = 63;
		//	IntParamType CachedPSLBound : 6;
		};

		// Internal fields

		GCompressedTriple<StorageType, ElementAllocatorType, SlotAllocatorType> Data;

		static constexpr IndexType NullSlotIndex = IndexType(-1);

		template <class OtherTableType>
		friend class GHashTableIterator;

		// Additional compile-time checks

		static_assert(TIsImplicitLifetimeObject<SlotType>::Value);

		static_assert(TNumericProperties<HashType>::Maximum >= TNumericProperties<IndexType>::Maximum,
					  "'GHashTable<AdapterType, HasherType, AllocType>': "
					  "TNumericProperties<HasherType::HashType>::Maximum must be greater or equal than "
					  "TNumericProperties<AllocType::IndexType>::Maximum.");

	//	static_assert(CRebindableAllocator<ElemAllocType, DSlotData>,
	//				  "'GHashTable<KeyType, ValueType, HasherType, AllocType>': "
	//				  "GHashTable requires an allocator that can rebind to internal DSlotData type.");
		static_assert(CRebindableAllocator<ElemAllocType, SlotType>,
					  "'GHashTable<AdapterType, HasherType, AllocType>': "
					  "GHashTable requires an allocator that can rebind to internal SlotType type.");

		AEON_FORCEINLINE IndexType MapHashToIndex_Internal(HashType Hash) const
		{
			StorageType const& Storage = Data.First();

			HashType HashShift = HashType(Storage.CachedHashShift);
			// Uncomment if we ever want to make it more input-independent (with a performance penalty)
		//	Hash ^= Hash >> HashShift;
			IndexType Index = IndexType((Hash * TFibonacciHashingFactor<HashType>::Value) >> HashShift);
			return Index;
		}

		void Allocate_Internal(IndexType InitElementCapacity)
		{
			StorageType& Storage = Data.First();
			ElementAllocatorType& Allocator = Data.Second();
			SlotAllocatorType& SlotAllocator = Data.Third();

			InitElementCapacity = AeonMath::NextPowerOfTwo(InitElementCapacity);
			if (InitElementCapacity)
			{
				// NOTE AllocatedCount count must be 2 at an absolute minimum, otherwise MapHashToIndex_Internal will have UB
				InitElementCapacity = AeonMath::Max<IndexType>(InitElementCapacity, 8);
			}

			AEON_ASSERT(SlotAllocator.IsAllocationSizeSupported(InitElementCapacity)
						&& Allocator.IsAllocationSizeSupported(InitElementCapacity));

			SlotPtrType InitSlotBufferPtr = SlotAllocator.Allocate(InitElementCapacity);
			ElementPtrType InitBufferPtr = Allocator.Allocate(InitElementCapacity);

			if (InitElementCapacity)
			{
				AEON_ASSERT(InitSlotBufferPtr && InitBufferPtr);

				AeonMemory::MemZero(InitSlotBufferPtr, Aeon::SizeOf(SlotType, InitElementCapacity));
				// NOTE Slot 0 is always the head of the single-linked list, as it simplifies this initialization code;
				// the next index is initialized with NullSlotIndex, which is used to mark the tail of the list
				InitSlotBufferPtr->NextSlotIdx = NullSlotIndex;
			}

			Storage.SlotBufferPtr = InitSlotBufferPtr;
			Storage.BufferPtr = InitBufferPtr;
			Storage.ElemCapacity = InitElementCapacity;

			Storage.CachedHashShift = Aeon::BitSizeOf(HashType) - (InitElementCapacity ? AeonMath::LogPowerOfTwo(InitElementCapacity) : 1);
		}
		void Reallocate_Internal(IndexType NewElementCapacity)
		{
			StorageType& Storage = Data.First();
			ElementAllocatorType& Allocator = Data.Second();
			SlotAllocatorType& SlotAllocator = Data.Third();

			NewElementCapacity = AeonMath::NextPowerOfTwo(NewElementCapacity);
			if (NewElementCapacity)
			{
				NewElementCapacity = AeonMath::Max<IndexType>(NewElementCapacity, 8);
			}

			AEON_ASSERT(SlotAllocator.IsAllocationSizeSupported(NewElementCapacity)
						&& Allocator.IsAllocationSizeSupported(NewElementCapacity));

			SlotPtrType NewSlotBufferPtr = SlotAllocator.Allocate(NewElementCapacity);
			ElementPtrType NewBufferPtr = Allocator.Allocate(NewElementCapacity);

			if (NewElementCapacity)
			{
				AEON_ASSERT(NewSlotBufferPtr && NewBufferPtr);

				AeonMemory::MemZero(NewSlotBufferPtr, Aeon::SizeOf(SlotType, NewElementCapacity));
				NewSlotBufferPtr->NextSlotIdx = NullSlotIndex;
			}

			SlotPtrType OldSlotBufferPtr = Exchange(Storage.SlotBufferPtr, NewSlotBufferPtr);
			ElementPtrType OldBufferPtr = Exchange(Storage.BufferPtr, NewBufferPtr);
			IndexType OldElementCapacity = Exchange(Storage.ElemCapacity, NewElementCapacity);

			Storage.CachedHashShift = Aeon::BitSizeOf(HashType) - (NewElementCapacity ? AeonMath::LogPowerOfTwo(NewElementCapacity) : 1);

			if (NewElementCapacity)
			{
				ReHash_Internal(OldSlotBufferPtr, OldBufferPtr);
			}

			if (OldElementCapacity)
			{
				SlotAllocator.Deallocate(OldSlotBufferPtr);
				Allocator.Deallocate(OldBufferPtr);
			}
		}
		void Deallocate_Internal()
		{
			StorageType& Storage = Data.First();
			ElementAllocatorType& Allocator = Data.Second();
			SlotAllocatorType& SlotAllocator = Data.Third();

			if (Storage.BufferPtr)
			{
				Allocator.Deallocate(Storage.BufferPtr);
				SlotAllocator.Deallocate(Storage.SlotBufferPtr);

				Storage.SlotBufferPtr = nullptr;
				Storage.BufferPtr = nullptr;
				Storage.ElemCapacity = 0;
				Storage.CachedHashShift = Aeon::BitSizeOf(HashType) - 1;
			}
		}

		void ReHash_Internal(SlotPtrType OldSlotBufferPtr, ElementPtrType OldBufferPtr)
		{
			StorageType& Storage = Data.First();

			IndexType RehashedElemCount = 0;
			for (IndexType Index = 0; RehashedElemCount < Storage.ElemCount; ++Index)
			{
				SlotType& OldSlot = OldSlotBufferPtr[Index];
				if (OldSlot.ProbeSeqLen)
				{
					ElementType* OldElemPtr = OldBufferPtr + Index;

					auto SlotInfo = FindSlot_Internal(*AdapterType::AccessKey(OldElemPtr));

					AEON_ASSERT(SlotInfo.PSL);

					ElementType* NewElemPtr = Storage.BufferPtr + SlotInfo.Index;

					SlotType& NewSlot = Storage.SlotBufferPtr[SlotInfo.Index];

					// If the slot is empty, link the slot to the internal linked-list
					if (SlotInfo.IsEmpty)
					{
						LinkSlot_Internal(SlotInfo.Index);
					}
					// Otherwise, if the slot is not empty, evict the element
					// Note that if an element is evicted, then its slot is already linked to the list
					else
					{
						ReAdd_Internal(NewElemPtr, NewSlot.Hash, NewSlot.ProbeSeqLen);
					}
					
					NewSlot.Hash = SlotInfo.Hash;
					NewSlot.ProbeSeqLen = SlotInfo.PSL;

					AdapterType::Construct(NewElemPtr, THashTableRehashElement::Tag, OldElemPtr);
					AdapterType::Destruct(OldElemPtr);

					++RehashedElemCount;
				}
			}
		}

		void ConstructCopyFromArray_Internal(GHashTable const& Other)
		{
			StorageType& Storage = Data.First();
			StorageType const& OtherStorage = Other.Data.First();

			IndexType OtherElemCount = OtherStorage.ElemCount;

			IndexType ConstructedElemCount = 0;
			for (IndexType Index = 0; ConstructedElemCount < OtherElemCount; ++Index)
			{
				SlotType& OtherSlot = OtherStorage.SlotBufferPtr[Index];
				if (OtherSlot.ProbeSeqLen)
				{
					Storage.SlotBufferPtr[Index] = OtherSlot;

					ElementType* ElemPtr = Storage.BufferPtr + Index;
					ConstElementType* OtherElemPtr = AdapterType::AccessElement(OtherStorage.BufferPtr + Index);

					AdapterType::Construct(ElemPtr, OtherElemPtr);

					++ConstructedElemCount;
				}
			}

			Storage.ElemCount = OtherElemCount;
		}
		void ConstructMoveFromArray_Internal(GHashTable& Other)
		{
			StorageType& Storage = Data.First();
			StorageType& OtherStorage = Other.Data.First();

			IndexType OtherElemCount = OtherStorage.ElemCount;

			IndexType ConstructedElemCount = 0;
			for (IndexType Index = 0; ConstructedElemCount < OtherElemCount; ++Index)
			{
				SlotType& OtherSlot = OtherStorage.SlotBufferPtr[Index];
				if (OtherSlot.ProbeSeqLen)
				{
					SlotType& Slot = Storage.SlotBufferPtr[Index];
					Slot = Exchange(OtherSlot, SlotType{});

					ElementType* ElemPtr = Storage.BufferPtr + Index;
					ElementType* OtherElemPtr = AdapterType::AccessElement(OtherStorage.BufferPtr + Index);

					AdapterType::Construct(ElemPtr, OtherElemPtr);
					AdapterType::Destruct(OtherElemPtr);

					++ConstructedElemCount;
				}
			}

			Storage.ElemCount = OtherElemCount;
			OtherStorage.ElemCount = 0;
		}
		void AppendCopyFromArray_Internal(GHashTable const& Other)
		{
			StorageType& Storage = Data.First();
			StorageType const& OtherStorage = Other.Data.First();

			IndexType OtherElemCount = OtherStorage.ElemCount;

			IndexType AppendedElemCount = 0;
			for (IndexType Index = 0; AppendedElemCount < OtherElemCount; ++Index)
			{
				SlotType& OtherSlot = OtherStorage.SlotBufferPtr[Index];
				if (OtherSlot.ProbeSeqLen)
				{
					KeyType const* OtherKeyPtr = AdapterType::AccessKey(OtherStorage.BufferPtr + Index);
					ConstElementType* OtherElemPtr = AdapterType::AccessElement(OtherStorage.BufferPtr + Index);

					auto SlotInfo = FindSlot_Internal(*OtherKeyPtr);

					AEON_ASSERT(SlotInfo.PSL);

					ElementType* ElemPtr = Storage.BufferPtr + SlotInfo.Index;

					// If we found a slot that is not occupied with this key,
					// it is either empty or occupied with an element with lower PSL, so construct in this slot
					if (!SlotInfo.HasRequestedElement)
					{
						SlotType& Slot = Storage.SlotBufferPtr[SlotInfo.Index];

						// If the slot is empty, link the slot to the internal linked-list
						if (SlotInfo.IsEmpty)
						{
							LinkSlot_Internal(SlotInfo.Index);
						}
						// Otherwise, if the slot is not empty, evict the element
						// Note that if an element is evicted, then its slot is already linked to the list
						else
						{
							ReAdd_Internal(ElemPtr, Slot.Hash, Slot.ProbeSeqLen);
						}

						Slot.Hash = SlotInfo.Hash;
						Slot.ProbeSeqLen = SlotInfo.PSL;

						AdapterType::Construct(ElemPtr, OtherElemPtr);

						++Storage.ElemCount;
					}

					++AppendedElemCount;
				}
			}
		}
		void AppendMoveFromArray_Internal(GHashTable& Other)
		{
			StorageType& Storage = Data.First();
			StorageType& OtherStorage = Other.Data.First();

			IndexType OtherElemCount = OtherStorage.ElemCount;

			IndexType AppendedElemCount = 0;
			for (IndexType Index = 0; AppendedElemCount < OtherElemCount; ++Index)
			{
				SlotType& OtherSlot = OtherStorage.SlotBufferPtr[Index];
				if (OtherSlot.ProbeSeqLen)
				{
					KeyType const* OtherKeyPtr = AdapterType::AccessKey(OtherStorage.BufferPtr + Index);
					ElementType* OtherElemPtr = AdapterType::AccessElement(OtherStorage.BufferPtr + Index);

					auto SlotInfo = FindSlot_Internal(*OtherKeyPtr);

					AEON_ASSERT(SlotInfo.PSL);

					ElementType* ElemPtr = Storage.BufferPtr + SlotInfo.Index;

					// If we found a slot that is not occupied with this key,
					// it is either empty or occupied with an element with lower PSL, so construct in this slot
					if (!SlotInfo.HasRequestedElement)
					{
						SlotType& Slot = Storage.SlotBufferPtr[SlotInfo.Index];

						// If the slot is empty, link the slot to the internal linked-list
						if (SlotInfo.IsEmpty)
						{
							LinkSlot_Internal(SlotInfo.Index);
						}
						// Otherwise, if the slot is not empty, evict the element
						// Note that if an element is evicted, then its slot is already linked to the list
						else
						{
							ReAdd_Internal(ElemPtr, Slot.Hash, Slot.ProbeSeqLen);
						}

						Slot.Hash = SlotInfo.Hash;
						Slot.ProbeSeqLen = SlotInfo.PSL;

						AdapterType::Construct(ElemPtr, OtherElemPtr);

						++Storage.ElemCount;
					}

					AdapterType::Destruct(OtherElemPtr);
					OtherSlot = SlotType{};

					++AppendedElemCount;
				}
			}

			OtherStorage.ElemCount = 0;
		}
		void Destruct_Internal()
		{
			StorageType& Storage = Data.First();

			for (IndexType Index = 0; Storage.ElemCount > 0; ++Index)
			{
				SlotType& Slot = Storage.SlotBufferPtr[Index];
				if (Slot.ProbeSeqLen)
				{
					Slot = SlotType{};

					AdapterType::Destruct(Storage.BufferPtr + Index);

					--Storage.ElemCount;
				}
			}
		}

		AEON_FORCEINLINE DFindSlotResult FindSlot_Internal(KeyType const& Key) const
		{
			return FindSlot_Internal(Key, HasherType::Hash(Key), 0);
		}
		DFindSlotResult FindSlot_Internal(KeyType const& Key, HashType KeyHash, IndexType KeyPSL) const
		{
			// NOTE This implementation uses Robin Hood hashing for collision resolution, which assigns a value,
			//  known as Probe Sequence Length, or PSL, to each slot in the table. 
			//  When searching a slot, from the home index of a given key, which is attributed PSL == 1, 
			//  we try to find a slot whose PSL is lower than the current key's PSL, 
			//  incrementing the PSL as it moves farther from its home slot. 
			//  Empty slots are attributed PSL == 0, thus they are always preferred.
			//  If a slot is found which is populated by a key with lower PSL than the current key's, 
			//  the key in that slot is evicted, the current key is placed in the slot,
			//  and the evicted key is re-added by searching for another slot,
			//  starting at the index/PSL of the slot that it occupied.

			StorageType const& Storage = Data.First();

			IndexType KeyIndex = MapHashToIndex_Internal(KeyHash);

			// Either start the search from the key's home index (KeyPSL == 0), 
			// or from the index N after the slot that the key used to occupy (KeyPSL == N), since the key was evicted.  
			for (IndexType Index = KeyPSL; Index < Storage.ElemCapacity; ++Index)
			{
				// Adjust PSL of the current Index, zero belongs to empty slots only
				++KeyPSL;

				IndexType SlotIndex = AeonMath::ModPowerOfTwo(KeyIndex + Index, Storage.ElemCapacity);
				SlotType const& Slot = Storage.SlotBufferPtr[SlotIndex];

				// Slot is either not occupied (PSL == 0) or occupied with a key with lower PSL, no element with this key, return bucket info
				if (KeyPSL > Slot.ProbeSeqLen)
				{
					// NOTE If the key was present, it would have the same KeyIndex, and thus equal PSL;
					//  note also that if the key was evicted (e.g. from this slot) at some point, its PSL would also have been updated,
					//  and the other key which had occupied this slot would have greater PSL than the current key's one (KeyPSL)
					//  (otherwise, the eviction would not have happened), so this branch would not be taken.
					bool IsEmpty = (Slot.ProbeSeqLen == 0);
					return DFindSlotResult{ .Hash = KeyHash, .PSL = KeyPSL, .Index = SlotIndex, .HasRequestedElement = false, .IsEmpty = IsEmpty };
				}
				// Slot is occupied with a key with equal PSL, check hash
				else if (KeyPSL == Slot.ProbeSeqLen && KeyHash == Slot.Hash)
				{
					// Since the hash is the same, check key for equality, they will almost always be equal
					auto ElemPtr = AdapterType::AccessKey(Storage.BufferPtr + SlotIndex);
					bool IsSameKey = GEqualTo<KeyType>()(*ElemPtr, Key);
					AEON_FAILSAFE(IsSameKey, void(),
								  "GHashTable: Though it is possible for the keys to be different, it is extraordinarily improbable, "
								  "so this is likely a bug in the key's comparison operator, review the code. "
								  "If the comparison logic is correct, you may ignore this warning.");
					// Slot is occupied with this key, return bucket info
					if (IsSameKey)
					{
						return DFindSlotResult{ .Hash = KeyHash, .PSL = KeyPSL, .Index = SlotIndex, .HasRequestedElement = true, .IsEmpty = false };
					}
				}

				// Otherwise, continue search, as we have lower PSL than the current key.
			}

			AEON_FAILSAFE(false, void(), "Should never return from here");
			return { .Hash = 0, .PSL = 0, .Index = 0, .HasRequestedElement = false, .IsEmpty = false };
		}

		template <class NewKeyType, class ... NewValArgTypes>
		AEON_FORCEINLINE ElementType* Add_Internal(NewKeyType&& Key, NewValArgTypes&&... ValArgs)
		{
			auto SlotInfo = FindSlot_Internal(Key);

			AEON_ASSERT(SlotInfo.PSL);

			return Add_Internal(SlotInfo, Forward<NewKeyType>(Key), Forward<NewValArgTypes>(ValArgs)...);
		}
		template <class NewKeyType, class ... NewValArgTypes>
		ElementType* Add_Internal(DFindSlotResult SlotInfo, NewKeyType&& Key, NewValArgTypes&&... ValArgs)
		{
			StorageType& Storage = Data.First();

			ElementType* ElemPtr = Storage.BufferPtr + SlotInfo.Index;

			// If we found a slot that is not occupied with this key,
			// it is either empty or occupied with an element with lower PSL, so construct in this slot
			if (!SlotInfo.HasRequestedElement)
			{
				SlotType& Slot = Storage.SlotBufferPtr[SlotInfo.Index];

				// If the slot is empty, link the slot to the internal linked-list
				if (SlotInfo.IsEmpty)
				{
					LinkSlot_Internal(SlotInfo.Index);
				}
				// Otherwise, if the slot is not empty, evict the element
				// Note that if an element is evicted, then its slot is already linked to the list
				else
				{
					ReAdd_Internal(ElemPtr, Slot.Hash, Slot.ProbeSeqLen);
				}

				Slot.Hash = SlotInfo.Hash;
				Slot.ProbeSeqLen = SlotInfo.PSL;

				ElemPtr = AdapterType::Construct(ElemPtr, Forward<NewKeyType>(Key), Forward<NewValArgTypes>(ValArgs)...);

				++Storage.ElemCount;
			}
			// Otherwise, if the slot is occupied with this key, overwrite the element with the new arguments
			else
			{
				AdapterType::Destruct(ElemPtr);

				ElemPtr = AdapterType::Construct(ElemPtr, Forward<NewKeyType>(Key), Forward<NewValArgTypes>(ValArgs)...);
			}

			return ElemPtr;
		}

		void ReAdd_Internal(ElementType* EvictedElemPtr, HashType EvictedElemHash, IndexType EvictedElemPSL)
		{
			StorageType& Storage = Data.First();

			auto SlotInfo = FindSlot_Internal(*AdapterType::AccessKey(EvictedElemPtr), EvictedElemHash, EvictedElemPSL);

			AEON_ASSERT(SlotInfo.PSL);

			// If we found a slot, it is either empty or occupied with a different key
			AEON_ASSERT(!SlotInfo.HasRequestedElement);

			ElementType* ElemPtr = Storage.BufferPtr + SlotInfo.Index;

			SlotType& Slot = Storage.SlotBufferPtr[SlotInfo.Index];
			
			// If the slot is empty, link the slot to the internal linked-list
			if (SlotInfo.IsEmpty)
			{
				LinkSlot_Internal(SlotInfo.Index);
			}
			// Otherwise, if the slot is not empty, evict the element
			// Note that if an element is evicted, then its slot is already linked to the list
			else
			{
				ReAdd_Internal(ElemPtr, Slot.Hash, Slot.ProbeSeqLen);
			}

			Slot.Hash = SlotInfo.Hash;
			Slot.ProbeSeqLen = SlotInfo.PSL;

			AdapterType::Construct(ElemPtr, THashTableRehashElement::Tag, EvictedElemPtr);
			AdapterType::Destruct(EvictedElemPtr);
		}

		bool Remove_Internal(KeyType const& Key)
		{
			auto SlotInfo = FindSlot_Internal(Key);

			AEON_ASSERT(SlotInfo.PSL);

			if (SlotInfo.HasRequestedElement)
			{
				StorageType& Storage = Data.First();

				IndexType SlotIndex = SlotInfo.Index;

				// Remove requested element

				ElementType* ElemPtr = Storage.BufferPtr + SlotIndex;
				SlotType& Slot = Storage.SlotBufferPtr[SlotIndex];

				AdapterType::Destruct(ElemPtr);
				Slot.Hash = 0;
				Slot.ProbeSeqLen = 0;

				--Storage.ElemCount;

				// Move any next elements (if contiguous) with PSL > 1 one slot back

				for (IndexType Index = 0; Index < Storage.ElemCapacity; ++Index)
				{
					IndexType NextSlotIndex = AeonMath::ModPowerOfTwo(SlotIndex + 1, Storage.ElemCapacity);

					SlotType& CurrSlot = Storage.SlotBufferPtr[SlotIndex];
					SlotType& NextSlot = Storage.SlotBufferPtr[NextSlotIndex];

					// If there is no next element, or we find one with PSL == 1, we are done
					if (NextSlot.ProbeSeqLen <= 1)
					{
						break;
					}

					CurrSlot.Hash = Exchange(NextSlot.Hash, 0);
					CurrSlot.ProbeSeqLen = Exchange(NextSlot.ProbeSeqLen, 0);

					--CurrSlot.ProbeSeqLen;

					ElementType* CurrElemPtr = Storage.BufferPtr + SlotIndex;
					ElementType* NextElemPtr = AdapterType::AccessElement(Storage.BufferPtr + NextSlotIndex);

					AdapterType::Construct(CurrElemPtr, THashTableRehashElement::Tag, NextElemPtr);
					AdapterType::Destruct(NextElemPtr);

					SlotIndex = NextSlotIndex;
				}

				// Unlink the empty slot, which might or might not be the slot of the removed element

				UnlinkSlot_Internal(SlotIndex);

				return true;
			}

			return false;
		}

		void LinkSlot_Internal(IndexType SlotIndex)
		{
			StorageType& Storage = Data.First();

			SlotType& FirstSlot = Storage.SlotBufferPtr[0];
			SlotType& Slot = Storage.SlotBufferPtr[SlotIndex];

			if (SlotIndex != 0 /*&& Slot.NextSlotIdx == 0*/)
			{
				Slot.NextSlotIdx = FirstSlot.NextSlotIdx;
				FirstSlot.NextSlotIdx = SlotIndex;

				if (Slot.NextSlotIdx != NullSlotIndex)
				{
					SlotType& NextSlot = Storage.SlotBufferPtr[Slot.NextSlotIdx];
					NextSlot.PrevSlotIdx = SlotIndex;
				}
			}
		}
		void UnlinkSlot_Internal(IndexType SlotIndex)
		{
			StorageType& Storage = Data.First();

			SlotType& Slot = Storage.SlotBufferPtr[SlotIndex];

			if (SlotIndex != 0)
			{
				SlotType& PrevSlot = Storage.SlotBufferPtr[Slot.PrevSlotIdx];
				PrevSlot.NextSlotIdx = Slot.NextSlotIdx;

				if (Slot.NextSlotIdx != NullSlotIndex)
				{
					SlotType& NextSlot = Storage.SlotBufferPtr[Slot.NextSlotIdx];
					NextSlot.PrevSlotIdx = Slot.PrevSlotIdx;
				}
			}
		}

		void CheckIncreaseAllocation_Internal()
		{
			StorageType const& Storage = Data.First();
			ElementAllocatorType const& Allocator = Data.Second();

			FloatParamType LoadFactorLimit = GetLoadFactorLimit();

			IndexType OldElementCapacity = Storage.ElemCapacity;
			IndexType BoundedElementCapacity = IndexType(OldElementCapacity * LoadFactorLimit);

		//	AEON_ASSERT(BoundedElementCapacity < OldElementCapacity);

			if (Allocator.ShouldIncreaseAllocationSize(BoundedElementCapacity, Storage.ElemCount + 1))
			{
				IndexType NewElementCapacity = Allocator.ComputeAllocationSizeIncrease(OldElementCapacity);
				Reallocate_Internal(NewElementCapacity);
			}
		}
		void CheckIncreaseAllocation_Internal(IndexType NewElemCount)
		{
			StorageType const& Storage = Data.First();
			ElementAllocatorType const& Allocator = Data.Second();

			FloatParamType LoadFactorLimit = GetLoadFactorLimit();

			IndexType OldElementCapacity = Storage.ElemCapacity;
			IndexType BoundedElementCapacity = IndexType(OldElementCapacity * LoadFactorLimit);

		//	AEON_ASSERT(BoundedElementCapacity < OldElementCapacity);

			if (Allocator.ShouldIncreaseAllocationSize(BoundedElementCapacity, NewElemCount))
			{
				IndexType NewElementCapacity = IndexType(AeonMath::Ceil(NewElemCount / LoadFactorLimit));
				Reallocate_Internal(NewElementCapacity);
			}
		}

		void CheckDecreaseAllocation_Internal()
		{
			StorageType const& Storage = Data.First();
			ElementAllocatorType const& Allocator = Data.Second();

			FloatParamType LoadFactorLimit = GetLoadFactorLimit();

			IndexType OldElementCapacity = Storage.ElemCapacity;
			IndexType BoundedElementCapacity = IndexType(OldElementCapacity * LoadFactorLimit);

		//	AEON_ASSERT(BoundedElementCapacity < OldElementCapacity);

			if (Allocator.ShouldDecreaseAllocationSize(BoundedElementCapacity, Storage.ElemCount))
			{
				IndexType NewElementCapacity = Allocator.ComputeAllocationSizeDecrease(OldElementCapacity);
				Reallocate_Internal(NewElementCapacity);
			}
		}

	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GHashTableIterator

	template <class TableType>
	class GHashTableIterator final
		: public IForwardAccessIterator<GHashTableIterator<TableType>, TPropagateConst<TableType, typename TableType::ElementType>>
	{
		using Base = IForwardAccessIterator<GHashTableIterator<TableType>, TPropagateConst<TableType, typename TableType::ElementType>>;

		using TableElementType = typename TableType::ElementType;
		using TableIndexType = typename TableType::IndexType;

	public:
		AEON_DECLARE_ITERATOR_TRAITS(Base);

		GHashTableIterator() = default;
		explicit GHashTableIterator(TableType* NewTablePtr, TableIndexType NewElemIdx)
			: TablePtr{ NewTablePtr }
			, ElemIdx{ NewElemIdx }
		{
		}

		AEON_MAKE_COPYABLE(GHashTableIterator);

		virtual ReferenceType operator*() const override
		{
			// Don't allow dereferencing the 'end' iterator
			AEON_ASSERT(ElemIdx != TableType::NullSlotIndex);

			typename TableType::StorageType const& Storage = TablePtr->Data.First();

			return *AeonMemory::MemAccess<TableElementType>(Storage.BufferPtr + ElemIdx);
		}

		virtual GHashTableIterator& operator++() override
		{
			AEON_ASSERT(ElemIdx != TableType::NullSlotIndex);

			typename TableType::StorageType const& Storage = TablePtr->Data.First();

			typename TableType::SlotType const& Slot = Storage.SlotBufferPtr[ElemIdx];
			ElemIdx = Slot.NextSlotIdx;

			return *this;
		}

		virtual bool operator==(GHashTableIterator const& Other) const override
		{
			AEON_ASSERT(TablePtr == Other.TablePtr);
			return ElemIdx == Other.ElemIdx;
		}

	private:
		TableType* TablePtr;
		TableIndexType ElemIdx;
	};
}

#pragma warning(pop)

#endif