#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_STACK
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_STACK

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Container/Array.hpp"

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GStack

	template <CScalarObject ElemType, CAllocatorForObject<TDecayType<ElemType>> AllocType = GDynamicAllocator<ElemType>>
	class GStack
	{
	public:
		using ElementType = ElemType;
		using ConstElementType = TAddConst<ElemType>;

		using AllocatorType = AllocType;

		using IndexType = typename AllocType::IndexType;

		template <class NewAllocatorType = AllocatorType>
		explicit GStack(NewAllocatorType&& NewAllocator = NewAllocatorType{})
		requires CSameClass<TDecayType<NewAllocatorType>, AllocatorType>
			: Data(Forward<NewAllocatorType>(NewAllocator))
		{
		}
		template <class NewAllocatorType = AllocatorType>
		explicit GStack(TReserveStorage, IndexType InitialAllocationCount, NewAllocatorType&& NewAllocator = NewAllocatorType{})
		requires CSameClass<TDecayType<NewAllocatorType>, AllocatorType>
			: Data(TReserveStorage::Tag, InitialAllocationCount, Forward<NewAllocatorType>(NewAllocator))
		{
		}

		GStack(InitList<ElementType> InitialElemList)
		requires CCopyable<ElementType>
			: Data(InitialElemList)
		{
		}
		GStack(RvalRefInitList<ElementType> InitialElemList)
		requires CMoveable<ElementType>
			: Data(InitialElemList)
		{
		}

		GStack(GStack const& Other)
		requires CCopyable<ElementType>
			: Data(Other.Data)
		{
		}
		GStack(GStack&& Other)
		requires CMoveable<ElementType> || CSwappableResourceAllocator<AllocatorType>
			: Data(Move(Other.Data))
		{
		}

		GStack& operator=(GStack const& Other)
		requires CCopyable<ElementType>
		{
			Data = Other.Data;
			return *this;
		}
		GStack& operator=(GStack&& Other)
		requires CMoveable<ElementType> || CSwappableResourceAllocator<AllocatorType>
		{
			Data = Move(Other.Data);
			return *this;
		}

		GStack& Append(GStack const& Other)
		requires CCopyable<ElementType>
		{
			Data.Append(Other.Data);
			return *this;
		}
		GStack& Append(GStack&& Other)
		requires CMoveable<ElementType>
		{
			Data.Append(Move(Other.Data));
			return *this;
		}

		void Push(ConstElementType& Element)
		requires CCopyable<ElementType>
		{
			Data.Add(Element);
		}
		void Push(ElementType&& Element)
		requires CMoveable<ElementType>
		{
			Data.Add(Move(Element));
		}

		template <class ... ArgumentTypes>
		void PushEmplace(ArgumentTypes&& ... Arguments)
		requires CConstructible<ElementType, ArgumentTypes ...>
		{
			Data.Emplace(Forward<ArgumentTypes>(Arguments) ...);
		}

		bool Pop()
		{
			bool HasElement = !IsEmpty();
			if (HasElement)
			{
				Data.Remove();
			}
			return HasElement;
		}
		bool Pop(TReclaimStorage)
		{
			bool HasElement = !IsEmpty();
			if (HasElement)
			{
				Data.Remove(TReclaimStorage::Tag);
			}
			return HasElement;
		}
		bool Pop(ElementType& Element)
		requires CMoveableElseCopyable<ElementType>
		{
			bool HasElement = !IsEmpty();
			if (HasElement)
			{
				Element = MoveElseCopy(Peek());
				Data.Remove();
			}
			return HasElement;
		}
		bool Pop(ElementType& Element, TReclaimStorage)
		requires CMoveableElseCopyable<ElementType>
		{
			bool HasElement = !IsEmpty();
			if (HasElement)
			{
				Element = MoveElseCopy(Peek());
				Data.Remove(TReclaimStorage::Tag);
			}
			return HasElement;
		}

		void Reserve(IndexType NewAllocatedCount)
		{
			Data.Reserve(NewAllocatedCount);
		}

		void Empty()
		{
			Data.Empty();
		}
		void Empty(TReclaimStorage)
		{
			Data.Empty(TReclaimStorage::Tag);
		}

		ConstElementType& Peek() const
		{
			return Data.Back();
		}
		ElementType& Peek()
		{
			return AEON_INVOKE_CONST_OVERLOAD(Peek);
		}

		IndexType AllocatedCount() const
		{
			return Data.AllocatedCount();
		}
		IndexType Count() const
		{
			return Data.Count();
		}

		bool IsEmpty() const
		{
			return Data.IsEmpty();
		}

		AllocatorType const& GetAllocator() const
		{
			return Data.Second();
		}

		friend void Swap(GStack& Stack1, GStack& Stack2)
		requires CSwappableResourceAllocator<AllocatorType>
		{
			using Aeon::Swap;
			Swap(Stack1.Data, Stack2.Data);
		};

	private:
		using StorageType = GArray<ElemType, AllocType>;

		StorageType Data;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GStaticStack

	template <CScalarObject ElemType, szint ElemCountValue>
	using GStaticStack = GStack<ElemType, GStaticAllocator<ElemType, ElemCountValue>>;
}

#endif
