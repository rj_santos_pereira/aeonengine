#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_HASHMAP
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_HASHMAP

#include "Aeon/Framework/Core.hpp"

// TODO MapCommon
#include "Aeon/Framework/Allocator/DynamicAllocator.hpp"
#include "Aeon/Framework/Container/HashTable.hpp"

namespace Aeon::ZzInternal
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GKeyValue

	template <CScalarObject KeyType, CScalarObject ValueType>
	class GKeyValue final
	{
	public:
		template <szint IndexValue>
		using ObjectTypeAt = typename TTypePack<TAddConst<KeyType>, ValueType>::template ElementAt<IndexValue>;

		template <class NewKeyType>
		constexpr GKeyValue(NewKeyType&& NewKey)
		requires CConvertibleClass<NewKeyType, KeyType> && CDefaultConstructible<ValueType>
			: KeyObj{ Forward<NewKeyType>(NewKey) }
		{
		}

		// Add
		template <class NewKeyType, class NewValueType>
		constexpr GKeyValue(NewKeyType&& NewKey, NewValueType&& NewValue)
		requires CConvertibleClass<NewKeyType, KeyType> && CConvertibleClass<NewValueType, ValueType>
			: KeyObj{ Forward<NewKeyType>(NewKey) }
			, ValueObj{ Forward<NewValueType>(NewValue) }
		{
		}
		// Emplace
		template <class NewKeyType, class ... ValueArgTypes>
		constexpr GKeyValue(NewKeyType&& NewKey, TConstructInPlace, ValueArgTypes&& ... ValueArgs)
		requires CConvertibleClass<NewKeyType, KeyType> && CConstructible<ValueType, ValueArgTypes ...>
			: KeyObj{ Forward<NewKeyType>(NewKey) }
			, ValueObj{ Forward<ValueArgTypes>(ValueArgs)... }
		{
		}

		ZZINTERNAL_AEON_DECLARE_DUPLE_CONSTRUCTORS(GKeyValue, KeyType, ValueType);

		AEON_FORCEINLINE KeyType const& Key() const
		{
			return KeyObj;
		}

		AEON_FORCEINLINE ValueType const& Value() const
		{
			return ValueObj;
		}
		AEON_FORCEINLINE ValueType& Value()
		{
			return ValueObj;
		}

		template <szint IndexValue>
		AEON_FORCEINLINE constexpr ObjectTypeAt<IndexValue> const& ObjectAt() const
		{
			static_assert(IndexValue <= 1,
						  "'GKeyValue<KeyType, ValueType>::ObjectAt<IndexValue>()': "
						  "IndexValue must be 0 or 1.");
			if constexpr (IndexValue == 0)
			{
				return KeyObj;
			}
			else
			{
				return ValueObj;
			}
		}
		template <szint IndexValue>
		AEON_FORCEINLINE constexpr ObjectTypeAt<IndexValue>& ObjectAt()
		{
			return AEON_INVOKE_CONST_OVERLOAD(template ObjectAt<IndexValue>);
		}

		template <std::size_t IndexValue>
		friend ObjectTypeAt<IndexValue> const& get(GKeyValue const& KeyVal)
		{
			return KeyVal.template ObjectAt<IndexValue>();
		}
		template <std::size_t IndexValue>
		friend ObjectTypeAt<IndexValue>& get(GKeyValue& KeyVal)
		{
			return KeyVal.template ObjectAt<IndexValue>();
		}
		template <std::size_t IndexValue>
		friend ObjectTypeAt<IndexValue> get(GKeyValue&& KeyVal)
		{
			return KeyVal.template ObjectAt<IndexValue>();
		}

	private:
		KeyType KeyObj;
		ValueType ValueObj;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GHashMapElementAdapter

	template <CScalarObject ElemKeyType, CScalarObject ElemValueType>
	struct GHashMapElementAdapter
	{
		using ElementType = GKeyValue<ElemKeyType, ElemValueType>;
		using ConstElementType = TAddConst<GKeyValue<ElemKeyType, ElemValueType>>;

		using KeyType = ElemKeyType;
		using ConstKeyType = TAddConst<ElemKeyType>;

		// Subscript operator
		static ElementType* Construct(ElementType* ElemPtr, ElemKeyType const& Key)
		{
			return AeonMemory::MemConstruct<ElementType>(ElemPtr, Key);
		}
		static ElementType* Construct(ElementType* ElemPtr, ElemKeyType&& Key)
		{
			return AeonMemory::MemConstruct<ElementType>(ElemPtr, Move(Key));
		}

		// Add
		static ElementType* Construct(ElementType* ElemPtr, ElemKeyType const& Key, ElemValueType const& Value)
		requires CCopyable<ElemKeyType> && CCopyable<ElemValueType>
		{
			return AeonMemory::MemConstruct<ElementType>(ElemPtr, Key, Value);
		}
		static ElementType* Construct(ElementType* ElemPtr, ElemKeyType const& Key, ElemValueType&& Value)
		requires CCopyable<ElemKeyType> && CMoveable<ElemValueType>
		{
			return AeonMemory::MemConstruct<ElementType>(ElemPtr, Key, Move(Value));
		}
		static ElementType* Construct(ElementType* ElemPtr, ElemKeyType&& Key, ElemValueType const& Value)
		requires CMoveable<ElemKeyType> && CCopyable<ElemValueType>
		{
			return AeonMemory::MemConstruct<ElementType>(ElemPtr, Move(Key), Value);
		}
		static ElementType* Construct(ElementType* ElemPtr, ElemKeyType&& Key, ElemValueType&& Value)
		requires CMoveable<ElemKeyType> && CMoveable<ElemValueType>
		{
			return AeonMemory::MemConstruct<ElementType>(ElemPtr, Move(Key), Move(Value));
		}

		// Emplace
		template <class ... ValueArgTypes>
		static ElementType* Construct(ElementType* ElemPtr, ElemKeyType const& Key, TConstructInPlace, ValueArgTypes&& ... ValueArgs)
		requires CCopyable<ElemKeyType> && CConstructible<ElemValueType, ValueArgTypes...>
		{
			return AeonMemory::MemConstruct<ElementType>(ElemPtr, Key, TConstructInPlace::Tag, Forward<ValueArgTypes>(ValueArgs)...);
		}
		template <class ... ValueArgTypes>
		static ElementType* Construct(ElementType* ElemPtr, ElemKeyType&& Key, TConstructInPlace, ValueArgTypes&& ... ValueArgs)
		requires CMoveable<ElemKeyType> && CConstructible<ElemValueType, ValueArgTypes...>
		{
			return AeonMemory::MemConstruct<ElementType>(ElemPtr, Move(Key), TConstructInPlace::Tag, Forward<ValueArgTypes>(ValueArgs)...);
		}

		// Copy/move constructors and assignment operators
		static ElementType* Construct(ElementType* ElemPtr, ConstElementType* OtherElemPtr)
		{
			return AeonMemory::MemConstruct<ElementType>(ElemPtr,
														 Copy(OtherElemPtr->Key()),
														 Copy(OtherElemPtr->Value()));
		}
		static ElementType* Construct(ElementType* ElemPtr, ElementType* OtherElemPtr)
		{
			return AeonMemory::MemConstruct<ElementType>(ElemPtr,
														 Move(const_cast<KeyType&>(OtherElemPtr->Key())),
														 Move(OtherElemPtr->Value()));
		}

		// Rehash
		static ElementType* Construct(ElementType* ElemPtr, THashTableRehashElement, ElementType* OtherElemPtr)
		{
			return AeonMemory::MemConstruct<ElementType>(ElemPtr,
														 MoveElseCopy(const_cast<KeyType&>(OtherElemPtr->Key())),
														 MoveElseCopy(OtherElemPtr->Value()));
		}

		static void Destruct(ElementType* ElemPtr)
		{
			AeonMemory::MemDestruct<ElementType>(ElemPtr);
		}

		static ConstKeyType* AccessKey(ConstElementType* ElemPtr)
		{
			return Aeon::AddressOf(AeonMemory::MemAccess<ConstElementType>(ElemPtr)->Key());
		}

		static ConstElementType* AccessElement(ConstElementType* ElemPtr)
		{
			return AeonMemory::MemAccess<ConstElementType>(ElemPtr);
		}
		static ElementType* AccessElement(ElementType* ElemPtr)
		{
			return AeonMemory::MemAccess<ElementType>(ElemPtr);
		}
	};
}

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GKeyValue

	template <CScalarObject KeyType, CScalarObject ValueType>
	using GKeyValue = ZzInternal::GKeyValue<KeyType, ValueType>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GHashMap

	using DHashMapParams = ZzInternal::DHashTableParams;

	template <CScalarObject ElemKeyType, CScalarObject ElemValueType,
	    	  CObjectHasherForObject<TDecayType<ElemKeyType>> ElemHasherType = GDefaultObjectHasher<ElemKeyType>,
			  CAllocatorForObject<GKeyValue<ElemKeyType, ElemValueType>> ElemAllocType = GDynamicAllocator<GKeyValue<ElemKeyType, ElemValueType>>>
	class GHashMap : protected ZzInternal::GHashTable<ZzInternal::GHashMapElementAdapter<ElemKeyType, ElemValueType>, ElemHasherType, ElemAllocType>
	{
		using Base = ZzInternal::GHashTable<ZzInternal::GHashMapElementAdapter<ElemKeyType, ElemValueType>, ElemHasherType, ElemAllocType>;

	public:
		using KeyType = ElemKeyType;
		using ValueType = ElemValueType;

		using ElementType = GKeyValue<KeyType, ValueType>;
		using ConstElementType = TAddConst<GKeyValue<KeyType, ValueType>>;

		using HasherType = Base::HasherType;
		using AllocatorType = Base::AllocatorType;

		using HashType = Base::HashType;
		using IndexType = Base::IndexType;

		using IteratorType = Base::IteratorType;
		using ConstIteratorType = Base::ConstIteratorType;

		template <class NewAllocatorType = AllocatorType>
		explicit GHashMap(NewAllocatorType&& NewAllocator = NewAllocatorType{})
		requires CSameClass<TDecayType<NewAllocatorType>, AllocatorType>
			: Base(Forward<NewAllocatorType>(NewAllocator))
		{
		}
		template <class NewAllocatorType = AllocatorType>
		explicit GHashMap(DHashMapParams const& Params, NewAllocatorType&& NewAllocator = NewAllocatorType{})
		requires CSameClass<TDecayType<NewAllocatorType>, AllocatorType>
			: Base(Params, Forward<NewAllocatorType>(NewAllocator))
		{
		}

		GHashMap(InitList<ElementType> InitElemList)
		requires CCopyable<ElementType>
			: Base(InitElemList)
		{
		}
		GHashMap(RvalRefInitList<ElementType> InitElemList)
		requires CMoveable<ElementType>
			: Base(InitElemList)
		{
		}

		GHashMap(GHashMap const& Other)
		requires CCopyable<ElementType>
			: Base(Other)
		{
		}
		GHashMap(GHashMap&& Other)
		requires CMoveable<ElementType> || CSwappableResourceAllocator<AllocatorType>
			: Base(Move(Other))
		{
		}

		GHashMap& operator=(GHashMap const& Other)
		requires CCopyable<ElementType>
		{
			Base::operator=(Other);
			return *this;
		}
		GHashMap& operator=(GHashMap&& Other)
		requires CMoveable<ElementType> || CSwappableResourceAllocator<AllocatorType>
		{
			Base::operator=(Move(Other));
			return *this;
		}

		using Base::Reserve;
		using Base::Prune;

		using Base::Empty;
		using Base::Append;

		ElementType& AddDefault(KeyType const& Key)
		requires CCopyable<KeyType> && CDefaultConstructible<ValueType>
		{
			return Base::AddGeneric(Key);
		}
		ElementType& AddDefault(KeyType&& Key)
		requires CMoveable<KeyType> && CDefaultConstructible<ValueType>
		{
			return Base::AddGeneric(Move(Key));
		}

		ElementType& Add(KeyType const& Key, ValueType const& Value)
		requires CCopyable<KeyType> && CCopyable<ValueType>
		{
			return Base::AddGeneric(Key, Value);
		}
		ElementType& Add(KeyType const& Key, ValueType&& Value)
		requires CCopyable<KeyType> && CMoveable<ValueType>
		{
			return Base::AddGeneric(Key, Move(Value));
		}
		ElementType& Add(KeyType&& Key, ValueType const& Value)
		requires CMoveable<KeyType> && CCopyable<ValueType>
		{
			return Base::AddGeneric(Move(Key), Value);
		}
		ElementType& Add(KeyType&& Key, ValueType&& Value)
		requires CMoveable<KeyType> && CMoveable<ValueType>
		{
			return Base::AddGeneric(Move(Key), Move(Value));
		}

		template <class ... ValueArgTypes>
		ElementType& Emplace(KeyType const& Key, ValueArgTypes&&... ValueArgs)
		requires CCopyable<KeyType> && CConstructible<ValueType, ValueArgTypes ...>
		{
			return Base::AddGeneric(Key, TConstructInPlace::Tag, Forward<ValueArgTypes>(ValueArgs) ...);
		}
		template <class ... ValueArgTypes>
		ElementType& Emplace(KeyType&& Key, ValueArgTypes&&... ValueArgs)
		requires CMoveable<KeyType> && CConstructible<ValueType, ValueArgTypes ...>
		{
			return Base::AddGeneric(Move(Key), TConstructInPlace::Tag, Forward<ValueArgTypes>(ValueArgs) ...);
		}

		ElementType& FindElseAddDefault(KeyType const& Key)
		requires CCopyable<KeyType> && CDefaultConstructible<ValueType>
		{
			return Base::FindElseAddGeneric(Key);
		}
		ElementType& FindElseAddDefault(KeyType&& Key)
		requires CMoveable<KeyType> && CDefaultConstructible<ValueType>
		{
			return Base::FindElseAddGeneric(Move(Key));
		}

		ElementType& FindElseAdd(KeyType const& Key, ValueType const& Value)
		requires CCopyable<KeyType> && CCopyable<ValueType>
		{
			return Base::FindElseAddGeneric(Key, Value);
		}
		ElementType& FindElseAdd(KeyType const& Key, ValueType&& Value)
		requires CCopyable<KeyType> && CMoveable<ValueType>
		{
			return Base::FindElseAddGeneric(Key, Move(Value));
		}
		ElementType& FindElseAdd(KeyType&& Key, ValueType const& Value)
		requires CMoveable<KeyType> && CCopyable<ValueType>
		{
			return Base::FindElseAddGeneric(Move(Key), Value);
		}
		ElementType& FindElseAdd(KeyType&& Key, ValueType&& Value)
		requires CMoveable<KeyType> && CMoveable<ValueType>
		{
			return Base::FindElseAddGeneric(Move(Key), Move(Value));
		}

		template <class ... ValueArgTypes>
		ElementType& FindElseEmplace(KeyType const& Key, ValueArgTypes&&... ValueArgs)
		requires CCopyable<KeyType> && CConstructible<ValueType, ValueArgTypes ...>
		{
			return Base::FindElseAddGeneric(Key, TConstructInPlace::Tag, Forward<ValueArgTypes>(ValueArgs) ...);
		}
		template <class ... ValueArgTypes>
		ElementType& FindElseEmplace(KeyType&& Key, ValueArgTypes&&... ValueArgs)
		requires CMoveable<KeyType> && CConstructible<ValueType, ValueArgTypes ...>
		{
			return Base::FindElseAddGeneric(Move(Key), TConstructInPlace::Tag, Forward<ValueArgTypes>(ValueArgs) ...);
		}

		using Base::Remove;

		using Base::Contains;

		using Base::Find;

		using Base::Relink;

		using Base::GetLoadFactor;
		using Base::GetLoadFactorLimit;
		using Base::SetLoadFactorLimit;

		using Base::Buffer;

		using Base::AllocatedCount;
		using Base::Count;

		using Base::IsEmpty;

		using Base::GetAllocator;

		ValueType const& operator[](KeyType const& Key) const
		{
			AEON_ASSERT(Contains(Key));
			return Find(Key)->Value();
		}
		ValueType& operator[](KeyType const& Key)
		{
			return AEON_INVOKE_CONST_OVERLOAD(operator[], Key);
		}

		using Base::cbegin;
		using Base::begin;
		using Base::cend;
		using Base::end;

		friend void Swap(GHashMap& Map1, GHashMap& Map2)
		requires CSwappableResourceAllocator<AllocatorType>
		{
			using Aeon::Swap;

			Swap(static_cast<Base&>(Map1), static_cast<Base&>(Map2));
		}
	};
}

namespace std
{
// Specialization of std::tuple_element to enable structured bindings with Aeon::ZzInternal::GKeyValue
	template <std::size_t IndexValue, class KeyType, class ValueType>
	struct tuple_element<IndexValue, Aeon::ZzInternal::GKeyValue<KeyType, ValueType>>
	{
		using type = typename Aeon::TTypePack<Aeon::TAddConst<KeyType>, ValueType>::template ElementAt<IndexValue>;
	};

// Specialization of std::tuple_size to enable structured bindings with Aeon::ZzInternal::GKeyValue
	template <class KeyType, class ValueType>
	struct tuple_size<Aeon::ZzInternal::GKeyValue<KeyType, ValueType>>
		: public std::integral_constant<std::size_t, 2>
	{
	};
}

#endif