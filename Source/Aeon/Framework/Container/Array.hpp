#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_ARRAY
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONTAINER_ARRAY

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Container/ArrayCommon.hpp"

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GArray

	// HACK Workaround for MSVC bug described here https://developercommunity.visualstudio.com/t/C20-Error-C2027-when-instantiating-a/10393482
	//  Remove this workaround whenever possible (bug still present in 19.35)
#if AEON_COMPILER_MSVC && 0
	template <CScalarObject ElemType, class AllocType = GDynamicAllocator<ElemType>>
	requires CAllocatorForObject<AllocType, TDecayType<ElemType>> // TODO check if String compiles fine with this line
#else
	template <CScalarObject ElemType, CAllocatorForObject<TDecayType<ElemType>> AllocType = GDynamicAllocator<ElemType>>
#endif
	class GArray
	{
		using ElementPtrType = TAddPointer<typename AllocType::ObjectType>;

	public:
		using ElementType = ElemType;
		using ConstElementType = TAddConst<ElemType>;

		using AllocatorType = AllocType;

		using IndexType = typename AllocType::IndexType;

		using IteratorType = GArrayIterator<GArray>;
		using ConstIteratorType = GArrayIterator<TAddConst<GArray>>;

		template <class NewAllocatorType = AllocatorType>
		explicit GArray(NewAllocatorType&& NewAllocator = NewAllocatorType{})
		requires CSameClass<TDecayType<NewAllocatorType>, AllocatorType>
			: Data(StorageType{}, Forward<NewAllocatorType>(NewAllocator))
		{
			// CHECKME Consider making this work as TReserveStorage::Tag == 0, instead,
			//  since the first call to CheckIncreaseAllocation uses RecommendedStartingAllocationSize regardless if the capacity is 0

			AllocatorType& Allocator = Data.Second();

			IndexType InitialAllocatedCount = Allocator.RecommendedStartingAllocationSize();
			Allocate_Internal(InitialAllocatedCount);
		}
		template <class NewAllocatorType = AllocatorType>
		explicit GArray(TReserveStorage, IndexType InitialAllocatedCount, NewAllocatorType&& NewAllocator = NewAllocatorType{})
		requires CSameClass<TDecayType<NewAllocatorType>, AllocatorType>
			: Data(StorageType{}, Forward<NewAllocatorType>(NewAllocator))
		{
			Allocate_Internal(InitialAllocatedCount);
		}

		template <class NewAllocatorType = AllocatorType>
		explicit GArray(TCreateElements, IndexType InitialCount, NewAllocatorType&& NewAllocator = NewAllocatorType{})
		requires CDefaultConstructible<ElementType>
				 && CSameClass<TDecayType<NewAllocatorType>, AllocatorType>
			: Data(StorageType{}, Forward<NewAllocatorType>(NewAllocator))
		{
			Allocate_Internal(InitialCount);
			if (InitialCount > 0)
			{
				Construct_Internal(InitialCount);
			}
		}
		template <class NewAllocatorType = AllocatorType>
		explicit GArray(TCreateElements, IndexType InitialCount, ConstElementType& InitialElement,
								   NewAllocatorType&& NewAllocator = NewAllocatorType{})
		requires CCopyable<ElementType>
				 && CSameClass<TDecayType<NewAllocatorType>, AllocatorType>
			: Data(StorageType{}, Forward<NewAllocatorType>(NewAllocator))
		{
			Allocate_Internal(InitialCount);
			if (InitialCount > 0)
			{
				ConstructFromValue_Internal(InitialCount, InitialElement);
			}
		}

		GArray(InitList<ElementType> InitialElemList)
		requires CCopyable<ElementType>
		{
			auto InitialCount = IndexType(InitListOps::Count(InitialElemList));
			auto ElemPtr = InitListOps::Data(InitialElemList);

			Allocate_Internal(InitialCount);
			if (InitialCount > 0)
			{
				ConstructFromInitList_Internal(InitialCount, ElemPtr);
			}
		}
		GArray(RvalRefInitList<ElementType> InitialElemList)
		requires CMoveable<ElementType>
		{
			auto InitialCount = IndexType(InitListOps::Count(InitialElemList));
			auto ElemPtr = InitListOps::Data(InitialElemList);

			Allocate_Internal(InitialCount);
			if (InitialCount > 0)
			{
				ConstructFromInitList_Internal(InitialCount, ElemPtr);
			}
		}

		GArray(GArray const& Other)
		requires CCopyable<ElementType>
			: Data(StorageType{}, Other.Data.Second().OnContainerCopy())
		{
			StorageType const& OtherStorage = Other.Data.First();

			IndexType OtherElemCount = OtherStorage.ElemCount;

			Allocate_Internal(OtherElemCount);
			if (OtherElemCount > 0)
			{
				AppendCopyFromArray_Internal(Other);
			}
		}
		GArray(GArray&& Other)
		requires CMoveable<ElementType> || CSwappableResourceAllocator<AllocatorType>
			: Data(StorageType{}, Other.Data.Second().OnContainerMove())
		{
			[[maybe_unused]]
			StorageType& Storage = Data.First();
			StorageType& OtherStorage = Other.Data.First();

			IndexType OtherElemCount = OtherStorage.ElemCount;

			if constexpr (CSwappableResourceAllocator<AllocatorType>)
			{
				Storage = Exchange(OtherStorage, StorageType{});
			}
			else
			{
				Allocate_Internal(OtherElemCount);

				if (OtherElemCount > 0)
				{
					AppendMoveFromArray_Internal(Other);
				}

				// Simulate move-like behavior on Other
				Other.Deallocate_Internal();
			}
		}

		~GArray()
		{
			Destruct_Internal(0);
			Deallocate_Internal();
		}

		GArray& operator=(GArray const& Other)
		requires CCopyable<ElementType>
		{
			AEON_FAILSAFE(this != addressof(Other), return *this);

			StorageType const& OtherStorage = Other.Data.First();

			AllocatorType& Allocator = Data.Second();
			AllocatorType const& OtherAllocator = Other.Data.Second();

			IndexType OtherElemCount = OtherStorage.ElemCount;

			Destruct_Internal(0);

			// NOTE Technically we could unconditionally always execute this branch and the logic would be correct,
			//  but perhaps less efficient in some cases
			if (Allocator != OtherAllocator)
			{
				Deallocate_Internal();
				Allocator = OtherAllocator.OnContainerCopy();
				Allocate_Internal(OtherElemCount);
			}
			else
			{
				Reallocate_Internal(OtherElemCount);
			}

			if (OtherElemCount > 0)
			{
				AppendCopyFromArray_Internal(Other);
			}

			return *this;
		}
		GArray& operator=(GArray&& Other)
		requires CMoveable<ElementType> || CSwappableResourceAllocator<AllocatorType>
		{
			AEON_FAILSAFE(this != addressof(Other), return *this);

			[[maybe_unused]]
			StorageType& Storage = Data.First();
			StorageType& OtherStorage = Other.Data.First();

			AllocatorType& Allocator = Data.Second();
			AllocatorType& OtherAllocator = Other.Data.Second();

			IndexType OtherElemCount = OtherStorage.ElemCount;

			Destruct_Internal(0);

			if constexpr (CSwappableResourceAllocator<AllocatorType>)
			{
				Deallocate_Internal();

				if (Allocator != OtherAllocator)
				{
					Allocator = OtherAllocator.OnContainerMove();
				}

				Storage = Exchange(OtherStorage, StorageType{});
			}
			else
			{
				if (Allocator != OtherAllocator)
				{
					Deallocate_Internal();
					Allocator = OtherAllocator.OnContainerMove();
					Allocate_Internal(OtherElemCount);
				}
				else
				{
					Reallocate_Internal(OtherElemCount);
				}

				if (OtherElemCount > 0)
				{
					AppendMoveFromArray_Internal(Other);
				}

				// Simulate move-like behavior on Other
				Other.Deallocate_Internal();
			}

			return *this;
		}

		void Reserve(IndexType NewCapacity)
		{
			CheckIncreaseAllocation_Internal(NewCapacity);
		}

		void Prune()
		{
			StorageType& Storage = Data.First();

			CheckDecreaseAllocation_Internal(Storage.ElemCount);
		}

		void Resize(IndexType NewElemCount)
		requires CDefaultConstructible<ElementType>
		{
			StorageType& Storage = Data.First();

			if (Storage.ElemCount < NewElemCount)
			{
				CheckIncreaseAllocation_Internal(NewElemCount);

				Construct_Internal(NewElemCount);
			}
			else if (Storage.ElemCount > NewElemCount)
			{
				Destruct_Internal(NewElemCount);

				CheckDecreaseAllocation_Internal(NewElemCount);
			}
		}
		void Resize(IndexType NewElemCount, ConstElementType& NewElement)
		requires CCopyable<ElementType>
		{
			StorageType& Storage = Data.First();

			if (Storage.ElemCount < NewElemCount)
			{
				CheckIncreaseAllocation_Internal(NewElemCount);

				ConstructFromValue_Internal(NewElemCount, NewElement);
			}
			else if (Storage.ElemCount > NewElemCount)
			{
				Destruct_Internal(NewElemCount);

				CheckDecreaseAllocation_Internal(NewElemCount);
			}
		}

		void Empty()
		{
			Destruct_Internal(0);
		}
		void Empty(TReclaimStorage)
		{
			Destruct_Internal(0);

			// Forcefully deallocate all memory
			Deallocate_Internal();
		}

		GArray& Append(GArray const& Other)
		requires CCopyable<ElementType>
		{
			AEON_FAILSAFE(this != addressof(Other), return *this);

			StorageType& Storage = Data.First();
			StorageType const& OtherStorage = Other.Data.First();

			if (OtherStorage.ElemCount > 0)
			{
				IndexType NewElemCount = Storage.ElemCount + OtherStorage.ElemCount;

				CheckIncreaseAllocation_Internal(NewElemCount);
				AppendCopyFromArray_Internal(Other);
			}

			return *this;
		}
		GArray& Append(GArray&& Other)
		requires CMoveable<ElementType>
		{
			AEON_FAILSAFE(this != addressof(Other), return *this);

			StorageType& Storage = Data.First();
			StorageType& OtherStorage = Other.Data.First();

			if (OtherStorage.ElemCount > 0)
			{
				IndexType NewElemCount = Storage.ElemCount + OtherStorage.ElemCount;

				CheckIncreaseAllocation_Internal(NewElemCount);
				AppendMoveFromArray_Internal(Other);

				// Simulate move-like behavior on Other
				Other.Deallocate_Internal();
			}

			return *this;
		}

		ElementType& Add(ConstElementType& NewElement)
		requires CCopyable<ElementType>
		{
			StorageType& Storage = Data.First();

			CheckIncreaseAllocation_Internal();

			ElementType* ElemPtr = Storage.BufferPtr + Storage.ElemCount;

			++Storage.ElemCount;

			return *AeonMemory::MemConstruct<ElementType>(ElemPtr, NewElement);
		}
		ElementType& Add(ElementType&& NewElement)
		requires CMoveable<ElementType>
		{
			StorageType& Storage = Data.First();

			CheckIncreaseAllocation_Internal();

			ElementType* ElemPtr = Storage.BufferPtr + Storage.ElemCount;

			++Storage.ElemCount;

			return *AeonMemory::MemConstruct<ElementType>(ElemPtr, Move(NewElement));
		}

		template <class ... ArgumentTypes>
		ElementType& Emplace(ArgumentTypes&& ... Arguments)
		requires CConstructible<ElementType, ArgumentTypes ...>
		{
			StorageType& Storage = Data.First();

			CheckIncreaseAllocation_Internal();

			ElementType* ElemPtr = Storage.BufferPtr + Storage.ElemCount;

			++Storage.ElemCount;

			return *AeonMemory::MemConstruct<ElementType>(ElemPtr, Forward<ArgumentTypes>(Arguments) ...);
		}

		void Remove()
		{
			StorageType& Storage = Data.First();
			AEON_ASSERT(Storage.ElemCount != 0);

			ElementType* ElemPtr = Storage.BufferPtr + Storage.ElemCount;
			AeonMemory::MemDestruct<ElementType>(ElemPtr);

			--Storage.ElemCount;
		}
		void Remove(TReclaimStorage)
		{
			StorageType& Storage = Data.First();
			AEON_ASSERT(Storage.ElemCount != 0);

			ElementType* ElemPtr = Storage.BufferPtr + Storage.ElemCount;
			AeonMemory::MemDestruct<ElementType>(ElemPtr);

			// Reclaim storage if possible
			CheckDecreaseAllocation_Internal();

			--Storage.ElemCount;
		}

		ElementType& AddAt(IndexType Index, ConstElementType& NewElement)
		requires CCopyable<ElementType>
		{
			StorageType& Storage = Data.First();
			// NOTE Allow Index == Count(), equivalent to invoking Add(NewElement)
			AEON_ASSERT(Index <= Storage.ElemCount);

			CheckIncreaseAllocation_Internal();
			RelocateAndOpenSlotAtIndex_Internal(Index);

			++Storage.ElemCount;

			ElementType* ElemPtr = Storage.BufferPtr + Index;
			return *AeonMemory::MemConstruct<ElementType>(ElemPtr, NewElement);
		}
		ElementType& AddAt(IndexType Index, ElementType&& NewElement)
		requires CMoveable<ElementType>
		{
			StorageType& Storage = Data.First();
			AEON_ASSERT(Index <= Storage.ElemCount);

			CheckIncreaseAllocation_Internal();
			RelocateAndOpenSlotAtIndex_Internal(Index);

			++Storage.ElemCount;

			ElementType* ElemPtr = Storage.BufferPtr + Index;
			return *AeonMemory::MemConstruct<ElementType>(ElemPtr, Move(NewElement));
		}

		template <class ... ArgumentTypes>
		ElementType& EmplaceAt(IndexType Index, ArgumentTypes&& ... Arguments)
		requires CConstructible<ElementType, ArgumentTypes ...>
		{
			StorageType& Storage = Data.First();
			AEON_ASSERT(Index <= Storage.ElemCount);

			CheckIncreaseAllocation_Internal();
			RelocateAndOpenSlotAtIndex_Internal(Index);

			++Storage.ElemCount;

			ElementType* ElemPtr = Storage.BufferPtr + Index;
			return *AeonMemory::MemConstruct<ElementType>(ElemPtr, Forward<ArgumentTypes>(Arguments) ...);
		}

		void RemoveAt(IndexType Index)
		{
			StorageType& Storage = Data.First();
			AEON_ASSERT(Index < Storage.ElemCount);

			ElementType* ElemPtr = Storage.BufferPtr + Index;
			AeonMemory::MemDestruct<ElementType>(ElemPtr);

			RelocateAndCloseSlotAtIndex_Internal(Index);

			--Storage.ElemCount;
		}
		void RemoveAt(IndexType Index, TReclaimStorage)
		{
			StorageType& Storage = Data.First();
			AEON_ASSERT(Index < Storage.ElemCount);

			ElementType* ElemPtr = Storage.BufferPtr + Index;
			AeonMemory::MemDestruct<ElementType>(ElemPtr);

			RelocateAndCloseSlotAtIndex_Internal(Index);
			CheckDecreaseAllocation_Internal();

			--Storage.ElemCount;
		}

		template <class PredicateType = GLessThan<ElementType>>
		ElementType& AddSorted(ConstElementType& NewElement, PredicateType&& Predicate = PredicateType{ })
		requires CCopyable<ElementType> && CCallableWithResult<PredicateType, bool, ConstElementType&, ConstElementType&>
		{
			auto Iter = std::lower_bound(cbegin(), cend(), NewElement, Predicate);
			auto Index = IndexType(Iter - cbegin());
			return AddAt(Index, NewElement);
		}
		template <class PredicateType = GLessThan<ElementType>>
		ElementType& AddSorted(ElementType&& NewElement, PredicateType&& Predicate = PredicateType{ })
		requires CMoveable<ElementType> && CCallableWithResult<PredicateType, bool, ConstElementType&, ConstElementType&>
		{
			auto Iter = std::lower_bound(cbegin(), cend(), NewElement, Predicate);
			auto Index = IndexType(Iter - cbegin());
			return AddAt(Index, Move(NewElement));
		}

		GOptionalRef<ElementType> AddUnique(ConstElementType& NewElement)
		requires CCopyable<ElementType>
		{
			if (!Contains(NewElement))
			{
				return GOptionalRef<ElementType>{ Add(NewElement) };
			}
			return GOptionalRef<ElementType>{};
		}
		GOptionalRef<ElementType> AddUnique(ElementType&& NewElement)
		requires CMoveable<ElementType>
		{
			if (!Contains(NewElement))
			{
				return GOptionalRef<ElementType>{ Add(Move(NewElement)) };
			}
			return GOptionalRef<ElementType>{};
		}
		template <class PredicateType>
		GOptionalRef<ElementType> AddUnique(ConstElementType& NewElement, PredicateType&& Predicate)
		requires CCopyable<ElementType> && CCallableWithResult<PredicateType, bool, ConstElementType&>
		{
			if (!Contains(Forward<PredicateType>(Predicate)))
			{
				return GOptionalRef<ElementType>{ Add(NewElement) };
			}
			return GOptionalRef<ElementType>{};
		}
		template <class PredicateType>
		GOptionalRef<ElementType> AddUnique(ElementType&& NewElement, PredicateType&& Predicate)
		requires CMoveable<ElementType> && CCallableWithResult<PredicateType, bool, ConstElementType&>
		{
			if (!Contains(Forward<PredicateType>(Predicate)))
			{
				return GOptionalRef<ElementType>{ Add(Move(NewElement)) };
			}
			return GOptionalRef<ElementType>{};
		}

		template <class PredicateType = GLessThan<ElementType>>
		GOptionalRef<ElementType> AddUniqueSorted(ConstElementType& NewElement, PredicateType&& Predicate = PredicateType{})
		requires CCopyable<ElementType> && CCallableWithResult<PredicateType, bool, ConstElementType&, ConstElementType&>
		{
			if (!ContainsSorted(Forward<PredicateType>(Predicate)))
			{
				return GOptionalRef<ElementType>{ AddSorted(NewElement) };
			}
			return GOptionalRef<ElementType>{};
		}
		template <class PredicateType = GLessThan<ElementType>>
		GOptionalRef<ElementType> AddUniqueSorted(ElementType&& NewElement, PredicateType&& Predicate = PredicateType{})
		requires CMoveable<ElementType> && CCallableWithResult<PredicateType, bool, ConstElementType&, ConstElementType&>
		{
			if (!ContainsSorted(Forward<PredicateType>(Predicate)))
			{
				return GOptionalRef<ElementType>{ AddSorted(Move(NewElement)) };
			}
			return GOptionalRef<ElementType>{};
		}

		bool Contains(ConstElementType& Value) const
		{
			return bool(Find(Value));
		}
		template <class PredicateType>
		bool Contains(PredicateType&& Predicate) const
		requires CCallableWithResult<PredicateType, bool, ConstElementType&>
		{
			return bool(Find(Forward<PredicateType>(Predicate)));
		}

		template <class PredicateType = GLessThan<ElementType>>
		bool ContainsSorted(ConstElementType& Value, PredicateType&& Predicate = PredicateType{}) const
		requires CCallableWithResult<PredicateType, bool, ConstElementType&, ConstElementType&>
		{
			return bool(FindSorted(Value, Forward<PredicateType>(Predicate)));
		}

		GOptional<IndexType> FindIndex(ConstElementType& Value) const
		{
			auto Iter = std::find(cbegin(), cend(), Value);
			if (Iter != cend())
			{
				return GOptional<IndexType>{ TConstructInPlace::Tag, IndexType(Iter - cbegin()) };
			}
			return GOptional<IndexType>{};
		}
		template <class PredicateType>
		GOptional<IndexType> FindIndex(PredicateType&& Predicate) const
		requires CCallableWithResult<PredicateType, bool, ConstElementType&>
		{
			auto Iter = std::find_if(cbegin(), cend(), Predicate);
			if (Iter != cend())
			{
				return GOptional<IndexType>{ TConstructInPlace::Tag, IndexType(Iter - cbegin()) };
			}
			return GOptional<IndexType>{};
		}

		template <class PredicateType = GLessThan<ElementType>>
		GOptional<IndexType> FindIndexSorted(ConstElementType& Value, PredicateType&& Predicate = PredicateType{}) const
		requires CCallableWithResult<PredicateType, bool, ConstElementType&, ConstElementType&>
		{
			auto Iter = std::lower_bound(cbegin(), cend(), Value, Predicate);
			if (Iter != cend())
			{
				ConstElementType& Element = *Iter;
				// Iter points to the first element for which Predicate(Element, Value) is false; if Predicate(Value, Element) is also false,
				// then Iter points to the element we want (with a few exceptions, e.g. floating-point NaN values)
				if (!Predicate(Value, Element))
				{
					return GOptional<IndexType>{ TConstructInPlace::Tag, IndexType(Iter - cbegin()) };
				}
			}
			return GOptional<IndexType>{};
		}

		GOptionalRef<ConstElementType> Find(ConstElementType& Value) const
		{
			auto Iter = std::find(cbegin(), cend(), Value);
			if (Iter != cend())
			{
				return { *Iter };
			}
			return {};
		}
		GOptionalRef<ElementType> Find(ConstElementType& Value)
		{
			return ZZINTERNAL_AEON_INVOKE_CONST_OPTIONALREF_OVERLOAD(Find, Value);
		}

		template <class PredicateType>
		GOptionalRef<ConstElementType> Find(PredicateType&& Predicate) const
		requires CCallableWithResult<PredicateType, bool, ConstElementType&>
		{
			auto Iter = std::find_if(cbegin(), cend(), Predicate);
			if (Iter != cend())
			{
				return { *Iter };
			}
			return {};
		}
		template <class PredicateType>
		GOptionalRef<ElementType> Find(PredicateType&& Predicate)
		requires CCallableWithResult<PredicateType, bool, ConstElementType&>
		{
			return ZZINTERNAL_AEON_INVOKE_CONST_OPTIONALREF_OVERLOAD(Find, Forward<PredicateType>(Predicate));
		}

		template <class PredicateType = GLessThan<ElementType>>
		GOptionalRef<ConstElementType> FindSorted(ConstElementType& Value, PredicateType&& Predicate = PredicateType{ }) const
		requires CCallableWithResult<PredicateType, bool, ConstElementType&, ConstElementType&>
		{
			auto Iter = std::lower_bound(cbegin(), cend(), Value, Predicate);
			if (Iter != cend())
			{
				ConstElementType& Element = *Iter;
				// Iter points to the first element for which Predicate(Element, Value) is false; if Predicate(Value, Element) is also false,
				// then Iter points to the element we want (with a few exceptions, e.g. floating-point NaN values)
				if (!Predicate(Value, Element))
				{
					return { Element };
				}
			}
			return {};
		}
		template <class PredicateType = GLessThan<ElementType>>
		GOptionalRef<ElementType> FindSorted(ConstElementType& Value, PredicateType&& Predicate = PredicateType{ })
		requires CCallableWithResult<PredicateType, bool, ConstElementType&, ConstElementType&>
		{
			return ZZINTERNAL_AEON_INVOKE_CONST_OPTIONALREF_OVERLOAD(FindSorted, Value, Forward<PredicateType>(Predicate));
		}

		template <class PredicateType = GLessThan<ElementType>>
		bool IsSorted(PredicateType&& Predicate = PredicateType{ }) const
		requires CCallableWithResult<PredicateType, bool, ConstElementType&, ConstElementType&>
		{
			return std::is_sorted(cbegin(), cend(), Predicate);
		}

		template <class PredicateType = GLessThan<ElementType>>
		GArray& Sort(PredicateType&& Predicate = PredicateType{ })
		requires CCallableWithResult<PredicateType, bool, ConstElementType&, ConstElementType&>
		{
			std::sort(begin(), end(), Predicate);
			return *this;
		}

		template <class PredicateType = GLessThan<ElementType>>
		GArray& StableSort(PredicateType&& Predicate = PredicateType{ })
		requires CCallableWithResult<PredicateType, bool, ConstElementType&, ConstElementType&>
		{
			std::stable_sort(begin(), end(), Predicate);
			return *this;
		}

		GArray& Reverse()
		{
			std::reverse(begin(), end());
			return *this;
		}

		template <CInferableScalarObject MappedElementType = TInferTypeTag,
				  CInferableAllocator MappedAllocatorType = TInferTypeTag,
				  class PredicateType>
		auto Map(PredicateType&& Predicate) const
			-> TMapOperationResult<GArray, ElementType, MappedElementType, AllocatorType, MappedAllocatorType, PredicateType>
		requires CCallable<PredicateType, ConstElementType&>
		{
			using MappedArrayType = TMapOperationResult<GArray,
														ElementType, MappedElementType,
														AllocatorType, MappedAllocatorType,
														PredicateType>;

			StorageType const& Storage = Data.First();

			MappedArrayType MappedArray{ TReserveStorage::Tag, Storage.ElemCount };
			for (IndexType Index = 0; Index < Storage.ElemCount; ++Index)
			{
				ConstElementType* ElemPtr = AeonMemory::MemAccess<ConstElementType>(Storage.BufferPtr + Index);
				MappedArray.Emplace(Predicate(*ElemPtr));
			}
			return MappedArray;
		}

		template <CInferableScalarObject FilteredElementType = TInferTypeTag,
				  CInferableAllocator FilteredAllocatorType = TInferTypeTag,
				  class PredicateType>
		auto Filter(PredicateType&& Predicate) const
			-> TFilterOperationResult<GArray, ElementType, FilteredElementType, AllocatorType, FilteredAllocatorType>
		requires CCallable<PredicateType, ConstElementType&>
		{
			using FilteredArrayType = TFilterOperationResult<GArray,
															 ElementType, FilteredElementType,
															 AllocatorType, FilteredAllocatorType>;

			StorageType const& Storage = Data.First();

			FilteredArrayType FilteredArray{ TReserveStorage::Tag, Storage.ElemCount };
			for (IndexType Index = 0; Index < Storage.ElemCount; ++Index)
			{
				ConstElementType* ElemPtr = AeonMemory::MemAccess<ConstElementType>(Storage.BufferPtr + Index);
				if (Predicate(*ElemPtr))
				{
					FilteredArray.Emplace(*ElemPtr);
				}
			}
			FilteredArray.Prune();
			return FilteredArray;
		}

		template <CScalarObject FoldedElementType = ElementType,
				  class InitialElementType = ElementType,
				  class PredicateType>
		FoldedElementType Fold(PredicateType&& Predicate, InitialElementType&& InitialValue = InitialElementType{}) const
		requires CCallableWithResult<PredicateType, ElementType, ConstElementType&, ConstElementType&>
				 && CConstructible<ElementType, InitialElementType>
				 && CConvertibleClass<ElementType, FoldedElementType>
		{
			StorageType const& Storage = Data.First();

			// Note: we'll implicitly convert the type to FoldedElementType in the return statement
			ElementType FoldedElement{ Forward<InitialElementType>(InitialValue) };
			for (IndexType Index = 0; Index < Storage.ElemCount; ++Index)
			{
				ConstElementType* ElemPtr = AeonMemory::MemAccess<ConstElementType>(Storage.BufferPtr + Index);
				FoldedElement = Predicate(FoldedElement, *ElemPtr);
			}
			return FoldedElement;
		}

		ConstElementType& Front() const
		{
			StorageType const& Storage = Data.First();
			AEON_ASSERT(Storage.ElemCount != 0);
			return *AeonMemory::MemAccess<ConstElementType>(Storage.BufferPtr);
		}
		ElementType& Front()
		{
			return AEON_INVOKE_CONST_OVERLOAD(Front);
		}

		ConstElementType& Back() const
		{
			StorageType const& Storage = Data.First();
			AEON_ASSERT(Storage.ElemCount != 0);
			return *AeonMemory::MemAccess<ConstElementType>(Storage.BufferPtr + Storage.ElemCount - 1);
		}
		ElementType& Back()
		{
			return AEON_INVOKE_CONST_OVERLOAD(Back);
		}

		ConstElementType* Buffer() const
		{
			return Data.First().BufferPtr;
		}
		ElementType* Buffer()
		{
			return AEON_INVOKE_CONST_OVERLOAD(Buffer);
		}

		IndexType AllocatedCount() const
		{
			return Data.First().ElemCapacity;
		}

		IndexType Count() const
		{
			return Data.First().ElemCount;
		}

		bool IsEmpty() const
		{
			return Count() == 0;
		}

		AllocatorType const& GetAllocator() const
		{
			return Data.Second();
		}

		ConstElementType& operator[](IndexType Index) const
		{
			StorageType const& Storage = Data.First();
			AEON_ASSERT(Index < Storage.ElemCount);
			return *AeonMemory::MemAccess<ConstElementType>(Storage.BufferPtr + Index);
		}
		ElementType& operator[](IndexType Index)
		{
			return AEON_INVOKE_CONST_OVERLOAD(operator[], Index);
		}

		voidptr DataBuffer_Internal(szint BufferIndex) const
		{
			AEON_ASSERT(BufferIndex == 0);
			return Data.First().BufferPtr;
		}
		szint DataBufferCount_Internal() const
		{
			return 1;
		}
		szint DataElementSize_Internal(szint BufferIndex) const
		{
			AEON_ASSERT(BufferIndex == 0);
			return Aeon::SizeOf(ElementType);
		}
		szint DataElementCount_Internal() const
		{
			return Count();
		}
		szint DataElementCapacity_Internal() const
		{
			return AllocatedCount();
		}

		AEON_FORCEINLINE ConstIteratorType begin() const
		{
			return ConstIteratorType{ this, 0 };
		}
		AEON_FORCEINLINE IteratorType begin()
		{
			return IteratorType{ this, 0 };
		}
		AEON_FORCEINLINE ConstIteratorType cbegin() const
		{
			return begin();
		}

		AEON_FORCEINLINE ConstIteratorType end() const
		{
			return ConstIteratorType{ this, Data.First().ElemCount };
		}
		AEON_FORCEINLINE IteratorType end()
		{
			return IteratorType{ this, Data.First().ElemCount };
		}
		AEON_FORCEINLINE ConstIteratorType cend() const
		{
			return end();
		}

		friend void Swap(GArray& Array1, GArray& Array2)
		requires CSwappableResourceAllocator<AllocatorType>
		{
			StorageType& Storage1 = Array1.Data.First();
			StorageType& Storage2 = Array2.Data.First();

			AllocatorType& Allocator1 = Array1.Data.Second();
			AllocatorType& Allocator2 = Array2.Data.Second();

			using Aeon::Swap;

			Swap(Storage1, Storage2);
			Swap(Allocator1, Allocator2);
		}

	private:
		void Allocate_Internal(IndexType InitialElemCapacity)
		{
			StorageType& Storage = Data.First();
			AllocatorType& Allocator = Data.Second();

			AEON_ASSERT(Allocator.IsAllocationSizeSupported(InitialElemCapacity));

			ElementPtrType InitialBufferPtr = Allocator.Allocate(InitialElemCapacity);

			// FIXME make this a FAILSAFE? (also do this for other containers)
			AEON_ASSERT(InitialBufferPtr || (InitialElemCapacity == 0));

			Storage.BufferPtr = InitialBufferPtr;
			Storage.ElemCapacity = InitialElemCapacity;
		}
		void Reallocate_Internal(IndexType NewElemCapacity)
		{
			StorageType& Storage = Data.First();
			AllocatorType& Allocator = Data.Second();

			AEON_ASSERT(Allocator.IsAllocationSizeSupported(NewElemCapacity));

			ElementPtrType NewBufferPtr = Allocator.Reallocate(Storage.BufferPtr, Storage.ElemCapacity, NewElemCapacity,
															   &GArray::RelocateElements_Internal, this);

			AEON_ASSERT(NewBufferPtr || (NewElemCapacity == 0));

			Storage.BufferPtr = NewBufferPtr;
			Storage.ElemCapacity = NewElemCapacity;
		}
		void Deallocate_Internal()
		{
			StorageType& Storage = Data.First();
			AllocatorType& Allocator = Data.Second();

			if (Storage.BufferPtr)
			{
				Allocator.Deallocate(Storage.BufferPtr);

				Storage.BufferPtr = nullptr;
				Storage.ElemCapacity = 0;
			}
		}

		void Construct_Internal(IndexType NewElemCount)
		{
			StorageType& Storage = Data.First();

			IndexType OldElemCount = Exchange(Storage.ElemCount, NewElemCount);
			for (IndexType Index = OldElemCount; Index < NewElemCount; ++Index)
			{
				AeonMemory::MemConstruct<ElementType>(Storage.BufferPtr + Index);
			}
		}
		void ConstructFromValue_Internal(IndexType NewElemCount, ConstElementType& Element)
		{
			StorageType& Storage = Data.First();

			IndexType OldElemCount = Exchange(Storage.ElemCount, NewElemCount);
			for (IndexType Index = OldElemCount; Index < NewElemCount; ++Index)
			{
				AeonMemory::MemConstruct<ElementType>(Storage.BufferPtr + Index, Element);
			}
		}
		template <class NewElementType>
		void ConstructFromInitList_Internal(IndexType NewElemCount, NewElementType const* ElemPtr)
		requires CSameClass<NewElementType, TDecayType<ElementType>>
				 || CSameClass<NewElementType, GRvalRefWrapper<TDecayType<ElementType>>>
		{
			StorageType& Storage = Data.First();

			Storage.ElemCount = NewElemCount;
			if constexpr (VIsTriviallyCopyable<NewElementType>)
			{
				IndexType BufferElemSize = NewElemCount * ElementSize;

				AeonMemory::MemCopy(Storage.BufferPtr, ElemPtr, BufferElemSize);
			}
			else
			{
				for (IndexType Index = 0; Index < NewElemCount; ++Index)
				{
					AeonMemory::MemConstruct<ElementType>(Storage.BufferPtr + Index, MoveElseCopy(*ElemPtr++));
				}
			}
		}

		void AppendCopyFromArray_Internal(GArray const& Other)
		{
			StorageType& Storage = Data.First();
			StorageType const& OtherStorage = Other.Data.First();

			ElementType* BufferPtr = Storage.BufferPtr + Storage.ElemCount;
			ConstElementType* OtherBufferPtr = OtherStorage.BufferPtr;
			IndexType OtherElemCount = OtherStorage.ElemCount;

			if constexpr (VIsTriviallyCopyable<ElementType>)
			{
				IndexType BufferElemSize = OtherElemCount * ElementSize;

				AeonMemory::MemCopy(BufferPtr, OtherBufferPtr, BufferElemSize);
			}
			else
			{
				for (IndexType Index = 0; Index < OtherElemCount; ++Index)
				{
					ElementType* ElemPtr = BufferPtr + Index;
					ConstElementType* OtherElemPtr = AeonMemory::MemAccess<ConstElementType>(OtherBufferPtr + Index);

					AeonMemory::MemConstruct<ElementType>(ElemPtr, *OtherElemPtr);
				}
			}

			Storage.ElemCount += OtherElemCount;
		}
		void AppendMoveFromArray_Internal(GArray& Other)
		{
			StorageType& Storage = Data.First();
			StorageType& OtherStorage = Other.Data.First();

			ElementType* BufferPtr = Storage.BufferPtr + Storage.ElemCount;
			ElementType* OtherBufferPtr = OtherStorage.BufferPtr;
			IndexType OtherElemCount = OtherStorage.ElemCount;

			if constexpr (VIsTriviallyCopyable<ElementType>)
			{
				IndexType BufferElemSize = OtherElemCount * ElementSize;

				AeonMemory::MemCopy(BufferPtr, OtherBufferPtr, BufferElemSize);
			}
			else
			{
				for (IndexType Index = 0; Index < OtherElemCount; ++Index)
				{
					ElementType* ElemPtr = BufferPtr + Index;
					ElementType* OtherElemPtr = AeonMemory::MemAccess<ElementType>(OtherBufferPtr + Index);

					AeonMemory::MemConstruct<ElementType>(ElemPtr, Move(*OtherElemPtr));
					AeonMemory::MemDestruct<ElementType>(OtherElemPtr);
				}
			}

			Storage.ElemCount += OtherElemCount;
			OtherStorage.ElemCount = 0;
		}

		void Destruct_Internal(IndexType NewElemCount)
		{
			StorageType& Storage = Data.First();

			IndexType OldElemCount = Exchange(Storage.ElemCount, NewElemCount);

			if constexpr (!VIsTriviallyDestructible<ElementType>)
			{
				for (IndexType Index = NewElemCount; Index < OldElemCount; ++Index)
				{
					AeonMemory::MemDestruct<ElementType>(Storage.BufferPtr + Index);
				}
			}
		}

		void CheckIncreaseAllocation_Internal()
		{
			StorageType const& Storage = Data.First();
			AllocatorType const& Allocator = Data.Second();

			IndexType OldElemCapacity = Storage.ElemCapacity;
			if (Allocator.ShouldIncreaseAllocationSize(OldElemCapacity, Storage.ElemCount + 1))
			{
				IndexType NewElemCapacity = Allocator.ComputeAllocationSizeIncrease(OldElemCapacity);
				Reallocate_Internal(NewElemCapacity);
			}
		}
		void CheckIncreaseAllocation_Internal(IndexType NewElemCount)
		{
			StorageType const& Storage = Data.First();
			AllocatorType const& Allocator = Data.Second();

			if (Allocator.ShouldIncreaseAllocationSize(Storage.ElemCapacity, NewElemCount))
			{
				Reallocate_Internal(NewElemCount);
			}
		}

		void CheckDecreaseAllocation_Internal()
		{
			StorageType const& Storage = Data.First();
			AllocatorType const& Allocator = Data.Second();

			IndexType OldElemCapacity = Storage.ElemCapacity;
			if (Allocator.ShouldDecreaseAllocationSize(OldElemCapacity, Storage.ElemCount - 1))
			{
				IndexType NewElemCapacity = Allocator.ComputeAllocationSizeDecrease(OldElemCapacity);
				Reallocate_Internal(NewElemCapacity);
			}
		}
		void CheckDecreaseAllocation_Internal(IndexType NewElemCount)
		{
			StorageType const& Storage = Data.First();
			AllocatorType const& Allocator = Data.Second();

			if (Allocator.ShouldDecreaseAllocationSize(Storage.ElemCapacity, NewElemCount))
			{
				// Don't reduce more than the minimal allocation
				NewElemCount = AeonMath::Max(NewElemCount, Allocator.RecommendedStartingAllocationSize());
				Reallocate_Internal(NewElemCount);
			}
		}

		void RelocateAndOpenSlotAtIndex_Internal(IndexType ElementIndex)
		{
			// Moves elements, assuming ElementIndex refers to an existing element,
			// so as to empty it ("opening the slot"), moving the element to a different slot,
			// and making the array discontiguous.

			StorageType& Storage = Data.First();

			// ElementIndex may be equal to NewLastIndex, in which case either branch is a no-op.
			// Assumes this refers to the last element index with the empty element.
			IndexType NewLastIndex = Storage.ElemCount;

			if constexpr (VIsTriviallyCopyable<ElementType>)
			{
				ElementType* OldBlockPtr = Storage.BufferPtr + ElementIndex;
				ElementType* NewBlockPtr = OldBlockPtr + 1;
				IndexType BlockSize = (NewLastIndex - ElementIndex) * ElementSize;

				AeonMemory::MemMove(NewBlockPtr, OldBlockPtr, BlockSize);
			}
			else
			{
				// Iterate from the end to the empty element, and move elements one index ahead
				for (IndexType Index = NewLastIndex; Index > ElementIndex; --Index)
				{
					ElementType* NewElemPtr = Storage.BufferPtr + Index;
					ElementType* OldElemPtr = NewElemPtr - 1;

					ElementType* RelocateElemPtr = AeonMemory::MemAccess<ElementType>(OldElemPtr);
					AeonMemory::MemConstruct<ElementType>(NewElemPtr, MoveElseCopy(*RelocateElemPtr));
					AeonMemory::MemDestruct<ElementType>(RelocateElemPtr);
				}
			}
		}
		void RelocateAndCloseSlotAtIndex_Internal(IndexType ElementIndex)
		{
			// Moves elements, assuming ElementIndex refers to an empty slot,
			// so as to fill it ("closing the slot") with an existing element,
			// and making the array contiguous.

			StorageType& Storage = Data.First();

			// ElementIndex may be equal to NewLastIndex, in which case either branch is a no-op.
			// Assumes this refers to the last element index with the empty element.
			IndexType NewLastIndex = Storage.ElemCount - 1;

			if constexpr (VIsTriviallyCopyable<ElementType>)
			{
				ElementType* NewBlockPtr = Storage.BufferPtr + ElementIndex;
				ElementType* OldBlockPtr = NewBlockPtr + 1;
				IndexType BlockSize = (NewLastIndex - ElementIndex) * ElementSize;

				AeonMemory::MemMove(NewBlockPtr, OldBlockPtr, BlockSize);
			}
			else
			{
				// Iterate from the empty element to the end, and move elements one index behind
				for (IndexType Index = ElementIndex; Index < NewLastIndex; ++Index)
				{
					ElementType* NewElemPtr = Storage.BufferPtr + Index;
					ElementType* OldElemPtr = NewElemPtr + 1;

					ElementType* RelocateElemPtr = AeonMemory::MemAccess<ElementType>(OldElemPtr);
					AeonMemory::MemConstruct<ElementType>(NewElemPtr, MoveElseCopy(*RelocateElemPtr));
					AeonMemory::MemDestruct<ElementType>(RelocateElemPtr);
				}
			}
		}

		static void RelocateElements_Internal(ElementPtrType OldStoragePtr, IndexType OldStorageCount,
											  ElementPtrType NewStoragePtr, IndexType NewStorageCount,
											  voidptr InstanceData)
		{
			AEON_UNUSED(NewStorageCount);

			auto InstancePtr = static_cast<GArray*>(InstanceData);

			StorageType& Storage = InstancePtr->Data.First();
			IndexType ElemCount = Storage.ElemCount;

			bool ShouldInvertRelocationIter = OldStoragePtr < NewStoragePtr && NewStoragePtr < OldStoragePtr + OldStorageCount;
			// Need to invert order of element relocation when the buffers overlap and the new buffer starts in the middle of the old one;
			// Note that buffers also overlap when 'NewStoragePtr < OldStoragePtr && OldStoragePtr < NewStoragePtr + NewStorageCount'
			// but if we inverted in that case, we would overwrite elements.
			if (ShouldInvertRelocationIter)
			{
				for (IndexType Index = ElemCount; Index > 0; --Index)
				{
					ElementType* OldElemPtr = OldStoragePtr + Index - 1;
					ElementType* NewElemPtr = NewStoragePtr + Index - 1;

					ElementType* RelocateElemPtr = AeonMemory::MemAccess<ElementType>(OldElemPtr);
					AeonMemory::MemConstruct<ElementType>(NewElemPtr, MoveElseCopy(*RelocateElemPtr));
					AeonMemory::MemDestruct<ElementType>(RelocateElemPtr);
				}
			}
			else
			{
				for (IndexType Index = 0; Index < ElemCount; ++Index)
				{
					ElementType* OldElemPtr = OldStoragePtr + Index;
					ElementType* NewElemPtr = NewStoragePtr + Index;

					ElementType* RelocateElemPtr = AeonMemory::MemAccess<ElementType>(OldElemPtr);
					AeonMemory::MemConstruct<ElementType>(NewElemPtr, MoveElseCopy(*RelocateElemPtr));
					AeonMemory::MemDestruct<ElementType>(RelocateElemPtr);
				}
			}
		}

		struct DDataStorage
		{
			ElementPtrType BufferPtr = nullptr; // Pointer to the bytes of the block of memory (also pointer to first element)
			IndexType ElemCount = 0; // Number of created elements
			IndexType ElemCapacity = 0; // Number of elements supported by allocated storage
		};

		using StorageType = DDataStorage;

		GCompressedDuple<StorageType, AllocatorType> Data;

		static constexpr szint ElementSize = Aeon::SizeOf(ElementType);
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GStaticArray

	template <CScalarObject ElemType, szint ElemCountValue>
	using GStaticArray = GArray<ElemType, GStaticAllocator<ElemType, ElemCountValue>>;
}

#endif
