project(AeonFramework CXX)

add_library(${PROJECT_NAME} STATIC)

set(AEON_BASE_PROJECT_NAME AeonEngine)

include(${CMAKE_SOURCE_DIR}/DefaultProjectConfig.cmake)

# TODO add fmt only if ${AEON_USING_FMT_LIBRARY} is true
# TODO compile 32-bit versions of these libs
target_link_libraries(${PROJECT_NAME}
	PUBLIC
		fmt
		utf8proc
		pcg_random
	)

add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Core)

add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Allocator)
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Concurrency)
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Container)
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Filesystem)
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Functional)
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Math)
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Memory)
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Text)
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Time)

target_sources(${PROJECT_NAME}
	PUBLIC
	# CHECKME Should Core.hpp be PRIVATE?
		${CMAKE_CURRENT_SOURCE_DIR}/Core.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/Framework.hpp
	)