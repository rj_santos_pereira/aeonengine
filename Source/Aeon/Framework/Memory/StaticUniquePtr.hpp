#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_MEMORY_STATICUNIQUEPTR
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_MEMORY_STATICUNIQUEPTR

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Memory/MemoryCommon.hpp"
#include "Aeon/Framework/Container/Tuple/Duple.hpp"

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GStaticUniquePtr

	template <CObject ObjType, CManagedDeleterForObject<ObjType> DelType = GDefaultDeleter<ObjType>>
	class [[deprecated("Prefer using GUniquePtr")]] GStaticUniquePtr : public IManagedPointer<ObjType>
	{
		using Base = IManagedPointer<ObjType>;

		AEON_DECLARE_TAG_CLASS(ConstructWithCtorArgs);
		AEON_DECLARE_TAG_CLASS(ConstructWithInitList);

		static_assert(!CManagedPointer<ObjType>,
					  "'GStaticUniquePtr<ObjType>': ObjType must not derive from IManagedPointer.");

	public:
		using ObjectType = Base::ObjectType;
		using SubObjectType = Base::SubObjectType;

		using ConstReferenceType = Base::ConstReferenceType;
		using ReferenceType = Base::ReferenceType;

		using ConstPointerType = Base::ConstPointerType;
		using PointerType = Base::PointerType;

		using DeleterType = DelType;

		GStaticUniquePtr()
			: Data{ nullptr, DeleterType{} }
		{
		}
		GStaticUniquePtr(nullptr_type)
			: GStaticUniquePtr()
		{
		}

		template <CObject NewObjectType, class NewDeleterType = DeleterType>
		explicit GStaticUniquePtr(NewObjectType* NewObjectPtr, NewDeleterType&& NewDeleter = DeleterType{})
		requires (CArray<ObjectType> ? CSameClass<NewObjectType, SubObjectType> : CConvertibleClass<NewObjectType*, SubObjectType*>)
				 && CSameClass<TDecayType<DeleterType>, DeleterType>
			: Data{ NewObjectPtr, Forward<NewDeleterType>(NewDeleter) }
		{
			if constexpr (!VIsSameClass<NewObjectType, SubObjectType>)
			{
				static_assert(VIsVirtuallyDestructible<ObjectType>,
				    		  "'GStaticUniquePtr<ObjType, DelType>::GStaticUniquePtr(NewObjType*, NewDelType&&)': "
							  "NewObjType must be the same type as ObjType, or if it is a subclass of ObjType, "
							  "ObjType must have a public virtual destructor.");
			}
		}

		// MakeStaticUniquePtr constructors
		template <class ... ArgTypes>
		explicit GStaticUniquePtr(ConstructWithCtorArgs, ArgTypes&& ... NewInitArgs)
			: Data{ nullptr, DeleterType{} }
		{
			voidptr AllocPtr = AeonMemory::MemAlloc(Aeon::SizeOf(ObjectType), Aeon::AlignOf(ObjectType));
			Data.First() = AeonMemory::MemConstruct<ObjectType>(AllocPtr, Forward<ArgTypes>(NewInitArgs) ...);
		}
		template <class NewObjectType, szint ObjectCountValue>
		explicit GStaticUniquePtr(ConstructWithInitList, InitList<NewObjectType> NewInitList, TObject<ObjectCountValue>)
			: Data{ nullptr, DeleterType{} }
		{
			using ObjectArrayType = TAddArrayDimension<SubObjectType, ObjectCountValue>;

			voidptr AllocPtr = AeonMemory::ZzInternal::AllocArray_WithNewExprInfo<ObjectArrayType>();
			Data.First() = AeonMemory::ZzInternal::ConstructArray<ObjectArrayType>(AllocPtr, NewInitList);
		}

		// MakeManagedPtr constructor
		template <class NewObjectType, class NewDeleterType>
		GStaticUniquePtr(ZzInternal::GUnmanagedPointerWrapper<NewObjectType, NewDeleterType>&& UnmanagedPtr)
			: GStaticUniquePtr(UnmanagedPtr.ObjectPtr, static_cast<NewDeleterType&&>(UnmanagedPtr.DeleterRef))
		{
		}

		// NOTE These are necessary, copy/move constructors cannot be templates,
		//  so even if the templated versions are appropriate, i.e., when ObjectType == OtherObjectType,
		//  the compiler may prefer the implicitly declared constructors, which would have the wrong semantics.

		// Copy/Move constructors
		GStaticUniquePtr(GStaticUniquePtr const&)
			= delete;
		GStaticUniquePtr(GStaticUniquePtr&& Other)
			: Data{ Exchange(Other.Data, StorageType{}) }
		{
		}

		// Copy/Move constructors from convertible object type
		template <CObjectElseVoid OtherObjectType, CManagedDeleter OtherDeleterType>
		GStaticUniquePtr(GStaticUniquePtr<OtherObjectType, OtherDeleterType> const&)
		requires CConvertibleClass<OtherObjectType*, ObjectType*>
		    = delete;
		template <CObjectElseVoid OtherObjectType, CManagedDeleter OtherDeleterType>
		GStaticUniquePtr(GStaticUniquePtr<OtherObjectType, OtherDeleterType>&& Other)
		requires CConvertibleClass<OtherObjectType*, ObjectType*>
			: Data{ Exchange(Other.Data.First(), nullptr), DeleterType{} }
		{
			if constexpr (!VIsSameClass<OtherObjectType, ObjectType>)
			{
				static_assert(VIsVirtuallyDestructible<ObjectType>,
							  "'GStaticUniquePtr<ObjType, DelType>::GStaticUniquePtr(GStaticUniquePtr<OtherObjType, OtherDelType>&&)': "
							  "OtherObjType must be the same type as ObjType, or if it is a subclass of ObjType, "
							  "ObjType must have a public virtual destructor.");
			}
		}

		~GStaticUniquePtr()
		{
			Reset();
		}

		GStaticUniquePtr& operator=(GStaticUniquePtr Other)
		{
			using Aeon::Swap;
			Swap(*this, Other);

			return *this;
		}

		ConstPointerType Get() const
		{
			return Data.First();
		}
		PointerType Get()
		{
			return AEON_INVOKE_CONST_OVERLOAD(Get);
		}

		DeleterType GetDeleter() const
		{
			return Data.Second();
		}

		void Reset()
		{
			if (auto& ObjectPtr = Data.First())
			{
				DeleterType& Deleter = Data.Second();

				Deleter.Delete(ObjectPtr);

				ObjectPtr = nullptr;
			}
		}

		AEON_NODISCARD SubObjectType* ToUnmanaged()
		{
			auto OldObjectPtr = Exchange(Data.First(), nullptr);
			return OldObjectPtr;
		}

		template <CObject NewObjectType, class NewDeleterType = GDefaultDeleter<NewObjectType>>
		AEON_NODISCARD
		GStaticUniquePtr<NewObjectType, TDecayType<NewDeleterType>> Downcast(NewDeleterType&& NewDeleter = TDecayType<NewDeleterType>{})
		requires CConvertibleClass<NewObjectType*, ObjectType*>
		{
			using UniquePtrType = GStaticUniquePtr<NewObjectType, TDecayType<NewDeleterType>>;
			if (auto NewObjectPtr = Exchange(Data.First(), nullptr))
			{
				return UniquePtrType{ static_cast<TAddPointer<TRemoveArrayDimension<NewObjectType>>>(NewObjectPtr),
									  Forward<NewDeleterType>(NewDeleter) };
			}
			return UniquePtrType{};
		}

		ConstReferenceType operator*() const
		{
			auto ObjectPtr = Data.First();
			AEON_ASSERT(ObjectPtr);
			return *ObjectPtr;
		}
		ReferenceType operator*()
		{
			return AEON_INVOKE_CONST_OVERLOAD(operator*);
		}

		ConstReferenceType operator[](szint Index) const
		requires CArray<ObjectType>
		{
			auto ObjectPtr = Data.First();
			AEON_ASSERT(ObjectPtr);
			return *(ObjectPtr + Index);
		}
		ReferenceType operator[](szint Index)
		requires CArray<ObjectType>
		{
			return AEON_INVOKE_CONST_OVERLOAD(operator[], Index);
		}

		ConstPointerType operator->() const
		requires CClass<ObjectType> || CUnion<ObjectType>
		{
			auto ObjectPtr = Data.First();
			AEON_ASSERT(ObjectPtr);
			return ObjectPtr;
		}
		PointerType operator->()
		requires CClass<ObjectType> || CUnion<ObjectType>
		{
			return AEON_INVOKE_CONST_OVERLOAD(operator->);
		}

		explicit operator bool() const
		{
			auto ObjectPtr = Data.First();
			return ObjectPtr != nullptr;
		}

		bool operator==(GStaticUniquePtr const& Other) const
		{
			return Data.First() == Other.Data.First();
		}
		strong_ordering operator<=>(GStaticUniquePtr const& Other) const
		{
			return Data.First() <=> Other.Data.First();
		}

		friend void Swap(GStaticUniquePtr& UniquePtr1, GStaticUniquePtr& UniquePtr2)
		{
			using Aeon::Swap;

			Swap(UniquePtr1.Data, UniquePtr2.Data);
		}

	private:
		using StorageType = GCompressedDuple<SubObjectType*, DeleterType>;

		StorageType Data;

		template <class OtherObjType, class ... ArgTypes>
		friend auto ZzInternal_MakeStaticUniquePtr(ArgTypes&& ... Args)
			-> GStaticUniquePtr<OtherObjType>;

		template <class OtherObjType>
		friend auto ZzInternal_MakeStaticUniquePtr(InitList<TRemoveArrayDimension<OtherObjType>> InitList)
			-> GStaticUniquePtr<TRemoveArrayDimensionSize<OtherObjType>>;
		template <class OtherObjType>
		friend auto ZzInternal_MakeStaticUniquePtr(RvalRefInitList<TRemoveArrayDimension<OtherObjType>> InitList)
			-> GStaticUniquePtr<TRemoveArrayDimensionSize<OtherObjType>>;

		template <CObject OtherObjType, CManagedDeleterForObject<OtherObjType>>
		friend class GStaticUniquePtr;
	};

	template <CObjectElseVoid ObjectType, szint ArraySizeValue>
	class GStaticUniquePtr<ObjectType[ArraySizeValue]>; // Disallow bounded array object types

	// CTAD deduction guides

	template <CObject ObjectType, class DeleterType>
	GStaticUniquePtr(ZzInternal::GUnmanagedPointerWrapper<ObjectType, DeleterType>&&) -> GStaticUniquePtr<ObjectType>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  MakeStaticUniquePtr

	template <class ObjectType, class ... ArgTypes>
	AEON_SPEC(FORCEINLINE)
	auto ZzInternal_MakeStaticUniquePtr(ArgTypes&& ... Args)
		-> GStaticUniquePtr<ObjectType>
	{
		using UniquePtrType = GStaticUniquePtr<ObjectType>;
		return UniquePtrType(UniquePtrType::ConstructWithCtorArgs::Tag, Forward<ArgTypes>(Args) ...);
	}

	template <class ObjectType>
	AEON_SPEC(FORCEINLINE)
	auto ZzInternal_MakeStaticUniquePtr(InitList<TRemoveArrayDimension<ObjectType>> InitList)
		-> GStaticUniquePtr<TRemoveArrayDimensionSize<ObjectType>>
	{
		using UniquePtrType = GStaticUniquePtr<TRemoveArrayDimensionSize<ObjectType>>;
		return UniquePtrType(UniquePtrType::ConstructWithInitList::Tag, InitList, TArrayDimensionSize<ObjectType>{});
	}
	template <class ObjectType>
	AEON_SPEC(FORCEINLINE)
	auto ZzInternal_MakeStaticUniquePtr(RvalRefInitList<TRemoveArrayDimension<ObjectType>> InitList)
		-> GStaticUniquePtr<TRemoveArrayDimensionSize<ObjectType>>
	{
		using UniquePtrType = GStaticUniquePtr<TRemoveArrayDimensionSize<ObjectType>>;
		return UniquePtrType(UniquePtrType::ConstructWithInitList::Tag, InitList, TArrayDimensionSize<ObjectType>{});
	}

	// HACK IDE complains when constraining the ObjectType on friend functions,
	//  as it doesn't correctly determine that the functions are declared friends,
	//  so we constrain these instead.

	template <CScalarObject ObjectType, class ... ArgTypes>
	AEON_SPEC(NODISCARD, FORCEINLINE)
	auto MakeStaticUniquePtr(ArgTypes&& ... Args)
		-> GStaticUniquePtr<ObjectType>
	{
		return ZzInternal_MakeStaticUniquePtr<ObjectType>(Forward<ArgTypes>(Args) ...);
	}

	template <CBoundedArray ObjectType>
	AEON_SPEC(NODISCARD, FORCEINLINE)
	auto MakeStaticUniquePtr(InitList<TRemoveArrayDimension<ObjectType>> InitList = {})
		-> GStaticUniquePtr<TRemoveArrayDimensionSize<ObjectType>>
	requires CCopyable<TRemoveArrayDimension<ObjectType>>
	{
		return ZzInternal_MakeStaticUniquePtr<ObjectType>(InitList);
	}
	template <CBoundedArray ObjectType>
	AEON_SPEC(NODISCARD, FORCEINLINE)
	auto MakeStaticUniquePtr(RvalRefInitList<TRemoveArrayDimension<ObjectType>> InitList = {})
		-> GStaticUniquePtr<TRemoveArrayDimensionSize<ObjectType>>
	requires CMoveable<TRemoveArrayDimension<ObjectType>> && CNotCopyable<TRemoveArrayDimension<ObjectType>>
	{
		return ZzInternal_MakeStaticUniquePtr<ObjectType>(InitList);
	}
	template <CBoundedArray ObjectType>
	AEON_SPEC(NODISCARD, FORCEINLINE)
	auto MakeStaticUniquePtr(RvalRefInitList<TRemoveArrayDimension<ObjectType>> InitList)
		-> GStaticUniquePtr<TRemoveArrayDimensionSize<ObjectType>>
	requires CMoveable<TRemoveArrayDimension<ObjectType>>
	{
		return ZzInternal_MakeStaticUniquePtr<ObjectType>(InitList);
	}
}

#endif