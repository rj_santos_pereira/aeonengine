#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_MEMORY_UNIQUEPTR
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_MEMORY_UNIQUEPTR

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Memory/MemoryCommon.hpp"
#include "Aeon/Framework/Memory/StorageController.hpp"
#include "Aeon/Framework/Functional/Optional.hpp"

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GUniquePtr

	template <CObjectElseVoid ObjType>
	class GUniquePtr : public IManagedPointer<ObjType>
	{
		using Base = IManagedPointer<ObjType>;

		using BaseStorageType = ZzInternal::IStorageController;

		AEON_DECLARE_TAG_CLASS(ConstructWithCtorArgs);
		AEON_DECLARE_TAG_CLASS(ConstructWithInitList);

		static_assert(!CManagedPointer<ObjType>,
		    		  "'GUniquePtr<ObjType>': ObjType must not derive from IManagedPointer.");

		AEON_DECLARE_TAG_CLASS(ConstructWithData);

	public:
		using ObjectType = Base::ObjectType;
		using SubObjectType = Base::SubObjectType;

		using ConstReferenceType = Base::ConstReferenceType;
		using ReferenceType = Base::ReferenceType;

		using ConstPointerType = Base::ConstPointerType;
		using PointerType = Base::PointerType;

		GUniquePtr()
			: ObjectPtr(nullptr)
			, StoragePtr(nullptr)
		{
		}
		GUniquePtr(nullptr_type)
			: GUniquePtr()
		{
		}

		// GUniquePtr::Downcast constructor
		explicit GUniquePtr(ConstructWithData, SubObjectType* NewObjectPtr, BaseStorageType* NewStoragePtr)
			: ObjectPtr(NewObjectPtr)
			, StoragePtr(NewStoragePtr)
		{
		}

		// NOTE This constructor is marked explicit to disallow implicit conversions for methods like operator==
		template <CObjectElseVoid NewObjectType, class NewDeleterType = GDefaultDeleter<TPropagateArrayDimension<ObjectType, NewObjectType>>>
		explicit GUniquePtr(NewObjectType* NewObjectPtr, NewDeleterType&& NewDeleter = TDecayType<NewDeleterType>{})
		requires (CArray<ObjectType> ? CSameClass<NewObjectType, SubObjectType> : CConvertibleClass<NewObjectType*, SubObjectType*>)
				 // NOTE Since we have type-erasure on the managed ptr, we enforce the actual object type on the deleter
				 && CManagedDeleterForObject<TDecayType<NewDeleterType>, TPropagateArrayDimension<ObjectType, NewObjectType>>
			: ObjectPtr(NewObjectPtr)
			, StoragePtr(nullptr)
		{
			using StorageType = ZzInternal::GMultiBlockStorageController<BaseStorageType, NewObjectType, TDecayType<NewDeleterType>>;

			if (NewObjectPtr)
			{
				StoragePtr = StorageType::New(NewObjectPtr, Forward<NewDeleterType>(NewDeleter));
			}
		}

		// MakeUniquePtr constructors
		template <class ... ArgTypes>
		explicit GUniquePtr(ConstructWithCtorArgs, ArgTypes&& ... NewArgs)
		{
			using StorageType = ZzInternal::GSingleBlockStorageController<BaseStorageType, ObjectType>;

			auto [NewObjectPtr, NewStoragePtr] = StorageType::New(Forward<ArgTypes>(NewArgs) ...);

			ObjectPtr = NewObjectPtr;
			StoragePtr = NewStoragePtr;
		}
		template <class NewObjectType, szint ObjectCountValue>
		explicit GUniquePtr(ConstructWithInitList, InitList<NewObjectType> NewInitList, TObject<ObjectCountValue>)
		{
			using StorageType = ZzInternal::GSingleBlockStorageController<BaseStorageType, ObjectType>;

			auto [NewObjectPtr, NewStoragePtr] = StorageType::template New<NewObjectType, ObjectCountValue>(NewInitList);

			ObjectPtr = NewObjectPtr;
			StoragePtr = NewStoragePtr;
		}

		// MakeManagedPtr constructor
		template <class NewObjectType, class NewDeleterType>
		GUniquePtr(ZzInternal::GUnmanagedPointerWrapper<NewObjectType, NewDeleterType>&& UnmanagedPtr)
			: GUniquePtr(UnmanagedPtr.ObjectPtr, static_cast<NewDeleterType&&>(UnmanagedPtr.DeleterRef))
		{
		}

		// NOTE These are necessary, copy/move constructors cannot be templates,
		//  so even if the template versions are appropriate, e.g., when ObjectType == OtherObjectType,
		//  the compiler may prefer the implicitly declared constructors, which would have the wrong semantics.

		// Copy/Move constructors
		GUniquePtr(GUniquePtr const&)
			= delete;
		GUniquePtr(GUniquePtr&& Other)
			: ObjectPtr(Exchange(Other.ObjectPtr, nullptr))
			, StoragePtr(Exchange(Other.StoragePtr, nullptr))
		{
		}

		// Copy/Move constructors from convertible object type (e.g. from derived other to base this)
		template <CObjectElseVoid OtherObjectType>
		GUniquePtr(GUniquePtr<OtherObjectType> const&)
		requires CConvertibleClass<OtherObjectType*, ObjectType*>
		    = delete;
		template <CObjectElseVoid OtherObjectType>
		GUniquePtr(GUniquePtr<OtherObjectType>&& Other)
		requires CConvertibleClass<OtherObjectType*, ObjectType*>
			: ObjectPtr(Exchange(Other.ObjectPtr, nullptr))
			, StoragePtr(Exchange(Other.StoragePtr, nullptr))
		{
		}

		~GUniquePtr()
		{
			Reset();
		}

		GUniquePtr& operator=(GUniquePtr Other)
		{
			using Aeon::Swap;
			Swap(*this, Other);

			return *this;
		}

		ConstPointerType Get() const
		{
			return ObjectPtr;
		}
		PointerType Get()
		{
			return AEON_INVOKE_CONST_OVERLOAD(Get);
		}

		template <class DeleterType>
		GOptional<DeleterType> GetDeleter() const
		{
			if (StoragePtr)
			{
				if (auto DeleterPtr = StoragePtr->CheckGetDeleter(typeid(DeleterType)))
				{
					return GOptional<DeleterType>{ TConstructInPlace::Tag, *static_cast<DeleterType const*>(DeleterPtr) };
				}
			}
			return GOptional<DeleterType>{};
		}

		void Reset()
		{
			if (StoragePtr)
			{
				ObjectPtr = nullptr;
				BaseStorageType* OldStoragePtr = Exchange(StoragePtr, nullptr);

				OldStoragePtr->DeleteObject();
				OldStoragePtr->DeleteThis();
			}
		}

		AEON_NODISCARD SubObjectType* ToUnmanaged()
		{
			if (StoragePtr)
			{
				SubObjectType* OldObjectPtr = Exchange(ObjectPtr, nullptr);
				BaseStorageType* OldStoragePtr = Exchange(StoragePtr, nullptr);

				OldStoragePtr->DeleteThis();

				return OldObjectPtr;
			}
			return nullptr;
		}

		// Downcast checks if stored object is of type ActualObjectType,
		// and requires NewObjectType to be an unambiguous public base class of ActualObjectType, similarly to dynamic_cast
		template <CObject NewObjectType, CObject ActualObjectType = NewObjectType>
		AEON_NODISCARD GUniquePtr<NewObjectType> Downcast()
		requires CConvertibleClass<NewObjectType*, ObjectType*>
				 && CConvertibleClass<ActualObjectType*, NewObjectType*>
		{
			using UniquePtrType = GUniquePtr<NewObjectType>;
			if (StoragePtr && StoragePtr->CheckObjectType(typeid(NewObjectType)))
			{
				auto NewObjectPtr = static_cast<TAddPointer<TRemoveArrayDimension<NewObjectType>>>(Exchange(ObjectPtr, nullptr));
				auto NewStoragePtr = Exchange(StoragePtr, nullptr);

				return UniquePtrType{ UniquePtrType::ConstructWithData::Tag, NewObjectPtr, NewStoragePtr };
			}
			return UniquePtrType{};
		}

		ConstReferenceType operator*() const
		requires CObject<ObjectType>
		{
			AEON_ASSERT(ObjectPtr);
			return *ObjectPtr;
		}
		ReferenceType operator*()
		requires CObject<ObjectType>
		{
			return AEON_INVOKE_CONST_OVERLOAD(operator*);
		}

		ConstReferenceType operator[](szint Index) const
		requires CArray<ObjectType>
		{
			AEON_ASSERT(ObjectPtr);
			return *(ObjectPtr + Index);
		}
		ReferenceType operator[](szint Index)
		requires CArray<ObjectType>
		{
			return AEON_INVOKE_CONST_OVERLOAD(operator[], Index);
		}

		ConstPointerType operator->() const
		requires CClass<ObjectType> || CUnion<ObjectType>
		{
			AEON_ASSERT(ObjectPtr);
			return ObjectPtr;
		}
		PointerType operator->()
		requires CClass<ObjectType> || CUnion<ObjectType>
		{
			return AEON_INVOKE_CONST_OVERLOAD(operator->);
		}

		explicit operator bool() const
		{
			return StoragePtr != nullptr;
		}

		bool operator==(GUniquePtr const& Other) const
		{
			return ObjectPtr == Other.ObjectPtr;
		}
		strong_ordering operator<=>(GUniquePtr const& Other) const
		{
			return ObjectPtr <=> Other.ObjectPtr;
		}

		friend void Swap(GUniquePtr& UniquePtr1, GUniquePtr& UniquePtr2)
		{
			using Aeon::Swap;

			Swap(UniquePtr1.ObjectPtr, UniquePtr2.ObjectPtr);
			Swap(UniquePtr1.StoragePtr, UniquePtr2.StoragePtr);
		}

	private:
		SubObjectType* ObjectPtr;
		BaseStorageType* StoragePtr;

		template <class OtherObjType, class ... ArgTypes>
		friend auto ZzInternal_MakeUniquePtr(ArgTypes&& ... Args)
			-> GUniquePtr<OtherObjType>;

		template <class OtherObjType>
		friend auto ZzInternal_MakeUniquePtr(InitList<TRemoveArrayDimension<OtherObjType>> InitList)
			-> GUniquePtr<TRemoveArrayDimensionSize<OtherObjType>>;
		template <class OtherObjType>
		friend auto ZzInternal_MakeUniquePtr(RvalRefInitList<TRemoveArrayDimension<OtherObjType>> InitList)
			-> GUniquePtr<TRemoveArrayDimensionSize<OtherObjType>>;

		template <CObjectElseVoid>
		friend class GUniquePtr;
	};

	template <CObjectElseVoid ObjectType, szint ArraySizeValue>
	class GUniquePtr<ObjectType[ArraySizeValue]>; // Disallow bounded array object types

	// CTAD deduction guides

	template <CObject ObjectType, class DeleterType>
	GUniquePtr(ZzInternal::GUnmanagedPointerWrapper<ObjectType, DeleterType>&&) -> GUniquePtr<ObjectType>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  MakeUniquePtr

	template <class ObjectType, class ... ArgTypes>
	AEON_SPEC(FORCEINLINE)
	auto ZzInternal_MakeUniquePtr(ArgTypes&& ... Args)
		-> GUniquePtr<ObjectType>
	{
		using UniquePtrType = GUniquePtr<ObjectType>;
		return UniquePtrType(UniquePtrType::ConstructWithCtorArgs::Tag, Forward<ArgTypes>(Args) ...);
	}

	template <class ObjectType>
	AEON_SPEC(FORCEINLINE)
	auto ZzInternal_MakeUniquePtr(InitList<TRemoveArrayDimension<ObjectType>> InitList)
		-> GUniquePtr<TRemoveArrayDimensionSize<ObjectType>>
	{
		using UniquePtrType = GUniquePtr<TRemoveArrayDimensionSize<ObjectType>>;
		return UniquePtrType(UniquePtrType::ConstructWithInitList::Tag, InitList, TArrayDimensionSize<ObjectType>{});
	}
	template <class ObjectType>
	AEON_SPEC(FORCEINLINE)
	auto ZzInternal_MakeUniquePtr(RvalRefInitList<TRemoveArrayDimension<ObjectType>> InitList)
		-> GUniquePtr<TRemoveArrayDimensionSize<ObjectType>>
	{
		using UniquePtrType = GUniquePtr<TRemoveArrayDimensionSize<ObjectType>>;
		return UniquePtrType(UniquePtrType::ConstructWithInitList::Tag, InitList, TArrayDimensionSize<ObjectType>{});
	}

	// HACK IDE complains when constraining the ObjectType on friend functions,
	//  as it doesn't correctly determine that the functions are declared friends,
	//  so we constrain these instead.

	template <CScalarObject ObjectType, class ... ArgTypes>
	AEON_SPEC(NODISCARD, FORCEINLINE)
	auto MakeUniquePtr(ArgTypes&& ... Args)
		-> GUniquePtr<ObjectType>
	{
		return ZzInternal_MakeUniquePtr<ObjectType>(Forward<ArgTypes>(Args) ...);
	}

	template <CBoundedArray ObjectType>
	AEON_SPEC(NODISCARD, FORCEINLINE)
	auto MakeUniquePtr(InitList<TRemoveArrayDimension<ObjectType>> InitList = {})
		-> GUniquePtr<TRemoveArrayDimensionSize<ObjectType>>
	requires CCopyable<TRemoveArrayDimension<ObjectType>>
	{
		return ZzInternal_MakeUniquePtr<ObjectType>(InitList);
	}
	template <CBoundedArray ObjectType>
	AEON_SPEC(NODISCARD, FORCEINLINE)
	auto MakeUniquePtr(RvalRefInitList<TRemoveArrayDimension<ObjectType>> InitList = {})
		-> GUniquePtr<TRemoveArrayDimensionSize<ObjectType>>
	requires CMoveable<TRemoveArrayDimension<ObjectType>> && CNotCopyable<TRemoveArrayDimension<ObjectType>>
	{
		return ZzInternal_MakeUniquePtr<ObjectType>(InitList);
	}
	template <CBoundedArray ObjectType>
	AEON_SPEC(NODISCARD, FORCEINLINE)
	auto MakeUniquePtr(RvalRefInitList<TRemoveArrayDimension<ObjectType>> InitList)
		-> GUniquePtr<TRemoveArrayDimensionSize<ObjectType>>
	requires CMoveable<TRemoveArrayDimension<ObjectType>>
	{
		return ZzInternal_MakeUniquePtr<ObjectType>(InitList);
	}
}

#endif