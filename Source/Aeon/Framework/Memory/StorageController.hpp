#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_MEMORY_STORAGECONTROLLER
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_MEMORY_STORAGECONTROLLER

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Container/Tuple/Duple.hpp"
#include "Aeon/Framework/Memory/MemoryCommon.hpp"
#include "Aeon/Framework/Memory/ThriftyPtr.hpp"

namespace Aeon::ZzInternal
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  IStorageController

	class IStorageController
	{
	protected:
		~IStorageController() = default;

	public:
		virtual voidptr GetObjectPtr() = 0;
		virtual bool CheckObjectType(STypeInfo TypeInfo) const = 0;
		virtual cvoidptr CheckGetDeleter(STypeInfo TypeInfo) const = 0;

		virtual void DeleteObject() = 0;
		virtual void DeleteThis() = 0;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GSingleBlockStorageController

	template <class BaseType, class ObjectType>
	class GSingleBlockStorageController final : public BaseType
	{
		static_assert(VIsBaseClass<IStorageController, BaseType>);

		using SubObjectType = TRemoveArrayDimension<ObjectType>;

	public:
		template <class ... ArgTypes>
		static GDuple<SubObjectType*, GSingleBlockStorageController*> New(ArgTypes&& ... InitArgs)
		requires CNotArray<ObjectType>
		{
			constexpr szint ObjectSize = Aeon::SizeOf(ObjectType);
			constexpr szint ControllerSize = Aeon::SizeOf(GSingleBlockStorageController);
			constexpr algnint Alignment = AeonMath::Max(Aeon::AlignOf(ObjectType), Aeon::AlignOf(GSingleBlockStorageController));

			constexpr bool NeedsPadding = (ObjectSize % szint(Alignment)) != 0;
			constexpr szint Padding = NeedsPadding ? (szint(Alignment) - (ObjectSize % szint(Alignment))) : 0;

			constexpr szint Size = ObjectSize + Padding + ControllerSize;

			byteptr AllocPtr = AeonMemory::MemAlloc_Ex<byte[]>(
				Size, Alignment, AeonMemory::EMemAllocExOptions::RelaxAlignConstraint
				);

			byteptr OffsetAllocPtr = AllocPtr + ObjectSize + Padding;

			AEON_ASSERT(AeonMemory::IsBlockPointerValid(OffsetAllocPtr, Aeon::AlignOf(GSingleBlockStorageController)));

			auto ObjectPtr = AeonMemory::MemConstruct<ObjectType>(AllocPtr, Forward<ArgTypes>(InitArgs) ...);
			auto StoragePtr = AeonMemory::MemConstruct<GSingleBlockStorageController>(OffsetAllocPtr, ObjectPtr);

			return { ObjectPtr, StoragePtr };
		}

		template <class NewObjectType, szint ObjectCountValue>
		static GDuple<SubObjectType*, GSingleBlockStorageController*> New(InitList<NewObjectType> InitList)
		requires CArray<ObjectType>
				 && (CSameClass<NewObjectType, SubObjectType> || CSameClass<NewObjectType, GRvalRefWrapper<SubObjectType>>)
		{
			using ObjectArrayType = TAddArrayDimension<SubObjectType, ObjectCountValue>;

			constexpr szint ObjectArraySize = Aeon::SizeOf(ObjectArrayType);
			constexpr szint ControllerSize = Aeon::SizeOf(GSingleBlockStorageController);
			constexpr algnint Alignment = AeonMath::Max(Aeon::AlignOf(SubObjectType), Aeon::AlignOf(GSingleBlockStorageController));

		//	constexpr szint InfoBlockPadding = szint(Alignment);
			constexpr bool NeedsPadding = (ObjectArraySize % szint(Alignment)) != 0;
			constexpr szint Padding = NeedsPadding ? (szint(Alignment) - (ObjectArraySize % szint(Alignment))) : 0;

			constexpr szint Size = /*InfoBlockPadding +*/ ObjectArraySize + Padding + ControllerSize;

			byteptr AllocPtr = AeonMemory::ZzInternal::AllocArray_WithNewExprInfo<ObjectArrayType>(
				Size, Alignment, AeonMemory::EMemAllocExOptions::RelaxAlignConstraint
				);

			// NOTE 'OffsetCorrection <= InfoBlockPadding' always holds (and is required to),
			//  since 'Alignment - (bit_cast<ptrint>(OffsetAllocPtr) % Alignment) <= Alignment'
			//  is equivalent to 'bit_cast<ptrint>(OffsetAllocPtr) % Alignment >= 0',
			//  which is always true for positive integers.

			// FIXME document reasoning for this (prob lack of control of alignment in AllocArray_WithNewExprInfo, which could be fixed)
		//	byteptr OffsetAllocPtr = static_cast<byteptr>(AllocPtr) + ObjectArraySize;
		//	if (szint OffsetCorrection = (bit_cast<ptrint>(OffsetAllocPtr) % szint(Alignment)))
		//	{
		//		OffsetAllocPtr += (szint(Alignment) - OffsetCorrection);
		//	}

			byteptr OffsetAllocPtr = AllocPtr + ObjectArraySize + Padding;

			AEON_ASSERT(AeonMemory::IsBlockPointerValid(OffsetAllocPtr, Aeon::AlignOf(GSingleBlockStorageController)));

			auto ObjectPtr = AeonMemory::ZzInternal::ConstructArray<ObjectArrayType>(AllocPtr, InitList);
			auto StoragePtr = AeonMemory::MemConstruct<GSingleBlockStorageController>(OffsetAllocPtr, ObjectPtr);

			return { ObjectPtr, StoragePtr };
		}

		explicit GSingleBlockStorageController(SubObjectType* NewObjectPtr)
			: Data(NewObjectPtr)
		{
		}

		virtual voidptr GetObjectPtr() override
		{
			auto ObjectPtr = Data.GetPointer();
			bool IsAlive = !(Data.GetData() & IsDeadFlag);
			return IsAlive ? ObjectPtr : nullptr;
		}
		virtual bool CheckObjectType(STypeInfo TypeInfo) const override
		{
			return TypeInfo == typeid(ObjectType);
		}
		virtual cvoidptr CheckGetDeleter(STypeInfo TypeInfo) const override
		{
			using DeleterType = GDefaultDeleter<ObjectType>;
			static DeleterType DummyDeleter;
			return TypeInfo == typeid(DeleterType) ? addressof(DummyDeleter) : nullptr;
		}

		virtual void DeleteObject() override
		{
			auto ObjectPtr = Data.GetPointer();

			if constexpr (VIsArray<ObjectType>)
			{
				AeonMemory::ZzInternal::DestructArray_WithNewExprInfo<ObjectType>(ObjectPtr);
			}
			else
			{
				AeonMemory::MemDestruct<SubObjectType>(ObjectPtr);
			}

			// Mark the object as destructed, note that we still need the pointer to dealloc the storage
			auto Flags = Data.GetData();
			Flags |= IsDeadFlag;
			Data.SetData(Flags);
		}
		virtual void DeleteThis() override
		{
			voidptr AllocPtr = Data.GetPointer();
			bool CanDealloc = Data.GetData() & IsDeadFlag;

			// NOTE Can't touch anything in 'this' from this point onwards
			AeonMemory::MemDestruct<GSingleBlockStorageController>(this);

			// If we already destructed the object, then we can dealloc the storage;
			// Otherwise, we released the object back to unmanaged memory,
			// and it's the user's responsibility to delete the object.
			// GDefaultDeleter is capable of destructing (and deallocating) the object correctly,
			// hence why CheckGetDeleter only returns a valid object for that type.
			if (CanDealloc)
			{
				if constexpr (VIsArray<ObjectType>)
				{
					AeonMemory::ZzInternal::DeallocArray_WithNewExprInfo<ObjectType>(AllocPtr);
				}
				else
				{
					AeonMemory::MemDealloc(AllocPtr);
				}
			}
		}

	private:
		GThriftyPtr<SubObjectType, uint8, 1> Data;

		static constexpr uint8 IsDeadFlag = 0x1;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GMultiBlockStorageController

	template <class BaseType, class ObjectType, class DeleterType>
	class GMultiBlockStorageController final : public BaseType
	{
		static_assert(VIsBaseClass<IStorageController, BaseType>);

		using SubObjectType = TRemoveArrayDimension<ObjectType>;

	public:
		template <class NewDeleterType>
		static GMultiBlockStorageController* New(SubObjectType* NewObjectPtr, NewDeleterType&& NewDeleter)
		{
			return new GMultiBlockStorageController{ NewObjectPtr, Forward<NewDeleterType>(NewDeleter) };
		}

		template <class NewDeleterType>
		explicit GMultiBlockStorageController(SubObjectType* NewObjectPtr, NewDeleterType&& NewDeleter)
			: Data{ NewObjectPtr, Forward<NewDeleterType>(NewDeleter) }
		{
		}

		virtual voidptr GetObjectPtr() override
		{
			auto ObjectPtr = Data.First();
			return ObjectPtr;
		}
		virtual bool CheckObjectType(STypeInfo TypeInfo) const override
		{
			return TypeInfo == typeid(ObjectType);
		}
		virtual cvoidptr CheckGetDeleter(STypeInfo TypeInfo) const override
		{
			auto const& Deleter = Data.Second();
			return TypeInfo == typeid(DeleterType) ? addressof(Deleter) : nullptr;
		}

		virtual void DeleteObject() override
		{
			auto& ObjectPtr = Data.First();
			auto& Deleter = Data.Second();

			Deleter.Delete(ObjectPtr);

			ObjectPtr = nullptr;
		}
		virtual void DeleteThis() override
		{
			delete this;
		}

	private:
		GCompressedDuple<SubObjectType*, DeleterType> Data;
	};
}

#endif