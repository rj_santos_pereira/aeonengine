#add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/)

target_sources(${PROJECT_NAME}
	PUBLIC
		${CMAKE_CURRENT_SOURCE_DIR}/MemoryCommon.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/StorageController.hpp

		${CMAKE_CURRENT_SOURCE_DIR}/ThriftyPtr.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/StaticUniquePtr.hpp

		${CMAKE_CURRENT_SOURCE_DIR}/UniquePtr.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/SharedPtr.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/WeakSharedPtr.hpp
#	PRIVATE
#		${CMAKE_CURRENT_SOURCE_DIR}/
	)