#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_MEMORY_THRIFTYPTR
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_MEMORY_THRIFTYPTR

#include "Aeon/Framework/Core.hpp"

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GThriftyPtr

	template <class Type>
	concept CBitfield = CBool<Type> || CCharacter<Type> || CInteger<Type> || CEnum<Type>;
	template <class Type>
	concept CNotBitfield = !CBitfield<Type>;

	template <CObjectElseVoid ObjType, CBitfield DataType, szint DataBitCountValue = AeonMath::LogPowerOfTwo(szint(Aeon::AlignOf(ObjType)))>
	requires (DataBitCountValue <= AeonMath::LogPowerOfTwo(szint(Aeon::AlignOf(ObjType))))
	class GThriftyPtr
	{
		using StorageType = ptrint;

	public:
		using PointerType = TAddPointer<TRemoveArrayDimension<ObjType>>;

		GThriftyPtr() = default;

		GThriftyPtr(PointerType NewPointer, DataType NewData = DataType{})
		{
			Storage = (bit_cast<StorageType>(NewPointer) & PointerMask)
					| (static_cast<StorageType>(NewData) & DataMask);
		}

		GThriftyPtr(GThriftyPtr const& Other)
			: Storage(Other.Storage)
		{
		}
		GThriftyPtr(GThriftyPtr&& Other)
			: Storage(Exchange(Other.Storage, StorageType{}))
		{
		}

		GThriftyPtr& operator=(GThriftyPtr Other)
		{
			using Aeon::Swap;
			Swap(Storage, Other.Storage);
			return *this;
		}

		PointerType GetPointer() const
		{
			return bit_cast<PointerType>(Storage & PointerMask);
		}
		void SetPointer(PointerType NewPointer)
		{
			// Clear the pointer bits
			Storage &= ~PointerMask;
			// Overwrite bits with the new pointer
			Storage |= bit_cast<StorageType>(NewPointer) & PointerMask;
		}

		DataType GetData() const
		{
			return static_cast<DataType>(Storage & DataMask);
		}
		void SetData(DataType NewData)
		{
			// Clear the data bits
			Storage &= ~DataMask;
			// Overwrite bits with the new data
			Storage |= static_cast<StorageType>(NewData) & DataMask;
		}

		GThriftyPtr& operator+=(diffint Offset)
		requires CNotVoid<ObjType>
		{
			PointerType NewPointer = GetPointer() + Offset;
			SetPointer(NewPointer);
			return *this;
		}
		GThriftyPtr& operator-=(diffint Offset)
		requires CNotVoid<ObjType>
		{
			PointerType NewPointer = GetPointer() - Offset;
			SetPointer(NewPointer);
			return *this;
		}

		friend GThriftyPtr operator+(GThriftyPtr Pointer, diffint Offset)
		requires CNotVoid<ObjType>
		{
			return GThriftyPtr{ Pointer } += Offset;
		}
		friend GThriftyPtr operator+(diffint Offset, GThriftyPtr Pointer)
		requires CNotVoid<ObjType>
		{
			return GThriftyPtr{ Pointer } += Offset;
		}

		friend GThriftyPtr operator-(GThriftyPtr Pointer, diffint Offset)
		requires CNotVoid<ObjType>
		{
			return GThriftyPtr{ Pointer } -= Offset;
		}
		friend GThriftyPtr operator-(diffint Offset, GThriftyPtr Pointer)
		requires CNotVoid<ObjType>
		{
			return GThriftyPtr{ Pointer } -= Offset;
		}

		PointerType operator->() const
		requires CClass<ObjType> || CUnion<ObjType>
		{
			return GetPointer();
		}

		GThriftyPtr& operator=(PointerType NewPointer)
		{
			SetPointer(NewPointer);
			return *this;
		}
		operator PointerType() const
		{
			return GetPointer();
		}

		bool operator==(GThriftyPtr const& Other) const
		{
			return GetPointer() == Other.GetPointer();
		}
		strong_ordering operator<=>(GThriftyPtr const& Other) const
		{
			return GetPointer() <=> Other.GetPointer();
		}

		static constexpr szint DataBitCount()
		{
			return DataBitCount_Internal;
		}

	private:
		StorageType Storage;

		static constexpr szint DataBitCount_Internal = DataBitCountValue;/*AeonMath::LogPowerOfTwo(szint(ObjAlignValue)) - 1;*/
		static constexpr szint PointerBitCount_Internal = Aeon::BitSizeOf(StorageType) - DataBitCount_Internal;

		static constexpr StorageType DataMask = AeonMath::MakeBitMask<StorageType, DataBitCount_Internal>();
		static constexpr StorageType PointerMask = AeonMath::MakeBitMask<StorageType, PointerBitCount_Internal, DataBitCount_Internal>();
	};
}

#endif