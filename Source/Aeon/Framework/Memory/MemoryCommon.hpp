#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_MEMORY_MEMORYCOMMON
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_MEMORY_MEMORYCOMMON

#include "Aeon/Framework/Core.hpp"

namespace Aeon
{
	namespace ZzInternal
	{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  TConstLvalRefElseVoid, TLvalRefElseVoid

		template <class ObjType>
		struct ZzInternal_TConstLvalRefElseVoid
		{
			using Type = TAddLvalueReference<TAddConst<ObjType>>;
		};
		template <>
		struct ZzInternal_TConstLvalRefElseVoid<void>
		{
			using Type = void;
		};

		template <class ObjType>
		using TConstLvalRefElseVoid = typename ZzInternal_TConstLvalRefElseVoid<ObjType>::Type;

		template <class ObjType>
		struct ZzInternal_TLvalRefElseVoid
		{
			using Type = TAddLvalueReference<ObjType>;
		};
		template <>
		struct ZzInternal_TLvalRefElseVoid<void>
		{
			using Type = void;
		};

		template <class ObjType>
		using TLvalRefElseVoid = typename ZzInternal_TLvalRefElseVoid<ObjType>::Type;
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  CManagedPointer

	template <class ObjType>
	class IManagedPointer;

	template <class Type>
	concept CManagedPointer = requires
	{
		// Has 'ObjectType' alias designating an object type.
		typename Type::ObjectType;
		requires CObjectElseVoid<typename Type::ObjectType>;

		// Has 'SubObjectType' alias designating an non-array-object type
		typename Type::SubObjectType;
		requires CScalarObjectElseVoid<typename Type::SubObjectType>;

		// 'TRemoveArrayDimension<ObjectType>' is the same as 'SubObjectType'
		requires CSameClass<TRemoveArrayDimension<typename Type::ObjectType>, typename Type::SubObjectType>;

		// Has base class 'IManagedPointer<ObjType>' with 'typename Type::ObjectType' as the 'ObjType'
		requires CBaseClass<IManagedPointer<typename Type::ObjectType>, Type>;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  CDereferenceableManagedPointer

	template <class Type>
	concept CDereferenceableManagedPointer = requires
	{
		requires CManagedPointer<Type>;

		requires requires (Type const Object) { { *Object } -> CSameClass<typename Type::SubObjectType const&>; };
		requires requires (Type Object) { { *Object } -> CSameClass<typename Type::SubObjectType&>; };

		requires CContextuallyConvertibleClass<Type const, bool>;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  IManagedPointer

	template <class ObjType>
	class IManagedPointer
	{
	protected:
		IManagedPointer() = default;
		~IManagedPointer() = default;

	public:
		using ObjectType = ObjType;
		using SubObjectType = TRemoveArrayDimension<ObjectType>;

		using ConstReferenceType = ZzInternal::TConstLvalRefElseVoid<SubObjectType>;
		using ReferenceType = ZzInternal::TLvalRefElseVoid<SubObjectType>;

		using ConstPointerType = TAddPointer<TAddConst<SubObjectType>>;
		using PointerType = TAddPointer<SubObjectType>;

#if 0
		virtual ConstPointerType Get() const = 0;
		{
			static_assert(ZzInternal::CDeclaredPointerGetter<SubPtrType>);
			return (*static_cast<SubPtrType const*>(this)).Get();
		}
		virtual PointerType Get() = 0;
		{
			static_assert(ZzInternal::CDeclaredPointerGetter<SubPtrType>);
			return (*static_cast<SubPtrType*>(this)).Get();
		}

		virtual ConstReferenceType operator*() const = 0;
		{
			static_assert(ZzInternal::COverloadedIndirectionOperator<SubPtrType>);
			return (*static_cast<SubPtrType const*>(this)).operator*();
		}
		virtual ReferenceType operator*() = 0;
		{
			static_assert(ZzInternal::COverloadedIndirectionOperator<SubPtrType>);
			return (*static_cast<SubPtrType*>(this)).operator*();
		}

		virtual explicit operator bool() const = 0;
		{
			static_assert(ZzInternal::COverloadedBooleanOperator<SubPtrType>);
			return (*static_cast<SubPtrType const*>(this)).operator bool();
		}
#endif

	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  CManagedDeleter, CManagedDeleterForObject

	template <class Type>
	concept CManagedDeleter = requires
	{
		// Has ObjectType alias to object type
		typename Type::ObjectType;
		requires CObject<typename Type::ObjectType>;

		// Has SubObjectType alias to non-array-object type
		typename Type::SubObjectType;
		requires CScalarObject<typename Type::SubObjectType>;

		requires CSameClass<TRemoveArrayDimension<typename Type::ObjectType>, typename Type::SubObjectType>;

		// Default-constructible
		requires CDefaultConstructible<Type>;
		requires CCopyable<Type>;
		requires CMoveable<Type>;

		// Copy-constructible
	//	requires requires (Type const& Deleter) { { Type{ Deleter } } -> CSameClass<Type>; };
		// Move-constructible
	//	requires requires (Type&& Deleter) { { Type{ Move(Deleter) } } -> CSameClass<Type>; };

		// Copy-assignable
	//	requires requires (Type Deleter, Type const& OtherDeleter) { { Deleter = OtherDeleter } -> CSameClass<Type&>; };
		// Move-assignable
	//	requires requires (Type Deleter, Type&& OtherDeleter) { { Deleter = Move(OtherDeleter) } -> CSameClass<Type&>; };

		// Has Delete function that takes a pointer to SubObjectType
		requires requires (Type const Deleter, typename Type::SubObjectType* ObjectPtr) { { Deleter.Delete(ObjectPtr) } -> CVoid; };
	};

	template <class Type, class ObjectType>
	concept CManagedDeleterForObject = requires
	{
		requires CManagedDeleter<Type>;

		requires CSameClass<typename Type::ObjectType, ObjectType>;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GDefaultDeleter

	template <CObject ObjType>
	class GDefaultDeleter
	{
	public:
		using ObjectType = ObjType;
		using SubObjectType = TRemoveArrayDimension<ObjectType>;

		GDefaultDeleter() = default;

		void Delete(SubObjectType* ObjectPtr) const
		{
			Aeon::Delete<ObjectType>(ObjectPtr);
		}
	};

	// Instantiation with a bounded array is disallowed
	template <CObject ObjType, szint ArrSizeValue>
	class GDefaultDeleter<ObjType[ArrSizeValue]>;

	namespace ZzInternal
	{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GUnmanagedPointerWrapper

		template <CObject ObjectType, class DeleterType>
		requires CManagedDeleter<TDecayType<DeleterType>>
		struct GUnmanagedPointerWrapper
		{
			explicit GUnmanagedPointerWrapper(TRemoveArrayDimension<ObjectType>* NewObjectPtr, DeleterType&& NewDeleterRef)
				: ObjectPtr(NewObjectPtr)
				, DeleterRef(static_cast<DeleterType&&>(NewDeleterRef))
			{
			}

			AEON_MAKE_NON_COPYABLE(GUnmanagedPointerWrapper);
			AEON_MAKE_NON_MOVEABLE(GUnmanagedPointerWrapper);

			TRemoveArrayDimension<ObjectType>* ObjectPtr;
			DeleterType&& DeleterRef;
		};
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  MakeManagedPtr

	// Wraps an unmanaged pointer to some memory with a special type,
	// and enables initialization of UniquePtr and SharedPtr with the result of this function.
	template <CObject ObjectType, class DeleterType = GDefaultDeleter<ObjectType>>
	AEON_SPEC(NODISCARD, FORCEINLINE)
	auto MakeManagedPtr(TRemoveArrayDimension<ObjectType>* NewObjectPtr, DeleterType&& NewDeleterRef = TDecayType<DeleterType>{})
		-> ZzInternal::GUnmanagedPointerWrapper<ObjectType, DeleterType>
	{
		return ZzInternal::GUnmanagedPointerWrapper<ObjectType, DeleterType>{ NewObjectPtr, static_cast<DeleterType&&>(NewDeleterRef) };
	}

	template <CBoundedArray ObjectType, class DeleterType = GDefaultDeleter<ObjectType>>
	auto MakeManagedPtr(TRemoveArrayDimension<ObjectType>* NewObjectPtr, DeleterType&& NewDeleterRef = TDecayType<DeleterType>{}) = delete;
}

#endif