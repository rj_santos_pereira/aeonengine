#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_MEMORY_SHAREDPTR
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_MEMORY_SHAREDPTR

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Functional/Optional.hpp"
#include "Aeon/Framework/Memory/MemoryCommon.hpp"
#include "Aeon/Framework/Memory/StorageController.hpp"

#define ZZINTERNAL_AEON_SHARED_STORAGE_CONTROLLER_LOG_REF_COUNTERS 0

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  ESharedAccessMode

	enum struct ESharedAccessMode
	{
		// This is the default access mode for SharedPtr/WeakSharedPtr,
		// which uses an implementation that is thread-safe.
		MultiThreaded,
		// This is an alternative access mode for SharedPtr/WeakSharedPtr,
		// which uses an implementation that is NOT thread-safe;
		// use this when performance is critical
		// and the object is owned only by a single thread.
		SingleThreaded,
	};

	namespace ZzInternal
	{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GSharedStorageController

		template <ESharedAccessMode>
		class GSharedStorageController : public ZzInternal::IStorageController
		{
		protected:
			explicit GSharedStorageController()
				: StrongRefCounter(1)
				, WeakRefCounter(1)
			{
#			if ZZINTERNAL_AEON_SHARED_STORAGE_CONTROLLER_LOG_REF_COUNTERS
				AEON_DEBUGGER_FORMAT_PRINT(Info, "GSharedStorageController[{:X}]: ++StrongRefs (1)", bit_cast<ptrint>(this));
				AEON_DEBUGGER_FORMAT_PRINT(Info, "GSharedStorageController[{:X}]: ++WeakRefs (1)", bit_cast<ptrint>(this));
#			endif
			}
			~GSharedStorageController() = default;

		public:
			// If any of these methods assert, someone is misusing SharedPtr:
			// multiple threads must not access the same SharedPtr object through non-const methods;
			// they may access separate copies of SharedPtr pointing to the same StorageController,
			// and thus the same underlying object, through any method
			// (note that synchronization of access to the object itself is a separate issue).

			void NewStrongRef()
			{
				uint32 OldRefCount = StrongRefCounter.FetchIncrement<EMemoryOrder::Relaxed>();
				AEON_ASSERT(OldRefCount);
#			if ZZINTERNAL_AEON_SHARED_STORAGE_CONTROLLER_LOG_REF_COUNTERS
				uint32 NewRefCount = OldRefCount + 1;
				AEON_DEBUGGER_FORMAT_PRINT(Info, "GSharedStorageController[{:X}]: ++StrongRefs ({})", bit_cast<ptrint>(this), NewRefCount);
#			endif
				NewWeakRef();
			}
			void NewWeakRef()
			{
				uint32 OldRefCount = WeakRefCounter.FetchIncrement<EMemoryOrder::Relaxed>();
				AEON_ASSERT(OldRefCount);
#			if ZZINTERNAL_AEON_SHARED_STORAGE_CONTROLLER_LOG_REF_COUNTERS
				uint32 NewRefCount = OldRefCount + 1;
				AEON_DEBUGGER_FORMAT_PRINT(Info, "GSharedStorageController[{:X}]: ++WeakRefs ({})", bit_cast<ptrint>(this), NewRefCount);
#			endif
			}

			bool TryAcquireStrongRef()
			{
				uint32 OldRefCount = 1;
				while (!StrongRefCounter.CompareExchangeIncrement<EMemoryOrder::Relaxed>(OldRefCount))
				{
					if (OldRefCount == 0)
					{
						break;
					}
				}
				bool HasAcquired = OldRefCount != 0;
#			if ZZINTERNAL_AEON_SHARED_STORAGE_CONTROLLER_LOG_REF_COUNTERS
				uint32 NewRefCount = OldRefCount + uint32(HasAcquired);
				AEON_DEBUGGER_FORMAT_PRINT(Info, "GSharedStorageController[{:X}]: ++StrongRefs ({})", bit_cast<ptrint>(this), NewRefCount);
#			endif
				if (HasAcquired)
				{
					// Note that GWeakSharedPtr::MakeShared does not mutate the WeakSharedPtr object,
					// so we also need to increment WeakRef here to maintain the invariant [SharedStorageController.RefCount.Invariant].
					NewWeakRef();
				}
				return HasAcquired;
			}
#		if 0
			bool TryReleaseWeakRef()
			{
				uint32 StrongRefCount = StrongRefCounter.Decrement();
				// Note that GSharedPtr::ToUnmanaged always mutates (resets) the SharedPtr object,
				// so we should also decrement WeakRef here to avoid extra logic in SharedPtr.
				bool HasReleased = StrongRefCount == 0;
				if (HasReleased)
				{
					uint32 WeakRefCount = 1;
					if (WeakRefCounter.CompareExchangeDecrement(WeakRefCount))
					{
						DeleteThis();
					}
					else
					{
						StrongRefCounter.FetchIncrement<EMemoryOrder::Relaxed>();
					}
				}
				else
				{
					DeleteWeakRef();
				}
				return HasReleased;
			}
#		endif

			void DeleteStrongRef()
			{
				uint32 RefCount = StrongRefCounter.Decrement();
#			if ZZINTERNAL_AEON_SHARED_STORAGE_CONTROLLER_LOG_REF_COUNTERS
				AEON_DEBUGGER_FORMAT_PRINT(Info, "GSharedStorageController[{:X}]: --StrongRefs ({})", bit_cast<ptrint>(this), RefCount);
#			endif
				if (RefCount == 0)
				{
					DeleteObject();
				}
				DeleteWeakRef();
			}
			void DeleteWeakRef()
			{
				uint32 RefCount = WeakRefCounter.Decrement();
#			if ZZINTERNAL_AEON_SHARED_STORAGE_CONTROLLER_LOG_REF_COUNTERS
				AEON_DEBUGGER_FORMAT_PRINT(Info, "GSharedStorageController[{:X}]: --WeakRefs ({})", bit_cast<ptrint>(this), RefCount);
#			endif
				if (RefCount == 0)
				{
					DeleteThis();
				}
			}

			uint32 GetStrongRefCount() const
			{
				return StrongRefCounter.Load<EMemoryOrder::Relaxed>();
			}
			uint32 GetWeakRefCount() const
			{
				return WeakRefCounter.Load<EMemoryOrder::Relaxed>();
			}

		protected:
			// Make these methods protected, so that GSharedPtr (and GWeakSharedPtr) cannot mistakenly access them
			virtual void DeleteObject() override = 0;
			virtual void DeleteThis() override = 0;

		private:
			// [SharedStorageController.RefCount.Invariant]: StrongRefCounter <= WeakRefCounter
			GAtomicUint32<EMemoryOrder::AcqRel, EMemoryOrder::Relaxed> StrongRefCounter;
			GAtomicUint32<EMemoryOrder::AcqRel, EMemoryOrder::Relaxed> WeakRefCounter;
		};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GSharedStorageController<SingleThreaded>

		template <>
		class GSharedStorageController<ESharedAccessMode::SingleThreaded> : public ZzInternal::IStorageController
		{
		protected:
			explicit GSharedStorageController()
				: StrongRefCounter(1)
				, WeakRefCounter(1)
			{
			}
			~GSharedStorageController() = default;

		public:
			void NewStrongRef()
			{
				++StrongRefCounter;
				NewWeakRef();
			}
			void NewWeakRef()
			{
				++WeakRefCounter;
			}

			bool TryAcquireStrongRef()
			{
				if (StrongRefCounter != 0)
				{
					NewStrongRef();
				}
				return StrongRefCounter != 0;
			}
			bool TryReleaseWeakRef()
			{
				--StrongRefCounter;
				DeleteWeakRef();
				return StrongRefCounter == 0;
			}

			void DeleteStrongRef()
			{
				if (--StrongRefCounter == 0)
				{
					DeleteObject();
				}
				DeleteWeakRef();
			}
			void DeleteWeakRef()
			{
				if (--WeakRefCounter == 0)
				{
					DeleteThis();
				}
			}

			uint32 GetStrongRefCount() const
			{
				return StrongRefCounter;
			}
			uint32 GetWeakRefCount() const
			{
				return WeakRefCounter;
			}

		protected:
			// Make these methods protected, so that GSharedPtr (and GWeakSharedPtr) cannot mistakenly access them
			virtual void DeleteObject() = 0;
			virtual void DeleteThis() = 0;

		private:
			// [SharedStorageController.RefCount.Invariant]: StrongRefCounter <= WeakRefCounter
			uint32 StrongRefCounter;
			uint32 WeakRefCounter;
		};
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GSharedPtr

	template <CObjectElseVoid ObjType, ESharedAccessMode AccModeValue = ESharedAccessMode::MultiThreaded>
	class GSharedPtr : public IManagedPointer<ObjType>
	{
		using Base = IManagedPointer<ObjType>;

		using BaseStorageType = ZzInternal::GSharedStorageController<AccModeValue>;

	//	static constexpr ESharedAccessMode AccessMode = AccModeValue;

		AEON_DECLARE_TAG_CLASS(ConstructWithCtorArgs);
		AEON_DECLARE_TAG_CLASS(ConstructWithInitList);
		AEON_DECLARE_TAG_CLASS(ConstructWithData);

		static_assert(!CManagedPointer<ObjType>,
					  "'GSharedPtr<ObjType>': ObjType must not derive from IManagedPointer.");

	public:
		using ObjectType = Base::ObjectType;
		using SubObjectType = Base::SubObjectType;

		using ConstReferenceType = Base::ConstReferenceType;
		using ReferenceType = Base::ReferenceType;

		using ConstPointerType = Base::ConstPointerType;
		using PointerType = Base::PointerType;

		GSharedPtr()
			: ObjectPtr(nullptr)
			, StoragePtr(nullptr)
		{
		}
		GSharedPtr(nullptr_type)
			: GSharedPtr()
		{
		}

		// GSharedPtr::Downcast/GWeakSharedPtr::MakeShared constructor
		explicit GSharedPtr(ConstructWithData, SubObjectType* NewObjectPtr, BaseStorageType* NewStoragePtr)
			: ObjectPtr(NewObjectPtr)
			, StoragePtr(NewStoragePtr)
		{
		}

		template <CObjectElseVoid NewObjectType, class NewDeleterType = GDefaultDeleter<TPropagateArrayDimension<ObjectType, NewObjectType>>>
		explicit GSharedPtr(NewObjectType* NewObjectPtr, NewDeleterType&& NewDeleter = TDecayType<NewDeleterType>{})
		requires (CArray<ObjectType> ? CSameClass<NewObjectType, SubObjectType> : CConvertibleClass<NewObjectType*, SubObjectType*>)
				 // NOTE Since we have type-erasure on the managed ptr, we enforce the actual object type on the deleter
				 && CManagedDeleterForObject<TDecayType<NewDeleterType>, TPropagateArrayDimension<ObjectType, NewObjectType>>
			: ObjectPtr(NewObjectPtr)
			, StoragePtr(nullptr)
		{
			using StorageType = ZzInternal::GMultiBlockStorageController<BaseStorageType, NewObjectType, TDecayType<NewDeleterType>>;

			if (NewObjectPtr)
			{
				StoragePtr = StorageType::New(NewObjectPtr, Forward<NewDeleterType>(NewDeleter));
			}
		}

		// MakeSharedPtr constructors
		template <class ... ArgTypes>
		explicit GSharedPtr(ConstructWithCtorArgs, ArgTypes&&... NewInitArgs)
		{
			using StorageType = ZzInternal::GSingleBlockStorageController<BaseStorageType, ObjectType>;

			auto [NewObjectPtr, NewStoragePtr] = StorageType::New(Forward<ArgTypes>(NewInitArgs) ...);

			ObjectPtr = NewObjectPtr;
			StoragePtr = NewStoragePtr;
		}
		template <class NewObjectType, szint ObjectCountValue>
		explicit GSharedPtr(ConstructWithInitList, InitList<NewObjectType> NewInitList, TObject<ObjectCountValue>)
		{
			using StorageType = ZzInternal::GSingleBlockStorageController<BaseStorageType, ObjectType>;

			auto [NewObjectPtr, NewStoragePtr] = StorageType::template New<NewObjectType, ObjectCountValue>(NewInitList);

			ObjectPtr = NewObjectPtr;
			StoragePtr = NewStoragePtr;
		}

		// MakeManagedPtr constructor
		template <class NewObjectType, class NewDeleterType>
		GSharedPtr(ZzInternal::GUnmanagedPointerWrapper<NewObjectType, NewDeleterType>&& UnmanagedPtr)
			: GSharedPtr(UnmanagedPtr.ObjectPtr, static_cast<NewDeleterType&&>(UnmanagedPtr.DeleterRef))
		{
		}

		// NOTE These are necessary, copy/move constructors cannot be templates,
		//  so even if the template versions are appropriate, e.g., when ObjectType == OtherObjectType,
		//  the compiler may prefer the implicitly declared constructors, which would have the wrong semantics.

		// Copy/Move constructors
		GSharedPtr(GSharedPtr const& Other)
			: ObjectPtr(Other.ObjectPtr)
			, StoragePtr(Other.StoragePtr)
		{
			if (StoragePtr)
			{
				StoragePtr->NewStrongRef();
			}
		}
		GSharedPtr(GSharedPtr&& Other)
			: ObjectPtr(Exchange(Other.ObjectPtr, nullptr))
			, StoragePtr(Exchange(Other.StoragePtr, nullptr))
		{
		}

		// Copy/Move constructors from convertible object type (e.g. from derived other to base this)
		template <CObjectElseVoid OtherObjectType>
		GSharedPtr(GSharedPtr<OtherObjectType, AccModeValue> const& Other)
		requires CConvertibleClass<OtherObjectType*, ObjectType*>
			: ObjectPtr(Other.ObjectPtr)
			, StoragePtr(Other.StoragePtr)
		{
			if (StoragePtr)
			{
				StoragePtr->NewStrongRef();
			}
		}
		template <CObjectElseVoid OtherObjectType>
		GSharedPtr(GSharedPtr<OtherObjectType, AccModeValue>&& Other)
		requires CConvertibleClass<OtherObjectType*, ObjectType*>
			: ObjectPtr(Exchange(Other.ObjectPtr, nullptr))
			, StoragePtr(Exchange(Other.StoragePtr, nullptr))
		{
		}

		~GSharedPtr()
		{
			Reset();
		}

		GSharedPtr& operator=(GSharedPtr Other)
		{
			using Aeon::Swap;
			Swap(*this, Other);

			return *this;
		}

		ConstPointerType Get() const
		{
			return ObjectPtr;
		}
		PointerType Get()
		{
			return AEON_INVOKE_CONST_OVERLOAD(Get);
		}

		template <class DeleterType>
		GOptional<DeleterType> GetDeleter() const
		{
			if (StoragePtr)
			{
				if (auto DeleterPtr = StoragePtr->CheckGetDeleter(typeid(DeleterType)))
				{
					return GOptional<DeleterType>{ TConstructInPlace::Tag, *static_cast<DeleterType const*>(DeleterPtr) };
				}
			}
			return GOptional<DeleterType>{};
		}

		void Reset()
		{
			if (StoragePtr)
			{
				StoragePtr->DeleteStrongRef();

				ObjectPtr = nullptr;
				StoragePtr = nullptr;
			}
		}

#	if 0
		AEON_NODISCARD SubObjectType* ToUnmanaged()
		{
			// NOTE Similar to Reset, however, instead of deleting the object if this is the last (strong) reference to it,
			//  simply returns the pointer to the object, and defers deleting it to the user.
			//  If this is not the last reference to the object, this still resets, but returns nullptr.

			// FIXME This breaks with GWeakSharedPtr, as any existence of a weak-ptr needs to keep the controller alive
			if (StoragePtr)
			{
				SubObjectType* OldObjectPtr = Exchange(ObjectPtr, nullptr);
				BaseStorageType* OldStoragePtr = Exchange(StoragePtr, nullptr);

				return OldStoragePtr->TryReleaseWeakRef() ? OldObjectPtr : nullptr;
			}
			return nullptr;
		}
#	endif

		// Downcast checks if stored object is of type ActualObjectType,
		// and requires NewObjectType to be an unambiguous public base class of ActualObjectType (or ActualObjectType itself),
		// similarly to dynamic_cast
		template <CObject NewObjectType, CObject ActualObjectType = NewObjectType>
		AEON_NODISCARD GSharedPtr<NewObjectType, AccModeValue> Downcast()
		requires CConvertibleClass<NewObjectType*, ObjectType*>
				 && CConvertibleClass<ActualObjectType*, NewObjectType*>
		{
			using SharedPtrType = GSharedPtr<NewObjectType, AccModeValue>;
			if (StoragePtr && StoragePtr->CheckObjectType(typeid(ActualObjectType)))
			{
				auto NewObjectPtr = static_cast<TAddPointer<TRemoveArrayDimension<NewObjectType>>>(Exchange(ObjectPtr, nullptr));
				auto NewStoragePtr = Exchange(StoragePtr, nullptr);

				return SharedPtrType{ SharedPtrType::ConstructWithData::Tag, NewObjectPtr, NewStoragePtr };
			}
			return SharedPtrType{};
		}

#	if not AEON_BUILD_DEVELOPMENT
		[[deprecated("This method is provided for debugging purposes only, code that uses this method should be omitted from production builds.")]]
#	endif
		uint32 GetRefCount_DEBUG() const
		{
			return StoragePtr ? StoragePtr->GetStrongRefCount() : 0;
		}
#	if not AEON_BUILD_DEVELOPMENT
		[[deprecated("This method is provided for debugging purposes only, code that uses this method should be omitted from production builds.")]]
#	endif
		uint32 GetWeakRefCount_DEBUG() const
		{
			return StoragePtr ? StoragePtr->GetWeakRefCount() : 0;
		}

		ConstReferenceType operator*() const
		requires CObject<ObjectType>
		{
			AEON_ASSERT(ObjectPtr);
			return *ObjectPtr;
		}
		ReferenceType operator*()
		requires CObject<ObjectType>
		{
			return AEON_INVOKE_CONST_OVERLOAD(operator*);
		}

		ConstReferenceType operator[](szint Index) const
		requires CArray<ObjectType>
		{
			AEON_ASSERT(ObjectPtr);
			return *(ObjectPtr + Index);
		}
		ReferenceType operator[](szint Index)
		requires CArray<ObjectType>
		{
			return AEON_INVOKE_CONST_OVERLOAD(operator[], Index);
		}

		ConstPointerType operator->() const
		requires CClass<ObjectType> || CUnion<ObjectType>
		{
			AEON_ASSERT(ObjectPtr);
			return ObjectPtr;
		}
		PointerType operator->()
		requires CClass<ObjectType> || CUnion<ObjectType>
		{
			return AEON_INVOKE_CONST_OVERLOAD(operator->);
		}

		explicit operator bool() const
		{
			return StoragePtr != nullptr;
		}

		bool operator==(GSharedPtr const& Other) const
		{
			return ObjectPtr == Other.ObjectPtr;
		}
		strong_ordering operator<=>(GSharedPtr const& Other) const
		{
			return ObjectPtr <=> Other.ObjectPtr;
		}

		friend void Swap(GSharedPtr& SharedPtr1, GSharedPtr& SharedPtr2)
		{
			using Aeon::Swap;

			Swap(SharedPtr1.ObjectPtr, SharedPtr2.ObjectPtr);
			Swap(SharedPtr1.StoragePtr, SharedPtr2.StoragePtr);
		}

	private:
		SubObjectType* ObjectPtr;
		BaseStorageType* StoragePtr;

		template <class OtherObjectType, ESharedAccessMode OtherAccessModeValue, class ... ArgTypes>
		friend auto ZzInternal_MakeSharedPtr(ArgTypes&& ... Args)
			-> GSharedPtr<OtherObjectType, OtherAccessModeValue>;

		template <class OtherObjectType, ESharedAccessMode OtherAccessModeValue>
		friend auto ZzInternal_MakeSharedPtr(InitList<TRemoveArrayDimension<OtherObjectType>> InitList)
			-> GSharedPtr<TRemoveArrayDimensionSize<OtherObjectType>, OtherAccessModeValue>;
		template <class OtherObjectType, ESharedAccessMode OtherAccessModeValue>
		friend auto ZzInternal_MakeSharedPtr(RvalRefInitList<TRemoveArrayDimension<OtherObjectType>> InitList)
			-> GSharedPtr<TRemoveArrayDimensionSize<OtherObjectType>, OtherAccessModeValue>;

		template <CObjectElseVoid, ESharedAccessMode>
		friend class GSharedPtr;

		template <CObjectElseVoid, ESharedAccessMode>
		friend class GWeakSharedPtr;
	};

	template <CObject ObjectType, szint ArraySizeValue, ESharedAccessMode AccessModeValue>
	class GSharedPtr<ObjectType[ArraySizeValue], AccessModeValue>; // Disallow bounded array object types

	// CTAD deduction guides

	template <CObject ObjectType, class DeleterType>
	GSharedPtr(ZzInternal::GUnmanagedPointerWrapper<ObjectType, DeleterType>&&) -> GSharedPtr<ObjectType>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  MakeSharedPtr

	template <class ObjectType, ESharedAccessMode AccessModeValue, class ... ArgTypes>
	AEON_SPEC(FORCEINLINE)
	auto ZzInternal_MakeSharedPtr(ArgTypes&& ... Args)
		-> GSharedPtr<ObjectType, AccessModeValue>
	{
		using SharedPtrType = GSharedPtr<ObjectType, AccessModeValue>;
		return SharedPtrType(SharedPtrType::ConstructWithCtorArgs::Tag, Forward<ArgTypes>(Args) ...);
	}

	template <class ObjectType, ESharedAccessMode AccessModeValue>
	AEON_SPEC(FORCEINLINE)
	auto ZzInternal_MakeSharedPtr(InitList<TRemoveArrayDimension<ObjectType>> InitList)
		-> GSharedPtr<TRemoveArrayDimensionSize<ObjectType>, AccessModeValue>
	{
		using SharedPtrType = GSharedPtr<TRemoveArrayDimensionSize<ObjectType>, AccessModeValue>;
		return SharedPtrType(SharedPtrType::ConstructWithInitList::Tag, InitList, TArrayDimensionSize<ObjectType>{});
	}
	template <class ObjectType, ESharedAccessMode AccessModeValue>
	AEON_SPEC(FORCEINLINE)
	auto ZzInternal_MakeSharedPtr(RvalRefInitList<TRemoveArrayDimension<ObjectType>> InitList)
		-> GSharedPtr<TRemoveArrayDimensionSize<ObjectType>, AccessModeValue>
	{
		using SharedPtrType = GSharedPtr<TRemoveArrayDimensionSize<ObjectType>, AccessModeValue>;
		return SharedPtrType(SharedPtrType::ConstructWithInitList::Tag, InitList, TArrayDimensionSize<ObjectType>{});
	}

	// HACK IDE complains when constraining the ObjectType on friend functions,
	//  as it doesn't correctly determine that the functions are declared friends,
	//  so we constrain these instead.

	template <CScalarObject ObjectType, ESharedAccessMode AccessModeValue = ESharedAccessMode::MultiThreaded, class ... ArgTypes>
	AEON_SPEC(NODISCARD, FORCEINLINE)
	auto MakeSharedPtr(ArgTypes&& ... Args)
		-> GSharedPtr<ObjectType, AccessModeValue>
	{
		return ZzInternal_MakeSharedPtr<ObjectType, AccessModeValue>(Forward<ArgTypes>(Args) ...);
	}

	template <CBoundedArray ObjectType, ESharedAccessMode AccessModeValue = ESharedAccessMode::MultiThreaded>
	AEON_SPEC(NODISCARD, FORCEINLINE)
	auto MakeSharedPtr(InitList<TRemoveArrayDimension<ObjectType>> InitList = {})
		-> GSharedPtr<TRemoveArrayDimensionSize<ObjectType>, AccessModeValue>
	requires CCopyable<TRemoveArrayDimension<ObjectType>>
	{
		return ZzInternal_MakeSharedPtr<ObjectType, AccessModeValue>(InitList);
	}
	template <CBoundedArray ObjectType, ESharedAccessMode AccessModeValue = ESharedAccessMode::MultiThreaded>
	AEON_SPEC(NODISCARD, FORCEINLINE)
	auto MakeSharedPtr(RvalRefInitList<TRemoveArrayDimension<ObjectType>> InitList = {})
		-> GSharedPtr<TRemoveArrayDimensionSize<ObjectType>, AccessModeValue>
	requires CMoveable<TRemoveArrayDimension<ObjectType>> && CNotCopyable<TRemoveArrayDimension<ObjectType>>
	{
		return ZzInternal_MakeSharedPtr<ObjectType, AccessModeValue>(InitList);
	}
	template <CBoundedArray ObjectType, ESharedAccessMode AccessModeValue = ESharedAccessMode::MultiThreaded>
	AEON_SPEC(NODISCARD, FORCEINLINE)
	auto MakeSharedPtr(RvalRefInitList<TRemoveArrayDimension<ObjectType>> InitList)
		-> GSharedPtr<TRemoveArrayDimensionSize<ObjectType>, AccessModeValue>
	requires CMoveable<TRemoveArrayDimension<ObjectType>>
	{
		return ZzInternal_MakeSharedPtr<ObjectType, AccessModeValue>(InitList);
	}
}

#endif