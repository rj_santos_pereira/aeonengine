#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_MEMORY_WEAKSHAREDPTR
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_MEMORY_WEAKSHAREDPTR

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Memory/MemoryCommon.hpp"
#include "Aeon/Framework/Memory/SharedPtr.hpp"

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GWeakSharedPtr

	template <CObjectElseVoid ObjType, ESharedAccessMode AccModeValue = ESharedAccessMode::MultiThreaded>
	class GWeakSharedPtr : public IManagedPointer<ObjType>
	{
	//	using Base = IManagedPointer<ObjType>;

		using BaseStorageType = ZzInternal::GSharedStorageController<AccModeValue>;

		static_assert(!CManagedPointer<ObjType>);

	public: // FIXME for some reason, using the Base aliases here breaks something...
		using ObjectType = ObjType; //Base::ObjectType;
		using SubObjectType = TRemoveArrayDimension<ObjType>; //Base::SubObjectType;

		GWeakSharedPtr()
			: StoragePtr(nullptr)
		{
		}
		GWeakSharedPtr(nullptr_type)
			: GWeakSharedPtr()
		{
		}

		// GSharedPtr constructor
		explicit GWeakSharedPtr(GSharedPtr<ObjectType, AccModeValue> const& SharedPtr)
			: StoragePtr(SharedPtr.StoragePtr)
		{
			if (StoragePtr)
			{
				StoragePtr->NewWeakRef();
			}
		}

		// NOTE These are necessary, copy/move constructors cannot be templates,
		//  so even if the template versions are appropriate, e.g., when ObjectType == OtherObjectType,
		//  the compiler may prefer the implicitly declared constructors, which would have the wrong semantics.

		// Copy/Move constructors
		GWeakSharedPtr(GWeakSharedPtr const& Other)
			: StoragePtr(Other.StoragePtr)
		{
			if (StoragePtr)
			{
				StoragePtr->NewWeakRef();
			}
		}
		GWeakSharedPtr(GWeakSharedPtr&& Other)
			: StoragePtr(Exchange(Other.StoragePtr, nullptr))
		{
		}

		// Copy/Move constructors from convertible object type
		template <CObjectElseVoid OtherObjectType>
		GWeakSharedPtr(GWeakSharedPtr<OtherObjectType, AccModeValue> const& Other)
		requires CConvertibleClass<OtherObjectType*, ObjectType*>
			: StoragePtr(Other.StoragePtr)
		{
			if (StoragePtr)
			{
				StoragePtr->NewWeakRef();
			}
		}
		template <CObjectElseVoid OtherObjectType>
		GWeakSharedPtr(GWeakSharedPtr<OtherObjectType, AccModeValue>&& Other)
		requires CConvertibleClass<OtherObjectType*, ObjectType*>
			: StoragePtr(Exchange(Other.StoragePtr, nullptr))
		{
		}

		~GWeakSharedPtr()
		{
			Reset();
		}

		GWeakSharedPtr& operator=(GWeakSharedPtr Other)
		{
			using Aeon::Swap;
			Swap(*this, Other);

			return *this;
		}

#if 0
		// BEGIN IManagedPointer interface
		[[deprecated("This function is defined for compliance with the interface and for internal use only.")]]
		SubObjectType const* Get() const
		{
			return StoragePtr ? static_cast<SubObjectType const*>(StoragePtr->GetObjectPtr()) : nullptr;
		}
		[[deprecated("This function is defined for compliance with the interface and for internal use only.")]]
		SubObjectType* Get()
		{
#		pragma warning(suppress: 4996)
			return AEON_INVOKE_CONST_OVERLOAD(Get);
		}
		// END IManagedPointer interface
#endif

		AEON_NODISCARD GSharedPtr<ObjectType, AccModeValue> MakeShared() const
		{
			using SharedPtrType = GSharedPtr<ObjectType, AccModeValue>;
			if (StoragePtr && StoragePtr->TryAcquireStrongRef())
			{
				auto NewObjectPtr = static_cast<SubObjectType*>(StoragePtr->GetObjectPtr());

				return SharedPtrType{ SharedPtrType::ConstructWithData::Tag, NewObjectPtr, StoragePtr };
			}
			return SharedPtrType{};
		}

		void Reset()
		{
			if (StoragePtr)
			{
				BaseStorageType* OldStoragePtr = Exchange(StoragePtr, nullptr);

				OldStoragePtr->DeleteWeakRef();
			}
		}
#	if not AEON_BUILD_DEVELOPMENT
		[[deprecated("This method is provided for debugging purposes only, code that uses this method should be omitted from production builds.")]]
#	endif
		uint32 GetRefCount_DEBUG() const
		{
			return StoragePtr ? StoragePtr->GetStrongRefCount() : 0;
		}
#	if not AEON_BUILD_DEVELOPMENT
		[[deprecated("This method is provided for debugging purposes only, code that uses this method should be omitted from production builds.")]]
#	endif
		uint32 GetWeakRefCount_DEBUG() const
		{
			return StoragePtr ? StoragePtr->GetWeakRefCount() : 0;
		}

		// This method evaluates whether this object has a StorageController from which a SharedPtr can be made, i.e.,
		//  if this returns false, MakeShared will always return an invalid SharedPtr.
		//  Note that if this returns true, it does not mean that MakeShared will return a valid SharedPtr,
		//  as the managed object may be destroyed between this call and MakeShared, e.g.,
		//  some thread holding onto the last SharedPtr (last strong reference) may delete the object.
		explicit operator bool() const
		{
			return StoragePtr != nullptr;
		}

		friend void Swap(GWeakSharedPtr& WeakPtr1, GWeakSharedPtr& WeakPtr2)
		{
			using Aeon::Swap;

			Swap(WeakPtr1.StoragePtr, WeakPtr2.StoragePtr);
		}

	private:
		BaseStorageType* StoragePtr;
	};
}

#endif