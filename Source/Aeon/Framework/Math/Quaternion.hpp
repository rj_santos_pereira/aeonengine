#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_MATH_QUATERNION
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_MATH_QUATERNION

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Math/Rotation.hpp"

#pragma pack(push, 4)

namespace Aeon
{
	template <CFloatingPoint FloatType>
	class GQuaternion : public ZzInternal::GVectorOps<GQuaternion<FloatType>, FloatType, 4>
	{
		using Base = ZzInternal::GVectorOps<GQuaternion<FloatType>, FloatType, 4>;

	public:
		using ScalarType = FloatType;
		static constexpr szint ElementCount = 4;

		GQuaternion() = default;
		constexpr GQuaternion(FloatType NewW, FloatType NewX, FloatType NewY, FloatType NewZ)
			: W{ NewW }
			, X{ NewX }
			, Y{ NewY }
			, Z{ NewZ }
		{
		}

	//	constexpr GQuaternion(GVector4<FloatType> const& NewVector)
	//		: W{ NewVector.W }
	//		, X{ NewVector.X }
	//		, Y{ NewVector.Y }
	//		, Z{ NewVector.Z }
	//	{
	//	}

		constexpr GQuaternion(FloatType RealPart, GVector3<FloatType> const& ImaginaryPart)
			: W{ RealPart }
			, X{ ImaginaryPart.X }
			, Y{ ImaginaryPart.Y }
			, Z{ ImaginaryPart.Z }
		{
		}

#	if 0
		static constexpr GQuaternion FromMatrix(GFloatMatrix<FloatType, 3, 3> RotMatrix)
		{
			
		}
#	endif

		// Overriding Base::operator*= overload with VectorType parameter
		constexpr GQuaternion& operator*=(GQuaternion const& Other)
		{
			FloatType NewW = W * Other.W - X * Other.X - Y * Other.Y - Z * Other.Z;
			FloatType NewX = W * Other.X + X * Other.W + Y * Other.Z - Z * Other.Y;
			FloatType NewY = W * Other.Y - X * Other.Z + Y * Other.W + Z * Other.X;
			FloatType NewZ = W * Other.Z + X * Other.Y - Y * Other.X + Z * Other.W;

			W = NewW;
			X = NewX;
			Y = NewY;
			Z = NewZ;

			return *this;
		}

		using Base::operator+=;
		using Base::operator*=;

		using Base::operator+;
		using Base::operator*;

		using Base::Normalize;
		using Base::NormalizeUnsafe;

		using Base::LengthSquared;
		using Base::Length;
		using Base::AsNormalized;

		using Base::IsApproxEqual;
		using Base::IsApproxZero;
		using Base::IsApproxUnit;

		constexpr GQuaternion Conjugate() const
		{
			return GQuaternion{ +W, -X, -Y, -Z };
		}

		constexpr bool IsApproxIdentity(FloatType Tolerance = TNumericProperties<FloatType>::Epsilon) const
		{
			return AeonMath::IsApproxEqual(W, FloatType(1.0), Tolerance)
				   && AeonMath::IsApproxZero(X, Tolerance)
				   && AeonMath::IsApproxZero(Y, Tolerance)
				   && AeonMath::IsApproxZero(Z, Tolerance);
		}

		using Base::operator[];

		constexpr GVector3<FloatType> RotateVector(GVector3<FloatType> const& Vector) const
		{
			// CHECKME Benchmark and ensure performance of cross product is better
#if 0
			// 18 adds, 21 muls
			FloatType XX = X * X;
			FloatType YY = Y * Y;
			FloatType ZZ = Z * Z;

			// Note that reciprocal requires only negating these W components
			FloatType WX = W * X;
			FloatType WY = W * Y;
			FloatType WZ = W * Z;

			FloatType XY = X * Y;
			FloatType YZ = Y * Z;
			FloatType ZX = Z * X;

			return Vector + FloatType(2.0) * GVector3<FloatType>{
				Vector.X * (YY + ZZ) + Vector.Y * (-WZ + XY) + Vector.Z * (WY + ZX),
				Vector.Y * (ZZ + XX) + Vector.Z * (-WX + YZ) + Vector.X * (WZ + XY),
				Vector.Z * (XX + YY) + Vector.X * (-WY + ZX) + Vector.Y * (WX + YZ),
			};
#else
			// 12 adds, 18 muls
			FloatType RealPart = W;
			GVector3<FloatType> ImaginaryPart{ X, Y, Z };

			GVector3<FloatType> HalfwayVector = FloatType(2.0) * GVector3<FloatType>::Cross(ImaginaryPart, Vector);
			return Vector + (RealPart * HalfwayVector) + GVector3<FloatType>::Cross(ImaginaryPart, HalfwayVector);
#endif
		}
		constexpr GVector3<FloatType> UnrotateVector(GVector3<FloatType> const& Vector) const
		{
			return Conjugate().RotateVector(Vector);
		}

		static constexpr GQuaternion RotationBetweenVectors(GVector3<FloatType> const& FromVector, GVector3<FloatType> const& ToVector)
		{
			// NOTE Implementing this in terms of RotationBetweenUnitVectors would require normalizing both vectors,
			// which would cost an extra sqrt (and possibly extra branching).
			// See: https://stackoverflow.com/a/11741520

			// CHECKME Also check https://stackoverflow.com/a/73103883 and test if that solution is better in some way

			FloatType InvMagnitude = AeonMath::RSqrt(FromVector.LengthSquared() * ToVector.LengthSquared());

			FloatType RealPart = GVector3<FloatType>::Dot(FromVector, ToVector) * InvMagnitude;
			GVector3<FloatType> ImaginaryPart;

			if (!AeonMath::IsApproxEqual(RealPart, FloatType(-1.0)))
			{
				ImaginaryPart = GVector3<FloatType>::Cross(FromVector, ToVector) * InvMagnitude;
			}
			else
			{
				GVector3<FloatType> HalfwayVector = FromVector.MakeApproxOrthonormalVector();

				ImaginaryPart = GVector3<FloatType>::Cross(FromVector, HalfwayVector);
			}

			RealPart += FloatType(1.0);

			return GQuaternion{ RealPart, ImaginaryPart }.NormalizeUnsafe();
		}
		static constexpr GQuaternion RotationBetweenUnitVectors(GVector3<FloatType> const& FromVector, GVector3<FloatType> const& ToVector)
		{
			FloatType RealPart = GVector3<FloatType>::Dot(FromVector, ToVector);
			GVector3<FloatType> ImaginaryPart;

			if (!AeonMath::IsApproxEqual(RealPart, FloatType(-1.0)))
			{
				ImaginaryPart = GVector3<FloatType>::Cross(FromVector, ToVector);
			}
			else
			{
				GVector3<FloatType> HalfwayVector = FromVector.MakeApproxOrthonormalVector();

				ImaginaryPart = GVector3<FloatType>::Cross(FromVector, HalfwayVector);
			}

			RealPart += FloatType(1.0);

			return GQuaternion{ RealPart, ImaginaryPart }.NormalizeUnsafe();
		}

		static constexpr GQuaternion FromEuler(GEulerRotation<FloatType> const& Euler)
		{
			FloatType RollCos = AeonMath::Cos(AeonMath::DegToRad(FloatType(0.5) * Euler.Roll));
			FloatType RollSin = AeonMath::Sin(AeonMath::DegToRad(FloatType(0.5) * Euler.Roll));

			FloatType PitchCos = AeonMath::Cos(AeonMath::DegToRad(FloatType(0.5) * Euler.Pitch));
			FloatType PitchSin = AeonMath::Sin(AeonMath::DegToRad(FloatType(0.5) * Euler.Pitch));

			FloatType YawCos = AeonMath::Cos(AeonMath::DegToRad(FloatType(0.5) * Euler.Yaw));
			FloatType YawSin = AeonMath::Sin(AeonMath::DegToRad(FloatType(0.5) * Euler.Yaw));

			return GQuaternion{ (RollCos * PitchCos * YawCos) + (RollSin * PitchSin * YawSin),
								(RollSin * PitchCos * YawCos) - (RollCos * PitchSin * YawSin),
								(RollCos * PitchSin * YawCos) + (RollSin * PitchCos * YawSin),
								(RollCos * PitchCos * YawSin) - (RollSin * PitchSin * YawCos) };
		}

		static constexpr GQuaternion FromAxisAngle(GVector4<FloatType> const& AxisAngle) // CHECKME maybe make dedicated type to abstract this
		{
			FloatType HalfAngle = FloatType(0.5) * AxisAngle.W;

			FloatType CosAngle = AeonMath::Cos(HalfAngle);
			FloatType SinAngle = AeonMath::Sin(HalfAngle);

			FloatType W = CosAngle;
			FloatType X = SinAngle * AxisAngle.X;
			FloatType Y = SinAngle * AxisAngle.Y;
			FloatType Z = SinAngle * AxisAngle.Z;

			return GQuaternion{ W, X, Y, Z };
		}
		
		constexpr GEulerRotation<FloatType> ToEuler() const
		{
			FloatType WW = W * W;
			FloatType XX = X * X;
			FloatType YY = Y * Y;
			FloatType ZZ = Z * Z;

			FloatType WX = W * X;
			FloatType WY = W * Y;
			FloatType WZ = W * Z;

			FloatType XY = X * Y;
			FloatType YZ = Y * Z;
			FloatType ZX = Z * X;

			// NOTE We add a small epsilon in the range [0.0, 0.5 * epsilon) to the pitch
			//  to ensure that when the argument to ArcSin approaches 1.0, it actually equals 1.0,
			//  otherwise the result of ArcSin will return values approaching but never reaching 90 degrees.
			//  We use the same expression (WY - ZX) to adjust the size of the epsilon because its value is bounded to the range [0.0, 0.5),
			//  and we want the epsilon to approach zero when this value also approaches zero.
			//  Note that the expression needs to be written exactly as is,
			//  otherwise the non-associativity of FP math will break the result.

			// CHECKME Mathematically, this might not be very correct, test this to ensure other values are correct,
			//  also check if we can offset negatively for Roll and Yaw to compensate
			constexpr FloatType Epsilon = TNumericProperties<FloatType>::Epsilon;

			return GEulerRotation<FloatType>{ AeonMath::RadToDeg(AeonMath::ArcTan(FloatType(2.0) * (WX + YZ), WW - XX - YY + ZZ)),
											  AeonMath::RadToDeg(AeonMath::ArcSin(FloatType(2.0) * (WY - ZX) + (WY - ZX) * Epsilon)),
											  AeonMath::RadToDeg(AeonMath::ArcTan(FloatType(2.0) * (WZ + XY), WW + XX - YY - ZZ)) };
		}

		constexpr GVector4<FloatType> ToAxisAngle() const
		{
			FloatType HalfAngle = AeonMath::ArcCos(W);
			FloatType InvSinAngle = FloatType(1.0) / AeonMath::Sin(HalfAngle);

			GVector3<FloatType> Axis = GVector3<FloatType>{ X, Y, Z } * InvSinAngle;
			FloatType Angle = FloatType(2.0) * HalfAngle;

			return { Axis, Angle };
		}

		AEON_FORCEINLINE constexpr FloatType const* Data() const { return &W; }
		AEON_FORCEINLINE constexpr FloatType* Data() { return AEON_INVOKE_CONST_OVERLOAD(Data); }

		static constexpr GQuaternion Identity() { return GQuaternion{ FloatType(1.0), FloatType(0.0), FloatType(0.0), FloatType(0.0) }; }

		static constexpr GQuaternion CW90() { return GQuaternion{ AeonMath::Constants<FloatType>::SqrtOneHalf,
																  +AeonMath::Constants<FloatType>::SqrtOneHalf,
																  FloatType(0.0),
																  FloatType(0.0) }; }
		static constexpr GQuaternion CCW90() { return GQuaternion{ AeonMath::Constants<FloatType>::SqrtOneHalf,
																   -AeonMath::Constants<FloatType>::SqrtOneHalf,
																   FloatType(0.0),
																   FloatType(0.0) }; }

		static constexpr GQuaternion Down90() { return GQuaternion{ AeonMath::Constants<FloatType>::SqrtOneHalf,
																	FloatType(0.0),
																	+AeonMath::Constants<FloatType>::SqrtOneHalf,
																	FloatType(0.0) }; }
		static constexpr GQuaternion Up90() { return GQuaternion{ AeonMath::Constants<FloatType>::SqrtOneHalf,
																  FloatType(0.0),
																  -AeonMath::Constants<FloatType>::SqrtOneHalf,
																  FloatType(0.0) }; }

		static constexpr GQuaternion Left90() { return GQuaternion{ AeonMath::Constants<FloatType>::SqrtOneHalf,
																	FloatType(0.0),
																	FloatType(0.0),
																	+AeonMath::Constants<FloatType>::SqrtOneHalf }; }
		static constexpr GQuaternion Right90() { return GQuaternion{ AeonMath::Constants<FloatType>::SqrtOneHalf,
																	 FloatType(0.0),
																	 FloatType(0.0),
																	 -AeonMath::Constants<FloatType>::SqrtOneHalf }; }

		static constexpr GQuaternion NaN() { return GQuaternion{ AeonMath::MakeNaN<FloatType>(),
		    													 AeonMath::MakeNaN<FloatType>(),
																 AeonMath::MakeNaN<FloatType>(),
																 AeonMath::MakeNaN<FloatType>() }; }

		FloatType W, X, Y, Z;
	};

	using SFP32Quaternion = GQuaternion<float32>;
	using SFP64Quaternion = GQuaternion<float64>;

	using SQuaternion = AEON_TOKEN_CONCAT(SFP, AEON_MATH_DEFAULT_WORD_SIZE, Quaternion);
}

#pragma pack(pop)

#endif