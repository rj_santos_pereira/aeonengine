#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_MATH_HYPERPLANE
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_MATH_HYPERPLANE

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Math/Vector.hpp"
#include "Aeon/Framework/Math/Quaternion.hpp"

// FIXME remove this once implementation is finished
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-parameter"

namespace Aeon
{
	// A hyperplane is a (n-1)-dimensional flat embedded in n-dimensional ambient space,
	// e.g. in Euclidean 3-space, the hyperplane is an Euclidean 2-plane (2-flat).

	template <CFloatingPoint FloatType>
	class GHyperplane2
	{
	public:
		GHyperplane2() = default;
		constexpr GHyperplane2(GVector2<FloatType> const& NewNormal, FloatType NewOriginDistance)
			: Normal{ NewNormal }
			, OriginDistance{ NewOriginDistance }
		{
			AEON_ASSERT(!Normal.IsApproxZero());

			FloatType LenSq = Normal.LengthSquared();
			if (!AeonMath::IsApproxEqual(LenSq, FloatType(1.0)))
			{
				FloatType Factor = FloatType(1.0) / AeonMath::Sqrt(LenSq);

				Normal *= Factor;
				OriginDistance *= Factor;
			}
		}

		// TODO implement this

		GVector2<FloatType> Normal;
		FloatType OriginDistance;
	};

	template <CFloatingPoint FloatType>
	class GHyperplane3
	{
	public:
		using VectorType = GVector3<FloatType>;
		using ScalarType = FloatType;
		static constexpr szint DimensionCount = 3;

	public:
		GHyperplane3() = default;
		constexpr GHyperplane3(GVector3<FloatType> const& NewNormal, FloatType NewOriginDistance)
			: Normal{ NewNormal }
			, OriginDistance{ NewOriginDistance }
		{
			AEON_ASSERT(!Normal.IsApproxZero());

			FloatType LenSq = Normal.LengthSquared();
			if (!AeonMath::IsApproxEqual(LenSq, FloatType(1.0)))
			{
				FloatType Factor = FloatType(1.0) / AeonMath::Sqrt(LenSq);

				Normal *= Factor;
				OriginDistance *= Factor;
			}
		}

		constexpr GHyperplane3& operator+=(GVector3<FloatType> const& Translation)
		{
			GVector3<FloatType> OldPoint = MakePoint();
			GVector3<FloatType> NewPoint = OldPoint + Translation;
			OriginDistance = Normal.Dot(NewPoint);
			return *this;
		}

		constexpr GHyperplane3& operator*=(GQuaternion<FloatType> const& Rotation)
		{
			// CHECKME May need to recalculate the OriginDistance, see
			// https://stackoverflow.com/questions/2306665/how-to-efficiently-rotate-and-translate-a-plane-in-3d
			Normal = Rotation.RotateVector(Normal);
			return *this;
		}

		friend constexpr GHyperplane3 operator+(GHyperplane3 const& Plane, GVector3<FloatType> const& Translation)
		{
			return GHyperplane3{ Plane } += Translation;
		}
		friend constexpr GHyperplane3 operator+(GVector3<FloatType> const& Translation, GHyperplane3 const& Plane)
		{
			return GHyperplane3{ Plane } += Translation;
		}

		friend constexpr GHyperplane3 operator*(GHyperplane3 const& Plane, GQuaternion<FloatType> const& Rotation)
		{
			return GHyperplane3{ Plane } *= Rotation;
		}
		friend constexpr GHyperplane3 operator*(GQuaternion<FloatType> const& Rotation, GHyperplane3 const& Plane)
		{
			return GHyperplane3{ Plane } *= Rotation;
		}

		constexpr GHyperplane3 operator+() const
		{
			return GHyperplane3{ *this };
		}
		constexpr GHyperplane3 operator-() const
		{
			return GHyperplane3{ Normal.Negation(), OriginDistance };
		}

		FloatType SignedDistance(GVector3<FloatType> const& Point) const
		{
			return Normal.Dot(Point - MakePoint());
		}
		FloatType Distance(GVector3<FloatType> const& Point) const
		{
			return AeonMath::Abs(SignedDistance(Point));
		}

		FloatType Angle(GVector3<FloatType> const& Line) const // CHECKME what is the use case of this? should this instead be moved to GVector(Ops)?
		{
			constexpr FloatType LimitAngle = FloatType(AeonMath::Constants<FloatType>::HalfPi);

			FloatType NormalAngle = AeonMath::ArcCos(GVector3<FloatType>::Dot(Normal, Line.AsNormalized()));
			FloatType PlaneAngle = LimitAngle - AeonMath::Mod(NormalAngle, LimitAngle);

			return PlaneAngle;
		}

		GVector3<FloatType> ProjectPointAlongDirection(GVector3<FloatType> const& Point, GVector3<FloatType> const& Direction) const
		{
			return Direction * (Normal.Dot(MakePoint() - Point) / Normal.Dot(Direction)) + Point;
		}
		GVector3<FloatType> ProjectPointAlongNormal(GVector3<FloatType> const& Point) const
		{
			return Normal * Normal.Dot(MakePoint() - Point) + Point;
		}

		// TODO Implement FindRandomPoint versions of these methods

		GVector3<FloatType> FindPointWithX(FloatType X) const
		{
			// TODO implement this
			return GVector3<FloatType>{};
		}
		GVector3<FloatType> FindPointWithY(FloatType Y) const
		{
			// TODO implement this
			return GVector3<FloatType>{};
		}
		GVector3<FloatType> FindPointWithZ(FloatType Z) const
		{
			// TODO implement this
			return GVector3<FloatType>{};
		}

		GVector3<FloatType> FindPointWithXY(FloatType X, FloatType Y) const
		{
			bool IsNonZeroX = !AeonMath::IsApproxZero(Normal.X);
			bool IsNonZeroY = !AeonMath::IsApproxZero(Normal.Y);

			FloatType ProjX = Normal.X * X;
			FloatType ProjY = Normal.Y * Y;
			FloatType ProjXY = ProjX + ProjY;

			if (!AeonMath::IsApproxZero(Normal.Z))
			{
				FloatType Z = (OriginDistance - ProjXY) / Normal.Z;
				return GVector3<FloatType>{ X, Y, Z };
			}

			if ((IsNonZeroX && IsNonZeroY && AeonMath::IsApproxEqual(ProjXY, OriginDistance))
				|| (IsNonZeroX && AeonMath::IsApproxEqual(ProjX, OriginDistance))
				|| (IsNonZeroY && AeonMath::IsApproxEqual(ProjY, OriginDistance)))
			{
				return GVector3<FloatType>{ X, Y, FloatType(0.0) };
			}

			return GVector3<FloatType>::NaN();
		}
		GVector3<FloatType> FindPointWithYZ(FloatType Y, FloatType Z) const
		{
			bool IsNonZeroY = !AeonMath::IsApproxZero(Normal.Y);
			bool IsNonZeroZ = !AeonMath::IsApproxZero(Normal.Z);

			FloatType ProjY = Normal.Y * Y;
			FloatType ProjZ = Normal.Z * Z;
			FloatType ProjYZ = ProjY + ProjZ;

			if (!AeonMath::IsApproxZero(Normal.X))
			{
				FloatType X = (OriginDistance - ProjYZ) / Normal.X;
				return GVector3<FloatType>{ X, Y, Z };
			}

			if ((IsNonZeroY && IsNonZeroZ && AeonMath::IsApproxEqual(ProjYZ, OriginDistance))
				|| (IsNonZeroY && AeonMath::IsApproxEqual(ProjY, OriginDistance))
				|| (IsNonZeroZ && AeonMath::IsApproxEqual(ProjZ, OriginDistance)))
			{
				return GVector3<FloatType>{ FloatType(0.0), Y, Z };
			}

			return GVector3<FloatType>::NaN();
		}
		GVector3<FloatType> FindPointWithZX(FloatType Z, FloatType X) const
		{
			bool IsNonZeroZ = !AeonMath::IsApproxZero(Normal.Z);
			bool IsNonZeroX = !AeonMath::IsApproxZero(Normal.X);

			FloatType ProjZ = Normal.Z * Z;
			FloatType ProjX = Normal.X * X;
			FloatType ProjZX = ProjZ + ProjX;

			if (!AeonMath::IsApproxZero(Normal.Y))
			{
				FloatType Y = (OriginDistance - ProjZX) / Normal.Y;
				return GVector3<FloatType>{ X, Y, Z };
			}

			if ((IsNonZeroZ && IsNonZeroX && AeonMath::IsApproxEqual(ProjZX, OriginDistance))
				|| (IsNonZeroZ && AeonMath::IsApproxEqual(ProjZ, OriginDistance))
				|| (IsNonZeroX && AeonMath::IsApproxEqual(ProjX, OriginDistance)))
			{
				return GVector3<FloatType>{ X, FloatType(0.0), Z };
			}

			return GVector3<FloatType>::NaN();
		}

		GVector3<FloatType> FindRandomPoint() const
		{
			// TODO implement this (needs Random)
			return GVector3<FloatType>{};
		}

		bool ContainsPoint(GVector3<FloatType> const& Point) const
		{
			return AeonMath::IsApproxEqual(Normal.Dot(Point), OriginDistance);
		}

		GVector3<FloatType> MakePoint() const
		{
			if (AeonMath::IsApproxZero(OriginDistance))
			{
				return GVector3<FloatType>::Zero();
			}

			if (!AeonMath::IsApproxZero(Normal.X))
			{
				return GVector3<FloatType>{ OriginDistance / Normal.X, 0.0, 0.0 };
			}

			if (!AeonMath::IsApproxZero(Normal.Y))
			{
				return GVector3<FloatType>{ 0.0, OriginDistance / Normal.Y, 0.0 };
			}

			if (!AeonMath::IsApproxZero(Normal.Z))
			{
				return GVector3<FloatType>{ 0.0, 0.0, OriginDistance / Normal.Z };
			}

			return GVector3<FloatType>::NaN();
		}

		bool IsValid() const
		{
			return Normal.IsApproxUnit();
		}

		GVector3<FloatType> Normal;
		FloatType OriginDistance;
	};

	template <CFloatingPoint FloatType>
	class GHyperplane4
	{
	public:
		GHyperplane4() = default;
		constexpr GHyperplane4(GVector4<FloatType> const& NewNormal, FloatType NewOriginDistance)
			: Normal{ NewNormal }
			, OriginDistance{ NewOriginDistance }
		{
			AEON_ASSERT(!Normal.IsApproxZero());

			FloatType LenSq = Normal.LengthSquared();
			if (!AeonMath::IsApproxEqual(LenSq, FloatType(1.0)))
			{
				FloatType Factor = FloatType(1.0) / AeonMath::Sqrt(LenSq);

				Normal *= Factor;
				OriginDistance *= Factor;
			}
		}

		// TODO implement this

		GVector4<FloatType> Normal;
		FloatType OriginDistance;
	};

	using SFP32Hyperplane2 = GHyperplane2<float32>;
	using SFP64Hyperplane2 = GHyperplane2<float64>;

	using SFP32Hyperplane3 = GHyperplane3<float32>;
	using SFP64Hyperplane3 = GHyperplane3<float64>;

	using SFP32Hyperplane4 = GHyperplane4<float32>;
	using SFP64Hyperplane4 = GHyperplane4<float64>;

	using SHyperplane2 = AEON_TOKEN_CONCAT(SFP, AEON_MATH_DEFAULT_WORD_SIZE, Hyperplane2);
	using SHyperplane3 = AEON_TOKEN_CONCAT(SFP, AEON_MATH_DEFAULT_WORD_SIZE, Hyperplane3);
	using SHyperplane4 = AEON_TOKEN_CONCAT(SFP, AEON_MATH_DEFAULT_WORD_SIZE, Hyperplane4);
}

#pragma clang diagnostic pop

#endif