#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_MATH_VECTORCOMMON
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_MATH_VECTORCOMMON

#include "Aeon/Framework/Core.hpp"

namespace Aeon::ZzInternal
{
	template <class VectorType, class NewScalarType>
	struct ZzInternal_TReplaceVectorScalarType;

	template <template <class ...> class VectorTemplate, class ScalarType, class ... NthParamTypes, class NewScalarType>
	struct ZzInternal_TReplaceVectorScalarType<VectorTemplate<ScalarType, NthParamTypes ...>, NewScalarType>
	{
		using Type = VectorTemplate<NewScalarType, NthParamTypes ...>;
	};

	template <class VectorType, class NewScalarType>
	using TReplaceVectorScalarType = typename ZzInternal_TReplaceVectorScalarType<VectorType, NewScalarType>::Type;



	template <class VectorType, class ScalarType, class ElementIndexSeqType>
	class ZzInternal_GVectorOps;

	template <class VectorType, class ScalarType, szint ElementCountValue>
	using GVectorOps = ZzInternal_GVectorOps<VectorType, ScalarType, TMakeIndexSequence<ElementCountValue>>;



	template <class Type, class ClassType = void>
	concept CNumericVector = requires
	{
		requires CSameClassTemplate<Type, ClassType> || CVoid<ClassType>;

		typename Type::ScalarType;
		requires CNumeric<typename Type::ScalarType>;

		Type::ElementCount;
		requires (Type::ElementCount > 0);
		
		requires CBaseClass<GVectorOps<Type, typename Type::ScalarType, Type::ElementCount>, Type>;
	};



	template <class VectorType, class ScalarType, szint... ElementIndexValues>
	class ZzInternal_GVectorOps<VectorType, ScalarType, TIndexSequence<ElementIndexValues ...>>
	{
		static constexpr szint ElementCount = TIndexSequence<ElementIndexValues ...>::Count;

	protected:
		using FloatCommonType = TSelectIf<VIsFloatingPoint<ScalarType>,
										  ScalarType,
										  float64>;

		explicit constexpr ZzInternal_GVectorOps()
		{
			// TODO error msgs
			// Derived class must be a CRTP class
			static_assert(VIsBaseClass<ZzInternal_GVectorOps, VectorType>);
			// Derived class must be standard-layout, so that Data() has well-defined behavior
			static_assert(VIsStandardLayoutObject<VectorType>);
			static_assert(VIsTriviallyCopyable<VectorType>);
		}

		// Arithmetic (compound) assignment operators

		template <CNumericVector<VectorType> OtherVectorType>
		constexpr VectorType& operator+=(OtherVectorType const& Other)
		{
			using OtherScalarType = typename OtherVectorType::ScalarType;

			ScalarType* DataPtr = Data();
			OtherScalarType const* OtherDataPtr = Other.Data();

			for (szint Index = 0; Index < ElementCount; ++Index)
			{
				DataPtr[Index] = ScalarType(DataPtr[Index] + OtherDataPtr[Index]);
			}

			return static_cast<VectorType&>(*this);
		}
		template <CNumericVector<VectorType> OtherVectorType>
		constexpr VectorType& operator-=(OtherVectorType const& Other)
		{
			using OtherScalarType = typename OtherVectorType::ScalarType;

			ScalarType* DataPtr = Data();
			OtherScalarType const* OtherDataPtr = Other.Data();

			for (szint Index = 0; Index < ElementCount; ++Index)
			{
				DataPtr[Index] = ScalarType(DataPtr[Index] - OtherDataPtr[Index]);
			}

			return static_cast<VectorType&>(*this);
		}
		template <CNumericVector<VectorType> OtherVectorType>
		constexpr VectorType& operator*=(OtherVectorType const& Other)
		{
			using OtherScalarType = typename OtherVectorType::ScalarType;

			ScalarType* DataPtr = Data();
			OtherScalarType const* OtherDataPtr = Other.Data();

			for (szint Index = 0; Index < ElementCount; ++Index)
			{
				DataPtr[Index] = ScalarType(DataPtr[Index] * OtherDataPtr[Index]);
			}

			return static_cast<VectorType&>(*this);
		}
		template <CNumericVector<VectorType> OtherVectorType>
		constexpr VectorType& operator/=(OtherVectorType const& Other)
		{
			using OtherScalarType = typename OtherVectorType::ScalarType;

			ScalarType* DataPtr = Data();
			OtherScalarType const* OtherDataPtr = Other.Data();

			for (szint Index = 0; Index < ElementCount; ++Index)
			{
				DataPtr[Index] = ScalarType(DataPtr[Index] / OtherDataPtr[Index]);
			}

			return static_cast<VectorType&>(*this);
		}
		template <CNumericVector<VectorType> OtherVectorType>
		constexpr VectorType& operator%=(OtherVectorType const& Other)
		{
			using OtherScalarType = typename OtherVectorType::ScalarType;

			ScalarType* DataPtr = Data();
			OtherScalarType const* OtherDataPtr = Other.Data();

			for (szint Index = 0; Index < ElementCount; ++Index)
			{
				DataPtr[Index] = ScalarType(DataPtr[Index] % OtherDataPtr[Index]);
			}

			return static_cast<VectorType&>(*this);
		}

		template <CNumeric OtherScalarType>
		constexpr VectorType& operator+=(OtherScalarType Scalar)
		{
			ScalarType* DataPtr = Data();

			for (szint Index = 0; Index < ElementCount; ++Index)
			{
				DataPtr[Index] = ScalarType(DataPtr[Index] + Scalar);
			}

			return static_cast<VectorType&>(*this);
		}
		template <CNumeric OtherScalarType>
		constexpr VectorType& operator-=(OtherScalarType Scalar)
		{
			ScalarType* DataPtr = Data();

			for (szint Index = 0; Index < ElementCount; ++Index)
			{
				DataPtr[Index] = ScalarType(DataPtr[Index] - Scalar);
			}

			return static_cast<VectorType&>(*this);
		}
		template <CNumeric OtherScalarType>
		constexpr VectorType& operator*=(OtherScalarType Scalar)
		{
			ScalarType* DataPtr = Data();

			for (szint Index = 0; Index < ElementCount; ++Index)
			{
				DataPtr[Index] = ScalarType(DataPtr[Index] * Scalar);
			}

			return static_cast<VectorType&>(*this);
		}
		template <CNumeric OtherScalarType>
		constexpr VectorType& operator/=(OtherScalarType Scalar)
		{
			ScalarType* DataPtr = Data();

			for (szint Index = 0; Index < ElementCount; ++Index)
			{
				DataPtr[Index] = ScalarType(DataPtr[Index] / Scalar);
			}

			return static_cast<VectorType&>(*this);
		}
		template <CNumeric OtherScalarType>
		constexpr VectorType& operator%=(OtherScalarType Scalar)
		{
			ScalarType* DataPtr = Data();

			for (szint Index = 0; Index < ElementCount; ++Index)
			{
				DataPtr[Index] = ScalarType(DataPtr[Index] % Scalar);
			}

			return static_cast<VectorType&>(*this);
		}

		// Arithmetic operators

		template <CNumericVector<VectorType> OtherVectorType>
		constexpr auto operator+(OtherVectorType const& Other) const
		{
			using CommonScalarType = TCommonType<ScalarType, typename OtherVectorType::ScalarType>;
			if constexpr (VIsSameClass<ScalarType, CommonScalarType>)
			{
				return VectorType{ static_cast<VectorType const&>(*this) } += Other;
			}
			else
			{
				return OtherVectorType{ static_cast<VectorType const&>(*this) } += Other;
			}
		}
		template <CNumericVector<VectorType> OtherVectorType>
		constexpr auto operator-(OtherVectorType const& Other) const
		{
			using CommonScalarType = TCommonType<ScalarType, typename OtherVectorType::ScalarType>;
			if constexpr (VIsSameClass<ScalarType, CommonScalarType>)
			{
				return VectorType{ static_cast<VectorType const&>(*this) } -= Other;
			}
			else
			{
				return OtherVectorType{ static_cast<VectorType const&>(*this) } -= Other;
			}
		}
		template <CNumericVector<VectorType> OtherVectorType>
		constexpr auto operator*(OtherVectorType const& Other) const
		{
			using CommonScalarType = TCommonType<ScalarType, typename OtherVectorType::ScalarType>;
			if constexpr (VIsSameClass<ScalarType, CommonScalarType>)
			{
				return VectorType{ static_cast<VectorType const&>(*this) } *= Other;
			}
			else
			{
				return OtherVectorType{ static_cast<VectorType const&>(*this) } *= Other;
			}
		}
		template <CNumericVector<VectorType> OtherVectorType>
		constexpr auto operator/(OtherVectorType const& Other) const
		{
			using CommonScalarType = TCommonType<ScalarType, typename OtherVectorType::ScalarType>;
			if constexpr (VIsSameClass<ScalarType, CommonScalarType>)
			{
				return VectorType{ static_cast<VectorType const&>(*this) } /= Other;
			}
			else
			{
				return OtherVectorType{ static_cast<VectorType const&>(*this) } /= Other;
			}
		}
		template <CNumericVector<VectorType> OtherVectorType>
		constexpr auto operator%(OtherVectorType const& Other) const
		{
			using CommonScalarType = TCommonType<ScalarType, typename OtherVectorType::ScalarType>;
			if constexpr (VIsSameClass<ScalarType, CommonScalarType>)
			{
				return VectorType{ static_cast<VectorType const&>(*this) } %= Other;
			}
			else
			{
				return OtherVectorType{ static_cast<VectorType const&>(*this) } %= Other;
			}
		}

#	pragma clang diagnostic push
#	pragma clang diagnostic ignored "-Wunused-value"

		template <CNumeric OtherScalarType>
		friend constexpr auto operator+(VectorType const& Vector, OtherScalarType Scalar)
		{
			using OtherVectorType = TReplaceVectorScalarType<VectorType, OtherScalarType>;
			return Vector + OtherVectorType{ (ElementIndexValues, Scalar) ... };
		}
		template <CNumeric OtherScalarType>
		friend constexpr auto operator+(OtherScalarType Scalar, VectorType const& Vector)
		{
			using OtherVectorType = TReplaceVectorScalarType<VectorType, OtherScalarType>;
			return OtherVectorType{ (ElementIndexValues, Scalar) ... } + Vector;
		}

		template <CNumeric OtherScalarType>
		friend constexpr auto operator-(VectorType const& Vector, OtherScalarType Scalar)
		{
			using OtherVectorType = TReplaceVectorScalarType<VectorType, OtherScalarType>;
			return Vector - OtherVectorType{ (ElementIndexValues, Scalar) ... };
		}
		template <CNumeric OtherScalarType>
		friend constexpr auto operator-(OtherScalarType Scalar, VectorType const& Vector)
		{
			using OtherVectorType = TReplaceVectorScalarType<VectorType, OtherScalarType>;
			return OtherVectorType{ (ElementIndexValues, Scalar) ... } - Vector;
		}

		template <CNumeric OtherScalarType>
		friend constexpr auto operator*(VectorType const& Vector, OtherScalarType Scalar)
		{
			using OtherVectorType = TReplaceVectorScalarType<VectorType, OtherScalarType>;
			return Vector * OtherVectorType{ (ElementIndexValues, Scalar) ... };
		}
		template <CNumeric OtherScalarType>
		friend constexpr auto operator*(OtherScalarType Scalar, VectorType const& Vector)
		{
			using OtherVectorType = TReplaceVectorScalarType<VectorType, OtherScalarType>;
			return OtherVectorType{ (ElementIndexValues, Scalar) ... } * Vector;
		}

		template <CNumeric OtherScalarType>
		friend constexpr auto operator/(VectorType const& Vector, OtherScalarType Scalar)
		{
			using OtherVectorType = TReplaceVectorScalarType<VectorType, OtherScalarType>;
			return Vector / OtherVectorType{ (ElementIndexValues, Scalar) ... };
		}
		template <CNumeric OtherScalarType>
		friend constexpr auto operator/(OtherScalarType Scalar, VectorType const& Vector)
		{
			using OtherVectorType = TReplaceVectorScalarType<VectorType, OtherScalarType>;
			return OtherVectorType{ (ElementIndexValues, Scalar) ... } / Vector;
		}

		template <CNumeric OtherScalarType>
		friend constexpr auto operator%(VectorType const& Vector, OtherScalarType Scalar)
		{
			using OtherVectorType = TReplaceVectorScalarType<VectorType, OtherScalarType>;
			return Vector % OtherVectorType{ (ElementIndexValues, Scalar) ... };
		}
		template <CNumeric OtherScalarType>
		friend constexpr auto operator%(OtherScalarType Scalar, VectorType const& Vector)
		{
			using OtherVectorType = TReplaceVectorScalarType<VectorType, OtherScalarType>;
			return OtherVectorType{ (ElementIndexValues, Scalar) ... } % Vector;
		}

#	pragma clang diagnostic pop

		constexpr VectorType operator+() const
		{
			return VectorType{ static_cast<VectorType const&>(*this) };
		}
		constexpr VectorType operator-() const
		{
			return Negation();
		}
#	if 0 // If we ever allow this, need to figure out how to exclude this from GVector<Int>
		constexpr VectorType operator*() const
		{
			return Reciprocal();
		}
#	endif

		// TODO maybe bit operators (for int vector)

		// TODO maybe other operations: Min, Max, Clamp, Abs, etc (see CoreMath)

		constexpr ScalarType Dot(VectorType const& Other) const
		{
			ScalarType const* DataPtr = Data();
			ScalarType const* OtherDataPtr = Other.Data();

			ScalarType Result = ScalarType(0);
			for (szint Index = 0; Index < ElementCount; ++Index)
			{
				Result += DataPtr[Index] * OtherDataPtr[Index];
			}

			return Result;
		}
		static constexpr ScalarType Dot(VectorType const& Vector1, VectorType const& Vector2)
		{
			return Vector1.Dot(Vector2);
		}

		constexpr VectorType Cross(VectorType const& Other) const
		requires (ElementCount == 3)
		{
			ScalarType const* DataPtr = Data();
			ScalarType const* OtherDataPtr = Other.Data();

			return VectorType{ DataPtr[1] * OtherDataPtr[2] - DataPtr[2] * OtherDataPtr[1],
							   DataPtr[2] * OtherDataPtr[0] - DataPtr[0] * OtherDataPtr[2],
							   DataPtr[0] * OtherDataPtr[1] - DataPtr[1] * OtherDataPtr[0] };
		}
		static constexpr VectorType Cross(VectorType const& Vector1, VectorType const& Vector2)
		requires (ElementCount == 3)
		{
			return Vector1.Cross(Vector2);
		}

		constexpr FloatCommonType LengthSquared() const
		{
			// NOTE The return type is a floating-point type of the appropriate size, even for integer ScalarType.
			return FloatCommonType(Dot(static_cast<VectorType const&>(*this)));
		}

		constexpr FloatCommonType Length() const
		{
			// NOTE The return type is a floating-point type of the appropriate size, even for integer ScalarType.
			return AeonMath::Sqrt(LengthSquared());
		}

		constexpr VectorType Negation() const
		{
			VectorType Result{ static_cast<VectorType const&>(*this) };
			ScalarType* DataPtr = Result.Data();

			for (szint Index = 0; Index < ElementCount; ++Index)
			{
				DataPtr[Index] = -DataPtr[Index];
			}

			return Result;
		}

		constexpr VectorType Reciprocal() const
		{
			VectorType Result{ static_cast<VectorType const&>(*this) };
			ScalarType* DataPtr = Result.Data();

			for (szint Index = 0; Index < ElementCount; ++Index)
			{
				DataPtr[Index] = 1 / DataPtr[Index];
			}

			return Result;
		}

		constexpr VectorType& Normalize()
		{
			ScalarType LenSq = LengthSquared();
			// Since we are already paying for the cost of the branch, we also check for the trivial case,
			// to avoid redundant (expensive) computation.
			if (!AeonMath::IsApproxZero(LenSq) && !AeonMath::IsApproxEqual(LenSq, ScalarType(1.0)))
			{
				ScalarType* DataPtr = Data();

				// CHECKME ensure codegen produces optimal code, namely rsqrtss for float32, instead of sqrt and div (float64 might be tougher)
				ScalarType Factor = ScalarType(1.0) / AeonMath::Sqrt(LenSq);
			//	ScalarType Factor = AeonMath::RSqrt(LenSq);

				for (szint Index = 0; Index < ElementCount; ++Index)
				{
					DataPtr[Index] *= Factor;
				}
			}

			return static_cast<VectorType&>(*this);
		}
		constexpr VectorType& NormalizeUnsafe()
		{
			ScalarType* DataPtr = Data();

			ScalarType LenSq = LengthSquared();

			ScalarType Factor = ScalarType(1.0) / AeonMath::Sqrt(LenSq);
		//	ScalarType Factor = AeonMath::RSqrt(LenSq);

			for (szint Index = 0; Index < ElementCount; ++Index)
			{
				DataPtr[Index] *= Factor;
			}

			return static_cast<VectorType&>(*this);
		}

		constexpr VectorType AsNormalized() const
		{
			return VectorType{ static_cast<VectorType const&>(*this) }.Normalize();
		}

		constexpr bool IsEqual(VectorType const& Other) const
		{
			// NOTE Using fold expressions to enable short-circuiting AND
			ScalarType const* DataPtr = Data();
			ScalarType const* OtherDataPtr = Other.Data();
			return (... && (DataPtr[ElementIndexValues] == OtherDataPtr[ElementIndexValues]));
		}
		constexpr bool IsApproxEqual(VectorType const& Other, ScalarType Tolerance = TNumericProperties<ScalarType>::Epsilon) const
		{
			ScalarType const* DataPtr = Data();
			ScalarType const* OtherDataPtr = Other.Data();
			return (... && AeonMath::IsApproxEqual(DataPtr[ElementIndexValues], OtherDataPtr[ElementIndexValues], Tolerance));
		}
		constexpr bool IsApproxZero(ScalarType Tolerance = TNumericProperties<ScalarType>::Epsilon) const
		{
			ScalarType const* DataPtr = Data();
			return (... && AeonMath::IsApproxZero(DataPtr[ElementIndexValues], Tolerance));
		}
		constexpr bool IsApproxUnit(ScalarType Tolerance = TNumericProperties<ScalarType>::Epsilon) const
		{
			// CHECKME Should we use Length() here?
			return AeonMath::IsApproxEqual(LengthSquared(), ScalarType(1), Tolerance);
		}

		constexpr ScalarType const& operator[](szint Index) const
		{
			AEON_ASSERT(Index < ElementCount);
			return Data()[Index];
		}
		constexpr ScalarType& operator[](szint Index)
		{
			AEON_ASSERT(Index < ElementCount);
			return Data()[Index];
		}

		// TODO add ToString methods? maybe in derived classes directly

		AEON_FORCEINLINE constexpr ScalarType const* Data() const { return static_cast<VectorType const&>(*this).Data(); }
		AEON_FORCEINLINE constexpr ScalarType* Data() { return AEON_INVOKE_CONST_OVERLOAD(Data); }

		template <class OtherVectorType, class OtherScalarType, class OtherElementIndexSeqType>
		friend class ZzInternal_GVectorOps;
	};
}

#endif