#add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/)

target_sources(${PROJECT_NAME}
	PUBLIC
		${CMAKE_CURRENT_SOURCE_DIR}/RandomGenerator.hpp
	PRIVATE
		${CMAKE_CURRENT_SOURCE_DIR}/RandomGenerator.cpp
	)