#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_MATH_RANDOM_RANDOMGENERATOR
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_MATH_RANDOM_RANDOMGENERATOR

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Container/Array.hpp"

namespace Aeon
{
	AEON_DECLARE_TAG_CLASS(TDeferSeeding);

	template <CNumeric NumType>
	class GRandomGenerator
	{
	public:
		using GenNumType = TMakeSignMagnitudeInteger<false, (sizeof(NumType) / (VIsFloatingPoint<NumType> + 1))>;
		using SeedNumType = uint32; // HACK Implementation detail, could be extended to uint64 in the future

		GRandomGenerator();
		explicit GRandomGenerator(TDeferSeeding);

		~GRandomGenerator();

		GRandomGenerator(GRandomGenerator const&) = delete;
		GRandomGenerator(GRandomGenerator&&) = delete;

		GRandomGenerator& operator=(GRandomGenerator const&) = delete;
		GRandomGenerator& operator=(GRandomGenerator&&) = delete;

		void Reseed();

		NumType Generate();
		NumType GenerateInRange(NumType MinValue, NumType MaxValue);
		NumType GenerateInRange_Biased(NumType MinValue, NumType MaxValue);

		GArray<SeedNumType> GetSeed() const;
		void SetSeed(GArray<SeedNumType> NewSeed);

		NumType operator()() { return Generate(); }
		NumType operator()(NumType MinValue, NumType MaxValue) { return GenerateInRange(MinValue, MaxValue); }

		static constexpr NumType Precision()
		requires CFloatingPoint<NumType>
		{
			// NOTE This class can only generate a limited number of values in the range [0.0, 1.0]
			//  with respect to the total number of possible values representable in floating-point, due to precision issues;
			//  this returns the minimum representable value, henceforth 'precision', that can be generated.
			//  Naturally, this also represents the distance between any two consecutive values generated within the range.
			constexpr NumType Prec = NumType(1.0) / NumType(TNumericProperties<GenNumType>::Maximum/* + 1.0*/);
			return Prec;
		}

	private:
		// HACK Implementation detail, might be extended if any platform needs more storage in the future
		static constexpr szint StorageSize = 64;

		byte Storage[StorageSize];

	};

	// TODO implement other generators GQuasiRandomGenerator
}

#endif