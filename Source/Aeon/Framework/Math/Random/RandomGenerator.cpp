#include "Aeon/Framework/Math/Random/RandomGenerator.hpp"

#if AEON_COMPILER_MSVC
#	pragma warning(push)
	// CHECKME Ensure that we can really silence these warnings, and we're not hiding some real issue (note that said issue can be confined to MSVC)
#	pragma warning(disable: 4127 4146 4244)

#	if AEON_IS_LITTLE_ENDIAN_ARCH
#		define PCG_LITTLE_ENDIAN 1
#	else
#		define PCG_LITTLE_ENDIAN 0
#	endif
#endif

#include "pcg_random/pcg_random.hpp"
#include "pcg_random/randutils.hpp"

#if AEON_COMPILER_MSVC
#	pragma warning(pop)
#endif

namespace Aeon
{
	template <CUnsignedInteger NumType>
	class GRNGEngine;

	template <>
	class GRNGEngine<uint8>
	{
	public:
		using engine_type = pcg32;
		using result_type = uint8;
		using state_type = uint16; // NOTE The state is actually of type uint64, but generate_in_range only needs uint16

		using seed_type = randutils::seed_seq_fe128;
		using params_type = std::array<seed_type::result_type, seed_type::size()>;

		AEON_FORCEINLINE void Reseed()
		{
			randutils::auto_seeded<seed_type> NewSeed;
			NewSeed.param(std::begin(SeedParams));
			Engine.seed(NewSeed);
		}
		AEON_FORCEINLINE void Reseed(params_type NewParams)
		{
			seed_type NewSeed(std::begin(NewParams), std::end(NewParams));
			SeedParams = NewParams;
			Engine.seed(NewSeed);
		}

		AEON_FORCEINLINE result_type operator()()
		{
			engine_type::result_type Value = Engine();
			return result_type(Value & 0xFF);
		}

		params_type SeedParams;
		engine_type Engine;
	};

	template <>
	class GRNGEngine<uint16>
	{
	public:
		using engine_type = pcg32;
		using result_type = uint16;
		using state_type = uint32; // NOTE The state is actually of type uint64, but generate_in_range only needs uint32

		using seed_type = randutils::seed_seq_fe128;
		using params_type = std::array<seed_type::result_type, seed_type::size()>;

		AEON_FORCEINLINE void Reseed()
		{
			randutils::auto_seeded<seed_type> NewSeed;
			NewSeed.param(std::begin(SeedParams));
			Engine.seed(NewSeed);
		}
		AEON_FORCEINLINE void Reseed(params_type NewParams)
		{
			seed_type NewSeed(std::begin(NewParams), std::end(NewParams));
			SeedParams = NewParams;
			Engine.seed(NewSeed);
		}

		AEON_FORCEINLINE result_type operator()()
		{
			engine_type::result_type Value = Engine();
			return result_type(Value & 0xFFFF);
		}

		params_type SeedParams;
		engine_type Engine;
	};

	template <>
	class GRNGEngine<uint32>
	{
	public:
		using engine_type = pcg32;
		using result_type = engine_type::result_type;
		using state_type = engine_type::state_type;

		using seed_type = randutils::seed_seq_fe128;
		using params_type = std::array<seed_type::result_type, seed_type::size()>;

		AEON_FORCEINLINE void Reseed()
		{
			randutils::auto_seeded<seed_type> NewSeed;
			NewSeed.param(std::begin(SeedParams));
			Engine.seed(NewSeed);
		}
		AEON_FORCEINLINE void Reseed(params_type NewParams)
		{
			seed_type NewSeed(std::begin(NewParams), std::end(NewParams));
			SeedParams = NewParams;
			Engine.seed(NewSeed);
		}

		AEON_FORCEINLINE result_type operator()()
		{
			return Engine();
		}

		params_type SeedParams;
		engine_type Engine;
	};

	template <>
	class GRNGEngine<uint64>
	{
	public:
		using engine_type = pcg64;
		using result_type = engine_type::result_type;
		using state_type = engine_type::state_type;

		using seed_type = randutils::seed_seq_fe256;
		using params_type = std::array<seed_type::result_type, seed_type::size()>;

		AEON_FORCEINLINE void Reseed()
		{
			randutils::auto_seeded<seed_type> NewSeed;
			NewSeed.param(std::begin(SeedParams));
			Engine.seed(NewSeed);
		}
		AEON_FORCEINLINE void Reseed(params_type NewParams)
		{
			seed_type NewSeed(std::begin(NewParams), std::end(NewParams));
			SeedParams = NewParams;
			Engine.seed(NewSeed);
		}

		AEON_FORCEINLINE result_type operator()()
		{
			return Engine();
		}

		params_type SeedParams;
		engine_type Engine;
	};

	template <CNumeric NumType>
	GRandomGenerator<NumType>::GRandomGenerator()
		: GRandomGenerator<NumType>(TDeferSeeding::Tag)
	{
		Reseed();
	}

	template <CNumeric NumType>
	GRandomGenerator<NumType>::GRandomGenerator(TDeferSeeding)
	{
		static_assert(VIsSameClass<SeedNumType, typename GRNGEngine<GenNumType>::params_type::value_type>);
		static_assert(Aeon::SizeOf(GRNGEngine<GenNumType>) <= StorageSize);

		AeonMemory::MemConstruct<GRNGEngine<GenNumType>>(Storage);
	}

	template <CNumeric NumType>
	GRandomGenerator<NumType>::~GRandomGenerator()
	{
		AeonMemory::MemDestruct<GRNGEngine<GenNumType>>(Storage);
	}

	template <CNumeric NumType>
	void GRandomGenerator<NumType>::Reseed()
	{
		auto& RNG = *AeonMemory::MemAccess<GRNGEngine<GenNumType>>(Storage);

		RNG.Reseed();
	}

	template <CNumeric NumType>
	NumType GRandomGenerator<NumType>::Generate()
	{
		auto& RNG = *AeonMemory::MemAccess<GRNGEngine<GenNumType>>(Storage);

		if constexpr (VIsFloatingPoint<NumType>)
		{
			return NumType(Precision() * RNG());
		}
		else
		{
			return NumType(RNG());
		}
	}

	template <CNumeric NumType>
	NumType GRandomGenerator<NumType>::GenerateInRange(NumType MinValue, NumType MaxValue)
	{
		AEON_FAILSAFE(MinValue < MaxValue, return NumType{});

		auto& RNG = *AeonMemory::MemAccess<GRNGEngine<GenNumType>>(Storage);

		if constexpr (VIsFloatingPoint<NumType>)
		{
			// CHECKME According to https://www.pcg-random.org/posts/bounded-rands.html, this approach might be biased...
			//  Note that the bias, if it exists, happens in the subexpression 'Range * GenVal',
			//  where GenVal is the result of 'Precision() * RNG()'.

			// NOTE std::uniform_real_distribution (MSVC/Dinkumware) seems to use the same formula to limit the range,
			//  so this might be fine (a.k.a. unbiased) after all.
			NumType Range = MaxValue - MinValue;
			return NumType(MinValue + Range * (Precision() * RNG()));
		}
		else
		{
			GenNumType Range = MaxValue - MinValue;
			return NumType(MinValue + randutils::generate_in_range(RNG, Range));
		}
	}

	template <CNumeric NumType>
	NumType GRandomGenerator<NumType>::GenerateInRange_Biased(NumType MinValue, NumType MaxValue)
	{
		AEON_FAILSAFE(MinValue < MaxValue, return NumType{});

		auto& RNG = *AeonMemory::MemAccess<GRNGEngine<GenNumType>>(Storage);

		if constexpr (VIsFloatingPoint<NumType>)
		{
			NumType Range = MaxValue - MinValue;
			return NumType(MinValue + Range * (Precision() * RNG()));
		}
		else
		{
			GenNumType Range = MaxValue - MinValue;
			return NumType(MinValue + randutils::generate_in_range_biased(RNG, Range));
		}
	}

	template <CNumeric NumType>
	auto GRandomGenerator<NumType>::GetSeed() const -> GArray<SeedNumType>
	{
		auto& RNG = *AeonMemory::MemAccess<GRNGEngine<GenNumType> const>(Storage);

		GArray<SeedNumType> Seed{ TReserveStorage::Tag, uint32(RNG.SeedParams.size()) };
		for (auto Param : RNG.SeedParams)
		{
			Seed.Add(Param);
		}
		return Seed;
	}

	template <CNumeric NumType>
	void GRandomGenerator<NumType>::SetSeed(GArray<SeedNumType> NewSeed)
	{
		auto& RNG = *AeonMemory::MemAccess<GRNGEngine<GenNumType>>(Storage);

		typename GRNGEngine<GenNumType>::params_type Params;
		AEON_FAILSAFE(NewSeed.Count() != Params.size(),
					  return RNG.Reseed(),
					  "Provided seed array has different size than the internal one. Auto-seeding...");

		for (uint32 Idx = 0; Idx < NewSeed.Count(); ++Idx)
		{
			Params[Idx] = NewSeed[Idx];
		}

		RNG.Reseed(Params);
	}

	template class GRandomGenerator<uint8>;
	template class GRandomGenerator<uint16>;
	template class GRandomGenerator<uint32>;
	template class GRandomGenerator<uint64>;

	template class GRandomGenerator<int8>;
	template class GRandomGenerator<int16>;
	template class GRandomGenerator<int32>;
	template class GRandomGenerator<int64>;

	template class GRandomGenerator<float32>;
	template class GRandomGenerator<float64>;
}
