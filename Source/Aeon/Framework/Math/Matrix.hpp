#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_MATH_MATRIX
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_MATH_MATRIX

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Math/Vector.hpp"

#pragma pack(push, 4)

namespace Aeon
{
	namespace ZzInternal
	{
		template <class MatrixType, class NewScalarType>
		struct ZzInternal_TReplaceMatrixScalarType;

		template <template <class, szint ...> class MatrixTemplate, class ScalarType, szint ... NthParamValues, class NewScalarType>
		struct ZzInternal_TReplaceMatrixScalarType<MatrixTemplate<ScalarType, NthParamValues ...>, NewScalarType>
		{
			using Type = MatrixTemplate<NewScalarType, NthParamValues ...>;
		};

		template <class MatrixType, class NewScalarType>
		using TReplaceMatrixScalarType = typename ZzInternal_TReplaceMatrixScalarType<MatrixType, NewScalarType>::Type;
	}

	template <CFloatingPoint FloatType, szint ColCountValue>
	class GMatrixRowView
	{
	public:
		static constexpr szint ElementCount = ColCountValue;

		constexpr GMatrixRowView(FloatType* NewElementsPtr)
			: Elements{ NewElementsPtr }
		{
		}

		// TODO create methods to convert this to GFloatVector

		constexpr FloatType const& operator[](szint Index) const
		{
			AEON_ASSERT(Index < ElementCount);
			return Elements[Index];
		}
		constexpr FloatType& operator[](szint Index)
		requires CNotConst<FloatType>
		{
			return AEON_INVOKE_CONST_OVERLOAD(operator[], Index);
		}

		FloatType* Elements;
	};

	AEON_DECLARE_BOOL_ENUM(EComputeUsingPartialPivoting);

	AEON_DECLARE_TAG_CLASS(TZeroMatrix);
	AEON_DECLARE_TAG_CLASS(TIdentityMatrix);
	AEON_DECLARE_TAG_CLASS(TPermutationMatrix);

	AEON_DECLARE_TAG_CLASS(TNegationMatrix);
	AEON_DECLARE_TAG_CLASS(TReciprocalMatrix);
	AEON_DECLARE_TAG_CLASS(TTransposeMatrix);

	template <CFloatingPoint FloatType, szint RowCountValue, szint ColumnCountValue>
	class GMatrix
	{
	public:
		struct DLUPFactorizationResult
		{
			GMatrix LUMatrix;
			GMatrix PermutationMatrix;
			szint PermutationCount;
		};

		using ScalarType = FloatType;

		static constexpr szint RowCount = RowCountValue;
		static constexpr szint ColumnCount = ColumnCountValue;

		static constexpr szint ElementCount = RowCount * ColumnCount;

	private:
#	pragma clang diagnostic push
#	pragma clang diagnostic ignored "-Wunused-value"

		template <szint ... ElementIndexValues>
		constexpr GMatrix(TZeroMatrix, TIndexSequence<ElementIndexValues ...>)
			: Elements{ (ElementIndexValues, FloatType(0.0)) ... }
		{
		}
		constexpr GMatrix(TZeroMatrix)
			: GMatrix{ TZeroMatrix::Tag, TMakeIndexSequence<ElementCount>{} }
		{
		}

		template <szint ... ElementIndexValues>
		constexpr GMatrix(TIdentityMatrix, TIndexSequence<ElementIndexValues ...>)
			: Elements{ FloatType((ElementIndexValues % (ColumnCount + 1)) == 0) ... }
		{
		}
		constexpr GMatrix(TIdentityMatrix)
			: GMatrix{ TIdentityMatrix::Tag, TMakeIndexSequence<ElementCount>{} }
		{
		}

		template <szint ... ElementIndexValues>
		constexpr GMatrix(TPermutationMatrix, szint const(& PermutationArray)[RowCount], TIndexSequence<ElementIndexValues ...>)
			: Elements{ FloatType(PermutationArray[(ElementIndexValues / ColumnCount)] == (ElementIndexValues % ColumnCount)) ... }
		{
		}
		constexpr GMatrix(TPermutationMatrix, szint const(& PermutationArray)[RowCount])
			: GMatrix{ TPermutationMatrix::Tag, PermutationArray, TMakeIndexSequence<ElementCount>{} }
		{
		}

		template <szint ... ElementIndexValues>
		constexpr GMatrix(TNegationMatrix, FloatType const(& InitElements)[ElementCount], TIndexSequence<ElementIndexValues ...>)
			: Elements{ (-InitElements[ElementIndexValues]) ... }
		{
		}
		constexpr GMatrix(TNegationMatrix, FloatType const(& InitElements)[ElementCount])
			: GMatrix{ TNegationMatrix::Tag, InitElements, TMakeIndexSequence<ElementCount>{} }
		{
		}

		template <szint ... ElementIndexValues>
		constexpr GMatrix(TReciprocalMatrix, FloatType const(& InitElements)[ElementCount], TIndexSequence<ElementIndexValues ...>)
			: Elements{ (FloatType(1.0) / InitElements[ElementIndexValues]) ... }
		{
		}
		constexpr GMatrix(TReciprocalMatrix, FloatType const(& InitElements)[ElementCount])
			: GMatrix{ TReciprocalMatrix::Tag, InitElements, TMakeIndexSequence<ElementCount>{} }
		{
		}

		template <szint ... ElementIndexValues>
		constexpr GMatrix(TTransposeMatrix, FloatType const(& InitElements)[ElementCount], TIndexSequence<ElementIndexValues ...>)
			: Elements{ InitElements[(ElementIndexValues / ColumnCount) + (ElementIndexValues * RowCount) % ElementCount] ... }
		{
		}
		constexpr GMatrix(TTransposeMatrix, FloatType const(& InitElements)[ElementCount])
			: GMatrix{ TTransposeMatrix::Tag, InitElements, TMakeIndexSequence<ElementCount>{} }
		{
		}

		template <CFloatingPoint OtherFloatType, szint ... ElementIndexValues>
		constexpr GMatrix(OtherFloatType InitElement, TIndexSequence<ElementIndexValues ...>)
			: Elements{ FloatType((ElementIndexValues, InitElement)) ... }
		{
		}
		template <CFloatingPoint OtherFloatType, szint ... ElementIndexValues>
		constexpr GMatrix(OtherFloatType const(& InitElements)[ElementCount], TIndexSequence<ElementIndexValues ...>)
			: Elements{ FloatType(InitElements[ElementIndexValues]) ... }
		{
		}

#	pragma clang diagnostic pop

	public:
		GMatrix() = default;

		constexpr GMatrix(InitList<FloatType> InitList)
		{
			auto InitElemArrayPtr = InitListOps::Data(InitList);
			auto InitElemCount = AeonMath::Min(InitListOps::Count(InitList), ElementCount);

			CopyElements_Internal(Elements, InitElemArrayPtr, InitElemCount);
		}

		template <CFloatingPoint OtherFloatType>
		constexpr GMatrix& operator+=(GMatrix<OtherFloatType, RowCount, ColumnCount> const& Other)
		{
			for (szint Index = 0; Index < ElementCount; ++Index)
			{
				Elements[Index] = FloatType(Elements[Index] + Other.Elements[Index]);
			}

			return *this;
		}
		template <CFloatingPoint OtherFloatType>
		constexpr GMatrix& operator-=(GMatrix<OtherFloatType, RowCount, ColumnCount> const& Other)
		{
			for (szint Index = 0; Index < ElementCount; ++Index)
			{
				Elements[Index] = FloatType(Elements[Index] - Other.Elements[Index]);
			}

			return *this;
		}
		template <CFloatingPoint OtherFloatType>
		constexpr GMatrix& operator*=(GMatrix<OtherFloatType, RowCount, ColumnCount> const& Other)
		{
			for (szint Index = 0; Index < ElementCount; ++Index)
			{
				Elements[Index] = FloatType(Elements[Index] * Other.Elements[Index]);
			}

			return *this;
		}
		template <CFloatingPoint OtherFloatType>
		constexpr GMatrix& operator/=(GMatrix<OtherFloatType, RowCount, ColumnCount> const& Other)
		{
			for (szint Index = 0; Index < ElementCount; ++Index)
			{
				Elements[Index] = FloatType(Elements[Index] / Other.Elements[Index]);
			}

			return *this;
		}

		template <CFloatingPoint OtherFloatType>
		GMatrix& operator^=(GMatrix<OtherFloatType, RowCount, ColumnCount> const& Other)
		requires (RowCount == ColumnCount)
		{
			for (szint RowIndex = 0; RowIndex < RowCount; ++RowIndex)
			{
				FloatType* ResultRow = Elements + RowIndex * ColumnCount;
				FloatType TempResultRow[ColumnCount] = {};

				for (szint Index = 0; Index < ColumnCount; ++Index)
				{
					for (szint ColumnIndex = 0; ColumnIndex < ColumnCount; ++ColumnIndex)
					{
						auto const& Element1 = Elements[RowIndex * ColumnCount + Index];
						auto const& Element2 = Other.Elements[Index * ColumnCount + ColumnIndex];

						auto& ResultElement = TempResultRow[ColumnIndex];

						ResultElement += FloatType(Element1 * Element2);
					}
				}

				CopyElements_Internal(ResultRow, TempResultRow, ColumnCount);
			}

			return *this;
		}

		template <CFloatingPoint OtherFloatType>
		constexpr GMatrix& operator+=(OtherFloatType Scalar)
		{
			for (szint Index = 0; Index < ElementCount; ++Index)
			{
				Elements[Index] = FloatType(Elements[Index] + Scalar);
			}

			return *this;
		}
		template <CFloatingPoint OtherFloatType>
		constexpr GMatrix& operator-=(OtherFloatType Scalar)
		{
			for (szint Index = 0; Index < ElementCount; ++Index)
			{
				Elements[Index] = FloatType(Elements[Index] - Scalar);
			}

			return *this;
		}
		template <CFloatingPoint OtherFloatType>
		constexpr GMatrix& operator*=(OtherFloatType Scalar)
		{
			for (szint Index = 0; Index < ElementCount; ++Index)
			{
				Elements[Index] = FloatType(Elements[Index] * Scalar);
			}

			return *this;
		}
		template <CFloatingPoint OtherFloatType>
		constexpr GMatrix& operator/=(OtherFloatType Scalar)
		{
			for (szint Index = 0; Index < ElementCount; ++Index)
			{
				Elements[Index] = FloatType(Elements[Index] / Scalar);
			}

			return *this;
		}

		template <CFloatingPoint OtherFloatType>
		constexpr auto operator+(GMatrix<OtherFloatType, RowCount, ColumnCount> const& Other) const
		{
			using CommonFloatType = TCommonType<FloatType, OtherFloatType>;
			if constexpr (VIsSameClass<FloatType, CommonFloatType>)
			{
				return GMatrix{ *this } += Other;
			}
			else
			{
				return GMatrix<OtherFloatType, RowCount, ColumnCount>{ *this } += Other;
			}
		}
		template <CFloatingPoint OtherFloatType>
		constexpr auto operator-(GMatrix<OtherFloatType, RowCount, ColumnCount> const& Other) const
		{
			using CommonFloatType = TCommonType<FloatType, OtherFloatType>;
			if constexpr (VIsSameClass<FloatType, CommonFloatType>)
			{
				return GMatrix{ *this } -= Other;
			}
			else
			{
				return GMatrix<OtherFloatType, RowCount, ColumnCount>{ *this } -= Other;
			}
		}
		template <CFloatingPoint OtherFloatType>
		constexpr auto operator*(GMatrix<OtherFloatType, RowCount, ColumnCount> const& Other) const
		{
			using CommonFloatType = TCommonType<FloatType, OtherFloatType>;
			if constexpr (VIsSameClass<FloatType, CommonFloatType>)
			{
				return GMatrix{ *this } *= Other;
			}
			else
			{
				return GMatrix<OtherFloatType, RowCount, ColumnCount>{ *this } *= Other;
			}
		}
		template <CFloatingPoint OtherFloatType>
		constexpr auto operator/(GMatrix<OtherFloatType, RowCount, ColumnCount> const& Other) const
		{
			using CommonFloatType = TCommonType<FloatType, OtherFloatType>;
			if constexpr (VIsSameClass<FloatType, CommonFloatType>)
			{
				return GMatrix{ *this } /= Other;
			}
			else
			{
				return GMatrix<OtherFloatType, RowCount, ColumnCount>{ *this } /= Other;
			}
		}

		template <CFloatingPoint OtherFloatType, szint OtherColumnCountValue>
		constexpr auto operator^(GMatrix<OtherFloatType, ColumnCount, OtherColumnCountValue> const& Other) const
		{
			using CommonFloatType = TCommonType<FloatType, OtherFloatType>;

			GMatrix<CommonFloatType, RowCount, OtherColumnCountValue> ResultMatrix{ TZeroMatrix::Tag };

			for (szint RowIndex = 0; RowIndex < RowCount; ++RowIndex)
			{
				for (szint Index = 0; Index < ColumnCount; ++Index)
				{
					for (szint ColumnIndex = 0; ColumnIndex < OtherColumnCountValue; ++ColumnIndex)
					{
						auto const& Element1 = Elements[RowIndex * ColumnCount + Index];
						auto const& Element2 = Other.Elements[Index * OtherColumnCountValue + ColumnIndex];

						auto& ResultElement = ResultMatrix.Elements[RowIndex * OtherColumnCountValue + ColumnIndex];

						ResultElement += Element1 * Element2;
					}
				}
			}

			return ResultMatrix;
		}

#	pragma clang diagnostic push
#	pragma clang diagnostic ignored "-Wunused-value"

		template <CFloatingPoint OtherFloatType>
		friend constexpr auto operator+(GMatrix const& Vector, OtherFloatType Scalar)
		{
			using OtherMatrixType = ZzInternal::TReplaceMatrixScalarType<GMatrix, OtherFloatType>;
			return Vector + OtherMatrixType{ Scalar, TMakeIndexSequence<ElementCount>{} };
		}
		template <CFloatingPoint OtherFloatType>
		friend constexpr auto operator+(OtherFloatType Scalar, GMatrix const& Vector)
		{
			using OtherMatrixType = ZzInternal::TReplaceMatrixScalarType<GMatrix, OtherFloatType>;
			return OtherMatrixType{ Scalar, TMakeIndexSequence<ElementCount>{} } + Vector;
		}

		template <CFloatingPoint OtherFloatType>
		friend constexpr auto operator-(GMatrix const& Vector, OtherFloatType Scalar)
		{
			using OtherMatrixType = ZzInternal::TReplaceMatrixScalarType<GMatrix, OtherFloatType>;
			return Vector - OtherMatrixType{ Scalar, TMakeIndexSequence<ElementCount>{} };
		}
		template <CFloatingPoint OtherFloatType>
		friend constexpr auto operator-(OtherFloatType Scalar, GMatrix const& Vector)
		{
			using OtherMatrixType = ZzInternal::TReplaceMatrixScalarType<GMatrix, OtherFloatType>;
			return OtherMatrixType{ Scalar, TMakeIndexSequence<ElementCount>{} } - Vector;
		}

		template <CFloatingPoint OtherFloatType>
		friend constexpr auto operator*(GMatrix const& Vector, OtherFloatType Scalar)
		{
			using OtherMatrixType = ZzInternal::TReplaceMatrixScalarType<GMatrix, OtherFloatType>;
			return Vector * OtherMatrixType{ Scalar, TMakeIndexSequence<ElementCount>{} };
		}
		template <CFloatingPoint OtherFloatType>
		friend constexpr auto operator*(OtherFloatType Scalar, GMatrix const& Vector)
		{
			using OtherMatrixType = ZzInternal::TReplaceMatrixScalarType<GMatrix, OtherFloatType>;
			return OtherMatrixType{ Scalar, TMakeIndexSequence<ElementCount>{} } * Vector;
		}

		template <CFloatingPoint OtherFloatType>
		friend constexpr auto operator/(GMatrix const& Vector, OtherFloatType Scalar)
		{
			using OtherMatrixType = ZzInternal::TReplaceMatrixScalarType<GMatrix, OtherFloatType>;
			return Vector / OtherMatrixType{ Scalar, TMakeIndexSequence<ElementCount>{} };
		}
		template <CFloatingPoint OtherFloatType>
		friend constexpr auto operator/(OtherFloatType Scalar, GMatrix const& Vector)
		{
			using OtherMatrixType = ZzInternal::TReplaceMatrixScalarType<GMatrix, OtherFloatType>;
			return OtherMatrixType{ Scalar, TMakeIndexSequence<ElementCount>{} } / Vector;
		}

#	pragma clang diagnostic pop

		constexpr GMatrix operator+() const
		{
			return GMatrix{ *this };
		}
		constexpr GMatrix operator-() const
		{
			return Negation();
		}

		constexpr GMatrix operator*() const
		requires (RowCount == ColumnCount)
		{
			return Inverse();
		}

		constexpr GMatrix Inverse() const
		requires (RowCount == ColumnCount)
		{
			auto [LUMat, PermMat, PermCnt] = LUPFactorization();

			return LUMat.InverseAsLUP(PermMat);
		}

		constexpr GMatrix InverseFast() const
		requires (RowCount == ColumnCount)
		{
			auto LUMat = LUFactorization();

			return LUMat.InverseAsLU();
		}

		constexpr GMatrix InverseUsingReduction() const
		{
			return ComputeUsingRowReduction_Internal<EComputeUsingRowReductionResult::LinearSystem,
													 EComputeUsingPartialPivoting::True>(GMatrix{ TIdentityMatrix::Tag }).ReducedMatrix;
		}

		constexpr GMatrix InverseAsLUP(GMatrix const& Permutation) const
		{
			auto const& LUMat = (*this);

			return LUMat.SolveAsLUMatrix_Internal(Permutation);
		}

		constexpr GMatrix InverseAsLU() const
		{
			auto const& LUMat = (*this);

			return LUMat.SolveAsLUMatrix_Internal(GMatrix{ TIdentityMatrix::Tag });
		}

		constexpr GMatrix<FloatType, RowCount, 1> Solve(GMatrix<FloatType, RowCount, 1> const& Values) const
		{
			auto [LUMat, PermMat, PermCnt] = LUPFactorization();

			return LUMat.SolveAsLUP(PermMat, Values);
		}

		constexpr GMatrix<FloatType, RowCount, 1> SolveFast(GMatrix<FloatType, RowCount, 1> const& Values) const
		{
			auto LUMat = LUFactorization();

			return LUMat.SolveAsLU(Values);
		}

		constexpr GMatrix<FloatType, RowCount, 1> SolveUsingReduction(GMatrix<FloatType, RowCount, 1> const& Values) const
		{
			return ComputeUsingRowReduction_Internal<EComputeUsingRowReductionResult::LinearSystem,
													 EComputeUsingPartialPivoting::True>(Values).ReducedMatrix;
		}

		constexpr GMatrix<FloatType, RowCount, 1> SolveAsLUP(GMatrix const& Permutation, GMatrix<FloatType, RowCount, 1> const& Values) const
		{
			auto const& LUMat = (*this);

			auto PermutatedValues = Permutation ^ Values;

			return LUMat.SolveAsLUMatrix_Internal(PermutatedValues);
		}

		constexpr GMatrix<FloatType, RowCount, 1> SolveAsLU(GMatrix<FloatType, RowCount, 1> const& Values) const
		{
			auto const& LUMat = (*this);

			return LUMat.SolveAsLUMatrix_Internal(Values);
		}

		constexpr DLUPFactorizationResult LUPFactorization() const
		requires (RowCount == ColumnCount) // TODO At some point, it might be interesting to remove this restriction
		{
			auto [LUPMat, PermMat,
				  PermCount] = ComputeUsingRowReduction_Internal<EComputeUsingRowReductionResult::LUMatrix,
				  												 EComputeUsingPartialPivoting::True>(GMatrix{ TIdentityMatrix::Tag });
			return { LUPMat, PermMat, PermCount };
		}
		constexpr GMatrix LUFactorization() const
		requires (RowCount == ColumnCount)
		{
			return ComputeUsingRowReduction_Internal<EComputeUsingRowReductionResult::LUMatrix,
													 EComputeUsingPartialPivoting::False>(GMatrix{ TIdentityMatrix::Tag }).Matrix;
		}

		constexpr FloatType Determinant() const
		requires (RowCount == ColumnCount)
		{
			auto ReductionResult = ComputeUsingRowReduction_Internal<EComputeUsingRowReductionResult::LUMatrix,
																	 EComputeUsingPartialPivoting::True>(GMatrix{ TIdentityMatrix::Tag });

			auto Determinant = FloatType(1);

			auto const& LUMat = ReductionResult.Matrix;
			auto const& PermCount = ReductionResult.PermutationCount;

			for (szint RowIdx = 0; RowIdx < RowCount; ++RowIdx)
			{
				Determinant *= LUMat[RowIdx][RowIdx];
			}

			Determinant *= bool(PermCount & 1) ? -1 : +1;

			return Determinant;
		}

#	if 0
		constexpr GDuple<GMatrix, FloatType> PartialPivot() const
		requires (RowCount == ColumnCount)
		{
			szint RowPermutation[RowCount];

			ComputePartialPivot_Internal(RowPermutation);

			// Compute sign of the determinant from the submatrix permutation indexes,
			// which might be flipped depending on the permutations

			szint Sign = RowCount;

			for (szint RowIdx = 0; RowIdx < RowCount - 1; ++RowIdx)
			{
				Sign ^= RowPermutation[RowIdx];
			}

			Sign ^= Sign | 1;

			// Recompute original row indexes from the submatrices indexes

			for (szint RowIdx1 = RowCount - 1; RowIdx1 > 0; --RowIdx1)
			{
				for (szint RowIdx2 = RowIdx1; RowIdx2 > 0; --RowIdx2)
				{
					RowPermutation[RowIdx1] += szint(RowPermutation[RowIdx2 - 1] <= RowPermutation[RowIdx1]);
				}
			}

			return { GMatrix{ TIdentityPermutationMatrix::Tag, RowPermutation }, FloatType(bool(Sign) ? -1 : +1) };
		}
#	endif

		constexpr GMatrix Negation() const
		{
			return GMatrix{ TNegationMatrix::Tag, Elements };
		}

		constexpr GMatrix Reciprocal() const
		{
			return GMatrix{ TReciprocalMatrix::Tag, Elements };
		}

		constexpr GMatrix<FloatType, ColumnCount, RowCount> Transpose() const
		{
			return GMatrix<FloatType, ColumnCount, RowCount>{ TTransposeMatrix::Tag, Elements };
		}

		constexpr GMatrix Upper() const
		requires (RowCount == ColumnCount)
		{
			GMatrix const& ThisMat = (*this);
			GMatrix UpperMat{ TZeroMatrix::Tag };

			for (szint RowIdx = 0; RowIdx < RowCount; ++RowIdx)
			{
				for (szint ColumnIdx = RowIdx; ColumnIdx < ColumnCount; ++ColumnIdx)
				{
					UpperMat[RowIdx][ColumnIdx] = ThisMat[RowIdx][ColumnIdx];
				}
			}

			return UpperMat;
		}

		constexpr GMatrix Lower() const
		requires (RowCount == ColumnCount)
		{
			GMatrix const& ThisMat = (*this);
			GMatrix LowerMat{ TIdentityMatrix::Tag }; // CHECKME Should this be TZeroMatrix instead?

			for (szint RowIdx = 1; RowIdx < RowCount; ++RowIdx)
			{
				for (szint ColumnIdx = 0; ColumnIdx < RowIdx; ++ColumnIdx)
				{
					LowerMat[RowIdx][ColumnIdx] = ThisMat[RowIdx][ColumnIdx];
				}
			}

			return LowerMat;
		}

		constexpr GMatrix<FloatType, RowCount - 1, ColumnCount - 1> Submatrix(szint SkippedRowIdx, szint SkippedColumnIdx) const
		requires (RowCount > 1 && ColumnCount > 1)
		{
			AEON_ASSERT(SkippedRowIdx < RowCount);
			AEON_ASSERT(SkippedColumnIdx < ColumnCount);

			constexpr szint SubRowCount = RowCount - 1;
			constexpr szint SubColumnCount = ColumnCount - 1;

			GMatrix const& ThisMat = (*this);
			GMatrix<FloatType, SubRowCount, SubColumnCount> SubMat{ TZeroMatrix::Tag };

			for (szint SubRowIdx = 0; SubRowIdx < SubRowCount; ++SubRowIdx)
			{
				szint RowIdx = SubRowIdx + (SkippedRowIdx <= SubRowIdx);

				for (szint SubColumnIdx = 0; SubColumnIdx < SubColumnCount; ++SubColumnIdx)
				{
					szint ColumnIdx = SubColumnIdx + (SkippedColumnIdx <= SubColumnIdx);

					SubMat[SubRowIdx][SubColumnIdx] = ThisMat[RowIdx][ColumnIdx];
				}
			}

			return SubMat;
		}

		// TODO comparison ops

		constexpr GMatrixRowView<FloatType const, ColumnCount> operator[](szint Index) const
		{
			AEON_ASSERT(Index < RowCount);
			return GMatrixRowView<FloatType const, ColumnCount>{ Elements + ColumnCount * Index };
		}
		constexpr GMatrixRowView<FloatType, ColumnCount> operator[](szint Index)
		{
			AEON_ASSERT(Index < RowCount);
			return GMatrixRowView<FloatType, ColumnCount>{ Elements + ColumnCount * Index };
		}

		AEON_FORCEINLINE constexpr ScalarType const* Data() const { return Elements; }
		AEON_FORCEINLINE constexpr ScalarType* Data() { return AEON_INVOKE_CONST_OVERLOAD(Data); }

		static constexpr GMatrix Zero() { return GMatrix{ TZeroMatrix::Tag }; }
		static constexpr GMatrix Identity() { return GMatrix{ TIdentityMatrix::Tag }; }

		FloatType Elements[ElementCount];

	private:
		enum struct EComputeUsingRowReductionResult
		{
			LUMatrix = 1,
			LinearSystem = 2,
		//	Inverse = 3,
		};

		struct DMatrixRowReduction
		{
			GMatrix Matrix;
			GMatrix ReducedMatrix;
			szint PermutationCount;
		};

		template <EComputeUsingRowReductionResult ResultValue, EComputeUsingPartialPivoting UsePartialPivotingValue,
				  szint ReducedColumnCountValue>
		constexpr DMatrixRowReduction ComputeUsingRowReduction_Internal(GMatrix<FloatType, RowCount, ReducedColumnCountValue> const& InitMat) const
		{
			GMatrix Mat{ *this };
			GMatrix<FloatType, RowCount, ReducedColumnCountValue> ReducedMat{ InitMat };
			szint PermCount = 0;

			// Convert matrix to echelon form

			for (szint ReductionIdx = 0; ReductionIdx < RowCount - 1; ++ReductionIdx)
			{
				FloatType Pivot = FloatType(0);

				if constexpr (UsePartialPivotingValue)
				{
					// Apply partial pivoting

					// Find row with absolute maximum element in the current column (ReductionIdx)

					FloatType PivotAbs = FloatType(0);
					szint PivotIdx = ReductionIdx;

					for (szint RowIdx = ReductionIdx; RowIdx < RowCount; ++RowIdx)
					{
						FloatType CurrPivot = Mat[RowIdx][ReductionIdx];
						FloatType CurrPivotAbs = AeonMath::AbsUnsafe(CurrPivot);

						if (PivotAbs < CurrPivotAbs)
						{
							Pivot = CurrPivot;
							PivotAbs = CurrPivotAbs;
							PivotIdx = RowIdx;
						}
					}

					// Ensure pivot is not null
					if (AeonMath::IsApproxZero(Pivot))
					{
						// Make this ill-formed in constexpr contexts
						AEON_CONSTEVAL_ASSERT(false);

						return { GMatrix{ TZeroMatrix::Tag }, GMatrix{ TIdentityMatrix::Tag }, 0_sz };
					}

					// Pivot rows
					if (PivotIdx != ReductionIdx)
					{
						SwapElements_Internal<ColumnCount>(Mat[ReductionIdx].Elements, Mat[PivotIdx].Elements);
						SwapElements_Internal<ReducedColumnCountValue>(ReducedMat[ReductionIdx].Elements, ReducedMat[PivotIdx].Elements);

						++PermCount;
					}
				}
				else
				{
					// Use diagonal element as pivot

					Pivot = Mat[ReductionIdx][ReductionIdx];
				}

				// Apply Gaussian elimination to remaining rows

				for (szint RowIdx = ReductionIdx + 1; RowIdx < RowCount; ++RowIdx)
				{
					FloatType Factor = Mat[RowIdx][ReductionIdx] / Pivot;
					szint InitColumnIdx = 0;

					// NOTE If we want the LU (or LUP) factorization, 
					// then overwrite the (would-be) zeroed element with the lower matrix's corresponding element,
					// which is the factor, and start reducing from the following column instead, 
					// instead of reducing all elements in that row.
					if constexpr (ResultValue == EComputeUsingRowReductionResult::LUMatrix)
					{
						Mat[RowIdx][ReductionIdx] = Factor;

						InitColumnIdx = ReductionIdx + 1;
					}

					for (szint ColumnIdx = InitColumnIdx; ColumnIdx < ColumnCount; ++ColumnIdx)
					{
						Mat[RowIdx][ColumnIdx] -= Factor * Mat[ReductionIdx][ColumnIdx];
					}

					if constexpr (ResultValue == EComputeUsingRowReductionResult::LinearSystem)
					{
						for (szint ColumnIdx = 0; ColumnIdx < ReducedColumnCountValue; ++ColumnIdx)
						{
							ReducedMat[RowIdx][ColumnIdx] -= Factor * ReducedMat[ReductionIdx][ColumnIdx];
						}
					}
				}
			}

			if constexpr (ResultValue == EComputeUsingRowReductionResult::LinearSystem)
			{
				// Apply Gaussian elimination elimination to all rows in reverse order
				
				for (szint ReductionIdx = RowCount - 1; ReductionIdx > 0; --ReductionIdx)
				{
					FloatType Pivot = Mat[ReductionIdx][ReductionIdx];

					for (szint InitRowIdx = ReductionIdx; InitRowIdx > 0; --InitRowIdx)
					{
						szint RowIdx = InitRowIdx - 1;
						FloatType Factor = Mat[RowIdx][ReductionIdx] / Pivot;

						for (szint ColumnIdx = 0; ColumnIdx < ColumnCount; ++ColumnIdx)
						{
							Mat[RowIdx][ColumnIdx] -= Factor * Mat[ReductionIdx][ColumnIdx];
						}
						for (szint ColumnIdx = 0; ColumnIdx < ReducedColumnCountValue; ++ColumnIdx)
						{
							ReducedMat[RowIdx][ColumnIdx] -= Factor * ReducedMat[ReductionIdx][ColumnIdx];
						}
					}
				}

				// Note that at the end of this step,
				// Mat would be the identity matrix (we skip dividing the diagonal by itself).

				for (szint RowIdx = 0; RowIdx < RowCount; ++RowIdx)
				{
					FloatType Factor = Mat[RowIdx][RowIdx];

					for (szint ColumnIdx = 0; ColumnIdx < ReducedColumnCountValue; ++ColumnIdx)
					{
						ReducedMat[RowIdx][ColumnIdx] /= Factor;
					}
				}
			}

			return { Mat, ReducedMat, PermCount };
		}

		template <szint ResultColumnCountValue>
		GMatrix<FloatType, RowCount, ResultColumnCountValue> SolveAsLUMatrix_Internal(GMatrix<FloatType, RowCount, ResultColumnCountValue> ResultMat) const
		{
			GMatrix const& ThisMat = (*this);

			// Similar step to Gaussian elimination

			for (szint RowIdx = 0; RowIdx < RowCount; ++RowIdx)
			{
				for (szint Idx = 0; Idx < RowIdx; ++Idx)
				{
					for (szint ColumnIdx = 0; ColumnIdx < ResultColumnCountValue; ++ColumnIdx)
					{
						ResultMat[RowIdx][ColumnIdx] -= ThisMat[RowIdx][Idx] * ResultMat[Idx][ColumnIdx];
					}
				}
			}

			// Similar step to Gauss-Jordan elimination

			for (szint InitRowIdx = RowCount; InitRowIdx > 0; --InitRowIdx)
			{
				szint RowIdx = InitRowIdx - 1;
				for (szint Idx = RowIdx + 1; Idx < RowCount; ++Idx)
				{
					for (szint ColumnIdx = 0; ColumnIdx < ResultColumnCountValue; ++ColumnIdx)
					{
						ResultMat[RowIdx][ColumnIdx] -= ThisMat[RowIdx][Idx] * ResultMat[Idx][ColumnIdx];
					}
				}

				// Divide by corresponding diagonal element

				for (szint ColumnIdx = 0; ColumnIdx < ResultColumnCountValue; ++ColumnIdx)
				{
					ResultMat[RowIdx][ColumnIdx] /= ThisMat[RowIdx][RowIdx];
				}
			}

			return ResultMat;
		}

#	if 0
		template <szint BaseRowCountValue, szint BaseRowIdx = 0>
		constexpr void ComputePartialPivot_Internal(szint (&Permutation)[BaseRowCountValue]) const
		{
			if constexpr (BaseRowIdx < BaseRowCountValue - 1)
			{
				FloatType PivotAbs = FloatType(0);
				szint PivotIdx = 0;

				for (szint RowIdx = 0; RowIdx < RowCount; ++RowIdx)
				{
					FloatType CurrPivotAbs = AeonMath::AbsUnsafe(Elements[RowIdx * ColumnCount]);

					if (PivotAbs < CurrPivotAbs)
					{
						PivotAbs = CurrPivotAbs;
						PivotIdx = RowIdx;
					}
				}

				Permutation[BaseRowIdx] = PivotIdx;

				Submatrix(PivotIdx, 0).template ComputePartialPivot_Internal<BaseRowCountValue, BaseRowIdx + 1>(Permutation);
			}
			else
			{
				Permutation[BaseRowIdx] = 0;
			}
		}
#	endif

		AEON_FORCEINLINE static constexpr void CopyElements_Internal(FloatType* DstPtr, FloatType const* SrcPtr, szint ElemCount)
		{
			if_consteval
			{
				for (szint Index = 0; Index < ElemCount; ++Index)
				{
					*DstPtr++ = *SrcPtr++;
				}
			}
			else
			{
				AeonMemory::MemCopy(DstPtr, SrcPtr, Aeon::SizeOf(FloatType, ElemCount));
			}
		}

		template <szint ElemCountValue>
		AEON_FORCEINLINE static constexpr void SwapElements_Internal(FloatType* ElemPtr1, FloatType* ElemPtr2)
		{
			FloatType TempArray[ElemCountValue];

			CopyElements_Internal(TempArray, ElemPtr1, ElemCountValue);
			CopyElements_Internal(ElemPtr1, ElemPtr2, ElemCountValue);
			CopyElements_Internal(ElemPtr2, TempArray, ElemCountValue);
		}

		template <CFloatingPoint OtherFloatType, szint OtherRowCountValue, szint OtherColumnCountValue>
		friend class GMatrix;
	};

	template <CFloatingPoint FloatType>
	using GMatrix2 = GMatrix<FloatType, 2, 2>;
	template <CFloatingPoint FloatType>
	using GMatrix3 = GMatrix<FloatType, 3, 3>;
	template <CFloatingPoint FloatType>
	using GMatrix4 = GMatrix<FloatType, 4, 4>;

	using SFP32Matrix2 = GMatrix2<float32>;
	using SFP32Matrix3 = GMatrix3<float32>;
	using SFP32Matrix4 = GMatrix4<float32>;

	using SFP64Matrix2 = GMatrix2<float64>;
	using SFP64Matrix3 = GMatrix3<float64>;
	using SFP64Matrix4 = GMatrix4<float64>;

	using SMatrix2 = AEON_TOKEN_CONCAT(SFP, AEON_MATH_DEFAULT_WORD_SIZE, Matrix2);
	using SMatrix3 = AEON_TOKEN_CONCAT(SFP, AEON_MATH_DEFAULT_WORD_SIZE, Matrix3);
	using SMatrix4 = AEON_TOKEN_CONCAT(SFP, AEON_MATH_DEFAULT_WORD_SIZE, Matrix4);
}

#pragma pack(pop)

#endif