#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_MATH_ROTATION
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_MATH_ROTATION

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Math/Vector.hpp"

#pragma pack(push, 4)

namespace Aeon
{
	template <CFloatingPoint FloatType>
	class GEulerRotation : public ZzInternal::GVectorOps<GEulerRotation<FloatType>, FloatType, 3>
	{
		using Base = ZzInternal::GVectorOps<GEulerRotation<FloatType>, FloatType, 3>;

	public:
		using ScalarType = FloatType;
		static constexpr szint ElementCount = 3;

		GEulerRotation() = default;
		constexpr GEulerRotation(FloatType NewRoll, FloatType NewPitch, FloatType NewYaw)
			: Roll{ NewRoll }
			, Pitch{ NewPitch }
			, Yaw{ NewYaw }
		{
		//	Unwind();
		}

		explicit constexpr GEulerRotation(GVector3<FloatType> const& NewVector)
			: Roll{ NewVector.X }
			, Pitch{ NewVector.Y }
			, Yaw{ NewVector.Z }
		{
		//	Unwind();
		}

		using Base::operator+=;
		using Base::operator-=;
		using Base::operator*=;
		using Base::operator/=;

		using Base::operator+;
		using Base::operator-;
		using Base::operator*;
		using Base::operator/;

	//	using Base::Dot;
	//	using Base::Cross;

		using Base::Negation;
		using Base::Reciprocal;

		using Base::IsApproxEqual;
		constexpr bool IsApproxIdentity(FloatType Tolerance = TNumericProperties<FloatType>::Epsilon) const
		{
			return Base::IsApproxZero(Tolerance);
		}

		constexpr GEulerRotation& Unwind()
		{
			constexpr FloatType PitchLimit = FloatType(180.0);
			constexpr FloatType RollYawLimit = FloatType(360.0);

			auto [UnwoundPitch, PitchWinding] = AeonMath::RemQuo<FloatType>(Pitch, PitchLimit);

			bool IsPitchFlipped = bool(PitchWinding & 0b01);
			FloatType ClampedWinding = FloatType(PitchWinding & 0b11) * PitchLimit;

			Pitch = IsPitchFlipped ? -UnwoundPitch : +UnwoundPitch;

			Roll = AeonMath::Rem<FloatType>(Roll + ClampedWinding, RollYawLimit);
			Yaw = AeonMath::Rem<FloatType>(Yaw + ClampedWinding, RollYawLimit);

			return *this;
		}

		constexpr GEulerRotation AsUnwound() const
		{
			return GEulerRotation{ *this }.Unwind();
		}

		using Base::operator[];

		explicit constexpr operator GVector3<FloatType>() const { return GVector3{ Roll, Pitch, Yaw }; }

		AEON_FORCEINLINE constexpr ScalarType const* Data() const { return &Roll; }
		AEON_FORCEINLINE constexpr ScalarType* Data() { return AEON_INVOKE_CONST_OVERLOAD(Data); }

		static constexpr GEulerRotation Identity() { return GEulerRotation{ FloatType(0.0), FloatType(0.0), FloatType(0.0) }; }

		static constexpr GEulerRotation CW90() { return GEulerRotation{ FloatType(+90.0), FloatType(0.0), FloatType(0.0) }; }
		static constexpr GEulerRotation CCW90() { return GEulerRotation{ FloatType(-90.0), FloatType(0.0), FloatType(0.0) }; }

		static constexpr GEulerRotation Down90() { return GEulerRotation{ FloatType(0.0), FloatType(+90.0), FloatType(0.0) }; }
		static constexpr GEulerRotation Up90() { return GEulerRotation{ FloatType(0.0), FloatType(-90.0), FloatType(0.0) }; }

		static constexpr GEulerRotation Left90() { return GEulerRotation{ FloatType(0.0), FloatType(0.0), FloatType(+90.0) }; }
		static constexpr GEulerRotation Right90() { return GEulerRotation{ FloatType(0.0), FloatType(0.0), FloatType(-90.0) }; }

		static constexpr GEulerRotation NaN() { return GEulerRotation{ AeonMath::MakeNaN<FloatType>(),
																	   AeonMath::MakeNaN<FloatType>(),
																	   AeonMath::MakeNaN<FloatType>() }; }

		// This class uses Davenport chained rotations and specifically one of the Tait-Bryan conventions,
		// where a rotation is applied in the order z -> y'-> x" (intrinsic rotations), with x = roll, y = pitch, and z = yaw,
		// and with the prime marks referring to the respective axis of the (body-local) rotating coordinate system XYZ,
		// as opposed to the fixed coordinate system, after each rotation about one of the axes.
		// https://en.wikipedia.org/wiki/Davenport_chained_rotations#Tait%E2%80%93Bryan_chained_rotations

		FloatType Roll, Pitch, Yaw;
	};

	using SFP32EulerRotation = GEulerRotation<float32>;
	using SFP64EulerRotation = GEulerRotation<float64>;

	using SEulerRotation = AEON_TOKEN_CONCAT(SFP, AEON_MATH_DEFAULT_WORD_SIZE, EulerRotation);

	// TODO maybe implement GEulerAxis for axis-angle representation
}

#pragma pack(pop)

#endif