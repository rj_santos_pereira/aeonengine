#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_MATH_STATICINDEX
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_MATH_STATICINDEX

#include "Aeon/Framework/Core.hpp"

namespace Aeon
{
	namespace ZzInternal
	{
		template <class DigitCharType>
		struct TConvertDigitCharToInt;
		template <char DigitCharValue>
		struct TConvertDigitCharToInt<TObject<DigitCharValue>>
		{
			static_assert('0' <= DigitCharValue && DigitCharValue <= '9');

			using Type = TObject<szint(DigitCharValue - '0')>;
		};

		template <class Digit1Type, class Digit2Type>
		struct TAddDigits;
		template <szint Digit1Value, szint Digit2Value>
		struct TAddDigits<TObject<Digit1Value>, TObject<Digit2Value>>
		{
			using Type = TObject<szint(Digit1Value + Digit2Value)>;
		};

		template <class Digit1Type, class Digit2Type>
		struct TMulDigits;
		template <szint Digit1Value, szint Digit2Value>
		struct TMulDigits<TObject<Digit1Value>, TObject<Digit2Value>>
		{
			using Type = TObject<szint(Digit1Value * Digit2Value)>;
		};

		template <class DigitType>
		struct TDecimalPowDigits;
		template <szint DigitValue>
		struct TDecimalPowDigits<TObject<DigitValue>>
		{
			using Type = TObject<AeonMath::Pow(10_sz, DigitValue)>;
		};

		template <CTypePack DigitCharsPackType>
		struct TMakeIndexUsingDigits
		{
			// Convert the pack of digit characters to the respective integer values:
			//  DN ... D3, D2, D1
			using DigitsPack = typename DigitCharsPackType::template Map<TConvertDigitCharToInt>;

			static constexpr szint Value
				// Create the indexes of each digit:
				//  0, 1, 2 ... N
				= TMakeIndexSequence<DigitsPack::Count>::AsPack
				// Reverse the indexes:
				//  N ... 2, 1, 0
					::Reverse
				// Compute the powers-of-ten using the indexes:
				//  10^(N-1) ... 100, 10, 1
					::template Map<TDecimalPowDigits>
				// Multiply the powers by each digit:
				//  DN*(10^(N-1)) ... D3*100, D2*10, D1*1
					::template Map<TMulDigits, DigitsPack>
				// Sum all the factors:
				//  DN*(10^(N-1)) + ... + D3*100 + D2*10 + D1*1 + 0
					::template Fold<TAddDigits, TObject<0_sz>>
					::Value;
		};
	}

	template <szint IndexValue>
	using TStaticIndex = TObject<IndexValue>;

	inline namespace Literals
	{
		template <char ... DigitValues>
		auto operator ""_sdx()
		{
			constexpr szint Index = ZzInternal::TMakeIndexUsingDigits<TTypePack<TObject<DigitValues> ...>>::Value;
			return TStaticIndex<Index>{};
		}
	}
}

#endif