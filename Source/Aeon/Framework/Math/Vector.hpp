#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_MATH_VECTOR
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_MATH_VECTOR

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Math/VectorCommon.hpp"

#pragma pack(push, 1)

namespace Aeon
{
	template <CNumeric NumType>
	class GVector2;

	template <CInteger IntType>
	class GVector2<IntType> : public ZzInternal::GVectorOps<GVector2<IntType>, IntType, 2>
	{
		using Base = ZzInternal::GVectorOps<GVector2<IntType>, IntType, 2>;

	public:
		using ScalarType = IntType;
		static constexpr szint ElementCount = 2;

		GVector2() = default;
		constexpr GVector2(IntType NewX, IntType NewY)
			: X{ NewX }
			, Y{ NewY }
		{
		}

		using Base::operator+=;
		using Base::operator-=;
		using Base::operator*=;
		using Base::operator/=;
		using Base::operator%=;

		using Base::operator+;
		using Base::operator-;
		using Base::operator*;
		using Base::operator/;
		using Base::operator%;

		using Base::Dot;

		using Base::Negation;

		using Base::LengthSquared;
		using Base::Length;

		using Base::IsEqual;

		using Base::operator[];

		template <CNumeric OtherNumType>
		explicit constexpr operator GVector2<OtherNumType>() const
		{
			return { OtherNumType(X), OtherNumType(Y) };
		}

		AEON_FORCEINLINE constexpr ScalarType const* Data() const { return &X; }
		AEON_FORCEINLINE constexpr ScalarType* Data() { return AEON_INVOKE_CONST_OVERLOAD(Data); }

		static constexpr GVector2 Zero() { return GVector2{ IntType(0), IntType(0) }; }
		static constexpr GVector2 One() { return GVector2{ IntType(1), IntType(1) }; }

		static constexpr GVector2 XAxis() { return GVector2{ IntType(1), IntType(0) }; }
		static constexpr GVector2 YAxis() { return GVector2{ IntType(0), IntType(1) }; }

		IntType X, Y;
	};

	template <CFloatingPoint FloatType>
	class GVector2<FloatType> : public ZzInternal::GVectorOps<GVector2<FloatType>, FloatType, 2>
	{
		using Base = ZzInternal::GVectorOps<GVector2<FloatType>, FloatType, 2>;

	public:
		using ScalarType = FloatType;
		static constexpr szint ElementCount = 2;

		GVector2() = default;
		constexpr GVector2(FloatType NewX, FloatType NewY)
			: X{ NewX }
			, Y{ NewY }
		{
		}

		using Base::operator+=;
		using Base::operator-=;
		using Base::operator*=;
		using Base::operator/=;

		using Base::operator+;
		using Base::operator-;
		using Base::operator*;
		using Base::operator/;

		using Base::Normalize;
		using Base::NormalizeUnsafe;

		using Base::Dot;

		using Base::Negation;
		using Base::Reciprocal;
		using Base::AsNormalized;

		using Base::LengthSquared;
		using Base::Length;

		using Base::IsApproxEqual;
		using Base::IsApproxZero;
		using Base::IsApproxUnit;

		using Base::operator[];

		template <CNumeric OtherNumType>
		explicit constexpr operator GVector2<OtherNumType>() const
		{
			return { OtherNumType(X), OtherNumType(Y) };
		}

		AEON_FORCEINLINE constexpr ScalarType const* Data() const { return &X; }
		AEON_FORCEINLINE constexpr ScalarType* Data() { return AEON_INVOKE_CONST_OVERLOAD(Data); }

		static constexpr GVector2 Zero() { return GVector2{ FloatType(0.0), FloatType(0.0) }; }
		static constexpr GVector2 One() { return GVector2{ FloatType(1.0), FloatType(1.0) }; }

		static constexpr GVector2 XAxis() { return GVector2{ FloatType(1.0), FloatType(0.0) }; }
		static constexpr GVector2 YAxis() { return GVector2{ FloatType(0.0), FloatType(1.0) }; }

		// CHECKME Should we add defaulted arguments for IsSignaling and Payload?
		static constexpr GVector2 NaN() { return GVector2{ AeonMath::MakeNaN<FloatType>(),
														   AeonMath::MakeNaN<FloatType>() }; }

		FloatType X, Y;
	};

	template <CNumeric NumType>
	class GVector3;

	template <CInteger IntType>
	class GVector3<IntType> : public ZzInternal::GVectorOps<GVector3<IntType>, IntType, 3>
	{
		using Base = ZzInternal::GVectorOps<GVector3<IntType>, IntType, 3>;

	public:
		using ScalarType = IntType;
		static constexpr szint ElementCount = 3;

		GVector3() = default;
		constexpr GVector3(IntType NewX, IntType NewY, IntType NewZ)
			: X{ NewX }
			, Y{ NewY }
			, Z{ NewZ }
		{
		}

		constexpr GVector3(GVector2<IntType> const& NewVector, IntType NewZ = IntType(0))
			: X{ NewVector.X }
			, Y{ NewVector.Y }
			, Z{ NewZ }
		{
		}

		using Base::operator+=;
		using Base::operator-=;
		using Base::operator*=;
		using Base::operator/=;
		using Base::operator%=;

		using Base::operator+;
		using Base::operator-;
		using Base::operator*;
		using Base::operator/;
		using Base::operator%;

		using Base::Dot;
		using Base::Cross;

		using Base::Negation;

		using Base::LengthSquared;
		using Base::Length;

		using Base::IsEqual;

		using Base::operator[];

		explicit constexpr operator GVector2<IntType>() const { return GVector2<IntType>{ X, Y }; }

		template <CNumeric OtherNumType>
		explicit constexpr operator GVector3<OtherNumType>() const
		{
			return { OtherNumType(X), OtherNumType(Y), OtherNumType(Z) };
		}

		AEON_FORCEINLINE constexpr ScalarType const* Data() const { return &X; }
		AEON_FORCEINLINE constexpr ScalarType* Data() { return AEON_INVOKE_CONST_OVERLOAD(Data); }

		static constexpr GVector3 Zero() { return GVector3{ IntType(0), IntType(0), IntType(0) }; }
		static constexpr GVector3 One() { return GVector3{ IntType(1), IntType(1), IntType(1) }; }

		static constexpr GVector3 XAxis() { return GVector3{ IntType(1), IntType(0), IntType(0) }; }
		static constexpr GVector3 YAxis() { return GVector3{ IntType(0), IntType(1), IntType(0) }; }
		static constexpr GVector3 ZAxis() { return GVector3{ IntType(0), IntType(0), IntType(1) }; }

		IntType X, Y, Z;
	};

	template <CFloatingPoint FloatType>
	class GVector3<FloatType> : public ZzInternal::GVectorOps<GVector3<FloatType>, FloatType, 3>
	{
		using Base = ZzInternal::GVectorOps<GVector3<FloatType>, FloatType, 3>;

	public:
		struct DOrthonormalBasis
		{
			GVector3<FloatType> E1;
			GVector3<FloatType> E2;
			GVector3<FloatType> E3;
		};

		using ScalarType = FloatType;
		static constexpr szint ElementCount = 3;

		GVector3() = default;
		constexpr GVector3(FloatType NewX, FloatType NewY, FloatType NewZ)
			: X{ NewX }
			, Y{ NewY }
			, Z{ NewZ }
		{
		}

		constexpr GVector3(GVector2<FloatType> const& NewVector, FloatType NewZ = FloatType(0))
			: X{ NewVector.X }
			, Y{ NewVector.Y }
			, Z{ NewZ }
		{
		}

		using Base::operator+=;
		using Base::operator-=;
		using Base::operator*=;
		using Base::operator/=;

		using Base::operator+;
		using Base::operator-;
		using Base::operator*;
		using Base::operator/;

		using Base::Dot;
		using Base::Cross;

		using Base::LengthSquared;
		using Base::Length;

		using Base::Negation;
		using Base::Reciprocal;

		using Base::Normalize;
		using Base::NormalizeUnsafe;

		using Base::AsNormalized;

		using Base::IsApproxEqual;
		using Base::IsApproxZero;
		using Base::IsApproxUnit;

		DOrthonormalBasis MakeOrthonormalBasis() const
		{
			// Gram-Schmidt algorithm
			// https://en.wikipedia.org/wiki/Gram-Schmidt_process
			// https://math.stackexchange.com/questions/1714365/finding-an-orthonormal-basis-with-only-one-vector

			DOrthonormalBasis Basis;

			Basis.E1 = (*this);
			Basis.E2 = Basis.E1.IsApproxEqual(GVector3<FloatType>::XAxis()) ? GVector3<FloatType>::ZAxis() : GVector3<FloatType>::XAxis();
			Basis.E3 = Basis.E1.IsApproxEqual(GVector3<FloatType>::YAxis()) ? GVector3<FloatType>::ZAxis() : GVector3<FloatType>::YAxis();

			Basis.E2 -= Basis.E1 * GVector3<FloatType>::Dot(Basis.E1, Basis.E2);
			Basis.E2.Normalize();

			Basis.E3 -= Basis.E1 * GVector3<FloatType>::Dot(Basis.E1, Basis.E3)
					  + Basis.E2 * GVector3<FloatType>::Dot(Basis.E2, Basis.E3);
			Basis.E3.Normalize();

			return Basis;
		}

		GVector3<FloatType> MakeApproxOrthonormalVector() const
		{
			FloatType AbsX = AeonMath::Abs(X);
			FloatType AbsY = AeonMath::Abs(Y);
			FloatType AbsZ = AeonMath::Abs(Z);

			GVector3<FloatType> Vector
				= (AbsX < AbsY)
				  ? (AbsX < AbsZ)
					? GVector3<FloatType>::XAxis()
					: GVector3<FloatType>::ZAxis()
				  : (AbsY < AbsZ)
					? GVector3<FloatType>::YAxis()
					: GVector3<FloatType>::ZAxis();

			return Vector;
		}

		GVector3<FloatType> MakeOrthonormalVector() const
		{
			GVector3<FloatType> const& ThisVector = (*this);
			GVector3<FloatType> Vector = MakeApproxOrthonormalVector();

			Vector -= ThisVector * GVector3<FloatType>::Dot(ThisVector, Vector);
			Vector.Normalize();

			return Vector;
		}

		using Base::operator[];

		explicit constexpr operator GVector2<FloatType>() const { return GVector2<FloatType>{ X, Y }; }

		template <CNumeric OtherNumType>
		explicit constexpr operator GVector3<OtherNumType>() const
		{
			return { OtherNumType(X), OtherNumType(Y), OtherNumType(Z) };
		}

		AEON_FORCEINLINE constexpr ScalarType const* Data() const { return &X; }
		AEON_FORCEINLINE constexpr ScalarType* Data() { return AEON_INVOKE_CONST_OVERLOAD(Data); }

		static constexpr GVector3 Zero() { return GVector3{ FloatType(0.0), FloatType(0.0), FloatType(0.0) }; }
		static constexpr GVector3 One() { return GVector3{ FloatType(1.0), FloatType(1.0), FloatType(1.0) }; }

		static constexpr GVector3 XAxis() { return GVector3{ FloatType(1.0), FloatType(0.0), FloatType(0.0) }; }
		static constexpr GVector3 YAxis() { return GVector3{ FloatType(0.0), FloatType(1.0), FloatType(0.0) }; }
		static constexpr GVector3 ZAxis() { return GVector3{ FloatType(0.0), FloatType(0.0), FloatType(1.0) }; }

		static constexpr GVector3 NaN() { return GVector3{ AeonMath::MakeNaN<FloatType>(),
		    											   AeonMath::MakeNaN<FloatType>(),
		    											   AeonMath::MakeNaN<FloatType>() }; }

		FloatType X, Y, Z;
	};

	template <CNumeric NumType>
	class GVector4;

	template <CInteger IntType>
	class GVector4<IntType> : public ZzInternal::GVectorOps<GVector4<IntType>, IntType, 4>
	{
		using Base = ZzInternal::GVectorOps<GVector4<IntType>, IntType, 4>;

	public:
		using ScalarType = IntType;
		static constexpr szint ElementCount = 4;

		GVector4() = default;
		constexpr GVector4(IntType NewX, IntType NewY, IntType NewZ, IntType NewW)
			: X{ NewX }
			, Y{ NewY }
			, Z{ NewZ }
			, W{ NewW }
		{
		}

		constexpr GVector4(GVector2<IntType> const& NewVector, IntType NewZ = IntType(0), IntType NewW = IntType(0))
			: X{ NewVector.X }
			, Y{ NewVector.Y }
			, Z{ NewZ }
			, W{ NewW }
		{
		}
		constexpr GVector4(GVector3<IntType> const& NewVector, IntType NewW = IntType(0))
			: X{ NewVector.X }
			, Y{ NewVector.Y }
			, Z{ NewVector.Z }
			, W{ NewW }
		{
		}

		using Base::operator+=;
		using Base::operator-=;
		using Base::operator*=;
		using Base::operator/=;
		using Base::operator%=;

		using Base::operator+;
		using Base::operator-;
		using Base::operator*;
		using Base::operator/;
		using Base::operator%;

		using Base::Dot;

		using Base::Negation;

		using Base::LengthSquared;
		using Base::Length;

		using Base::IsEqual;

		using Base::operator[];

		explicit constexpr operator GVector2<IntType>() const { return GVector2<IntType>{ X, Y }; }
		explicit constexpr operator GVector3<IntType>() const { return GVector3<IntType>{ X, Y, Z }; }

		template <CNumeric OtherNumType>
		explicit constexpr operator GVector4<OtherNumType>() const
		{
			return { OtherNumType(X), OtherNumType(Y), OtherNumType(Z), OtherNumType(W) };
		}

		AEON_FORCEINLINE constexpr ScalarType const* Data() const { return &X; }
		AEON_FORCEINLINE constexpr ScalarType* Data() { return AEON_INVOKE_CONST_OVERLOAD(Data); }

		static constexpr GVector4 Zero() { return GVector4{ IntType(0), IntType(0), IntType(0), IntType(0) }; }
		static constexpr GVector4 One() { return GVector4{ IntType(1), IntType(1), IntType(1), IntType(1) }; }

		static constexpr GVector4 XAxis() { return GVector4{ IntType(1), IntType(0), IntType(0), IntType(0) }; }
		static constexpr GVector4 YAxis() { return GVector4{ IntType(0), IntType(1), IntType(0), IntType(0) }; }
		static constexpr GVector4 ZAxis() { return GVector4{ IntType(0), IntType(0), IntType(1), IntType(0) }; }
		static constexpr GVector4 WAxis() { return GVector4{ IntType(0), IntType(0), IntType(0), IntType(1) }; }

		IntType X, Y, Z, W;
	};

	template <CFloatingPoint FloatType>
	class GVector4<FloatType> : public ZzInternal::GVectorOps<GVector4<FloatType>, FloatType, 4>
	{
		using Base = ZzInternal::GVectorOps<GVector4<FloatType>, FloatType, 4>;

	public:
		using ScalarType = FloatType;
		static constexpr szint ElementCount = 4;

		GVector4() = default;
		constexpr GVector4(FloatType NewX, FloatType NewY, FloatType NewZ, FloatType NewW)
			: X{ NewX }
			, Y{ NewY }
			, Z{ NewZ }
			, W{ NewW }
		{
		}

		constexpr GVector4(GVector2<FloatType> const& NewVector, FloatType NewZ = FloatType(0), FloatType NewW = FloatType(0))
			: X{ NewVector.X }
			, Y{ NewVector.Y }
			, Z{ NewZ }
			, W{ NewW }
		{
		}
		constexpr GVector4(GVector3<FloatType> const& NewVector, FloatType NewW = FloatType(0))
			: X{ NewVector.X }
			, Y{ NewVector.Y }
			, Z{ NewVector.Z }
			, W{ NewW }
		{
		}

		using Base::operator+=;
		using Base::operator-=;
		using Base::operator*=;
		using Base::operator/=;

		using Base::operator+;
		using Base::operator-;
		using Base::operator*;
		using Base::operator/;

		using Base::Normalize;
		using Base::NormalizeUnsafe;

		using Base::Dot;

		using Base::Negation;
		using Base::Reciprocal;
		using Base::AsNormalized;

		using Base::LengthSquared;
		using Base::Length;

		using Base::IsApproxEqual;
		using Base::IsApproxZero;
		using Base::IsApproxUnit;

		using Base::operator[];

		explicit constexpr operator GVector2<FloatType>() const { return GVector2<FloatType>{ X, Y }; }
		explicit constexpr operator GVector3<FloatType>() const { return GVector3<FloatType>{ X, Y, Z }; }

		template <CNumeric OtherNumType>
		explicit constexpr operator GVector4<OtherNumType>() const
		{
			return { OtherNumType(X), OtherNumType(Y), OtherNumType(Z), OtherNumType(W) };
		}

		AEON_FORCEINLINE constexpr ScalarType const* Data() const { return &X; }
		AEON_FORCEINLINE constexpr ScalarType* Data() { return AEON_INVOKE_CONST_OVERLOAD(Data); }

		static constexpr GVector4 Zero() { return GVector4{ FloatType(0.0), FloatType(0.0), FloatType(0.0), FloatType(0.0) }; }
		static constexpr GVector4 One() { return GVector4{ FloatType(1.0), FloatType(1.0), FloatType(1.0), FloatType(1.0) }; }

		static constexpr GVector4 XAxis() { return GVector4{ FloatType(1.0), FloatType(0.0), FloatType(0.0), FloatType(0.0) }; }
		static constexpr GVector4 YAxis() { return GVector4{ FloatType(0.0), FloatType(1.0), FloatType(0.0), FloatType(0.0) }; }
		static constexpr GVector4 ZAxis() { return GVector4{ FloatType(0.0), FloatType(0.0), FloatType(1.0), FloatType(0.0) }; }
		static constexpr GVector4 WAxis() { return GVector4{ FloatType(0.0), FloatType(0.0), FloatType(0.0), FloatType(1.0) }; }

		static constexpr GVector4 NaN() { return GVector4{ AeonMath::MakeNaN<FloatType>(),
														   AeonMath::MakeNaN<FloatType>(),
														   AeonMath::MakeNaN<FloatType>(),
														   AeonMath::MakeNaN<FloatType>() }; }

		FloatType X, Y, Z, W;
	};

	using SI32Vector2 = GVector2<int32>;
	using SI32Vector3 = GVector3<int32>;
	using SI32Vector4 = GVector4<int32>;

	using SI64Vector2 = GVector2<int64>;
	using SI64Vector3 = GVector3<int64>;
	using SI64Vector4 = GVector4<int64>;

	using SFP32Vector2 = GVector2<float32>;
	using SFP32Vector3 = GVector3<float32>;
	using SFP32Vector4 = GVector4<float32>;

	using SFP64Vector2 = GVector2<float64>;
	using SFP64Vector3 = GVector3<float64>;
	using SFP64Vector4 = GVector4<float64>;

	using SIntVector2 = AEON_TOKEN_CONCAT(SI, AEON_MATH_DEFAULT_WORD_SIZE, Vector2);
	using SIntVector3 = AEON_TOKEN_CONCAT(SI, AEON_MATH_DEFAULT_WORD_SIZE, Vector3);
	using SIntVector4 = AEON_TOKEN_CONCAT(SI, AEON_MATH_DEFAULT_WORD_SIZE, Vector4);

	using SVector2 = AEON_TOKEN_CONCAT(SFP, AEON_MATH_DEFAULT_WORD_SIZE, Vector2);
	using SVector3 = AEON_TOKEN_CONCAT(SFP, AEON_MATH_DEFAULT_WORD_SIZE, Vector3);
	using SVector4 = AEON_TOKEN_CONCAT(SFP, AEON_MATH_DEFAULT_WORD_SIZE, Vector4);
}

#pragma pack(pop)

#endif
