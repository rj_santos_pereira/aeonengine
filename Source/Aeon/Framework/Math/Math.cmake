add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Random)

target_sources(${PROJECT_NAME}
	PUBLIC
		${CMAKE_CURRENT_SOURCE_DIR}/VectorCommon.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/Vector.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/Rotation.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/Quaternion.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/Hyperplane.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/Matrix.hpp
#		${CMAKE_CURRENT_SOURCE_DIR}/Transform.hpp
#	PRIVATE
#		${CMAKE_CURRENT_SOURCE_DIR}/
	)