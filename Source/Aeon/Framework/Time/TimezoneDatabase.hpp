#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_TIME_TIMEZONEDATABASE
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_TIME_TIMEZONEDATABASE

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Time/Timezone.hpp"

namespace Aeon
{
	class STimezoneDatabase
	{
		explicit STimezoneDatabase() = default;

	public:
		static STimezoneDatabase& Access();

		STimezoneDatabase(STimezoneDatabase const&) = delete;
		STimezoneDatabase(STimezoneDatabase&&) = delete;

		STimezone FindTimezone(ESpecificTimezone SpecificZone) const;
		STimezone FindTimezone(SStringView Name) const;

		bool IsUpToDate() const;

		void Update();

		void CleanUp();

	private:
		stdtm::tzdb_list* GetTzDbList_Internal() const;
	};
}

#endif