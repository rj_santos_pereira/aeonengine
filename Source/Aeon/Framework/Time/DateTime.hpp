#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_TIME_DATETIME
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_TIME_DATETIME

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Time/Timezone.hpp"

#include "Aeon/Framework/Text/StringView.hpp"

namespace Aeon
{
	class SPreciseDateTime;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SDateTime

	class SDateTime
	{
		explicit SDateTime(stdtm::local_timepoint const& NewPoint, STimezone NewZone);

	public:
		SDateTime() = default;

		SDateTime(SDateTime const&) = default;
		SDateTime(SDateTime&&) = default;

		SDateTime& operator=(SDateTime Other);

		static SDateTime Now(ESpecificTimezone SpecificZone = ESpecificTimezone::Local);
		static SDateTime Now(SStringView TimezoneName);

		static SDateTime Epoch(ESpecificTimezone SpecificZone = ESpecificTimezone::Local);
		static SDateTime Epoch(SStringView TimezoneName);

		int64 Year() const;
		int64 Month() const;
		int64 Day() const;

		int64 Hours() const;
		int64 Minutes() const;
		int64 Seconds() const;

		float64 Subseconds() const;

		STimezone Timezone() const;

		bool IsValid() const;

		SPreciseDateTime AsPrecise() const;

		// NOTE This returns a sys_timepoint in local time (in reference to the passed Timezone), and not in system time
		stdtm::sys_timepoint AsNative() const;
		std::tm AsNativeLegacy() const;

		bool operator==(SDateTime const& Other) const;
		strong_ordering operator<=>(SDateTime const& Other) const;

		static constexpr float64 SubsecondsPrecision();

		friend void Swap(SDateTime& DateTime1, SDateTime& DateTime2);

	private:
		stdtm::date_desc Date;
		stdtm::time_desc Time;
		STimezone Zone;

		friend class STimePoint;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SPreciseDateTime

	// This type is simply used to represent subseconds in formatting functions
	class SPreciseDateTime : public SDateTime
	{
	public:
		SPreciseDateTime() = default;

		SPreciseDateTime(SPreciseDateTime const&) = default;
		SPreciseDateTime(SPreciseDateTime&&) = default;

		SPreciseDateTime(SDateTime const& DateTime);

		SPreciseDateTime& operator=(SPreciseDateTime Other);

	private:
		// Disallow calling this from here, as it would be redundant (equivalent to a call to a copy ctor)
		using SDateTime::AsPrecise;
	};
}

namespace ZZINTERNAL_AEON_FMT_NS
{
	template <Aeon::CCharacter CharType>
	struct formatter<Aeon::SDateTime, CharType> : ZZINTERNAL_AEON_FMT_NS::formatter<std::chrono::sys_seconds, CharType>
	{
		template <class ContextType>
		auto format(Aeon::SDateTime const& DateTime, ContextType& Context) const
		{
			auto SecondsTimePoint = std::chrono::floor<std::chrono::seconds>(DateTime.AsNative());
			return formatter<std::chrono::sys_seconds, CharType>::format(SecondsTimePoint, Context);
		}
	};

	template <Aeon::CCharacter CharType>
	struct formatter<Aeon::SPreciseDateTime, CharType> : ZZINTERNAL_AEON_FMT_NS::formatter<std::chrono::system_clock::time_point, CharType>
	{
		template <class ContextType>
		auto format(Aeon::SPreciseDateTime const& DateTime, ContextType& Context) const
		{
			auto SubsecondsTimePoint = DateTime.AsNative();
			return formatter<std::chrono::system_clock::time_point, CharType>::format(SubsecondsTimePoint, Context);
		}
	};
}

#endif