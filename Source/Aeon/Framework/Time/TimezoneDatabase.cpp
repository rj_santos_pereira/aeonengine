#include "Aeon/Framework/Time/TimezoneDatabase.hpp"

namespace Aeon
{
	STimezoneDatabase& STimezoneDatabase::Access()
	{
		static STimezoneDatabase Database;
		return Database;
	}

	STimezone STimezoneDatabase::FindTimezone(ESpecificTimezone SpecificZone) const
	{
		if (auto TzDbList = GetTzDbList_Internal())
		{
			try
			{
				switch (SpecificZone)
				{
					case ESpecificTimezone::Local:
						return { STimezone::DatabaseAccess::Tag, TzDbList->front().current_zone() };
					case ESpecificTimezone::UTC:
						return { STimezone::DatabaseAccess::Tag, TzDbList->front().locate_zone("UTC") };
				}
			}
			catch (std::runtime_error const& Exception)
			{
				constexpr char const* ReferenceName[2] = { "local", "'UTC'" };
				AEON_DEBUGGER_FORMAT_PRINT(Warn, "STimezoneDatabase: failed to find {} timezone. Reason: {}",
										   ReferenceName[uint8(SpecificZone)], Exception.what());
			}
		}
		return { STimezone::DatabaseAccess::Tag, nullptr };
	}
	STimezone STimezoneDatabase::FindTimezone(SStringView Name) const
	{
		if (auto TzDbList = GetTzDbList_Internal())
		{
			try
			{
				return { STimezone::DatabaseAccess::Tag, TzDbList->front().locate_zone(std::string_view{ Name }) };
			}
			catch (std::runtime_error const& Exception)
			{
				AEON_DEBUGGER_FORMAT_PRINT(Warn, "STimezoneDatabase: failed to find timezone '{}'. Reason: {}",
										   Name.CharBuffer(), Exception.what());
			}
		}
		return { STimezone::DatabaseAccess::Tag, nullptr };
	}

	bool STimezoneDatabase::IsUpToDate() const
	{
		auto TzDbList = GetTzDbList_Internal();
		return TzDbList && TzDbList->front().version == stdtm::remote_version();
	}

	void STimezoneDatabase::Update()
	{
		stdtm::reload_tzdb();
	}

	void STimezoneDatabase::CleanUp()
	{
		if (auto TzDbList = GetTzDbList_Internal())
		{
			TzDbList->erase_after(TzDbList->begin());
		}
	}

	stdtm::tzdb_list* STimezoneDatabase::GetTzDbList_Internal() const
	{
		stdtm::tzdb_list* TzDbList = nullptr;
		try
		{
			TzDbList = &stdtm::get_tzdb_list();
		}
		catch (std::runtime_error const& Exception)
		{
			AEON_DEBUGGER_FORMAT_PRINT(Error, "STimezoneDatabase init failed: {}", Exception.what());
		}
		return TzDbList;
	}

}
