#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_TIME_TIMEPOINT
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_TIME_TIMEPOINT

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Time/Timezone.hpp"

#include "Aeon/Framework/Text/StringView.hpp"

namespace Aeon
{
	class STimeSpan;
	class SDateTime;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// STimePoint

	class STimePoint
	{
		explicit STimePoint(stdtm::sys_timepoint const& NewPoint, STimezone NewZone);
		explicit STimePoint(stdtm::local_timepoint const& NewPoint, STimezone NewZone);

	public:
		STimePoint() = default;

		STimePoint(STimePoint const&) = default;
		STimePoint(STimePoint&&) = default;

		STimePoint& operator=(STimePoint Other);

		// TODO add ctor (or static func) to construct from date/time

		static STimePoint Now(ESpecificTimezone SpecificZone = ESpecificTimezone::Local);
		static STimePoint Now(SStringView TimezoneName);

		static STimePoint Epoch(ESpecificTimezone SpecificZone = ESpecificTimezone::Local);
		static STimePoint Epoch(SStringView TimezoneName);

		static STimePoint FromTimestamp(uint64 NewTimestamp, ESpecificTimezone SpecificZone = ESpecificTimezone::Local);

		STimePoint& operator+=(STimeSpan const& TimeSpan);
		STimePoint& operator-=(STimeSpan const& TimeSpan);

		STimePoint operator+(STimeSpan const& TimeSpan) const;
		STimePoint operator-(STimeSpan const& TimeSpan) const;

		int64 Year() const;
		int64 Month() const;
		int64 Day() const;

		int64 Hours() const;
		int64 Minutes() const;
		int64 Seconds() const;

		float64 Subseconds() const;

		uint64 ToTimestamp() const;

		STimezone Timezone() const;

		bool IsValid() const;

		SDateTime AsDateTime() const;

		stdtm::sys_timepoint AsNative() const;

		bool operator==(STimePoint const& Other) const;
		strong_ordering operator<=>(STimePoint const& Other) const;

		static constexpr float64 SubsecondsPrecision();

		friend void Swap(STimePoint& TimePoint1, STimePoint& TimePoint2);

	private:
		stdtm::date_desc MakeDateDescription_Internal() const;
		stdtm::time_desc MakeTimeDescription_Internal() const;

		stdtm::local_timepoint Point;
		STimezone Zone;

		friend class STimezone;
		friend class STimeSpan;
		friend class SFile;
	};
}

namespace ZZINTERNAL_AEON_FMT_NS
{
	template <Aeon::CCharacter CharType>
	struct formatter<Aeon::STimePoint, CharType> : ZZINTERNAL_AEON_FMT_NS::formatter<std::chrono::sys_seconds, CharType>
	{
		template <class FormatContextType>
		auto format(Aeon::STimePoint const& TimePoint, FormatContextType& Context) const
		{
			auto SecondsTimePoint = std::chrono::floor<std::chrono::seconds>(TimePoint.AsNative());
			return formatter<std::chrono::sys_seconds, CharType>::format(SecondsTimePoint, Context);
		}
	};
}

#endif