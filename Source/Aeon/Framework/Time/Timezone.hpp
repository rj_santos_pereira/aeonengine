#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_TIME_TIMEZONE
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_TIME_TIMEZONE

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Text/StringView.hpp"

namespace Aeon
{
	class STimePoint;

	enum struct ESpecificTimezone	: uint8
	{
		Local 						= 0b00,
		UTC							= 0b01,
	//	Named						= 0b10,
	};

	struct DTimezoneInfo
	{
		stdtm::sec_timepoint Start;
		stdtm::sec_timepoint End;
		stdtm::min_timespan UTCOffset;
		stdtm::min_timespan DSTOffset;
	};

	class STimezone
	{
	public:
		AEON_DECLARE_ACCESS_CLASS(DatabaseAccess, class STimezoneDatabase);

		STimezone();

		STimezone(DatabaseAccess, stdtm::tz const* NewZone);

		STimezone(STimezone const&) = default;
		STimezone(STimezone&&) = default;

		STimezone& operator=(STimezone Other);

		SStringView Name() const;

		DTimezoneInfo Info(stdtm::sys_timepoint const& TimePoint) const;

		STimePoint Convert(STimePoint const& TimePoint) const;

		stdtm::sys_timepoint ToSystem_Internal(stdtm::local_timepoint const& TimePoint) const;
		stdtm::local_timepoint ToLocal_Internal(stdtm::sys_timepoint const& TimePoint) const;

		bool IsValid() const;

		stdtm::tz const& AsNative() const;

		friend void Swap(STimezone& Timezone1, STimezone& Timezone2);

	private:
		stdtm::tz const* Zone;
	};
}

#endif