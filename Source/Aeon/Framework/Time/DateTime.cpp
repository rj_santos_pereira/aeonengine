#include "Aeon/Framework/Time/DateTime.hpp"

#include "Aeon/Framework/Time/TimePoint.hpp"

namespace Aeon
{
	SDateTime::SDateTime(stdtm::local_timepoint const& NewPoint, STimezone NewZone)
	{
		stdtm::sys_timespan SubsecondsSpan = NewPoint.time_since_epoch();
		stdtm::day_timespan DaysSpan = stdtm::floor<stdtm::day_timespan>(SubsecondsSpan);

		Date = stdtm::date_desc{ stdtm::day_timepoint{ DaysSpan } };
		Time = stdtm::time_desc{ SubsecondsSpan - DaysSpan };
		Zone = NewZone;
	}

	SDateTime& SDateTime::operator=(SDateTime Other)
	{
		using Aeon::Swap;
		Swap(*this, Other);
		return *this;
	}

	SDateTime SDateTime::Now(ESpecificTimezone SpecificZone)
	{
		return STimePoint::Now(SpecificZone).AsDateTime();
	}
	SDateTime SDateTime::Now(SStringView TimezoneName)
	{
		return STimePoint::Now(TimezoneName).AsDateTime();
	}

	SDateTime SDateTime::Epoch(ESpecificTimezone SpecificZone)
	{
		return STimePoint::Epoch(SpecificZone).AsDateTime();
	}
	SDateTime SDateTime::Epoch(SStringView TimezoneName)
	{
		return STimePoint::Epoch(TimezoneName).AsDateTime();
	}

	int64 SDateTime::Year() const
	{
		return int64(int32(Date.year()));
	}

	int64 SDateTime::Month() const
	{
		return int64(uint32(Date.month()));
	}

	int64 SDateTime::Day() const
	{
		return int64(uint32(Date.day()));
	}

	int64 SDateTime::Hours() const
	{
		return int64(Time.hours().count());
	}

	int64 SDateTime::Minutes() const
	{
		return int64(Time.minutes().count());
	}

	int64 SDateTime::Seconds() const
	{
		return int64(Time.seconds().count());
	}

	float64 SDateTime::Subseconds() const
	{
		return float64(Time.subseconds().count()) * SubsecondsPrecision();
	}

	STimezone SDateTime::Timezone() const
	{
		return Zone;
	}

	bool SDateTime::IsValid() const
	{
		// An invalid SDateTime (e.g. default-constructed) always has an invalid 'Zone' pointer
		return Zone.IsValid();
	}

	bool SDateTime::operator==(SDateTime const& Other) const
	{
		auto ThisLocalTime = stdtm::local_timepoint{ AsNative().time_since_epoch() };
		auto OtherLocalTime = stdtm::local_timepoint{ Other.AsNative().time_since_epoch() };
		return Zone.ToSystem_Internal(ThisLocalTime) == Other.Zone.ToSystem_Internal(OtherLocalTime);
	}
	strong_ordering SDateTime::operator<=>(SDateTime const& Other) const
	{
		auto ThisLocalTime = stdtm::local_timepoint{ AsNative().time_since_epoch() };
		auto OtherLocalTime = stdtm::local_timepoint{ Other.AsNative().time_since_epoch() };
		return Zone.ToSystem_Internal(ThisLocalTime) <=> Other.Zone.ToSystem_Internal(OtherLocalTime);
	}

	SPreciseDateTime SDateTime::AsPrecise() const
	{
		return SPreciseDateTime(*this);
	}

	stdtm::sys_timepoint SDateTime::AsNative() const // TODO refactor this into AsTimePoint
	{
		return stdtm::day_timepoint{ Date } + Time.to_duration();
	}

	std::tm SDateTime::AsNativeLegacy() const // TODO Rename this to AsNative after refactor of current one
	{
		using namespace std::chrono_literals;

		stdtm::day_timespan WeekDay = stdtm::weekdate_desc{ Date }.weekday() - stdtm::Sunday;
		stdtm::day_timespan YearDay = stdtm::day_timepoint{ Date } - stdtm::day_timepoint{ Date.year() / stdtm::January / 1d };
		stdtm::month_timespan Month = Date.month() - stdtm::January;
		stdtm::year_timespan Year = Date.year() - 1900y;

		// We only use this to compute the offset for daylight savings
		stdtm::local_timepoint LocalPoint{ AsNative().time_since_epoch() };
		stdtm::sys_timepoint SysPoint = Zone.ToSystem_Internal(LocalPoint);
		stdtm::min_timespan DSTOffset = Zone.Info(SysPoint).DSTOffset;

		std::tm Calendar;
		Calendar.tm_sec = int32(Seconds());
		Calendar.tm_min = int32(Minutes());
		Calendar.tm_hour = int32(Hours());
		Calendar.tm_wday = int32(WeekDay.count());
		Calendar.tm_mday = int32(Day());
		Calendar.tm_yday = int32(YearDay.count());
		Calendar.tm_mon = int32(Month.count());
		Calendar.tm_year = int32(Year.count());
		Calendar.tm_isdst = int32(DSTOffset.count() != 0);
		return Calendar;
	}

	constexpr float64 SDateTime::SubsecondsPrecision()
	{
		constexpr float64 Precision = 1.0 / stdtm::time_desc::precision::period::den;
		return Precision;
	}

	void Swap(SDateTime& DateTime1, SDateTime& DateTime2)
	{
		using Aeon::Swap;
		Swap(DateTime1.Date, DateTime2.Date);
		Swap(DateTime1.Time, DateTime2.Time);
		Swap(DateTime1.Zone, DateTime2.Zone);
	}

	SPreciseDateTime::SPreciseDateTime(SDateTime const& DateTime)
		: SDateTime(DateTime)
	{
	}

	SPreciseDateTime& SPreciseDateTime::operator=(SPreciseDateTime Other)
	{
		using Aeon::Swap;
		Swap(static_cast<SDateTime&>(*this), static_cast<SDateTime&>(Other));
		return *this;
	}
}
