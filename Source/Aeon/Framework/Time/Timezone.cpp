#include "Aeon/Framework/Time/Timezone.hpp"

#include "Aeon/Framework/Time/TimePoint.hpp"

namespace Aeon
{
	STimezone::STimezone()
		: Zone(nullptr)
	{
	}

	STimezone::STimezone(DatabaseAccess, stdtm::tz const* NewZone)
		: Zone(NewZone)
	{
	}

	STimezone& STimezone::operator=(STimezone Other)
	{
		using Aeon::Swap;
		Swap(*this, Other);
		return *this;
	}

	SStringView STimezone::Name() const
	{
		if (Zone)
		{
			auto Name = Zone->name();
			return SStringView{ Name.data(), SStringView::IndexType(Name.length()) };
		}
		return SStringView{};
	}

	DTimezoneInfo STimezone::Info(stdtm::sys_timepoint const& TimePoint) const
	{
		auto Info = Zone ? Zone->get_info(TimePoint) : stdtm::tz_info{};
		auto UTCOffset = stdtm::floor<stdtm::min_timespan>(Info.offset);
		auto DSTOffset = Info.save;
		return DTimezoneInfo{ Info.begin, Info.end, UTCOffset, DSTOffset };
	}

	STimePoint STimezone::Convert(STimePoint const& TimePoint) const
	{
		return STimePoint{ TimePoint.Zone.ToSystem_Internal(TimePoint.Point), *this };
	}

	stdtm::sys_timepoint STimezone::ToSystem_Internal(stdtm::local_timepoint const& TimePoint) const
	{
		return Zone
			? Zone->to_sys(TimePoint, stdtm::choose::latest)
			: stdtm::sys_timepoint{ TimePoint.time_since_epoch() };
	}
	stdtm::local_timepoint STimezone::ToLocal_Internal(stdtm::sys_timepoint const& TimePoint) const
	{
		return Zone
			? Zone->to_local(TimePoint)
			: stdtm::local_timepoint{ TimePoint.time_since_epoch() };
	}

	bool STimezone::IsValid() const
	{
		return Zone;
	}

	stdtm::tz const& STimezone::AsNative() const
	{
		AEON_ASSERT(IsValid());
		return *Zone;
	}

	void Swap(STimezone& Timezone1, STimezone& Timezone2)
	{
		using Aeon::Swap;
		Swap(Timezone1.Zone, Timezone2.Zone);
	}
}
