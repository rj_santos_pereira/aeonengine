#include "Aeon/Framework/Time/TimePoint.hpp"

#include "Aeon/Framework/Time/TimezoneDatabase.hpp"
#include "Aeon/Framework/Time/TimeSpan.hpp"
#include "Aeon/Framework/Time/DateTime.hpp"

namespace Aeon
{
	STimePoint::STimePoint(stdtm::sys_timepoint const& NewPoint, STimezone NewZone)
		: Point(NewZone.ToLocal_Internal(NewPoint))
		, Zone(NewZone)
	{
	}
	STimePoint::STimePoint(stdtm::local_timepoint const& NewPoint, STimezone NewZone)
		: Point(NewPoint)
		, Zone(NewZone)
	{
	}

	STimePoint& STimePoint::operator=(STimePoint Other)
	{
		using Aeon::Swap;
		Swap(*this, Other);
		return *this;
	}

	// FIXME better handling of null Timezone in static methods

	STimePoint STimePoint::Now(ESpecificTimezone SpecificZone)
	{
		auto Timezone = STimezoneDatabase::Access().FindTimezone(SpecificZone);
		return STimePoint{ stdtm::sys_clock::now(), Timezone };
	}
	STimePoint STimePoint::Now(SStringView TimezoneName)
	{
		auto Timezone = STimezoneDatabase::Access().FindTimezone(TimezoneName);
		return STimePoint{ stdtm::sys_clock::now(), Timezone };
	}

	STimePoint STimePoint::Epoch(ESpecificTimezone SpecificZone)
	{
		auto Timezone = STimezoneDatabase::Access().FindTimezone(SpecificZone);
		return STimePoint{ stdtm::sys_timepoint{ stdtm::sys_timespan::zero() }, Timezone };
	}
	STimePoint STimePoint::Epoch(SStringView TimezoneName)
	{
		auto Timezone = STimezoneDatabase::Access().FindTimezone(TimezoneName);
		return STimePoint{ stdtm::sys_timepoint{ stdtm::sys_timespan::zero() }, Timezone };
	}

	STimePoint STimePoint::FromTimestamp(uint64 NewTimestamp, ESpecificTimezone SpecificZone)
	{
		// CHECKME Consider using stdtm::sys_clock::from_time_t()
		auto Timezone = STimezoneDatabase::Access().FindTimezone(SpecificZone);
		return STimePoint{ stdtm::local_timepoint{ stdtm::sys_timespan{ stdtm::sys_timespan::rep(NewTimestamp) } }, Timezone };
	}

	STimePoint& STimePoint::operator+=(STimeSpan const& TimeSpan)
	{
		Point += TimeSpan.Span;
		return *this;
	}

	STimePoint& STimePoint::operator-=(STimeSpan const& TimeSpan)
	{
		Point -= TimeSpan.Span;
		return *this;
	}

	STimePoint STimePoint::operator+(STimeSpan const& TimeSpan) const
	{
		return STimePoint{ *this } += TimeSpan;
	}

	STimePoint STimePoint::operator-(STimeSpan const& TimeSpan) const
	{
		return STimePoint{ *this } -= TimeSpan;
	}

	int64 STimePoint::Year() const
	{
		auto Date = MakeDateDescription_Internal();
		return int64(int32(Date.year()));
	}

	int64 STimePoint::Month() const
	{
		auto Date = MakeDateDescription_Internal();
		return int64(uint32(Date.month()));
	}

	int64 STimePoint::Day() const
	{
		auto Date = MakeDateDescription_Internal();
		return int64(uint32(Date.day()));
	}

	int64 STimePoint::Hours() const
	{
		auto Time = MakeTimeDescription_Internal();
		return int64(Time.hours().count());
	}

	int64 STimePoint::Minutes() const
	{
		auto Time = MakeTimeDescription_Internal();
		return int64(Time.minutes().count());
	}

	int64 STimePoint::Seconds() const
	{
		auto Time = MakeTimeDescription_Internal();
		return int64(Time.seconds().count());
	}

	float64 STimePoint::Subseconds() const
	{
		auto Time = MakeTimeDescription_Internal();
		return float64(Time.subseconds().count()) * SubsecondsPrecision();
	}

	uint64 STimePoint::ToTimestamp() const
	{
		// CHECKME Consider using stdtm::sys_clock::to_time_t()
		return uint64(Point.time_since_epoch().count());
	}

	STimezone STimePoint::Timezone() const
	{
		return Zone;
	}

	bool STimePoint::IsValid() const
	{
		// An invalid STimePoint (e.g. default-constructed) always has an invalid 'Zone'
		return Zone.IsValid();
	}

	SDateTime STimePoint::AsDateTime() const
	{
		return SDateTime(Point, Zone);
	}

	stdtm::sys_timepoint STimePoint::AsNative() const
	{
		// NOTE We convert the local timepoint to a system one directly because most stdlib APIs can't consume local timepoints,
		//  but we want to preserve the values as they are (i.e. in the specific timezone that the user requested).
	//	return Zone.ToSystem(Point);
		return stdtm::sys_timepoint{ Point.time_since_epoch() };
	}

	bool STimePoint::operator==(STimePoint const& Other) const
	{
		return Zone.ToSystem_Internal(Point) == Other.Zone.ToSystem_Internal(Other.Point);
	}
	strong_ordering STimePoint::operator<=>(STimePoint const& Other) const
	{
		return Zone.ToSystem_Internal(Point) <=> Other.Zone.ToSystem_Internal(Other.Point);
	}

	constexpr float64 STimePoint::SubsecondsPrecision()
	{
		constexpr float64 Precision = 1.0 / stdtm::time_desc::precision::period::den;
		return Precision;
	}

	void Swap(STimePoint& TimePoint1, STimePoint& TimePoint2)
	{
		using Aeon::Swap;
		Swap(TimePoint1.Point, TimePoint2.Point);
		Swap(TimePoint1.Zone, TimePoint2.Zone);
	}

	stdtm::date_desc STimePoint::MakeDateDescription_Internal() const
	{
		return stdtm::date_desc{ stdtm::day_timepoint{ stdtm::floor<stdtm::day_timespan>(Point.time_since_epoch()) }};
	}

	stdtm::time_desc STimePoint::MakeTimeDescription_Internal() const
	{
		return stdtm::time_desc{ Point.time_since_epoch() - stdtm::floor<stdtm::day_timespan>(Point.time_since_epoch()) };
	}

}
