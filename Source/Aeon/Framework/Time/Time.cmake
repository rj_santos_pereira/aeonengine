#add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/)

target_sources(${PROJECT_NAME}
	PUBLIC
		${CMAKE_CURRENT_SOURCE_DIR}/Timezone.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/TimezoneDatabase.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/TimeSpan.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/TimePoint.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/DateTime.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/Stopwatch.hpp
	PRIVATE
		${CMAKE_CURRENT_SOURCE_DIR}/Timezone.cpp
		${CMAKE_CURRENT_SOURCE_DIR}/TimezoneDatabase.cpp
		${CMAKE_CURRENT_SOURCE_DIR}/TimeSpan.cpp
		${CMAKE_CURRENT_SOURCE_DIR}/TimePoint.cpp
		${CMAKE_CURRENT_SOURCE_DIR}/DateTime.cpp
	)