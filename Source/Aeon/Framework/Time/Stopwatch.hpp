#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_TIME_STOPWATCH
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_TIME_STOPWATCH

#include "Aeon/Framework/Core.hpp"

// TODO separate this into cpp

namespace Aeon
{
#if 0
	enum struct EStopwatchMemoryOrder
	{
		NonSerial,
		Serial,
	};
#endif

	// TODO move this elsewhere
	class SPreciseSleeper
	{
		// TODO implement this, see:
		//  https://stackoverflow.com/questions/85122/how-to-make-thread-sleep-less-than-a-millisecond-on-windows
		//  https://stackoverflow.com/questions/7827062/is-there-a-windows-equivalent-of-nanosleep
		//  https://stackoverflow.com/questions/13397571/precise-thread-sleep-needed-max-1ms-error
	};

	class SCycleData
	{
	public:
		SCycleData(uint64 NewCycles) : Cycles{ NewCycles } {}

		SCycleData(SCycleData const&) = default;
		SCycleData(SCycleData&&) = default;

		SCycleData& operator=(SCycleData const&) = default;
		SCycleData& operator=(SCycleData&&) = default;

		uint64 AsCycles() const
		{
			return Cycles;
		}

		float64 AsSeconds() const
		{
			static float64 InvFreq =
				[] {
#				if AEON_PLATFORM_WINDOWS
					LARGE_INTEGER FreqStorage;
					QueryPerformanceFrequency(&FreqStorage);

					uint64 Freq = bit_cast<uint64>(FreqStorage);
					return 1.0_f64 / Freq;
#				else
					// NOTE For Unix, check clock_gettime and clock_getres
#					error Not implemented yet!
#				endif
				} ();

			return Cycles * InvFreq;
		}

	private:
		uint64 Cycles;
	};

	class SLightweightStopwatch
	{
	public:
		// NOTE On "modern" Intel archs, rdtsc has over 20 cycles of issue latency,
		//  and on "modern" AMD archs, rdtsc has over 30 cycles of issue latency.
		//  In other words, these are somewhat expensive instructions, so try to be frugal with them.

		// > The RDTSC instruction is not a serializing instruction.
		// > It does not necessarily wait until all previous instructions have been executed before reading the counter.
		// > Similarly, subsequent instructions may begin execution before the read operation is performed.

		SLightweightStopwatch() = default;

		SLightweightStopwatch(SLightweightStopwatch const&) = default;
		SLightweightStopwatch(SLightweightStopwatch&&) = default;

		SLightweightStopwatch& operator=(SLightweightStopwatch const&) = default;
		SLightweightStopwatch& operator=(SLightweightStopwatch&&) = default;

		~SLightweightStopwatch() = default;

		void Restart()
		{
#		if AEON_ARCHITECTURE_X86 && 0
			StartCycles = __rdtsc();

			if constexpr (MemOrderValue == EStopwatchMemoryOrder::Serial)
			{
				// > If software requires RDTSC to be executed prior to execution of any subsequent instruction (including any memory accesses),
				// > it can execute the sequence LFENCE immediately after RDTSC.
				Aeon::LoadFence();
			}
#		endif

			StartCycles = QueryCounter();
		}

		SCycleData Current() const
		{
			AEON_FAILSAFE(IsRunning(), return {0});

			return { QueryCounter() - StartCycles };
		}

		void Reset()
		{
			StartCycles = 0;
		}

#if 0
		SCycleData Stop()
		{
			AEON_ASSERT(IsRunning());

#		if AEON_ARCHITECTURE_X86 && 0
			if constexpr (MemOrderValue == EStopwatchMemoryOrder::Serial)
			{
				// > If software requires RDTSC to be executed only after all previous instructions have executed
				// > and all previous loads are globally visible, it can execute LFENCE immediately before RDTSC.
				// > If software requires RDTSC to be executed only after all previous instructions have executed
				// > and all previous loads and stores are globally visible, it can execute the sequence MFENCE;LFENCE immediately before RDTSC.
				Aeon::MemoryFence();
			//	Aeon::LoadFence();
			}

			uint64 StopCycles = __rdtsc();
#		endif

			return { QueryCounter() - Exchange(StartCycles, 0) };
		}
#endif

		bool IsRunning() const
		{
			return StartCycles != 0;
		}

		uint64 QueryCounter() const
		{
#		if AEON_PLATFORM_WINDOWS
			LARGE_INTEGER CycleStorage;
			QueryPerformanceCounter(&CycleStorage);

			return bit_cast<uint64>(CycleStorage);
#		else
			// NOTE For Unix, check clock_gettime and clock_getres
#			error Not implemented yet!
#		endif
		}

	protected:
		uint64 StartCycles = 0;
	};

	class SStopwatch : protected SLightweightStopwatch
	{
		using Base = SLightweightStopwatch;
		
	public:
		SStopwatch() = default;

		SStopwatch(SStopwatch const&) = default;
		SStopwatch(SStopwatch&&) = default;

		SStopwatch& operator=(SStopwatch const&) = default;
		SStopwatch& operator=(SStopwatch&&) = default;

		~SStopwatch() = default;

		void Restart()
		{
			Base::Restart();
			SplitCycles = StartCycles;
		}

		using Base::Current;

		SCycleData Split()
		{
			AEON_FAILSAFE(IsRunning(), return {0});

			uint64 OldSplitCycles = Exchange(SplitCycles, QueryCounter());
			return { SplitCycles - OldSplitCycles };
		}
		
		using Base::Reset;

		using Base::IsRunning;

		using Base::QueryCounter;

	protected:
		uint64 SplitCycles = 0;

	};
}

#endif