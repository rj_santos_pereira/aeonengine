#include "Aeon/Framework/Time/TimeSpan.hpp"

#include "Aeon/Framework/Time/TimePoint.hpp"

namespace Aeon
{
	STimeSpan::STimeSpan(stdtm::sys_timespan const& NewSpan)
		: Span(NewSpan)
	{
	}

	STimeSpan& STimeSpan::operator=(STimeSpan Other)
	{
		using Aeon::Swap;
		Swap(*this, Other);
		return *this;
	}

	STimeSpan STimeSpan::MakeYears(int64 Units)
	{
		return STimeSpan{ stdtm::years{ Units } };
	}

	STimeSpan STimeSpan::MakeMonths(int64 Units)
	{
		return STimeSpan{ stdtm::months{ Units } };
	}

	STimeSpan STimeSpan::MakeDays(int64 Units)
	{
		return STimeSpan{ stdtm::days{ Units } };
	}

	STimeSpan STimeSpan::MakeHours(int64 Units)
	{
		return STimeSpan{ stdtm::hours{ Units } };
	}

	STimeSpan STimeSpan::MakeMinutes(int64 Units)
	{
		return STimeSpan{ stdtm::minutes{ Units } };
	}

	STimeSpan STimeSpan::MakeSeconds(int64 Units)
	{
		return STimeSpan{ stdtm::seconds{ Units } };
	}

	STimeSpan STimeSpan::MakeMilliseconds(int64 Units)
	{
		return STimeSpan{ stdtm::milliseconds{ Units } };
	}

	STimeSpan STimeSpan::MakeMicroseconds(int64 Units)
	{
		return STimeSpan{ stdtm::microseconds{ Units } };
	}

#if 0
	STimeSpan STimeSpan::MakeNanoseconds(int64 Units)
	{
		return STimeSpan{ stdtm::nanoseconds{ Units } };
	}
#endif

	STimeSpan STimeSpan::Difference(STimePoint const& TimePoint1, STimePoint const& TimePoint2)
	{
		stdtm::sys_timepoint SysPoint1 = TimePoint1.Zone.ToSystem_Internal(TimePoint1.Point);
		stdtm::sys_timepoint SysPoint2 = TimePoint2.Zone.ToSystem_Internal(TimePoint2.Point);
		return STimeSpan{ stdtm::abs(SysPoint1 - SysPoint2) };
	}

	STimeSpan STimeSpan::operator+() const
	{
		return STimeSpan{ +Span };
	}

	STimeSpan STimeSpan::operator-() const
	{
		return STimeSpan{ -Span };
	}

	STimeSpan& STimeSpan::operator+=(STimeSpan const& Other)
	{
		Span += Other.Span;
		return *this;
	}

	STimeSpan& STimeSpan::operator-=(STimeSpan const& Other)
	{
		Span -= Other.Span;
		return *this;
	}

	STimeSpan& STimeSpan::operator*=(float64 Factor)
	{
		Span = stdtm::sys_timespan{ int64(Span.count() * Factor) };
		return *this;
	}

	STimeSpan& STimeSpan::operator/=(int64 Factor)
	{
		Span = stdtm::sys_timespan{ Span.count() / Factor };
		return *this;
	}

	STimeSpan& STimeSpan::operator%=(int64 Factor)
	{
		Span = stdtm::sys_timespan{ Span.count() % Factor };
		return *this;
	}

	STimeSpan& STimeSpan::operator%=(STimeSpan const& Other)
	{
		Span %= Other.Span;
		return *this;
	}

	STimeSpan STimeSpan::operator+(STimeSpan const& Other) const
	{
		return STimeSpan{ Span } += Other;
	}

	STimeSpan STimeSpan::operator-(STimeSpan const& Other) const
	{
		return STimeSpan{ Span } -= Other;
	}

	STimeSpan operator*(STimeSpan const& TimeSpan, float64 Factor)
	{
		return STimeSpan{ TimeSpan } *= Factor;
	}
	STimeSpan operator*(float64 Factor, STimeSpan const& TimeSpan)
	{
		return STimeSpan{ TimeSpan } *= Factor;
	}

	STimeSpan STimeSpan::operator/(int64 Factor) const
	{
		return STimeSpan{ *this } /= Factor;
	}
	STimeSpan STimeSpan::operator%(int64 Factor) const
	{
		return STimeSpan{ *this } %= Factor;
	}

	STimeSpan STimeSpan::operator%(STimeSpan const& Other) const
	{
		return STimeSpan{ *this } %= Other;
	}

	int64 STimeSpan::NumYears() const
	{
		return stdtm::floor<stdtm::years>(Span).count();
	}

	int64 STimeSpan::NumMonths() const
	{
		return stdtm::floor<stdtm::months>(Span).count();
	}

	int64 STimeSpan::NumDays() const
	{
		return stdtm::floor<stdtm::days>(Span).count();
	}

	int64 STimeSpan::NumHours() const
	{
		return stdtm::floor<stdtm::hours>(Span).count();
	}

	int64 STimeSpan::NumMinutes() const
	{
		return stdtm::floor<stdtm::minutes>(Span).count();
	}

	int64 STimeSpan::NumSeconds() const
	{
		return stdtm::floor<stdtm::seconds>(Span).count();
	}

	int64 STimeSpan::NumMilliseconds() const
	{
		return stdtm::floor<stdtm::milliseconds>(Span).count();
	}

	int64 STimeSpan::NumMicroseconds() const
	{
		return stdtm::floor<stdtm::microseconds>(Span).count();
	}

	stdtm::sys_timespan STimeSpan::AsNative() const
	{
		return Span;
	}

	bool STimeSpan::operator==(STimeSpan const& Other) const
	{
		return Span == Other.Span;
	}
	strong_ordering STimeSpan::operator<=>(STimeSpan const& Other) const
	{
		// operator<=> requires this to be valid, due to strong_ordering
		// If this breaks on any platform, adjust the function to account for that, and
		static_assert(VIsInteger<stdtm::sys_timespan::rep>);
		return Span <=> Other.Span;
	}

	void Swap(STimeSpan& TimeSpan1, STimeSpan& TimeSpan2)
	{
		using Aeon::Swap;
		Swap(TimeSpan1.Span, TimeSpan2.Span);
	}
}
