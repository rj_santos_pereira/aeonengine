#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_TIME_TIMESPAN
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_TIME_TIMESPAN

#include "Aeon/Framework/Core.hpp"

namespace Aeon
{
	class STimePoint;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// STimeSpan

	class STimeSpan
	{
		STimeSpan(stdtm::sys_timespan const& NewSpan);

	public:
		STimeSpan() = default;

		STimeSpan(STimeSpan const&) = default;
		STimeSpan(STimeSpan&&) = default;

		STimeSpan& operator=(STimeSpan Other);

		static STimeSpan MakeYears(int64 Units);
		static STimeSpan MakeMonths(int64 Units);
		static STimeSpan MakeDays(int64 Units);

		static STimeSpan MakeHours(int64 Units);
		static STimeSpan MakeMinutes(int64 Units);
		static STimeSpan MakeSeconds(int64 Units);

		static STimeSpan MakeMilliseconds(int64 Units);
		static STimeSpan MakeMicroseconds(int64 Units);
#	if 0
		static STimeSpan MakeNanoseconds(int64 Units);
#	endif

		static STimeSpan Difference(STimePoint const& TimePoint1, STimePoint const& TimePoint2);

		STimeSpan operator+() const;
		STimeSpan operator-() const;

		STimeSpan& operator+=(STimeSpan const& Other);
		STimeSpan& operator-=(STimeSpan const& Other);

		STimeSpan& operator*=(float64 Factor);

		STimeSpan& operator/=(int64 Factor);
		STimeSpan& operator%=(int64 Factor);

		STimeSpan& operator%=(STimeSpan const& Other);

		STimeSpan operator+(STimeSpan const& Other) const;
		STimeSpan operator-(STimeSpan const& Other) const;

		friend STimeSpan operator*(STimeSpan const& TimeSpan, float64 Factor);
		friend STimeSpan operator*(float64 Factor, STimeSpan const& TimeSpan);

		STimeSpan operator/(int64 Factor) const;
		STimeSpan operator%(int64 Factor) const;

		STimeSpan operator%(STimeSpan const& Other) const;

		int64 NumYears() const;
		int64 NumMonths() const;
		int64 NumDays() const;

		int64 NumHours() const;
		int64 NumMinutes() const;
		int64 NumSeconds() const;

		int64 NumMilliseconds() const;
		int64 NumMicroseconds() const;
#	if 0
		int64 NumNanoseconds() const;
#	endif

		// TODO methods for rounding (floor, ceil) to nearest whole Seconds, etc

		stdtm::sys_timespan AsNative() const;

		bool operator==(STimeSpan const& Other) const;
		strong_ordering operator<=>(STimeSpan const& Other) const;

		friend void Swap(STimeSpan& TimeSpan1, STimeSpan& TimeSpan2);

	private:
		stdtm::sys_timespan Span;

		friend class STimePoint;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Literal operators

	inline namespace Literals
	{
		inline STimeSpan operator""_yr(ltrint Literal)
		{
			return STimeSpan::MakeYears(Literal);
		}
		inline STimeSpan operator""_mon(ltrint Literal)
		{
			return STimeSpan::MakeMonths(Literal);
		}
		inline STimeSpan operator""_day(ltrint Literal)
		{
			return STimeSpan::MakeDays(Literal);
		}

		inline STimeSpan operator""_hr(ltrint Literal)
		{
			return STimeSpan::MakeHours(Literal);
		}
		inline STimeSpan operator""_min(ltrint Literal)
		{
			return STimeSpan::MakeMinutes(Literal);
		}
		inline STimeSpan operator""_sec(ltrint Literal)
		{
			return STimeSpan::MakeSeconds(Literal);
		}

		inline STimeSpan operator""_msec(ltrint Literal)
		{
			return STimeSpan::MakeMilliseconds(Literal);
		}
		inline STimeSpan operator""_usec(ltrint Literal)
		{
			return STimeSpan::MakeMicroseconds(Literal);
		}
#	if 0
		inline STimeSpan operator""_nsec(luint Literal)
		{
			return STimeSpan::MakeNanoseconds(Literal);
		}
#	endif
	}
}

namespace ZZINTERNAL_AEON_FMT_NS
{
	template <Aeon::CCharacter CharType>
	struct formatter<Aeon::STimeSpan, CharType> : ZZINTERNAL_AEON_FMT_NS::formatter<std::chrono::nanoseconds, CharType>
	{
		template <class FormatContextType>
		auto format(Aeon::STimeSpan const& TimeSpan, FormatContextType& Context) const
		{
			auto SecondsTimeSpan = std::chrono::floor<std::chrono::nanoseconds>(TimeSpan.AsNative());
			return formatter<std::chrono::nanoseconds, CharType>::format(SecondsTimeSpan, Context);
		}
	};
}

#endif