#add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/)

target_sources(${PROJECT_NAME}
	PUBLIC
		${CMAKE_CURRENT_SOURCE_DIR}/Allocator.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/DynamicAllocator.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/FixedAllocator.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/StaticAllocator.hpp
#	PRIVATE
#		${CMAKE_CURRENT_SOURCE_DIR}/
	)