#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_ALLOCATOR_DYNAMICALLOCATOR
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_ALLOCATOR_DYNAMICALLOCATOR

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Allocator/Allocator.hpp"

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GDynamicAllocator

	template <CScalarObject ObjType, CUnsignedInteger IdxType = uint32>
	requires CMoveableElseCopyable<ObjType>
	class GDynamicAllocator
	{
	public:
		using ObjectType = TRemoveConstVolatile<ObjType>;
		using IndexType = IdxType;
		using RelocationFunctionType = void(*)(ObjectType*, IndexType, ObjectType*, IndexType, voidptr);

		explicit GDynamicAllocator() = default;

		GDynamicAllocator(GDynamicAllocator const&) = default;
		GDynamicAllocator(GDynamicAllocator&&) = default;

		GDynamicAllocator& operator=(GDynamicAllocator const&) = default;
		GDynamicAllocator& operator=(GDynamicAllocator&&) = default;

		template <class OtherObjType>
		GDynamicAllocator(GDynamicAllocator<OtherObjType, IdxType> const&)
		{
		}
		template <class OtherObjType>
		GDynamicAllocator(GDynamicAllocator<OtherObjType, IdxType>&&)
		{
		}

		GDynamicAllocator OnContainerCopy() const
		{
			return GDynamicAllocator();
		}

		GDynamicAllocator OnContainerMove()
		{
			return GDynamicAllocator();
		}

		AEON_NODISCARD ObjectType* Allocate(IndexType ObjectCount)
		{
			// If the size is 0, then interpret it as a request to not allocate any memory at this time and return immediately
			if (ObjectCount == 0)
			{
				return nullptr;
			}

			szint StorageSize = Aeon::SizeOf(ObjectType, ObjectCount);
			ObjectType* StoragePtr = AeonMemory::MemAlloc<ObjectType[]>(StorageSize, ObjectAlignment);

			return StoragePtr;
		}

		AEON_NODISCARD ObjectType* Reallocate(ObjectType* OldStoragePtr, IndexType OldObjectCount, IndexType NewObjectCount,
											  RelocationFunctionType RelocationFunction, voidptr InstanceData)
		{
			// If there is no change to the size, just return the old pointer
			if (OldObjectCount == NewObjectCount)
			{
				return OldStoragePtr;
			}

			// If the size is 0, then interpret it as a request to deallocate the current one and return immediately
			if (NewObjectCount == 0)
			{
				Deallocate(OldStoragePtr);
				return nullptr;
			}

			// If there was no prior allocation, make a regular one now and return immediately
			if (OldObjectCount == 0)
			{
				return Allocate(NewObjectCount);
			}

			ObjectType* NewStoragePtr;
#		if 0
			// CHECKME this breaks certain containers (e.g. GCyclicArray), any way we can make it work?
			if constexpr (VIsTriviallyCopyable<ObjectType>)
			{
				IndexType NewStorageSize = Aeon::SizeOf(ObjectType, NewObjectCount);

				// Valid under P0593R6, since the type is trivially copyable (implies trivially destructible)
				NewStoragePtr = AeonMemory::MemRealloc<ObjectType[]>(OldStoragePtr, NewStorageSize);
			}
			else
#		endif
			{
				NewStoragePtr = Allocate(NewObjectCount);
				if (NewStoragePtr)
				{
					RelocationFunction(OldStoragePtr, OldObjectCount, NewStoragePtr, NewObjectCount, InstanceData);

					Deallocate(OldStoragePtr);
				}
			}

			return NewStoragePtr;
		}

		void Deallocate(ObjectType* StoragePtr)
		{
			// If the pointer is null, simply no-op, this simplifies some code on containers
			if (StoragePtr == nullptr)
			{
				return;
			}

			AeonMemory::MemDealloc(StoragePtr);
		}

		bool IsAllocationSizeSupported(IndexType ObjectCount) const
		{
			constexpr szint MaximumObjectCount = AeonMath::Min<szint>(TNumericProperties<IndexType>::Maximum - 1,
																	  TNumericProperties<szint>::Maximum / ObjectSize);
			return szint(ObjectCount) <= MaximumObjectCount;
		}

		IndexType RecommendedStartingAllocationSize() const
		{
			constexpr IndexType AllocationSize = RecommendedStartingAllocationSize_Internal();
			return AllocationSize;
		}

		bool ShouldIncreaseAllocationSize(IndexType OldObjectCount, IndexType NewObjectCount) const
		{
			return OldObjectCount < NewObjectCount;
		}
		bool ShouldDecreaseAllocationSize(IndexType OldObjectCount, IndexType NewObjectCount) const
		{
			constexpr IndexType MinimumObjectCount = RecommendedStartingAllocationSize_Internal();
			// This ensures we don't make redundant calls to Realloc
			bool CanDecreaseAllocationSize = MinimumObjectCount < OldObjectCount;
			// Simple deallocation policy: ensure we keep at most four times the amount of used memory, and reduce to half when it reaches that limit;
			// this avoids continuous reallocation when adding and removing elements at the same rate near the half used memory threshold
			return CanDecreaseAllocationSize && NewObjectCount <= (OldObjectCount >> 2);
		}

		IndexType ComputeAllocationSizeIncrease(IndexType OldObjectCount) const
		{
			return AeonMath::Max(OldObjectCount << 1, RecommendedStartingAllocationSize());
		}
		IndexType ComputeAllocationSizeDecrease(IndexType OldObjectCount) const
		{
			return AeonMath::Max(OldObjectCount >> 1, RecommendedStartingAllocationSize());
		}

		bool operator==(GDynamicAllocator const&) const
		{
			return true;
		}

	private:
		static constexpr IndexType RecommendedStartingAllocationSize_Internal()
		{
			return AeonMath::Max<IndexType>(IndexType(AeonMemory::PlatformCacheLinePrefetchSize / ObjectSize), 1);
		}

		static constexpr szint ObjectSize = Aeon::SizeOf(ObjectType);
		static constexpr algnint ObjectAlignment = Aeon::AlignOf(ObjectType);
	};

	template <CScalarObject ObjType, CUnsignedInteger IdxType>
	struct ZzInternal_TAllocatorTraits<GDynamicAllocator<ObjType, IdxType>>
	{
		static constexpr bool UsesGlobalResource = true;
		static constexpr bool IsChunkableResource = true;
		static constexpr bool IsShareableResource = true;
		static constexpr bool IsSwappableResource = true;
	};

	template <CScalarObject ObjType, CUnsignedInteger IdxType, CScalarObject OtherObjType>
	struct ZzInternal_TRebindAllocator<GDynamicAllocator<ObjType, IdxType>, OtherObjType>
	{
		using Type = GDynamicAllocator<OtherObjType, IdxType>;
	};

}

#endif