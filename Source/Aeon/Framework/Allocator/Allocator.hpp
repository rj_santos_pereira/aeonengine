#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_ALLOCATOR_ALLOCATOR
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_ALLOCATOR_ALLOCATOR

#include "Aeon/Framework/Core.hpp"

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CAllocator, CAllocatorForObject

	// NOTE Allocators have no abstract base class to allow certain classes to be empty and to reduce the runtime overhead on function calls;
	//  polymorphic classes won't be empty even if they have no data members because virtual methods require space for the instance's vtable,
	//  and virtual calls need to dereference the vtable pointer to determine the actual overridden method to jump to.

	template <class Type>
	concept CAllocator = requires
	{
		requires CDefaultConstructible<Type>;

		requires CCopyable<Type> || CMoveable<Type>;

		// Check if Allocator type defines a ObjectType nested type
		typename Type::ObjectType;
		// Check if the ObjectType nested type designates an object type that is decayed to the simplest type
		requires CScalarObject<typename Type::ObjectType> && CDecayed<typename Type::ObjectType>;

		// Check if Allocator type defines a IndexType nested type
		typename Type::IndexType;
		// Check if the IndexType nested type designates an integer type that is decayed to the simplest type
		requires CInteger<typename Type::IndexType> && CDecayed<typename Type::IndexType>;

		// Check if Allocator type defines a RelocationFunctionType nested type
		typename Type::RelocationFunctionType;
		// Check if the RelocationFunctionType nested type designates a function-pointer type that is decayed to the simplest type
		requires CFunctionPointer<typename Type::RelocationFunctionType> && CDecayed<typename Type::RelocationFunctionType>;

		// Check if the RelocationFunctionType is a callable type with the following arguments
		requires CCallable<typename Type::RelocationFunctionType, typename Type::ObjectType*, typename Type::IndexType,
						   typename Type::ObjectType*, typename Type::IndexType, voidptr>;

		requires requires (Type const Allocator)
		{ { Allocator.OnContainerCopy() } -> CSameClass<Type>; };

		requires requires (Type Allocator)
		{ { Allocator.OnContainerMove() } -> CSameClass<Type>; };

		requires requires (Type Allocator, typename Type::IndexType NewStorageCount)
		{ { Allocator.Allocate(NewStorageCount) } -> CSameClass<typename Type::ObjectType*>; };

		requires requires (Type Allocator, typename Type::ObjectType* OldStoragePtr, typename Type::IndexType OldStorageCount,
						   typename Type::IndexType NewStorageCount, typename Type::RelocationFunctionType RelocationFunction, voidptr InstanceData)
		{ { Allocator.Reallocate(OldStoragePtr, OldStorageCount, NewStorageCount, RelocationFunction, InstanceData) }
			-> CSameClass<typename Type::ObjectType*>; };

		requires requires (Type Allocator, typename Type::ObjectType* StoragePtr)
		{ { Allocator.Deallocate(StoragePtr) } -> CVoid; };

		requires requires (Type const Allocator, typename Type::IndexType StorageCount)
		{ { Allocator.IsAllocationSizeSupported(StorageCount) } -> CBool; };

		requires requires (Type const Allocator)
		{ { Allocator.RecommendedStartingAllocationSize() } -> CSameClass<typename Type::IndexType>; };

		requires requires (Type const Allocator, typename Type::IndexType OldStorageCount, typename Type::IndexType NewStorageCount)
		{ { Allocator.ShouldIncreaseAllocationSize(OldStorageCount, NewStorageCount) } -> CBool; };

		requires requires (Type const Allocator, typename Type::IndexType OldStorageCount, typename Type::IndexType NewStorageCount)
		{ { Allocator.ShouldDecreaseAllocationSize(OldStorageCount, NewStorageCount) } -> CBool; };

		requires requires (Type const Allocator, typename Type::IndexType OldStorageCount)
		{ { Allocator.ComputeAllocationSizeIncrease(OldStorageCount) } -> CSameClass<typename Type::IndexType>; };

		requires requires (Type const Allocator, typename Type::IndexType OldStorageCount)
		{ { Allocator.ComputeAllocationSizeDecrease(OldStorageCount) } -> CSameClass<typename Type::IndexType>; };

		requires requires (Type const Allocator1, Type const Allocator2)
		{ { Allocator1 == Allocator2 } -> CBool; };
		requires requires (Type const Allocator1, Type const Allocator2)
		{ { Allocator1 != Allocator2 } -> CBool; };
	};

	template <class Type, class ObjectType>
	concept CAllocatorForObject = requires
	{
		requires CAllocator<Type>;

		requires CSameClass<typename Type::ObjectType, ObjectType>;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TRebindAllocator

	template <CAllocator AllocatorType, CScalarObject ElementType>
	struct ZzInternal_TRebindAllocator
	{
		using Type = TNullTypeTag;
	};

	template <CAllocator AllocatorType, CScalarObject ElementType>
	using TRebindAllocator = typename ZzInternal_TRebindAllocator<AllocatorType, ElementType>::Type;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CRebindableAllocator

	// FIXME should these ZzInternal concepts also check for presence of assignment operators?

	template <class Type, class RebindType>
	concept ZzInternal_TCopyableRebindAllocator = requires
	{
		// Rebind allocator is constructible from const lvalue-ref to original allocator (copy-like constructor)
		requires requires (Type const Allocator) { { RebindType{ Allocator } } -> CSameClass<RebindType>; };
	};

	template <class Type, class RebindType>
	concept ZzInternal_TMoveableRebindAllocator = requires
	{
		// Rebind allocator is constructible from rvalue-ref to original allocator (move-like constructor)
		requires requires (Type Allocator) { { RebindType{ Move(Allocator) } } -> CSameClass<RebindType>; };
	};

	template <class Type, class NewObjType>
	concept CRebindableAllocator = requires
	{
		requires CAllocator<Type>;

		// FIXME TRebindAllocator should work as expected for ALL allocators, instead,
		//  RebindableAllocator should be restricted to whether you can construct an allocator from a TRebindAllocator<OtherAlloc, Type>
		//  (maybe rename concept to something like RebindConstructibleAllocator)
		typename TRebindAllocator<Type, NewObjType>;
		requires CAllocator<TRebindAllocator<Type, NewObjType>> && CDecayed<TRebindAllocator<Type, NewObjType>>;

		requires CSameClass<NewObjType, typename TRebindAllocator<Type, NewObjType>::ObjectType>;

		requires ZzInternal_TCopyableRebindAllocator<Type, TRebindAllocator<Type, NewObjType>>
			  || ZzInternal_TMoveableRebindAllocator<Type, TRebindAllocator<Type, NewObjType>>;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TAllocatorTraits

/**
 * Example of some allocators and respective traits, and which container operations typically work with each.
 * 							Dynamic			FixedDynamic	FixedStatic		Static			Pool
 *
 * UsesGlobalResource		o				x				x				x				x
 * IsChunkableResource		o				o				o				x				o
 * IsShareableResource		o				x				x				x				o
 * IsSwappableResource		o				o				x				x				o
 *
 * Copy 					o				o				o				o				o
 * Move (piecewise)			o				o				o				o				o
 * Append (piecewise)		o				o				o				o				o
 * Move/Swap				o				o				x				x				o
 * (Requires 'Swappable')
 * Append					o				x				x				x				o
 * (Requires 'Shareable')
**/

	template <CAllocator AllocatorType>
	struct ZzInternal_TAllocatorTraits
	{
		// Traits that determine the behavior of certain allocator (and thus container) operations;
		// for most cases, "resource" is synonymous with "memory".

		// Whether the allocator uses a common underlying resource for all instances, otherwise, it may use a per-instance resource;
		// when this is applicable, a resource is typically assumed to be shareable and swappable.
		// FIXME should UsesGlobalResource imply shareable and swappable?
		static constexpr bool UsesGlobalResource = false;

		// Whether the allocator supports partitioning (and coalescing) the underlying resource on each call to Allocate/Reallocate/Deallocate:
		// this means that each request to Allocate/Reallocate can partition the underlying resource into chunk(s)
		// and return only a chunk that fulfills the request; conversely,
		// any request to Reallocate/Deallocate can take any chunk returned from Allocate/Reallocate and coalesce it with the underlying resource.
		// Otherwise, each time a resource is acquired with Allocate, it must be released with Deallocate before any subsequent request to Allocate,
		// otherwise those requests should fail.
		// Requesting Reallocate is permitted on an allocator that does not support chunking;
		// that request should return the same resource if the request can be fulfilled with the underlying resource, otherwise it should fail.
		static constexpr bool IsChunkableResource = false;

		// Whether the allocator supports sharing the underlying resource between instances:
		// this means that multiple container instances can use the same underlying resource,
		// and typically means that the allocator (and the resource) may be copied between instances;
		// otherwise, a copy of the resource will typically just produce a new, separate instance.
		static constexpr bool IsShareableResource = false;

		// Whether the allocator supports swapping the underlying resource between instances:
		// this means that different underlying resources may be swapped between different container instances,
		// and typically means that the allocator (and the resource) may be moved/swapped between instances;
		// otherwise, a move of the resource will typically just produce a new, separate instance (and a swap may be a no-op).
		static constexpr bool IsSwappableResource = false;
	};

	// NOTE Values from TAllocatorTraits should never depend/be defined based on allocator parameters
	template <CAllocator AllocatorType>
	using TAllocatorTraits = ZzInternal_TAllocatorTraits<AllocatorType>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CChunkableResourceAllocator, CShareableResourceAllocator, CSwappableResourceAllocator

	template <class AllocatorType>
	concept CChunkableResourceAllocator = TAllocatorTraits<AllocatorType>::IsChunkableResource;
	template <class AllocatorType>
	concept CNotChunkableResourceAllocator = !TAllocatorTraits<AllocatorType>::IsChunkableResource;

	template <class AllocatorType>
	concept CShareableResourceAllocator = TAllocatorTraits<AllocatorType>::IsShareableResource;
	template <class AllocatorType>
	concept CNotShareableResourceAllocator = !TAllocatorTraits<AllocatorType>::IsShareableResource;

	template <class AllocatorType>
	concept CSwappableResourceAllocator = TAllocatorTraits<AllocatorType>::IsSwappableResource;
	template <class AllocatorType>
	concept CNotSwappableResourceAllocator = !TAllocatorTraits<AllocatorType>::IsSwappableResource;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MakeTrivialRelocationFunction

	template <CScalarObject ElementType, CUnsignedInteger IndexType>
	AEON_FORCEINLINE auto MakeTrivialRelocationFunction()
	requires CTriviallyCopyable<ElementType>
	{
		auto Function =
			[] (ElementType* OldStoragePtr, IndexType OldElementCount, ElementType* NewStoragePtr, IndexType NewElementCount, voidptr)
			{
				AeonMemory::MemMove(NewStoragePtr, OldStoragePtr, AeonMath::Min(NewElementCount, OldElementCount));
			};
		return Function;
	};

}

#endif
