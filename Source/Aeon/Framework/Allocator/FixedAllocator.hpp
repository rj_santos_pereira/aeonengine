#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_ALLOCATOR_FIXEDALLOCATOR
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_ALLOCATOR_FIXEDALLOCATOR

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Allocator/Allocator.hpp"

#if AEON_COMPILER_MSVC
#	pragma warning(push)
#	pragma warning(disable: 4996)
#endif

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  EFixedAllocatorStorageMode

	enum struct EFixedAllocatorStorageMode
	{
		Static,
		Dynamic,
	};

	namespace ZzInternal
	{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  TCompressedInteger

		template <CInteger IntType, IntType IntMaxValue>
		struct ZzInternal_TCompressedInteger
		{
			static constexpr bool ZzInternal_SignValue = TIsSigned<IntType>::Value;

			static constexpr szint ZzInternal_MaxBitSize = AeonMath::Log2(AeonMath::Abs(IntMaxValue))
														   + szint(ZzInternal_SignValue);
			static constexpr szint ZzInternal_MagnitudeValue = AeonMath::NextPowerOfTwo(ZzInternal_MaxBitSize)
															   / AeonMemory::PlatformByteBitSize;

			static_assert(ZzInternal_MagnitudeValue <= Aeon::SizeOf(IntType));

			using Type = TMakeSignMagnitudeInteger<ZzInternal_SignValue, ZzInternal_MagnitudeValue>;

		//	static constexpr szint Size = ZzInternal_MagnitudeValue;
			static constexpr szint BitSize = ZzInternal_MaxBitSize;
		};

		template <CInteger IntType, IntType IntMaxValue>
		using TCompressedInteger = ZzInternal_TCompressedInteger<IntType, IntMaxValue>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GFixedAllocatorStorage

		template <szint BufferSizeValue, algnint BufferAlignmentValue, EFixedAllocatorStorageMode>
		struct GFixedAllocatorStorage;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GFixedAllocatorStorage<Static>

		template <szint BufferSizeValue, algnint BufferAlignmentValue>
		struct GFixedAllocatorStorage<BufferSizeValue, BufferAlignmentValue, EFixedAllocatorStorageMode::Static>
		{
			explicit GFixedAllocatorStorage() = default;

			GFixedAllocatorStorage(GFixedAllocatorStorage const&) = delete;
			GFixedAllocatorStorage(GFixedAllocatorStorage&&)
				: GFixedAllocatorStorage()
			{
			}

			GFixedAllocatorStorage& operator=(GFixedAllocatorStorage const&) = delete;
			GFixedAllocatorStorage& operator=(GFixedAllocatorStorage&&)
			{
				LastAllocBlockPtr = nullptr;
				return *this;
			}

			void Init()
			{
				// HACK Marking the storage as initialized by setting this to non-null,
				//  taking advantage of the fact that the allocator logic never needs to null this value; not very elegant, but (space) efficient...
				LastAllocBlockPtr = Buffer;
			}
			bool HasInit() const
			{
				return LastAllocBlockPtr;
			}

			// HACK Neither MSVC nor GCC like functional casts inside alignas expressions
			alignas(static_cast<szint>(BufferAlignmentValue)) byte Buffer[BufferSizeValue];
			// Offset to the block from which to start the search for an empty block in the next Allocate/Reallocate call.
			byteptr LastAllocBlockPtr = nullptr;
		};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GFixedAllocatorStorage<Dynamic>

		template <szint BufferSizeValue, algnint BufferAlignmentValue>
		struct GFixedAllocatorStorage<BufferSizeValue, BufferAlignmentValue, EFixedAllocatorStorageMode::Dynamic>
		{
			explicit GFixedAllocatorStorage()
				: Buffer(nullptr)
				, LastAllocBlockPtr(nullptr)
			{
			}

			GFixedAllocatorStorage(GFixedAllocatorStorage const&) = delete;
			GFixedAllocatorStorage(GFixedAllocatorStorage&& Other)
				: Buffer(Exchange(Other.Buffer, nullptr))
				, LastAllocBlockPtr(Exchange(Other.LastAllocBlockPtr, nullptr))
			{
			}

			~GFixedAllocatorStorage()
			{
				if (HasInit())
				{
					AeonMemory::MemDealloc(Buffer);
				}
			}

			GFixedAllocatorStorage& operator=(GFixedAllocatorStorage Other)
			{
				using Aeon::Swap;
				Swap(Buffer, Other.Buffer);
				Swap(LastAllocBlockPtr, Other.LastAllocBlockPtr);
				return *this;
			}

			void Init()
			{
				if (!HasInit())
				{
					Buffer = AeonMemory::MemAlloc<byte[]>(BufferSizeValue, BufferAlignmentValue);
					// HACK Marking the storage as initialized by setting this to non-null,
					//  taking advantage of the fact that the allocator logic never needs to null this value; not very elegant, but (space) efficient...
					LastAllocBlockPtr = Buffer;
				}
			}
			bool HasInit() const
			{
				return LastAllocBlockPtr;
			}

			byteptr Buffer;
			// Offset to the block from which to start the search for an empty block in the next Allocate/Reallocate call.
			byteptr LastAllocBlockPtr;
		};
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GFixedAllocator

	// TODO maybe reimplement this using a bitset, this is quite complex and wastes memory, compare performance

	template <CScalarObject ObjType, szint ElemCountValue, CUnsignedInteger IdxType, EFixedAllocatorStorageMode StorageModeValue>
	requires CMoveableElseCopyable<ObjType>
	class [[deprecated("Prefer using GStaticAllocator")]] GFixedAllocator
	{
		// This allocator implements a simple memory allocation algorithm, Next Fit,
		// for finding memory blocks with the requested size within the fixed-size buffer,
		// effectively acting as a MemAlloc call within the boundaries of that buffer.
		// This way, it can serve multiple allocation requests, and is swappable with other allocator instances if using a dynamic backing buffer,
		// but is less performant and occupies more memory than a GStaticAllocator for the same number of elements.

	public:
		using ObjectType = TRemoveConstVolatile<ObjType>;
		using IndexType = IdxType;
		using RelocationFunctionType = void(*)(ObjectType*, IndexType, ObjectType*, IndexType, voidptr);

		explicit GFixedAllocator() = default;

		GFixedAllocator(GFixedAllocator const&) = delete;
		GFixedAllocator(GFixedAllocator&&) = default;

		GFixedAllocator& operator=(GFixedAllocator Other)
		{
			using Aeon::Swap;
			Swap(Storage, Other.Storage);
			return *this;
		}

		template <class OtherObjType>
		GFixedAllocator(GFixedAllocator<OtherObjType, ElemCountValue, IdxType, StorageModeValue>&& Other)
		requires (StorageModeValue == EFixedAllocatorStorageMode::Dynamic)
		{
			// In other words, this must be a freshly created instance, with no operations having been requested to it
			AEON_ASSERT(!Other.Storage.HasInit());
		}

		GFixedAllocator OnContainerCopy() const
		{
			return GFixedAllocator();
		}

		GFixedAllocator OnContainerMove()
		requires (StorageModeValue == EFixedAllocatorStorageMode::Static)
		{
			return GFixedAllocator();
		}
		GFixedAllocator OnContainerMove()
		requires (StorageModeValue == EFixedAllocatorStorageMode::Dynamic)
		{
			return Move(*this);
		}

		ObjectType* Allocate(IndexType ObjectCount)
		{
			if (!Storage.HasInit())
			{
				InitStorage_Internal();
			}

			// If the size is 0, then interpret it as a request to not allocate any memory at this time and return immediately
			if (ObjectCount == 0)
			{
				return nullptr;
			}

			byteptr const LastBlockPtr = Storage.LastAllocBlockPtr;
			byteptr const PastTheLastPtr = Storage.Buffer + StorageSize;

			byteptr CurrBlockPtr = LastBlockPtr;
			bool HasFoundBlock = false;

			BlockIndexType PrevSlotCount = 0;
			do
			{
				// Check if this block is empty.
				DBlockInfo* CurrBlockInfo = AeonMemory::MemAccess<DBlockInfo>(CurrBlockPtr);
				if (CurrBlockInfo->PrevBlockSlotCount == EmptyBlockMask)
				{
					// Since it is empty, check if this block has enough size.
					BlockIndexType EmptySlotCount = CurrBlockInfo->BlockSlotCount;
					if (EmptySlotCount >= ObjectCount)
					{
						// Update slot count with the size of the storage that will be used,
						// and update the previous slot count with the distance to the previous block, if there was one.
						CurrBlockInfo->BlockSlotCount = BlockIndexType(ObjectCount);
						CurrBlockInfo->PrevBlockSlotCount = PrevSlotCount;

						// Assign Storage.LastAllocBlockPtr to this block (instead of the next one),
						// so that if the next allocation call uses the next empty block,
						// it can obtain the slot count of this block through PrevSlotCount.
						Storage.LastAllocBlockPtr = CurrBlockPtr;

						EmptySlotCount -= CurrBlockInfo->BlockSlotCount;

						// If there is any leftover space, create a new block with that size and mark it as empty.
						// Note that if there is any block next to this one, it is not empty,
						// since Reallocate/Deallocate coalesce sequential empty blocks into a single empty one.
						if (EmptySlotCount > 0)
						{
							byteptr EmptyBlockPtr = CurrBlockPtr + ObjectCount * SlotSize;

							AeonMemory::MemConstruct<DBlockInfo>(EmptyBlockPtr, DBlockInfo{ EmptySlotCount, EmptyBlockMask });

							// We don't need to coalesce blocks at this point,
							// but we do need to adjust the block following this empty one, if it exists.
							byteptr NextBlockPtr = EmptyBlockPtr + EmptySlotCount * SlotSize;
							if (NextBlockPtr < PastTheLastPtr)
							{
								DBlockInfo* NextBlockInfo = AeonMemory::MemAccess<DBlockInfo>(NextBlockPtr);

								NextBlockInfo->PrevBlockSlotCount = EmptySlotCount;
							}
						}

						// Note that if there is no new empty block, we don't need to update any block that might exist after this one,
						// as the size of this block is still the same (it simply changed from empty to in-use).

						HasFoundBlock = true;
						break;
					}
				}

				// If we haven't found the block yet, advance to the next one.
				CurrBlockPtr += CurrBlockInfo->BlockSlotCount * SlotSize;
				if (CurrBlockPtr < PastTheLastPtr)
				{
					PrevSlotCount = CurrBlockInfo->BlockSlotCount;
				}
				// When past the end of the buffer, continue from the start of the buffer instead
				else
				{
					CurrBlockPtr = Storage.Buffer;
					PrevSlotCount = 0;
				}
			}
			while (CurrBlockPtr != LastBlockPtr);

			// Couldn't find block with enough size for requested allocation.
			if (!HasFoundBlock)
			{
				return nullptr;
			}

			byteptr StoragePtr = CurrBlockPtr + ObjectOffset;

			// This should never happen
			AEON_ASSERT(AeonMemory::IsBlockPointerValid(StoragePtr, ObjectAlignment));

			return AeonMemory::StartObjectArrayLifetime<ObjectType[]>(StoragePtr, ObjectCount);
		}
		AEON_NODISCARD ObjectType* Reallocate(ObjectType* OldStoragePtr, IndexType OldObjectCount, IndexType NewObjectCount,
											  RelocationFunctionType RelocationFunction, voidptr InstanceData)
		{
			if (!Storage.HasInit())
			{
				InitStorage_Internal();
			}

			if (OldObjectCount == NewObjectCount)
			{
				return OldStoragePtr;
			}

			if (NewObjectCount == 0)
			{
				Deallocate(OldStoragePtr);
				return nullptr;
			}

			if (OldObjectCount == 0)
			{
				return Allocate(NewObjectCount);
			}

			byteptr CurrBlockPtr = reinterpret_cast<byteptr>(OldStoragePtr) - ObjectOffset;
			byteptr const PastTheLastPtr = Storage.Buffer + StorageSize;

			DBlockInfo* CurrBlockInfo = AeonMemory::MemAccess<DBlockInfo>(CurrBlockPtr);

			AEON_ASSERT(CurrBlockInfo->BlockSlotCount == OldObjectCount);

			// If the new size is smaller than the old one,
			// we just need to adjust the size of this block, and create a new empty block after this one.
			if (OldObjectCount > NewObjectCount)
			{
				CurrBlockInfo->BlockSlotCount = NewObjectCount;

				// Create a new empty block with the extra slots we are no longer using.
				BlockIndexType EmptySlotCount = BlockIndexType(OldObjectCount - NewObjectCount);

				byteptr EmptyBlockPtr = CurrBlockPtr + NewObjectCount * SlotSize;
				AeonMemory::MemConstruct<DBlockInfo>(EmptyBlockPtr, DBlockInfo{ EmptySlotCount, EmptyBlockMask });

				// If there is any block after the current old one, we have to adjust sizes.
				CoalesceBlocks_Internal(EmptyBlockPtr);

				return AeonMemory::StartObjectArrayLifetime<ObjectType[]>(OldStoragePtr, NewObjectCount);
			}

			// Otherwise, we'll try to expand this block in-place.
			ObjectType* NewStoragePtr = nullptr;

			IndexType UsableSlotCount = OldObjectCount;

			// If there is any block after this one, check if we can use it.
			{
				byteptr NextBlockPtr = CurrBlockPtr + CurrBlockInfo->BlockSlotCount * SlotSize;
				if (NextBlockPtr != PastTheLastPtr)
				{
					// If the next block is empty, try to use it.
					DBlockInfo* NextBlockInfo = AeonMemory::MemAccess<DBlockInfo>(NextBlockPtr);
					if (NextBlockInfo->PrevBlockSlotCount == EmptyBlockMask)
					{
						UsableSlotCount += NextBlockInfo->BlockSlotCount;
					}
				}
			}

			// If we can expand with just the next empty block in-place, adjust the size and use the old pointer.
			if (UsableSlotCount >= NewObjectCount)
			{
				// We have enough memory, adjust the slot sizes.
				CurrBlockInfo->BlockSlotCount = NewObjectCount;

				NewStoragePtr = AeonMemory::StartObjectArrayLifetime<ObjectType[]>(OldStoragePtr, NewObjectCount);
			}
			// Otherwise, check if we can expand with both the next and the previous empty blocks in-place
			else
			{
				// If there is any block before this one, check if we can use it.
				{
					byteptr PrevBlockPtr = CurrBlockPtr - CurrBlockInfo->PrevBlockSlotCount * SlotSize;
					if (PrevBlockPtr != CurrBlockPtr)
					{
						DBlockInfo* PrevBlockInfo = AeonMemory::MemAccess<DBlockInfo>(PrevBlockPtr);
						// If the next block is empty, try to use it.
						if (PrevBlockInfo->PrevBlockSlotCount == EmptyBlockMask)
						{
							UsableSlotCount += PrevBlockInfo->BlockSlotCount;
						}
					}
				}

				// If we can expand with both the next and previous blocks, adjust the size, relocate the data,
				// and adjust the pointer to the previous block.
				if (UsableSlotCount >= NewObjectCount)
				{
					// Note that if we enter here, we already know that there is a previous block.
					// Overwrite CurrBlockInfo with the info from that previous block.
					CurrBlockPtr -= CurrBlockInfo->PrevBlockSlotCount * SlotSize;
					CurrBlockInfo = AeonMemory::MemAccess<DBlockInfo>(CurrBlockPtr);

					// Adjust the block element size
					CurrBlockInfo->BlockSlotCount = NewObjectCount;

					byteptr NewOffsetStoragePtr = CurrBlockPtr + ObjectOffset;

					// We need to compute an oversize element count that is potentially greater than the new size,
					// so that addressing elements in the storage of the old buffer is well-defined, as the arrays will overlap,
					// and the old array technically is no longer valid once the new one is created.
					// We also need to compute it through the difference of pointers,
					// as additional storage is required for the DBlockInfo objects.
					// Hopefully, at higher optimization levels, all the code associated with this is elided.
					diffint StorageByteDiff = reinterpret_cast<byteptr>(OldStoragePtr) - NewOffsetStoragePtr;

					IndexType StorageElemDiff = IndexType(StorageByteDiff / ObjectSize);
				//	IndexType StorageSlotDiff = CurrBlockInfo->BlockSlotCount;

					// CHECKME if this ever fails, just make StorageElemDiff = StorageByteDiff / ObjectSize + szint(StorageByteDiff % ObjectSize > 0)
					AEON_ASSERT(StorageByteDiff % ObjectSize == 0);
				//	AEON_UNUSED(StorageSlotDiff);

					IndexType OversizeObjectCount = OldObjectCount + StorageElemDiff;
				//	IndexType OversizeObjectCount = OldObjectCount + StorageSlotDiff;

					NewStoragePtr = AeonMemory::StartObjectArrayLifetime<ObjectType[]>(NewOffsetStoragePtr,
																					   AeonMath::Max(NewObjectCount, OversizeObjectCount));

					// CHECKME does OldStoragePtr need this std::launder/MemAccess?
					RelocationFunction(AeonMemory::MemAccess<ObjectType>(OldStoragePtr), OldObjectCount, NewStoragePtr, NewObjectCount, InstanceData);

					// Recreate the pointer to the new array with the correct size now.
					NewStoragePtr = AeonMemory::StartObjectArrayLifetime<ObjectType[]>(NewOffsetStoragePtr, NewObjectCount);
				}
			}

			// If we managed to expand the block in-place, create a block info with any empty slots, if present
			if (NewStoragePtr)
			{
				// If there is some leftover space, create a new empty block.
				BlockIndexType EmptySlotCount = BlockIndexType(UsableSlotCount - NewObjectCount);
				if (EmptySlotCount > 0)
				{
					byteptr EmptyBlockPtr = CurrBlockPtr + NewObjectCount * SlotSize;
					AeonMemory::MemConstruct<DBlockInfo>(EmptyBlockPtr, DBlockInfo{ EmptySlotCount, EmptyBlockMask });
				}

				// If there is a block next to the new empty one (or the current one, if there is no leftover space),
				// adjust its PrevBlockSlotCount with the size of the block that precedes it, if its non-empty.
				byteptr NextBlockPtr = CurrBlockPtr + UsableSlotCount * SlotSize;
				if (NextBlockPtr < PastTheLastPtr)
				{
					DBlockInfo* NextBlockInfo = Memory::MemAccess<DBlockInfo>(NextBlockPtr);
					if (NextBlockInfo->PrevBlockSlotCount != EmptyBlockMask)
					{
						NextBlockInfo->PrevBlockSlotCount = EmptySlotCount > 0 ? EmptySlotCount : UsableSlotCount;
					}
				}
			}
			// Otherwise, no success so far, so just Allocate normally and relocate the data
			else
			{
				NewStoragePtr = Allocate(NewObjectCount);

				RelocationFunction(OldStoragePtr, OldObjectCount, NewStoragePtr, NewObjectCount, InstanceData);

				// Now deallocate the old storage block
				Deallocate(OldStoragePtr);
			}

			return NewStoragePtr;
		}
		void Deallocate(ObjectType* StoragePtr)
		{
			AEON_ASSERT(Storage.HasInit());
			
			// If the pointer is null, simply no-op, this simplifies some code on containers.
			if (StoragePtr == nullptr)
			{
				return;
			}

			// Not much to do, we simply need to coalesce the current block with any preceding and/or succeeding it;
			// we are careful to keep the block's information before calling the method,
			// as that will enable coalescing this block with any block preceding it.
			// The method will always overwrite the current block's information at the end.
			byteptr CurrBlockPtr = reinterpret_cast<byteptr>(StoragePtr) - ObjectOffset;

			CoalesceBlocks_Internal(CurrBlockPtr);
		}

		bool IsAllocationSizeSupported(IndexType ObjectCount) const
		{
			// NOTE This simply checks whether an allocation requested with ObjectCount can be fulfilled by an "unused" allocator,
			//  not whether such an allocation requested can be fulfilled by an allocator instance in its current state.
			//  This is by design.
			return szint(ObjectCount) <= SlotCount;
		}
		IndexType RecommendedStartingAllocationSize() const
		{
			constexpr IndexType AllocationSize = RecommendedStartingAllocationSize_Internal();
			return AllocationSize;
		}

		bool ShouldIncreaseAllocationSize(IndexType OldObjectCount, IndexType NewObjectCount) const
		{
			return OldObjectCount < NewObjectCount;
		}
		bool ShouldDecreaseAllocationSize(IndexType OldObjectCount, IndexType NewObjectCount) const
		{
			constexpr IndexType MinimumObjectCount = RecommendedStartingAllocationSize_Internal();
			// This ensures we don't make redundant calls to Realloc
			bool CanDecreaseAllocationSize = MinimumObjectCount < OldObjectCount;
			// Simple deallocation policy: ensure we keep at most four times the amount of used memory, and reduce to half when it reaches that limit;
			// this avoids continuous reallocation when adding and removing elements at the same rate near the threshold
			return CanDecreaseAllocationSize && NewObjectCount <= (OldObjectCount >> 2);
		}

		IndexType ComputeAllocationSizeIncrease(IndexType OldObjectCount) const
		{
			return AeonMath::Max(OldObjectCount << 1, RecommendedStartingAllocationSize());
		}
		IndexType ComputeAllocationSizeDecrease(IndexType OldObjectCount) const
		{
			return AeonMath::Max(OldObjectCount >> 1, RecommendedStartingAllocationSize());
		}

		bool operator==(GFixedAllocator const& Other) const
		{
			// Should always be false for two different instances; using unary plus to force decay array into pointer
			return +Storage.Buffer == +Other.Storage.Buffer;
		}

	//	friend void Swap(GFixedAllocator& Allocator1, GFixedAllocator& Allocator2)
	//	{
	//		if constexpr (StorageModeValue == EFixedAllocatorStorageMode::Dynamic)
	//		{
	//			using Aeon::Swap;
//
	//			Swap(Allocator1.Storage.Buffer, Allocator2.Storage.Buffer);
	//			Swap(Allocator1.Storage.LastAllocBlockPtr, Allocator2.Storage.LastAllocBlockPtr);
	//		}
	//	}

	private:
		using BlockIndexType = typename ZzInternal::TCompressedInteger<IdxType, ElemCountValue>::Type;
		static constexpr szint BlockIndexBitSize = ZzInternal::TCompressedInteger<IdxType, ElemCountValue>::BitSize;

		static constexpr BlockIndexType EmptyBlockMask = AeonMath::MakeBitMask<BlockIndexType, BlockIndexBitSize>();

		struct DBlockInfo
		{
			// Number of slots that this block occupies
			BlockIndexType BlockSlotCount : BlockIndexBitSize;
			// Distance to the previous block, or flag indicating an empty slot
			BlockIndexType PrevBlockSlotCount : BlockIndexBitSize; // TODO better name for member
		};

		static_assert(VIsTriviallyDestructible<DBlockInfo>);

		static constexpr IndexType RecommendedStartingAllocationSize_Internal()
		{
			IndexType IdealStartingObjectCount = AeonMath::Max<IndexType>(IndexType(AeonMemory::PlatformCacheLinePrefetchSize / ObjectSize), 1);
			return AeonMath::Min(IdealStartingObjectCount, IndexType(SlotCount));
		}

		void InitStorage_Internal()
		{
			Storage.Init();

			// Mark the whole storage as empty
			AeonMemory::MemConstruct<DBlockInfo>(Storage.Buffer, DBlockInfo{ SlotCount, EmptyBlockMask });
			Storage.LastAllocBlockPtr = Storage.Buffer;
		}

		void CoalesceBlocks_Internal(byteptr CurrBlockPtr)
		{
			byteptr const PastTheLastPtr = Storage.Buffer + StorageSize;
			bool NeedsUpdateLastAllocPtr = CurrBlockPtr == Storage.LastAllocBlockPtr;

			DBlockInfo* EmptyBlockInfo = AeonMemory::MemAccess<DBlockInfo>(CurrBlockPtr);

			// Immediately get the next block pointer, since in the next branch we may change the pointer to the empty block info.
			byteptr NextBlockPtr = CurrBlockPtr + EmptyBlockInfo->BlockSlotCount * SlotSize;

			// Start accumulating the total empty memory,
			// we will try to coalesce this newly empty block with any empty blocks that may exist before or after this one.
			BlockIndexType EmptyBlockCount = EmptyBlockInfo->BlockSlotCount;

			// If there is a block before this one, check it to see if it's empty;
			// note that if this block itself is an empty block (when called from Reallocate), then the previous block will not be an empty block.
			if (EmptyBlockInfo->PrevBlockSlotCount != 0 && EmptyBlockInfo->PrevBlockSlotCount != EmptyBlockMask)
			{
				byteptr PrevBlockPtr = CurrBlockPtr - EmptyBlockInfo->PrevBlockSlotCount * SlotSize;
				DBlockInfo* PrevBlockInfo = AeonMemory::MemAccess<DBlockInfo>(PrevBlockPtr);

				// If the previous block is empty, we want to update that block instead,
				// so add the previous block size to the accumulator, and update EmptyBlockInfo to point to the previous block instead.
				if (PrevBlockInfo->PrevBlockSlotCount == EmptyBlockMask)
				{
					EmptyBlockCount += PrevBlockInfo->BlockSlotCount;

					EmptyBlockInfo = PrevBlockInfo;
				}
				// If it is not empty, make Storage.LastAllocBlockPtr point to it if the current one was the last one to be allocated.
				else if (NeedsUpdateLastAllocPtr)
				{
					Storage.LastAllocBlockPtr = PrevBlockPtr;
					NeedsUpdateLastAllocPtr = false;
				}
			}

			// If there is a block after this one, check it to see if it's empty.
			if (NextBlockPtr < PastTheLastPtr)
			{
				DBlockInfo* NextBlockInfo = AeonMemory::MemAccess<DBlockInfo>(NextBlockPtr);

				// If the next block is empty, we want to coalesce it with the empty block (regardless if it's the current or the previous one),
				// so add the next block size to the accumulator.
				if (NextBlockInfo->PrevBlockSlotCount == EmptyBlockMask)
				{
					EmptyBlockCount += NextBlockInfo->BlockSlotCount;

					// We update the NextBlockPtr for the next check, since if we entered this condition,
					// we also want to update the block following this one, if there is one.
					NextBlockPtr += NextBlockInfo->BlockSlotCount * SlotSize;
					if (NextBlockPtr < PastTheLastPtr)
					{
						NextBlockInfo = AeonMemory::MemAccess<DBlockInfo>(NextBlockPtr);
					}
					else
					{
						// If there is no block following this one, we are done coalescing blocks.
						goto FinishCoalesceBlocks;
					}
				}

				// If the block pointed to by NextBlockPtr is valid, it's necessarily a non-empty block at this point,
				// and we need to update its PrevBlockSlotCount, since the size of the newly empty block potentially changed.
				NextBlockInfo->PrevBlockSlotCount = EmptyBlockCount;

				// If we still need to update LastAllocBlockPtr, make it point to this one,
				// as we probably could not find the block before this new empty one.
				if (NeedsUpdateLastAllocPtr)
				{
					Storage.LastAllocBlockPtr = NextBlockPtr;
					NeedsUpdateLastAllocPtr = false;
				}
			}

		FinishCoalesceBlocks:
			// If we still need to update LastAllocBlockPtr, we reached the end of the buffer while coalescing blocks,
			// so make LastAllocBlockPtr point to the start of the buffer.
			if (NeedsUpdateLastAllocPtr)
			{
				Storage.LastAllocBlockPtr = Storage.Buffer;
			}

			// Finally update the info in the empty block.
			EmptyBlockInfo->BlockSlotCount = EmptyBlockCount;
			EmptyBlockInfo->PrevBlockSlotCount = EmptyBlockMask;
		}

		// RequirementList for the ObjectOffset computation to be correct
		static_assert(AeonMath::IsPowerOfTwo(Aeon::SizeOf(DBlockInfo)));

		static constexpr szint ObjectSize = Aeon::SizeOf(ObjectType);
		static constexpr algnint ObjectAlignment = Aeon::AlignOf(ObjectType);
		static constexpr szint ObjectOffset = AeonMath::Max(Aeon::SizeOf(DBlockInfo), szint(ObjectAlignment));

		// A slot is a unit of space that can support both an ObjectType object and a DBlockInfo object,
		// since both objects need to be properly aligned, but alignment of a DBlockInfo will always be half of its size,
		// we use the maximum between the alignment of ObjectType and size of DBlockInfo.
		static constexpr szint SlotSize = ObjectSize + ObjectOffset;
		static constexpr szint SlotCount = ElemCountValue;

		static constexpr szint StorageSize = SlotCount * SlotSize;
		static constexpr algnint StorageAlignment = AeonMath::Max(Aeon::AlignOf(DBlockInfo), ObjectAlignment);

		static_assert(0 < SlotCount && SlotCount <= AeonMath::Min<szint>(TNumericProperties<IndexType>::Maximum - 1,
																		 TNumericProperties<szint>::Maximum / SlotSize));

		ZzInternal::GFixedAllocatorStorage<StorageSize, StorageAlignment, StorageModeValue> Storage;

		template <CScalarObject OtherObjType, szint OtherElemCountValue, CUnsignedInteger OtherIdxType, EFixedAllocatorStorageMode OtherStorageModeValue>
		requires CMoveableElseCopyable<OtherObjType>
		friend class GFixedAllocator;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GFixedStaticAllocator

	template <CScalarObject ObjType, szint ElemCountValue, CUnsignedInteger IdxType = uint32>
	using GFixedStaticAllocator = GFixedAllocator<ObjType, ElemCountValue, IdxType, EFixedAllocatorStorageMode::Static>;

//  TAllocatorTraits

	template <CScalarObject ObjType, szint ElemCountValue, CUnsignedInteger IdxType>
	struct ZzInternal_TAllocatorTraits<GFixedAllocator<ObjType, ElemCountValue, IdxType, EFixedAllocatorStorageMode::Static>>
	{
		static constexpr bool UsesGlobalResource = false;
		static constexpr bool IsChunkableResource = true;
		static constexpr bool IsShareableResource = false;
		static constexpr bool IsSwappableResource = false;
	};

//  TRebindAllocator

	template <CScalarObject ObjType, szint ElemCountValue, CUnsignedInteger IdxType, CScalarObject OtherObjType>
	struct ZzInternal_TRebindAllocator<GFixedAllocator<ObjType, ElemCountValue, IdxType, EFixedAllocatorStorageMode::Static>, OtherObjType>
	{
		using Type = GFixedAllocator<OtherObjType, ElemCountValue, IdxType, EFixedAllocatorStorageMode::Static>;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GFixedDynamicAllocator

	template <CScalarObject ObjType, szint ElemCountValue, CUnsignedInteger IdxType = uint32>
	using GFixedDynamicAllocator = GFixedAllocator<ObjType, ElemCountValue, IdxType, EFixedAllocatorStorageMode::Dynamic>;

//  TAllocatorTraits

	template <CScalarObject ObjType, szint ElemCountValue, CUnsignedInteger IdxType>
	struct ZzInternal_TAllocatorTraits<GFixedAllocator<ObjType, ElemCountValue, IdxType, EFixedAllocatorStorageMode::Dynamic>>
	{
		static constexpr bool UsesGlobalResource = false;
		static constexpr bool IsChunkableResource = true;
		static constexpr bool IsShareableResource = false;
		static constexpr bool IsSwappableResource = true;
	};

//  TRebindAllocator

	template <CScalarObject ObjType, szint ElemCountValue, CUnsignedInteger IdxType, CScalarObject OtherObjType>
	struct ZzInternal_TRebindAllocator<GFixedAllocator<ObjType, ElemCountValue, IdxType, EFixedAllocatorStorageMode::Dynamic>, OtherObjType>
	{
		using Type = GFixedAllocator<OtherObjType, ElemCountValue, IdxType, EFixedAllocatorStorageMode::Dynamic>;
	};
}

#if AEON_COMPILER_MSVC
#	pragma warning(pop)
#endif

#endif