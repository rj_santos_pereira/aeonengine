#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_ALLOCATOR_STATICALLOCATOR
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_ALLOCATOR_STATICALLOCATOR

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Allocator/Allocator.hpp"

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GStaticAllocator

	template <CScalarObject ObjType, szint ElemCountValue, CUnsignedInteger IdxType = uint32>
	class GStaticAllocator
	{
	public:
		using ObjectType = TRemoveConstVolatile<ObjType>;
		using IndexType = IdxType;
		using RelocationFunctionType = void(*)(ObjectType*, IndexType, ObjectType*, IndexType, voidptr);

		explicit GStaticAllocator() = default;

		GStaticAllocator(GStaticAllocator const&) = delete;
		GStaticAllocator(GStaticAllocator&&)
			: GStaticAllocator()
		{
		}

		GStaticAllocator& operator=(GStaticAllocator const&) = delete;
		GStaticAllocator& operator=(GStaticAllocator&&)
		{
			IsInUse = false;
			return *this;
		}

		GStaticAllocator OnContainerCopy() const
		{
			return GStaticAllocator();
		}

		GStaticAllocator OnContainerMove()
		{
			return GStaticAllocator();
		}

		AEON_NODISCARD ObjectType* Allocate(IndexType ObjectCount)
		{
			AEON_FAILSAFE(!IsInUse, return nullptr);

			if (!AeonMath::IsClamped<IndexType>(ObjectCount, 1, ElementCount))
			{
				return nullptr;
			}

			ObjectType* StoragePtr = AeonMemory::StartObjectArrayLifetime<ObjectType[]>(Buffer, ObjectCount);
			IsInUse = true;

			return StoragePtr;
		}
		AEON_NODISCARD ObjectType* Reallocate(ObjectType* OldStoragePtr, IndexType OldObjectCount, IndexType NewObjectCount,
											  RelocationFunctionType, voidptr)
		{
			// If the size is 0, then interpret it as a request to deallocate the current one and return immediately
			if (NewObjectCount == 0)
			{
				Deallocate(OldStoragePtr);
				return nullptr;
			}

			// If there was no prior allocation, make a regular one now and return immediately
			if (OldObjectCount == 0)
			{
				return Allocate(NewObjectCount);
			}

			AEON_FAILSAFE(IsInUse && reinterpret_cast<byteptr>(OldStoragePtr) == +Buffer, return nullptr);

			IsInUse = false;
			return Allocate(NewObjectCount);
		}
		void Deallocate(ObjectType* StoragePtr)
		{
			AEON_FAILSAFE(IsInUse && reinterpret_cast<byteptr>(StoragePtr) == +Buffer, return);

			IsInUse = false;
			return;
		}

		bool IsAllocationSizeSupported(IndexType ObjectCount) const
		{
			return szint(ObjectCount) <= ElementCount;
		}
		IndexType RecommendedStartingAllocationSize() const
		{
			return ElementCount;
		}

		bool ShouldIncreaseAllocationSize(IndexType OldObjectCount, IndexType NewObjectCount) const
		{
			return OldObjectCount < NewObjectCount;
		}
		bool ShouldDecreaseAllocationSize(IndexType OldObjectCount, IndexType NewObjectCount) const
		{
			AEON_UNUSED(OldObjectCount, NewObjectCount);
			return false;
		}

		IndexType ComputeAllocationSizeIncrease(IndexType OldObjectCount) const
		{
			AEON_UNUSED(OldObjectCount);
			return RecommendedStartingAllocationSize();
		}
		IndexType ComputeAllocationSizeDecrease(IndexType OldObjectCount) const
		{
			AEON_UNUSED(OldObjectCount);
			return RecommendedStartingAllocationSize();
		}

		bool operator==(GStaticAllocator const& Other) const
		{
			return +Buffer == +Other.Buffer;
		}

	private:
		static constexpr szint ObjectSize = Aeon::SizeOf(ObjectType);
		static constexpr szint StorageSize = ElemCountValue * ObjectSize;

		static constexpr szint ElementCount = ElemCountValue;

		static_assert(0 < ElementCount && ElementCount <= AeonMath::Min<szint>(TNumericProperties<IndexType>::Maximum - 1,
																			   TNumericProperties<szint>::Maximum / ObjectSize));

		alignas(ObjectType) byte Buffer[StorageSize];
		bool IsInUse = false;
	};

// TAllocatorTraits

	template <CScalarObject ObjType, szint ElemCountValue, CUnsignedInteger IdxType>
	struct ZzInternal_TAllocatorTraits<GStaticAllocator<ObjType, ElemCountValue, IdxType>>
	{
		static constexpr bool UsesGlobalResource = false;
		static constexpr bool IsChunkableResource = false;
		static constexpr bool IsShareableResource = false;
		static constexpr bool IsSwappableResource = false;
	};

// TRebindAllocator

	// NOTE GStaticAllocator can only rebind to the same object type (this allows Filter and Map in containers to work)
	template <CScalarObject ObjType, szint ElemCountValue, CUnsignedInteger IdxType, CScalarObject OtherObjType>
	struct ZzInternal_TRebindAllocator<GStaticAllocator<ObjType, ElemCountValue, IdxType>, OtherObjType>
	{
		using Type = GStaticAllocator<OtherObjType, ElemCountValue, IdxType>;
	};
}

#endif