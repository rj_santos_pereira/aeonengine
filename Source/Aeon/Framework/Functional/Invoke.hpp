#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_FUNCTIONAL_INVOKE
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_FUNCTIONAL_INVOKE

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Memory/MemoryCommon.hpp"

// TODO documentation, tons of it

namespace Aeon
{
	namespace ZzInternal
	{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  TIsClassPointerLikeObject

		// NOTE We allow CDereferenceableManagedPointer to be able to pass smart pointers (or pointer-like classes) to a class object
		//  to Invoke/BindInvoke, with that class object's type as the underlying class for the callable, when callable has member-pointer type.
		//  We use CDereferenceableManagedPointer to disambiguate whether we should treat a class object as the object itself
		//  or as a pointer to the object.
		//  Otherwise, if we would use an object Obj of class T for which the expressions '*Obj' and 'Obj ? void() ? void()' are well-formed,
		//  the implementation would always treat the object as a "pointer" to a class type, instead of as the class type itself,
		//  of which the callable is a member.
		//  This implies that Invoke/BindInvoke have the limitation that a member function of an IManagedPointer cannot be bound as the callable.

		template <class Type, class = void>
		struct TIsClassPointerLikeObject : TFalseConstant {};
		template <class Type>
		struct TIsClassPointerLikeObject<Type, TEnableIf<CClassPointer<Type>>> : TTrueConstant {};
		template <class Type>
		struct TIsClassPointerLikeObject<Type, TEnableIf<CDereferenceableManagedPointer<Type>>> : TIsClass<typename Type::ObjectType> {};

		template <class Type>
		inline constexpr bool VIsClassPointerLikeObject = TIsClassPointerLikeObject<Type>::Value;
		
		template <class Type>
		concept CClassPointerLikeObject = TIsClassPointerLikeObject<Type>::Value;
		template <class Type>
		concept CNotClassPointerLikeObject = TNegation<TIsClassPointerLikeObject<Type>>::Value;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  ECallableEntity

		enum struct ECallableEntity
		{
			Null,
			Object,
			Function,
			MemberObject,
			MemberFunction
		};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  TResolveInvokeReturn

		template <class CallRetType, class RetType, class = void>
		struct ZzInternal_TResolveInvokeReturn
		{
			using Type = TNullTypeTag;
		};
		template <class CallRetType>
		struct ZzInternal_TResolveInvokeReturn<CallRetType, TInferTypeTag>
		{
			using Type = CallRetType;
		};
		template <class CallRetType, class RetType>
		struct ZzInternal_TResolveInvokeReturn<CallRetType, RetType, TEnableIf<VIsVoid<RetType> || VIsConvertibleClass<CallRetType, RetType>>>
		{
			using Type = RetType;
		};

		template <class CallRetType, class RetType>
		using TResolveInvokeReturn = typename ZzInternal_TResolveInvokeReturn<CallRetType, RetType>::Type;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  TResolveInvokeCallable

		// TODO check and add more errors and messages about validity of declval(ObjType).*declval(CallType)

		struct TResolveInvokeCallable_Invalid
		{
			using ObjectType = TEmptyTypeTag;
			using SubObjectType = TEmptyTypeTag;

			using CallableType = TEmptyTypeTag;
			using SubCallableType = TEmptyTypeTag;

			static constexpr bool HasSubCallable = false;
			static constexpr SubCallableType SubCallable = TEmptyTypeTag{};
			
			static constexpr bool IsValid = false;
		};

		template <class CallType, class ObjType, class FuncType, class = void>
		struct TResolveInvokeCallable
			: TResolveInvokeCallable_Invalid
		{
			static constexpr ECallableEntity CallableEntity = ECallableEntity::Null;
		};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  TResolveInvokeCallable_Object

		// TResolveInvokeCallable_Object_Cast

		template <class CallType, class ObjType, class FuncType, class = void>
		struct TResolveInvokeCallable_Object_Cast
			: TResolveInvokeCallable_Invalid
		{
		};
		template <class CallType, class ObjType, class FuncType>
		struct TResolveInvokeCallable_Object_Cast<CallType, ObjType, FuncType,
												  TCheckWellFormed<
													  decltype(static_cast<FuncType TDecayType<CallType>::*>(&TDecayType<CallType>::operator()))
												  >>
		{
			using ObjectType = TEmptyTypeTag;
			using SubObjectType = CallType;

			using CallableType = TEmptyTypeTag;
			using SubCallableType = decltype(static_cast<FuncType TDecayType<SubObjectType>::*>(&TDecayType<SubObjectType>::operator()));

			static constexpr bool HasSubCallable = true;
			static constexpr SubCallableType SubCallable = static_cast<FuncType TDecayType<SubObjectType>::*>(&TDecayType<SubObjectType>::operator());
	
			static constexpr bool IsValid = true;
		};

		// TResolveInvokeCallable_Object_Infer

		template <class CallType, class ObjType, class = void>
		struct TResolveInvokeCallable_Object_Infer
			: TResolveInvokeCallable_Invalid
		{
		};
		template <class CallType, class ObjType>
		struct TResolveInvokeCallable_Object_Infer<CallType, ObjType,
												   TCheckWellFormed<decltype(&TDecayType<CallType>::operator())>>
		{
			using ObjectType = TEmptyTypeTag;
			using SubObjectType = CallType;

			using CallableType = TEmptyTypeTag;
			using SubCallableType = decltype(&TDecayType<SubObjectType>::operator());

			static constexpr bool HasSubCallable = true;
			static constexpr SubCallableType SubCallable = &TDecayType<SubObjectType>::operator();

			static constexpr bool IsValid = true;
		};

		// TResolveInvokeCallable_Object

		template <class CallType, class ObjType, class FuncType>
		struct TResolveInvokeCallable_Object
			: TResolveInvokeCallable_Object_Cast<CallType, ObjType, FuncType>
		{
		};
		template <class CallType, class ObjType>
		struct TResolveInvokeCallable_Object<CallType, ObjType, TInferTypeTag>
			: TResolveInvokeCallable_Object_Infer<CallType, ObjType>
		{
		};

		// TResolveInvokeCallable

		template <class CallType, class ObjType, class FuncType>
		struct TResolveInvokeCallable<CallType, ObjType, FuncType, TEnableIf<VIsClass<TDecayType<CallType>>>>
			: TResolveInvokeCallable_Object<CallType, ObjType, FuncType>
		{
			static constexpr ECallableEntity CallableEntity = ECallableEntity::Object;
		};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  TResolveInvokeCallable_Function

		// TResolveInvokeCallable_Function_Cast

		template <class CallType, class ObjType, class FuncType, class = void>
		struct TResolveInvokeCallable_Function_Cast
			: TResolveInvokeCallable_Invalid
		{
		};
		template <class CallType, class ObjType, class FuncType>
		struct TResolveInvokeCallable_Function_Cast<CallType, ObjType, FuncType,
													TCheckWellFormed<decltype(static_cast<FuncType*>(declval(CallType)))>>
		{
			using ObjectType = TEmptyTypeTag;
			using SubObjectType = TEmptyTypeTag;

			// If CallType is of a type F, FuncType must be exactly F
			using CallableType = decltype(static_cast<FuncType*>(declval(CallType)));
			using SubCallableType = TEmptyTypeTag;

			static constexpr bool HasSubCallable = false;
			static constexpr SubCallableType SubCallable = TEmptyTypeTag{};

			static constexpr bool IsValid = true;
		};

		// TResolveInvokeCallable_Function_Infer

		// Infer is always valid for functions, since a function that is inferable has no overloads
		template <class CallType, class ObjType>
		struct TResolveInvokeCallable_Function_Infer
		{
			using ObjectType = TEmptyTypeTag;
			using SubObjectType = TEmptyTypeTag;

			using CallableType = CallType;
			using SubCallableType = TEmptyTypeTag;

			static constexpr bool HasSubCallable = false;
			static constexpr SubCallableType SubCallable = TEmptyTypeTag{};

			static constexpr bool IsValid = true;
		};

		// TResolveInvokeCallable_Function

		template <class CallType, class ObjType, class FuncType>
		struct TResolveInvokeCallable_Function
			: TResolveInvokeCallable_Function_Cast<CallType, ObjType, FuncType>
		{
		};
		template <class CallType, class ObjType>
		struct TResolveInvokeCallable_Function<CallType, ObjType, TInferTypeTag>
			: TResolveInvokeCallable_Function_Infer<CallType, ObjType>
		{
		};

		// TResolveInvokeCallable

		template <class CallType, class ObjType, class FuncType>
		struct TResolveInvokeCallable<CallType, ObjType, FuncType, TEnableIf<VIsFunctionPointer<TDecayType<CallType>>>>
			: TResolveInvokeCallable_Function<CallType, ObjType, FuncType>
		{
			static constexpr ECallableEntity CallableEntity = ECallableEntity::Function;
		};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  TResolveInvokeCallable_MemberObject

		// TResolveInvokeCallable_MemberObject_Cast

		template <class CallType, class ObjType, class FuncType, class = void>
		struct TResolveInvokeCallable_MemberObject_Cast
			: TResolveInvokeCallable_Invalid
		{
		};
		template <class CallType, class ObjType, class FuncType>
		struct TResolveInvokeCallable_MemberObject_Cast<CallType, ObjType, FuncType,
													    TCheckWellFormed<
														    decltype(
															    static_cast<FuncType TDecayType<decltype(declval(ObjType).*declval(CallType))>::*>(
															   	    &TDecayType<decltype(declval(ObjType).*declval(CallType))>::operator()
														   	    )
														    )
													    >>
		{
			using ObjectType = ObjType;
			using SubObjectType = decltype(declval(ObjectType).*declval(CallType));

			using CallableType = TEmptyTypeTag;
			using SubCallableType = decltype(static_cast<FuncType TDecayType<SubObjectType>::*>(&TDecayType<SubObjectType>::operator()));

			static constexpr bool HasSubCallable = true;
			static constexpr SubCallableType SubCallable = static_cast<FuncType TDecayType<SubObjectType>::*>(&TDecayType<SubObjectType>::operator());

			static constexpr bool IsValid = true;
		};

		// TResolveInvokeCallable_MemberObject_Infer

		template <class CallType, class ObjType, class = void>
		struct TResolveInvokeCallable_MemberObject_Infer
			: TResolveInvokeCallable_Invalid
		{
		};
		template <class CallType, class ObjType>
		struct TResolveInvokeCallable_MemberObject_Infer<CallType, ObjType,
														 TCheckWellFormed<
															 decltype(&TDecayType<decltype(declval(ObjType).*declval(CallType))>::operator())
														 >>
		{
			using ObjectType = ObjType;
			using SubObjectType = decltype(declval(ObjectType).*declval(CallType));

			using CallableType = TEmptyTypeTag;
			using SubCallableType = decltype(&TDecayType<SubObjectType>::operator());

			static constexpr bool HasSubCallable = true;
			static constexpr SubCallableType SubCallable = &TDecayType<SubObjectType>::operator();

			static constexpr bool IsValid = true;
		};

		// TResolveInvokeCallable_MemberObject

		template <class CallType, class ObjType, class FuncType>
		struct TResolveInvokeCallable_MemberObject
			: TResolveInvokeCallable_MemberObject_Cast<CallType, ObjType, FuncType>
		{
		};
		template <class CallType, class ObjType>
		struct TResolveInvokeCallable_MemberObject<CallType, ObjType, TInferTypeTag>
			: TResolveInvokeCallable_MemberObject_Infer<CallType, ObjType>
		{
		};

		// TResolveInvokeCallable

		template <class CallType, class ObjType, class FuncType>
		struct TResolveInvokeCallable<CallType, ObjType, FuncType, TEnableIf<VIsMemberObjectPointerToClass<TDecayType<CallType>>>>
			: TResolveInvokeCallable_MemberObject<CallType, ObjType, FuncType>
		{
			static constexpr ECallableEntity CallableEntity = ECallableEntity::MemberObject;
		};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  TResolveInvokeCallable_MemberFunction

		// TResolveInvokeCallable_MemberFunction_Cast

		template <class CallType, class ObjType, class FuncType, class = void>
		struct TResolveInvokeCallable_MemberFunction_Cast
			: TResolveInvokeCallable_Invalid
		{
		};
		template <class CallType, class ObjType, class FuncType>
		struct TResolveInvokeCallable_MemberFunction_Cast<CallType, ObjType, FuncType,
														  TCheckWellFormed<decltype(static_cast<FuncType TDecayType<ObjType>::*>(declval(CallType)))>>
		{
			using ObjectType = ObjType;
			using SubObjectType = TEmptyTypeTag;

			// If CallType is of a type F O::*, FuncType must be exactly F
			using CallableType = decltype(static_cast<FuncType TDecayType<ObjectType>::*>(declval(CallType)));
			using SubCallableType = TEmptyTypeTag;

			static constexpr bool HasSubCallable = false;
			static constexpr SubCallableType SubCallable = TEmptyTypeTag{};

			static constexpr bool IsValid = true;
		};

		// TResolveInvokeCallable_MemberFunction_Infer

		// Infer is always valid for member-functions, since a member-function that is inferable has no overloads
		template <class CallType, class ObjType>
		struct TResolveInvokeCallable_MemberFunction_Infer
		{
			using ObjectType = ObjType;
			using SubObjectType = TEmptyTypeTag;

			using CallableType = CallType;
			using SubCallableType = TEmptyTypeTag;

			static constexpr bool HasSubCallable = false;
			static constexpr SubCallableType SubCallable = TEmptyTypeTag{};

			static constexpr bool IsValid = true;
		};

		// TResolveInvokeCallable_MemberFunction

		template <class CallType, class ObjType, class FuncType>
		struct TResolveInvokeCallable_MemberFunction
			: TResolveInvokeCallable_MemberFunction_Cast<CallType, ObjType, FuncType>
		{
		};
		template <class CallType, class ObjType>
		struct TResolveInvokeCallable_MemberFunction<CallType, ObjType, TInferTypeTag>
			: TResolveInvokeCallable_MemberFunction_Infer<CallType, ObjType>
		{
		};

		// TResolveInvokeCallable

		template <class CallType, class ObjType, class FuncType>
		struct TResolveInvokeCallable<CallType, ObjType, FuncType, TEnableIf<VIsMemberFunctionPointer<TDecayType<CallType>>>>
			: TResolveInvokeCallable_MemberFunction<CallType, ObjType, FuncType>
		{
			static constexpr ECallableEntity CallableEntity = ECallableEntity::MemberFunction;
		};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  TResolveInvokeObject

		template <class ObjType, class = void>
		struct ZzInternal_TResolveInvokeObject
		{
			using Type = ObjType;
		};
		template <class ObjType>
		struct ZzInternal_TResolveInvokeObject<ObjType, TEnableIf<VIsClassPointerLikeObject<TDecayType<ObjType>>>>
		{
			using Type = decltype(*declval(ObjType));
		};

		template <class ArgType>
		using TResolveInvokeObject = typename ZzInternal_TResolveInvokeObject<ArgType>::Type;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  ZzInternal::Invoke

		template <class RetType, class CallType, class SubCallType, class ObjType, class ... ArgTypes>
		AEON_FORCEINLINE constexpr RetType Invoke(CallType&& Call, SubCallType&& SubCall, ObjType&& Obj, ArgTypes&& ... Args)
		{
			// NOTE static_cast<ReturnType> is used to force the expression to be void when ReturnType is void,
			//  otherwise the syntax would be invalid when the callable has a non-void return type.

			// NOTE We make this check here to ensure that the object is also valid when operator() is called on a GInvokeBinderStorage object.
			if constexpr (VIsClassPointerLikeObject<TDecayType<ObjType>>)
			{
				AEON_ASSERT(Obj,
							"'Invoke<ReturnType>(CallableType&& Callable, ObjectType&& Object, ArgumentTypes&& ... Arguments)': "
							"Object must point to a valid object (through contextual conversion to bool).");
			}

			if constexpr (VIsClass<TDecayType<CallType>>)
			{
				if constexpr (VIsEmptyTypeTag<SubCallType>)
				{
					return static_cast<RetType>((Call)(Forward<ArgTypes>(Args) ...));
				}
				else
				{
					return static_cast<RetType>(((Call).*SubCall)(Forward<ArgTypes>(Args) ...));
				}
			}
			else if constexpr (VIsFunctionPointer<TDecayType<CallType>>)
			{
				return static_cast<RetType>((Call)(Forward<ArgTypes>(Args) ...));
			}
			else if constexpr (VIsMemberObjectPointerToClass<TDecayType<CallType>>)
			{
				if constexpr (VIsEmptyTypeTag<SubCallType>)
				{
					if constexpr (VIsClassPointerLikeObject<TDecayType<ObjType>>)
					{
						return static_cast<RetType>(((*Obj).*Call)(Forward<ArgTypes>(Args) ...));
					}
					else
					{
						return static_cast<RetType>(((Obj).*Call)(Forward<ArgTypes>(Args) ...));
					}
				}
				else
				{
					if constexpr (VIsClassPointerLikeObject<TDecayType<ObjType>>)
					{
						return static_cast<RetType>((((*Obj).*Call).*SubCall)(Forward<ArgTypes>(Args) ...));
					}
					else
					{
						return static_cast<RetType>((((Obj).*Call).*SubCall)(Forward<ArgTypes>(Args) ...));
					}
				}
			}
			else if constexpr (VIsMemberFunctionPointer<TDecayType<CallType>>)
			{
				if constexpr (VIsClassPointerLikeObject<TDecayType<ObjType>>)
				{
					return static_cast<RetType>(((*Obj).*Call)(Forward<ArgTypes>(Args) ...));
				}
				else
				{
					return static_cast<RetType>(((Obj).*Call)(Forward<ArgTypes>(Args) ...));
				}
			}
			else
			{
				static_assert(VAlwaysFalse<RetType>,
							  "'ZzInternal::Invoke<RetType>(CallType&& Call, SubCallType&& SubCall, ObjType&& Obj, ArgTypes&& ... Args)': "
							  "Invalid Invoke call. Please review the implementation.");
			}
		}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  CheckThenInvoke

		template <class RetType, class FuncType, class CallType, class ObjType, class ... ArgTypes>
		AEON_FORCEINLINE constexpr auto CheckThenInvoke(CallType&& Call, ObjType&& Obj, ArgTypes&& ... Args)
		{
			// Obtain the actual class type from the passed object, which might be a ClassPointerLikeObject (pointer to class, or CManagedPointer)
			using ResolvedObjType = TResolveInvokeObject<ObjType>;

			// Henceforth, we consider that a callable of (member-object-pointer to) class type has a subcallable,
			// which is the operator() function that will effectively be called.
			using ResolvedCallInfo = TResolveInvokeCallable<CallType, ResolvedObjType, FuncType>;

			// These callable traits are used instead of the resolved call info only when FuncType is TInferTypeTag,
			// the reasoning is explained below.
			using InferredCallableTraits = TSelectIf<VIsEmptyTypeTag<ObjType>,
													 TCallableTraits<CallType, ArgTypes ...>,
													 TMemberCallableTraits<CallType, ResolvedObjType, ArgTypes ...>>;

			// NOTE TResolveInvokeCallable expects that when FuncType is TInferTypeTag, there exists exactly one valid function (no overloads);
			//  this is necessary for BindInvoke, as it may lack the full context of an invocation (i.e. deferred object and/or unbound arguments),
			//  but here, we assume we have the full context of an invocation,
			//  so we can infer the function type by testing whether the invocation expression is valid.
			constexpr bool CanInvoke = VIsInferTypeTag<FuncType> ? InferredCallableTraits::IsCallable : ResolvedCallInfo::IsValid;
			if constexpr (ResolvedCallInfo::CallableEntity == ECallableEntity::Object)
			{
				static_assert(CanInvoke,
							  "'Invoke<ReturnType>(CallableType&& Callable, ArgumentTypes&& ... Arguments)': "
							  "Invalid function: Callable must be func_cast to a function type "
							  "corresponding to an overload of operator() with CallableType underlying class type, "
							  "and with ArgumentTypes parameter types "
							  "(CallableType is a class type).");
			}
			else if constexpr (ResolvedCallInfo::CallableEntity == ECallableEntity::Function)
			{
				static_assert(CanInvoke,
							  "'Invoke<ReturnType>(CallableType&& Callable, ArgumentTypes&& ... Arguments)': "
							  "Invalid function: Callable must be func_cast to a function type with ArgumentTypes parameter types, "
							  "that is an element of the overload set of functions of the same name "
							  "(CallableType is a function-pointer type).");
			}
			else if constexpr (ResolvedCallInfo::CallableEntity == ECallableEntity::MemberObject)
			{
				static_assert(CanInvoke,
							  "'Invoke<ReturnType>(CallableType&& Callable, ObjectType&& Object, ArgumentTypes&& ... Arguments)': "
							  "Invalid function: Callable must be func_cast to a function type "
							  "corresponding to an overload of operator() with CallableType underlying class type, "
							  "and with ArgumentTypes parameter types "
							  "(CallableType is a member-object-pointer to class type).");
			}
			else if constexpr (ResolvedCallInfo::CallableEntity == ECallableEntity::MemberFunction)
			{
				static_assert(CanInvoke,
							  "'Invoke<ReturnType>(CallableType&& Callable, ObjectType&& Object, ArgumentTypes&& ... Arguments)': "
							  "Invalid function: Callable must be func_cast to a member function type with ObjectType underlying class type, "
							  "and with ArgumentTypes parameter types, "
							  "that is an element of the overload set of member functions of the same name "
							  "(CallableType is a member-function-pointer type).");
			}
			else
			{
				static_assert(CanInvoke,
							  "'Invoke<ReturnType>(CallableType&& Callable, ObjectType&& Object, ArgumentTypes&& ... Arguments)': "
							  "Callable must be a callable object of the following type: "
							  "an object, potentially cv-qualified, of class type with an overloaded operator(), "
							  "a function-pointer, "
							  "a member-object-pointer to an object, potentially cv-qualified, of class type with an overloaded operator(), "
							  "or a member-function-pointer.");
			}

			using FuncSignatureInfo = TBreakFunctionSignature<TSelectIf<ResolvedCallInfo::HasSubCallable,
																		typename ResolvedCallInfo::SubCallableType,
																		typename ResolvedCallInfo::CallableType>>;

			// Return type of the invocation expression
			using FuncRetType = TSelectIf<VIsInferTypeTag<FuncType>,
										  typename InferredCallableTraits::ReturnType,
										  typename FuncSignatureInfo::ReturnType>;

			using InvokeRetType = ZzInternal::TResolveInvokeReturn<FuncRetType, RetType>;
			using InvokeArgsPackType = TTypePack<ArgTypes ...>;

			using InvokeObjType = typename ResolvedCallInfo::ObjectType;
			using InvokeSubObjType = typename ResolvedCallInfo::SubObjectType;

			using InvokeCallType = typename ResolvedCallInfo::CallableType;
			using InvokeSubCallType = typename ResolvedCallInfo::SubCallableType;

			using CallableTraits =
				AEON_TRAIT_IF(ResolvedCallInfo::CallableEntity == ECallableEntity::Object)
				<
					ZzInternal_TMemberCallableTraits<InvokeSubCallType, InvokeSubObjType, InvokeArgsPackType>
				>
				AEON_TRAIT_ELSEIF(ResolvedCallInfo::CallableEntity == ECallableEntity::Function)
				<
					ZzInternal_TCallableTraits<InvokeCallType, InvokeArgsPackType>
				>
				AEON_TRAIT_ELSEIF(ResolvedCallInfo::CallableEntity == ECallableEntity::MemberObject)
				<
					ZzInternal_TMemberCallableTraits<InvokeSubCallType, InvokeSubObjType, InvokeArgsPackType>
				>
				AEON_TRAIT_ELSEIF(ResolvedCallInfo::CallableEntity == ECallableEntity::MemberFunction)
				<
					ZzInternal_TMemberCallableTraits<InvokeCallType, InvokeObjType, InvokeArgsPackType>
				>
				AEON_TRAIT_ELSE
				<
					ZzInternal_TCallableTraits<void, TTypePack<>>
				>
				AEON_TRAIT_ENDIF;

			// This refers to Callable, SubCallable, and Object
			constexpr bool CanInvokeWithReturn = CanInvoke ? !VIsNullTypeTag<InvokeRetType> : true;
			// NOTE No need to retest this if we are inferring FuncType, as this was already tested by CanInvoke
			constexpr bool CanInvokeWithParameters = CanInvokeWithReturn ? VIsInferTypeTag<FuncType> || CallableTraits::IsCallable : true;
			if constexpr (VIsEmptyTypeTag<ObjType>)
			{
				static_assert(CanInvokeWithReturn,
							  "'Invoke<ReturnType>(CallableType&& Callable, ArgumentTypes&& ... Arguments)': "
							  "ReturnType must be either the return type of the callable, a type convertible therefrom, or void.");

				static_assert(CanInvokeWithParameters,
							  "'Invoke<ReturnType>(CallableType&& Callable, ArgumentTypes&& ... Arguments)': "
							  "Arguments should correspond to the parameters of the specified overload of either operator() or of the function "
							  "(for either class or function-pointer type, respectively).");
			}
			else
			{
				static_assert(CanInvokeWithReturn,
							  "'Invoke<ReturnType>(CallableType&& Callable, ObjectType&& Object, ArgumentTypes&& ... Arguments)': "
							  "ReturnType must be either the return type of the callable, a type convertible therefrom, or void.");

				static_assert(CanInvokeWithParameters,
							  "'Invoke<ReturnType>(CallableType&& Callable, ObjectType&& Object, ArgumentTypes&& ... Arguments)': "
							  "Object must be of the underlying class type of Callable, or a subclass thereof, "
							  "and the cv-qualification and ref-qualification of ObjectType and CallableType must match; "
							  "Arguments should correspond to the parameters of the specified overload of either operator() or of the function "
							  "(for either member-object-pointer to class or member-function-pointer type, respectively).");
			}

			// Obtain SubCall info, regardless of existence.
			// We'll just pass this directly, TResolveInvokeCallable already makes an appropriate type/object
			// that Invoke can deal with when SubCall is non-existent.
			using SubCallType = typename ResolvedCallInfo::SubCallableType;
			constexpr SubCallType SubCall = ResolvedCallInfo::SubCallable;

			return Invoke<InvokeRetType>(Forward<CallType>(Call), SubCall, Forward<ObjType>(Obj), Forward<ArgTypes>(Args) ...);
		}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  Machinery for 'func_cast'

#	define func_cast ::Aeon::ZzInternal::WrapCallable

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GCallableWrapper

		template <CFunction FunctionType, class CallableType>
		class GCallableWrapper
		{
		public:
			GCallableWrapper(CallableType&& NewCallable)
				: CallableRef(static_cast<CallableType&&>(NewCallable))
			{
			}

			operator CallableType&&()
			{
				return static_cast<CallableType&&>(CallableRef);
			}

		private:
			CallableType&& CallableRef;
		};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  TUnwrapCallable

		template <class DecayedCallType, class CallType>
		struct ZzInternal_TUnwrapCallable
		{
			using FunctionType = TInferTypeTag;
			using CallableType = CallType;
		};
		template <class FuncType, class CallType, class WrapperType>
		struct ZzInternal_TUnwrapCallable<GCallableWrapper<FuncType, CallType>, WrapperType>
		{
			using FunctionType = FuncType;
			using CallableType = CallType;
		};

		template <class WrappedCallableType>
		using TUnwrapCallable = ZzInternal_TUnwrapCallable<TDecayType<WrappedCallableType>, WrappedCallableType>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  WrapCallable

		// General case for class and member-object-pointer to class types
		template <CFunction FuncType, class CallType>
		GCallableWrapper<FuncType, CallType> WrapCallable(CallType&& Call)
		requires CClass<TDecayType<CallType>> || CMemberObjectPointerToClass<TDecayType<CallType>>
		{
			return { Aeon::Forward<CallType>(Call) };
		}

		// Overload for function-pointer types, note that CallType is not deduced,
		// and so the argument may be "ambiguous" at the call site (will be disambiguated by the specified FuncType)
		template <CFunction FuncType>
		GCallableWrapper<FuncType, FuncType*> WrapCallable(FuncType*&& Call)
		{
			// NOTE Call must be a (rvalue) reference, otherwise the wrapper will bind to this parameter,
			//  which will go out of scope once this function returns
			return { Aeon::Forward<FuncType*>(Call) };
		}
		// Overload for member-function-pointer types, note that CallType is not deduced,
		// and so the argument may be "ambiguous" at the call site (will be disambiguated by the specified FuncType)
		template <CFunction FuncType, class ObjType>
		GCallableWrapper<FuncType, FuncType ObjType::*> WrapCallable(FuncType ObjType::*&& Call)
		{
			// NOTE Call must be a (rvalue) reference, otherwise the wrapper will bind to this parameter,
			//  which will go out of scope once this function returns
			return { Aeon::Forward<FuncType ObjType::*>(Call) };
		}
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  Invoke

	template <class ReturnType = TInferTypeTag, class WrappedCallableType, class ... ArgumentTypes>
	AEON_SPEC(NODISCARD, FORCEINLINE)
	constexpr auto Invoke(WrappedCallableType&& Callable, ArgumentTypes&& ... Arguments)
	requires CNotMemberPointer<TDecayType<typename ZzInternal::TUnwrapCallable<WrappedCallableType>::CallableType>>
	{
		using FunctionType = typename ZzInternal::TUnwrapCallable<WrappedCallableType>::FunctionType;
		using CallableType = typename ZzInternal::TUnwrapCallable<WrappedCallableType>::CallableType;

		return ZzInternal::CheckThenInvoke<ReturnType, FunctionType>(Forward<CallableType>(Callable),
																	 TEmptyTypeTag{},
																	 Forward<ArgumentTypes>(Arguments) ...);
	}

	template <class ReturnType = TInferTypeTag, class WrappedCallableType, class ObjectType, class ... ArgumentTypes>
	AEON_SPEC(NODISCARD, FORCEINLINE)
	constexpr auto Invoke(WrappedCallableType&& Callable, ObjectType&& Object, ArgumentTypes&& ... Arguments)
	requires CMemberPointer<TDecayType<typename ZzInternal::TUnwrapCallable<WrappedCallableType>::CallableType>>
			 && (CClass<TDecayType<ObjectType>> || ZzInternal::CClassPointerLikeObject<TDecayType<ObjectType>>)
	{
		using FunctionType = typename ZzInternal::TUnwrapCallable<WrappedCallableType>::FunctionType;
		using CallableType = typename ZzInternal::TUnwrapCallable<WrappedCallableType>::CallableType;

		static_assert(!CManagedPointer<TMemberPointerUnderlyingType<TDecayType<CallableType>>>,
					  "'Invoke<ReturnType>(CallableType&& Callable, ObjectType&& Object, ArgumentTypes&& ... Arguments)': "
		    		  "Callable cannot be a member-function-pointer or member-object-pointer to class with underlying CManagedPointer class type.");

		return ZzInternal::CheckThenInvoke<ReturnType, FunctionType>(Forward<CallableType>(Callable),
																	 Forward<ObjectType>(Object),
																	 Forward<ArgumentTypes>(Arguments) ...);
	}

#if 0
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  TIsInvocable

// TODO implement this in terms of TResolveInvoke types
	template <class RetType, class CallType, class SubCallType, class ObjType, class ArgPackType, class = void>
	struct ZzInternal_TIsInvocable_Base
		: TFalseConstant
	{
	};

	template <class RetType, class CallType, class SubCallType, class ObjType, class ... ArgTypes>
	struct ZzInternal_TIsInvocable_Base<RetType, CallType, SubCallType, ObjType, TTypePack<ArgTypes ...>,
		TCheckWellFormed<decltype(ZzInternal::Invoke<RetType>(declval(CallType), declval(SubCallType), declval(ObjType), declval(ArgTypes) ...))>>
		: TTrueConstant
	{
	};

	template <class RetType, class FuncType, class CallType, class ArgPackType>
	struct ZzInternal_TIsInvocable
	{
		using ObjType = TSelectIf<VIsMemberPointer<TDecayType<CallType>>, typename ArgPackType::Front, TEmptyTypeTag>;
		using InvokeArgPackType = TSelectIf<VIsMemberPointer<TDecayType<CallType>>, typename ArgPackType::template PopFront<1>, ArgPackType>;

		using ResolvedObjType = ZzInternal::TResolveInvokeObject<ObjType>;
		using ResolvedCallInfo = ZzInternal::TResolveInvokeCallable<CallType, ResolvedObjType, FuncType>;

		using FuncSignatureInfo = TBreakFunctionSignature<TSelectIf<ResolvedCallInfo::HasSubCallable,
																	typename ResolvedCallInfo::SubCallableType,
																	typename ResolvedCallInfo::CallableType>>;

		using SubCallType = typename ResolvedCallInfo::SubCallableType;

		using InferredCallableTraits = TSelectIf<VIsEmptyTypeTag<ObjType>,
												 ZzInternal_TCallableTraits<CallType, InvokeArgPackType>,
												 ZzInternal_TMemberCallableTraits<CallType, ResolvedObjType, InvokeArgPackType>>;

		using FuncRetType = TSelectIf<VIsInferTypeTag<FuncType>,
									  typename InferredCallableTraits::ReturnType,
									  typename FuncSignatureInfo::ReturnType>;

		using InvokeRetType = ZzInternal::TResolveInvokeReturn<FuncRetType, RetType>;

		using Type = ZzInternal_TIsInvocable_Base<InvokeRetType, CallType, SubCallType, ObjType, InvokeArgPackType>;
	};

	template <class CallableType, class ... ArgTypes>
	using TIsInvocable = typename ZzInternal_TIsInvocable<TInferTypeTag,
														  typename ZzInternal::TUnwrapCallable<CallableType>::HandlerType,
														  typename ZzInternal::TUnwrapCallable<CallableType>::CallableType,
														  TTypePack<ArgTypes ...>>::Type;

	template <class CallableType, class ... ArgTypes>
	inline constexpr bool VIsInvocable = TIsInvocable<CallableType, ArgTypes ...>::Value;

	template <class CallableType, class ... ArgTypes>
	concept CInvocable = TIsInvocable<CallableType, ArgTypes ...>::Value;
	template <class CallableType, class ... ArgTypes>
	concept CNotInvocable = TNegation<TIsInvocable<CallableType, ArgTypes ...>>::Value;

	// TODO should we expand these traits to include:
	//  TIsInvocableWithParams<ArgTypes...> (equal to default),
	//  TIsInvocableWithObj<ObjectType, ArgTypes...> (checks ObjectType base class, and cv-qualifiers (maybe eventually lref/rref qualifiers)),
	//  TIsInvocableWithSignature<FuncType> (checks if static_cast is well-formed)
#endif
}

#endif