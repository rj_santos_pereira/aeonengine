#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_FUNCTIONAL_FUNCTIONCOMMON
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_FUNCTIONAL_FUNCTIONCOMMON

#include "Aeon/Framework/Core.hpp"

namespace Aeon::ZzInternal
{
	template <class FunctionType>
	class IFunctionStorage;

	template <class ReturnType, class ... ArgumentTypes>
	class IFunctionStorage<ReturnType(ArgumentTypes ...)>
	{
	public:
		virtual ~IFunctionStorage() = default;

		virtual ReturnType Invoke(ArgumentTypes ...) = 0;
		virtual bool CanInvoke() const = 0;

		virtual void RebindObject(voidptr NewObjectPtr) = 0;

	};
}

#endif