#add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/)

target_sources(${PROJECT_NAME}
	PUBLIC
		${CMAKE_CURRENT_SOURCE_DIR}/Optional.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/Relational.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/FunctionCommon.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/Invoke.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/BindInvoke.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/Function.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/Delegate.hpp
#	PRIVATE
#		${CMAKE_CURRENT_SOURCE_DIR}/
	)