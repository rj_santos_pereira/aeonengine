#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_FUNCTIONAL_RELATIONAL
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_FUNCTIONAL_RELATIONAL

#include "Aeon/Framework/Core.hpp"

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GEqualTo, GNotEqualTo, GLessThan, GGreaterThan, GLessThanOrEqualTo, GGreaterThanOrEqualTo

	template <CScalarObject ObjType>
	struct GEqualTo
	{
		constexpr bool operator()(ObjType const& Value1, ObjType const& Value2)
		{
			return (Value1 == Value2);
		}
	};

	template <CScalarObject ObjType>
	struct GNotEqualTo
	{
		constexpr bool operator()(ObjType const& Value1, ObjType const& Value2)
		{
			return (Value1 != Value2);
		}
	};

	template <CScalarObject ObjType>
	struct GLessThan
	{
		constexpr bool operator()(ObjType const& Value1, ObjType const& Value2)
		{
			return (Value1 < Value2);
		}
	};

	template <CScalarObject ObjType>
	struct GGreaterThan
	{
		constexpr bool operator()(ObjType const& Value1, ObjType const& Value2)
		{
			return (Value1 > Value2);
		}
	};

	template <CScalarObject ObjType>
	struct GLessThanOrEqualTo
	{
		constexpr bool operator()(ObjType const& Value1, ObjType const& Value2)
		{
			return (Value1 <= Value2);
		}
	};

	template <CScalarObject ObjType>
	struct GGreaterThanOrEqualTo
	{
		constexpr bool operator()(ObjType const& Value1, ObjType const& Value2)
		{
			return (Value1 >= Value2);
		}
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CEqualityComparable, CInequalityComparable

	template <class Type>
	concept CEqualityComparable = requires
	{
		requires requires (Type const Value1, Type const Value2) { { GEqualTo<Type>()(Value1, Value2) } -> CBool; };
		requires requires (Type const Value1, Type const Value2) { { GNotEqualTo<Type>()(Value1, Value2) } -> CBool; };
	};

	template <class Type>
	concept CInequalityComparable = requires
	{
		requires requires (Type const Value1, Type const Value2) { { GLessThan<Type>()(Value1, Value2) } -> CBool; };
		requires requires (Type const Value1, Type const Value2) { { GLessThanOrEqualTo<Type>()(Value1, Value2) } -> CBool; };
		requires requires (Type const Value1, Type const Value2) { { GGreaterThan<Type>()(Value1, Value2) } -> CBool; };
		requires requires (Type const Value1, Type const Value2) { { GGreaterThanOrEqualTo<Type>()(Value1, Value2) } -> CBool; };
	};
}

#endif