#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_FUNCTIONAL_OPTIONAL
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_FUNCTIONAL_OPTIONAL

#include "Aeon/Framework/Core.hpp"

namespace Aeon
{
	// TODO maybe eventually refactor to preserve triviality characteristics of object's ctors/dtor

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GOptional

	AEON_DECLARE_TAG_CLASS(TConstructInPlace);

	template <CScalarObject ObjType>
	class GOptional
	{
	public:
		using ObjectType = ObjType;

		GOptional()
			: ObjectStorage()
			, HasObj(false)
		{
		}
		template <class ... ArgTypes>
		explicit GOptional(TConstructInPlace, ArgTypes&& ... Args)
			: ObjectStorage()
			, HasObj(true)
		{
			ObjectStorage.Construct(Forward<ArgTypes>(Args) ...);
		}

		GOptional(GOptional const& Other)
		requires CCopyable<ObjectType>
			: GOptional()
		{
			if (Other.HasObj)
			{
				Set(*Other.ObjectStorage);
			}
		}
		GOptional(GOptional&& Other)
		requires CMoveable<ObjectType>
			: GOptional()
		{
			if (Other.HasObj)
			{
				Set(Move(*Other.ObjectStorage));
				// CHECKME Should other be destroyed here?
			}
		}

		GOptional& operator=(GOptional Other)
		{
			using Aeon::Swap;
			Swap(*this, Other);
			return *this;
		}

		~GOptional()
		{
			Reset();
		}

		ObjectType const& Get() const
		{
			AEON_ASSERT(HasObj);
			return *ObjectStorage;
		}
		ObjectType& Get()
		{
			return AEON_INVOKE_CONST_OVERLOAD(Get);
		}

		void Set(ObjectType const& Object)
		{
			Reset();

			ObjectStorage.Construct(Object);
			HasObj = true;
		}
		void Set(ObjectType&& Object)
		{
			Reset();

			ObjectStorage.Construct(Move(Object));
			HasObj = true;
		}

		void Reset()
		{
			if (HasObj)
			{
				ObjectStorage.Destruct();
				HasObj = false;
			}
		}

		bool HasObject() const
		{
			return HasObj;
		}

		ObjectType const& operator*() const
		{
			AEON_ASSERT(HasObj);
			return *ObjectStorage;
		}
		ObjectType& operator*()
		{
			return AEON_INVOKE_CONST_OVERLOAD(operator*);
		}

		ObjectType const* operator->() const
		requires CClass<ObjectType> || CUnion<ObjectType>
		{
			AEON_ASSERT(HasObj);
			return &ObjectStorage;
		}
		ObjectType* operator->()
		requires CClass<ObjectType> || CUnion<ObjectType>
		{
			return AEON_INVOKE_CONST_OVERLOAD(operator->);
		}

		explicit operator bool() const
		{
			return HasObj;
		}
		explicit operator bool()
		{
			return HasObj;
		}

		friend void Swap(GOptional& Optional1, GOptional& Optional2)
		{
			using Aeon::Swap;
			if (Optional1.HasObj && Optional2.HasObj)
			{
				Swap(*Optional1.ObjectStorage, *Optional2.ObjectStorage);
			}
			else if (Optional1.HasObj)
			{
				Optional2.ObjectStorage.Construct(MoveElseCopy(*Optional1.ObjectStorage));
				Optional1.ObjectStorage.Destruct();
			}
			else if (Optional2.HasObj)
			{
				Optional1.ObjectStorage.Construct(MoveElseCopy(*Optional2.ObjectStorage));
				Optional2.ObjectStorage.Destruct();
			}
			Swap(Optional1.HasObj, Optional2.HasObj);
		}

	private:
		GStorage<ObjectType> ObjectStorage;
		bool HasObj;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GOptionalRef

	template <CScalarObject ObjType>
	class GOptionalRef
	{
	public:
		using ObjectType = ObjType;

		GOptionalRef()
			: ObjectPtr(nullptr)
		{
		}
		GOptionalRef(ObjectType& NewObject)
			: ObjectPtr(addressof(NewObject))
		{
		}
		// Disallow constructing ref to a temporary object
		GOptionalRef(ObjectType&& NewObject) = delete;

		GOptionalRef(GOptionalRef const& Other) = default;
		GOptionalRef& operator=(GOptionalRef const& Other) = default;

		GOptionalRef(GOptionalRef&& Other) = delete;
		GOptionalRef& operator=(GOptionalRef&& Other) = delete;

		ObjectType const& Get() const
		{
			AEON_ASSERT(ObjectPtr);
			return *ObjectPtr;
		}
		ObjectType& Get()
		{
			return AEON_INVOKE_CONST_OVERLOAD(Get);
		}

#	if 0
		void Set(ObjectType& NewObject)
		{
			ObjectPtr = addressof(NewObject);
		}

		void Reset()
		{
			ObjectPtr = nullptr;
		}
#	endif

		bool HasObject() const
		{
			return ObjectPtr;
		}

		ObjectType const& operator*() const
		{
			AEON_ASSERT(ObjectPtr);
			return *ObjectPtr;
		}
		ObjectType& operator*()
		{
			return AEON_INVOKE_CONST_OVERLOAD(operator*);
		}

		ObjectType const* operator->() const
		requires CClass<ObjectType> || CUnion<ObjectType>
		{
			AEON_ASSERT(ObjectPtr);
			return ObjectPtr;
		}
		ObjectType* operator->()
		requires CClass<ObjectType> || CUnion<ObjectType>
		{
			return AEON_INVOKE_CONST_OVERLOAD(operator->);
		}

#	if 0
		operator ObjectType const&() const
		{
			AEON_ASSERT(ObjectPtr);
			return *ObjectPtr;
		}
		operator ObjectType&()
		{
			AEON_ASSERT(ObjectPtr);
			return *ObjectPtr;
		}
#	endif

		// NOTE Non-const version also needs to be declared so that contextual conversion prefers it to the object conversion operators
		//  when the type is implicitly convertible to bool
		explicit operator bool() const
		{
			return ObjectPtr;
		}
		explicit operator bool()
		{
			return ObjectPtr;
		}

	private:
		ObjectType* ObjectPtr;
	};

	namespace ZzInternal
	{
		template <class ObjectType, class ConstObjectType>
	//	[[deprecated("This is a library internal function, it should NOT be invoked in user code!")]]
		GOptionalRef<ObjectType> ConstCast_OptionalRef(GOptionalRef<ConstObjectType>&& OptionalRef)
		requires CSameClass<TRemoveConst<ObjectType>, TRemoveConst<ConstObjectType>>
		{
			if (OptionalRef.HasObject())
			{
				return GOptionalRef<ObjectType>{ const_cast<ObjectType&>(OptionalRef.Get()) };
			}
			return GOptionalRef<ObjectType>{};
		}
	}

#define ZZINTERNAL_AEON_INVOKE_CONST_OPTIONALREF_OVERLOAD(Method, ...) \
    ::Aeon::ZzInternal::ConstCast_OptionalRef<typename decltype(this->Method(__VA_ARGS__))::ObjectType>(\
        static_cast<::Aeon::TRemovePointer<decltype(this)> const*>(this)->Method(__VA_ARGS__)\
    )

}

#endif