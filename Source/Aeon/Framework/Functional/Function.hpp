#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_FUNCTIONAL_FUNCTION
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_FUNCTIONAL_FUNCTION

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Functional/FunctionCommon.hpp"
#include "Aeon/Framework/Functional/BindInvoke.hpp"
#include "Aeon/Framework/Memory/SharedPtr.hpp"

// TODO documentation, tons of it

namespace Aeon
{
	namespace ZzInternal
	{
		template <class ResolvedCallableInfoType, class UnderlyingClassType, class = void>
		struct ZzInternal_TDeduceCallableFunction
		{
			using Type = TNullTypeTag;
		};

		template <class ResolvedCallableInfoType, class UnderlyingClassType>
		struct ZzInternal_TDeduceCallableFunction<ResolvedCallableInfoType, UnderlyingClassType,
												  TEnableIf<(ResolvedCallableInfoType::CallableEntity == ECallableEntity::Object)>>
		{
			using Type
				= TRemovePointer<TMakeFunctionSignature<void,
														typename TBreakFunctionSignature<typename ResolvedCallableInfoType::SubCallableType>
															::ReturnType,
														typename TBreakFunctionSignature<typename ResolvedCallableInfoType::SubCallableType>
															::ParametersPackType,
														TFunctionQualifierSequence<>
														>>;
		};

		template <class ResolvedCallableInfoType, class UnderlyingClassType>
		struct ZzInternal_TDeduceCallableFunction<ResolvedCallableInfoType, UnderlyingClassType,
												  TEnableIf<(ResolvedCallableInfoType::CallableEntity == ECallableEntity::Function)>>
		{
			using Type
				= TRemovePointer<TMakeFunctionSignature<void,
														typename TBreakFunctionSignature<typename ResolvedCallableInfoType::CallableType>
															::ReturnType,
														typename TBreakFunctionSignature<typename ResolvedCallableInfoType::CallableType>
															::ParametersPackType,
														TFunctionQualifierSequence<>
														>>;
		};

		template <class ResolvedCallableInfoType, class UnderlyingClassType>
		struct ZzInternal_TDeduceCallableFunction<ResolvedCallableInfoType, UnderlyingClassType,
												  TEnableIf<(ResolvedCallableInfoType::CallableEntity == ECallableEntity::MemberObject)>>
		{
			using Type
				= TRemovePointer<TMakeFunctionSignature<void,
														typename TBreakFunctionSignature<typename ResolvedCallableInfoType::SubCallableType>
														    ::ReturnType,
														typename TBreakFunctionSignature<typename ResolvedCallableInfoType::SubCallableType>
														    ::ParametersPackType
															::template PushFront<UnderlyingClassType>,
														TFunctionQualifierSequence<>
														>>;
		};

		template <class ResolvedCallableInfoType, class UnderlyingClassType>
		struct ZzInternal_TDeduceCallableFunction<ResolvedCallableInfoType, UnderlyingClassType,
												  TEnableIf<(ResolvedCallableInfoType::CallableEntity == ECallableEntity::MemberFunction)>>
		{
			using Type
				= TRemovePointer<TMakeFunctionSignature<void,
														typename TBreakFunctionSignature<typename ResolvedCallableInfoType::CallableType>
														    ::ReturnType,
														typename TBreakFunctionSignature<typename ResolvedCallableInfoType::CallableType>
														    ::ParametersPackType
															::template PushFront<UnderlyingClassType>,
														TFunctionQualifierSequence<>
														>>;
		};

		template <class CallableType, class FunctionType>
		using TDeduceCallableFunction
			= typename ZzInternal_TDeduceCallableFunction<
				TResolveInvokeCallable<CallableType, TMemberPointerUnderlyingType<CallableType>, FunctionType>,
				TMemberPointerUnderlyingType<CallableType>
			>::Type;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GCallableFunctionStorage

		template <class CallableType, class ObjectType, class ReturnType, class ArgumentsPackType>
		class GCallableFunctionStorage;

		template <class CallableType, class ReturnType, class ... ArgumentTypes>
		requires CNotMemberPointer<TDecayType<CallableType>>
		class GCallableFunctionStorage<CallableType, TEmptyTypeTag, ReturnType, TTypePack<ArgumentTypes ...>> final
			: public IFunctionStorage<ReturnType(ArgumentTypes ...)>
		{
		public:
			explicit GCallableFunctionStorage(CallableType&& NewCallable)
				: Callable(Forward<CallableType>(NewCallable))
			{
				static_assert(VIsCallableWithResult<CallableType, ReturnType, ArgumentTypes ...>,
				    		  "'GFunction<ReturnType(ArgumentTypes ...)>::GFunction(CallableType&& Callable)': "
							  "Invalid invocation expression for 'Invoke<ReturnType>(CallableType, ArgumentsType ...)', "
							  "where CallableType is a non-member class type 'C' or a non-member function-pointer type 'F*', "
							  "and either 'C' has a potentially overloaded operator(), or 'F' is a function, "
							  "with signature 'ReturnType(ArgumentTypes ...)'.");
			}

			virtual ReturnType Invoke(ArgumentTypes... Arguments) override
			{
				return Aeon::Invoke<ReturnType>(Forward<CallableType>(Callable), Forward<ArgumentTypes>(Arguments) ...);
			}
			virtual bool CanInvoke() const override
			{
				return true;
			}

			virtual void RebindObject(voidptr) override
			{
				AEON_FAILSAFE(false, return, "Trying to rebind object in GFunction for non-object handler (AEON_EVENT_HANDLER).");
			}

		private:
			TDecayType<CallableType> Callable;
		};

		template <class CallableType, class ObjectType, class ReturnType, class ... ArgumentTypes>
		requires CMemberPointer<TDecayType<CallableType>>
		class GCallableFunctionStorage<CallableType, ObjectType, ReturnType, TTypePack<ArgumentTypes ...>> final
			: public IFunctionStorage<ReturnType(ObjectType, ArgumentTypes ...)>
		{
		public:
			explicit GCallableFunctionStorage(CallableType&& NewCallable)
				: Callable(Forward<CallableType>(NewCallable))
			{
				static_assert(VIsMemberCallableWithResult<CallableType, TResolveInvokeObject<ObjectType>, ReturnType, ArgumentTypes ...>,
							  "'GFunction<ReturnType(ObjectType, ArgumentTypes ...)>::GFunction(CallableType&& Callable)': "
							  "Invalid invocation expression for 'Invoke<ReturnType>(CallableType, ObjectType, ArgumentsType ...)', "
							  "where CallableType is a member-data-pointer to class type 'C TDecayType<ObjectType>::*', where C is some class type, "
							  "or a member-function-pointer type 'F TDecayType<ObjectType>::*', where 'F' is a function, "
							  "and either 'C' has a potentially overloaded operator(), or 'F' is a function, "
							  "with signature 'ReturnType(ArgumentTypes ...)'.");
			}

			virtual ReturnType Invoke(ObjectType Object, ArgumentTypes... Arguments) override
			{
				return Aeon::Invoke<ReturnType>(Forward<CallableType>(Callable), Forward<ObjectType>(Object), Forward<ArgumentTypes>(Arguments) ...);
			}
			virtual bool CanInvoke() const override
			{
				return true;
			}

			virtual void RebindObject(voidptr) override
			{
				AEON_FAILSAFE(false, return, "Trying to rebind object in GFunction for non-object handler (AEON_EVENT_HANDLER).");
			}

		private:
			TDecayType<CallableType> Callable;
		};
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GFunction

	template <CSimpleFunction FunctionType>
	class GFunction
	{
		using FunctionSignature = TBreakFunctionSignature<FunctionType>;

		using ReturnType = typename FunctionSignature::ReturnType;
		using ArgumentsPackType = typename FunctionSignature::ParametersPackType;

	public:
		GFunction() = default;

		// Callable (pointer-to-function, class/functor, lambda) constructor
		template <class WrappedCallableType>
		GFunction(WrappedCallableType&& Callable)
		requires CNotMemberPointer<TDecayType<typename ZzInternal::TUnwrapCallable<WrappedCallableType>::CallableType>>
		//		 && CNotSameClass<TDecayType<typename ZzInternal::TUnwrapCallable<WrappedCallableType>::CallableType>, GFunction>
		{
			using CallableType = typename ZzInternal::TUnwrapCallable<WrappedCallableType>::CallableType;

			// NOTE Technically, GFunction is a callable,
			//  but this constructor would cause recursive construction of the callable
			//  (GCallableFunctionStorage would invoke this constructor of GFunction in the construction of Callable),
			//  which would cause a stack overflow.
			static_assert(!VIsSameClass<TDecayType<CallableType>, GFunction>);
			static_assert(!VIsInvokeBinder<TDecayType<CallableType>>);

			using StorageType = ZzInternal::GCallableFunctionStorage<CallableType,
																	 TEmptyTypeTag,
																	 ReturnType,
																	 ArgumentsPackType>;

			bool ShouldInit = true;
			if constexpr (VIsPointer<TDecayType<CallableType>>)
			{
				ShouldInit = Callable;
			}

			if (ShouldInit)
			{
				Data = MakeSharedPtr<StorageType>(Forward<CallableType>(Callable));
			}
		}

		// Callable (member-pointer-to-function, member-pointer-to-class/functor) constructor
		template <class WrappedCallableType>
		GFunction(WrappedCallableType&& Callable)
		requires CMemberPointer<TDecayType<typename ZzInternal::TUnwrapCallable<WrappedCallableType>::CallableType>>
		//		 && CNotSameClass<TDecayType<typename ZzInternal::TUnwrapCallable<WrappedCallableType>::CallableType>, GFunction>
		{
			using CallableType = typename ZzInternal::TUnwrapCallable<WrappedCallableType>::CallableType;

			// NOTE This ctor probably can't be called recursively, but this check remains here for safety.
			static_assert(!VIsSameClass<TDecayType<CallableType>, GFunction>);
			static_assert(!VIsInvokeBinder<TDecayType<CallableType>>);

			using StorageType = ZzInternal::GCallableFunctionStorage<CallableType,
																	 typename ArgumentsPackType::Front,
																	 ReturnType,
																	 typename ArgumentsPackType::template PopFront<1>>;

			bool ShouldInit = true;
			if constexpr (VIsPointer<TDecayType<CallableType>>)
			{
				ShouldInit = Callable;
			}

			if (ShouldInit)
			{
				Data = MakeSharedPtr<StorageType>(Forward<CallableType>(Callable));
			}
		}

		// GInvokeBinderStorage (BindInvoke) constructor
		template <EBindMode BindModeValue, class CallableType, class SubCallableType, class ObjectType, class BindArgumentsPackType>
		GFunction(ZzInternal::GInvokeBinder<BindModeValue, CallableType, SubCallableType, ObjectType,
											ReturnType, BindArgumentsPackType, ArgumentsPackType> Binder)
			: Data{ Binder.MoveStorage_Internal() }
		{
		}

		GFunction(nullptr_type)
		{
		}

		GFunction(GFunction const& Other) = default;
		GFunction(GFunction&& Other) = default;

		GFunction& operator=(GFunction const& Other) = default;
		GFunction& operator=(GFunction&& Other) = default;

		template <class ... ArgTypes>
		ReturnType operator()(ArgTypes&& ... Args)
		{
			AEON_ASSERT(operator bool());
			return Data->Invoke(Forward<ArgTypes>(Args) ...);
		}

		explicit operator bool() const
		{
			return Data && Data->CanInvoke();
		}

#	if 0
		// NOTE vvvv FOR GEventDelegate INTERNAL USAGE ONLY vvvv
		void RebindObject_Internal(voidptr NewObjectPtr)
		{
			Data->RebindObject(NewObjectPtr);
		}
		// NOTE ^^^^ FOR GEventDelegate INTERNAL USAGE ONLY ^^^^
#	endif

	private:
		GSharedPtr<ZzInternal::IFunctionStorage<FunctionType>> Data;
	};

	template <class WrappedCallableType>
	GFunction(WrappedCallableType&&)
		->	GFunction<
		    	ZzInternal::TDeduceCallableFunction<
			    	typename ZzInternal::TUnwrapCallable<WrappedCallableType>::CallableType,
					typename ZzInternal::TUnwrapCallable<WrappedCallableType>::FunctionType
					>
				>;

	template <EBindMode BindModeValue, class CallableType, class SubCallableType, class ObjectType,
									   class ReturnType, class BindArgumentsPackType, class ArgumentsPackType>
	GFunction(ZzInternal::GInvokeBinder<BindModeValue, CallableType, SubCallableType, ObjectType,
										ReturnType, BindArgumentsPackType, ArgumentsPackType>)
		->	GFunction<ZzInternal::TMakeBinderFunctionSignature<CallableType, ObjectType, ReturnType, ArgumentsPackType>>;
}

#endif