#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_FUNCTIONAL_BINDINVOKE
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_FUNCTIONAL_BINDINVOKE

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Container/Tuple/Duple.hpp"
#include "Aeon/Framework/Container/Tuple/Tuple.hpp"
#include "Aeon/Framework/Functional/FunctionCommon.hpp"
#include "Aeon/Framework/Functional/Invoke.hpp"
#include "Aeon/Framework/Memory/SharedPtr.hpp"

// TODO documentation, tons of it

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  EBindMode

	enum struct EBindMode
	{
		// Bind passed arguments to the first N parameters of the invocable, where N is the number of passed arguments. This is the default.
		FirstArgs,
		// Bind passed arguments to the last N parameters of the invocable, where N is the number of passed arguments.
		LastArgs,
	};

	namespace ZzInternal
	{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  SDeferredObject

		struct SDeferredObject
		{
			constexpr bool operator==(SDeferredObject) const noexcept { return true; }
			constexpr strong_ordering operator<=>(SDeferredObject) const noexcept { return strong_ordering::equal; }
		};

		template <class Type>
		concept CDeferredObject = CSameClass<Type, SDeferredObject>;
		template <class Type>
		concept CNotDeferredObject = !CDeferredObject<Type>;

		template <class Type>
		using TIsDeferredObject = TValueTrait<CDeferredObject<Type>>;
		template <class Type>
		inline constexpr bool VIsDeferredObject = CDeferredObject<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  TResolveInvokeArgument

		template <class ArgType, class = void>
		struct ZzInternal_TResolveInvokeArgument
		{
			using Type = ArgType;
		};
		template <class ArgType>
		struct ZzInternal_TResolveInvokeArgument<ArgType, TEnableIf<VIsLvalRefWrapper<ArgType>>>
		{
			using Type = typename ArgType::ObjectType&;
		};
		template <class ArgType>
		struct ZzInternal_TResolveInvokeArgument<ArgType, TEnableIf<VIsRvalRefWrapper<ArgType>>>
		{
			using Type = typename ArgType::ObjectType&&;
		};

		template <class ArgType>
		using TResolveInvokeArgument = typename ZzInternal_TResolveInvokeArgument<ArgType>::Type;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  TMakeBinderFunctionSignature

		template <class CallableType, class ObjectType, class ReturnType, class ArgumentsPackType>
		using TMakeBinderFunctionSignature
			= TRemovePointer<
				TMakeFunctionSignature<
					void,
					ReturnType,
					TSelectIf<VIsDeferredObject<TDecayType<ObjectType>>,
							  typename ArgumentsPackType::template PushFront<TMemberPointerUnderlyingType<TDecayType<CallableType>>>,
							  ArgumentsPackType>,
					TFunctionQualifierSequence<>
				>
			>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GInvokeBinderStorage

		template <EBindMode BindModeValue, class CallableType, class SubCallableType, class ObjectType,
				  						   class ReturnType, class BoundArgumentsPackType, class UnboundArgumentsPackType,
										   class BoundArgumentIndexSequenceType, class UnboundArgumentIndexSequenceType>
		class GInvokeBinderStorage;

		template <EBindMode BindModeValue, class CallableType, class SubCallableType, class ObjectType,
				  						   class ReturnType, class ... BoundArgumentTypes, class ... UnboundArgumentTypes,
										   szint ... BoundArgumentIndexValue, szint ... UnboundArgumentIndexValue>
		class GInvokeBinderStorage<BindModeValue, CallableType, SubCallableType, ObjectType,
								   ReturnType, TTypePack<BoundArgumentTypes ...>, TTypePack<UnboundArgumentTypes ...>,
								   TIndexSequence<BoundArgumentIndexValue ...>, TIndexSequence<UnboundArgumentIndexValue ...>>
			: public IFunctionStorage<TMakeBinderFunctionSignature<CallableType, ObjectType, ReturnType, TTypePack<UnboundArgumentTypes ...>>>
		{
			using FunctionSignatureType = TBreakFunctionSignature<TMakeBinderFunctionSignature<CallableType, ObjectType,
																							   ReturnType, TTypePack<UnboundArgumentTypes ...>>>;

			template <szint IndexValue>
			using FunctionArgumentType = typename FunctionSignatureType::ParametersPackType::template ElementAt<IndexValue>;

		public:
			template <class CallType, class SubCallType, class ObjType, class ... ArgTypes>
			GInvokeBinderStorage(CallType&& NewCall, SubCallType&& NewSubCall, ObjType&& NewObj, ArgTypes&&... NewArgs)
				: BoundCallables{ Forward<CallType>(NewCall), Forward<SubCallType>(NewSubCall) }
				, BoundArguments{ Forward<ObjType>(NewObj), Forward<ArgTypes>(NewArgs) ... }
			{
			}

			// NOTE vvvv FOR GFunction INTERNAL USAGE ONLY vvvv
			virtual ReturnType Invoke(FunctionArgumentType<UnboundArgumentIndexValue> ... Arguments) override
			{
				if constexpr (!VIsDeferredObject<TDecayType<ObjectType>>)
				{
					return Invoke_Internal(TEmptyTypeTag{}, Forward<FunctionArgumentType<UnboundArgumentIndexValue>>(Arguments) ...);
				}
				else
				{
					return Invoke_Internal(Forward<FunctionArgumentType<UnboundArgumentIndexValue>>(Arguments) ...);
				}
			}
			virtual bool CanInvoke() const override
			{
				return CanInvoke_Internal();
			}

			virtual void RebindObject(voidptr NewObjectPtr) override
			{
				if constexpr (VIsPointer<TDecayType<ObjectType>>)
				{
					auto& ObjectRef = BoundArguments.template ObjectAt<0>();
					ObjectRef = static_cast<TDecayType<ObjectType>>(NewObjectPtr);
				}
			}
			// NOTE ^^^^ FOR GFunction INTERNAL USAGE ONLY ^^^^

			template <class ObjType, class ... ArgTypes>
			ReturnType Invoke_Internal([[maybe_unused]] ObjType&& UnboundObject, ArgTypes&&... UnboundArguments)
			{
				AEON_ASSERT(CanInvoke_Internal());

				using ThisType = decltype(this);

				// Logic to execute at the exit of the method; avoids wacky logic to wrangle the return value.
				struct SBindInvalidator
				{
					~SBindInvalidator()
					{
						// Null the callable/subcallable if any of the bound parameters is an rval-ref or an rval-ref wrapper,
						// as a subsequent invocation would likely pass invalid (moved-from) parameters.
						// Note that either one of these is always a pointer.
						constexpr bool IsNonIdempotent = TDisjunction<TDisjunction<TIsRvalueReference<BoundArgumentTypes>,
						    													   TIsRvalRefWrapper<BoundArgumentTypes>> ...>::Value;
						if constexpr (IsNonIdempotent)
						{
							if constexpr (VIsEmptyTypeTag<SubCallableType>)
							{
								This->BoundCallables.First() = nullptr;
							}
							else
							{
								This->BoundCallables.Second() = nullptr;
							}
						}
					}

					ThisType This;
				}
				Invalidator{ .This = this };

				if constexpr (VIsDeferredObject<TDecayType<ObjectType>>)
				{
					// CheckThenInvoke will re-compute the SubCallable (if necessary) from the function type.
					// SubCallable is always a member-pointer, but Callable may be a member-pointer or a regular pointer,
					// so we use the fact that TRemovePointer is passthrough when Callable is a member.
					using FunctionType = TRemoveMemberPointer<TSelectIf<VIsEmptyTypeTag<TDecayType<SubCallableType>>,
					    												TRemovePointer<TDecayType<CallableType>>,
					    												TDecayType<SubCallableType>>>;

					if constexpr (BindModeValue == EBindMode::FirstArgs)
					{
						return ZzInternal::CheckThenInvoke<ReturnType, FunctionType>(
							Forward<CallableType>(BoundCallables.First()),
							Forward<ObjType>(UnboundObject),
							Forward<BoundArgumentTypes>(BoundArguments.template ObjectAt<BoundArgumentIndexValue + 1>()) ...,
							Forward<UnboundArgumentTypes>(UnboundArguments) ...
						);
					}
					else
					{
						return ZzInternal::CheckThenInvoke<ReturnType, FunctionType>(
							Forward<CallableType>(BoundCallables.First()),
							Forward<ObjType>(UnboundObject),
							Forward<UnboundArgumentTypes>(UnboundArguments) ...,
							Forward<BoundArgumentTypes>(BoundArguments.template ObjectAt<BoundArgumentIndexValue + 1>()) ...
						);
					}
				}
				else
				{
					if constexpr (BindModeValue == EBindMode::FirstArgs)
					{
						return ZzInternal::Invoke<ReturnType>(
							Forward<CallableType>(BoundCallables.First()),
							Forward<SubCallableType>(BoundCallables.Second()),
							Forward<ObjectType>(BoundArguments.template ObjectAt<0>()),
							Forward<BoundArgumentTypes>(BoundArguments.template ObjectAt<BoundArgumentIndexValue + 1>()) ...,
							Forward<UnboundArgumentTypes>(UnboundArguments) ...
						);
					}
					else
					{
						return ZzInternal::Invoke<ReturnType>(
							Forward<CallableType>(BoundCallables.First()),
							Forward<SubCallableType>(BoundCallables.Second()),
							Forward<ObjectType>(BoundArguments.template ObjectAt<0>()),
							Forward<UnboundArgumentTypes>(UnboundArguments) ...,
							Forward<BoundArgumentTypes>(BoundArguments.template ObjectAt<BoundArgumentIndexValue + 1>()) ...
						);
					}
				}
			}

			bool CanInvoke_Internal() const
			{
				if constexpr (VIsEmptyTypeTag<SubCallableType>)
				{
					return BoundCallables.First();
				}
				else
				{
					return BoundCallables.Second();
				}
			}

			// NOTE
			//  - Must be invoked as 'ForwardBoundArg_Internal<TResolveInvokeArgument<BoundArgumentTypes>>(Arg)'
			//  - These forwarding functions are not strictly necessary
			//    because the checks in CheckThenBindInvoke already fail (correctly) on situations such as e.g.
			//    a parameter of type T& binding to the first parameter of a function with signature 'void(T&&)';
			//    note that, for the considered example, if T& is stored through GLvalRefWrapper<T> (from a call to Aeon::LRef),
			//    the conversion of GLvalRefWrapper<T> to T&& is allowed, meaning a simple call to Aeon::Forward would be incorrect.
			//  - This no longer applies since making operator T&& in GRefWrapper conditionally explicit.
#		if 0
			template <class ForwardArgType, class ArgType>
			AEON_FORCEINLINE static ForwardArgType ForwardBoundArg_Internal(ArgType&& Argument)
			{
				return Forward<ForwardArgType>(Argument);
			}
			template <class ForwardArgType, class ArgType>
			AEON_FORCEINLINE static ForwardArgType ForwardBoundArg_Internal(GLvalRefWrapper<ArgType> Argument)
			requires CSameClass<ForwardArgType, ArgType&>
			{
				return Forward<ForwardArgType>(Argument);
			}
			template <class ForwardArgType, class ArgType>
			AEON_FORCEINLINE static ForwardArgType ForwardBoundArg_Internal(GRvalRefWrapper<ArgType> Argument)
			requires CSameClass<ForwardArgType, ArgType&&>
			{
				return Forward<ForwardArgType>(Argument);
			}
#		endif

		private:
			GCompressedDuple<TDecayType<CallableType>, TDecayType<SubCallableType>> BoundCallables;
			GTuple<TDecayType<ObjectType>, TDecayType<BoundArgumentTypes> ...> BoundArguments;

		};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GInvokeBinder

		template <EBindMode BindModeValue, class CallableType, class SubCallableType, class ObjectType,
										   class ReturnType, class BoundArgumentsPackType, class UnboundArgumentsPackType>
		class GInvokeBinder
		{
			using StorageType = GInvokeBinderStorage<BindModeValue, CallableType, SubCallableType, ObjectType,
													 ReturnType, BoundArgumentsPackType, UnboundArgumentsPackType,
													 TMakeIndexSequence<BoundArgumentsPackType::Count>,
													 TMakeIndexSequence<UnboundArgumentsPackType::Count + VIsDeferredObject<TDecayType<ObjectType>>>>;

		public:
			template <class CallType, class SubCallType, class ObjType, class ... ArgTypes>
			GInvokeBinder(CallType&& NewCall, SubCallType&& NewSubCall, ObjType&& NewObj, ArgTypes&&... NewArgs)
				: Data{ MakeSharedPtr<StorageType>(Forward<CallType>(NewCall), Forward<SubCallType>(NewSubCall),
				    							   Forward<ObjType>(NewObj), Forward<ArgTypes>(NewArgs) ...) }
			{
			}

			GInvokeBinder(GInvokeBinder const& Other) = default;
			GInvokeBinder(GInvokeBinder&& Other) = default;

			GInvokeBinder& operator=(GInvokeBinder const& Other) = default;
			GInvokeBinder& operator=(GInvokeBinder&& Other) = default;

			template <class ... ArgTypes>
			ReturnType operator()(ArgTypes&& ... UnboundArguments)
			requires CNotDeferredObject<TDecayType<ObjectType>>
			{
				AEON_ASSERT(Data);
				return Data->Invoke_Internal(TEmptyTypeTag{}, Forward<ArgTypes>(UnboundArguments) ...);
			}
			template <class ObjType, class ... ArgTypes>
			ReturnType operator()(ObjType&& UnboundObject, ArgTypes&& ... UnboundArguments)
			requires CDeferredObject<TDecayType<ObjectType>>
			{
				AEON_ASSERT(Data);
				static_assert(VIsBaseClass<TMemberPointerUnderlyingType<TDecayType<CallableType>>,
										   TDecayType<ZzInternal::TResolveInvokeObject<ObjType>>>,
							  "'GInvokeBinder::operator()(ObjType&& Objs, ArgTypes&& ... Args)': "
							  "Invalid invocation: ObjType is not compatible with the underlying class type of the bound callable; "
							  "must be the same class or a subclass of the underlying class type.");
				return Data->Invoke_Internal(Forward<ObjType>(UnboundObject), Forward<ArgTypes>(UnboundArguments) ...);
			}

			explicit operator bool() const
			{
				AEON_ASSERT(Data);
				return Data->CanInvoke_Internal();
			}

			// NOTE vvvv FOR GFunction INTERNAL USAGE ONLY vvvv
			GSharedPtr<StorageType> MoveStorage_Internal()
			{
				AEON_ASSERT(Data);
				return Move(Data);
			}
			// NOTE ^^^^ FOR GFunction INTERNAL USAGE ONLY ^^^^

		private:
			GSharedPtr<StorageType> Data;

		};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  CheckThenBindInvoke

		template <EBindMode BindModeValue, class RetType, class FuncType, class CallType, class ObjType, class ... ArgTypes>
		auto CheckThenBindInvoke(CallType&& Call, ObjType&& Obj, ArgTypes&& ... Args)
		{
			// Obtain the actual class type from the passed object, which might be a CClassPointerLikeObject (pointer to class, or CManagedPointer).
			// If it's a SDeferredObject, extract the object type from the callable, and use it for any checks now;
			// some of these checks will be re-run once the binder's operator() is called with the actual object type.
			using ResolvedObjType = TSelectIf<VIsDeferredObject<TDecayType<ObjType>>,
			    							  TMemberPointerUnderlyingType<TDecayType<CallType>>,
			    							  TResolveInvokeObject<ObjType>>;

			// Henceforth, we consider that a callable of class (or member-object-pointer to class) type has a subcallable,
			// which is the operator() function that will effectively be called.

			// This will be used to obtain the return type and parameter types of the function that should be called;
			// it uses FuncType to disambiguate overload resolution if CallType is
			// of (member-object-pointer to) class type with an overloaded operator(),
			// otherwise, FuncType must exactly match the only candidate function signature,
			// or be TInferTypeTag.
			using ResolvedCallInfo = TResolveInvokeCallable<CallType, ResolvedObjType, FuncType>;

			constexpr bool CanInvoke = ResolvedCallInfo::IsValid;
			if constexpr (ResolvedCallInfo::CallableEntity == ECallableEntity::Object)
			{
				static_assert(CanInvoke,
							  "'BindInvoke<ReturnType, BindModeValue>(CallableType&& Callable, ArgumentTypes&& ... Arguments)': "
							  "Ambiguous/Invalid function: Callable must be func_cast to a function type "
							  "corresponding to an overload of operator() with CallableType underlying class type, "
							  "and with ArgumentTypes parameter types "
							  "(CallableType is a class type).");
			}
			else if constexpr (ResolvedCallInfo::CallableEntity == ECallableEntity::Function)
			{
				static_assert(CanInvoke,
							  "'BindInvoke<ReturnType, BindModeValue>(CallableType&& Callable, ArgumentTypes&& ... Arguments)': "
							  "Invalid function: Callable must be func_cast to a function type with ArgumentTypes parameter types, "
							  "that is an element of the overload set of functions of the same name "
							  "(CallableType is a function-pointer type).");
			}
			else if constexpr (ResolvedCallInfo::CallableEntity == ECallableEntity::MemberObject)
			{
				static_assert(CanInvoke,
							  "'BindInvoke<ReturnType, BindModeValue>(CallableType&& Callable, ObjectType&& Object, ArgumentTypes&& ... Arguments)': "
							  "Ambiguous/Invalid function: Callable must be func_cast to a function type "
							  "corresponding to an overload of operator() with CallableType underlying class type, "
							  "and with ArgumentTypes parameter types "
							  "(CallableType is a member-object-pointer to class type).");
			}
			else if constexpr (ResolvedCallInfo::CallableEntity == ECallableEntity::MemberFunction)
			{
				static_assert(CanInvoke,
							  "'BindInvoke<ReturnType, BindModeValue>(CallableType&& Callable, ObjectType&& Object, ArgumentTypes&& ... Arguments)': "
							  "Invalid function: Callable must be func_cast to a member function type with ObjectType underlying class type, "
							  "and with ArgumentTypes parameter types, "
							  "that is an element of the overload set of member functions of the same name "
							  "(CallableType is a member-function-pointer type).");
			}
			else
			{
				static_assert(CanInvoke,
							  "'BindInvoke<ReturnType, BindModeValue>(CallableType&& Callable, ObjectType&& Object, ArgumentTypes&& ... Arguments)': "
							  "Callable must be a callable object of the following type: "
							  "an object, potentially cv-qualified, of class type with an overloaded operator(), "
							  "a function-pointer, "
							  "a member-object-pointer to an object, potentially cv-qualified, of class type with an overloaded operator(), "
							  "or a member-function-pointer.");
			}

			using FuncSignatureInfo = TBreakFunctionSignature<TSelectIf<ResolvedCallInfo::HasSubCallable,
																		typename ResolvedCallInfo::SubCallableType,
																		typename ResolvedCallInfo::CallableType>>;

			// Return type of the invocation expression
			using FuncRetType = typename FuncSignatureInfo::ReturnType;
			// Argument types of the invocation expression
			using FuncArgsPackType = typename FuncSignatureInfo::ParametersPackType;

			// Argument types of bound arguments, passed to BindInvoke, that GInvokeBinderStorage::operator() will forward to Invoke
			// NOTE GInvokeBinderStorage will store GRefWrapper objects directly, and then unwrap/forward the appropriate reference to Invoke
			using BoundArgsPackType = TTypePack<ArgTypes ...>;
			// Argument types of unbound arguments, to pass to GInvokeBinderStorage::operator(), that GInvokeBinderStorage::operator() will forward to Invoke
			using UnboundArgsPackType =
				AEON_TRAIT_IF(BindModeValue == EBindMode::FirstArgs)
				<
					typename FuncArgsPackType::template PopFront<BoundArgsPackType::Count>
				>
				AEON_TRAIT_ELSE
				<
					typename FuncArgsPackType::template PopBack<BoundArgsPackType::Count>
				>
				AEON_TRAIT_ENDIF;

			// Obtain the actual bound argument types from the passed arguments, which might include GRefWrapper objects
			using ResolvedArgsPackType = TTypePack<TResolveInvokeArgument<ArgTypes> ...>;

			// Return type of underlying Invoke call when GInvokeBinderStorage::operator() is called
			using InvokeRetType = ZzInternal::TResolveInvokeReturn<FuncRetType, RetType>;
			// Argument types of underlying Invoke call when GInvokeBinderStorage::operator() is called
			using InvokeArgsPackType =
				AEON_TRAIT_IF(BindModeValue == EBindMode::FirstArgs)
				<
					typename ResolvedArgsPackType::template Append<UnboundArgsPackType>
				>
				AEON_TRAIT_ELSE
				<
					typename UnboundArgsPackType::template Append<ResolvedArgsPackType>
				>
				AEON_TRAIT_ENDIF;

			// Check if the callable, the actual function, which is either the (member-)function-pointer,
			// or the function call operator (when Callable is of (member-object-pointer to) class type),
			// is effectively callable with the passed Callable and Object arguments.

			// NOTE We need to use the resolved callable instead of the passed one
			//  as the user might have specified a different function with func_cast,
			//  i.e. this would not work:
			// 		using CallableTraits =
			// 			AEON_TRAIT_IF(VIsEmptyTypeTag<ObjType>)
			// 			<
			// 				TCallableTraits<CallType, InvokeArgsType>
			// 			>
			//			AEON_TRAIT_ELSE
			// 			<
			// 				TMemberCallableTraits<CallType, ResolvedObjType, InvokeArgsType>
			//			>
			//			AEON_TRAIT_ENDIF;

			// TResolveInvokeCallable types for different callable entities:
			// 	Type\Entity	| Class		| Func 		| MmbClass 	| MmbFunc	|
			//	ObjType		| --- 		| --- 		| Obj 		| Obj		|
			//	SubObjType	| Call 		| --- 		| Obj.*Call	| ---		|
			//	CallType	| --- 		| Call 		| --- 		| Call		|
			//	SubCallType	| op()		| ---		| op()		| ---		|
			// Note that SubObjType and CallType are the same parameter (CallType) for Invoke/GInvokeBinderStorage,
			// only represented here as separate entities for clarity.

			using InvokeObjType = typename ResolvedCallInfo::ObjectType;
			using InvokeSubObjType = typename ResolvedCallInfo::SubObjectType;

			using InvokeCallType = typename ResolvedCallInfo::CallableType;
			using InvokeSubCallType = typename ResolvedCallInfo::SubCallableType;

			using CallableTraits =
				AEON_TRAIT_IF(ResolvedCallInfo::CallableEntity == ECallableEntity::Object)
				<
					ZzInternal_TMemberCallableTraits<InvokeSubCallType, InvokeSubObjType, InvokeArgsPackType>
				>
				AEON_TRAIT_ELSEIF(ResolvedCallInfo::CallableEntity == ECallableEntity::Function)
				<
					ZzInternal_TCallableTraits<InvokeCallType, InvokeArgsPackType>
				>
				AEON_TRAIT_ELSEIF(ResolvedCallInfo::CallableEntity == ECallableEntity::MemberObject)
				<
					ZzInternal_TMemberCallableTraits<InvokeSubCallType, InvokeSubObjType, InvokeArgsPackType>
				>
				AEON_TRAIT_ELSEIF(ResolvedCallInfo::CallableEntity == ECallableEntity::MemberFunction)
				<
					ZzInternal_TMemberCallableTraits<InvokeCallType, InvokeObjType, InvokeArgsPackType>
				>
				AEON_TRAIT_ELSE
				<
					ZzInternal_TCallableTraits<void, TTypePack<>>
				>
				AEON_TRAIT_ENDIF;

			// This refers to Callable, SubCallable, and Object
			constexpr bool CanInvokeWithReturn = CanInvoke ? !VIsNullTypeTag<InvokeRetType> : true;
			constexpr bool CanInvokeWithParameters = CanInvokeWithReturn ? CallableTraits::IsCallable : true;
			if constexpr (VIsEmptyTypeTag<ObjType>)
			{
				static_assert(CanInvokeWithReturn,
							  "'BindInvoke<ReturnType, BindModeValue>(CallableType&& Callable, ArgumentTypes&& ... Arguments)': "
							  "ReturnType must be either the return type of the callable, a type convertible therefrom, or void.");

				static_assert(CanInvokeWithParameters,
							  "'BindInvoke<ReturnType, BindModeValue>(CallableType&& Callable, ArgumentTypes&& ... Arguments)': "
							  "Arguments should correspond to a subset of the parameters (of N arguments for M total parameters) "
							  "of some potential overload of either operator() or of the function "
							  "(for either class or function-pointer type, respectively), "
							  "and which are provided in order of declaration, starting from the first parameter to parameter N, "
							  "or from parameter M-N to the last parameter, depending on BindModeValue.");

			}
			else
			{
				static_assert(CanInvokeWithReturn,
							  "'BindInvoke<ReturnType, BindModeValue>(CallableType&& Callable, ObjectType&& Object, ArgumentTypes&& ... Arguments)': "
							  "ReturnType must be either the return type of the callable, a type convertible therefrom, or void.");

				static_assert(CanInvokeWithParameters,
							  "'BindInvoke<ReturnType, BindModeValue>(CallableType&& Callable, ObjectType&& Object, ArgumentTypes&& ... Arguments)': "
							  "Object must be of the underlying class type of Callable, or a subclass thereof, "
							  "and the cv-qualification and ref-qualification of ObjectType and CallableType must match; "
							  "Arguments should correspond to a subset of the parameters (of N arguments for M total parameters) "
							  "of some potential overload of either operator() or of the function "
							  "(for either member-object-pointer to class or member-function-pointer type, respectively), "
							  "and which are provided in order of declaration, starting from the first parameter to parameter N, "
							  "or from parameter M-N to the last parameter, depending on BindModeValue.");
			}

			// Obtain SubCall info, regardless of existence.
			// We'll just pass this directly to GInvokeBinderStorage, TResolveInvokeCallable already makes an appropriate type/object
			// that Invoke can deal with when SubCall is non-existent.
			using SubCallType = typename ResolvedCallInfo::SubCallableType;
			constexpr SubCallType SubCall = ResolvedCallInfo::SubCallable;

			// CHECKME Consider extending BindInvoke to allow specifying to return the binder object on the stack,
			//  returning GInvokeBinderStorage directly instead of a reference
			using BinderType = ZzInternal::GInvokeBinder<BindModeValue,
														 CallType, SubCallType, ObjType,
														 InvokeRetType, BoundArgsPackType, UnboundArgsPackType>;

			return BinderType{ Forward<CallType>(Call), SubCall, Forward<ObjType>(Obj), Forward<ArgTypes>(Args) ... };
		}
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  TIsInvokeBinder

	template <class Type>
	concept CInvokeBinder = CSameClassTemplate<Type, ZzInternal::GInvokeBinder<EBindMode::FirstArgs,
																			   void(*)(void), TEmptyTypeTag, TEmptyTypeTag,
																			   void, TTypePack<>, TTypePack<>>>;
	template <class Type>
	concept CNotInvokeBinder = !CInvokeBinder<Type>;

	template <class Type>
	using TIsInvokeBinder = TValueTrait<CInvokeBinder<Type>>;
	template <class Type>
	inline constexpr bool VIsInvokeBinder = CInvokeBinder<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  Deferred

	// Mono-state type instance used to inform BindInvoke that the instance object of the callable's underlying class type,
	// when the callable is of member-pointer type, will be unbound and passed at invocation time
	[[maybe_unused]]
	inline constexpr ZzInternal::SDeferredObject Deferred;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  BindInvoke

	template <class ReturnType = TInferTypeTag, EBindMode BindModeValue = EBindMode::FirstArgs,
			  class WrappedCallableType, class ... ArgumentTypes>
	AEON_NODISCARD AEON_FORCEINLINE auto BindInvoke(WrappedCallableType&& Callable, ArgumentTypes&& ... Arguments)
	requires CNotMemberPointer<TDecayType<typename ZzInternal::TUnwrapCallable<WrappedCallableType>::CallableType>>
	{
		using FunctionType = typename ZzInternal::TUnwrapCallable<WrappedCallableType>::FunctionType;
		using CallableType = typename ZzInternal::TUnwrapCallable<WrappedCallableType>::CallableType;

		return ZzInternal::CheckThenBindInvoke<BindModeValue, ReturnType, FunctionType>(Forward<CallableType>(Callable),
																						TEmptyTypeTag{},
																						Forward<ArgumentTypes>(Arguments) ...);
	}

	template <class ReturnType = TInferTypeTag, EBindMode BindModeValue = EBindMode::FirstArgs,
			  class WrappedCallableType, class ObjectType, class ... ArgumentTypes>
	AEON_NODISCARD AEON_FORCEINLINE auto BindInvoke(WrappedCallableType&& Callable, ObjectType&& Object, ArgumentTypes&& ... Arguments)
	requires CMemberPointer<TDecayType<typename ZzInternal::TUnwrapCallable<WrappedCallableType>::CallableType>>
			 && (CClass<TDecayType<ObjectType>> || ZzInternal::CClassPointerLikeObject<TDecayType<ObjectType>>)
	{
		using FunctionType = typename ZzInternal::TUnwrapCallable<WrappedCallableType>::FunctionType;
		using CallableType = typename ZzInternal::TUnwrapCallable<WrappedCallableType>::CallableType;

		static_assert(!CManagedPointer<TMemberPointerUnderlyingType<TDecayType<CallableType>>>,
					  "'BindInvoke<ReturnType, BindModeValue>(CallableType&& Callable, ObjectType&& Object, ArgumentTypes&& ... Arguments)': "
					  "Callable cannot be a member-function-pointer or member-object-pointer to class with underlying CManagedPointer class type.");

		return ZzInternal::CheckThenBindInvoke<BindModeValue, ReturnType, FunctionType>(Forward<CallableType>(Callable),
																						Forward<ObjectType>(Object),
																						Forward<ArgumentTypes>(Arguments) ...);
	}
}

#endif