#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_FUNCTIONAL_DELEGATE
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_FUNCTIONAL_DELEGATE

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Container/Array.hpp"
#include "Aeon/Framework/Functional/Function.hpp"

namespace Aeon::ZzInternal
{
#define AEON_EVENT_HANDLER_CLASS ::Aeon::ZzInternal::SEventHandler
#define AEON_EVENT_HANDLER public virtual AEON_EVENT_HANDLER_CLASS

#define AEON_GENERATE_EVENT_HANDLER_OVERRIDES(...) \
	virtual bool CheckHandlerTypeInfo(STypeInfo Info) override\
	{\
		return Info == STypeInfo{ typeid(this) } \
			|| AEON_LIST_HEAD(__VA_ARGS__ __VA_OPT__(,) ::Aeon::ZzInternal::SEventHandler) ::CheckHandlerTypeInfo(Info);\
	}\
	virtual voidptr HandlerThis() override\
	{\
		return this;\
	}\
	static_assert(AEON_LIST_SIZE(__VA_ARGS__) <= 1,\
		"AEON_GENERATE_EVENT_HANDLER_OVERRIDES must have zero or one argument, "\
		"which should be the most-derived base class which is also an AEON_EVENT_HANDLER.")

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// IEventDelegate

	class IEventDelegate
	{
	protected:
		IEventDelegate() = default;
		~IEventDelegate() = default;

		IEventDelegate(IEventDelegate const&) = delete;
		IEventDelegate(IEventDelegate&&) = delete;

		IEventDelegate& operator=(IEventDelegate const&) = delete;
		IEventDelegate& operator=(IEventDelegate&&) = delete;
	public:
		AEON_DECLARE_ACCESS_CLASS(HandlerAccess, class SEventHandler);

	//	virtual void DuplicateHandler(HandlerAccess, SEventHandler* OldHandler, SEventHandler* NewHandler) = 0;
		virtual void CleanupHandler(HandlerAccess, SEventHandler* Handler) = 0;

		// NOTE Defined after SEventHandler, as these methods need that class to be complete
		void RegisterThisInHandler(SEventHandler* Handler);
		void UnregisterThisInHandler(SEventHandler* Handler);
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SEventHandler

	class SEventHandler
	{
	protected:
		SEventHandler() = default;
		virtual ~SEventHandler()
		{
			CleanupHandler_Internal();
		}

	public:
		AEON_DECLARE_ACCESS_CLASS(DelegateAccess, class IEventDelegate);

		SEventHandler(SEventHandler const&) = delete;
		SEventHandler(SEventHandler&&) = delete;

		SEventHandler& operator=(SEventHandler const&) = delete;
		SEventHandler& operator=(SEventHandler&&) = delete;

#	if 0
		void DuplicateHandler(SEventHandler const& Other)
		{
			DelegateRegistryArray = Other.DelegateRegistryArray;
			for (auto& Delegate : DelegateRegistryArray)
			{
				Delegate->DuplicateHandler(IEventDelegate::HandlerAccess::Tag, const_cast<SEventHandler*>(addressof(Other)), this);
			}
		}
#	endif

		void RegisterDelegate(DelegateAccess, IEventDelegate* NewDelegate)
		{
			DelegateRegistryArray.AddSorted(NewDelegate);
		}
		void UnregisterDelegate(DelegateAccess, IEventDelegate* Delegate)
		{
			auto Index = DelegateRegistryArray.FindIndexSorted(Delegate);
			AEON_ASSERT(Index);
			DelegateRegistryArray.RemoveAt(*Index);
		}

#	if 0
		virtual bool CheckHandlerTypeInfo(STypeInfo)
		{
			return false;
		}
		virtual voidptr HandlerThis()
		{
			return this;
		}
#	endif

	private:
		void CleanupHandler_Internal()
		{
			for (auto& Delegate : DelegateRegistryArray)
			{
				Delegate->CleanupHandler(IEventDelegate::HandlerAccess::Tag, this);
			}
		}

		GArray<IEventDelegate*> DelegateRegistryArray;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// IEventDelegate::RegisterThisInHandler, IEventDelegate::UnregisterThisInHandler

	inline void IEventDelegate::RegisterThisInHandler(SEventHandler* Handler)
	{
		Handler->RegisterDelegate(SEventHandler::DelegateAccess::Tag, this);
	}
	inline void IEventDelegate::UnregisterThisInHandler(SEventHandler* Handler)
	{
		Handler->UnregisterDelegate(SEventHandler::DelegateAccess::Tag, this);
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GEventFuncHandlerData

	template <CSimpleFunction FunctionType, CScalarObject MaskType>
	class GEventFuncHandlerData
	{
	public:
		GEventFuncHandlerData(FunctionType* NewHandler)
			: Handler{ NewHandler }
			, MaskArray{ TReserveStorage::Tag, 0 }
		{
		}

		bool operator==(GEventFuncHandlerData const& Other) const
		{
			return Handler == Other.Handler;
		}
		strong_ordering operator<=>(GEventFuncHandlerData const& Other) const
		{
			return AeonMemory::MemCompare(addressof(Handler), addressof(Other.Handler), sizeof(Handler)) <=> 0;
		}

		FunctionType* Handler;
		GArray<MaskType> MaskArray;
	};
	template <CSimpleFunction FunctionType>
	class GEventFuncHandlerData<FunctionType, TEmptyTypeTag>
	{
	public:
		GEventFuncHandlerData(FunctionType* NewHandler)
			: Handler{ NewHandler }
		{
		}

		bool operator==(GEventFuncHandlerData const& Other) const
		{
			return Handler == Other.Handler;
		}
		strong_ordering operator<=>(GEventFuncHandlerData const& Other) const
		{
			return AeonMemory::MemCompare(addressof(Handler), addressof(Other.Handler), sizeof(Handler)) <=> 0;
		}

		FunctionType* Handler;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GEventObjFuncHandlerData

	template <CSimpleFunction FunctionType, CScalarObject MaskType>
	class GEventObjFuncHandlerData
	{
	public:
		// NOTE Experimental limit, based on the size of member-function-pointers (for 64-bit platforms) in major compilers;
		//  if the asserts ever break, we can probably just increase this limit.
		static constexpr szint HandlerDataSizeLimit = 16;

		template <CClass ObjectType>
		GEventObjFuncHandlerData(FunctionType ObjectType::* NewHandler)
			: HandlerByteData{}
			, HandlerTypeInfo{ STypeInfo::Null() }
			, Handler{}
			, MaskArray{ TReserveStorage::Tag, 0 }
		{
			static_assert(sizeof(NewHandler) <= HandlerDataSizeLimit,
						  "Compiler uses member-function-pointers wider than 16 bytes; review implementation.");
			AeonMemory::MemCopy(HandlerByteData, addressof(NewHandler), sizeof(NewHandler));
		}
		template <CClass ActualObjectType, CClass ObjectType>
		GEventObjFuncHandlerData(ActualObjectType* NewHandlerObject, FunctionType ObjectType::* NewHandler)
		requires CBaseClass<ObjectType, ActualObjectType>
			: HandlerByteData{}
			, HandlerTypeInfo{ typeid(ObjectType*) }
			, Handler{ Aeon::BindInvoke<void>(NewHandler, NewHandlerObject) }
			, MaskArray{ TReserveStorage::Tag, 0 }
		{
			static_assert(sizeof(NewHandler) <= HandlerDataSizeLimit,
						  "Compiler uses member-function-pointers wider than 16 bytes; review implementation.");
			AeonMemory::MemCopy(HandlerByteData, addressof(NewHandler), sizeof(NewHandler));
		}

		bool operator==(GEventObjFuncHandlerData const& Other) const
		{
			return AeonMemory::MemCompare(HandlerByteData, Other.HandlerByteData, HandlerDataSizeLimit) == 0;
		}
		strong_ordering operator<=>(GEventObjFuncHandlerData const& Other) const
		{
			return AeonMemory::MemCompare(HandlerByteData, Other.HandlerByteData, HandlerDataSizeLimit) <=> 0;
		}

		byte HandlerByteData[HandlerDataSizeLimit];
		STypeInfo HandlerTypeInfo;
		GFunction<FunctionType> Handler;
		GArray<MaskType> MaskArray;
	};
	template <CSimpleFunction FunctionType>
	class GEventObjFuncHandlerData<FunctionType, TEmptyTypeTag>
	{
	public:
		// NOTE Experimental limit, based on the size of member-function-pointers (for 64-bit platforms) in major compilers;
		//  if the asserts ever break, we can probably just increase this limit.
		static constexpr szint HandlerDataSizeLimit = 16;

		template <CClass ObjectType>
		GEventObjFuncHandlerData(FunctionType ObjectType::* NewHandler)
			: HandlerByteData{}
			, HandlerTypeInfo{ STypeInfo::Null() }
			, Handler{}
		{
			static_assert(sizeof(NewHandler) <= HandlerDataSizeLimit,
						  "Compiler uses member-function-pointers wider than 16 bytes; review implementation.");
			AeonMemory::MemCopy(HandlerByteData, addressof(NewHandler), sizeof(NewHandler));
		}
		template <CClass ActualObjectType, CClass ObjectType>
		GEventObjFuncHandlerData(ActualObjectType* NewHandlerObject, FunctionType ObjectType::* NewHandler)
		requires CBaseClass<ObjectType, ActualObjectType>
			: HandlerByteData{}
			, HandlerTypeInfo{ typeid(ObjectType*) }
			, Handler{ Aeon::BindInvoke<void>(NewHandler, NewHandlerObject) }
		{
			static_assert(sizeof(NewHandler) <= HandlerDataSizeLimit,
						  "Compiler uses member-function-pointers wider than 16 bytes; review implementation.");
			AeonMemory::MemCopy(HandlerByteData, addressof(NewHandler), sizeof(NewHandler));
		}

		bool operator==(GEventObjFuncHandlerData const& Other) const
		{
			return AeonMemory::MemCompare(HandlerByteData, Other.HandlerByteData, HandlerDataSizeLimit) == 0;
		}
		strong_ordering operator<=>(GEventObjFuncHandlerData const& Other) const
		{
			return AeonMemory::MemCompare(HandlerByteData, Other.HandlerByteData, HandlerDataSizeLimit) <=> 0;
		}

		byte HandlerByteData[HandlerDataSizeLimit];
		STypeInfo HandlerTypeInfo;
		GFunction<FunctionType> Handler;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GEventObjHandlerData

	template <CSimpleFunction FunctionType, CScalarObject MaskType>
	class GEventObjHandlerData
	{
	public:
		GEventObjHandlerData(SEventHandler* NewHandlerObject)
			: HandlerObject{ NewHandlerObject }
		{
		}

		bool operator==(GEventObjHandlerData const& Other) const
		{
			return HandlerObject == Other.HandlerObject;
		}
		strong_ordering operator<=>(GEventObjHandlerData const& Other) const
		{
			return HandlerObject <=> Other.HandlerObject;
		}

		SEventHandler* HandlerObject;
		GArray<GEventObjFuncHandlerData<FunctionType, MaskType>> HandlersArray;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GEventDelegateStorage

	template <CSimpleFunction FunctionType, CScalarObject MaskType>
	class GEventDelegateStorage;

	template <class RetType, class ... ArgTypes, CScalarObject MaskType>
	requires CSameClass<typename TTypePack<ArgTypes ...>::Front, MaskType> || TIsEmptyTypeTag<MaskType>::Value
	class GEventDelegateStorage<RetType(ArgTypes ...), MaskType> : private IEventDelegate
	{
	protected:
		using FunctionType = RetType(ArgTypes ...);

		using FuncHandlerType = GEventFuncHandlerData<FunctionType, MaskType>;
		using ObjHandlerType = GEventObjHandlerData<FunctionType, MaskType>;
		using ObjFuncHandlerType = GEventObjFuncHandlerData<FunctionType, MaskType>;

		GEventDelegateStorage() = default;
		~GEventDelegateStorage()
		{
			UnbindAll();
		}

	public:
		bool Bind(FunctionType* Handler)
		{
			AEON_ASSERT(Handler);

			AEON_FAILSAFE(!FuncHandlersArray.ContainsSorted(Handler), return false,
						  "Trying to re-bind already bound handler!");

			FuncHandlersArray.AddSorted({ Handler });
			return true;
		}
		template <CClass ActualObjectType, CClass ObjectType>
		bool Bind(ActualObjectType* HandlerObject, FunctionType ObjectType::* Handler)
		requires CBaseClass<SEventHandler, ObjectType> && CBaseClass<ObjectType, ActualObjectType>
		{
			AEON_ASSERT(HandlerObject && Handler);

			auto ObjectDataOpt = ObjHandlersArray.FindSorted(HandlerObject);
			auto& ObjectData = ObjectDataOpt ? ObjectDataOpt.Get() : ObjHandlersArray.AddSorted({ HandlerObject });

			if (!ObjectDataOpt)
			{
				RegisterThisInHandler(HandlerObject);
			}

			AEON_FAILSAFE(!ObjectData.HandlersArray.ContainsSorted(Handler), return false,
						  "Trying to re-bind already bound handler!");

			ObjectData.HandlersArray.AddSorted({ HandlerObject, Handler });
			return true;
		}

		template <CScalarObject NewMaskType>
		bool BindWithMask(NewMaskType&& MaskObject, FunctionType* Handler)
		requires CSameClass<TDecayType<NewMaskType>, MaskType>
		{
			static_assert(!TIsEmptyTypeTag<MaskType>::Value);

			AEON_ASSERT(Handler);

			auto HandlerDataOpt = FuncHandlersArray.FindSorted(Handler);
			auto& HandlerData = HandlerDataOpt ? HandlerDataOpt.Get() : FuncHandlersArray.AddSorted({ Handler });

			if (HandlerDataOpt)
			{
				AEON_FAILSAFE(!HandlerData.MaskArray.IsEmpty(), return false,
							  "Trying to re-bind already bound non-masked handler!");

				AEON_FAILSAFE(!HandlerData.MaskArray.Contains(MaskObject), return false,
							  "Trying to re-bind already bound handler with existing mask!");
			}

			HandlerData.MaskArray.Add(Forward<NewMaskType>(MaskObject));
			return true;
		}
		template <CScalarObject NewMaskType, CClass ActualObjectType, CClass ObjectType>
		bool BindWithMask(NewMaskType&& MaskObject, ActualObjectType* HandlerObject, FunctionType ObjectType::* Handler)
		requires CSameClass<TDecayType<NewMaskType>, MaskType> && CBaseClass<SEventHandler, ObjectType> && CBaseClass<ObjectType, ActualObjectType>
		{
			static_assert(!TIsEmptyTypeTag<MaskType>::Value);

			AEON_ASSERT(HandlerObject && Handler);

			// Find else add object handler data

			auto ObjectDataOpt = ObjHandlersArray.FindSorted(HandlerObject);
			auto& ObjectData = ObjectDataOpt ? ObjectDataOpt.Get() : ObjHandlersArray.AddSorted({ HandlerObject });

			if (!ObjectDataOpt)
			{
				RegisterThisInHandler(HandlerObject);
			}

			// Find else add handler data

			auto HandlerDataOpt = ObjectData.HandlersArray.FindSorted(Handler);
			auto& HandlerData = HandlerDataOpt ? HandlerDataOpt.Get() : ObjectData.HandlersArray.AddSorted({ HandlerObject, Handler });

			// Add mask object if not present; avoid check if we just added the handler

			if (HandlerDataOpt)
			{
				AEON_FAILSAFE(!HandlerData.MaskArray.IsEmpty(), return false,
							  "Trying to re-bind already bound non-masked handler!");

				AEON_FAILSAFE(!HandlerData.MaskArray.Contains(MaskObject), return false,
							  "Trying to re-bind already bound handler with existing mask!");
			}

			HandlerData.MaskArray.Add(Forward<NewMaskType>(MaskObject));
			return true;
		}

		void Unbind(FunctionType* Handler)
		{
			AEON_ASSERT(Handler);

			if (auto Index = FuncHandlersArray.FindIndexSorted(Handler))
			{
				auto& HandlerData = FuncHandlersArray[*Index];

				if constexpr (!TIsEmptyTypeTag<MaskType>::Value)
				{
					AEON_FAILSAFE(HandlerData.MaskArray.IsEmpty(), return,
								  "Trying to unbind masked handler!");
				}

				FuncHandlersArray.RemoveAt(*Index);
			}
		}
		template <CClass ActualObjectType, CClass ObjectType>
		void Unbind(ActualObjectType* HandlerObject, FunctionType ObjectType::* Handler)
		requires CBaseClass<SEventHandler, ObjectType> && CBaseClass<ObjectType, ActualObjectType>
		{
			AEON_ASSERT(HandlerObject && Handler);

			if (auto ObjectIndex = ObjHandlersArray.FindIndexSorted(HandlerObject))
			{
				auto& ObjectData = ObjHandlersArray[*ObjectIndex];

				if (auto HandlerIndex = ObjectData.HandlersArray.FindIndexSorted(Handler))
				{
					auto& HandlerData = ObjectData.HandlersArray[*HandlerIndex];

					if constexpr (!TIsEmptyTypeTag<MaskType>::Value)
					{
						AEON_FAILSAFE(HandlerData.MaskArray.IsEmpty(), return,
									  "Trying to unbind masked handler!");
					}

					ObjectData.HandlersArray.RemoveAt(*HandlerIndex);
				}

				if (ObjectData.HandlersArray.IsEmpty())
				{
					UnregisterThisInHandler(HandlerObject);
					ObjHandlersArray.RemoveAt(*ObjectIndex);
				}
			}
		}

		template <CScalarObject NewMaskType>
		void Unbind(NewMaskType&& MaskObject, FunctionType* Handler)
		requires CSameClass<TDecayType<NewMaskType>, MaskType>
		{
			static_assert(!TIsEmptyTypeTag<MaskType>::Value);

			AEON_ASSERT(Handler);

			if (auto HandlerIndex = FuncHandlersArray.FindIndexSorted(Handler))
			{
				auto& HandlerData = FuncHandlersArray[*HandlerIndex];

				AEON_FAILSAFE(!HandlerData.MaskArray.IsEmpty(), return,
							  "Trying to unbind non-masked handler!");

				if (auto MaskIndex = HandlerData.MaskArray.FindIndex(MaskObject))
				{
					HandlerData.MaskArray.RemoveAt(*MaskIndex);
				}

				if (HandlerData.MaskArray.IsEmpty())
				{
					FuncHandlersArray.RemoveAt(*HandlerIndex);
				}
			}
		}
		template <CScalarObject NewMaskType, CClass ActualObjectType, CClass ObjectType>
		void Unbind(NewMaskType&& MaskObject, ActualObjectType* HandlerObject, FunctionType ObjectType::* Handler)
		requires CSameClass<TDecayType<NewMaskType>, MaskType> && CBaseClass<SEventHandler, ObjectType> && CBaseClass<ObjectType, ActualObjectType>
		{
			static_assert(!TIsEmptyTypeTag<MaskType>::Value);

			AEON_ASSERT(HandlerObject && Handler);

			if (auto ObjectIndex = ObjHandlersArray.FindIndexSorted(HandlerObject))
			{
				auto& ObjectData = ObjHandlersArray[*ObjectIndex];

				if (auto HandlerIndex = ObjectData.HandlersArray.FindIndexSorted(Handler))
				{
					auto& HandlerData = ObjectData.HandlersArray[*HandlerIndex];

					AEON_FAILSAFE(!HandlerData.MaskArray.IsEmpty(), return,
								  "Trying to unbind non-masked handler!");

					if (auto MaskIndex = HandlerData.MaskArray.FindIndex(MaskObject))
					{
						HandlerData.MaskArray.RemoveAt(*MaskIndex);
					}

					if (HandlerData.MaskArray.IsEmpty())
					{
						HandlerData.HandlersArray.RemoveAt(*HandlerIndex);
					}
				}

				if (ObjectData.HandlersArray.IsEmpty())
				{
					UnregisterThisInHandler(HandlerObject);
					ObjHandlersArray.RemoveAt(*ObjectIndex);
				}
			}
		}

		void Unbind(SEventHandler* HandlerObject)
		{
			AEON_ASSERT(HandlerObject);

			if (auto Index = ObjHandlersArray.FindIndexSorted(HandlerObject))
			{
				UnregisterThisInHandler(HandlerObject);
				ObjHandlersArray.RemoveAt(*Index);
			}
		}

		// NOTE Currently not exposed in public interface
		void UnbindFunctions()
		{
			FuncHandlersArray.Empty();
		}

		// NOTE Currently not exposed in public interface
		void UnbindObjects()
		{
			for (auto& ObjectData : ObjHandlersArray)
			{
				UnregisterThisInHandler(ObjectData.HandlerObject);
			}

			ObjHandlersArray.Empty();
		}

		void UnbindAll()
		{
			UnbindFunctions();
			UnbindObjects();
		}

	protected:
		void Broadcast(ArgTypes ... Args)
		{
			AEON_FAILSAFE(CanBroadcast, return, "Currently already broadcasting through this delegate!");

			GScopeObjectGuard BroadcastGuard{ CanBroadcast, false };

			if constexpr (TIsEmptyTypeTag<MaskType>::Value)
			{
				for (auto& HandlerData : FuncHandlersArray)
				{
					HandlerData.Handler(Forward<ArgTypes>(Args)...);
				}
				for (auto& ObjectData : ObjHandlersArray)
				{
					for (auto& HandlerData : ObjectData.HandlersArray)
					{
#					if 0
						AEON_FAILSAFE(ObjectData.HandlerObject->CheckHandlerTypeInfo(HandlerData.HandlerTypeInfo), continue,
									  "Trying to invoke GFunction::operator() with a bound object type deriving from AEON_EVENT_HANDLER "
									  " which is not compatible with the bound callable type!\n"
									  "Ensure you have declared AEON_GENERATE_EVENT_HANDLER_OVERRIDES in the handler object class.");
#					endif

						HandlerData.Handler(Forward<ArgTypes>(Args)...);
					}
				}
			}
			else
			{
				for (auto& HandlerData : FuncHandlersArray)
				{
					if (HandlerData.MaskArray.IsEmpty()
						|| HandlerData.MaskArray.Contains(ExtractMaskArgument_Internal(Forward<ArgTypes>(Args)...)))
					{
						HandlerData.Handler(Forward<ArgTypes>(Args)...);
					}
				}
				for (auto& ObjectData : ObjHandlersArray)
				{
					for (auto& HandlerData : ObjectData.HandlersArray)
					{
#					if 0
						AEON_FAILSAFE(ObjectData.HandlerObject->CheckHandlerTypeInfo(HandlerData.HandlerTypeInfo), continue,
									  "Trying to invoke GFunction::operator() with a bound object type deriving from AEON_EVENT_HANDLER "
									  " which is not compatible with the bound callable type!\n"
									  "Ensure you have declared AEON_GENERATE_EVENT_HANDLER_OVERRIDES in the handler object class.");
#					endif

						if (HandlerData.MaskArray.IsEmpty()
							|| HandlerData.MaskArray.Contains(ExtractMaskArgument_Internal(Forward<ArgTypes>(Args)...)))
						{
							HandlerData.Handler(Forward<ArgTypes>(Args)...);
						}
					}
				}
			}
		}

	private:
#	if 0
		virtual void DuplicateHandler(HandlerAccess, SEventHandler* OldHandlerObject, SEventHandler* NewHandlerObject) override
		{
			// NOTE This is always called by an handler that is bound to this delegate and is being copied/moved,
			//  so no need to either check presence or register the delegate

			// NOTE Need to add the new element first because it might trigger a reallocation
			auto& NewObjectData = ObjHandlersArray.AddSorted(NewHandlerObject);
			auto OldObjectData = ObjHandlersArray.FindSorted(OldHandlerObject);
			AEON_ASSERT(OldObjectData);

			NewObjectData.HandlersArray = OldObjectData->HandlersArray;
			for (auto& HandlerData : NewObjectData.HandlersArray)
			{
				HandlerData.Handler.RebindEventObject_Internal(NewHandlerObject->HandlerThis());
			}
		}
#	endif
		virtual void CleanupHandler(HandlerAccess, SEventHandler* HandlerObject) override
		{
			// NOTE This is always called by an handler that is bound to this delegate and is being destroyed,
			//  so no need to either check presence or unregister the delegate

			auto Index = ObjHandlersArray.FindIndexSorted(HandlerObject);
			AEON_ASSERT(Index);
			ObjHandlersArray.RemoveAt(*Index);
		}

		// CHECKME Ensure that compiler omits this call and simply extracts the first argument
		template <class MaskArgType, class ... NonMaskArgTypes>
		AEON_FORCEINLINE static MaskArgType&& ExtractMaskArgument_Internal(MaskArgType&& Mask, NonMaskArgTypes&& ...)
		{
			return Forward<MaskArgType>(Mask);
		}

		GArray<FuncHandlerType> FuncHandlersArray;
		GArray<ObjHandlerType> ObjHandlersArray;
		bool CanBroadcast = true;
	};
}

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GGlobalEventDelegate

#define AEON_DECLARE_GLOBAL_EVENT_DELEGATE(Identifier, FuncSignature) \
    static_assert(::Aeon::VIsSimpleFunction<FuncSignature>, "FuncSignature must be a simple function signature with 'void' return type.");\
	using Identifier = ::Aeon::GGlobalEventDelegate<FuncSignature>

	template <CSimpleFunction FunctionType>
	class GGlobalEventDelegate;

	template <class ... ArgTypes>
	requires (... && CNotRvalueReference<ArgTypes>)
	class GGlobalEventDelegate<void(ArgTypes ...)> final
		: private ZzInternal::GEventDelegateStorage<void(ArgTypes ...), TEmptyTypeTag>
	{
		using Base = ZzInternal::GEventDelegateStorage<void(ArgTypes ...), TEmptyTypeTag>;

	public:
		GGlobalEventDelegate() = default;

		GGlobalEventDelegate(GGlobalEventDelegate const&) = delete;
		GGlobalEventDelegate(GGlobalEventDelegate&&) = delete;

		GGlobalEventDelegate& operator=(GGlobalEventDelegate const&) = delete;
		GGlobalEventDelegate& operator=(GGlobalEventDelegate&&) = delete;

		using Base::Bind;

		using Base::Unbind;

		using Base::UnbindAll;

		using Base::Broadcast;

	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GEventDelegate

#define AEON_DECLARE_EVENT_DELEGATE(Identifier, BroadcasterClass, FuncSignature) \
    static_assert(::Aeon::VIsSimpleFunction<FuncSignature>, "FuncSignature must be a simple function signature with 'void' return type.");\
	using Identifier = ::Aeon::GEventDelegate<BroadcasterClass, FuncSignature>

	template <CClass BroadcasterClassType, CSimpleFunction FunctionType>
	class GEventDelegate;

	template <CClass BroadcasterClassType, class ... ArgTypes>
	requires (... && CNotRvalueReference<ArgTypes>)
	class GEventDelegate<BroadcasterClassType, void(ArgTypes ...)> final
		: private ZzInternal::GEventDelegateStorage<void(ArgTypes ...), TEmptyTypeTag>
	{
		using Base = ZzInternal::GEventDelegateStorage<void(ArgTypes ...), TEmptyTypeTag>;

	public:
		AEON_DECLARE_ACCESS_CLASS(BroadcasterAccess, BroadcasterClassType);

		GEventDelegate() = default;

		GEventDelegate(GEventDelegate const&) = delete;
		GEventDelegate(GEventDelegate&&) = delete;

		GEventDelegate& operator=(GEventDelegate const&) = delete;
		GEventDelegate& operator=(GEventDelegate&&) = delete;

		using Base::Bind;

		using Base::Unbind;

		using Base::UnbindAll;

		AEON_FORCEINLINE void Broadcast(BroadcasterAccess, ArgTypes ... Args)
		{
			Base::Broadcast(Args ...);
		}
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GMaskingGlobalEventDelegate

#define AEON_DECLARE_MASKING_GLOBAL_EVENT_DELEGATE(Identifier, FuncSignature) \
    static_assert(::Aeon::VIsSimpleFunction<FuncSignature>, "FuncSignature must be a simple function signature with 'void' return type "\
															"and (at least) one parameter which must be CEqualityComparable.");\
	using Identifier = ::Aeon::GMaskingGlobalEventDelegate<FuncSignature>

	template <CSimpleFunction FunctionType>
	class GMaskingGlobalEventDelegate;

	template <class MaskType, class ... ArgTypes>
	requires CEqualityComparable<MaskType>
			 && (CNotRvalueReference<MaskType> && ... && CNotRvalueReference<ArgTypes>)
	class GMaskingGlobalEventDelegate<void(MaskType, ArgTypes ...)> final
		: private ZzInternal::GEventDelegateStorage<void(MaskType, ArgTypes ...), MaskType>
	{
		using Base = ZzInternal::GEventDelegateStorage<void(MaskType, ArgTypes ...), MaskType>;

	public:
		GMaskingGlobalEventDelegate() = default;

		GMaskingGlobalEventDelegate(GMaskingGlobalEventDelegate const&) = delete;
		GMaskingGlobalEventDelegate(GMaskingGlobalEventDelegate&&) = delete;

		GMaskingGlobalEventDelegate& operator=(GMaskingGlobalEventDelegate const&) = delete;
		GMaskingGlobalEventDelegate& operator=(GMaskingGlobalEventDelegate&&) = delete;

		using Base::Bind;

		using Base::BindWithMask;

		using Base::Unbind;

		using Base::UnbindAll;

		using Base::Broadcast;

	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GMaskingEventDelegate

#define AEON_DECLARE_MASKING_EVENT_DELEGATE(Identifier, BroadcasterClass, FuncSignature) \
    static_assert(::Aeon::VIsSimpleFunction<FuncSignature>, "FuncSignature must be a simple function signature with 'void' return type "\
															"and (at least) one parameter which must be CEqualityComparable.");\
	using Identifier = ::Aeon::GMaskingEventDelegate<BroadcasterClass, FuncSignature>

	template <CClass BroadcasterClassType, CSimpleFunction FunctionType>
	class GMaskingEventDelegate;

	template <CClass BroadcasterClassType, class MaskType, class ... ArgTypes>
	requires CEqualityComparable<MaskType>
			 && (CNotRvalueReference<MaskType> && ... && CNotRvalueReference<ArgTypes>)
	class GMaskingEventDelegate<BroadcasterClassType, void(MaskType, ArgTypes ...)> final
		: private ZzInternal::GEventDelegateStorage<void(MaskType, ArgTypes ...), MaskType>
	{
		using Base = ZzInternal::GEventDelegateStorage<void(MaskType, ArgTypes ...), MaskType>;

	public:
		AEON_DECLARE_ACCESS_CLASS(BroadcasterAccess, BroadcasterClassType);

		GMaskingEventDelegate() = default;

		GMaskingEventDelegate(GMaskingEventDelegate const&) = delete;
		GMaskingEventDelegate(GMaskingEventDelegate&&) = delete;

		GMaskingEventDelegate& operator=(GMaskingEventDelegate const&) = delete;
		GMaskingEventDelegate& operator=(GMaskingEventDelegate&&) = delete;

		using Base::Bind;

		using Base::BindWithMask;

		using Base::Unbind;

		using Base::UnbindAll;

		AEON_FORCEINLINE void Broadcast(BroadcasterAccess, MaskType Mask, ArgTypes ... Args)
		{
			Base::Broadcast(Mask, Args ...);
		}
	};

}

#endif