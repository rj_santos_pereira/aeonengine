#include "Aeon/Framework/Filesystem/File.hpp"

#include "Aeon/Framework/Filesystem/Path.hpp"

namespace Aeon
{
	SFile::SFile()
		: Stream{ nullptr }
		, DirEntry{}
		, WriteBuffer{ TReserveStorage::Tag, 0 }
		, CachedSize{ 0 }
		, CachedOffset{ 0 }
		, Mode{ EFileAccessMode::Invalid }
		, LastOpMode{ EFileAccessMode::Invalid }
		, HasPendingSeek{ false }
	{
	}
	
	SFile::SFile(TInternalAccess, SPath NewPath, EFileAccessMode NewMode, bool IsCreating)
		: Stream{ OpenStream_Internal(NewPath.AsNative(), NewMode, IsCreating) }
		, DirEntry{ NewPath.AsNative() }
		, WriteBuffer{ TReserveStorage::Tag, 0 }
		, CachedSize{ 0 }
		, CachedOffset{ 0 }
		, Mode{ Stream ? NewMode : EFileAccessMode::Invalid }
		, LastOpMode{ EFileAccessMode::Invalid }
		, HasPendingSeek{ false }
	{
		SetupSizeAndOffset_Internal();
	}

	SFile::SFile(TInternalAccess, stdfs::directory_entry&& NewDirEntry)
		: Stream{ nullptr }
		, DirEntry{ Move(NewDirEntry) }
		, WriteBuffer{ TReserveStorage::Tag, 0 }
		, CachedSize{ 0 }
		, CachedOffset{ 0 }
		, Mode{ EFileAccessMode::Invalid }
		, LastOpMode{ EFileAccessMode::Invalid }
		, HasPendingSeek{ false }
	{
	}

	SFile::SFile(SFile&& Other)
		: Stream{ Exchange(Other.Stream, nullptr) }
		, DirEntry{ Move(Other.DirEntry) }
		, WriteBuffer{ Move(Other.WriteBuffer) }
		, CachedSize{ Other.CachedSize }
		, CachedOffset{ Other.CachedOffset }
		, Mode{ EFileAccessMode::Invalid }
		, LastOpMode{ EFileAccessMode::Invalid }
		, HasPendingSeek{ false }
	{
		AEON_SWAP_BITFIELD(Mode, Other.Mode);
		AEON_SWAP_BITFIELD(LastOpMode, Other.LastOpMode);
		AEON_SWAP_BITFIELD(HasPendingSeek, Other.HasPendingSeek);
	}

	SFile& SFile::operator=(SFile Other)
	{
		using Aeon::Swap;
		Swap(Stream, Other.Stream);
		Swap(DirEntry, Other.DirEntry);
		Swap(WriteBuffer, Other.WriteBuffer);
		Swap(CachedSize, Other.CachedSize);
		Swap(CachedOffset, Other.CachedOffset);
		AEON_SWAP_BITFIELD(Mode, Other.Mode);
		AEON_SWAP_BITFIELD(LastOpMode, Other.LastOpMode);
		AEON_SWAP_BITFIELD(HasPendingSeek, Other.HasPendingSeek);
		return *this;
	}

	SFile::~SFile()
	{
		Close();
	}

	SFile SFile::CreateAt(SPath const& Path, EFileAccessMode AccessMode)
	{
		[[maybe_unused]] std::error_code ErrCode;

		AEON_FAILSAFE(Aeon::HasAnyFlags(AccessMode, EFileAccessMode::Write), return SFile{},
					  "Requires AccessMode to have a EFileAccessMode::Write flag when creating a file!");

		// If the path points to an existing file, just create the file object.
		if (Path.IsFile())
		{
			return SFile{ TInternalAccess::Tag, Path, AccessMode, false };
		}

		// Otherwise, ensure that no entry with this name exists, and the parent path points to an existing directory.
		if (!Path.Exists())
		{
			SPath DirPath = Path.ParentPath();

			if (DirPath.IsDirectory())
			{
				return SFile{ TInternalAccess::Tag, Move(Path), AccessMode, true };
			}
		}

		return SFile{};
	}

	SFile SFile::OpenAt(SPath const& Path, EFileAccessMode AccessMode)
	{
		return SFile{ TInternalAccess::Tag, Path, AccessMode, false };
	}

	SFile SFile::At(SPath const& Path)
	{
		[[maybe_unused]] std::error_code ErrCode;
		stdfs::directory_entry NewDirEntry{ Path.AsNative(), ErrCode };

		return SFile{ TInternalAccess::Tag, NewDirEntry.is_regular_file(ErrCode) ? Move(NewDirEntry) : stdfs::directory_entry{} };
	}

	bool SFile::Delete()
	{
		AEON_ASSERT(IsValid());

		Close();

		std::error_code ErrCode;
		// Since we are in the context of a file object,
		// we already know the path refers to a file, no need to check it beforehand.
		stdfs::remove(DirEntry.path(), ErrCode);

		Reset();

		return (!ErrCode);
	}

	void SFile::Reset()
	{
		Close();

		[[maybe_unused]] std::error_code ErrCode;
		// Invalidate the entry
		DirEntry.assign(stdfs::path{}, ErrCode);
	}

	bool SFile::CopyTo(SPath const& NewPath, EFileCopyOptions Options)
	{
		if (NewPath.IsFile())
		{
			std::error_code ErrCode;
			stdfs::copy_file(DirEntry.path(), NewPath.AsNative(), stdfs::copy_options(Options), ErrCode);
			return (!ErrCode);
		}
		return false;
	}

	bool SFile::MoveTo(SPath const& NewPath)
	{
		// NOTE stdfs::rename has semantics for an existing directory on some platforms, which typically involves deleting that directory.
		//  We'd rather disallow this completely.
		if (!NewPath.Exists())
		{
			std::error_code ErrCode;
			stdfs::rename(DirEntry.path(), NewPath.AsNative(), ErrCode);
			return (!ErrCode);
		}
		return false;
	}

	bool SFile::Open(EFileAccessMode NewMode)
	{
		Close();

		Stream = OpenStream_Internal(DirEntry.path(), NewMode, false);
		Mode = NewMode;

		SetupSizeAndOffset_Internal();

		return IsOpen();
	}

	void SFile::Close()
	{
		if (IsOpen())
		{
		//	std::fflush(FileStream);
			std::fclose(Stream);

			Stream = nullptr;
			Mode = EFileAccessMode::Invalid;

			RefreshStats();
		}
	}

	bool SFile::IsOpen() const
	{
		return Stream;
	}

	EFileAccessMode SFile::AccessMode() const
	{
		return Mode;
	}

	bool SFile::ReadBytes(voidptr BufferPtr, szint ByteCount)
	{
		// NOTE Technically, Write supports reading, but we disallow it here for consistency with AccessMode semantics.
		AEON_ASSERT(Aeon::HasAnyFlags(Mode, EFileAccessMode::Read));

		return ReadBytes_Internal(BufferPtr, ByteCount);
	}

	bool SFile::WriteBytes(cvoidptr BufferPtr, szint ByteCount, EFileWriteMode WriteMode)
	{
		AEON_ASSERT(Aeon::HasAnyFlags(Mode, EFileAccessMode::Write));

		bool ShouldWrite = true;

		if (WriteMode == EFileWriteMode::Insert)
		{
			ShouldWrite = RelocateBytes_Internal(ByteCount);
		}

		return ShouldWrite && WriteBytes_Internal(BufferPtr, ByteCount);
	}

	bool SFile::EraseBytes(szint ByteCount)
	{
		// Don't erase more than the number of bytes in the file (counting from offset),
		// negate the argument so that RelocateBytes_Internal knows the move direction.
		diffint BytesToErase = -AeonMath::Min<diffint>(CachedSize - CachedOffset, ByteCount);
		return RelocateBytes_Internal(BytesToErase);
	}

	szint SFile::GetByteOffset() const
	{
		return CachedOffset;
	}

	void SFile::SetByteOffset(szint NewOffset)
	{
		AEON_ASSERT(Mode != EFileAccessMode::AppendWrite);

		// CHECKME Ensure SetByteOffset can seek beyond the end of the file in every platform,
		// since RelocateBytes_Internal depends on this behavior.
		// - Windows: supported

		// Just update the cached offset, and mark the seek as pending
		CachedOffset = NewOffset;
		HasPendingSeek = true;
	}

	szint SFile::BufferSizeInBytes() const
	{
		return CachedSize;
	}

	SPlatformString SFile::GetName() const
	{
		// NOTE filename() creates a new path, so we must persist the string memory

		// FIXME once we add const funcs to StringView,
		//  we can return a view into the substring with path(), as it returns a const lval-ref (instead of filename())
		//  and index of last stdfs::preferred_separator as args;
		//  #include "String.hpp" can be removed from FilesystemCommon afterwards
		return SPlatformString{ DirEntry.path().filename().c_str() };
	}

	bool SFile::SetName(SPlatformStringView NewName)
	{
		stdfs::path OldPath = DirEntry.path();
		stdfs::path NewPath{ std::basic_string_view<stdfs::path::value_type>(NewName) };

		AEON_FAILSAFE(!NewPath.filename().empty() && NewPath.parent_path().empty(), return false,
					  "NewName must be a name, and not a path!");

		NewPath = OldPath.replace_filename(NewPath);

		return Rename_Internal(OldPath, NewPath);
	}

	SPlatformString SFile::GetExtension() const
	{
		// FIXME See SFile::GetName
		return SPlatformString{ DirEntry.path().extension().c_str() };
	}

	bool SFile::SetExtension(SPlatformStringView NewExtension)
	{
		stdfs::path OldPath = DirEntry.path();
		stdfs::path NewPath{ std::basic_string_view<stdfs::path::value_type>(NewExtension) };

		AEON_FAILSAFE(!NewPath.filename().empty() && NewPath.parent_path().empty(), return false,
					  "NewName must be a name, and not a path!");

		NewPath = OldPath.replace_extension(NewPath);

		return Rename_Internal(OldPath, NewPath);
	}

	EFilesystemPermissions SFile::GetPermissions() const
	{
		return EFilesystemPermissions(int32(DirEntry.status().permissions()));
	}

	bool SFile::SetPermissions(EFilesystemPermissions NewPermissions)
	{
		[[maybe_unused]] std::error_code ErrCode;
		stdfs::permissions(DirEntry.path(), stdfs::perms(NewPermissions),
						   stdfs::perm_options::replace | stdfs::perm_options::nofollow,
						   ErrCode);

		if (!ErrCode)
		{
			RefreshStats();
		}

		return (!ErrCode);
	}

	void SFile::RefreshStats() const
	{
		[[maybe_unused]] std::error_code ErrCode;
		DirEntry.refresh(ErrCode);
	}

	STimePoint SFile::LastModifyTime(ESpecificTimezone SpecificTimezone) const
	{
		[[maybe_unused]] std::error_code ErrCode;

		auto FileTimePoint = DirEntry.last_write_time(ErrCode);
		auto SysTimePoint = stdtm::clock_cast<stdtm::sys_clock>(FileTimePoint);

		auto Timezone = STimezoneDatabase::Access().FindTimezone(SpecificTimezone);
		return STimePoint{ SysTimePoint, Timezone };
	}

	szint SFile::SizeInBytes() const
	{
		[[maybe_unused]] std::error_code ErrCode;
		return DirEntry.file_size(ErrCode);
	}

	SPath SFile::Path() const
	{
		return SPath{ SPath::TInternalAccess::Tag, stdfs::path(DirEntry.path()) };
	}

	bool SFile::IsValid() const
	{
		return !DirEntry.path().empty();
	}

	SFileOpenContext SFile::OpenForScope(EFileAccessMode AccessMode)
	{
		return SFileOpenContext(SFileOpenContext::TInternalAccess::Tag, *this, AccessMode);
	}

	bool SFile::Rename_Internal(stdfs::path const& OldPath, stdfs::path const& NewPath)
	{
		std::error_code ErrCode;
		if (stdfs::exists(NewPath, ErrCode))
		{
			return false;
		}

		ErrCode.clear();
		stdfs::rename(OldPath, NewPath, ErrCode);

		if (!ErrCode)
		{
			RefreshStats();
		}

		return (!ErrCode);
	}

	void SFile::SetupSizeAndOffset_Internal()
	{
		if (IsOpen())
		{
			// HACK
			// > Binary streams are not required to support SEEK_END, in particular if additional null bytes are output.
			// But most/all platforms support this, at least on regular files.
			SeekOffset_Internal(Stream, 0, SEEK_END);
			CachedSize = QueryOffset_Internal(Stream);
			CachedOffset = 0;
			//std::rewind(Stream);
			SeekOffset_Internal(Stream, 0, SEEK_SET);

			LastOpMode = EFileAccessMode::Read;
			HasPendingSeek = false;
		}
	}

	bool SFile::RelocateBytes_Internal(diffint ByteDiff)
	{
		// NOTE All Write flags except AppendWrite have read semantics, which are necessary for this to work.
		//  Note that appending to the file, by definition, should never need to invoke this.
		AEON_ASSERT(Aeon::HasAnyFlags(Mode, EFileAccessMode::Write) && Mode != EFileAccessMode::AppendWrite);

		if (ByteDiff == 0)
		{
			return true;
		}

		// CHECKME Test with this disabled, remove it if unnecessary
	//	if (HasPendingSeek)
	//	{
	//		SeekOffset_Internal(Stream, CachedOffset, SEEK_SET);
	//		HasPendingSeek = false;
	//	}

		szint BaseOffset = CachedOffset;
		bool Success = true;

		// Called by WriteBytes with WriteMode == Insert
		if (ByteDiff > 0)
		{
			// TODO maybe improve this code path to only need a single fwrite call (copy bytes to insert to the start of WriteBuffer)

			// Compute the number of bytes we need to move to the new offset
			szint BytesToMoveCount = CachedSize - CachedOffset;
			WriteBuffer.Resize(BytesToMoveCount);

			// Now read the bytes to move to the temporary buffer
			Success = ReadBytes_Internal(WriteBuffer.Buffer(), WriteBuffer.Count());
			if (!Success)
			{
				goto FinishOffset;
			}

			// Update the offset to the first byte we want to move forward
			// NOTE This may need to seek beyond the end-of-file
			szint BytesToMoveOffset = BaseOffset + ByteDiff;
			SetByteOffset(BytesToMoveOffset);

			// Now write the bytes to the file again
			Success = WriteBytes_Internal(WriteBuffer.Buffer(), WriteBuffer.Count());
			if (!Success)
			{
				goto FinishOffset;
			}
		}
		// Called by EraseBytes
		else
		{
			// Update the offset to the first byte we want to move back
			szint BytesToMoveOffset = BaseOffset - ByteDiff;
			SetByteOffset(BytesToMoveOffset);

			// Compute the number of bytes we need to move using the new offset
			szint BytesToMoveCount = CachedSize - CachedOffset;
			WriteBuffer.Resize(BytesToMoveCount);

			// Now read the bytes to move to the temporary buffer
			Success = ReadBytes_Internal(WriteBuffer.Buffer(), WriteBuffer.Count());
			if (!Success)
			{
				goto FinishOffset;
			}

			// Move the offset back and write the bytes to the file again
			SetByteOffset(BaseOffset);
			Success = WriteBytes_Internal(WriteBuffer.Buffer(), WriteBuffer.Count());
			if (!Success)
			{
				goto FinishOffset;
			}

			// Lastly, compute the new size and resize the file using the filesystem
			CachedSize += ByteDiff;

			std::error_code ErrCode;
			stdfs::resize_file(DirEntry.path(), CachedSize, ErrCode);

			Success = (!ErrCode);
		}

	FinishOffset:
		// Restore the byte offset and empty the temporary buffer
		SetByteOffset(BaseOffset);
		WriteBuffer.Empty();

		return Success;
	}

	bool SFile::ReadBytes_Internal(voidptr BufferPtr, szint ByteCount)
	{
		if (ByteCount == 0)
		{
			return true;
		}

		if (LastOpMode != EFileAccessMode::Read)
		{
			std::fflush(Stream);
		//	std::rewind(Stream);
		//	HasPendingSeek = true;
		}

		if (HasPendingSeek)
		{
			SeekOffset_Internal(Stream, CachedOffset, SEEK_SET);
			HasPendingSeek = false;
		}

		// If no more bytes to read, just return false
		szint BytesUnread = CachedSize - CachedOffset;
		if (BytesUnread == 0)
		{
			return false;
		}

		szint BytesToRead = AeonMath::Min(ByteCount, BytesUnread);

		LastOpMode = EFileAccessMode::Read;

		szint BytesRead = std::fread(BufferPtr, sizeof(byte), BytesToRead, Stream);
		if (BytesRead == 0)
		{
			// Restore old offset
			SeekOffset_Internal(Stream, CachedOffset, SEEK_SET);
			return false;
		}

		// Update the cached offset
		CachedOffset += BytesRead;

		return true;
	}

	bool SFile::WriteBytes_Internal(cvoidptr BufferPtr, szint ByteCount)
	{
		if (ByteCount == 0)
		{
			return true;
		}

		if (LastOpMode != EFileAccessMode::Write)
		{
			// NOTE
			// > When a file is opened with update mode ('+' as the second or third character in the [...] list of mode argument values)
			// > [...] input [read] shall not be directly followed by output [write] without an intervening call to a file positioning function,
			// > unless the input operation encounters end- of-file.
		//	std::rewind(Stream);
			HasPendingSeek = true;
		}

		if (HasPendingSeek)
		{
			SeekOffset_Internal(Stream, CachedOffset, SEEK_SET);
			HasPendingSeek = false;
		}

		LastOpMode = EFileAccessMode::Write;

		szint BytesToWrite = ByteCount;
		szint BytesWritten = std::fwrite(BufferPtr, sizeof(byte), BytesToWrite, Stream);

		if (BytesWritten == 0)
		{
			// Restore old offset
			SeekOffset_Internal(Stream, CachedOffset, SEEK_SET);
			return false;
		}

		CachedOffset += BytesWritten;
		CachedSize = AeonMath::Max(CachedOffset, CachedSize);

		return true;
	}

	bool SFile::SeekOffset_Internal(std::FILE* Stream, szint NewOffset, int OriginFlag)
	{
		static auto const SeekOffsetFunc =
#	if AEON_PLATFORM_WINDOWS
			&::_fseeki64_nolock
#	else
			&::std::fseek
#	endif
		;

		return bool(SeekOffsetFunc(Stream, NewOffset, OriginFlag) == 0);
	}

	szint SFile::QueryOffset_Internal(std::FILE* Stream)
	{
		static auto const QueryOffsetFunc =
#	if AEON_PLATFORM_WINDOWS
			&::_ftelli64_nolock
#	else
			&::std::ftell
#	endif
		;

		return szint(QueryOffsetFunc(Stream));
	}

	std::FILE* SFile::OpenStream_Internal(stdfs::path const& Path, EFileAccessMode AccessMode, bool IsCreating)
	{
		static auto const OpenStreamFunc =
#	if AEON_PLATFORM_WINDOWS
			&::_wfopen
#	else
			&::std::fopen
#	endif
		;

		auto NewStream = OpenStreamFunc(Path.c_str(), MakeFileAccessFlags_Internal(AccessMode, IsCreating));
		if (NewStream)
		{
			uint32 BufferSize = BUFSIZ;
			// CHECKME Links with info related to file buffering on different platforms:
			//  	https://learn.microsoft.com/en-us/windows/win32/fileio/file-buffering?redirectedfrom=MSDN
			//  	https://learn.microsoft.com/en-us/windows/win32/devio/calling-deviceiocontrol?redirectedfrom=MSDN
			//  	https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/sys_stat.h.html
			//  On Windows, see DISK_GEOMETRY and BytesPerSector; also see FILE_STORAGE_INFO and PhysicalBytesPerSectorForPerformance
			//  On POSIX, see fstat() and st_blksize
#		if AEON_PLATFORM_WINDOWS
			int FileDescriptor = _fileno(NewStream);
			auto FileHandle = bit_cast<HANDLE>(_get_osfhandle(FileDescriptor));

			FILE_STORAGE_INFO StorageInfo;
			if (GetFileInformationByHandleEx(FileHandle, FILE_INFO_BY_HANDLE_CLASS::FileStorageInfo, addressof(StorageInfo), sizeof StorageInfo))
			{
				BufferSize = StorageInfo.PhysicalBytesPerSectorForPerformance;
			}
#		else
			// TODO other platforms
			AEON_COMPILER_WARNING("TODO implement stream buffer resize on SFile::OpenStream_Internal for this platform");
#		endif

			std::setvbuf(NewStream, nullptr, _IOFBF, BufferSize);
		}
		return NewStream;
	}

	stdfs::path::value_type const* SFile::MakeFileAccessFlags_Internal(EFileAccessMode AccessMode, bool IsCreating)
	{
		// NOTE See https://en.cppreference.com/w/cpp/io/c/fopen for possible values

		// NOTE We use 'w+' instead of 'w' to allow EraseBytes to work when AccessMode either Write or DiscardWrite

		// NOTE 'b' must be a suffix, according to Windows documentation:
		// https://learn.microsoft.com/en-us/cpp/c-runtime-library/reference/fopen-wfopen?view=msvc-170
		constexpr stdfs::path::value_type const* FileAccessFlags[10] = {
			PATHSTR("ab"), 			// Creating/AppendWrite - Open/create for write, append to end

			PATHSTR("r+b"), 		// Existing/Write - Open for write (also supports read due to fopen semantics)
			PATHSTR("rb"), 			// Existing/Read - Open for read
			PATHSTR("r+b"), 		// Existing/ReadWrite - Open for read/write

			PATHSTR("w+b"), 		// Creating/DiscardWrite - Open/create for write, discard contents

			PATHSTR("w+b"), 		// Existing/DiscardWrite - Open/create for write, discard contents

			PATHSTR("w+bx"), 		// Creating/ReadWrite - Create for read/write
			nullptr, 				// Creating/Read - Invalid when creating (should never happen)
			PATHSTR("w+bx"), 		// Creating/Write - Create for write (also supports read due to fopen semantics)

			PATHSTR("ab"), 			// Existing/AppendWrite - Open/create for write, append to end
		};

		return FileAccessFlags[IsCreating ? (9 - AccessMode) : AccessMode];
	}

	SFileOpenContext::SFileOpenContext(SFileOpenContext::TInternalAccess, SFile& NewFileRef, EFileAccessMode AccessMode)
		: FileRef(NewFileRef)
	{
		FileRef.Open(AccessMode);
	}

	SFileOpenContext::~SFileOpenContext()
	{
		FileRef.Close();
	}

	SFileOpenContext::operator bool() const
	{
		return FileRef.IsOpen();
	}
}
