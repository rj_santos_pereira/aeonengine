#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_FILESYSTEM_FILESYSTEMCOMMON
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_FILESYSTEM_FILESYSTEMCOMMON

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Container/Iterator.hpp"
#include "Aeon/Framework/Functional/Optional.hpp"

#include "Aeon/Framework/Text/StringView.hpp"
#include "Aeon/Framework/Text/String.hpp"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wshadow"

namespace Aeon
{
	class SPath;
	class SDirectory;
	class SFile;

	AEON_DECLARE_FLAG_ENUM(EFilesystemPermissions, uint32)
	(
		AEON_FLAG_ENUM_RULE_AS_IS,

		None 							= int32(stdfs::perms::none),

		OwnerRead 						= int32(stdfs::perms::owner_read),
		OwnerWrite 						= int32(stdfs::perms::owner_write),
		OwnerExec 						= int32(stdfs::perms::owner_exec),
		OwnerAll 						= int32(stdfs::perms::owner_all),

		GroupRead 						= int32(stdfs::perms::group_read),
		GroupWrite 						= int32(stdfs::perms::group_write),
		GroupExec 						= int32(stdfs::perms::group_exec),
		GroupAll 						= int32(stdfs::perms::group_all),

		OthersRead 						= int32(stdfs::perms::others_read),
		OthersWrite 					= int32(stdfs::perms::others_write),
		OthersExec 						= int32(stdfs::perms::others_exec),
		OthersAll 						= int32(stdfs::perms::others_all),

		All 							= int32(stdfs::perms::all),

		// POSIX-only
		RemovableByOwnerOnly			= int32(stdfs::perms::sticky_bit),
	);

	template <class EntryType>
	class GDirectoryIterator final : public IIterator<EntryType, std::forward_iterator_tag>
	{
		// HACK We derive from IIterator instead of IForwardAccessIterator because we need operator* to return a value type,
		//  instead of a reference type, so that we can construct SDirectoryEntry objects on the fly.
		using IteratorTraits = IIterator<EntryType, std::forward_iterator_tag>;

	public:
		AEON_DECLARE_ITERATOR_TRAITS(GDirectoryIterator);

		GDirectoryIterator() = default;
		explicit GDirectoryIterator(stdfs::path const& Path, [[maybe_unused]] std::error_code ErrCode = std::error_code{});

		AEON_MAKE_COPYABLE(GDirectoryIterator);
		AEON_MAKE_MOVEABLE(GDirectoryIterator);

		EntryType operator*() const;

		GDirectoryIterator& operator++();

		bool operator==(GDirectoryIterator const& Other) const;
		strong_ordering operator<=>(GDirectoryIterator const& Other) const = delete;

	private:
		stdfs::directory_iterator Iterator;

	};

	class SDirectoryEntry
	{
	public:
		AEON_DECLARE_ACCESS_CLASS(TInternalAccess,
								  class GDirectoryIterator<SDirectoryEntry>,
								  class GDirectoryIterator<SDirectoryEntry const>);

		explicit SDirectoryEntry(TInternalAccess, stdfs::directory_entry const& NewEntry);

		GOptional<SDirectory> AsDirectory() const;
		GOptional<SFile> AsFile() const;
	//	GOptional<SFile> AsSymlink() const;

		SPath AsPath() const;

		bool IsDirectory() const;
		bool IsFile() const;
	//	bool IsSymlink() const;

	private:
		stdfs::directory_entry Entry;

	};
}

#pragma clang diagnostic pop

#endif