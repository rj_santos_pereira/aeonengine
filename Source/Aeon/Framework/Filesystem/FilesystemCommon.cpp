#include "Aeon/Framework/Filesystem/FilesystemCommon.hpp"

#include "Aeon/Framework/Filesystem/Path.hpp"
#include "Aeon/Framework/Filesystem/Directory.hpp"
#include "Aeon/Framework/Filesystem/File.hpp"

namespace Aeon
{
	template <class EntryType>
	GDirectoryIterator<EntryType>::GDirectoryIterator(stdfs::path const& Path, std::error_code ErrCode)
		: Iterator{ Path, stdfs::directory_options::follow_directory_symlink, ErrCode }
	{
	}

	template <class EntryType>
	EntryType GDirectoryIterator<EntryType>::operator*() const
	{
		// FIXME Propagate const-ness somehow
		AEON_ASSERT(Iterator != stdfs::directory_iterator{});
		return SDirectoryEntry{ SDirectoryEntry::TInternalAccess::Tag, *Iterator };
	}

	template <class EntryType>
	GDirectoryIterator<EntryType>& GDirectoryIterator<EntryType>::operator++()
	{
		AEON_ASSERT(Iterator != stdfs::directory_iterator{});
		++Iterator;
		return *this;
	}

	template <class EntryType>
	bool GDirectoryIterator<EntryType>::operator==(GDirectoryIterator const& Other) const
	{
		return Iterator == Other.Iterator;
	}

	// Instantiate the only specializations we want from GDirectoryIterator
	template class GDirectoryIterator<SDirectoryEntry>;
	template class GDirectoryIterator<SDirectoryEntry const>;

	SDirectoryEntry::SDirectoryEntry(SDirectoryEntry::TInternalAccess, stdfs::directory_entry const& NewEntry)
		: Entry{ NewEntry }
	{
	}

	GOptional<SDirectory> SDirectoryEntry::AsDirectory() const
	{
		return IsDirectory()
			   ? GOptional<SDirectory>{ TConstructInPlace::Tag, SDirectory::TInternalAccess::Tag, stdfs::directory_entry(Entry) }
			   : GOptional<SDirectory>{};
	}

	GOptional<SFile> SDirectoryEntry::AsFile() const
	{
		return IsFile()
			   ? GOptional<SFile>{ TConstructInPlace::Tag, SFile::TInternalAccess::Tag, stdfs::directory_entry(Entry) }
			   : GOptional<SFile>{};
	}

	SPath SDirectoryEntry::AsPath() const
	{
		return SPath{ SPath::TInternalAccess::Tag, stdfs::path(Entry.path()) };
	}

	// CHECKME Handle symlinks/hardlinks, treat them as the thing they point to (stdfs::read_symlink obtains the path to the target)

	bool SDirectoryEntry::IsDirectory() const
	{
		[[maybe_unused]] std::error_code ErrCode;
		return Entry.is_directory(ErrCode);
	}

	bool SDirectoryEntry::IsFile() const
	{
		[[maybe_unused]] std::error_code ErrCode;
		return Entry.is_regular_file(ErrCode);
	}

}
