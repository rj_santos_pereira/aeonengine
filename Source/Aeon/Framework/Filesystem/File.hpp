#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_FILESYSTEM_FILE
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_FILESYSTEM_FILE

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Filesystem/FilesystemCommon.hpp"

#include "Aeon/Framework/Time/TimezoneDatabase.hpp"
#include "Aeon/Framework/Time/TimePoint.hpp"

namespace Aeon
{
	AEON_DECLARE_FLAG_ENUM(EFileAccessMode, uint8)
	(
		AEON_FLAG_ENUM_RULE_AS_IS,

		Invalid 							= 0b0000,

		// Open for reading; requires the file to exist
		Read 								= 0b0010, // std::ios::in
		// Open for writing
		Write 								= 0b0001, // std::ios::out
		// Open for reading/writing
		ReadWrite 							= 0b0011, // std::ios::in | std::ios::out
		// Open for writing; discard the contents of the file when opening
		DiscardWrite 						= 0b0101, // std::ios::out | std::ios::trunc
		// Open for writing; move position indicator to the end of the file when opening
		AppendWrite 						= 0b1001, // std::ios::out | std::ios::app
	);

	enum struct EFileWriteMode 				: uint8
	{
		// Overwrite existing characters at position indicator
		Default 							= 0b0,
		// Insert new characters, pushing existing characters forward, at position indicator
		Insert 								= 0b1,
	};

	AEON_DECLARE_FLAG_ENUM(EFileCopyOptions, uint32)
	(
		AEON_FLAG_ENUM_RULE_AS_IS,

		// Keep the existing file and report an error.
		ErrorOnExisting = int32(stdfs::copy_options::none),
		// Keep the existing file, without reporting an error.
		KeepExisting = int32(stdfs::copy_options::skip_existing),
		// Replace the existing file.
		OverwriteExisting = int32(stdfs::copy_options::overwrite_existing),
		// Replace the existing file only if it is older than the file being copied.
		OverwriteOutdatedExisting = int32(stdfs::copy_options::update_existing),
	);

	class SFileOpenContext;

	class SFile
	{
	public:
		AEON_DECLARE_ACCESS_CLASS(TInternalAccess,
								  class SFile,
								  class SDirectoryEntry);

		SFile();

		explicit SFile(TInternalAccess, SPath NewEntry, EFileAccessMode AccessMode, bool IsCreating);
		explicit SFile(TInternalAccess, stdfs::directory_entry&& NewEntry);

		SFile(SFile const& Other) = delete;
		SFile(SFile&& Other);

		SFile& operator=(SFile Other);

		~SFile();

		// NOTE Create automatically opens the file, even when it already exists.
		static SFile CreateAt(SPath const& Path, EFileAccessMode AccessMode);

		static SFile OpenAt(SPath const& Path, EFileAccessMode AccessMode);

		static SFile At(SPath const& Path);

		bool Delete();

		void Reset();

		bool CopyTo(SPath const& NewPath, EFileCopyOptions Options);

		bool MoveTo(SPath const& NewPath);

		bool Open(EFileAccessMode AccessMode);

		void Close();

		bool IsOpen() const;

		EFileAccessMode AccessMode() const;

		bool ReadBytes(voidptr BufferPtr, szint ByteCount);

		bool WriteBytes(cvoidptr BufferPtr, szint ByteCount, EFileWriteMode WriteMode = EFileWriteMode::Default);

		bool EraseBytes(szint ByteCount);

		szint GetByteOffset() const;

		void SetByteOffset(szint NewOffset);

		szint BufferSizeInBytes() const;

		SPlatformString GetName() const;

		bool SetName(SPlatformStringView NewName);

		SPlatformString GetExtension() const;

		bool SetExtension(SPlatformStringView NewName);

		EFilesystemPermissions GetPermissions() const;

		bool SetPermissions(EFilesystemPermissions NewPermissions);

		// CHECKME Should this really be const? (also see SDirectory)
		void RefreshStats() const;

		STimePoint LastModifyTime(ESpecificTimezone SpecificTimezone) const;

		szint SizeInBytes() const;

		SPath Path() const;

		bool IsValid() const;

		SFileOpenContext OpenForScope(EFileAccessMode AccessMode);

	private:
		bool Rename_Internal(stdfs::path const& OldPath, stdfs::path const& NewPath);

		void SetupSizeAndOffset_Internal();

		bool RelocateBytes_Internal(diffint ByteDiff);
		bool ReadBytes_Internal(voidptr BufferPtr, szint ByteCount);
		bool WriteBytes_Internal(cvoidptr BufferPtr, szint ByteCount);

		static bool SeekOffset_Internal(std::FILE* Stream, szint NewOffset, int OriginFlag);
		static szint QueryOffset_Internal(std::FILE* Stream);

		static std::FILE* OpenStream_Internal(stdfs::path const& Path, EFileAccessMode AccessMode, bool IsCreating);
		static stdfs::path::value_type const* MakeFileAccessFlags_Internal(EFileAccessMode AccessMode, bool IsCreating);

		std::FILE* Stream;
		mutable stdfs::directory_entry DirEntry;
		GArray<byte, GDynamicAllocator<byte, uint64>> WriteBuffer; // TODO obtain this from a global table
		szint CachedSize;
		szint CachedOffset;
		EFileAccessMode Mode : 4;
		EFileAccessMode LastOpMode : 2;
		bool HasPendingSeek : 1;
	};

	class SFileOpenContext
	{
	public:
		AEON_DECLARE_ACCESS_CLASS(TInternalAccess, class SFile);

		SFileOpenContext(TInternalAccess, SFile& NewFileRef, EFileAccessMode AccessMode);
		~SFileOpenContext();

		AEON_MAKE_NON_COPYABLE(SFileOpenContext);
		AEON_MAKE_NON_MOVEABLE(SFileOpenContext);

		explicit operator bool() const;

	private:
		SFile& FileRef;
	};
}

#endif