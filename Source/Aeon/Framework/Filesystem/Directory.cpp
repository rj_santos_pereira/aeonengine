#include "Aeon/Framework/Filesystem/Directory.hpp"

#include "Aeon/Framework/Filesystem/Path.hpp"
#include "Aeon/Framework/Filesystem/File.hpp"

namespace Aeon
{
	SDirectory::SDirectory(TInternalAccess, stdfs::directory_entry&& NewDirEntry)
		: DirEntry{ Move(NewDirEntry) }
	{
	}

	SDirectory SDirectory::Create(SPath const& Path)
	{
		[[maybe_unused]] std::error_code ErrCode;

		// If the path points to an existing directory, just create the directory object.
		if (Path.IsDirectory())
		{
			return SDirectory{ TInternalAccess::Tag, stdfs::directory_entry{ Path.AsNative(), ErrCode } };
		}

		// Otherwise, ensure that some base of the path points to an existing directory.

		// NOTE We could unconditionally call create_directories,
		//  but that would require inspecting std::error_code, which is platform-dependent,
		//  to determine whether the directory already existed.
		if (!Path.Exists())
		{
			// Traverse the directory tree towards the root.
			SPath BasePath = Path.ParentPath();
			while (!BasePath.Exists())
			{
				BasePath = BasePath.ParentPath();
			}

			// Once we find an existing path, ensure it is pointing to a directory, and create the directories.
			// Some reasons for failure to create the directories at this point are e.g. that either BasePath is not a directory (e.g. is a file),
			// or that the process owner does not have permission to create the new directories under BasePath.
			if (BasePath.IsDirectory() && stdfs::create_directories(Path.AsNative(), ErrCode))
			{
				return SDirectory{ TInternalAccess::Tag, stdfs::directory_entry{ Path.AsNative(), ErrCode } };
			}
		}

		return SDirectory{};
	}

	SDirectory SDirectory::At(SPath const& Path)
	{
		[[maybe_unused]] std::error_code ErrCode;
		stdfs::directory_entry NewDirEntry{ Path.AsNative(), ErrCode };

		return SDirectory{ TInternalAccess::Tag, NewDirEntry.is_directory(ErrCode) ? Move(NewDirEntry) : stdfs::directory_entry{} };
	}

	SDirectory SDirectory::Parent() const
	{
		SPath ParentPath = Path().ParentPath();

		[[maybe_unused]] std::error_code ErrCode;
		stdfs::directory_entry NewDirEntry{ ParentPath.AsNative(), ErrCode };

		return SDirectory{ TInternalAccess::Tag, NewDirEntry.is_directory(ErrCode) ? Move(NewDirEntry) : stdfs::directory_entry{} };
	}

	SDirectory SDirectory::Child(SPath const& NewDirectory) const
	{
		SPath ChildPath = NewDirectory.IsRelative() ? Path() / NewDirectory : SPath{};

		[[maybe_unused]] std::error_code ErrCode;
		stdfs::directory_entry NewDirEntry{ ChildPath.AsNative(), ErrCode };

		return SDirectory{ TInternalAccess::Tag, NewDirEntry.is_directory(ErrCode) ? Move(NewDirEntry) : stdfs::directory_entry{} };
	}

	bool SDirectory::Delete(EDirectoryDeleteEmptyOnly DeleteEmptyOnly)
	{
		AEON_ASSERT(IsValid());

		// Since we are in the context of a directory object,
		// we already know the path refers to a directory, no need to check it beforehand.

		std::error_code ErrCode;
		if (DeleteEmptyOnly)
		{
			stdfs::remove(DirEntry.path(), ErrCode);
		}
		else
		{
			stdfs::remove_all(DirEntry.path(), ErrCode);
		}

		Reset();

		return (!ErrCode);
	}

	void SDirectory::Reset()
	{
		[[maybe_unused]] std::error_code ErrCode;
		// Invalidate the entry
		DirEntry.assign(stdfs::path{}, ErrCode);
	}

	bool SDirectory::CopyTo(SPath const& NewPath, EDirectoryCopyOptions Options)
	{
		// TODO make an option to copy only contents of directory (and not the directory itself) into NewPath
		std::error_code ErrCode;
		if (NewPath.IsDirectory())
		{
			stdfs::copy(DirEntry.path(), NewPath.AsNative(), stdfs::copy_options(Options), ErrCode);
		}

		return (!ErrCode);
	}

	bool SDirectory::MoveTo(SPath const& NewPath)
	{
		// TODO maybe add support for existing directory

		// NOTE stdfs::rename has semantics for an existing directory on some platforms, which typically involves deleting that directory.
		//  We'd rather disallow this completely.
		if (!NewPath.Exists())
		{
			std::error_code ErrCode;
			stdfs::rename(DirEntry.path(), NewPath.AsNative(), ErrCode);
			return (!ErrCode);
		}
		return false;
	}

	SPlatformString SDirectory::GetName() const
	{
		// NOTE filename() creates a new path, so we must persist the string memory

		// FIXME once we add const funcs to StringView,
		//  we can return a view into the substring with path(), as it returns a const lval-ref (instead of filename())
		//  and index of last stdfs::preferred_separator as args;
		//  #include "String.hpp" can be removed from FilesystemCommon afterwards
		return SPlatformString{ DirEntry.path().filename().c_str() };
	}

	bool SDirectory::SetName(SPlatformStringView NewName)
	{
		stdfs::path OldPath = DirEntry.path();
		stdfs::path NewPath{ std::basic_string_view<stdfs::path::value_type>(NewName) };

		AEON_FAILSAFE(!NewPath.filename().empty() && NewPath.parent_path().empty(), return false,
					  "NewName must be a name, and not a path!");

		NewPath = OldPath.replace_filename(NewPath);

		std::error_code ErrCode;
		if (stdfs::exists(NewPath, ErrCode))
		{
			return false;
		}

		ErrCode.clear();
		stdfs::rename(OldPath, NewPath, ErrCode);

		if (!ErrCode)
		{
			RefreshStats();
		}

		return (!ErrCode);
	}

	EFilesystemPermissions SDirectory::GetPermissions() const
	{
		return EFilesystemPermissions(int32(DirEntry.status().permissions()));
	}

	bool SDirectory::SetPermissions(EFilesystemPermissions NewPermissions)
	{
		[[maybe_unused]] std::error_code ErrCode;
		stdfs::permissions(DirEntry.path(), stdfs::perms(NewPermissions),
						   stdfs::perm_options::replace | stdfs::perm_options::nofollow,
						   ErrCode);

		if (!ErrCode)
		{
			RefreshStats();
		}

		return (!ErrCode);
	}

	void SDirectory::RefreshStats() const
	{
		[[maybe_unused]] std::error_code ErrCode;
		DirEntry.refresh(ErrCode);
	}

	szint SDirectory::SizeInBytes() const
	{
#	if 0
		// CHECKME Use this for every platform where it is supported
		// - Windows: unsupported
		[[maybe_unused]] std::error_code ErrCode;
		return DirEntry.file_size(ErrCode);
#	else
		// NOTE Because it is implementation-defined whether stdfs::file_size() reports the size of directories,
		// this just queries the size of each file within the directory, and recursively queries the size (through this function) in subdirectories.
		szint DirSize = 0;
		for (auto CurrEntry : *this)
		{
			if (CurrEntry.IsDirectory())
			{
				auto SubDir = CurrEntry.AsDirectory();
				DirSize += SubDir->SizeInBytes();
			}
			else if (CurrEntry.IsFile())
			{
				auto File = CurrEntry.AsFile();
				DirSize += File->SizeInBytes();
			}
		}
		return DirSize;
#	endif
	}

	SPath SDirectory::Path() const
	{
		return SPath{ SPath::TInternalAccess::Tag, stdfs::path(DirEntry.path()) };
	}

	bool SDirectory::IsValid() const
	{
		return !DirEntry.path().empty();
	}

	SDirectory::ConstIteratorType SDirectory::begin() const
	{
		return ConstIteratorType{ DirEntry.path() };
	}
	SDirectory::IteratorType SDirectory::begin()
	{
		return IteratorType{ DirEntry.path() };
	}
	SDirectory::ConstIteratorType SDirectory::cbegin() const
	{
		return ConstIteratorType{ DirEntry.path() };
	}

	SDirectory::ConstIteratorType SDirectory::end() const
	{
		return ConstIteratorType{};
	}
	SDirectory::IteratorType SDirectory::end()
	{
		return IteratorType{};
	}
	SDirectory::ConstIteratorType SDirectory::cend() const
	{
		return ConstIteratorType{};
	}

	SDirectory SDirectory::GetWorkingDirectory()
	{
		std::error_code ErrCode;
		stdfs::path DirPath = stdfs::current_path(ErrCode);
		AEON_FAILSAFE(!ErrCode, return SDirectory{}, "Failed to query the working directory!");
		return SDirectory{ TInternalAccess::Tag, stdfs::directory_entry{ DirPath } };
	}

	bool SDirectory::SetWorkingDirectory(SDirectory const& NewWorkingDir)
	{
		std::error_code ErrCode;
		AEON_FAILSAFE(NewWorkingDir.IsValid(), return false, "Failed to change the working directory!");
		stdfs::current_path(NewWorkingDir.Path().AsNative(), ErrCode);
		return !ErrCode;
	}

	SDirectoryWorkingContext SDirectory::SwitchWorkingDirectory(SDirectory const& NewWorkingDir)
	{
		return SDirectoryWorkingContext{ SDirectoryWorkingContext::TInternalAccess::Tag, NewWorkingDir };
	}

	SDirectoryWorkingContext::SDirectoryWorkingContext(TInternalAccess, SDirectory const& NewWorkingDir)
		: WorkingDir(SDirectory::GetWorkingDirectory())
	{
		if (operator bool())
		{
			SDirectory::SetWorkingDirectory(NewWorkingDir);
		}
	}

	SDirectoryWorkingContext::~SDirectoryWorkingContext()
	{
		if (operator bool())
		{
			SDirectory::SetWorkingDirectory(WorkingDir);
		}
	}

	SDirectoryWorkingContext::operator bool() const
	{
		return WorkingDir.IsValid();
	}
}
