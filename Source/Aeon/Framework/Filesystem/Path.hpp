#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_FILESYSTEM_PATH
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_FILESYSTEM_PATH

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Filesystem/FilesystemCommon.hpp"

// TODO rename this to PATHSTR
#if AEON_PLATFORM_WINDOWS

// Create a string literal as a platform-specific encoded string literal
#	define PATHSTR(Str) wstr(Str)
	static_assert(std::is_same_v<stdfs::path::value_type, wchar>);

#elif AEON_PLATFORM_POSIX

// Create a string literal as a platform-specific encoded string literal
#	define PATHSTR(Str) Str
	static_assert(std::is_same_v<stdfs::path::value_type, char>);

#else
#	error Platform not supported!
#endif

namespace Aeon
{
	class SPath
	{
	public:
		AEON_DECLARE_ACCESS_CLASS(TInternalAccess, class SPath, class SDirectory, class SFile, class SDirectoryEntry);

		SPath() = default;

		explicit SPath(TInternalAccess, stdfs::path&& NewPath);
		SPath(SPlatformStringView NewPath);

		AEON_MAKE_COPYABLE(SPath);
		AEON_MAKE_MOVEABLE(SPath);

		SPath ParentPath() const;
		SPath MakeChildPath(SPath const& NewChildPath) const;

		SPath AsCanonical() const;
		SPath AsAbsolute() const;
		SPath AsRelative(SPath const& CommonRoot) const;

		stdfs::path AsNative() const;

		bool IsAbsolute() const;
		bool IsRelative() const;

		bool Exists() const;

		bool IsDirectory() const;
		bool IsFile() const;
		bool IsSymlink() const; // TODO Implement symlinks

		SPlatformString GetLeafName() const;
		SPlatformString GetLeafExtension() const;
		void SetLeafName(SPlatformStringView NewName);
		void SetLeafExtension(SPlatformStringView NewName);

		void Append(SPlatformStringView NewName);

		bool IsEquivalent(SPath const& Other) const;

		bool IsValid() const;

		SPath operator*() const;
		SPath operator/(SPath const& NewPath) const;
		SPath operator/(SPlatformStringView NewPath) const;

		bool operator==(SPath const&) const = default;
		strong_ordering operator<=>(SPath const&) const = default;

		explicit operator bool() const;

		operator SPlatformStringView() const&;
		operator SPlatformStringView() const&& = delete;

	private:
		stdfs::path Path;
	};
}

#endif