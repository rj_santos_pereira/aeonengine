#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_FILESYSTEM_DIRECTORY
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_FILESYSTEM_DIRECTORY

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Filesystem/FilesystemCommon.hpp"

namespace Aeon
{
	AEON_DECLARE_BOOL_ENUM(EDirectoryDeleteEmptyOnly);

	AEON_DECLARE_FLAG_ENUM(EDirectoryCopyOptions, uint32)
	(
		AEON_FLAG_ENUM_RULE_AS_IS,

		// Report error on existing files, follow symlinks, skip subdirectories, copy all files.
		Default = int32(stdfs::copy_options::none),

		ErrorOnExistingFiles = int32(stdfs::copy_options::none),
		// Keep the existing file, without reporting an error.
		KeepExistingFiles = int32(stdfs::copy_options::skip_existing),
		// Replace the existing file.
		OverwriteExistingFiles = int32(stdfs::copy_options::overwrite_existing),
		// Replace the existing file only if it is older than the file being copied.
		OverwriteOutdatedExistingFiles = int32(stdfs::copy_options::update_existing),

		FollowSymlinks = int32(stdfs::copy_options::none),
		// Copy symlinks as symlinks, not as the files they point to.
		CopySymlinks = int32(stdfs::copy_options::copy_symlinks),
		// Ignore symlinks.
		SkipSymlinks = int32(stdfs::copy_options::skip_symlinks),

		SkipSubdirectories = int32(stdfs::copy_options::none),
		// Recursively copy subdirectories and files contain therein.
		CopySubdirectoriesRecursively = int32(stdfs::copy_options::none),

		CopyAllFiles = int32(stdfs::copy_options::directories_only),
		// Copy the directory structure, but do not copy any non-directory files.
		CopyDirectoriesOnly = int32(stdfs::copy_options::directories_only),
		// Instead of creating copies of files, create symlinks pointing to the originals.
		CreateSymlinksToFiles = int32(stdfs::copy_options::create_symlinks),
	//	CreateHardLinksToFiles = int32(stdfs::copy_options::create_hard_links),

	//	WindowsDefault = KeepExistingFiles | CopySubdirectoriesRecursively | CopySymlinks,
		// TODO maybe defaults for other platforms?
	);

	class SDirectoryWorkingContext;

	class SDirectory
	{
	public:
		AEON_DECLARE_ACCESS_CLASS(TInternalAccess,
								  class SDirectory,
								  class SDirectoryEntry);

		using ConstIteratorType = GDirectoryIterator<SDirectoryEntry const>;
		using IteratorType = GDirectoryIterator<SDirectoryEntry>;

		SDirectory() = default;
		explicit SDirectory(TInternalAccess, stdfs::directory_entry&& NewDirEntry);

		AEON_MAKE_COPYABLE(SDirectory);
		AEON_MAKE_MOVEABLE(SDirectory);

		static SDirectory Create(SPath const& NewPath);

		static SDirectory At(SPath const& Path);

		SDirectory Parent() const;

		SDirectory Child(SPath const& NewDirectory) const;

		bool Delete(EDirectoryDeleteEmptyOnly DeleteEmptyOnly);

		void Reset();

		bool CopyTo(SPath const& NewPath, EDirectoryCopyOptions Options);

		bool MoveTo(SPath const& NewPath);

		SPlatformString GetName() const;

		bool SetName(SPlatformStringView NewName);

		EFilesystemPermissions GetPermissions() const;

		bool SetPermissions(EFilesystemPermissions NewPermissions);

		void RefreshStats() const;

		szint SizeInBytes() const;

		// TODO implement these
		szint FileCount() const;

		szint SubdirectoryCount() const;

		SPath Path() const;

		bool IsValid() const;

		ConstIteratorType begin() const;
		IteratorType begin();
		ConstIteratorType cbegin() const;

		ConstIteratorType end() const;
		IteratorType end();
		ConstIteratorType cend() const;

		static SDirectory GetWorkingDirectory();
		static bool SetWorkingDirectory(SDirectory const& NewWorkingDir);

		static SDirectoryWorkingContext SwitchWorkingDirectory(SDirectory const& NewWorkingDir);

	private:
		mutable stdfs::directory_entry DirEntry;
	};

	class SDirectoryWorkingContext
	{
	public:
		AEON_DECLARE_ACCESS_CLASS(TInternalAccess, class SDirectory);

		SDirectoryWorkingContext(TInternalAccess, SDirectory const& NewWorkingDir);
		~SDirectoryWorkingContext();

		AEON_MAKE_NON_COPYABLE(SDirectoryWorkingContext);
		AEON_MAKE_NON_MOVEABLE(SDirectoryWorkingContext);

		explicit operator bool() const;

	private:
		SDirectory WorkingDir;
	};

}

#endif