#include "Aeon/Framework/Filesystem/Path.hpp"

namespace Aeon
{
	SPath::SPath(TInternalAccess, stdfs::path&& NewPath)
		: Path{ Move(NewPath) }
	{
	}

	SPath::SPath(SPlatformStringView NewPath)
		: Path{ stdfs::path{ std::basic_string_view<stdfs::path::value_type>(NewPath), stdfs::path::generic_format }.lexically_normal() }
	{
	}

	SPath SPath::ParentPath() const
	{
		return SPath{ TInternalAccess::Tag, Path.parent_path() };
	}

	SPath SPath::MakeChildPath(SPath const& NewChildPath) const
	{
		AEON_ASSERT(!NewChildPath.Path.has_root_directory());
		stdfs::path NewPath = Path / NewChildPath.Path/*.relative_path()*/;
		return SPath{ TInternalAccess::Tag, Move(NewPath) };
	}

	SPath SPath::AsCanonical() const
	{
		std::error_code ErrCode;
		// NOTE We use weakly_canonical to allow non-existent paths to be made "canonical"
		stdfs::path NewPath = stdfs::weakly_canonical(Path, ErrCode);
		return (!ErrCode) ? SPath{ TInternalAccess::Tag, Move(NewPath) } : SPath{};
	}

	SPath SPath::AsAbsolute() const
	{
		std::error_code ErrCode;
		stdfs::path NewPath = stdfs::absolute(Path, ErrCode);
		return (!ErrCode) ? SPath{ TInternalAccess::Tag, Move(NewPath) } : SPath{};
	}

	SPath SPath::AsRelative(SPath const& CommonRoot) const
	{
		std::error_code ErrCode;
		stdfs::path NewPath = stdfs::relative(Path, CommonRoot.Path, ErrCode);
		return (!ErrCode) ? SPath{ TInternalAccess::Tag, Move(NewPath) } : SPath{};
	}

	stdfs::path SPath::AsNative() const
	{
		return Path;
	}

	bool SPath::IsAbsolute() const
	{
		return Path.is_absolute();
	}

	bool SPath::IsRelative() const
	{
		return Path.is_relative();
	}

	bool SPath::Exists() const
	{
		[[maybe_unused]] std::error_code ErrCode;
		return stdfs::exists(Path, ErrCode);
	}

	bool SPath::IsDirectory() const
	{
		[[maybe_unused]] std::error_code ErrCode;
		return stdfs::is_directory(Path, ErrCode);
	}

	bool SPath::IsFile() const
	{
		[[maybe_unused]] std::error_code ErrCode;
		return stdfs::is_regular_file(Path, ErrCode);
	}

	bool SPath::IsSymlink() const
	{
		[[maybe_unused]] std::error_code ErrCode;
		return stdfs::is_symlink(Path, ErrCode);
	}

	SPlatformString SPath::GetLeafName() const
	{
		// FIXME See SFile::GetName
		return SPlatformString{ Path.filename().c_str() };
	}
	SPlatformString SPath::GetLeafExtension() const
	{
		// FIXME See SFile::GetName
		return SPlatformString{ Path.extension().c_str() };
	}

	void SPath::SetLeafName(SPlatformStringView NewName)
	{
		Path.replace_filename(stdfs::path{ std::basic_string_view<stdfs::path::value_type>{ NewName } });
	}
	void SPath::SetLeafExtension(SPlatformStringView NewName)
	{
		Path.replace_extension(stdfs::path{ std::basic_string_view<stdfs::path::value_type>{ NewName } });
	}

	void SPath::Append(SPlatformStringView NewName)
	{
		Path.concat(std::basic_string_view<stdfs::path::value_type>{ NewName });
	}

	bool SPath::IsEquivalent(SPath const& Other) const
	{
		[[maybe_unused]] std::error_code ErrCode;
		return stdfs::equivalent(Path, Other.Path, ErrCode);
	}

	bool SPath::IsValid() const
	{
		return !Path.empty();
	}

	SPath SPath::operator*() const
	{
		return ParentPath();
	}

	SPath SPath::operator/(SPath const& NewPath) const
	{
		return MakeChildPath(NewPath);
	}

	SPath SPath::operator/(SPlatformStringView NewPath) const
	{
		return MakeChildPath(SPath{ NewPath });
	}

	SPath::operator bool() const
	{
		return IsValid();
	}

	SPath::operator SPlatformStringView() const&
	{
		return SPlatformStringView{ Path.c_str() };
	}
}
