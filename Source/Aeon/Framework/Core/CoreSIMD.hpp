#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CORE_CORESIMD
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CORE_CORESIMD

#include "CoreMemory.hpp"

namespace Aeon::SIMD
{
	AEON_DECLARE_TAG_CLASS(TZeroInitRegister);
	AEON_DECLARE_TAG_CLASS(TFullOneInitRegister);
	AEON_DECLARE_TAG_CLASS(TAlignedInitRegister);

	// TODO maybe implement SIntRegister64? (for SWAR)

	// TODO wrap everything relevant in arch / SSE checks (through pre-processor)
	
	template <class Type>
	concept CSIMDIntRegister = TDisjunction<TIsSameClass<Type, __m128i>,
	    									TIsSameClass<Type, __m256i>,
	    									TIsSameClass<Type, __m512i>>::Value;

	template <class Type>
	concept CSIMDFloat16Register = TDisjunction<TIsSameClass<Type, __m128bh>,
												TIsSameClass<Type, __m256bh>,
												TIsSameClass<Type, __m512bh>>::Value;

	template <class Type>
	concept CSIMDFloat32Register = TDisjunction<TIsSameClass<Type, __m128>,
												TIsSameClass<Type, __m256>,
												TIsSameClass<Type, __m512>>::Value;

	template <class Type>
	concept CSIMDFloat64Register = TDisjunction<TIsSameClass<Type, __m128d>,
												TIsSameClass<Type, __m256d>,
												TIsSameClass<Type, __m512d>>::Value;

	template <class Type>
	concept CSIMDFloatRegister = CSIMDFloat16Register<Type> || CSIMDFloat32Register<Type> || CSIMDFloat64Register<Type>;

	template <class Type>
	concept CSIMDRegister = CSIMDIntRegister<Type> || CSIMDFloatRegister<Type>;

#if AEON_COMPILER_MSVC
#	pragma warning(push)
#	pragma warning(disable: 4700)
#	pragma runtime_checks("u", off) // Disable /RTCu for these functions // FIXME this pragma isn't working... typical
#elif AEON_COMPILER_CLANG
#	pragma clang diagnostic push
#	pragma clang diagnostic ignored "-Wuninitialized"
#endif

	template <CSIMDRegister RegType>
	AEON_FORCEINLINE RegType MakeFullZeroRegister()
	{
		// CHECKME validate that MSVC generates correct code in Prod builds
		RegType Reg;
#	if AEON_COMPILER_MSVC and AEON_BUILD_DEVELOPMENT
		Reg = {};
#	endif
		constexpr szint RegSize = sizeof(RegType);
		if constexpr (RegSize == 64)
		{
			if constexpr (CSIMDFloat32Register<RegType>)
			{
				return _mm512_xor_ps(Reg, Reg); // AVX512DQ
			}
			else if constexpr (CSIMDFloat64Register<RegType>)
			{
				return _mm512_xor_pd(Reg, Reg); // AVX512DQ
			}
			else
			{
				return _mm512_xor_si512(Reg, Reg); // AVX512F
			}
		}
		else if constexpr (RegSize == 32)
		{
			if constexpr (CSIMDFloat32Register<RegType>)
			{
				return _mm256_xor_ps(Reg, Reg); // AVX
			}
			else if constexpr (CSIMDFloat64Register<RegType>)
			{
				return _mm256_xor_pd(Reg, Reg); // AVX
			}
			else
			{
				return _mm256_xor_si256(Reg, Reg); // AVX2
			}
		}
		else /* if constexpr (RegSize == 16) */
		{
			if constexpr (CSIMDFloat32Register<RegType>)
			{
				return _mm_xor_ps(Reg, Reg); // SSE
			}
			else if constexpr (CSIMDFloat64Register<RegType>)
			{
				return _mm_xor_pd(Reg, Reg); // SSE2
			}
			else
			{
				return _mm_xor_si128(Reg, Reg); // SSE2
			}
		}
	}

	// TODO see https://stackoverflow.com/a/32422471 for generating masks

	template <CSIMDIntRegister IntRegType>
	AEON_FORCEINLINE IntRegType MakeFullOneRegister()
	{
		[[maybe_unused]] IntRegType Reg;
		constexpr szint RegSize = sizeof(IntRegType);
		if constexpr (RegSize == 64)
		{
		//	return _mm512_broadcastmb_epi64(0xFF);
			return _mm512_set1_epi32(-1); // vpternlogd/vpbroadcastd (AVX512F)
		}
		else if constexpr (RegSize == 32)
		{
		//	return _mm256_cmpeq_epi32(Reg, Reg); // AVX2
			return _mm256_set1_epi32(-1); // pcmpeqd (AVX)
		}
		else /* if constexpr (RegSize == 16) */
		{
		//	return _mm_cmpeq_epi32(Reg, Reg); // SSE2
			return _mm_set1_epi32(-1); // pcmpeqd (SSE2)
		}
	}

#if AEON_COMPILER_MSVC
#	pragma runtime_checks("u", restore)
#	pragma warning(pop)
#elif AEON_COMPILER_CLANG
#	pragma clang diagnostic pop
#endif

	template <CInteger IntType, IntType IntValue>
	struct GIntImmediate : TObject<IntValue>
	{
	};

	template <int8 IntValue>
	using GIntImmediateI8 = GIntImmediate<int8, IntValue>;
	template <int16 IntValue>
	using GIntImmediateI16 = GIntImmediate<int16, IntValue>;
	template <int32 IntValue>
	using GIntImmediateI32 = GIntImmediate<int32, IntValue>;
	template <int64 IntValue>
	using GIntImmediateI64 = GIntImmediate<int64, IntValue>;

	template <uint8 IntValue>
	using GIntImmediateU8 = GIntImmediate<uint8, IntValue>;
	template <uint16 IntValue>
	using GIntImmediateU16 = GIntImmediate<uint16, IntValue>;
	template <uint32 IntValue>
	using GIntImmediateU32 = GIntImmediate<uint32, IntValue>;
	template <uint64 IntValue>
	using GIntImmediateU64 = GIntImmediate<uint64, IntValue>;

	template <auto IntValue>
	AEON_FORCEINLINE consteval auto Immediate()
	{
		return GIntImmediate<decltype(IntValue), IntValue>{};
	}

	struct SIntRegister128;
	struct SFP32Register128;
	struct SFP64Register128;

	struct SIntRegister128
	{
		using StorageType = __m128i;

		SIntRegister128() = default;

		explicit SIntRegister128(TZeroInitRegister)
			: Storage(MakeFullZeroRegister<StorageType>())
		{
		};
		explicit SIntRegister128(TFullOneInitRegister)
			: Storage(MakeFullOneRegister<StorageType>())
		{
		};
		explicit SIntRegister128(StorageType NewStorage)
			: Storage(NewStorage)
		{
		}

		explicit SIntRegister128(cvoidptr NewStoragePtr)
		{
			Storage = _mm_loadu_si128(static_cast<StorageType const*>(NewStoragePtr)); // movdqu (SSE2)
		}
		explicit SIntRegister128(TAlignedInitRegister, cvoidptr NewStoragePtr)
		{
			AEON_ASSERT(AeonMemory::IsBlockPointerValid(NewStoragePtr, AeonMemory::x86PlatformSSEAlignment));
			Storage = _mm_load_si128(static_cast<StorageType const*>(NewStoragePtr)); // movdqa (SSE2)
		}

		SIntRegister128(InitList<int8> InitList)
		{
			AEON_ASSERT(Aeon::InitListOps::Count(InitList) == 16);

			Storage = _mm_loadu_si128(reinterpret_cast<StorageType const*>(Aeon::InitListOps::Data(InitList))); // movdqu (SSE2)
			// CHECKME further study this
			// https://www.felixcloutier.com/x86/lddqu
			// "In situations that require the data loaded by (V)LDDQU be modified and stored to the same location,
			//  use (V)MOVDQU or (V)MOVDQA instead of (V)LDDQU."
			// "This instruction is a replacement for (V)MOVDQU (load) in situations where cache line splits significantly affect performance.
			//  It should not be used in situations where store-load forwarding is performance critical.
			//  If performance of store-load forwarding is critical to the application,
			//  use (V)MOVDQA store-load pairs when data is 256/128-bit aligned or (V)MOVDQU store-load pairs when data is 256/128-bit unaligned."
		//	Storage = _mm_lddqu_si128(reinterpret_cast<DataStorageType const*>(Aeon::InitListOps::Data(InitList))); // lddqu (SSE3)
		}
		SIntRegister128(InitList<int16> InitList)
		{
			AEON_ASSERT(Aeon::InitListOps::Count(InitList) == 8);

			Storage = _mm_loadu_si128(reinterpret_cast<StorageType const*>(Aeon::InitListOps::Data(InitList))); // movdqu (SSE2)
		}
		SIntRegister128(InitList<int32> InitList)
		{
			AEON_ASSERT(Aeon::InitListOps::Count(InitList) == 4);

			Storage = _mm_loadu_si128(reinterpret_cast<StorageType const*>(Aeon::InitListOps::Data(InitList))); // movdqu (SSE2)
		}
		SIntRegister128(InitList<int64> InitList)
		{
			AEON_ASSERT(Aeon::InitListOps::Count(InitList) == 2);
			Storage = _mm_loadu_si128(reinterpret_cast<StorageType const*>(Aeon::InitListOps::Data(InitList))); // movdqu (SSE2)
		}

		operator SFP32Register128() const;
		operator SFP64Register128() const;

		StorageType Storage;
	};

	// TODO implement Int 256 register
	// TODO implement Int 512 register

	// CHECKME should VectorIntXOps really derive from this?

	struct VectorIntOps
	{
		AEON_FORCEINLINE static SIntRegister128 Not(SIntRegister128 const& Reg)
		{
			auto FullOneReg = MakeFullOneRegister<typename SIntRegister128::StorageType>(); // pcmpeqd (SSE2)
			return SIntRegister128{ _mm_xor_si128(Reg.Storage, FullOneReg) }; // pxor (SSE2)
		}
		AEON_FORCEINLINE static SIntRegister128 And(SIntRegister128 const& Reg1, SIntRegister128 const& Reg2)
		{
			return SIntRegister128{ _mm_and_si128(Reg1.Storage, Reg2.Storage) }; // pand (SSE2)
		}
		AEON_FORCEINLINE static SIntRegister128 NotAnd(SIntRegister128 const& Reg1, SIntRegister128 const& Reg2)
		{
			// NOTE Despite the name of the intrinsic (and of the mnemonic),
			//  this computes the NOT of the first argument and then ANDs it with the second argument
			return SIntRegister128{ _mm_andnot_si128(Reg1.Storage, Reg2.Storage) }; // pandn (SSE2)
		}
		AEON_FORCEINLINE static SIntRegister128 Or(SIntRegister128 const& Reg1, SIntRegister128 const& Reg2)
		{
			return SIntRegister128{ _mm_or_si128(Reg1.Storage, Reg2.Storage) }; // por (SSE2)
		}
		AEON_FORCEINLINE static SIntRegister128 Xor(SIntRegister128 const& Reg1, SIntRegister128 const& Reg2)
		{
			return SIntRegister128{ _mm_xor_si128(Reg1.Storage, Reg2.Storage) }; // pxor (SSE2)
		}

#if 0
		template <int32 ShiftValue>
		AEON_FORCEINLINE static SIntRegister128 ByteLsh(SIntRegister128 const& Reg, TIntImmediate<ShiftValue>)
		requires (AeonMath::IsClamped(ShiftValue, 0, 15))
		{
			return SIntRegister128{ _mm_bslli_si128(Reg.Storage, ShiftValue) };
		}
		template <int32 ShiftValue>
		AEON_FORCEINLINE static SIntRegister128 ByteRsh(SIntRegister128 const& Reg, TIntImmediate<ShiftValue>)
		requires (AeonMath::IsClamped(ShiftValue, 0, 15))
		{
			return SIntRegister128{ _mm_bsrli_si128(Reg.Storage, ShiftValue) };
		}
#endif
	};

	// TODO ScalarIntOps (certain SSE ops operate on singular scalar values loaded into vector registers)

	struct VectorInt8Ops : VectorIntOps
	{
		using VectorIntOps::Not;
		using VectorIntOps::And;
		using VectorIntOps::NotAnd;
		using VectorIntOps::Or;
		using VectorIntOps::Xor;

		AEON_FORCEINLINE static int32 MakeMask(SIntRegister128 const& Reg)
		{
			return _mm_movemask_epi8(Reg.Storage); // pmovmskb (SSE2)
		}

		AEON_FORCEINLINE static SIntRegister128 Add(SIntRegister128 const& Reg1, SIntRegister128 const& Reg2)
		{
			return SIntRegister128{ _mm_add_epi8(Reg1.Storage, Reg2.Storage) }; // paddb (SSE2)
		}
		AEON_FORCEINLINE static SIntRegister128 Sub(SIntRegister128 const& Reg1, SIntRegister128 const& Reg2)
		{
			return SIntRegister128{ _mm_sub_epi8(Reg1.Storage, Reg2.Storage) }; // psubb (SSE2)
		}

		AEON_FORCEINLINE static SIntRegister128 UnsignedAvg(SIntRegister128 const& Reg1, SIntRegister128 const& Reg2)
		{
			return SIntRegister128{ _mm_avg_epu8(Reg1.Storage, Reg2.Storage) }; // pavgb (SSE2)
		}

		AEON_FORCEINLINE static SIntRegister128 Abs(SIntRegister128 const& Reg)
		{
			return SIntRegister128{ _mm_abs_epi8(Reg.Storage) }; // pabsb (SSSE3)
		}

		AEON_FORCEINLINE static SIntRegister128 Min(SIntRegister128 const& Reg1, SIntRegister128 const& Reg2)
		{
			return SIntRegister128{ _mm_min_epi8(Reg1.Storage, Reg2.Storage) }; // pminsb (SSE4.1)
		}
		AEON_FORCEINLINE static SIntRegister128 UnsignedMin(SIntRegister128 const& Reg1, SIntRegister128 const& Reg2)
		{
			return SIntRegister128{ _mm_min_epu8(Reg1.Storage, Reg2.Storage) }; // pminub (SSE2)
		}

		AEON_FORCEINLINE static SIntRegister128 Max(SIntRegister128 const& Reg1, SIntRegister128 const& Reg2)
		{
			return SIntRegister128{ _mm_max_epi8(Reg1.Storage, Reg2.Storage) }; // pmaxsb (SSE4.1)
		}
		AEON_FORCEINLINE static SIntRegister128 UnsignedMax(SIntRegister128 const& Reg1, SIntRegister128 const& Reg2)
		{
			return SIntRegister128{ _mm_max_epu8(Reg1.Storage, Reg2.Storage) }; // pmaxub (SSE2)
		}

		AEON_FORCEINLINE static SIntRegister128 CompareEq(SIntRegister128 const& Reg1, SIntRegister128 const& Reg2)
		{
			return SIntRegister128{ _mm_cmpeq_epi8(Reg1.Storage, Reg2.Storage) }; // pcmpeqb (SSE2)
		}
	};

	struct VectorInt16Ops : VectorIntOps
	{
		using VectorIntOps::Not;
		using VectorIntOps::And;
		using VectorIntOps::NotAnd;
		using VectorIntOps::Or;
		using VectorIntOps::Xor;

		AEON_FORCEINLINE static SIntRegister128 Add(SIntRegister128 const& Reg1, SIntRegister128 const& Reg2)
		{
			return SIntRegister128{ _mm_add_epi16(Reg1.Storage, Reg2.Storage) }; // paddw (SSE2)
		}
		AEON_FORCEINLINE static SIntRegister128 Sub(SIntRegister128 const& Reg1, SIntRegister128 const& Reg2)
		{
			return SIntRegister128{ _mm_sub_epi16(Reg1.Storage, Reg2.Storage) }; // psubw (SSE2)
		}

		AEON_FORCEINLINE static SIntRegister128 UnsignedAvg(SIntRegister128 const& Reg1, SIntRegister128 const& Reg2)
		{
			return SIntRegister128{ _mm_avg_epu16(Reg1.Storage, Reg2.Storage) }; // pavgw (SSE2)
		}

		template <uint8 MaskValue>
		AEON_FORCEINLINE static SIntRegister128 Blend(SIntRegister128 const& Reg1, SIntRegister128 const& Reg2, GIntImmediateU8<MaskValue>)
		{
			// NOTE the mask argument to pblendw must be an immediate value
			return SIntRegister128{ _mm_blend_epi16(Reg1.Storage, Reg2.Storage, int32(MaskValue)) }; // pblendw (SSE4.1)
		}

		AEON_FORCEINLINE static SIntRegister128 Abs(SIntRegister128 const& Reg)
		{
			return SIntRegister128{ _mm_abs_epi16(Reg.Storage) }; // pabsw (SSSE3)
		}

		AEON_FORCEINLINE static SIntRegister128 Min(SIntRegister128 const& Reg1, SIntRegister128 const& Reg2)
		{
			return SIntRegister128{ _mm_min_epi16(Reg1.Storage, Reg2.Storage) }; // pminsw (SSE2)
		}
		AEON_FORCEINLINE static SIntRegister128 UnsignedMin(SIntRegister128 const& Reg1, SIntRegister128 const& Reg2)
		{
			return SIntRegister128{ _mm_min_epu16(Reg1.Storage, Reg2.Storage) }; // pminuw (SSE4.1)
		}

		AEON_FORCEINLINE static SIntRegister128 Max(SIntRegister128 const& Reg1, SIntRegister128 const& Reg2)
		{
			return SIntRegister128{ _mm_max_epi16(Reg1.Storage, Reg2.Storage) }; // pmaxsw (SSE2)
		}
		AEON_FORCEINLINE static SIntRegister128 UnsignedMax(SIntRegister128 const& Reg1, SIntRegister128 const& Reg2)
		{
			return SIntRegister128{ _mm_max_epu16(Reg1.Storage, Reg2.Storage) }; // pmaxuw (SSE4.1)
		}

		AEON_FORCEINLINE static SIntRegister128 CompareEq(SIntRegister128 const& Reg1, SIntRegister128 const& Reg2)
		{
			return SIntRegister128{ _mm_cmpeq_epi16(Reg1.Storage, Reg2.Storage) }; // pcmpeqw (SSE2)
		}
	};

	struct VectorInt32Ops : VectorIntOps
	{
		using VectorIntOps::Not;
		using VectorIntOps::And;
		using VectorIntOps::NotAnd;
		using VectorIntOps::Or;
		using VectorIntOps::Xor;

		AEON_FORCEINLINE static SIntRegister128 Add(SIntRegister128 const& Reg1, SIntRegister128 const& Reg2)
		{
			return SIntRegister128{ _mm_add_epi32(Reg1.Storage, Reg2.Storage) }; // paddd (SSE2)
		}
		AEON_FORCEINLINE static SIntRegister128 Sub(SIntRegister128 const& Reg1, SIntRegister128 const& Reg2)
		{
			return SIntRegister128{ _mm_sub_epi32(Reg1.Storage, Reg2.Storage) }; // psubd (SSE2)
		}

		AEON_FORCEINLINE static SIntRegister128 Abs(SIntRegister128 const& Reg)
		{
			return SIntRegister128{ _mm_abs_epi32(Reg.Storage) }; // pabsd (SSSE3)
		}

		AEON_FORCEINLINE static SIntRegister128 Min(SIntRegister128 const& Reg1, SIntRegister128 const& Reg2)
		{
			return SIntRegister128{ _mm_min_epi32(Reg1.Storage, Reg2.Storage) }; // pminsd (SSE4.1)
		}
		AEON_FORCEINLINE static SIntRegister128 UnsignedMin(SIntRegister128 const& Reg1, SIntRegister128 const& Reg2)
		{
			return SIntRegister128{ _mm_min_epu32(Reg1.Storage, Reg2.Storage) }; // pminud (SSE4.1)
		}

		AEON_FORCEINLINE static SIntRegister128 Max(SIntRegister128 const& Reg1, SIntRegister128 const& Reg2)
		{
			return SIntRegister128{ _mm_max_epi32(Reg1.Storage, Reg2.Storage) }; // pmaxsd (SSE4.1)
		}
		AEON_FORCEINLINE static SIntRegister128 UnsignedMax(SIntRegister128 const& Reg1, SIntRegister128 const& Reg2)
		{
			return SIntRegister128{ _mm_max_epu32(Reg1.Storage, Reg2.Storage) }; // pmaxud (SSE4.1)
		}

		AEON_FORCEINLINE static SIntRegister128 CompareEq(SIntRegister128 const& Reg1, SIntRegister128 const& Reg2)
		{
			return SIntRegister128{ _mm_cmpeq_epi32(Reg1.Storage, Reg2.Storage) }; // pcmpeqd (SSE2)
		}
	};

	struct VectorInt64Ops : VectorIntOps
	{
		using VectorIntOps::Not;
		using VectorIntOps::And;
		using VectorIntOps::NotAnd;
		using VectorIntOps::Or;
		using VectorIntOps::Xor;

		AEON_FORCEINLINE static SIntRegister128 Add(SIntRegister128 const& Reg1, SIntRegister128 const& Reg2)
		{
			return SIntRegister128{ _mm_add_epi64(Reg1.Storage, Reg2.Storage) }; // paddq (SSE2)
		}
		AEON_FORCEINLINE static SIntRegister128 Sub(SIntRegister128 const& Reg1, SIntRegister128 const& Reg2)
		{
			return SIntRegister128{ _mm_sub_epi64(Reg1.Storage, Reg2.Storage) }; // psubq (SSE2)
		}

#if 0
		AEON_FORCEINLINE static SIntRegister128 Abs(SIntRegister128 const& Reg)
		{
			return SIntRegister128{ _mm_abs_epi64(Reg.Storage) }; // pabsq (AVX512VL)
		}
#endif

		AEON_FORCEINLINE static SIntRegister128 CompareEq(SIntRegister128 const& Reg1, SIntRegister128 const& Reg2)
		{
			return SIntRegister128{ _mm_cmpeq_epi64(Reg1.Storage, Reg2.Storage) }; // pcmpeqq (SSE4.1)
		}
	};

#if 0
	struct SFloat16Register128
	{
		using DataStorageType = __m128bh;

		SFloat16Register128() = default;

		explicit SFloat16Register128(TZeroInitRegister)
			: Storage(MakeFullZeroRegister<DataStorageType>())
		{
		};
		explicit SFloat16Register128(DataStorageType NewStorage)
			: Storage(NewStorage)
		{
		}

		explicit SFloat16Register128(cvoidptr NewStoragePtr)
		{
			Storage = _mm_loadu_ps(static_cast<float32 const*>(NewStoragePtr)); // movups (SSE)
		}
		explicit SFloat16Register128(TAlignedInitRegister, cvoidptr NewStoragePtr)
		{
			AEON_ASSERT(AeonMemory::IsBlockPointerValid(NewStoragePtr, AeonMemory::x86PlatformSSEAlignment));
			Storage = _mm_load_ps(static_cast<float32 const*>(NewStoragePtr)); // movaps (SSE)
		}

		SFloat16Register128(InitList<float32> InitList)
		{
			AEON_ASSERT(Aeon::InitListOps::Count(InitList) == 4);

			Storage = _mm_loadu_ps(Aeon::InitListOps::Data(InitList)); // movups (SSE)
		}

		operator SIntRegister128() const;
		operator SFloatSingleRegister128() const;
		operator SFloatDoubleRegister128() const;

		DataStorageType Storage;
	};
#endif

	struct SFP32Register128
	{
		using StorageType = __m128;

		SFP32Register128() = default;

		explicit SFP32Register128(TZeroInitRegister)
			: Storage(MakeFullZeroRegister<StorageType>())
		{
		};
		explicit SFP32Register128(StorageType NewStorage)
			: Storage(NewStorage)
		{
		}

		explicit SFP32Register128(cvoidptr NewStoragePtr)
		{
			Storage = _mm_loadu_ps(static_cast<float32 const*>(NewStoragePtr)); // movups (SSE)
		}
		explicit SFP32Register128(TAlignedInitRegister, cvoidptr NewStoragePtr)
		{
			AEON_ASSERT(AeonMemory::IsBlockPointerValid(NewStoragePtr, AeonMemory::x86PlatformSSEAlignment));
			Storage = _mm_load_ps(static_cast<float32 const*>(NewStoragePtr)); // movaps (SSE)
		}

		SFP32Register128(InitList<float32> InitList)
		{
			AEON_ASSERT(Aeon::InitListOps::Count(InitList) == 4);

			Storage = _mm_loadu_ps(Aeon::InitListOps::Data(InitList)); // movups (SSE)
		}

		operator SIntRegister128() const;
		operator SFP64Register128() const;

		StorageType Storage;
	};

	// TODO implement SingleFloat 256 register
	// TODO implement SingleFloat 512 register

	// TODO implement VectorFloat32Ops

	struct SFP64Register128
	{
		using StorageType = __m128d;

		SFP64Register128() = default;

		explicit SFP64Register128(TZeroInitRegister)
			: Storage(MakeFullZeroRegister<StorageType>())
		{
		};
		explicit SFP64Register128(StorageType NewStorage)
			: Storage(NewStorage)
		{
		}

		explicit SFP64Register128(cvoidptr NewStoragePtr)
		{
			Storage = _mm_loadu_pd(static_cast<float64 const*>(NewStoragePtr)); // movupd (SSE2)
		}
		explicit SFP64Register128(TAlignedInitRegister, cvoidptr NewStoragePtr)
		{
			AEON_ASSERT(AeonMemory::IsBlockPointerValid(NewStoragePtr, AeonMemory::x86PlatformSSEAlignment));
			Storage = _mm_load_pd(static_cast<float64 const*>(NewStoragePtr)); // movapd (SSE2)
		}

		SFP64Register128(InitList<float64> InitList)
		{
			AEON_ASSERT(Aeon::InitListOps::Count(InitList) == 2);

			Storage = _mm_loadu_pd(Aeon::InitListOps::Data(InitList)); // movupd (SSE2)
		}

		operator SIntRegister128() const;
		operator SFP32Register128() const;

		StorageType Storage;
	};
	
	// TODO implement DoubleFloat 256 register
	// TODO implement DoubleFloat 512 register
	
	// TODO implement VectorFloat64Ops

	AEON_FORCEINLINE inline SIntRegister128::operator SFP32Register128() const
	{
		return SFP32Register128{ _mm_castsi128_ps(Storage) }; // no-op (SSE2)
	}
	AEON_FORCEINLINE inline SIntRegister128::operator SFP64Register128() const
	{
		return SFP64Register128{ _mm_castsi128_pd(Storage) }; // no-op (SSE2)
	}

	AEON_FORCEINLINE inline SFP32Register128::operator SIntRegister128() const
	{
		return SIntRegister128{ _mm_castps_si128(Storage) }; // no-op (SSE2)
	}
	AEON_FORCEINLINE inline SFP32Register128::operator SFP64Register128() const
	{
		return SFP64Register128{ _mm_castps_pd(Storage) }; // no-op (SSE2)
	}

	AEON_FORCEINLINE inline SFP64Register128::operator SIntRegister128() const
	{
		return SIntRegister128{ _mm_castpd_si128(Storage) }; // no-op (SSE2)
	}
	AEON_FORCEINLINE inline SFP64Register128::operator SFP32Register128() const
	{
		return SFP32Register128{ _mm_castpd_ps(Storage) }; // no-op (SSE2)
	}

}

namespace AeonSIMD = Aeon::SIMD;

#endif