#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CORE_COREUTILITIES
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CORE_COREUTILITIES

#include "CoreMetaTypes.hpp"

namespace Aeon
{
	namespace ZzInternal
	{
		using std::addressof;
		// Utility function to obtain the address of an object without triggering a function call to any overloaded operators
		// Introducing it into the global namespace to simulate a language construct (for symmetry with sizeof/alignof).
#define addressof ::Aeon::ZzInternal::addressof

		using std::bit_cast;
		// Cast-like function to simplify reinterpreting an object of some type as another (without breaking the Strict Aliasing rule).
		// This requires that the size of the objects is the same.
		// Introducing it into the global namespace to simulate a language cast.
#define bit_cast ::Aeon::ZzInternal::bit_cast

		// TODO possibly deprecate (and remove) this
		template <class ToType, class FromType>
		AEON_NODISCARD ToType bitseq_cast(FromType const* OldObj) noexcept
		requires CImplicitLifetimeObject<ToType> && CTriviallyCopyable<ToType> && CTriviallyCopyable<FromType>
				 && (sizeof(ToType) >= sizeof(FromType) && sizeof(ToType) % sizeof(FromType) == 0)
		{
			// NOTE: See https://www.open-std.org/jtc1/sc22/wg21/docs/papers/2020/p0593r6.html#when-to-create-objects
			//  and https://www.open-std.org/jtc1/sc22/wg21/docs/papers/2022/p2590r2.pdf
			//  for reasoning behind the code
			constexpr szint NewObjSize = sizeof(ToType);
			alignas(ToType) byte NewObjBytes[NewObjSize];
			::std::memcpy(NewObjBytes, OldObj, NewObjSize);
			return *::std::launder(reinterpret_cast<ToType*>(NewObjBytes));
		}
		// Cast-like function to simplify reinterpreting a sequence of objects of some type as another (without breaking the Strict Aliasing rule).
		// This requires that the size of the destination object is a multiple of the size of a source object. Unlike bit_cast, this is not constexpr.
		// Introducing it into the global namespace to simulate a language cast.
#define bitseq_cast ::Aeon::ZzInternal::bitseq_cast
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// STypeInfo

	class STypeInfo
	{
	private:
		STypeInfo()
			: TypeInfoPtr(nullptr)
		{
		}

	public:
		constexpr STypeInfo(std::type_info const& NewTypeInfo)
			: TypeInfoPtr(&NewTypeInfo)
		{
		}

		constexpr STypeInfo(STypeInfo const&) = default;
		constexpr STypeInfo(STypeInfo&&) = default;

		constexpr STypeInfo& operator=(STypeInfo const&) = default;
		constexpr STypeInfo& operator=(STypeInfo&&) = default;

		char const* Name() const
		{
			return TypeInfoPtr ? TypeInfoPtr->name() : "";
		}
		szint Hash() const
		{
			return TypeInfoPtr ? TypeInfoPtr->hash_code() : 0;
		}

		AEON_FORCEINLINE bool IsValid() const
		{
			return TypeInfoPtr;
		}

		bool operator==(STypeInfo const& Other) const
		{
			return (TypeInfoPtr && Other.TypeInfoPtr) ? (*TypeInfoPtr == *Other.TypeInfoPtr) : (TypeInfoPtr == Other.TypeInfoPtr);
		}
		strong_ordering operator<=>(STypeInfo const& Other) const
		{
			if (TypeInfoPtr && Other.TypeInfoPtr)
			{
				if ((*TypeInfoPtr).before(*Other.TypeInfoPtr))
				{
					return strong_ordering::less;
				}
				if ((*Other.TypeInfoPtr).before(*TypeInfoPtr))
				{
					return strong_ordering::greater;
				}
				return strong_ordering::equal;
			}
			return (TypeInfoPtr <=> Other.TypeInfoPtr);
		}

		static STypeInfo Null()
		{
			return {};
		}

	private:
		// NOTE Must store either a reference or a pointer,
		//  as the actual object may be polymorphic, according to the standard.
		std::type_info const* TypeInfoPtr;

	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GRefWrapper

#if 0
	template <class ObjType, bool = VIsTriviallyCopyable<ObjType>>
	struct ZzInternal_TRefWrapperTrivialityChecker {};

	template <class ObjType>
	struct ZzInternal_TRefWrapperTrivialityChecker<ObjType, true>
	{
		// TODO should we keep this warning here? or move it to the containers instead

		[[deprecated("'Aeon::GRefWrapper<ObjType, RefCategoryType>': ObjType is trivially copyable; "
					 "working with GRefWrapper might be less efficient than working with ObjType directly.")]]
		TRefWrapperTrivialityChecker_Internal()
		{
		}
	};
#endif

	// Enum-like struct, as workaround for TIsRefWrapper using TIsSameClassTemplate
	struct EWrappedRefCategory
	{
		struct LvalRef;
		struct RvalRef;
	};

	template <class Type>
	concept CWrappedRefCategory = CSameClass<Type, EWrappedRefCategory::LvalRef> || CSameClass<Type, EWrappedRefCategory::RvalRef>;

	template <CObject ObjType, CWrappedRefCategory RefCategoryType>
	// HACK Currently we can't correctly enforce the semantics below when we allow cv-qualified types here;
	//  as there isn't a compelling use case for them yet, we're just constraining the type to be cv-unqualified
	requires CNotConstVolatile<ObjType>
	class GRefWrapper
#if 0
		: ZzInternal_TRefWrapperTrivialityChecker<ObjType>
#endif
	{
		// Summarizing the type:
		// When RefCategoryType == LvalRef, we are representing a non-temporary type, which may be:
		//  - init from an lvalue-ref (lvalue)
		//  - cast to an lvalue-ref (lvalue)
		//  - cast to an rvalue-ref (xvalue)
		// When RefCategoryType == RvalRef, we are representing a temporary type, which may be:
		//  - init from an lvalue-ref (xvalue)
		//  - init from an rvalue-ref (xvalue or prvalue)
		//  - cast to an rvalue-ref (xvalue or prvalue)

	public:
		using ObjectType = ObjType;

		GRefWrapper(ObjectType& NewObject)
			: ObjectRef(static_cast<ObjectType&&>(NewObject))
		{
		}
		// Constraint: Can't initialize a lval-ref (representing a lvalue) from a rval-ref
		GRefWrapper(ObjectType&& NewObject)
		requires CSameClass<RefCategoryType, EWrappedRefCategory::RvalRef>
			: ObjectRef(static_cast<ObjectType&&>(NewObject))
		{
		}

		// Define copy/move ctors to make wrapper non-trivially-copyable
		GRefWrapper(GRefWrapper const& Other)
			: ObjectRef(static_cast<ObjectType&&>(Other.ObjectRef))
		{
		}
		GRefWrapper(GRefWrapper&& Other)
			: ObjectRef(static_cast<ObjectType&&>(Other.ObjectRef))
		{
		}

		// Delete assign operators since we can't reassign refs (would be solvable by storing a pointer, but meh)
		GRefWrapper& operator=(GRefWrapper const& Other) = delete;
		GRefWrapper& operator=(GRefWrapper&& Other) = delete;

		// Constraint: Can't cast a rval-ref (potentially representing a prvalue) to a lval-ref
		operator ObjectType&() const
		requires CSameClass<RefCategoryType, EWrappedRefCategory::LvalRef>
		{
			return static_cast<ObjectType&>(ObjectRef);
		}
		// Constraint: Conversion to a rval-ref must be explicit when storing a lval-ref
		explicit(CSameClass<RefCategoryType, EWrappedRefCategory::LvalRef>)
		operator ObjectType&&() const
		{
			return static_cast<ObjectType&&>(ObjectRef);
		}

	private:
		// NOTE When initializing this with a temporary, its lifetime is bound to the expression where this object is created.
		ObjectType&& ObjectRef;
	};

	template <CObject ObjType>
	using GLvalRefWrapper = GRefWrapper<ObjType, EWrappedRefCategory::LvalRef>;

	template <CObject ObjType>
	using GRvalRefWrapper = GRefWrapper<ObjType, EWrappedRefCategory::RvalRef>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsRefWrapper, TIsLvalRefWrapper, TIsRvalRefWrapper

	template <class Type>
	struct TIsRefWrapper : TIsSameClassTemplate<Type, GRefWrapper<int32, EWrappedRefCategory::LvalRef>> {};

	template <class Type>
	inline constexpr bool VIsRefWrapper = TIsRefWrapper<Type>::Value;

	template <class>
	struct TIsLvalRefWrapper : TFalseConstant {};
	template <class ObjType>
	struct TIsLvalRefWrapper<GLvalRefWrapper<ObjType>> : TTrueConstant {};

	template <class Type>
	inline constexpr bool VIsLvalRefWrapper = TIsLvalRefWrapper<Type>::Value;

	template <class>
	struct TIsRvalRefWrapper : TFalseConstant {};
	template <class ObjType>
	struct TIsRvalRefWrapper<GRvalRefWrapper<ObjType>> : TTrueConstant {};

	template <class Type>
	inline constexpr bool VIsRvalRefWrapper = TIsRvalRefWrapper<Type>::Value;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TUnwrapRefObjectType

	template <class WrappedType, class = void>
	struct ZzInternal_TUnwrapRefObjectType
	{
		// NOTE We use TRemoveReference instead of TDecayType
		//  to preserve cv-qualification and enable this to be used in Aeon::Move and Aeon::Copy
		using Type = TRemoveReference<WrappedType>;
	};

	template <class WrappedType>
	struct ZzInternal_TUnwrapRefObjectType<WrappedType, TEnableIf<VIsRefWrapper<TDecayType<WrappedType>>>>
	{
		using Type = typename TDecayType<WrappedType>::ObjectType;
	};

	template <class WrappedType>
	using TUnwrapRefObjectType = typename ZzInternal_TUnwrapRefObjectType<WrappedType>::Type;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// InitList, RvalRefInitList

//	template <class ElemType, szint ElemCountValue>
//	using StaticInitList = std::array<ElemType, ElemCountValue>;

	// Initializer list type, supports only copyable types
	template <class ElemType>
	using InitList = std::initializer_list<ElemType>;
	// Initializer list type, wraps objects with a special object to support moveable types
	template <class ElemType>
	using RvalRefInitList = std::initializer_list<GRvalRefWrapper<ElemType>>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// InitListOps

	struct InitListOps
	{
		template <class ElemType>
		AEON_FORCEINLINE static constexpr ElemType const* Data(InitList<ElemType> const& InitList)
		{
			return std::data(InitList);
		}

		template <class ElemType>
		AEON_FORCEINLINE static constexpr szint Count(InitList<ElemType> const& InitList)
		{
			return std::size(InitList);
		}
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copy

	template <class Type>
	AEON_FORCEINLINE constexpr decltype(auto) Copy(Type&& Value) noexcept
	{
#if 0
		if constexpr (VIsRefWrapper<TDecayType<Type>>)
		{
			return static_cast<typename TDecayType<Type>::ObjectType const&>(Value);
		}
		else
		{
			return static_cast<TRemoveReference<Type> const&>(Value);
		}
#endif
		// NOTE We need to cast this first to the non-cv-qualified version, which preserves cv-qualifiers regardless when Type IS NOT a RefWrapper,
		//  so that when Type IS a RefWrapper, this correctly selects the ObjectType& conversion operator.
		//  E.g.:
		//   T o1;
		//   T o2 = Aeon::Copy(Aeon::AsConst(o1)); // This is allowed
		//   T o3 = Aeon::Copy(Aeon::LRef(o1)); // This is allowed
		//   T o4 = Aeon::Copy(Aeon::RRef(o1)); // This is disallowed
		using ObjectType = TUnwrapRefObjectType<Type>;
		return static_cast<ObjectType const&>(static_cast<ObjectType&>(Value));
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Move

	template <class Type>
	AEON_FORCEINLINE constexpr decltype(auto) Move(Type&& Value) noexcept
	{
#if 0
		if constexpr (VIsRefWrapper<TDecayType<Type>>)
		{
			return static_cast<typename TDecayType<Type>::ObjectType&&>(Value);
		}
		else
		{
			return static_cast<TRemoveReference<Type>&&>(Value);
		}
#endif
		using ObjectType = TUnwrapRefObjectType<Type>;
		return static_cast<ObjectType&&>(Value);
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MoveElseCopy

	template <class Type>
	AEON_FORCEINLINE constexpr decltype(auto) MoveElseCopy(Type&& Value) noexcept
	{
		if constexpr (VIsMoveable<TUnwrapRefObjectType<Type>>)
		{
			return Move(Value);
		}
		else
		{
			return Copy(Value);

		}
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Forward

	// CHECKME should Forward be RefWrapper-aware?

	template <class Type>
	AEON_FORCEINLINE constexpr decltype(auto) Forward(TRemoveReference<Type>& Value) noexcept
	{
		return static_cast<Type&&>(Value);
	}

	template <class Type>
	AEON_FORCEINLINE constexpr decltype(auto) Forward(TRemoveReference<Type>&& Value) noexcept
	requires CNotLvalueReference<Type>
	{
		// NOTE The constraint would be violated if Forward was parameterized with an lvalue-reference, and an rvalue(-reference) was passed instead.
		//  An rvalue(-reference) must never be cast to an lvalue-reference.
		return static_cast<Type&&>(Value);
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// LRef, RRef

	template <class ObjType>
	GLvalRefWrapper<TDecayType<ObjType>> LRef(ObjType&& Object)
	{
		return { Forward<ObjType>(Object) };
	}

	template <class ObjType>
	GRvalRefWrapper<TDecayType<ObjType>> RRef(ObjType&& Object)
	{
		return { Forward<ObjType>(Object) };
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  AsConst, AsVolatile, AsConstVolatile

	template <class Type>
	AEON_FORCEINLINE constexpr TAddLvalueReference<TAddConst<Type>> AsConst(Type& Value) noexcept
	{
		return Value;
	}

	template <class Type>
	AEON_FORCEINLINE constexpr TAddLvalueReference<TAddVolatile<Type>> AsVolatile(Type& Value) noexcept
	{
		return Value;
	}

	template <class Type>
	AEON_FORCEINLINE constexpr TAddLvalueReference<TAddConstVolatile<Type>> AsConstVolatile(Type& Value) noexcept
	{
		return Value;
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Exchange

	template <CObject Type, class NewType>
	constexpr Type Exchange(Type& Value, NewType&& NewValue)
	requires CMoveableElseCopyable<Type> && CConvertibleClass<NewType, Type>
	{
		Type OldValue = MoveElseCopy(Value);
		Value = Forward<NewType>(NewValue);
		return MoveElseCopy(OldValue); // FIXME This is a pessimization, but we need to force it for copy-only types
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Swap

// Also see https://stackoverflow.com/questions/53495848

	template <CObject Type>
	constexpr void Swap(Type& Value1, Type& Value2)
	requires CMoveableElseCopyable<Type>
	{
		Value2 = Exchange(Value1, MoveElseCopy(Value2));
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GScopeObjectGuard

	template <CScalarObject ObjectType>
	requires CMoveableElseCopyable<ObjectType> && CNotConst<ObjectType>
	class GScopeObjectGuard // TODO better name
	{
	public:
		template <class NewObjectType>
		explicit GScopeObjectGuard(ObjectType& NewObjectRef, NewObjectType&& NewObjectValue)
		requires CSameClass<ObjectType, TDecayType<NewObjectType>>
			: GuardedObjectValue(MoveElseCopy(NewObjectRef))
			, ObjectRef(NewObjectRef)
		{
			ObjectRef = MoveElseCopy(NewObjectValue);
		}
		~GScopeObjectGuard()
		{
			ObjectRef = MoveElseCopy(GuardedObjectValue);
		}

		GScopeObjectGuard(GScopeObjectGuard const&) = delete;
		GScopeObjectGuard(GScopeObjectGuard&&) = delete;

		GScopeObjectGuard& operator=(GScopeObjectGuard const&) = delete;
		GScopeObjectGuard& operator=(GScopeObjectGuard&&) = delete;

	private:
		ObjectType GuardedObjectValue;
		[[maybe_unused]]
		ObjectType& ObjectRef;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// FNV1aHash

	// FNV-1a hashing algorithm
	// https://en.wikipedia.org/wiki/Fowler%E2%80%93Noll%E2%80%93Vo_hash_function#FNV-1a_hash

	template <CUnsignedInteger IntType>
	struct ZzInternal_TFNV1aHashPrime;
	template <>
	struct ZzInternal_TFNV1aHashPrime<uint32>
	{
		static constexpr uint32 Value = 0x0100'0193;
	};
	template <>
	struct ZzInternal_TFNV1aHashPrime<uint64>
	{
		static constexpr uint64 Value = 0x0000'0100'0000'01B3;
	};

	template <CUnsignedInteger IntType>
	struct ZzInternal_TFNV1aHashBasis;
	template <>
	struct ZzInternal_TFNV1aHashBasis<uint32>
	{
		static constexpr uint32 Value = 0x811C'9DC5;
	};
	template <>
	struct ZzInternal_TFNV1aHashBasis<uint64>
	{
		static constexpr uint64 Value = 0xCBF2'9CE4'8422'2325;
	};

	template <CUnsignedInteger IntType>
	constexpr IntType FNV1aHash(cbyteptr DataPtr, szint DataSize, IntType HashInit = ZzInternal_TFNV1aHashBasis<IntType>::Value) noexcept
	{
		constexpr IntType FNVPrime = ZzInternal_TFNV1aHashPrime<IntType>::Value;

		IntType Hash = HashInit;

		while (DataSize-- > 0)
		{
			// This looks weird, but it is correct,
			// it only XORs the lowest byte (as the rest are zero)
			IntType DataByte = static_cast<IntType>(*DataPtr++);
			Hash ^= DataByte;
			Hash *= FNVPrime;
		}

		return Hash;
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// AEON_MAKE_SOURCE_FILE_HASH, AEON_MAKE_SOURCE_LINE_HASH

	namespace ZzInternal
	{
		template <szint SourceInfoStrLenValue>
		consteval uint64 ComputeSourceHash(char const(& SourceInfoStr)[SourceInfoStrLenValue], uint64 LineInfo)
		{
			constexpr uint64 SourceInfoStrLen = SourceInfoStrLenValue - 1;
			constexpr uint64 DataSize = SourceInfoStrLen + sizeof LineInfo;

			byte DataBytes[DataSize];

			uint64 DataIdx = 0;
			while (DataIdx < SourceInfoStrLen)
			{
				DataBytes[DataIdx] = static_cast<byte>(SourceInfoStr[DataIdx]);
				++DataIdx;
			}

			// Endianness doesn't matter, we just want to hash these bytes
			DataBytes[DataIdx++] = static_cast<byte>(LineInfo >> 0x00);
			DataBytes[DataIdx++] = static_cast<byte>(LineInfo >> 0x08);
			DataBytes[DataIdx++] = static_cast<byte>(LineInfo >> 0x10);
			DataBytes[DataIdx++] = static_cast<byte>(LineInfo >> 0x18);
			DataBytes[DataIdx++] = static_cast<byte>(LineInfo >> 0x20);
			DataBytes[DataIdx++] = static_cast<byte>(LineInfo >> 0x28);
			DataBytes[DataIdx++] = static_cast<byte>(LineInfo >> 0x30);
			DataBytes[DataIdx  ] = static_cast<byte>(LineInfo >> 0x38);

			return Aeon::FNV1aHash<uint64>(DataBytes, DataSize);
		}
	}

#define AEON_MAKE_SOURCE_FILE_HASH() \
	::Aeon::ZzInternal::ComputeSourceHash(__FILE__, 0)

#define AEON_MAKE_SOURCE_LINE_HASH() \
	::Aeon::ZzInternal::ComputeSourceHash(__DATE__ __TIME__ __FILE__, __LINE__)

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CObjectHasher, CObjectHasherForObject

	template <class Type>
	concept CObjectHasher = requires
	{
		typename Type::ObjectType;
		requires CScalarObject<typename Type::ObjectType> && CDecayed<typename Type::ObjectType>;

		typename Type::HashType;
		requires CUnsignedInteger<typename Type::HashType> && CDecayed<typename Type::ObjectType>;

		requires requires (typename Type::ObjectType const Object)
		{ { Type::Hash(Object) } -> CSameClass<typename Type::HashType>; };
	};

	template <class Type, class ObjectType>
	concept CObjectHasherForObject = requires
	{
		requires CObjectHasher<Type>;

		requires CSameClass<typename Type::ObjectType, ObjectType>;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GDefaultObjectHasher

	// CHECKME benchmark this, compare with xxHash3

	template <CScalarObject ObjType, CUnsignedInteger HshType>
	struct GDefaultObjectHasher;

	template <CScalarObject ObjectType, CUnsignedInteger HashType, bool = VIsTriviallyHashable<ObjectType>>
	struct ZzInternal_GDefaultObjectHasherBase
	{
		AEON_FORCEINLINE static HashType Hash(ObjectType const& Object)
		{
			return FNV1aHash<HashType>(reinterpret_cast<cbyteptr>(addressof(Object)), sizeof(Object));
		}
	};
	template <CScalarObject ObjectType, CUnsignedInteger HashType>
	struct ZzInternal_GDefaultObjectHasherBase<ObjectType, HashType, false>
	{
		[[deprecated("'Aeon::GDefaultObjectHasher<ObjectType, HashType>::Hash(ObjectType const&)': "
					 "Invoking base Hash method for a non-trivially-hashable ObjectType: "
					 "this might result in different hashes for objects that compare equal; "
					 "specialize GDefaultObjectHasher for ObjectType to ensure that only bytes "
					 "that are part of the object representation are hashed.")]]
		AEON_FORCEINLINE static HashType Hash(ObjectType const& Object)
		{
			return FNV1aHash<HashType>(reinterpret_cast<cbyteptr>(addressof(Object)), sizeof(Object));
		}
	};

	template <>
	struct ZzInternal_GDefaultObjectHasherBase<uint8, uint8, true>
	{
		AEON_FORCEINLINE static uint8 Hash(uint8 const& Object) { return Object; }
	};
	template <>
	struct ZzInternal_GDefaultObjectHasherBase<uint16, uint16, true>
	{
		AEON_FORCEINLINE static uint16 Hash(uint16 const& Object) { return Object; }
	};
	template <>
	struct ZzInternal_GDefaultObjectHasherBase<uint32, uint32, true>
	{
		AEON_FORCEINLINE static uint32 Hash(uint32 const& Object) { return Object; }
	};
	template <>
	struct ZzInternal_GDefaultObjectHasherBase<uint64, uint64, true>
	{
		AEON_FORCEINLINE static uint64 Hash(uint64 const& Object) { return Object; }
	};

	template <>
	struct ZzInternal_GDefaultObjectHasherBase<int8, uint8, true>
	{
		AEON_FORCEINLINE static uint8 Hash(int8 const& Object) { return bit_cast<uint8>(Object); }
	};
	template <>
	struct ZzInternal_GDefaultObjectHasherBase<int16, uint16, true>
	{
		AEON_FORCEINLINE static uint16 Hash(int16 const& Object) { return bit_cast<uint16>(Object); }
	};
	template <>
	struct ZzInternal_GDefaultObjectHasherBase<int32, uint32, true>
	{
		AEON_FORCEINLINE static uint32 Hash(int32 const& Object) { return bit_cast<uint32>(Object); }
	};
	template <>
	struct ZzInternal_GDefaultObjectHasherBase<int64, uint64, true>
	{
		AEON_FORCEINLINE static uint64 Hash(int64 const& Object) { return bit_cast<uint64>(Object); }
	};

	template <>
	struct ZzInternal_GDefaultObjectHasherBase<float32, uint32, true>
	{
		AEON_FORCEINLINE static uint32 Hash(float32 const& Object) { return bit_cast<uint32>(Object); }
	};
	template <>
	struct ZzInternal_GDefaultObjectHasherBase<float64, uint64, true>
	{
		AEON_FORCEINLINE static uint64 Hash(float64 const& Object) { return bit_cast<uint64>(Object); }
	};

	template <class ObjType>
	requires (sizeof(ObjType*) == sizeof(uint32))
	struct ZzInternal_GDefaultObjectHasherBase<ObjType*, uint32, true>
	{
		AEON_FORCEINLINE static uint32 Hash(ObjType* const& Object) { return bit_cast<uint32>(Object); }
	};

	template <class ObjType>
	requires (sizeof(ObjType*) == sizeof(uint64))
	struct ZzInternal_GDefaultObjectHasherBase<ObjType*, uint64, true>
	{
		AEON_FORCEINLINE static uint64 Hash(ObjType* const& Object) { return bit_cast<uint64>(Object); }
	};

	template <CScalarObject ObjType, CUnsignedInteger HshType = uint32>
	struct GDefaultObjectHasher : public ZzInternal_GDefaultObjectHasherBase<ObjType, HshType>
	{
		using ObjectType = ObjType;
		using HashType = HshType;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// HasAllFlags, HasAnyFlags, HasNoneFlags

	// TODO maybe extend this to all enums and use TEnumUnderlyingType to operate on the values

	template <class Type>
	concept CFlagObject = CInteger<Type> || CFlagEnum<Type>;

	template <CFlagObject FlagType, CFlagObject ... FlagTypes>
	AEON_FORCEINLINE constexpr bool HasAllFlags(FlagType FlagObject, FlagTypes... FlagValues)
	requires (... && CSameClass<FlagType, FlagTypes>)
	{
		return (FlagObject & (0x0 | ... | FlagValues)) == (0x0 | ... | FlagValues);
	}

	template <CFlagObject FlagType, CFlagObject ... FlagTypes>
	AEON_FORCEINLINE constexpr bool HasAnyFlags(FlagType FlagObject, FlagTypes... FlagValues)
	requires (... && CSameClass<FlagType, FlagTypes>)
	{
		return (FlagObject & (0x0 | ... | FlagValues)) != 0x0;
	}

	template <CFlagObject FlagType, CFlagObject ... FlagTypes>
	AEON_FORCEINLINE constexpr bool HasNoneFlags(FlagType FlagObject, FlagTypes... FlagValues)
	requires (... && CSameClass<FlagType, FlagTypes>)
	{
		return (FlagObject & (0x0 | ... | FlagValues)) == 0x0;
	}

	template <CFlagObject FlagType>
	AEON_FORCEINLINE constexpr bool HasFlag(FlagType FlagObject, FlagType FlagValue)
	{
		return HasAnyFlags(FlagObject, FlagValue);
	}
}

#endif