#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CORE_COREBASICTYPES
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CORE_COREBASICTYPES

#include "CoreStandardLibrary.hpp"
#include "CorePlatformLibrary.hpp"

// DONE rename include guards to be ZZINTERNAL_AEON_INCLUDE_<path> (also need to change file templates)
// DONE rename internal macros to be ZZINTERNAL_AEON_<name>
// DONE rename Internal namespace to ZzInternal
// DONE either rename _Internal types to include ZzInternal_ at the start of the name,
//  or move them to ZzInternal (and rename them to whatever where adequate)

// TODO maybe change naming rules for protected/private members to exclude _Internal from the name
//  idea: prefix I[name] for inheritable (protected) members, and P[name] for private members,
//   - justification: P is fine as it isn't used anywhere else, and while I is used, it matches the meaning somewhat (interfaces)

// TODO have better specified rules about naming types (aliases) and static constexpr values declared inside classes / class templates;
//  maybe use T and V prefixes (to match type prefixes at namespace scope)?

// TODO document template parameter naming conventions: suffix 'Type' for type params, suffix 'Value' for non-type params,
//  suffix 'Template' for template params;
//  consider whether we might want to change these rules depending on the rules about aliases (et al) inside classes / class templates,
//  ideally making it so they never clash

// TODO add any general documentation about the library here

//
//	=== Aeon naming conventions ===
//
//	Type prefixes:
//
//	T[name] -> Type traits (empty class, class template, type alias)
//		- Metaprogramming types should follow this rule;
//		- Tag-like classes should also follow this rule;
//	V[name] -> Type traits' values (inline/static constant object)
//		- Metaprogramming objects should follow this rule;
//	C[name] -> Type constraints (concept)
//		- Concepts should follow this rule;
//	E[name] -> Enumerations (enum struct / struct::enum)
//		- Scoped enums and structs enclosing unscoped enums should follow this rule;
//		- Boolean-like and flag-like enums should also follow this rule; // TODO add note about what these are
//		- Note: Avoid unscoped enum at namespace scope;
//	I[name] -> Interfaces (abstract class / class template)
//		- Any class / class template with pure virtual methods should follow this rule;
//	G[name] -> Generic classes (class template)
//		- Class templates and partial specializations of class templates should follow this rule;
//	S[name] -> Specific classes (class)
//		- Classes and full specializations of class templates should follow this rule;
//	D[name] -> Data structures (aggregate class)
//		- Collections of data should follow this rule;
//	H[name] -> Handle types (special class)
//		- Aliases of GOpaque<Class> for classes abstracting some handle type should follow this rule;
//	Note: Namespace scope functions and constants require no prefix.
//
//	Avoid declaring namespace scope variables.
//
namespace Aeon { namespace ZzInternal {} }

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Common type aliases

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Fundamental types

// Void type
// using void;

// Boolean type
// using bool;

// Byte type
using std::byte;

static_assert(sizeof(byte) == 1);

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Character types

// Notes:
// 	Prepend 'u8' to a literal to produce an utf8char string literal, e.g., u8"Hello World"
//  Prepend 'u' to a literal to produce an utf16char string literal, e.g., u"Hello World"
//	Prepend 'U' to a literal to produce an utf32char string literal, e.g., U"Hello World"
//	Prepend 'L' to a literal to produce a wchar string literal, e.g., L"Hello World"

// Create a string literal as a UTF-8 encoded string literal
#define u8str(Str) u8 ## Str
// Create a string literal as a UTF-16 encoded string literal
#define u16str(Str) u ## Str
// Create a string literal as a UTF-32 encoded string literal
#define u32str(Str) U ## Str
// Create a string literal as a wide character string literal
#define wstr(Str) L ## Str

// Narrow character type;
//  May be used to represent single or multibyte (e.g. ASCII or UTF-8, respectively) character strings
//using char;

// UTF-8 character type
using utf8char = char8_t;
// UTF-16 character type
using utf16char = char16_t;
// UTF-32 character type
using utf32char = char32_t;

// Wide character type
using wchar = wchar_t;

#if AEON_PLATFORM_WINDOWS
static_assert(sizeof(utf16char) == sizeof(wchar));
#elif AEON_PLATFORM_LINUX
static_assert(sizeof(utf32char) == sizeof(wchar));
#else
// TODO other platforms
#error Not implemented yet!
#endif

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Integer types

// Signed 8-bit integer type
using int8 = std::int8_t;
// Signed 16-bit integer type
using int16 = std::int16_t;
// Signed 32-bit integer type
using int32 = std::int32_t;
// Signed 64-bit integer type
using int64 = std::int64_t;

// Unsigned 8-bit integer type
using uint8 = std::uint8_t;
// Unsigned 16-bit integer type
using uint16 = std::uint16_t;
// Unsigned 32-bit integer type
using uint32 = std::uint32_t;
// Unsigned 64-bit integer type
using uint64 = std::uint64_t;

namespace Aeon::ZzInternal
{
	template <bool SignednessValue, std::endian EndiannessValue>
	struct [[deprecated]] GDoubleQuadWordInteger;

	// FIX ME should signed 128-bits use unsigned types internally? both High and Low, or just Low?

	template <>
	struct [[deprecated]] GDoubleQuadWordInteger<true, std::endian::little>
	{
		uint64 StorageLo;
		int64 StorageHi;
	};
	template <>
	struct [[deprecated]] GDoubleQuadWordInteger<true, std::endian::big>
	{
		int64 StorageHi;
		uint64 StorageLo;
	};

	template <>
	struct [[deprecated]] GDoubleQuadWordInteger<false, std::endian::little>
	{
		uint64 StorageLo;
		uint64 StorageHi;
	};
	template <>
	struct [[deprecated]] GDoubleQuadWordInteger<false, std::endian::big>
	{
		uint64 StorageHi;
		uint64 StorageLo;
	};
}

// TODO implement this if necessary

// Signed 128-bit integer type
//using int128 = Aeon::ZzInternal::GDoubleQuadWordInteger<true, std::endian::native>;
// Unsigned 128-bit integer type
//using uint128 = Aeon::ZzInternal::GDoubleQuadWordInteger<false, std::endian::native>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Floating-point types

// Single precision floating-point number type
using float32 = float;
// Double precision floating-point number type
using float64 = double;

// Extended double precision floating-point number type
// Note: might be the same as float64 in some platforms, e.g., Windows
using exdfloat = long double;

namespace Aeon::ZzInternal
{
	inline constexpr bool VIsExtendedDoublePrecisionFloatSupported = sizeof(exdfloat) == sizeof(float64) + 2;

	struct [[deprecated]] SHalfPrecisionFloatingPoint
	{
		uint16 Storage;
	};
}

// TODO implement this if necessary

// Half precision floating-point number type
//using float16 = Aeon::ZzInternal::SHalfPrecisionFloatingPoint;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Utility integer types

// Unsigned integer type suitable for pointer representation, and unidirectional indexing
using ptrint = std::uintptr_t;

// Signed integer type suitable for pointer computation and comparison, and bidirectional indexing
// > The bit width of std::ptrdiff_t is not less than 17. (since C++11)
using diffint = std::ptrdiff_t;

// Unsigned integer type suitable for object size representation, and unidirectional indexing
// > The bit width of std::size_t is not less than 16. (since C++11)
using szint = std::size_t;

// Unsigned integer type suitable for object alignment representation
using algnint = std::align_val_t;

static_assert(sizeof(ptrint) == sizeof(void*));
static_assert(sizeof(diffint) > sizeof(uint16));
static_assert(sizeof(szint) >= sizeof(uint16));
static_assert(sizeof(algnint) >= sizeof(void*) && std::is_same_v<std::underlying_type_t<algnint>, szint>);

// Unsigned integer type appropriate for user-literal representation; usually the largest unsigned integer type
using ltrint = unsigned long long;
// Floating-point type appropriate for user-literal representation; usually the largest floating-point type
using ltrfloat = long double;

// Signed integer type suitable for intermediate floating-point rounded representation
// NOTE No integer type is actually suitable for representation of floating-point numbers, but for certain situations, this suffices
using fltint = int64;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Pointer types

// Null pointer type
// NOTE nullptr is a mono-state type convertible to any pointer type (thus NOT a pointer type per se)
using nullptr_type = std::nullptr_t;

// Void pointer type
using voidptr = void*;
// Const void pointer type
using cvoidptr = void const*;
// Volatile void pointer type
using vvoidptr = void volatile*;
// Const-volatile void pointer type
using cvvoidptr = void const volatile*;

// Byte pointer type
using byteptr = byte*;
// Const byte pointer type
using cbyteptr = byte const*;
// Volatile byte pointer type
using vbyteptr = byte volatile*;
// Const-volatile byte pointer type
using cvbyteptr = byte const volatile*;

// Miscellaneous types

// Thread identifier type
using thread_id = std::thread::id;

// C Standard calendar (broken-down) time type
//using cstdtime = std::tm;

// C Standard input/output text stream type
//using cstdstream = std::FILE;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Relational operator types

using std::strong_ordering;
using std::weak_ordering;
using std::partial_ordering;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Common tag types

// Tag type to select noexcept function overloads
using noexcept_tag = std::nothrow_t;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Common type literals

inline constexpr int8 operator""_i8(ltrint Literal) noexcept
{
    return int8(Literal);
}
inline constexpr int16 operator""_i16(ltrint Literal) noexcept
{
    return int16(Literal);
}
inline constexpr int32 operator""_i32(ltrint Literal) noexcept
{
    return int32(Literal);
}
inline constexpr int64 operator""_i64(ltrint Literal) noexcept
{
    return int64(Literal);
}

inline constexpr uint8 operator""_u8(ltrint Literal) noexcept
{
    return uint8(Literal);
}
inline constexpr uint16 operator""_u16(ltrint Literal) noexcept
{
    return uint16(Literal);
}
inline constexpr uint32 operator""_u32(ltrint Literal) noexcept
{
    return uint32(Literal);
}
inline constexpr uint64 operator""_u64(ltrint Literal) noexcept
{
    return uint64(Literal);
}

inline constexpr float32 operator""_f32(ltrfloat Literal) noexcept
{
    return float32(Literal);
}
inline constexpr float64 operator""_f64(ltrfloat Literal) noexcept
{
    return float64(Literal);
}

inline constexpr szint operator""_sz(ltrint Literal) noexcept
{
    return szint(Literal);
}
inline constexpr algnint operator""_algn(ltrint Literal) noexcept
{
    return algnint(Literal);
}

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// EBasicColor

	// TODO maybe move this elsewhere

	enum struct EBasicColor		: uint16
	{
//		Format:
//		ColorName				= 0xRGBc,
//		 where R,G,B contain the (compressed) values of the respective channels,
//		 and c is a number that identifies the color

//	Monochrome

		Black 					= 0x0000, // #000000
		Gray 					= 0x8881, // #808080
		Silver 					= 0xCCC2, // #C0C0C0
		White 					= 0xFFF3, // #FFFFFF

//	Primary

		Red 					= 0xF004, // #FF0000
		Green 					= 0x0F05, // #00FF00
		Blue 					= 0x00F6, // #0000FF

//	Secondary

		Yellow 					= 0xFF07, // #FFFF00
		Cyan 					= 0x0FF8, // #00FFFF
		Magenta 				= 0xF0F9, // #FF00FF

//	Tertiary

		Orange 					= 0xF70A, // #FF7F00
		Chartreuse 				= 0x7F0B, // #7FFF00
		Spring 					= 0x0F7C, // #00FF7F
		Azure 					= 0x07FD, // #007FFF
		Violet 					= 0x70FE, // #7F00FF
		Rose 					= 0xF07F, // #FF007F
	};
}
#endif
