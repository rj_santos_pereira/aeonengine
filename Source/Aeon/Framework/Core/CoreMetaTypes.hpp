#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CORE_COREMETATYPES
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CORE_COREMETATYPES

#include "CoreBasicTypes.hpp"

// Detects whether the function call occurs within a constant-evaluated context.
#if __cplusplus > 202002L
#	define if_consteval 		if consteval
#	define if_not_consteval 	if !consteval
#else
#	define if_consteval 		if (::std::is_constant_evaluated())
#	define if_not_consteval 	if (!::std::is_constant_evaluated())
#endif

namespace Aeon
{
	using ::std::declval;
// Utility function declaration to simplify obtaining not easily constructible types in unevaluated contexts.
// Must not be odr-used, otherwise the program is ill-formed, no diagnostic required.
// Introducing it into the global namespace to simulate a language construct (for symmetry with decltype).
#define declval(Type) ::Aeon::declval<Type>()

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TClass, TObject

	// Identity wrappers for compile-time types and values, respectively

	template <class ClassType>
	struct TClass
	{
		using Type = ClassType;
		
	//	static constexpr Type Value = Type{};
	};

	template <auto ObjectValue>
	struct TObject
	{
		using Type = decltype(ObjectValue);

		static constexpr Type Value = ObjectValue;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TTypeTrait, TValueTrait

	template <class ClassType>
	using TTypeTrait = TClass<ClassType>;

	template <auto ObjectValue>
	using TValueTrait = TObject<ObjectValue>;

	using TFalseConstant = TValueTrait<false>; // Type = bool
	using TTrueConstant = TValueTrait<true>; // Type = bool

	using TZeroConstant = TValueTrait<0>; // Type = int32
	using TOneConstant = TValueTrait<1>; // Type = int32

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TAlwaysTrue, TAlwaysFalse

	template <class Type>
	struct TAlwaysTrue : TTrueConstant {};

	template <class Type>
	inline constexpr bool VAlwaysTrue = TAlwaysTrue<Type>::Value;

	template <class Type>
	struct TAlwaysFalse : TFalseConstant {};

	template <class Type>
	inline constexpr bool VAlwaysFalse = TAlwaysFalse<Type>::Value;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Tag types

	// NOTE This is intended to be an incomplete type
	struct TNullTypeTag;

	struct TEmptyTypeTag {};
	struct TInferTypeTag {};

	template <class Type>
	struct TIsNullTypeTag : TValueTrait<std::is_same_v<std::decay_t<Type>, TNullTypeTag>> {};
	template <class Type>
	inline constexpr bool VIsNullTypeTag = TIsNullTypeTag<Type>::Value;

	template <class Type>
	struct TIsEmptyTypeTag : TValueTrait<std::is_same_v<std::decay_t<Type>, TEmptyTypeTag>> {};
	template <class Type>
	inline constexpr bool VIsEmptyTypeTag = TIsEmptyTypeTag<Type>::Value;

	template <class Type>
	struct TIsInferTypeTag : TValueTrait<std::is_same_v<std::decay_t<Type>, TInferTypeTag>> {};
	template <class Type>
	inline constexpr bool VIsInferTypeTag = TIsInferTypeTag<Type>::Value;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TCheckWellFormed

	// Helpers for metaprogramming techniques using SFINAE, equivalent to std::void_t
	// For more info, check: https://stackoverflow.com/questions/27687389

	template <class ...>
	using TCheckWellFormed = void;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TEnableIf

	template <bool ConditionValue, class EnableType>
	struct ZzInternal_TEnableIf
	{
	};
	template <class EnableType>
	struct ZzInternal_TEnableIf<true, EnableType>
	{
		using Type = EnableType;
	};

	template <bool ConditionValue, class EnableType = void>
	using TEnableIf = typename ZzInternal_TEnableIf<ConditionValue, EnableType>::Type;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TSelectIf

	template <bool ConditionValue, class TrueType, class FalseType>
	struct ZzInternal_TSelectIf
	{
		using Type = FalseType;
	};
	template <class TrueType, class FalseType>
	struct ZzInternal_TSelectIf<true, TrueType, FalseType>
	{
		using Type = TrueType;
	};

	template <bool ConditionValue, class TrueType, class FalseType>
	using TSelectIf = typename ZzInternal_TSelectIf<ConditionValue, TrueType, FalseType>::Type;

#if 1
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TTraitIf - identical to TSelectIf, but simulates language construct

#define AEON_TRAIT_IF(...) typename ::Aeon::TTraitIf<bool( __VA_ARGS__ )>::template Then
#define AEON_TRAIT_ELSEIF(...) ::template ElseIf<bool( __VA_ARGS__ )>::template Then
#define AEON_TRAIT_ELSE ::template Else
#define AEON_TRAIT_ENDIF ::EndIf

	template <bool ConditionValue, class BranchType>
	struct ZzInternal_TEvaluateTraitIf;

	template <bool ConditionValue>
	struct ZzInternal_TTraitIfBranch
	{
		template <class BranchType>
		using Then = ZzInternal_TEvaluateTraitIf<ConditionValue, BranchType>;
	};

	template <class BranchType>
	struct ZzInternal_TIgnoredTraitIfBranch
	{
		template <class NullBranchType>
		using Then = ZzInternal_TEvaluateTraitIf<true, BranchType>;
	};

	template <class BranchType>
	struct ZzInternal_TFinalTraitIfBranch
	{
		using EndIf = BranchType;
	};

	template <bool, class NullBranchType>
	struct ZzInternal_TEvaluateTraitIf
	{
		template <bool ConditionValue>
		using ElseIf = ZzInternal_TTraitIfBranch<ConditionValue>;

		template <class BranchType>
		using Else = ZzInternal_TFinalTraitIfBranch<BranchType>;

		using EndIf = TNullTypeTag;
	};

	template <class BranchType>
	struct ZzInternal_TEvaluateTraitIf<true, BranchType>
	{
		template <bool NullConditionValue>
		using ElseIf = ZzInternal_TIgnoredTraitIfBranch<BranchType>;

		template <class NullBranchType>
		using Else = ZzInternal_TFinalTraitIfBranch<BranchType>;

		using EndIf = BranchType;
	};

	template <bool ConditionValue>
	using TTraitIf = ZzInternal_TTraitIfBranch<ConditionValue>;
#endif

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TNegation

	template <class Type>
	struct TNegation : TValueTrait<!bool(Type::Value)> {};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TConjunction

	template <bool, class ... Types>
	struct ZzInternal_TConjunction : TFalseConstant {};

	template <class Type, class ... Types>
	struct ZzInternal_TConjunction<true, Type, Types ...> : ZzInternal_TConjunction<bool(Type::Value), Types ...> {};

	template <>
	struct ZzInternal_TConjunction<true> : TTrueConstant {};

	template <class ... Types>
	struct TConjunction : ZzInternal_TConjunction<true, Types ...> {};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TDisjunction

	template <bool, class ... Types>
	struct ZzInternal_TDisjunction : TTrueConstant {};

	template <class Type, class ... Types>
	struct ZzInternal_TDisjunction<false, Type, Types ...> : ZzInternal_TDisjunction<bool(Type::Value), Types ...> {};

	template <>
	struct ZzInternal_TDisjunction<false> : TFalseConstant {};

	template <class ... Types>
	struct TDisjunction : ZzInternal_TDisjunction<false, Types ...> {};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsSameClass

	template <class Type1, class Type2>
	concept CSameClass = std::is_same<Type1, Type2>::value;
	template <class Type1, class Type2>
	concept CNotSameClass = !CSameClass<Type1, Type2>;

	template <class Type1, class Type2>
	using TIsSameClass = TValueTrait<CSameClass<Type1, Type2>>;
	template <class Type1, class Type2>
	inline constexpr bool VIsSameClass = CSameClass<Type1, Type2>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsSameClassTemplate

	template <class, class>
	struct ZzInternal_TIsSameClassTemplate : TFalseConstant {};

	template <template <class ...> class ClassTemplate, class ... ElementTypes1, class ... ElementTypes2>
	struct ZzInternal_TIsSameClassTemplate<ClassTemplate<ElementTypes1 ...>, ClassTemplate<ElementTypes2 ...>> : TTrueConstant {};

	template <class Type1, class Type2>
	concept CSameClassTemplate = ZzInternal_TIsSameClassTemplate<Type1, Type2>::Value;
	template <class Type1, class Type2>
	concept CNotSameClassTemplate = !CSameClassTemplate<Type1, Type2>;

	template <class Type1, class Type2>
	using TIsSameClassTemplate = TValueTrait<CSameClassTemplate<Type1, Type2>>;
	template <class Type1, class Type2>
	inline constexpr bool VIsSameClassTemplate = CSameClassTemplate<Type1, Type2>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CTypePack

	template <class ...>
	struct TTypePack;

	template <class Type>
	concept CTypePack = TIsSameClassTemplate<Type, TTypePack<>>::Value;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TTypePack

	template <CTypePack, CTypePack>
	struct ZzInternal_TAppendTypePackOperation;

	template <CTypePack, szint>
	struct ZzInternal_TPopFrontPackOperation;

	template <CTypePack, szint, CTypePack = TTypePack<>>
	struct ZzInternal_TPopBackPackOperation;

	template <CTypePack, CTypePack>
	struct ZzInternal_TReverseTypePackOperation;

	template <template <class> class, CTypePack, CTypePack>
	struct ZzInternal_TFilterTypePackOperation;

	template <template <class ...> class, CTypePack, CTypePack, CTypePack ...>
	struct ZzInternal_TMapTypePackOperation;

	template <template <class, class> class, class, CTypePack>
	struct ZzInternal_TFoldTypePackOperation;

	template <class ...>
	struct TTypePack
	{
		static constexpr szint Count = 0;

		template <class PackType>
		using Append = typename ZzInternal_TAppendTypePackOperation<TTypePack<>, PackType>::Result;

		template <class ... PushElementTypes>
		using PushFront = TTypePack<PushElementTypes ...>;
		template <class ... PushElementTypes>
		using PushBack = TTypePack<PushElementTypes ...>;

		template <szint PopCountValue>
		using PopFront = TTypePack<>;
		template <szint PopCountValue>
		using PopBack = TTypePack<>;

		using Reverse = TTypePack<>;

		template <template <class> class FuncTemplate>
		using Filter = TTypePack<>;
		template <template <class ...> class FuncTemplate, CTypePack ... OtherPackTypes>
		using Map = TTypePack<>;
		template <template <class, class> class FuncTemplate, class InitialType>
		using Fold = InitialType;

		using Front = TNullTypeTag;
		using Back = TNullTypeTag;

		template <szint IndexValue>
		using ElementAt = TNullTypeTag;
	};

	template <class Type>
	struct TTypePack<Type>
	{
		static constexpr szint Count = 1;

		template <class PackType>
		using Append = typename ZzInternal_TAppendTypePackOperation<TTypePack<Type>, PackType>::Result;

		template <class ... PushElementTypes>
		using PushFront = TTypePack<PushElementTypes ..., Type>;
		template <class ... PushElementTypes>
		using PushBack = TTypePack<Type, PushElementTypes ...>;

		template <szint PopCountValue>
		using PopFront = TSelectIf<bool(PopCountValue), TTypePack<>, TTypePack<Type>>;
		template <szint PopCountValue>
		using PopBack = TSelectIf<bool(PopCountValue), TTypePack<>, TTypePack<Type>>;

		using Reverse = TTypePack<Type>;

		template <template <class> class FuncTemplate>
		using Filter = typename ZzInternal_TFilterTypePackOperation<FuncTemplate, TTypePack<>,
																	TTypePack<Type>>::Result;
		template <template <class ...> class FuncTemplate, CTypePack ... OtherPackTypes>
		using Map = typename ZzInternal_TMapTypePackOperation<FuncTemplate, TTypePack<>,
															  TTypePack<Type>, OtherPackTypes ...>::Result;
		template <template <class, class> class FuncTemplate, class InitialType>
		using Fold = typename ZzInternal_TFoldTypePackOperation<FuncTemplate, InitialType,
																TTypePack<Type>>::Result;

		using Front = Type;
		using Back = Type;

		template <szint IndexValue>
		using ElementAt = typename PopFront<IndexValue>::Front;
	};

	template <class Type, class ... Types>
	struct TTypePack<Type, Types ...>
	{
		static constexpr szint Count = 1 + sizeof...(Types);

		template <class PackType>
		using Append = typename ZzInternal_TAppendTypePackOperation<TTypePack<Type, Types ...>, PackType>::Result;

		template <class ... PushElementTypes>
		using PushFront = TTypePack<PushElementTypes ..., Type, Types ...>;
		template <class ... PushElementTypes>
		using PushBack = TTypePack<Type, Types ..., PushElementTypes ...>;

		template <szint PopCountValue>
		using PopFront = typename ZzInternal_TPopFrontPackOperation<TTypePack<Type, Types ...>, PopCountValue>::Result;
		template <szint PopCountValue>
		using PopBack = typename ZzInternal_TPopBackPackOperation<TTypePack<Type, Types ...>,
																(Count > PopCountValue ? Count - PopCountValue : 0)>::Result;

		using Reverse = typename ZzInternal_TReverseTypePackOperation<TTypePack<Type, Types ...>, TTypePack<>>::Result;

		template <template <class> class FuncTemplate>
		using Filter = typename ZzInternal_TFilterTypePackOperation<FuncTemplate, TTypePack<>,
																	TTypePack<Type, Types ...>>::Result;
		template <template <class ...> class FuncTemplate, CTypePack ... OtherPackTypes>
		using Map = typename ZzInternal_TMapTypePackOperation<FuncTemplate, TTypePack<>,
															  TTypePack<Type, Types...>, OtherPackTypes ...>::Result;
		template <template <class, class> class FuncTemplate, class InitialType>
		using Fold = typename ZzInternal_TFoldTypePackOperation<FuncTemplate, InitialType,
																TTypePack<Type, Types ...>>::Result;

		using Front = Type;
		using Back = typename TTypePack<Types ...>::Back;

		template <szint IndexValue>
		using ElementAt = typename PopFront<IndexValue>::Front;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ZzInternal_TAppendTypePackOperation

	template <class ... OriginalTypes, class ... AppendedTypes>
	struct ZzInternal_TAppendTypePackOperation<TTypePack<OriginalTypes ...>, TTypePack<AppendedTypes ...>>
	{
		using Result = TTypePack<OriginalTypes ..., AppendedTypes ...>;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ZzInternal_TPopFrontPackOperation

	template <class HeadType, class ... TailTypes, szint PopCountValue>
	struct ZzInternal_TPopFrontPackOperation<TTypePack<HeadType, TailTypes ...>, PopCountValue>
	{
		using PoppedPack = TTypePack<TailTypes ...>;

		using Result = typename ZzInternal_TPopFrontPackOperation<PoppedPack, PopCountValue - 1>::Result;
	};
	template <class HeadType, szint PopCountValue>
	struct ZzInternal_TPopFrontPackOperation<TTypePack<HeadType>, PopCountValue>
	{
		using PoppedPack = TTypePack<>;

		using Result = typename ZzInternal_TPopFrontPackOperation<PoppedPack, PopCountValue - 1>::Result;
	};
	template <szint PopCountValue>
	struct ZzInternal_TPopFrontPackOperation<TTypePack<>, PopCountValue>
	{
		using PoppedPack = TTypePack<>;

		using Result = typename ZzInternal_TPopFrontPackOperation<PoppedPack, PopCountValue - 1>::Result;
	};

	// NOTE We need to specialize the base cases for each specialization of TypePack
	//  because we also specialized the general cases for each specialization of TypePack,
	//  thus the base cases must be "more specialized" than the general cases,
	//  otherwise they would cause an 'ambiguous partial specialization' error.
	template <class HeadType, class ... TailTypes>
	struct ZzInternal_TPopFrontPackOperation<TTypePack<HeadType, TailTypes ...>, 0>
	{
		using Result = TTypePack<HeadType, TailTypes ...>;
	};
	template <class HeadType>
	struct ZzInternal_TPopFrontPackOperation<TTypePack<HeadType>, 0>
	{
		using Result = TTypePack<HeadType>;
	};
	template <>
	struct ZzInternal_TPopFrontPackOperation<TTypePack<>, 0>
	{
		using Result = TTypePack<>;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ZzInternal_TPopBackPackOperation

	template <class HeadType, class ... TailTypes, szint PoppedPackCountValue, class ... RemainingTypes>
	struct ZzInternal_TPopBackPackOperation<TTypePack<HeadType, TailTypes ...>, PoppedPackCountValue, TTypePack<RemainingTypes ...>>
	{
		using WorkingPack = TTypePack<TailTypes ...>;
		using PoppedPack = TTypePack<RemainingTypes ..., HeadType>;

		using Result = typename ZzInternal_TPopBackPackOperation<WorkingPack, PoppedPackCountValue - 1, PoppedPack>::Result;
	};
	template <class HeadType, szint PoppedPackCountValue, class ... RemainingTypes>
	struct ZzInternal_TPopBackPackOperation<TTypePack<HeadType>, PoppedPackCountValue, TTypePack<RemainingTypes ...>>
	{
		using WorkingPack = TTypePack<>;
		using PoppedPack = TTypePack<RemainingTypes ..., HeadType>;

		using Result = typename ZzInternal_TPopBackPackOperation<WorkingPack, PoppedPackCountValue - 1, PoppedPack>::Result;
	};
	template <szint PoppedPackCountValue, class ... RemainingTypes>
	struct ZzInternal_TPopBackPackOperation<TTypePack<>, PoppedPackCountValue, TTypePack<RemainingTypes ...>>
	{
		using WorkingPack = TTypePack<>;
		using PoppedPack = TTypePack<RemainingTypes ...>;

		using Result = typename ZzInternal_TPopBackPackOperation<WorkingPack, PoppedPackCountValue - 1, PoppedPack>::Result;
	};

	// NOTE We need to specialize the base cases for each specialization of TypePack
	//  because we also specialized the general cases for each specialization of TypePack,
	//  thus the base cases must be "more specialized" than the general cases,
	//  otherwise they would cause an 'ambiguous partial specialization' error.
	template <class HeadType, class ... TailType, class ... RemainingTypes>
	struct ZzInternal_TPopBackPackOperation<TTypePack<HeadType, TailType ...>, 0, TTypePack<RemainingTypes ...>>
	{
		using Result = TTypePack<RemainingTypes ...>;
	};
	template <class HeadType, class ... RemainingTypes>
	struct ZzInternal_TPopBackPackOperation<TTypePack<HeadType>, 0, TTypePack<RemainingTypes ...>>
	{
		using Result = TTypePack<RemainingTypes ...>;
	};
	template <class ... RemainingTypes>
	struct ZzInternal_TPopBackPackOperation<TTypePack<>, 0, TTypePack<RemainingTypes ...>>
	{
		using Result = TTypePack<RemainingTypes ...>;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ZzInternal_TReverseTypePackOperation

	template <class HeadType, class ... TailTypes, class ... ReversedTypes>
	struct ZzInternal_TReverseTypePackOperation<TTypePack<HeadType, TailTypes ...>, TTypePack<ReversedTypes ...>>
	{
		using RemainingPack = TTypePack<TailTypes ...>;

		using ReversedPack = TTypePack<HeadType, ReversedTypes ...>;

		using Result = typename ZzInternal_TReverseTypePackOperation<RemainingPack, ReversedPack>::Result;
	};

	template <CTypePack ReversedPackType>
	struct ZzInternal_TReverseTypePackOperation<TTypePack<>, ReversedPackType>
	{
		using Result = ReversedPackType;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ZzInternal_TFilterTypePackOperation

	template <template <class> class FilterTemplate, CTypePack FilteredPackType, CTypePack ThisPackType>
	struct ZzInternal_TFilterTypePackOperation
	{
		using CurrentElement = typename ThisPackType::Front;
		using RemainingPack = typename ThisPackType::template PopFront<1>;

		using FilteredPack = TSelectIf<FilterTemplate<CurrentElement>::Value,
									   typename FilteredPackType::template PushBack<CurrentElement>,
									   FilteredPackType>;

		using Result = typename ZzInternal_TFilterTypePackOperation<FilterTemplate, FilteredPack, RemainingPack>::Result;
	};

	template <template <class> class FilterTemplate, CTypePack FilteredPackType>
	struct ZzInternal_TFilterTypePackOperation<FilterTemplate, FilteredPackType, TTypePack<>>
	{
		using Result = FilteredPackType;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ZzInternal_TMapTypePackOperation

	template <template <class ...> class MapTemplate, CTypePack MappedPackType, CTypePack ThisPackType, CTypePack ... PackTypes>
	struct ZzInternal_TMapTypePackOperation
	{
		static_assert((... && (ThisPackType::Count == PackTypes::Count)));

		using MappedElement = typename MapTemplate<typename ThisPackType::Front, typename PackTypes::Front ...>::Type;
		using MappedPack = typename MappedPackType::template PushBack<MappedElement>;

		using Result = typename ZzInternal_TMapTypePackOperation<MapTemplate, MappedPack,
																 typename ThisPackType::template PopFront<1>,
																 typename PackTypes::template PopFront<1> ...>::Result;
	};

	template <template <class ...> class MapTemplate, CTypePack MappedPackType, CTypePack ... PackTypes>
	struct ZzInternal_TMapTypePackOperation<MapTemplate, MappedPackType, TTypePack<>, PackTypes ...>
	{
		using Result = MappedPackType;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ZzInternal_TFoldTypePackOperation

	template <template <class, class> class FoldTemplate, class FoldedType, CTypePack ThisPackType>
	struct ZzInternal_TFoldTypePackOperation
	{
		using CurrentElement = typename ThisPackType::Front;
		using RemainingPack = typename ThisPackType::template PopFront<1>;

		using FoldedElement = typename FoldTemplate<FoldedType, CurrentElement>::Type;

		using Result = typename ZzInternal_TFoldTypePackOperation<FoldTemplate, FoldedElement, RemainingPack>::Result;
	};

	template <template <class, class> class FoldTemplate, class FoldedType>
	struct ZzInternal_TFoldTypePackOperation<FoldTemplate, FoldedType, TTypePack<>>
	{
		using Result = FoldedType;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TDecayType

	template <class Type>
	struct ZzInternal_TDecayType : TTypeTrait<std::decay_t<Type>> {};

	template <class Type>
	using TDecayType = typename ZzInternal_TDecayType<Type>::Type;

	template <class Type>
	concept CDecayed = CSameClass<Type, TDecayType<Type>>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TCommonType

	template <CTypePack, class = void>
	struct ZzInternal_TCommonType
	{
		using Type = TNullTypeTag;
	};

	template <class ... Types>
	struct ZzInternal_TCommonType<TTypePack<Types ...>, TCheckWellFormed<std::common_type_t<Types ...>>>
	{
		using Type = std::common_type_t<Types ...>;
	};

	template <class ... Types>
	using TCommonType = typename ZzInternal_TCommonType<TTypePack<Types ...>>::Type;

//	template <class CommonType>
//	struct ZzInternal_THasCommonType : TTrueConstant {};
//	template <>
//	struct ZzInternal_THasCommonType<void> : TFalseConstant {};
//
//	template <class ... Types>
//	using THasCommonType = ZzInternal_THasCommonType<TCommonType<Types ...>>;
//
//	template <class ... Types>
//	inline constexpr bool VHasCommonType = THasCommonType<Types ...>::Value;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsBaseClass

	template <class BaseType, class SubType>
	concept CBaseClass = std::is_base_of<BaseType, SubType>::value;
	template <class BaseType, class SubType>
	concept CNotBaseClass = !CBaseClass<BaseType, SubType>;

	template <class BaseType, class SubType>
	using TIsBaseClass = TValueTrait<CBaseClass<BaseType, SubType>>;
	template <class BaseType, class SubType>
	inline constexpr bool VIsBaseClass = CBaseClass<BaseType, SubType>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsConvertibleClass

	template <class FromType, class ToType>
	concept CConvertibleClass = std::is_convertible<FromType, ToType>::value;
	template <class FromType, class ToType>
	concept CNotConvertibleClass = !CConvertibleClass<FromType, ToType>;

	template <class FromType, class ToType>
	using TIsConvertibleClass = TValueTrait<CConvertibleClass<FromType, ToType>>;
	template <class FromType, class ToType>
	inline constexpr bool VIsConvertibleClass = CConvertibleClass<FromType, ToType>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsContextuallyConvertibleClass

	template <class FromType, class ToType>
	concept CContextuallyConvertibleClass = std::is_constructible<ToType, FromType>::value;
	template <class FromType, class ToType>
	concept CNotContextuallyConvertibleClass = !CContextuallyConvertibleClass<FromType, ToType>;

	template <class FromType, class ToType>
	using TIsContextuallyConvertibleClass = TValueTrait<CContextuallyConvertibleClass<FromType, ToType>>;
	template <class FromType, class ToType>
	inline constexpr bool VIsContextuallyConvertibleClass = CContextuallyConvertibleClass<FromType, ToType>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsVoid

	template <class Type>
	concept CVoid = std::is_void<Type>::value;
	template <class Type>
	concept CNotVoid = !CVoid<Type>;

	template <class Type>
	using TIsVoid = TValueTrait<CVoid<Type>>;
	template <class Type>
	inline constexpr bool VIsVoid = CVoid<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsByte

	template <class Type>
	concept CByte = std::is_same<std::remove_cv_t<Type>, byte>::value;
	template <class Type>
	concept CNotByte = !CByte<Type>;

	template <class Type>
	using TIsByte = TValueTrait<CByte<Type>>;
	template <class Type>
	inline constexpr bool VIsByte = CByte<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsBool

	template <class Type>
	concept CBool = std::is_same<std::remove_cv_t<Type>, bool>::value;
	template <class Type>
	concept CNotBool = !CBool<Type>;

	template <class Type>
	using TIsBool = TValueTrait<CBool<Type>>;
	template <class Type>
	inline constexpr bool VIsBool = CBool<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsCharacter

	template <class Type>
	concept CCharacter = std::disjunction<std::is_same<std::remove_cv_t<Type>, char>,
										  std::is_same<std::remove_cv_t<Type>, utf8char>,
										  std::is_same<std::remove_cv_t<Type>, utf16char>,
										  std::is_same<std::remove_cv_t<Type>, utf32char>,
										  std::is_same<std::remove_cv_t<Type>, wchar>>::value;
	template <class Type>
	concept CNotCharacter = !CCharacter<Type>;

	template <class Type>
	using TIsCharacter = TValueTrait<CCharacter<Type>>;
	template <class Type>
	inline constexpr bool VIsCharacter = CCharacter<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsInteger

	template <class Type>
	concept CInteger = std::disjunction<std::is_same<std::remove_cv_t<Type>, unsigned char>,
										std::is_same<std::remove_cv_t<Type>, signed char>,
										std::is_same<std::remove_cv_t<Type>, unsigned short>,
										std::is_same<std::remove_cv_t<Type>, signed short>,
										std::is_same<std::remove_cv_t<Type>, unsigned int>,
										std::is_same<std::remove_cv_t<Type>, signed int>,
										std::is_same<std::remove_cv_t<Type>, unsigned long>,
										std::is_same<std::remove_cv_t<Type>, signed long>,
										std::is_same<std::remove_cv_t<Type>, unsigned long long>,
										std::is_same<std::remove_cv_t<Type>, signed long long>,
//										std::is_same<std::remove_cv_t<Type>, uint128>,
//										std::is_same<std::remove_cv_t<Type>, int128>,
										std::is_same<std::remove_cv_t<Type>, ptrint>,
										std::is_same<std::remove_cv_t<Type>, szint>,
										std::is_same<std::remove_cv_t<Type>, diffint>,
										std::is_same<std::remove_cv_t<Type>, algnint>
										>::value;
	template <class Type>
	concept CNotInteger = !CInteger<Type>;

	template <class Type>
	using TIsInteger = TValueTrait<CInteger<Type>>;
	template <class Type>
	inline constexpr bool VIsInteger = CInteger<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsFloatingPoint

	template <class Type>
	concept CFloatingPoint = std::disjunction<std::is_same<std::remove_cv_t<Type>, float>,
											  std::is_same<std::remove_cv_t<Type>, double>,
											  std::is_same<std::remove_cv_t<Type>, long double>
											  >::value;
	template <class Type>
	concept CNotFloatingPoint = !CFloatingPoint<Type>;

	template <class Type>
	using TIsFloatingPoint = TValueTrait<CFloatingPoint<Type>>;
	template <class Type>
	inline constexpr bool VIsFloatingPoint = CFloatingPoint<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsNumeric

	template <class Type>
	concept CNumeric = CInteger<Type> || CFloatingPoint<Type>;
	template <class Type>
	concept CNotNumeric = !CNumeric<Type>;

	template <class Type>
	using TIsNumeric = TValueTrait<CNumeric<Type>>;
	template <class Type>
	inline constexpr bool VIsNumeric = CNumeric<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsArithmetic

	template <class Type>
	concept CArithmetic = CBool<Type> || CCharacter<Type> || CNumeric<Type>;
	template <class Type>
	concept CNotArithmetic = !CArithmetic<Type>;

	template <class Type>
	using TIsArithmetic = TValueTrait<CArithmetic<Type>>;
	template <class Type>
	inline constexpr bool VIsArithmetic = CArithmetic<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsPointer

	template <class Type>
	concept CPointer = std::is_pointer<Type>::value;
	template <class Type>
	concept CNotPointer = !CPointer<Type>;

	template <class Type>
	using TIsPointer = TValueTrait<CPointer<Type>>;
	template <class Type>
	inline constexpr bool VIsPointer = CPointer<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsNullPointer

	template <class Type>
	concept CNullPointer = std::is_null_pointer<Type>::value;
	template <class Type>
	concept CNotNullPointer = !CNullPointer<Type>;

	template <class Type>
	using TIsNullPointer = TValueTrait<CNullPointer<Type>>;
	template <class Type>
	inline constexpr bool VIsNullPointer = CNullPointer<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsVoidPointer

	// TODO maybe deprecate this

	template <class Type>
	concept CVoidPointer = std::conjunction<std::is_pointer<Type>,
											std::is_void<std::remove_pointer_t<Type>>>::value;
	template <class Type>
	concept CNotVoidPointer = !CVoidPointer<Type>;

	template <class Type>
	using TIsVoidPointer = TValueTrait<CVoidPointer<Type>>;
	template <class Type>
	inline constexpr bool VIsVoidPointer = CVoidPointer<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsFunctionPointer

	template <class Type>
	concept CFunctionPointer = std::conjunction<std::is_pointer<Type>,
												std::is_function<std::remove_pointer_t<Type>>>::value;
	template <class Type>
	concept CNotFunctionPointer = !CFunctionPointer<Type>;

	template <class Type>
	using TIsFunctionPointer = TValueTrait<CFunctionPointer<Type>>;
	template <class Type>
	inline constexpr bool VIsFunctionPointer = CFunctionPointer<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsObjectPointer

	template <class Type>
	concept CObjectPointer = std::conjunction<std::is_pointer<Type>,
											  std::is_object<std::remove_pointer_t<Type>>>::value;
	template <class Type>
	concept CNotObjectPointer = !CObjectPointer<Type>;

	template <class Type>
	using TIsObjectPointer = TValueTrait<CObjectPointer<Type>>;
	template <class Type>
	inline constexpr bool VIsObjectPointer = CObjectPointer<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsClassPointer

	template <class Type>
	concept CClassPointer = std::conjunction<std::is_pointer<Type>,
											 std::is_class<std::remove_pointer_t<Type>>>::value;
	template <class Type>
	concept CNotClassPointer = !CClassPointer<Type>;

	template <class Type>
	using TIsClassPointer = TValueTrait<CClassPointer<Type>>;
	template <class Type>
	inline constexpr bool VIsClassPointer = CClassPointer<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsMemberPointer

	template <class Type>
	concept CMemberPointer = std::is_member_pointer<Type>::value;
	template <class Type>
	concept CNotMemberPointer = !CMemberPointer<Type>;

	template <class Type>
	using TIsMemberPointer = TValueTrait<CMemberPointer<Type>>;
	template <class Type>
	inline constexpr bool VIsMemberPointer = CMemberPointer<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsMemberFunctionPointer

	template <class Type>
	concept CMemberFunctionPointer = std::is_member_function_pointer<Type>::value;
	template <class Type>
	concept CNotMemberFunctionPointer = !CMemberFunctionPointer<Type>;

	template <class Type>
	using TIsMemberFunctionPointer = TValueTrait<CMemberFunctionPointer<Type>>;
	template <class Type>
	inline constexpr bool VIsMemberFunctionPointer = CMemberFunctionPointer<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsMemberObjectPointer

	template <class Type>
	concept CMemberObjectPointer = std::is_member_object_pointer<Type>::value;
	template <class Type>
	concept CNotMemberObjectPointer = !CMemberObjectPointer<Type>;

	template <class Type>
	using TIsMemberObjectPointer = TValueTrait<CMemberObjectPointer<Type>>;
	template <class Type>
	inline constexpr bool VIsMemberObjectPointer = CMemberObjectPointer<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsMemberObjectPointerToClass

	template <class Type>
	struct ZzInternal_TIsMemberObjectPointerToClass : TFalseConstant {};

	template <class ObjType, class ClassType>
	struct ZzInternal_TIsMemberObjectPointerToClass<ObjType ClassType::*> : TValueTrait<std::is_class_v<std::decay_t<ObjType>>> {};

	template <class Type>
	concept CMemberObjectPointerToClass = ZzInternal_TIsMemberObjectPointerToClass<std::remove_cv_t<Type>>::Value;
	template <class Type>
	concept CNotMemberObjectPointerToClass = !CMemberObjectPointerToClass<Type>;

	template <class Type>
	using TIsMemberObjectPointerToClass = TValueTrait<CMemberObjectPointerToClass<Type>>;
	template <class Type>
	inline constexpr bool VIsMemberObjectPointerToClass = CMemberObjectPointerToClass<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsEnum

	template <class Type>
	concept CEnum = std::is_enum<Type>::value;
	template <class Type>
	concept CNotEnum = !CEnum<Type>;

	template <class Type>
	using TIsEnum = TValueTrait<CEnum<Type>>;
	template <class Type>
	inline constexpr bool VIsEnum = CEnum<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsBoolEnum

	// NOTE This is unique to AeonFramework

	template <CEnum EnumType>
	inline constexpr bool ZzInternal_IsBoolEnum(EnumType const&)
	{
		return false;
	}

	template <class, class = void>
	struct ZzInternal_TIsBoolEnum
		: TFalseConstant {};
	template <class Type>
	struct ZzInternal_TIsBoolEnum<Type, TCheckWellFormed<decltype(ZzInternal_IsBoolEnum(declval(Type)))>>
		: TValueTrait<ZzInternal_IsBoolEnum(Type{})> {};

	template <class Type>
	concept CBoolEnum = ZzInternal_TIsBoolEnum<Type>::Value;
	template <class Type>
	concept CNotBoolEnum = !CBoolEnum<Type>;

	template <class Type>
	using TIsBoolEnum = TValueTrait<CBoolEnum<Type>>;
	template <class Type>
	inline constexpr bool VIsBoolEnum = CBoolEnum<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsFlagEnum

	// NOTE This is unique to AeonFramework

	template <CEnum EnumType>
	inline constexpr bool ZzInternal_IsFlagEnum(EnumType const&)
	{
		return false;
	}

	template <class, class = void>
	struct ZzInternal_TIsFlagEnum
		: TFalseConstant {};
	template <class Type>
	struct ZzInternal_TIsFlagEnum<Type, TCheckWellFormed<decltype(ZzInternal_IsFlagEnum(declval(Type)))>>
		: TValueTrait<ZzInternal_IsFlagEnum(Type{})> {};

	template <class Type>
	concept CFlagEnum = ZzInternal_TIsFlagEnum<Type>::Value;
	template <class Type>
	concept CNotFlagEnum = !CFlagEnum<Type>;

	template <class Type>
	using TIsFlagEnum = TValueTrait<CFlagEnum<Type>>;
	template <class Type>
	inline constexpr bool VIsFlagEnum = CFlagEnum<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsClass

	template <class Type>
	concept CClass = std::is_class<Type>::value;
	template <class Type>
	concept CNotClass = !CClass<Type>;

	template <class Type>
	using TIsClass = TValueTrait<CClass<Type>>;
	template <class Type>
	inline constexpr bool VIsClass = CClass<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsUnion

	template <class Type>
	concept CUnion = std::is_union<Type>::value;
	template <class Type>
	concept CNotUnion = !CUnion<Type>;

	template <class Type>
	using TIsUnion = TValueTrait<CUnion<Type>>;
	template <class Type>
	inline constexpr bool VIsUnion = CUnion<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsScalar

	template <class Type>
	concept CScalar = CArithmetic<Type> || CPointer<Type> || CMemberPointer<Type> || CNullPointer<Type> || CEnum<Type>;
	template <class Type>
	concept CNotScalar = !CScalar<Type>;

	template <class Type>
	using TIsScalar = TValueTrait<CScalar<Type>>;
	template <class Type>
	inline constexpr bool VIsScalar = CScalar<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsArray, IsBoundedArray, IsUnboundedArray

	template <class Type>
	concept CArray = std::is_array<Type>::value;
	template <class Type>
	concept CNotArray = !CArray<Type>;

	template <class Type>
	using TIsArray = TValueTrait<CArray<Type>>;
	template <class Type>
	inline constexpr bool VIsArray = CArray<Type>;

	template <class Type>
	concept CBoundedArray = std::is_bounded_array<Type>::value;
	template <class Type>
	concept CNotBoundedArray = !CBoundedArray<Type>;

	template <class Type>
	using TIsBoundedArray = TValueTrait<CBoundedArray<Type>>;
	template <class Type>
	inline constexpr bool VIsBoundedArray = CBoundedArray<Type>;

	template <class Type>
	concept CUnboundedArray = std::is_unbounded_array<Type>::value;
	template <class Type>
	concept CNotUnboundedArray = !CUnboundedArray<Type>;

	template <class Type>
	using TIsUnboundedArray = TValueTrait<CUnboundedArray<Type>>;
	template <class Type>
	inline constexpr bool VIsUnboundedArray = CUnboundedArray<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsObject

//	template <class Type>
//	concept CObject = CScalar<Type> || CArray<Type> || CClass<Type> || CUnion<Type>;

	template <class Type>
	concept CObject = std::is_object<Type>::value;
	template <class Type>
	concept CNotObject = !CObject<Type>;

	template <class Type>
	using TIsObject = TValueTrait<CObject<Type>>;
	template <class Type>
	inline constexpr bool VIsObject = CObject<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsFunction

	template <class Type>
	concept CFunction = std::is_function<Type>::value;
	template <class Type>
	concept CNotFunction = !CFunction<Type>;

	template <class Type>
	using TIsFunction = TValueTrait<CFunction<Type>>;
	template <class Type>
	inline constexpr bool VIsFunction = CFunction<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsSimpleFunction

	template <class Type>
	struct ZzInternal_TIsSimpleFunction : TFalseConstant {};
	template <class RetType, class ... ArgTypes>
	struct ZzInternal_TIsSimpleFunction<RetType(ArgTypes ...)> : TTrueConstant {};

	template <class Type>
	concept CSimpleFunction = ZzInternal_TIsSimpleFunction<Type>::Value;
	template <class Type>
	concept CNotSimpleFunction = !CSimpleFunction<Type>;

	template <class Type>
	using TIsSimpleFunction = TValueTrait<CSimpleFunction<Type>>;
	template <class Type>
	inline constexpr bool VIsSimpleFunction = CSimpleFunction<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsReference, TIsLvalueReference, TIsRvalueReference

	template <class Type>
	concept CReference = std::is_reference<Type>::value;
	template <class Type>
	concept CNotReference = !CReference<Type>;

	template <class Type>
	using TIsReference = TValueTrait<CReference<Type>>;
	template <class Type>
	inline constexpr bool VIsReference = CReference<Type>;

	template <class Type>
	concept CLvalueReference = std::is_lvalue_reference<Type>::value;
	template <class Type>
	concept CNotLvalueReference = !CLvalueReference<Type>;

	template <class Type>
	using TIsLvalueReference = TValueTrait<CLvalueReference<Type>>;
	template <class Type>
	inline constexpr bool VIsLvalueReference = CLvalueReference<Type>;

	template <class Type>
	concept CRvalueReference = std::is_rvalue_reference<Type>::value;
	template <class Type>
	concept CNotRvalueReference = !CRvalueReference<Type>;

	template <class Type>
	using TIsRvalueReference = TValueTrait<CRvalueReference<Type>>;
	template <class Type>
	inline constexpr bool VIsRvalueReference = CRvalueReference<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsImplicitLifetimeObject

	// Some info on implicit-lifetime objects:
	// https://www.open-std.org/jtc1/sc22/wg21/docs/papers/2020/p0593r6.html
	// https://en.cppreference.com/w/cpp/language/object#Object_creation
	// https://en.cppreference.com/w/cpp/named_req/ImplicitLifetimeType
	// https://stackoverflow.com/questions/37644977
	// along with some discussion on the problem it is solving:
	// https://groups.google.com/a/isocpp.org/g/std-discussion/c/bsb8okPgDak

	// NOTE: This is an imperfect implementation, although sufficient for most purposes,
	//  as an implicit-lifetime object of class type requires only that one of its constructors is trivial;
	//  while this is technically not verifiable without compiler support (also see std::is_implicit_lifetime, added in C++23),
	//  since the only trivial constructors (in C++20) are the default, copy, and move constructors,
	//  along with aggregate initialization from a parenthesized list, it is enough to check these traits when the type is a class;
	//  see https://stackoverflow.com/questions/66234335

	template <class Type>
	struct ZzInternal_TIsImplicitLifetimeObject
		: TValueTrait<
			std::is_scalar_v<Type>
			// NOTE: Implicit initialization of an array of non-implicit-lifetime objects
			//  does not initialize those objects; conversely, initialization of an implicit-lifetime object
			//  will also initialize any implicit-lifetime sub-objects.
			//  This is unique in the standard, and only valid since C++20.
			|| std::is_array_v<Type>
			|| (std::is_aggregate_v<Type>
				&& std::is_trivially_destructible_v<Type>)
			// CHECKME can a union be a ILO?
			|| (std::is_class_v<Type>
				&& (std::is_trivially_default_constructible_v<Type>
					|| std::is_trivially_copy_constructible_v<Type>
					|| std::is_trivially_move_constructible_v<Type>)
				&& std::is_trivially_destructible_v<Type>)
		> {};

	template <class Type>
	concept CImplicitLifetimeObject = ZzInternal_TIsImplicitLifetimeObject<std::remove_cv_t<Type>>::Value;
	template <class Type>
	concept CNotImplicitLifetimeObject = !CImplicitLifetimeObject<Type>;

	template <class Type>
	using TIsImplicitLifetimeObject = TValueTrait<CImplicitLifetimeObject<Type>>;
	template <class Type>
	inline constexpr bool VIsImplicitLifetimeObject = CImplicitLifetimeObject<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CScalarObject, CObjectElseVoid, CILOElseVoid, CScalarObjectElseVoid

	// CHECKME any better place to declare these?

	template <class Type>
	concept CScalarObject = CObject<Type> && CNotArray<Type>;

	template <class Type>
	concept CObjectElseVoid = CObject<Type> || CVoid<Type>;

	template <class Type>
	concept CILOElseVoid = CImplicitLifetimeObject<Type> || CVoid<Type>;

	template <class Type>
	concept CScalarObjectElseVoid = CScalarObject<Type> || CVoid<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsEmptyClass

	template <class Type>
	concept CEmptyClass = std::is_empty<Type>::value;
	template <class Type>
	concept CNotEmptyClass = !CEmptyClass<Type>;

	template <class Type>
	using TIsEmptyClass = TValueTrait<CEmptyClass<Type>>;
	template <class Type>
	inline constexpr bool VIsEmptyClass = CEmptyClass<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsStandardLayoutClass

	template <class Type>
	concept CStandardLayoutObject = std::is_standard_layout<Type>::value;
	template <class Type>
	concept CNotStandardLayoutObject = !CStandardLayoutObject<Type>;

	template <class Type>
	using TIsStandardLayoutObject = TValueTrait<CStandardLayoutObject<Type>>;
	template <class Type>
	inline constexpr bool VIsStandardLayoutObject = CStandardLayoutObject<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsAggregateClass

	template <class Type>
	concept CAggregateClass = std::is_aggregate<Type>::value;
	template <class Type>
	concept CNotAggregateClass = !CAggregateClass<Type>;

	template <class Type>
	using TIsAggregateClass = TValueTrait<CAggregateClass<Type>>;
	template <class Type>
	inline constexpr bool VIsAggregateClass = CAggregateClass<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsAbstractClass

	template <class Type>
	concept CAbstractClass = std::is_abstract<Type>::value;
	template <class Type>
	concept CNotAbstractClass = !CAbstractClass<Type>;

	template <class Type>
	using TIsAbstractClass = TValueTrait<CAbstractClass<Type>>;
	template <class Type>
	inline constexpr bool VIsAbstractClass = CAbstractClass<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsFinalClass

	template <class Type>
	concept CFinalClass = std::is_final<Type>::value;
	template <class Type>
	concept CNotFinalClass = !CFinalClass<Type>;

	template <class Type>
	using TIsFinalClass = TValueTrait<CFinalClass<Type>>;
	template <class Type>
	inline constexpr bool VIsFinalClass = CFinalClass<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsConstructible

	template <class ObjType, class ... ArgTypes>
	concept CConstructible = std::is_constructible<ObjType, ArgTypes ...>::value;
	template <class ObjType, class ... ArgTypes>
	concept CNotConstructible = !CConstructible<ObjType, ArgTypes ...>;

	template <class ObjType, class ... ArgTypes>
	using TIsConstructible = TValueTrait<CConstructible<ObjType, ArgTypes ...>>;
	template <class ObjType, class ... ArgTypes>
	inline constexpr bool VIsConstructible = CConstructible<ObjType, ArgTypes ...>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsDefaultConstructible

	template <class Type>
	concept CDefaultConstructible = std::is_default_constructible<Type>::value;
	template <class Type>
	concept CNotDefaultConstructible = !CDefaultConstructible<Type>;

	template <class Type>
	using TIsDefaultConstructible = TValueTrait<CDefaultConstructible<Type>>;
	template <class Type>
	inline constexpr bool VIsDefaultConstructible = CDefaultConstructible<Type>;

// TODO should there be a TIsCopyConstructible and a TIsMoveConstructible ? (prob yes)

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsCopyable, TIsMovable, TIsMoveableElseCopyable

	template <class Type>
	concept CCopyable = std::conjunction<std::is_copy_constructible<Type>, std::is_copy_assignable<Type>>::value;
	template <class Type>
	concept CNotCopyable = !CCopyable<Type>;

	template <class Type>
	using TIsCopyable = TValueTrait<CCopyable<Type>>;
	template <class Type>
	inline constexpr bool VIsCopyable = CCopyable<Type>;

	template <class Type>
	concept CMoveable = std::conjunction<std::is_move_constructible<Type>, std::is_move_assignable<Type>>::value;
	template <class Type>
	concept CNotMoveable = !CMoveable<Type>;

	template <class Type>
	using TIsMoveable = TValueTrait<CMoveable<Type>>;
	template <class Type>
	inline constexpr bool VIsMoveable = CMoveable<Type>;

	template <class Type>
	concept CMoveableElseCopyable = CMoveable<Type> || CCopyable<Type>;
	template <class Type>
	concept CNotMoveableElseCopyable = !CMoveableElseCopyable<Type>;

	template <class Type>
	using TIsMoveableElseCopyable = TValueTrait<CMoveableElseCopyable<Type>>;
	template <class Type>
	inline constexpr bool VIsMoveableElseCopyable = CMoveableElseCopyable<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsDestructible

	template <class Type>
	concept CDestructible = std::is_destructible<Type>::value;
	template <class Type>
	concept CNotDestructible = !CDestructible<Type>;

	template <class Type>
	using TIsDestructible = TValueTrait<CDestructible<Type>>;
	template <class Type>
	inline constexpr bool VIsDestructible = CDestructible<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsTriviallyConstructible

	template <class ObjType, class ... ArgTypes>
	concept CTriviallyConstructible = std::is_trivially_constructible<ObjType, ArgTypes...>::value;
	template <class ObjType, class ... ArgTypes>
	concept CNotTriviallyConstructible = !CTriviallyConstructible<ObjType, ArgTypes...>;

	template <class ObjType, class ... ArgTypes>
	using TIsTriviallyConstructible = TValueTrait<CTriviallyConstructible<ObjType, ArgTypes ...>>;
	template <class ObjType, class ... ArgTypes>
	inline constexpr bool VIsTriviallyConstructible = CTriviallyConstructible<ObjType, ArgTypes ...>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsTriviallyDefaultConstructible

// CHECKME is this of any use?

	template <class Type>
	concept CTriviallyDefaultConstructible = std::is_trivially_default_constructible<Type>::value;
	template <class Type>
	concept CNotTriviallyDefaultConstructible = !CTriviallyDefaultConstructible<Type>;

	template <class Type>
	using TIsTriviallyDefaultConstructible = TValueTrait<CTriviallyDefaultConstructible<Type>>;
	template <class Type>
	inline constexpr bool VIsTriviallyDefaultConstructible = CTriviallyDefaultConstructible<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsTriviallyCopyable

	// NOTE: Implies TIsTriviallyDestructible

	template <class Type>
	concept CTriviallyCopyable = std::is_trivially_copyable<Type>::value;
	template <class Type>
	concept CNotTriviallyCopyable = !CTriviallyCopyable<Type>;

	template <class Type>
	using TIsTriviallyCopyable = TValueTrait<CTriviallyCopyable<Type>>;
	template <class Type>
	inline constexpr bool VIsTriviallyCopyable = CTriviallyCopyable<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsTriviallyDestructible

	template <class Type>
	concept CTriviallyDestructible = std::is_trivially_destructible<Type>::value;
	template <class Type>
	concept CNotTriviallyDestructible = !CTriviallyDestructible<Type>;

	template <class Type>
	using TIsTriviallyDestructible = TValueTrait<CTriviallyDestructible<Type>>;
	template <class Type>
	inline constexpr bool VIsTriviallyDestructible = CTriviallyDestructible<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsVirtuallyDestructible

	template <class Type>
	concept CVirtuallyDestructible = std::has_virtual_destructor<Type>::value;
	template <class Type>
	concept CNotVirtuallyDestructible = !CVirtuallyDestructible<Type>;

	template <class Type>
	using TIsVirtuallyDestructible = TValueTrait<CVirtuallyDestructible<Type>>;
	template <class Type>
	inline constexpr bool VIsVirtuallyDestructible = CVirtuallyDestructible<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsTriviallyHashable

	template <class Type>
	concept CTriviallyHashable = std::has_unique_object_representations<Type>::value;
	template <class Type>
	concept CNotTriviallyHashable = !CTriviallyHashable<Type>;

	template <class Type>
	using TIsTriviallyHashable = TValueTrait<CTriviallyHashable<Type>>;
	template <class Type>
	inline constexpr bool VIsTriviallyHashable = CTriviallyHashable<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsConst, TIsVolatile, TIsConstVolatile

	template <class Type>
	concept CConst = std::is_const<Type>::value;
	template <class Type>
	concept CNotConst = !CConst<Type>;

	template <class Type>
	using TIsConst = TValueTrait<CConst<Type>>;
	template <class Type>
	inline constexpr bool VIsConst = CConst<Type>;

	template <class Type>
	concept CVolatile = std::is_volatile<Type>::value;
	template <class Type>
	concept CNotVolatile = !CVolatile<Type>;

	template <class Type>
	using TIsVolatile = TValueTrait<CVolatile<Type>>;
	template <class Type>
	inline constexpr bool VIsVolatile = CVolatile<Type>;

	template <class Type>
	concept CConstVolatile = std::conjunction<std::is_const<Type>, std::is_volatile<Type>>::value;
	template <class Type>
	concept CNotConstVolatile = !CConstVolatile<Type>;

	template <class Type>
	using TIsConstVolatile = TValueTrait<CConstVolatile<Type>>;
	template <class Type>
	inline constexpr bool VIsConstVolatile = CConstVolatile<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TAddConst, TAddVolatile, TAddConstVolatile, TRemoveConstVolatile

	template <class ClassType>
	struct ZzInternal_TAddConst : TTypeTrait<std::add_const_t<ClassType>> {};

	template <class ClassType>
	using TAddConst = typename ZzInternal_TAddConst<ClassType>::Type;

	template <class ClassType>
	struct ZzInternal_TAddVolatile : TTypeTrait<std::add_volatile_t<ClassType>> {};

	template <class ClassType>
	using TAddVolatile = typename ZzInternal_TAddVolatile<ClassType>::Type;

	template <class ClassType>
	struct ZzInternal_TAddConstVolatile : TTypeTrait<std::add_cv_t<ClassType>> {};

	template <class ClassType>
	using TAddConstVolatile = typename ZzInternal_TAddConstVolatile<ClassType>::Type;

	template <class ClassType>
	struct ZzInternal_TRemoveConst : TTypeTrait<std::remove_const_t<ClassType>> {};

	template <class ClassType>
	using TRemoveConst = typename ZzInternal_TRemoveConst<ClassType>::Type;

	template <class ClassType>
	struct ZzInternal_TRemoveVolatile : TTypeTrait<std::remove_volatile_t<ClassType>> {};

	template <class ClassType>
	using TRemoveVolatile = typename ZzInternal_TRemoveVolatile<ClassType>::Type;

	template <class ClassType>
	struct ZzInternal_TRemoveConstVolatile : TTypeTrait<std::remove_cv_t<ClassType>> {};

	template <class ClassType>
	using TRemoveConstVolatile = typename ZzInternal_TRemoveConstVolatile<ClassType>::Type;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TPropagateConst, TPropagateVolatile, TPropagateConstVolatile

	template <class FromType, class ToType>
	using TPropagateConst = TSelectIf<std::is_const_v<FromType>, ToType const, ToType>;

	template <class FromType, class ToType>
	using TPropagateVolatile = TSelectIf<std::is_volatile_v<FromType>, ToType volatile, ToType>;

	template <class FromType, class ToType>
	using TPropagateConstVolatile = TPropagateVolatile<FromType, TPropagateConst<FromType, ToType>>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TAddPointer, TRemovePointer

	template <class ClassType>
	struct ZzInternal_TAddPointer : TTypeTrait<std::add_pointer_t<ClassType>> {};

	template <class ClassType>
	using TAddPointer = typename ZzInternal_TAddPointer<ClassType>::Type;

	template <class ClassType>
	struct ZzInternal_TRemovePointer : TTypeTrait<std::remove_pointer_t<ClassType>> {};

	template <class ClassType>
	using TRemovePointer = typename ZzInternal_TRemovePointer<ClassType>::Type;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TAddMemberPointer, TRemoveMemberPointer

	template <class MemberType, class ClassType>
	struct ZzInternal_TAddMemberPointer
	{
		using Type = MemberType ClassType::*;
	};

	template <class MemberType, class ClassType>
	using TAddMemberPointer = typename ZzInternal_TAddMemberPointer<MemberType, ClassType>::Type;

	template <class ClassType>
	struct ZzInternal_TRemoveMemberPointer
	{
		using Type = ClassType;
	};

	template <class MemberType, class ClassType>
	struct ZzInternal_TRemoveMemberPointer<MemberType ClassType::*>
	{
		using Type = MemberType;
	};

	template <class ClassType>
	using TRemoveMemberPointer = typename ZzInternal_TRemoveMemberPointer<std::remove_cv_t<ClassType>>::Type;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TAddArrayDimension

	template <class ObjectType, szint SizeValue>
	struct ZzInternal_TAddArrayDimension : TTypeTrait<ObjectType[SizeValue]> {};

	template <class ObjectType>
	struct ZzInternal_TAddArrayDimension<ObjectType, 0> : TTypeTrait<ObjectType[]> {};

	template <class ObjectType, szint SizeValue = 0>
	using TAddArrayDimension = typename ZzInternal_TAddArrayDimension<ObjectType, SizeValue>::Type;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TRemoveArrayDimension, TRemoveAllArrayDimensions

	template <class ArrayType>
	struct ZzInternal_TRemoveArrayDimension : TTypeTrait<std::remove_extent_t<ArrayType>> {};

	template <class ArrayType>
	using TRemoveArrayDimension = typename ZzInternal_TRemoveArrayDimension<ArrayType>::Type;

	template <class ArrayType>
	struct ZzInternal_TRemoveAllArrayDimensions : TTypeTrait<std::remove_all_extents_t<ArrayType>> {};

	template <class ArrayType>
	using TRemoveAllArrayDimensions = typename ZzInternal_TRemoveAllArrayDimensions<ArrayType>::Type;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TRemoveArrayDimensionSize

	// TODO deprecate this (move it to memorycommon)

	template <class ObjectType>
	struct ZzInternal_TRemoveArrayDimensionSize : TTypeTrait<ObjectType> {};

	template <class ArrayType, szint SizeValue>
	struct ZzInternal_TRemoveArrayDimensionSize<ArrayType[SizeValue]> : TTypeTrait<ArrayType[]> {};

	template <class ArrayType>
	using TRemoveArrayDimensionSize = typename ZzInternal_TRemoveArrayDimensionSize<ArrayType>::Type;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TArrayDimensionSize

	template <class ArrayType, szint IndexValue = 0>
	struct TArrayDimensionSize : TValueTrait<std::extent_v<ArrayType, unsigned(IndexValue)>> {};

	template <class ArrayType, szint IndexValue = 0>
	inline constexpr szint VArrayDimensionSize = TArrayDimensionSize<ArrayType, IndexValue>::Value;

	// TODO add TArrayDimensionCount (std::rank)

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TPropagateArrayDimension

	// TODO deprecate this (move it to memorycommon)

	template <class FromType, class ToType, bool = VIsArray<FromType>>
	struct ZzInternal_TPropagateArrayDimension
	{
		using Type = TAddArrayDimension<ToType, VArrayDimensionSize<FromType>>;
	};
	template <class FromType, class ToType>
	struct ZzInternal_TPropagateArrayDimension<FromType, ToType, false>
	{
		using Type = ToType;
	};

	template <class FromType, class ToType>
	using TPropagateArrayDimension = typename ZzInternal_TPropagateArrayDimension<FromType, ToType>::Type;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TAddLvalueReference, TAddRvalueReference, TRemoveReference

	template <class ClassType>
	struct ZzInternal_TAddLvalueReference : TTypeTrait<std::add_lvalue_reference_t<ClassType>> {};

	template <class ClassType>
	using TAddLvalueReference = typename ZzInternal_TAddLvalueReference<ClassType>::Type;

	template <class ClassType>
	struct ZzInternal_TAddRvalueReference : TTypeTrait<std::add_rvalue_reference_t<ClassType>> {};

	template <class ClassType>
	using TAddRvalueReference = typename ZzInternal_TAddRvalueReference<ClassType>::Type;

	template <class ClassType>
	struct ZzInternal_TRemoveReference : TTypeTrait<std::remove_reference_t<ClassType>> {};

	template <class ClassType>
	using TRemoveReference = typename ZzInternal_TRemoveReference<ClassType>::Type;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TMemberPointerUnderlyingType

	template <class ClassType>
	struct ZzInternal_TMemberPointerUnderlyingType
	{
		using Type = TNullTypeTag;
	};

	template <class MemberType, class ClassType>
	struct ZzInternal_TMemberPointerUnderlyingType<MemberType ClassType::*>
	{
		using Type = ClassType;
	};

	template <class ClassType>
	using TMemberPointerUnderlyingType = typename ZzInternal_TMemberPointerUnderlyingType<std::remove_cv_t<ClassType>>::Type;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TEnumUnderlyingType

	template <class EnumType, bool = VIsEnum<EnumType>>
	struct ZzInternal_TEnumUnderlyingType : TTypeTrait<TNullTypeTag> {};

	template <class EnumType>
	struct ZzInternal_TEnumUnderlyingType<EnumType, true> : TTypeTrait<std::underlying_type_t<EnumType>> {};

	template <class EnumType>
	using TEnumUnderlyingType = typename ZzInternal_TEnumUnderlyingType<std::remove_cv_t<EnumType>>::Type;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TCallableTraits

	// CHECKME should ZzInternal_T(Member)CallableTraits be exposed as a public API?

	template <class CallableType, class ArgumentPackType, class = void>
	struct ZzInternal_TCallableTraits
	{
		using ReturnType = TNullTypeTag;

		static constexpr bool IsCallable = false;
	};

	template <class CallableType, class ... ArgumentTypes>
	struct ZzInternal_TCallableTraits<CallableType, TTypePack<ArgumentTypes ...>,
									  TCheckWellFormed<decltype(declval(CallableType)(declval(ArgumentTypes) ...))>>
	{
		using ReturnType = decltype(declval(CallableType)(declval(ArgumentTypes) ...));

		static constexpr bool IsCallable = true;
	};

	template <class CallableType, class ... ArgumentTypes>
	struct TCallableTraits : ZzInternal_TCallableTraits<CallableType, TTypePack<ArgumentTypes ...>> {};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TMemberCallableTraits

	template <class CallableType, class ObjectType, class ArgumentPackType, class = void>
	struct ZzInternal_TMemberCallableTraits
	{
		using ReturnType = TNullTypeTag;

		static constexpr bool IsCallable = false;
	};

	template <class CallableType, class ObjectType, class ... ArgumentTypes>
	struct ZzInternal_TMemberCallableTraits<CallableType, ObjectType, TTypePack<ArgumentTypes ...>,
										    TCheckWellFormed<decltype((declval(ObjectType).*declval(CallableType))(declval(ArgumentTypes) ...))>>
	{
		using ReturnType = decltype((declval(ObjectType).*declval(CallableType))(declval(ArgumentTypes) ...));

		static constexpr bool IsCallable = true;
	};

	template <class CallableType, class ObjectType, class ... ArgumentTypes>
	struct TMemberCallableTraits : ZzInternal_TMemberCallableTraits<CallableType, ObjectType, TTypePack<ArgumentTypes ...>> {};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TCallableResult

	template <class CallableType, class ... ArgumentTypes>
	using TCallableResult = typename TCallableTraits<CallableType, ArgumentTypes ...>::ReturnType;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TMemberCallableResult

	template <class CallableType, class ObjectType, class ... ArgumentTypes>
	using TMemberCallableResult = typename TMemberCallableTraits<CallableType, ObjectType, ArgumentTypes ...>::ReturnType;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsCallable

	template <class CallableType, class ... ArgumentTypes>
	concept CCallable = TCallableTraits<CallableType, ArgumentTypes ...>::IsCallable;
	template <class CallableType, class ... ArgumentTypes>
	concept CNotCallable = !CCallable<CallableType, ArgumentTypes ...>;

	template <class CallableType, class ... ArgumentTypes>
	using TIsCallable = TValueTrait<CCallable<CallableType, ArgumentTypes ...>>;
	template <class CallableType, class ... ArgumentTypes>
	inline constexpr bool VIsCallable = CCallable<CallableType, ArgumentTypes ...>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsCallableWithResult

	template <class CallableType, class ReturnType, class ... ArgumentTypes>
	concept CCallableWithResult = CCallable<CallableType, ArgumentTypes ...>
								  && CSameClass<TCallableResult<CallableType, ArgumentTypes ...>, ReturnType>;
	template <class CallableType, class ReturnType, class ... ArgumentTypes>
	concept CNotCallableWithResult = !CCallableWithResult<CallableType, ReturnType, ArgumentTypes ...>;

	template <class CallableType, class ReturnType, class ... ArgumentTypes>
	using TIsCallableWithResult = TValueTrait<CCallableWithResult<CallableType, ReturnType, ArgumentTypes ...>>;
	template <class CallableType, class ReturnType, class ... ArgumentTypes>
	inline constexpr bool VIsCallableWithResult = CCallableWithResult<CallableType, ReturnType, ArgumentTypes ...>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsCallableWithConvertibleResult

	template <class CallableType, class ReturnType, class ... ArgumentTypes>
	concept CCallableWithConvertibleResult = CCallable<CallableType, ArgumentTypes ...>
											 && CConvertibleClass<TCallableResult<CallableType, ArgumentTypes ...>, ReturnType>;
	template <class CallableType, class ReturnType, class ... ArgumentTypes>
	concept CNotCallableWithConvertibleResult = !CCallableWithConvertibleResult<CallableType, ReturnType, ArgumentTypes ...>;

	template <class CallableType, class ReturnType, class ... ArgumentTypes>
	using TIsCallableWithConvertibleResult = TValueTrait<CCallableWithConvertibleResult<CallableType, ReturnType, ArgumentTypes ...>>;
	template <class CallableType, class ReturnType, class ... ArgumentTypes>
	inline constexpr bool VIsCallableWithConvertibleResult = CCallableWithConvertibleResult<CallableType, ReturnType, ArgumentTypes ...>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsMemberCallable

	template <class CallableType, class ObjectType, class ... ArgumentTypes>
	concept CMemberCallable = TMemberCallableTraits<CallableType, ObjectType, ArgumentTypes ...>::IsCallable;
	template <class CallableType, class ObjectType, class ... ArgumentTypes>
	concept CNotMemberCallable = !CMemberCallable<CallableType, ObjectType, ArgumentTypes ...>;

	template <class CallableType, class ObjectType, class ... ArgumentTypes>
	using TIsMemberCallable = TValueTrait<CMemberCallable<CallableType, ObjectType, ArgumentTypes ...>>;
	template <class CallableType, class ObjectType, class ... ArgumentTypes>
	inline constexpr bool VIsMemberCallable = CMemberCallable<CallableType, ObjectType, ArgumentTypes ...>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsMemberCallableWithResult

	template <class CallableType, class ObjectType, class ReturnType, class ... ArgumentTypes>
	concept CMemberCallableWithResult = CMemberCallable<CallableType, ObjectType, ArgumentTypes ...>
										&& CSameClass<TMemberCallableResult<CallableType, ObjectType, ArgumentTypes ...>, ReturnType>;
	template <class CallableType, class ObjectType, class ReturnType, class ... ArgumentTypes>
	concept CNotMemberCallableWithResult = !CMemberCallableWithResult<CallableType, ObjectType, ReturnType, ArgumentTypes ...>;

	template <class CallableType, class ObjectType, class ReturnType, class ... ArgumentTypes>
	using TIsMemberCallableWithResult = TValueTrait<CMemberCallableWithResult<CallableType, ObjectType, ReturnType, ArgumentTypes ...>>;
	template <class CallableType, class ObjectType, class ReturnType, class ... ArgumentTypes>
	inline constexpr bool VIsMemberCallableWithResult = CMemberCallableWithResult<CallableType, ObjectType, ReturnType, ArgumentTypes ...>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsMemberCallableWithConvertibleResult

	template <class CallableType, class ObjectType, class ReturnType, class ... ArgumentTypes>
	concept CMemberCallableWithConvertibleResult = CMemberCallable<CallableType, ObjectType, ArgumentTypes ...>
												   && CConvertibleClass<TMemberCallableResult<CallableType, ObjectType, ArgumentTypes ...>,
												   						ReturnType>;
	template <class CallableType, class ObjectType, class ReturnType, class ... ArgumentTypes>
	concept CNotMemberCallableWithConvertibleResult = !CMemberCallableWithConvertibleResult<CallableType, ObjectType, ReturnType, ArgumentTypes ...>;

	template <class CallableType, class ObjectType, class ReturnType, class ... ArgumentTypes>
	using TIsMemberCallableWithConvertibleResult = TValueTrait<CMemberCallableWithConvertibleResult<CallableType, ObjectType,
																									ReturnType, ArgumentTypes ...>>;
	template <class CallableType, class ObjectType, class ReturnType, class ... ArgumentTypes>
	inline constexpr bool VIsMemberCallableWithConvertibleResult = CMemberCallableWithConvertibleResult<CallableType, ObjectType,
																										ReturnType, ArgumentTypes ...>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TMakeSignMagnitudeInteger

	template <bool SignValue, szint MagnitudeValue>
	struct ZzInternal_TMakeSignMagnitudeInteger;

	template <>
	struct ZzInternal_TMakeSignMagnitudeInteger<true, 1>
	{
		using Type = int8;
	};
	template <>
	struct ZzInternal_TMakeSignMagnitudeInteger<true, 2>
	{
		using Type = int16;
	};
	template <>
	struct ZzInternal_TMakeSignMagnitudeInteger<true, 4>
	{
		using Type = int32;
	};
	template <>
	struct ZzInternal_TMakeSignMagnitudeInteger<true, 8>
	{
		using Type = int64;
	};
//	template <>
//	struct ZzInternal_TMakeSignMagnitudeInteger<true, 16>
//	{
//		using Type = int128;
//	};

	template <>
	struct ZzInternal_TMakeSignMagnitudeInteger<false, 1>
	{
		using Type = uint8;
	};
	template <>
	struct ZzInternal_TMakeSignMagnitudeInteger<false, 2>
	{
		using Type = uint16;
	};
	template <>
	struct ZzInternal_TMakeSignMagnitudeInteger<false, 4>
	{
		using Type = uint32;
	};
	template <>
	struct ZzInternal_TMakeSignMagnitudeInteger<false, 8>
	{
		using Type = uint64;
	};
//	template <>
//	struct ZzInternal_TMakeSignMagnitudeInteger<false, 16>
//	{
//		using Type = uint128;
//	};

	template <bool SignValue, szint MagnitudeValue>
	using TMakeSignMagnitudeInteger = typename ZzInternal_TMakeSignMagnitudeInteger<SignValue, MagnitudeValue>::Type;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsSigned

	template <class ArithType, class = void>
	struct ZzInternal_TIsSigned
	{
		static constexpr bool Value = false;
	};

	template <>
	struct ZzInternal_TIsSigned<bool>
	{
		static constexpr bool Value = false;
	};

	template <class ArithType>
	struct ZzInternal_TIsSigned<ArithType, TEnableIf<VIsCharacter<ArithType>>>
	{
		static constexpr bool Value = std::is_signed<ArithType>::value;
	};

	template <>
	struct ZzInternal_TIsSigned<signed char>
	{
		static constexpr bool Value = true;
	};
	template <>
	struct ZzInternal_TIsSigned<signed short>
	{
		static constexpr bool Value = true;
	};
	template <>
	struct ZzInternal_TIsSigned<signed int>
	{
		static constexpr bool Value = true;
	};
	template <>
	struct ZzInternal_TIsSigned<signed long>
	{
		static constexpr bool Value = true;
	};
	template <>
	struct ZzInternal_TIsSigned<signed long long>
	{
		static constexpr bool Value = true;
	};
//	template <>
//	struct ZzInternal_TIsSigned<int128>
//	{
//		static constexpr bool Value = true;
//	};

	template <>
	struct ZzInternal_TIsSigned<algnint>
	{
		static constexpr bool Value = false;
	};

	template <class ArithType>
	struct ZzInternal_TIsSigned<ArithType, TEnableIf<VIsFloatingPoint<ArithType>>>
	{
		static constexpr bool Value = true;
	};

	template <class ArithType>
	concept CSigned = ZzInternal_TIsSigned<TRemoveConstVolatile<ArithType>>::Value;
	template <class ArithType>
	concept CNotSigned = !CSigned<ArithType>;

	template <class ArithType>
	using TIsSigned = TValueTrait<CSigned<ArithType>>;
	template <class ArithType>
	inline constexpr bool VIsSigned = CSigned<ArithType>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsUnsigned

	template <class ArithType, class = void>
	struct ZzInternal_TIsUnsigned
	{
		static constexpr bool Value = false;
	};

	template <>
	struct ZzInternal_TIsUnsigned<bool>
	{
		static constexpr bool Value = true;
	};

	template <class ArithType>
	struct ZzInternal_TIsUnsigned<ArithType, TEnableIf<VIsCharacter<ArithType>>>
	{
		static constexpr bool Value = std::is_unsigned<ArithType>::value;
	};

	template <>
	struct ZzInternal_TIsUnsigned<unsigned char>
	{
		static constexpr bool Value = true;
	};
	template <>
	struct ZzInternal_TIsUnsigned<unsigned short>
	{
		static constexpr bool Value = true;
	};
	template <>
	struct ZzInternal_TIsUnsigned<unsigned int>
	{
		static constexpr bool Value = true;
	};
	template <>
	struct ZzInternal_TIsUnsigned<unsigned long>
	{
		static constexpr bool Value = true;
	};
	template <>
	struct ZzInternal_TIsUnsigned<unsigned long long>
	{
		static constexpr bool Value = true;
	};
//	template <>
//	struct ZzInternal_TIsUnsigned<uint128>
//	{
//		static constexpr bool Value = true;
//	};

	template <>
	struct ZzInternal_TIsUnsigned<algnint>
	{
		static constexpr bool Value = true;
	};

	template <class ArithType>
	struct ZzInternal_TIsUnsigned<ArithType, TEnableIf<VIsFloatingPoint<ArithType>>>
	{
		static constexpr bool Value = false;
	};

	template <class ArithType>
	concept CUnsigned = ZzInternal_TIsUnsigned<TRemoveConstVolatile<ArithType>>::Value;
	template <class ArithType>
	concept CNotUnsigned = !CUnsigned<ArithType>;

	template <class ArithType>
	using TIsUnsigned = TValueTrait<CUnsigned<ArithType>>;
	template <class ArithType>
	inline constexpr bool VIsUnsigned = CUnsigned<ArithType>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TMakeSigned

	template <class NumericType>
	struct ZzInternal_TMakeSigned
	{
		using Type = NumericType;
	};

	template <>
	struct ZzInternal_TMakeSigned<unsigned char>
	{
		using Type = signed char;
	};
	template <>
	struct ZzInternal_TMakeSigned<unsigned short>
	{
		using Type = signed short;
	};
	template <>
	struct ZzInternal_TMakeSigned<unsigned int>
	{
		using Type = signed int;
	};
	template <>
	struct ZzInternal_TMakeSigned<unsigned long>
	{
		using Type = signed long;
	};
	template <>
	struct ZzInternal_TMakeSigned<unsigned long long>
	{
		using Type = signed long long;
	};
//	template <>
//	struct ZzInternal_TMakeUnsigned<uint128>
//	{
//		using Type = int128;
//	};

	template <CNumeric NumericType>
	using TMakeSigned = TPropagateConstVolatile<NumericType, typename ZzInternal_TMakeSigned<TRemoveConstVolatile<NumericType>>::Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TMakeUnsigned

	template <class NumericType>
	struct ZzInternal_TMakeUnsigned
	{
		using Type = NumericType;
	};

	template <>
	struct ZzInternal_TMakeUnsigned<signed char>
	{
		using Type = unsigned char;
	};
	template <>
	struct ZzInternal_TMakeUnsigned<signed short>
	{
		using Type = unsigned short;
	};
	template <>
	struct ZzInternal_TMakeUnsigned<signed int>
	{
		using Type = unsigned int;
	};
	template <>
	struct ZzInternal_TMakeUnsigned<signed long>
	{
		using Type = unsigned long;
	};
	template <>
	struct ZzInternal_TMakeUnsigned<signed long long>
	{
		using Type = unsigned long long;
	};
//	template <>
//	struct ZzInternal_TMakeUnsigned<int128>
//	{
//		using Type = uint128;
//	};

	template <CNumeric NumericType>
	using TMakeUnsigned = TPropagateConstVolatile<NumericType, typename ZzInternal_TMakeUnsigned<TRemoveConstVolatile<NumericType>>::Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TNumericProperties

	template <CNumeric IntType>
	struct ZzInternal_TIntProperties
	{
		// Representation properties

		// Minimum non-positive value representable by the type
		static constexpr IntType Minimum = std::numeric_limits<IntType>::lowest();
		// Maximum non-negative value representable by the type
		static constexpr IntType Maximum = std::numeric_limits<IntType>::max();
		// Smallest positive value representable by the type
		static constexpr IntType Smallest = IntType{1};

		// Platform representation details

		// The base of the number system used in the internal representation of the type
		static constexpr szint Radix = std::numeric_limits<IntType>::radix;

		// The amount of radix digits that can be represented by the type
		// E.g., for floating-point numbers,
		// this is usually the number of digits used by the mantissa plus the implicit leading 1 digit
		static constexpr szint RadixDigits = std::numeric_limits<IntType>::digits;

		// Decimal representation limitations

		// Minimum amount of decimal significant digits that are needed to exactly represent an object of the type as text,
		// such that conversion back to the type retains all digits.
		// This is useful in serialization/deserialization of such objects to/from text.
		static constexpr szint MinimumWritableDigits = std::numeric_limits<IntType>::digits10 + 1;
		// Maximum amount of decimal significant digits that can be converted exactly from text to an object of the type,
		// i.e., without loss of precision, such that conversion back to text retains all digits.
		// This is useful in conversion of values from an arbitrary text source to objects of the type.
		static constexpr szint MaximumReadableDigits = std::numeric_limits<IntType>::digits10;

	};

	template <CNumeric FloatType>
	struct ZzInternal_TFloatProperties
	{
		// Representation properties

		// Minimum non-positive value representable by the type
		static constexpr FloatType Minimum = std::numeric_limits<FloatType>::lowest();
		// Maximum non-negative value representable by the type
		static constexpr FloatType Maximum = std::numeric_limits<FloatType>::max();
		// Smallest positive value representable by the type
		static constexpr FloatType Smallest = std::numeric_limits<FloatType>::min();
		// Smallest positive subnormal value representable by the type
		static constexpr FloatType SmallestSubnormal = std::numeric_limits<FloatType>::denorm_min();
		// Smallest positive value that represents the difference between 1 and the next representable value (towards positive infinity)
		static constexpr FloatType Epsilon = std::numeric_limits<FloatType>::epsilon();
		// Largest positive value that represents the rounding error in Units of Least Precision (ULPs)
		static constexpr FloatType LargestError = std::numeric_limits<FloatType>::round_error();

		// Platform representation details

		// The base of the number system used in the internal representation of the type
		static constexpr szint Radix = std::numeric_limits<FloatType>::radix;

		// The amount of radix digits that can be represented by the type
		// E.g., for floating-point numbers,
		// this is usually the number of digits used by the mantissa plus the implicit leading 1 digit
		static constexpr szint RadixDigits = std::numeric_limits<FloatType>::digits;

		// Decimal representation limitations
		// Usually MinimumWritableDigits >= MaximumReadableDigits

		// Minimum amount of decimal significant digits that are needed to exactly represent an object of the type as text,
		// such that conversion back to the type retains all digits.
		// This is useful in serialization/deserialization of such objects to/from text.
		static constexpr szint MinimumWritableDigits = std::numeric_limits<FloatType>::max_digits10;
		// Maximum amount of decimal significant digits that can be converted exactly from text to an object of the type,
		// i.e., without loss of precision, such that conversion back to text retains all digits.
		// This is useful in conversion of values from an arbitrary text source to objects of the type.
		static constexpr szint MaximumReadableDigits = std::numeric_limits<FloatType>::digits10;

		// Floating-point details

		static constexpr bool IsISO60559Conforming = std::numeric_limits<FloatType>::is_iec559;
		static constexpr bool HasInfinity = std::numeric_limits<FloatType>::has_infinity;
		static constexpr bool HasQuietNaN = std::numeric_limits<FloatType>::has_quiet_NaN;
		static constexpr bool HasSignalingNaN = std::numeric_limits<FloatType>::has_signaling_NaN;

		// NOTE: Do not use these directly to compare with a value, use instead the appropriate methods in the AeonMath namespace.

		static constexpr FloatType Infinity = std::numeric_limits<FloatType>::infinity();
		static constexpr FloatType QuietNaN = std::numeric_limits<FloatType>::quiet_NaN();
		static constexpr FloatType SignalingNaN = std::numeric_limits<FloatType>::signaling_NaN();
	};

	template <CNumeric NumericType>
	struct TNumericProperties : TSelectIf<VIsInteger<NumericType>,
										  ZzInternal_TIntProperties<NumericType>,
										  ZzInternal_TFloatProperties<NumericType>>
	{
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CUnsignedInteger, CSignedInteger, CStandardFloatingPoint

	template <class IntType>
	concept CUnsignedInteger = CInteger<IntType> && CUnsigned<IntType>;
	template <class IntType>
	concept CSignedInteger = CInteger<IntType> && CSigned<IntType>;

	template <class FloatType>
	concept CStandardFloatingPoint = CFloatingPoint<FloatType> && TNumericProperties<FloatType>::IsISO60559Conforming;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TNumericSequence, TIndexSequence, TIntSequence, TFloatSequence

	template <CNumeric NumericType, NumericType ... NumericValues>
	struct TNumericSequence
	{
		using ValueType = NumericType;

		template <szint IndexValue>
		static constexpr ValueType ValueAt = TTypePack<TObject<NumericValues> ...>::template ElementAt<IndexValue>::Value;

		using AsPack = TTypePack<TObject<NumericValues> ...>;

		static constexpr szint Count = sizeof...(NumericValues);
	};

	template <szint ... SequenceValues>
	using TIndexSequence = TNumericSequence<szint, SequenceValues ...>;

	template <CInteger IntType, IntType ... SequenceValues>
	using TIntSequence = TNumericSequence<IntType, SequenceValues ...>;

	template <CFloatingPoint FloatType, FloatType ... SequenceValues>
	using TFloatSequence = TNumericSequence<FloatType, SequenceValues ...>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TMakeIndexSequence, TMakeIntSequence, TMakeFloatSequence

	template <CNumeric NumericType, class SequenceType>
	struct ZzInternal_TMakeNumericSequence;

	template <CNumeric NumericType, szint ... SequenceValues>
	struct ZzInternal_TMakeNumericSequence<NumericType, std::index_sequence<SequenceValues ...>>
		: TTypeTrait<TNumericSequence<NumericType, NumericType(SequenceValues) ...>> {};

	template <szint SequenceCountValue>
	using TMakeIndexSequence = typename ZzInternal_TMakeNumericSequence<szint, std::make_index_sequence<SequenceCountValue>>::Type;

	template <CInteger IntType, szint SequenceCountValue>
	using TMakeIntSequence = typename ZzInternal_TMakeNumericSequence<IntType, std::make_index_sequence<SequenceCountValue>>::Type;

	template <CFloatingPoint FloatType, szint SequenceCountValue>
	using TMakeFloatSequence = typename ZzInternal_TMakeNumericSequence<FloatType, std::make_index_sequence<SequenceCountValue>>::Type;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// EFunctionQualifier and TFunctionQualifierSequence

	struct EFunctionQualifier
	{
		enum Flag 		: uint8
		{
			None 		= 0x00,

			Const 		= 0x01,
			Volatile 	= 0x02,

			LvalueRef 	= 0x04,
			RvalueRef 	= 0x08,

			Noexcept 	= 0x10,
		};
	};

	constexpr EFunctionQualifier::Flag operator&(EFunctionQualifier::Flag const& Value1, EFunctionQualifier::Flag const& Value2)
	{
		using UnderlyingType = TEnumUnderlyingType<EFunctionQualifier::Flag>;
		return static_cast<EFunctionQualifier::Flag>(static_cast<UnderlyingType const&>(Value1) & static_cast<UnderlyingType const&>(Value2));
	}
	constexpr EFunctionQualifier::Flag operator|(EFunctionQualifier::Flag const& Value1, EFunctionQualifier::Flag const& Value2)
	{
		using UnderlyingType = TEnumUnderlyingType<EFunctionQualifier::Flag>;
		return static_cast<EFunctionQualifier::Flag>(static_cast<UnderlyingType const&>(Value1) | static_cast<UnderlyingType const&>(Value2));
	}
	constexpr EFunctionQualifier::Flag operator^(EFunctionQualifier::Flag const& Value1, EFunctionQualifier::Flag const& Value2)
	{
		using UnderlyingType = TEnumUnderlyingType<EFunctionQualifier::Flag>;
		return static_cast<EFunctionQualifier::Flag>(static_cast<UnderlyingType const&>(Value1) ^ static_cast<UnderlyingType const&>(Value2));
	}

	template <EFunctionQualifier::Flag FuncQualValue = EFunctionQualifier::None>
	// LvalueRef and RvalueRef are mutually exclusive
	requires (((EFunctionQualifier::LvalueRef | EFunctionQualifier::RvalueRef) & FuncQualValue)
			  != (EFunctionQualifier::LvalueRef | EFunctionQualifier::RvalueRef))
	struct TFunctionQualifierSequence
	{
		static constexpr bool IsConst = FuncQualValue & EFunctionQualifier::Const;
		static constexpr bool IsVolatile = FuncQualValue & EFunctionQualifier::Volatile;
		static constexpr bool IsLvalueRef = FuncQualValue & EFunctionQualifier::LvalueRef;
		static constexpr bool IsRvalueRef = FuncQualValue & EFunctionQualifier::RvalueRef;
		static constexpr bool IsNoexcept = FuncQualValue & EFunctionQualifier::Noexcept;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TBreakFunctionSignature

	template <class FuncType>
	struct ZzInternal_TBreakFunctionSignatureBase
	{
		// Return type
		using ReturnType = TNullTypeTag;
		// Parameter types
	//	using ParametersPackType = TNullTypeTag;
		using ParametersPackType = TTypePack<>;
		// Qualifier sequence type
	//	using QualifierSeqType = TNullTypeTag;
		using QualifierSeqType = TFunctionQualifierSequence<>;

	//	static_assert(VAlwaysFalse<FuncType>, "'TBreakFunctionSignature<FuncType>': "
	//										  "FuncType must be of function type, a reference to function type, "
	//										  "a pointer-to-function type, or a pointer-to-member-function type.");
	};

	template <class RetType, class ... ParamTypes>
	struct ZzInternal_TBreakFunctionSignatureBase<RetType(ParamTypes ...)>
	{
		using ReturnType = RetType;
		using ParametersPackType = TTypePack<ParamTypes ...>;
		using QualifierSeqType = TFunctionQualifierSequence<>;
	};

	template <class RetType, class ... ParamTypes>
	struct ZzInternal_TBreakFunctionSignatureBase<RetType(ParamTypes ...) const>
	{
		using ReturnType = RetType;
		using ParametersPackType = TTypePack<ParamTypes ...>;
		using QualifierSeqType = TFunctionQualifierSequence<EFunctionQualifier::Const>;
	};

	template <class RetType, class ... ParamTypes>
	struct ZzInternal_TBreakFunctionSignatureBase<RetType(ParamTypes ...) volatile>
	{
		using ReturnType = RetType;
		using ParametersPackType = TTypePack<ParamTypes ...>;
		using QualifierSeqType = TFunctionQualifierSequence<EFunctionQualifier::Volatile>;
	};

	template <class RetType, class ... ParamTypes>
	struct ZzInternal_TBreakFunctionSignatureBase<RetType(ParamTypes ...) const volatile>
	{
		using ReturnType = RetType;
		using ParametersPackType = TTypePack<ParamTypes ...>;
		using QualifierSeqType = TFunctionQualifierSequence<EFunctionQualifier::Const | EFunctionQualifier::Volatile>;
	};

	template <class RetType, class ... ParamTypes>
	struct ZzInternal_TBreakFunctionSignatureBase<RetType(ParamTypes ...)&>
	{
		using ReturnType = RetType;
		using ParametersPackType = TTypePack<ParamTypes ...>;
		using QualifierSeqType = TFunctionQualifierSequence<EFunctionQualifier::LvalueRef>;
	};

	template <class RetType, class ... ParamTypes>
	struct ZzInternal_TBreakFunctionSignatureBase<RetType(ParamTypes ...) const&>
	{
		using ReturnType = RetType;
		using ParametersPackType = TTypePack<ParamTypes ...>;
		using QualifierSeqType = TFunctionQualifierSequence<EFunctionQualifier::Const 
															| EFunctionQualifier::LvalueRef>;
	};

	template <class RetType, class ... ParamTypes>
	struct ZzInternal_TBreakFunctionSignatureBase<RetType(ParamTypes ...) volatile&>
	{
		using ReturnType = RetType;
		using ParametersPackType = TTypePack<ParamTypes ...>;
		using QualifierSeqType = TFunctionQualifierSequence<EFunctionQualifier::Volatile 
															| EFunctionQualifier::LvalueRef>;
	};

	template <class RetType, class ... ParamTypes>
	struct ZzInternal_TBreakFunctionSignatureBase<RetType(ParamTypes ...) const volatile&>
	{
		using ReturnType = RetType;
		using ParametersPackType = TTypePack<ParamTypes ...>;
		using QualifierSeqType = TFunctionQualifierSequence<EFunctionQualifier::Const | EFunctionQualifier::Volatile
															| EFunctionQualifier::LvalueRef>;
	};

	template <class RetType, class ... ParamTypes>
	struct ZzInternal_TBreakFunctionSignatureBase<RetType(ParamTypes ...)&&>
	{
		using ReturnType = RetType;
		using ParametersPackType = TTypePack<ParamTypes ...>;
		using QualifierSeqType = TFunctionQualifierSequence<EFunctionQualifier::RvalueRef>;
	};

	template <class RetType, class ... ParamTypes>
	struct ZzInternal_TBreakFunctionSignatureBase<RetType(ParamTypes ...) const&&>
	{
		using ReturnType = RetType;
		using ParametersPackType = TTypePack<ParamTypes ...>;
		using QualifierSeqType = TFunctionQualifierSequence<EFunctionQualifier::Const
															| EFunctionQualifier::RvalueRef>;
	};

	template <class RetType, class ... ParamTypes>
	struct ZzInternal_TBreakFunctionSignatureBase<RetType(ParamTypes ...) volatile&&>
	{
		using ReturnType = RetType;
		using ParametersPackType = TTypePack<ParamTypes ...>;
		using QualifierSeqType = TFunctionQualifierSequence<EFunctionQualifier::Volatile
															| EFunctionQualifier::RvalueRef>;
	};

	template <class RetType, class ... ParamTypes>
	struct ZzInternal_TBreakFunctionSignatureBase<RetType(ParamTypes ...) const volatile&&>
	{
		using ReturnType = RetType;
		using ParametersPackType = TTypePack<ParamTypes ...>;
		using QualifierSeqType = TFunctionQualifierSequence<EFunctionQualifier::Const | EFunctionQualifier::Volatile
															| EFunctionQualifier::RvalueRef>;
	};

	template <class RetType, class ... ParamTypes>
	struct ZzInternal_TBreakFunctionSignatureBase<RetType(ParamTypes ...) noexcept>
	{
		using ReturnType = RetType;
		using ParametersPackType = TTypePack<ParamTypes ...>;
		using QualifierSeqType = TFunctionQualifierSequence<EFunctionQualifier::Noexcept>;
	};

	template <class RetType, class ... ParamTypes>
	struct ZzInternal_TBreakFunctionSignatureBase<RetType(ParamTypes ...) const noexcept>
	{
		using ReturnType = RetType;
		using ParametersPackType = TTypePack<ParamTypes ...>;
		using QualifierSeqType = TFunctionQualifierSequence<EFunctionQualifier::Const
															| EFunctionQualifier::Noexcept>;
	};

	template <class RetType, class ... ParamTypes>
	struct ZzInternal_TBreakFunctionSignatureBase<RetType(ParamTypes ...) volatile noexcept>
	{
		using ReturnType = RetType;
		using ParametersPackType = TTypePack<ParamTypes ...>;
		using QualifierSeqType = TFunctionQualifierSequence<EFunctionQualifier::Volatile
															| EFunctionQualifier::Noexcept>;
	};

	template <class RetType, class ... ParamTypes>
	struct ZzInternal_TBreakFunctionSignatureBase<RetType(ParamTypes ...) const volatile noexcept>
	{
		using ReturnType = RetType;
		using ParametersPackType = TTypePack<ParamTypes ...>;
		using QualifierSeqType = TFunctionQualifierSequence<EFunctionQualifier::Const | EFunctionQualifier::Volatile
															| EFunctionQualifier::Noexcept>;
	};

	template <class RetType, class ... ParamTypes>
	struct ZzInternal_TBreakFunctionSignatureBase<RetType(ParamTypes ...)& noexcept>
	{
		using ReturnType = RetType;
		using ParametersPackType = TTypePack<ParamTypes ...>;
		using QualifierSeqType = TFunctionQualifierSequence<EFunctionQualifier::LvalueRef>;
	};

	template <class RetType, class ... ParamTypes>
	struct ZzInternal_TBreakFunctionSignatureBase<RetType(ParamTypes ...) const& noexcept>
	{
		using ReturnType = RetType;
		using ParametersPackType = TTypePack<ParamTypes ...>;
		using QualifierSeqType = TFunctionQualifierSequence<EFunctionQualifier::Const
															| EFunctionQualifier::LvalueRef | EFunctionQualifier::Noexcept>;
	};

	template <class RetType, class ... ParamTypes>
	struct ZzInternal_TBreakFunctionSignatureBase<RetType(ParamTypes ...) volatile& noexcept>
	{
		using ReturnType = RetType;
		using ParametersPackType = TTypePack<ParamTypes ...>;
		using QualifierSeqType = TFunctionQualifierSequence<EFunctionQualifier::Volatile
															| EFunctionQualifier::LvalueRef | EFunctionQualifier::Noexcept>;
	};

	template <class RetType, class ... ParamTypes>
	struct ZzInternal_TBreakFunctionSignatureBase<RetType(ParamTypes ...) const volatile& noexcept>
	{
		using ReturnType = RetType;
		using ParametersPackType = TTypePack<ParamTypes ...>;
		using QualifierSeqType = TFunctionQualifierSequence<EFunctionQualifier::Const | EFunctionQualifier::Volatile
															| EFunctionQualifier::LvalueRef | EFunctionQualifier::Noexcept>;
	};

	template <class RetType, class ... ParamTypes>
	struct ZzInternal_TBreakFunctionSignatureBase<RetType(ParamTypes ...)&& noexcept>
	{
		using ReturnType = RetType;
		using ParametersPackType = TTypePack<ParamTypes ...>;
		using QualifierSeqType = TFunctionQualifierSequence<EFunctionQualifier::RvalueRef>;
	};

	template <class RetType, class ... ParamTypes>
	struct ZzInternal_TBreakFunctionSignatureBase<RetType(ParamTypes ...) const&& noexcept>
	{
		using ReturnType = RetType;
		using ParametersPackType = TTypePack<ParamTypes ...>;
		using QualifierSeqType = TFunctionQualifierSequence<EFunctionQualifier::Const
															| EFunctionQualifier::RvalueRef | EFunctionQualifier::Noexcept>;
	};

	template <class RetType, class ... ParamTypes>
	struct ZzInternal_TBreakFunctionSignatureBase<RetType(ParamTypes ...) volatile&& noexcept>
	{
		using ReturnType = RetType;
		using ParametersPackType = TTypePack<ParamTypes ...>;
		using QualifierSeqType = TFunctionQualifierSequence<EFunctionQualifier::Volatile
															| EFunctionQualifier::RvalueRef | EFunctionQualifier::Noexcept>;
	};

	template <class RetType, class ... ParamTypes>
	struct ZzInternal_TBreakFunctionSignatureBase<RetType(ParamTypes ...) const volatile&& noexcept>
	{
		using ReturnType = RetType;
		using ParametersPackType = TTypePack<ParamTypes ...>;
		using QualifierSeqType = TFunctionQualifierSequence<EFunctionQualifier::Const | EFunctionQualifier::Volatile
															| EFunctionQualifier::RvalueRef | EFunctionQualifier::Noexcept>;
	};
	
	template <class FuncType>
	struct ZzInternal_TBreakFunctionSignature : ZzInternal_TBreakFunctionSignatureBase<FuncType>
	{
		// Type of the underlying class object, if it applies
		using ObjectType = TNullTypeTag;
		using FunctionType = TNullTypeTag;
		using SignatureType = TNullTypeTag;
	};

	template <CFunction FuncType>
	struct ZzInternal_TBreakFunctionSignature<FuncType> : ZzInternal_TBreakFunctionSignatureBase<FuncType>
	{
		using ObjectType = void;
		using FunctionType = FuncType;
		using SignatureType = FunctionType;
	};

	template <CFunction FuncType>
	struct ZzInternal_TBreakFunctionSignature<FuncType*> : ZzInternal_TBreakFunctionSignatureBase<FuncType>
	{
		using ObjectType = void;
		using FunctionType = FuncType;
		using SignatureType = FunctionType*;
	};

	template <CFunction FuncType, CObject ObjType>
	struct ZzInternal_TBreakFunctionSignature<FuncType ObjType::*> : ZzInternal_TBreakFunctionSignatureBase<FuncType>
	{
		using ObjectType = ObjType;
		using FunctionType = FuncType;
		using SignatureType = FunctionType ObjectType::*;
	};

	template <class FuncType>
	using TBreakFunctionSignature = ZzInternal_TBreakFunctionSignature<TDecayType<FuncType>>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TMakeFunctionSignature

	template <class, class, class, class>
	struct ZzInternal_TMakeFunctionSignature
	{
		using Type = TNullTypeTag;
	};

#if 0
	template <class ReturnType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<void, ReturnType, TTypePack<ParamTypes ...>,
										     TFunctionQualifierSequence<>>
	{
		using Type = ReturnType(ParamTypes ...);
	};

	template <class ReturnType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<void, ReturnType, TTypePack<ParamTypes ...>,
										     TFunctionQualifierSequence<EFunctionQualifier::Const>>
	{
		using Type = ReturnType(ParamTypes ...) const;
	};

	template <class ReturnType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<void, ReturnType, TTypePack<ParamTypes ...>,
										     TFunctionQualifierSequence<EFunctionQualifier::Volatile>>
	{
		using Type = ReturnType(ParamTypes ...) volatile;
	};

	template <class ReturnType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<void, ReturnType, TTypePack<ParamTypes ...>,
										     TFunctionQualifierSequence<EFunctionQualifier::Const | EFunctionQualifier::Volatile>>
	{
		using Type = ReturnType(ParamTypes ...) const volatile;
	};

	template <class ReturnType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<void, ReturnType, TTypePack<ParamTypes ...>,
										     TFunctionQualifierSequence<EFunctionQualifier::LvalueRef>>
	{
		using Type = ReturnType(ParamTypes ...)&;
	};

	template <class ReturnType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<void, ReturnType, TTypePack<ParamTypes ...>,
										     TFunctionQualifierSequence<EFunctionQualifier::Const | EFunctionQualifier::LvalueRef>>
	{
		using Type = ReturnType(ParamTypes ...) const&;
	};

	template <class ReturnType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<void, ReturnType, TTypePack<ParamTypes ...>,
										     TFunctionQualifierSequence<EFunctionQualifier::Volatile | EFunctionQualifier::LvalueRef>>
	{
		using Type = ReturnType(ParamTypes ...) volatile&;
	};

	template <class ReturnType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<void, ReturnType, TTypePack<ParamTypes ...>,
										     TFunctionQualifierSequence<EFunctionQualifier::Const | EFunctionQualifier::Volatile | EFunctionQualifier::LvalueRef>>
	{
		using Type = ReturnType(ParamTypes ...) const volatile&;
	};

	template <class ReturnType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<void, ReturnType, TTypePack<ParamTypes ...>,
										     TFunctionQualifierSequence<EFunctionQualifier::RvalueRef>>
	{
		using Type = ReturnType(ParamTypes ...)&&;
	};

	template <class ReturnType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<void, ReturnType, TTypePack<ParamTypes ...>,
										     TFunctionQualifierSequence<EFunctionQualifier::Const | EFunctionQualifier::RvalueRef>>
	{
		using Type = ReturnType(ParamTypes ...) const&&;
	};

	template <class ReturnType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<void, ReturnType, TTypePack<ParamTypes ...>,
										     TFunctionQualifierSequence<EFunctionQualifier::Volatile | EFunctionQualifier::RvalueRef>>
	{
		using Type = ReturnType(ParamTypes ...) volatile&&;
	};

	template <class ReturnType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<void, ReturnType, TTypePack<ParamTypes ...>,
										     TFunctionQualifierSequence<EFunctionQualifier::Const | EFunctionQualifier::Volatile | EFunctionQualifier::RvalueRef>>
	{
		using Type = ReturnType(ParamTypes ...) const volatile&&;
	};

	template <class ReturnType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<void, ReturnType, TTypePack<ParamTypes ...>,
										     TFunctionQualifierSequence<EFunctionQualifier::Noexcept>>
	{
		using Type = ReturnType(ParamTypes ...) noexcept;
	};

	template <class ReturnType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<void, ReturnType, TTypePack<ParamTypes ...>,
										     TFunctionQualifierSequence<EFunctionQualifier::Const | EFunctionQualifier::Noexcept>>
	{
		using Type = ReturnType(ParamTypes ...) const noexcept;
	};

	template <class ReturnType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<void, ReturnType, TTypePack<ParamTypes ...>,
										     TFunctionQualifierSequence<EFunctionQualifier::Volatile | EFunctionQualifier::Noexcept>>
	{
		using Type = ReturnType(ParamTypes ...) volatile noexcept;
	};

	template <class ReturnType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<void, ReturnType, TTypePack<ParamTypes ...>,
										     TFunctionQualifierSequence<EFunctionQualifier::Const | EFunctionQualifier::Volatile | EFunctionQualifier::Noexcept>>
	{
		using Type = ReturnType(ParamTypes ...) const volatile noexcept;
	};

	template <class ReturnType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<void, ReturnType, TTypePack<ParamTypes ...>,
										     TFunctionQualifierSequence<EFunctionQualifier::LvalueRef | EFunctionQualifier::Noexcept>>
	{
		using Type = ReturnType(ParamTypes ...)& noexcept;
	};

	template <class ReturnType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<void, ReturnType, TTypePack<ParamTypes ...>,
										     TFunctionQualifierSequence<EFunctionQualifier::Const | EFunctionQualifier::LvalueRef | EFunctionQualifier::Noexcept>>
	{
		using Type = ReturnType(ParamTypes ...) const& noexcept;
	};

	template <class ReturnType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<void, ReturnType, TTypePack<ParamTypes ...>,
										     TFunctionQualifierSequence<EFunctionQualifier::Volatile | EFunctionQualifier::LvalueRef | EFunctionQualifier::Noexcept>>
	{
		using Type = ReturnType(ParamTypes ...) volatile& noexcept;
	};

	template <class ReturnType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<void, ReturnType, TTypePack<ParamTypes ...>,
										     TFunctionQualifierSequence<EFunctionQualifier::Const | EFunctionQualifier::Volatile | EFunctionQualifier::LvalueRef | EFunctionQualifier::Noexcept>>
	{
		using Type = ReturnType(ParamTypes ...) const volatile& noexcept;
	};

	template <class ReturnType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<void, ReturnType, TTypePack<ParamTypes ...>,
										     TFunctionQualifierSequence<EFunctionQualifier::RvalueRef | EFunctionQualifier::Noexcept>>
	{
		using Type = ReturnType(ParamTypes ...)&& noexcept;
	};

	template <class ReturnType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<void, ReturnType, TTypePack<ParamTypes ...>,
										     TFunctionQualifierSequence<EFunctionQualifier::Const | EFunctionQualifier::RvalueRef | EFunctionQualifier::Noexcept>>
	{
		using Type = ReturnType(ParamTypes ...) const&& noexcept;
	};

	template <class ReturnType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<void, ReturnType, TTypePack<ParamTypes ...>,
										     TFunctionQualifierSequence<EFunctionQualifier::Volatile | EFunctionQualifier::RvalueRef | EFunctionQualifier::Noexcept>>
	{
		using Type = ReturnType(ParamTypes ...) volatile&& noexcept;
	};

	template <class ReturnType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<void, ReturnType, TTypePack<ParamTypes ...>,
										     TFunctionQualifierSequence<EFunctionQualifier::Const | EFunctionQualifier::Volatile
										   							| EFunctionQualifier::RvalueRef | EFunctionQualifier::Noexcept>>
	{
		using Type = ReturnType(ParamTypes ...) const volatile&& noexcept;
	};
#endif

#if 0
	template <class RetType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<void, RetType, TTypePack<ParamTypes ...>,
											 TFunctionQualifierSequence<>>
	{
		using Type = RetType(ParamTypes ...);
	};
#endif

	template <class RetType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<void, RetType, TTypePack<ParamTypes ...>,
											 TFunctionQualifierSequence<>>
	{
		using Type = RetType(*)(ParamTypes ...);
	};

	template <class RetType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<void, RetType, TTypePack<ParamTypes ...>,
											 TFunctionQualifierSequence<EFunctionQualifier::Noexcept>>
	{
		using Type = RetType(*)(ParamTypes ...) noexcept;
	};

	template <class ObjType, class RetType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<ObjType, RetType, TTypePack<ParamTypes ...>,
											 TFunctionQualifierSequence<>>
	{
		using Type = RetType(ObjType::*)(ParamTypes ...);
	};

	template <class ObjType, class RetType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<ObjType, RetType, TTypePack<ParamTypes ...>,
											 TFunctionQualifierSequence<EFunctionQualifier::Const>>
	{
		using Type = RetType(ObjType::*)(ParamTypes ...) const;
	};

	template <class ObjType, class RetType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<ObjType, RetType, TTypePack<ParamTypes ...>,
											 TFunctionQualifierSequence<EFunctionQualifier::Volatile>>
	{
		using Type = RetType(ObjType::*)(ParamTypes ...) volatile;
	};

	template <class ObjType, class RetType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<ObjType, RetType, TTypePack<ParamTypes ...>,
											 TFunctionQualifierSequence<EFunctionQualifier::Const | EFunctionQualifier::Volatile>>
	{
		using Type = RetType(ObjType::*)(ParamTypes ...) const volatile;
	};

	template <class ObjType, class RetType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<ObjType, RetType, TTypePack<ParamTypes ...>,
											 TFunctionQualifierSequence<EFunctionQualifier::LvalueRef>>
	{
		using Type = RetType(ObjType::*)(ParamTypes ...)&;
	};

	template <class ObjType, class RetType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<ObjType, RetType, TTypePack<ParamTypes ...>,
											 TFunctionQualifierSequence<EFunctionQualifier::Const | EFunctionQualifier::LvalueRef>>
	{
		using Type = RetType(ObjType::*)(ParamTypes ...) const&;
	};

	template <class ObjType, class RetType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<ObjType, RetType, TTypePack<ParamTypes ...>,
											 TFunctionQualifierSequence<EFunctionQualifier::Volatile | EFunctionQualifier::LvalueRef>>
	{
		using Type = RetType(ObjType::*)(ParamTypes ...) volatile&;
	};

	template <class ObjType, class RetType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<ObjType, RetType, TTypePack<ParamTypes ...>,
											 TFunctionQualifierSequence<EFunctionQualifier::Const | EFunctionQualifier::Volatile
																		| EFunctionQualifier::LvalueRef>>
	{
		using Type = RetType(ObjType::*)(ParamTypes ...) const volatile&;
	};

	template <class ObjType, class RetType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<ObjType, RetType, TTypePack<ParamTypes ...>,
											 TFunctionQualifierSequence<EFunctionQualifier::RvalueRef>>
	{
		using Type = RetType(ObjType::*)(ParamTypes ...)&&;
	};

	template <class ObjType, class RetType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<ObjType, RetType, TTypePack<ParamTypes ...>,
											 TFunctionQualifierSequence<EFunctionQualifier::Const | EFunctionQualifier::RvalueRef>>
	{
		using Type = RetType(ObjType::*)(ParamTypes ...) const&&;
	};

	template <class ObjType, class RetType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<ObjType, RetType, TTypePack<ParamTypes ...>,
											 TFunctionQualifierSequence<EFunctionQualifier::Volatile | EFunctionQualifier::RvalueRef>>
	{
		using Type = RetType(ObjType::*)(ParamTypes ...) volatile&&;
	};

	template <class ObjType, class RetType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<ObjType, RetType, TTypePack<ParamTypes ...>,
											 TFunctionQualifierSequence<EFunctionQualifier::Const | EFunctionQualifier::Volatile
																		| EFunctionQualifier::RvalueRef>>
	{
		using Type = RetType(ObjType::*)(ParamTypes ...) const volatile&&;
	};

	template <class ObjType, class RetType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<ObjType, RetType, TTypePack<ParamTypes ...>,
											 TFunctionQualifierSequence<EFunctionQualifier::Noexcept>>
	{
		using Type = RetType(ObjType::*)(ParamTypes ...) noexcept;
	};

	template <class ObjType, class RetType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<ObjType, RetType, TTypePack<ParamTypes ...>,
											 TFunctionQualifierSequence<EFunctionQualifier::Const | EFunctionQualifier::Noexcept>>
	{
		using Type = RetType(ObjType::*)(ParamTypes ...) const noexcept;
	};

	template <class ObjType, class RetType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<ObjType, RetType, TTypePack<ParamTypes ...>,
											 TFunctionQualifierSequence<EFunctionQualifier::Volatile | EFunctionQualifier::Noexcept>>
	{
		using Type = RetType(ObjType::*)(ParamTypes ...) volatile noexcept;
	};

	template <class ObjType, class RetType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<ObjType, RetType, TTypePack<ParamTypes ...>,
											 TFunctionQualifierSequence<EFunctionQualifier::Const | EFunctionQualifier::Volatile
																		| EFunctionQualifier::Noexcept>>
	{
		using Type = RetType(ObjType::*)(ParamTypes ...) const volatile noexcept;
	};

	template <class ObjType, class RetType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<ObjType, RetType, TTypePack<ParamTypes ...>,
											 TFunctionQualifierSequence<EFunctionQualifier::LvalueRef | EFunctionQualifier::Noexcept>>
	{
		using Type = RetType(ObjType::*)(ParamTypes ...)& noexcept;
	};

	template <class ObjType, class RetType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<ObjType, RetType, TTypePack<ParamTypes ...>,
											 TFunctionQualifierSequence<EFunctionQualifier::Const | EFunctionQualifier::LvalueRef
																		| EFunctionQualifier::Noexcept>>
	{
		using Type = RetType(ObjType::*)(ParamTypes ...) const& noexcept;
	};

	template <class ObjType, class RetType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<ObjType, RetType, TTypePack<ParamTypes ...>,
											 TFunctionQualifierSequence<EFunctionQualifier::Volatile | EFunctionQualifier::LvalueRef
																		| EFunctionQualifier::Noexcept>>
	{
		using Type = RetType(ObjType::*)(ParamTypes ...) volatile& noexcept;
	};

	template <class ObjType, class RetType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<ObjType, RetType, TTypePack<ParamTypes ...>,
											 TFunctionQualifierSequence<EFunctionQualifier::Const | EFunctionQualifier::Volatile
																		| EFunctionQualifier::LvalueRef | EFunctionQualifier::Noexcept>>
	{
		using Type = RetType(ObjType::*)(ParamTypes ...) const volatile& noexcept;
	};

	template <class ObjType, class RetType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<ObjType, RetType, TTypePack<ParamTypes ...>,
											 TFunctionQualifierSequence<EFunctionQualifier::RvalueRef | EFunctionQualifier::Noexcept>>
	{
		using Type = RetType(ObjType::*)(ParamTypes ...)&& noexcept;
	};

	template <class ObjType, class RetType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<ObjType, RetType, TTypePack<ParamTypes ...>,
											 TFunctionQualifierSequence<EFunctionQualifier::Const | EFunctionQualifier::RvalueRef
																		| EFunctionQualifier::Noexcept>>
	{
		using Type = RetType(ObjType::*)(ParamTypes ...) const&& noexcept;
	};

	template <class ObjType, class RetType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<ObjType, RetType, TTypePack<ParamTypes ...>,
											 TFunctionQualifierSequence<EFunctionQualifier::Volatile | EFunctionQualifier::RvalueRef
																		| EFunctionQualifier::Noexcept>>
	{
		using Type = RetType(ObjType::*)(ParamTypes ...) volatile&& noexcept;
	};

	template <class ObjType, class RetType, class ... ParamTypes>
	struct ZzInternal_TMakeFunctionSignature<ObjType, RetType, TTypePack<ParamTypes ...>,
											 TFunctionQualifierSequence<EFunctionQualifier::Const | EFunctionQualifier::Volatile
																		| EFunctionQualifier::RvalueRef | EFunctionQualifier::Noexcept>>
	{
		using Type = RetType(ObjType::*)(ParamTypes ...) const volatile&& noexcept;
	};

	template <class ObjType, class RetType, class ParamPackType, class QualSeqType>
	using TMakeFunctionSignature = typename ZzInternal_TMakeFunctionSignature<ObjType, RetType, ParamPackType, QualSeqType>::Type;

}

#endif
