#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CORE_COREDEBUGGER
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CORE_COREDEBUGGER

#include "CorePreprocessor.hpp"

// TODO reorganize this file, move related macros/functions closer together

// TODO probably remove this altogether (otherwise, move it to a specific I/O file)
#if AEON_PLATFORM_WINDOWS and not AEON_USING_FMT_LIBRARY
#	if __cpp_lib_format < 202207L
// HACK The P2508R1 DR, which exposes the std::basic_format_string facility,
// seems to not yet have been merged (https://github.com/microsoft/STL/pull/3074)
namespace std
{
	template <class CharType, class ... ArgTypes>
	using basic_format_string = std::_Basic_format_string<CharType, std::type_identity_t<ArgTypes> ...>;
}
#	endif
#endif

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// AEON_SOURCE_LINE_INFO

#define AEON_SOURCE_LINE_INFO() ZZINTERNAL_AEON_SOURCE_LINE_INFO()

#if AEON_PLATFORM_WINDOWS
#	if AEON_COMPILER_MSVC || (AEON_COMPILER_CLANG >= 16) || AEON_COMPILER_GCC
#		define ZZINTERNAL_AEON_SOURCE_LINE_INFO() \
			(::Aeon::Debugger::GWindowsSourceDebugInfo<AEON_MAKE_SOURCE_LINE_HASH()>{ __FILE__, __LINE__, __func__, __FUNCSIG__ }\
				.AddSourceInfo(::std::source_location::current(), 0))
#	else
#		define ZZINTERNAL_AEON_SOURCE_LINE_INFO() \
			(::Aeon::Debugger::GWindowsSourceDebugInfo<AEON_MAKE_SOURCE_LINE_HASH()>{ __FILE__, __LINE__, __func__, __PRETTY_FUNCTION__ })
#	endif
#else
#	error Not implemented yet!
#endif

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// AEON_HAS_DEBUGGER

// TODO add locks to Debugger macros
// TODO doc Debugger macros

// TODO rename this (maybe AEON_HAS_DEBUGGER_ATTACHED)
#define AEON_HAS_DEBUGGER() ZZINTERNAL_AEON_HAS_DEBUGGER()

// TODO determine correct checks for each compiler-platform combination
#if AEON_PLATFORM_WINDOWS
// See: https://stackoverflow.com/questions/3268483/
#	define ZZINTERNAL_AEON_PLATFORM_HAS_DEBUGGER() \
		return ::IsDebuggerPresent();
#else
// See: https://stackoverflow.com/questions/3596781
#	error Not implemented yet!
#endif

#if AEON_BUILD_DEVELOPMENT or AEON_BUILD_DISTRIBUTION_DEBUG
#	define ZZINTERNAL_AEON_HAS_DEBUGGER() [] { ZZINTERNAL_AEON_PLATFORM_HAS_DEBUGGER() } ()
#else
// CHECKME is this really a good idea?
#	define ZZINTERNAL_AEON_HAS_DEBUGGER() [] { return false; } ()
#endif

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// AEON_DEBUGGER_BREAK

#define AEON_DEBUGGER_BREAK(...) ZZINTERNAL_AEON_TOKEN_CONCAT(ZZINTERNAL_AEON_OVERLOAD_DEBUGGER_BREAK_, AEON_LIST_SIZE(__VA_ARGS__))(__VA_ARGS__)

#define ZZINTERNAL_AEON_OVERLOAD_DEBUGGER_BREAK_1(Condition) ZZINTERNAL_AEON_DEBUGGER_BREAK_CONDITIONAL(Condition)
#define ZZINTERNAL_AEON_OVERLOAD_DEBUGGER_BREAK_0() ZZINTERNAL_AEON_DEBUGGER_BREAK()

// TODO determine correct checks for each compiler-platform(-architecture) combination
#if AEON_PLATFORM_WINDOWS
#	define ZZINTERNAL_AEON_PLATFORM_DEBUGGER_BREAK() \
		;{\
			::fflush(stdout);\
			/* ::DebugBreak causes IDEs to display disassembly of the function, this works better */\
			::__debugbreak();\
		}
#else
#error Not implemented yet!
// TODO test this
#	define ZZINTERNAL_AEON_PLATFORM_DEBUGGER_BREAK() \
    	::std::fflush(stdout);\
		::std::raise(SIGTRAP);
#endif

#if AEON_BUILD_DEVELOPMENT or AEON_BUILD_DISTRIBUTION_DEBUG
#	define ZZINTERNAL_AEON_DEBUGGER_BREAK_CONDITIONAL(Condition) \
		;{                                                                                                                                           \
		    if (AEON_HAS_DEBUGGER())                                                                                                                 \
		    {                                                                                                                                        \
		        thread_local static bool volatile AEON_TOKEN_CONCAT(AEON_DEBUGGER_BREAK_, __LINE__, _ShouldBreak) = true;                            \
		        bool AEON_TOKEN_CONCAT(AEON_DEBUGGER_BREAK_, __LINE__, _Condition) = Condition;                                                      \
		                                                                                                                                             \
		        if (AEON_TOKEN_CONCAT(AEON_DEBUGGER_BREAK_, __LINE__, _ShouldBreak) &&                                                               \
		            AEON_TOKEN_CONCAT(AEON_DEBUGGER_BREAK_, __LINE__, _Condition))                                                                   \
		        {                                                                                                                                    \
		        	ZZINTERNAL_AEON_PLATFORM_DEBUGGER_BREAK()																						 \
		        }                                                                                                                                    \
		    }                                                                                                                                        \
		}
#	define ZZINTERNAL_AEON_DEBUGGER_BREAK() \
		;{                                                                                                                                           \
		    if (AEON_HAS_DEBUGGER())                                                                                                                 \
		    {                                                                                                                                        \
	        	ZZINTERNAL_AEON_PLATFORM_DEBUGGER_BREAK()																						     \
		    }                                                                                                                                        \
		}
#else
#	define ZZINTERNAL_AEON_DEBUGGER_BREAK_CONDITIONAL(Condition)
#	define ZZINTERNAL_AEON_DEBUGGER_BREAK()
#endif

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// AEON_DEBUGGER_PRINT, AEON_DEBUGGER_FORMAT_PRINT

// NOTE these numbers have meaning, they represent the ANSI escape code of the color to display
#define ZZINTERNAL_AEON_DEBUGGER_PRINT_CATEGORY_Error 1
#define ZZINTERNAL_AEON_DEBUGGER_PRINT_CATEGORY_Info 2
#define ZZINTERNAL_AEON_DEBUGGER_PRINT_CATEGORY_Warn 3

#define AEON_DEBUGGER_PRINT(Category, MessageLiteral) \
	ZZINTERNAL_AEON_DEBUGGER_PRINT(AEON_TOKEN_CONCAT(ZZINTERNAL_AEON_DEBUGGER_PRINT_CATEGORY_, Category), MessageLiteral)

#define AEON_DEBUGGER_FORMAT_PRINT(Category, MessageLiteral, ...) \
	ZZINTERNAL_AEON_DEBUGGER_FORMAT_PRINT(AEON_TOKEN_CONCAT(ZZINTERNAL_AEON_DEBUGGER_PRINT_CATEGORY_, Category), MessageLiteral, __VA_ARGS__)

// TODO determine correct checks for each compiler-platform combination
// FIXME use STATIC_COUNTER instead of __LINE__ to choose mutex
#if AEON_PLATFORM_WINDOWS
#	define ZZINTERNAL_AEON_PLATFORM_DEBUGGER_PRINT(Message, MessageLength) \
        ::Aeon::Debugger::ZzInternal::DebuggerPrintMutex.lock();\
		::_fflush_nolock(stdout);\
		/* Only works with LLDB (e.g. CLion) */\
		::std::fwrite(Message, sizeof(char), MessageLength, stdout);\
		/* Only works with WinDbg/msvsmon (e.g. Visual Studio) */\
		::OutputDebugStringA(Message);\
		::Aeon::Debugger::ZzInternal::DebuggerPrintMutex.unlock();
#else
#	error Not implemented yet!
#endif

#if AEON_BUILD_DEVELOPMENT or AEON_BUILD_DISTRIBUTION_DEBUG
#	define ZZINTERNAL_AEON_DEBUGGER_PRINT(Category, MessageLiteral) \
		;{                                                                                                                                           \
	        if (AEON_HAS_DEBUGGER())                                                                                                                 \
	        {                                                                                                                                        \
				[] <szint MessageLengthValue> (char const(& Message)[MessageLengthValue])                                                            \
                {                                                                                                                                    \
                    ZZINTERNAL_AEON_PLATFORM_DEBUGGER_PRINT(Message, MessageLengthValue - 1)                                                         \
                }                                                                                                                                    \
			    ("\x1B[38;5;" AEON_TOKEN_STR(Category) "m" MessageLiteral "\x1B[0m\n");                                                              \
	        }                                                                                                                                        \
		}
#else
#	define ZZINTERNAL_AEON_DEBUGGER_PRINT(Category, MessageLiteral)
#endif

#if AEON_BUILD_DEVELOPMENT or AEON_BUILD_DISTRIBUTION_DEBUG
#if 0
#	define ZZINTERNAL_AEON_DEBUGGER_FORMAT_PRINT(MessageLiteral, ...) \
		;{                                                                                                                                           \
			if (AEON_HAS_DEBUGGER())                                                                                                                 \
			{                                                                                                                                        \
			    [] <szint FormatStringLengthValue, class ... FormatDataTypes>                                                                        \
					(char const(& FormatString)[FormatStringLengthValue], FormatDataTypes&& ... FormatData)                                          \
			    {                                                                                                                                    \
			        ptrdffint MessageLength = ::std::snprintf(nullptr, 0, FormatString, ::std::forward<FormatDataTypes>(FormatData) ...) + 1;        \
			        char* Message = ::new (::std::malloc(MessageLength)) char[MessageLength];                                                        \
			        ::std::snprintf(Message, MessageLength, FormatString, ::std::forward<FormatDataTypes>(FormatData) ...);                          \
			                                                                                                                                         \
			        ZZINTERNAL_AEON_PLATFORM_DEBUGGER_PRINT(Message, ::std::strlen(Message))                                                         \
					                                                                                                                                 \
			        ::std::free(Message);                                                                                                            \
			    }                                                                                                                                    \
			    ("" MessageLiteral "\n" __VA_OPT__(,) __VA_ARGS__);                                                                                  \
			}                                                                                                                                        \
		}
#else
#	define ZZINTERNAL_AEON_DEBUGGER_FORMAT_PRINT(Category, MessageLiteral, ...) \
		;{                                                                                                                                           \
			if (AEON_HAS_DEBUGGER())                                                                                                                 \
			{                                                                                                                                        \
			    [] <class ... FormatDataTypes>                                                                                                       \
					(ZZINTERNAL_AEON_FMT_NS::basic_format_string<char, typename ::Aeon::TClass<FormatDataTypes>::Type ...> FormatString,             \
					 FormatDataTypes&& ... FormatData)                                                                                               \
			    {                                                                                                                                    \
			        ::std::string Message = ::ZZINTERNAL_AEON_FMT_NS::format(FormatString, ::std::forward<FormatDataTypes>(FormatData) ...);         \
			                                                                                                                                         \
			        ZZINTERNAL_AEON_PLATFORM_DEBUGGER_PRINT(Message.data(), Message.size())                                                          \
			    }                                                                                                                                    \
			    ("\x1B[38;5;" AEON_TOKEN_STR(Category) "m" MessageLiteral "\x1B[0m\n" __VA_OPT__(,) __VA_ARGS__);                                    \
			}                                                                                                                                        \
		}
#endif
#else
#	define ZZINTERNAL_AEON_DEBUGGER_FORMAT_PRINT(MessageLiteral, ...) AEON_UNUSED(__VA_ARGS__)
#endif

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// AEON_ASSERT, AEON_EVAL_ASSERT, AEON_FAILSAFE

// From https://groups.google.com/g/comp.lang.c++.moderated/c/jgI11YyGNcI/m/ZM518aZd4vQJ
//	"The more experience I have, the more I'm convinced that a failing
//	assertion should reveal *a bug*. The corollary is that if it's not a
//	bug, assertions are not the correct way to handle it.
//
//	So that means a few important things:
//	1. Assertions in production code are a good thing. Test before you
//	ship if you're afraid they might fail.
//
//	2. A failing assertion should crash immediately (after due logging).
//	Not throw an exception, not return, just crash. Don't let your crazy
//	app corrupt the user's data.
//
//	3. The assert-and-if pattern as shown above is always wrong: either
//	assert(), or handle it, but not both."

// AEON_ASSERT(Expression[, Message])
// Checks whether 'Expression' evaluates to true at run-time, crashing if it evaluates to false.
// Optionally prints 'Message' on failure, which should be a string literal.
// Only works in Development builds; in Production (and Production Debug) builds, it does nothing (does NOT evaluate Expression).
#define	AEON_ASSERT(Expression, ...) ZZINTERNAL_AEON_ASSERT(Expression, __VA_ARGS__)

// AEON_EVAL_ASSERT(Expression[, Message])
// Checks whether 'Expression' evaluates to true at run-time, crashing if it evaluates to false.
// Optionally prints 'Message' on failure, which should be a string literal.
// Only works in Development builds; in Production (and Production Debug) builds, it simply evaluates Expression.
#define	AEON_EVAL_ASSERT(Expression, ...) ZZINTERNAL_AEON_EVAL_ASSERT(Expression, __VA_ARGS__)

// AEON_CONSTEVAL_ASSERT(Expression)
// Checks whether 'Expression' evaluates to true in a constant-evaluated context, failing compilation if it evaluates to false.
#define	AEON_CONSTEVAL_ASSERT(Expression) ZZINTERNAL_AEON_CONSTEVAL_ASSERT(Expression)

// AEON_FAILSAFE(Expression, FailureExpression[, Message])
// Checks whether 'Expression' evaluates to true during run-time, returning 'FailureExpression' from the calling context if it evaluates to false
// Optionally prints 'Message' on failure, which should be a string literal
#define AEON_FAILSAFE(Expression, FailureExpression, ...) ZZINTERNAL_AEON_FAILSAFE(Expression, FailureExpression, __VA_ARGS__)

// TODO allow enabling ASSERTs in non-dev builds through some build flag

#if AEON_BUILD_DEVELOPMENT
#	define ZZINTERNAL_AEON_ASSERT(Expression, ...) \
		ZZINTERNAL_AEON_RUNTIME_ASSERT("AEON_ASSERT", ::Aeon::Debugger::ZzInternal::TCheckAssertArguments, Error, /*true*/, \
									   Expression, ::Aeon::Debugger::CrashProgram(true, 0), __VA_ARGS__)
#else // not AEON_BUILD_DEVELOPMENT
// Just ensure correct syntax in non-debug builds
#	define ZZINTERNAL_AEON_ASSERT(Expression, ...) \
		;{                                                                                                                                           \
			static_assert(::Aeon::TIsContextuallyConvertibleClass<decltype(Expression), bool>::Value,                                                \
						  "Expression '" AEON_TOKEN_STR(Expression) "' is not contextually convertible to 'bool'");                                  \
			static_assert(::Aeon::Debugger::ZzInternal::TCheckAssertArguments<AEON_LIST_APPLY(decltype, __VA_ARGS__)>::Value);                       \
		}
#endif

#if AEON_BUILD_DEVELOPMENT
#	define ZZINTERNAL_AEON_EVAL_ASSERT(Expression, ...) \
		ZZINTERNAL_AEON_RUNTIME_ASSERT("AEON_EVAL_ASSERT", ::Aeon::Debugger::ZzInternal::TCheckAssertArguments, Error, /*true*/, \
									   Expression, ::Aeon::Debugger::CrashProgram(true, 0), __VA_ARGS__)
#else // not AEON_BUILD_DEVELOPMENT
#	define ZZINTERNAL_AEON_EVAL_ASSERT(Expression, ...) \
		;{                                                                                                                                           \
			static_assert(::Aeon::TIsContextuallyConvertibleClass<decltype(Expression), bool>::Value,                                                \
						  "Expression '" AEON_TOKEN_STR(Expression) "' is not contextually convertible to 'bool'");                                  \
			static_assert(::Aeon::Debugger::ZzInternal::TCheckAssertArguments<AEON_LIST_APPLY(decltype, __VA_ARGS__)>::Value);                       \
            /* Evaluate expression, any side-effects will be executed this way */                                                                    \
			static_cast<void>(Expression);                                                                                                           \
		}
#endif

#define ZZINTERNAL_AEON_CONSTEVAL_ASSERT(Expression) \
	;{                                                                                                                                               \
        if_consteval                                                                                                                                 \
        {                                                                                                                                            \
        	if (Expression);                                                                                                                         \
        	else                                                                                                                                     \
        	{                                                                                                                                        \
        	    throw; /* Ill-formed expression in constexpr context. */                                                                             \
        	}                                                                                                                                        \
        }                                                                                                                                            \
	}

// FIXME remove debugger instrumentation in PROD builds (but not in PROD_DEBUG!)

#define ZZINTERNAL_AEON_FAILSAFE(Expression, FailureExpression, ...) \
	;{                                                                                                                                               \
        [[maybe_unused]] thread_local static uint8 AEON_TOKEN_CONCAT(AEON_FAILSAFE_, __LINE__, _ShouldBreak) = 0b11;                                 \
        struct AEON_TOKEN_CONCAT(ZZINTERNAL_AEON_FAILSAFE_, __LINE__, _SExecuteOnceGuard)                                                            \
        {                                                                                                                                            \
			AEON_TOKEN_CONCAT(ZZINTERNAL_AEON_FAILSAFE_, __LINE__, _SExecuteOnceGuard)(uint8& NewShouldBreakRef)                                     \
				: ShouldBreakRef{ NewShouldBreakRef }                                                                                                \
			{                                                                                                                                        \
			}                                                                                                                                        \
			~AEON_TOKEN_CONCAT(ZZINTERNAL_AEON_FAILSAFE_, __LINE__, _SExecuteOnceGuard)()                                                            \
			{                                                                                                                                        \
				if (ShouldBreakRef == 0b01)                                                                                                          \
                {                                                                                                                                    \
                	ShouldBreakRef = 0b00;                                                                                                           \
                }                                                                                                                                    \
			}                                                                                                                                        \
            operator bool()                                                                                                                          \
			{                                                                                                                                        \
				return ShouldBreakRef &= 0b01;                                                                                                       \
			}                                                                                                                                        \
			uint8& ShouldBreakRef;                                                                                                                   \
        };                                                                                                                                           \
        [[maybe_unused]] AEON_TOKEN_CONCAT(ZZINTERNAL_AEON_FAILSAFE_, __LINE__, _SExecuteOnceGuard)                                                  \
		AEON_TOKEN_CONCAT(ZzInternal_AEON_FAILSAFE_, __LINE__, _ShouldBreak){ AEON_TOKEN_CONCAT(AEON_FAILSAFE_, __LINE__, _ShouldBreak) };           \
                                                                                                                                                     \
		ZZINTERNAL_AEON_RUNTIME_ASSERT("AEON_FAILSAFE", ::Aeon::Debugger::ZzInternal::TCheckFailsafeArguments, Warn,                                 \
									   AEON_TOKEN_CONCAT(ZzInternal_AEON_FAILSAFE_, __LINE__, _ShouldBreak),                                         \
									   Expression, FailureExpression, __VA_ARGS__);                                                                  \
	}

#define ZZINTERNAL_AEON_RUNTIME_ASSERT(MacroName, CheckTrait, Category, ShouldBreak, Expression, FailureExpression, ...) \
	;{                                                                                                                                               \
		if constexpr (CheckTrait<AEON_LIST_APPLY(decltype, __VA_ARGS__)>::Value)                                                                     \
		{                                                                                                                                            \
            /*ZZINTERNAL_AEON_CONSTEVAL_ASSERT(Expression);*/                                                                                        \
			if (Expression);                                                                                                                         \
			else                                                                                                                                     \
	    	{                                                                                                                                        \
	    	    AEON_DEBUGGER_PRINT(Category,                                                                                                        \
									AEON_LIST_HEAD(__VA_ARGS__ __VA_OPT__(,)                                                                         \
												   MacroName " failure: Expression '" AEON_TOKEN_STR(Expression) "' evaluated to 'false'"));         \
	    	    AEON_DEBUGGER_BREAK(ShouldBreak);                                                                                                    \
	    	    FailureExpression;                                                                                                                   \
	    	}                                                                                                                                        \
		}                                                                                                                                            \
	}

namespace Aeon::Debugger
{
	namespace ZzInternal
	{
		inline std::mutex DebuggerPrintMutex;

		template <class ... Types>
		struct TCheckAssertArguments
		{
			static_assert(VAlwaysFalse<TTypePack<Types ...>>,
						  "'AEON_ASSERT(Expression[, Message])': "
						  "Macro must have either one or two parameters");
			static constexpr bool Value = false;
		};

		template <class Type>
		struct TCheckAssertArguments<Type>
		{
			static_assert(VAlwaysFalse<Type>,
						  "'AEON_ASSERT(Expression[, Message])': "
						  "The optional 'Message' parameter must be a string literal");
			static constexpr bool Value = false;
		};

		template <szint LengthValue>
		struct TCheckAssertArguments<char const(&)[LengthValue]>
		{
			static constexpr bool Value = true;
		};

		template <>
		struct TCheckAssertArguments<>
		{
			static constexpr bool Value = true;
		};

		template <class ... Types>
		struct TCheckFailsafeArguments
		{
			static_assert(VAlwaysFalse<TTypePack<Types ...>>,
						  "'AEON_FAILSAFE(Expression, FailureExpression[, Message])': "
						  "Macro must have either two or three parameters");
			static constexpr bool Value = false;
		};

		template <class Type>
		struct TCheckFailsafeArguments<Type>
		{
			static_assert(VAlwaysFalse<Type>,
						  "'AEON_FAILSAFE(Expression, FailureExpression[, Message])': "
						  "The optional 'Message' parameter must be a string literal");
			static constexpr bool Value = false;
		};

		template <szint LengthValue>
		struct TCheckFailsafeArguments<char const(&)[LengthValue]>
		{
			static constexpr bool Value = true;
		};

		template <>
		struct TCheckFailsafeArguments<>
		{
			static constexpr bool Value = true;
		};
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ISourceDebugInfo

	struct ISourceDebugInfo
	{
	protected:
		~ISourceDebugInfo() = default;

	public:
		constexpr virtual char const* FileName() const = 0;
		constexpr virtual char const* FilePath() const = 0;

		constexpr virtual char const* FuncName() const = 0;
		constexpr virtual char const* FuncSig() const = 0;

		constexpr virtual uint32 LineNum() const = 0;
		constexpr virtual uint32 LineChar() const = 0;

		constexpr virtual char const* LineText() const = 0;

	};

#if AEON_PLATFORM_WINDOWS
	template <uint64 SourceLineHashValue>
	class GWindowsSourceDebugInfo final : public ISourceDebugInfo
	{
	public:
		explicit constexpr GWindowsSourceDebugInfo(char const* NewFilePath, uint32 NewLineNum,
												   char const* NewFuncName, char const* NewFuncSig)
			: FilePath_Internal(NewFilePath)
			, FileName_Internal("")
			, FuncName_Internal(NewFuncName)
			, FuncSig_Internal(NewFuncSig)
			, LineNum_Internal(NewLineNum)
			, LineChar_Internal(0)
		{
		}
		~GWindowsSourceDebugInfo() = default;

#	if AEON_COMPILER_MSVC || (AEON_COMPILER_CLANG >= 16) || AEON_COMPILER_GCC
		GWindowsSourceDebugInfo& AddSourceInfo(std::source_location const& NewSourceInfo, uint32 LineCharOffset)
		{
			FileName_Internal 	= NewSourceInfo.file_name();
		//	FuncName_Internal 	= NewSourceInfo.function_name();
		//	LineNum_Internal 	= NewSourceInfo.line();
			// NOTE While source_location seemingly returns the column, in some implementations (e.g. MSVC) it actually returns the character,
			//  which is subtly different from the column (which is usually what is reported by IDEs).
			//  For example, even in plain ASCII, tabs are enough to create a discrepancy between these values, and additionally,
			//  there is no simple solution even if number of tabs is known,
			//  as user configuration may change the size of a tab (in number of characters, thus changing the column).
			LineChar_Internal 	= NewSourceInfo.column() - LineCharOffset;
			return *this;
		}
#	endif

		constexpr virtual char const* FilePath() const override
		{
			return FilePath_Internal;
		}
		constexpr virtual char const* FileName() const override
		{
			return FileName_Internal;
		}
		constexpr virtual char const* FuncName() const override
		{
			return FuncName_Internal;
		}
		constexpr virtual char const* FuncSig() const override
		{
			return FuncSig_Internal;
		}
		constexpr virtual uint32 LineNum() const override
		{
			return LineNum_Internal;
		}
		constexpr virtual uint32 LineChar() const override
		{
			return LineChar_Internal;
		}

		virtual char const* LineText() const override
		{
			// Initialize variable using lambda;
			// note that this can be static because we have a different class template instance for every source line that uses this.
			static const std::string LineText =
				[] (char const* FilePath, uint32 LineNum)
				{
					if (auto SourceFile = std::ifstream{ FilePath })
					{
						// \x08, \b -> backspace
						// \x09, \t -> char tab
						// \x0A, \n -> line feed
						// \x0B, \v -> line tab
						// \x0C, \f -> form feed
						// \x0D, \r -> carriage return
						// \x20 -> space
						// \x5C -> backslash
						constexpr std::string_view TrimmableCharacters = "\b\t\n\v\f\r\x20\x5C";

						// Note that, by definition, the line should always be non-empty.
						std::string SourceLine;

						while (--LineNum)
						{
							SourceFile.ignore(TNumericProperties<std::streamsize>::Maximum, '\n');
						}

						std::getline(SourceFile, SourceLine);

						// Note that if there are no trailing trimmable characters,
						// find_last_not_of will return an iterator to the last character,
						// so incrementing the iterator is always valid.
						SourceLine.erase(SourceLine.find_last_not_of(TrimmableCharacters) + 1);
						SourceLine.erase(0, SourceLine.find_first_not_of(TrimmableCharacters));

						return SourceLine;
					}
					return std::string{};
				}
				(FilePath_Internal, LineNum_Internal);

			return LineText.c_str();
		}

	private:
		char const* FilePath_Internal;
		char const* FileName_Internal;
		char const* FuncName_Internal;
		char const* FuncSig_Internal;
		uint32 LineNum_Internal;
		uint32 LineChar_Internal;
	};
#else
	// TODO add implementations as necessary
#endif

#if 0 // TODO remove this
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// EPrintStream, FormatPrint, FlushPendingPrint

	enum struct EPrintStream
	{
		Stdout,
		Stderr,
	};

	template <EPrintStream StreamValue = EPrintStream::Stdout, class ... FormatDataTypes>
	bool FormatPrint(ZZINTERNAL_AEON_FMT_NS::basic_format_string<char, typename TClass<FormatDataTypes>::Type ...> FormatString,
					 FormatDataTypes&& ... FormatData)
	{
		std::ostream* Stream;
		if constexpr (StreamValue == EPrintStream::Stdout)
		{
			Stream = addressof(std::cout);
		}
		else
		{
			Stream = addressof(std::cerr);
		}

		auto Iter = std::ostreambuf_iterator<char>(Stream->rdbuf());

		Iter = ZZINTERNAL_AEON_FMT_NS::format_to(Iter, FormatString, Forward<FormatDataTypes>(FormatData) ...);
		AEON_FAILSAFE(!Iter.failed(), false);

		Iter = '\n';
		AEON_FAILSAFE(!Iter.failed(), false);

		return true;
	}

	template <EPrintStream StreamValue = EPrintStream::Stdout>
	void FlushPendingPrints()
	{
		if constexpr (StreamValue == EPrintStream::Stdout)
		{
			std::cout.flush();
		}
		else
		{
			std::cerr.flush();
		}
	}
#endif

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SSymbolManager

#if AEON_PLATFORM_WINDOWS
	class SSymbolManager
	{
	public:
		// TODO instantiate this on some cpp
		static SSymbolManager* Instance()
		{
			static SSymbolManager Manager;
			return addressof(Manager);
		}

		void FindSymbolThroughAddress(DWORD64 Address, SYMBOL_INFO& SymbolInfo)
		{
			std::scoped_lock Lock{ Mutex };
			::SymFromAddr(ProcessHandle, Address, NULL, addressof(SymbolInfo));
		}

	private:
		SSymbolManager()
		{
			HANDLE ProcessPseudoHandle = ::GetCurrentProcess();
			::DuplicateHandle(/* hSourceProcessHandle */ ProcessPseudoHandle,
							  /* hSourceHandle */ ProcessPseudoHandle,
							  /* hTargetProcessHandle */ ProcessPseudoHandle,
							  /* lpTargetHandle */ addressof(ProcessHandle),
							  /* dwDesiredAccess */ 0,
							  /* bInheritHandle */ FALSE,
							  /* dwOption */ DUPLICATE_SAME_ACCESS);

			// CHECKME Is this handled correctly from within different modules?
			::SymInitialize(ProcessHandle, NULL, TRUE);
		}
		~SSymbolManager()
		{
			::SymCleanup(ProcessHandle);
		}

		// TODO replace this with Aeon type
		std::mutex Mutex;
		HANDLE ProcessHandle;
	};

	// TODO wrap these methods in some class

#if AEON_ARCHITECTURE_X86_64
	inline void StackUnwindFrame(CONTEXT& Context, KNONVOLATILE_CONTEXT_POINTERS& NVContext, UNWIND_HISTORY_TABLE& UnwindHistoryTable)
	{
		DWORD64 ModuleBaseAddress;

		RtlZeroMemory(addressof(NVContext), sizeof(KNONVOLATILE_CONTEXT_POINTERS));

		// Try to look up unwind metadata for the current function.
		if (PRUNTIME_FUNCTION FunctionTableEntry = RtlLookupFunctionEntry(Context.Rip, &ModuleBaseAddress, &UnwindHistoryTable))
		{
			PVOID ProgramCounterLocation;
			DWORD64 EstablisherFrame;

			// Call upon RtlVirtualUnwind to execute the unwind for us.
			RtlVirtualUnwind(UNW_FLAG_NHANDLER,
							 ModuleBaseAddress,
							 Context.Rip,
							 FunctionTableEntry,
							 addressof(Context),
							 addressof(ProgramCounterLocation),
							 addressof(EstablisherFrame),
							 addressof(NVContext));
		}
		else
		{
			// CHECKME In which platforms is this valid?
			AEON_DEBUGGER_BREAK();

			// Otherwise, if we don't have a FunctionTableEntry, then we've encountered a leaf function.
			// Adjust the stack appropriately.
			Context.Rip = *bit_cast<DWORD64*>(Context.Rsp);
			Context.Rsp += 8;
		}
	}

	inline void PrintProgramStack(szint FramesToSkip = 0)
	{
		// Based on http://www.nynaeve.net/Code/StackWalk64.cpp
		// https://stackoverflow.com/questions/17069557/function-call-stack-in-c

		auto SymbolManager = SSymbolManager::Instance();

		CONTEXT 						Context;
		KNONVOLATILE_CONTEXT_POINTERS 	NVContext;
		UNWIND_HISTORY_TABLE 			UnwindHistory;

		constexpr szint 				SymbolInfoSize = sizeof(SYMBOL_INFO);
		constexpr szint 				SymbolNameSize = 256;

		alignas(SYMBOL_INFO) byte 		SymbolInfoBuffer[SymbolInfoSize + SymbolNameSize];

		RtlZeroMemory(addressof(Context), sizeof(CONTEXT));
		RtlZeroMemory(addressof(NVContext), sizeof(KNONVOLATILE_CONTEXT_POINTERS));
		RtlZeroMemory(addressof(UnwindHistory), sizeof(UNWIND_HISTORY_TABLE));

		AEON_DEBUGGER_FORMAT_PRINT(Error, "Unwinding stack (skipping {} frames)", FramesToSkip);

		// First, we'll get the caller's context.
		RtlCaptureContext(addressof(Context));

		// This unwind loop intentionally skips the first call frame,
		// as it shall correspond to the call to this function, which we aren't interested in.
		szint Frame = FramesToSkip;
		do
		{
			StackUnwindFrame(Context, NVContext, UnwindHistory);
		}
		while (Context.Rip != 0 && Frame-- != 0);

		// If we reach an RIP of zero,
		// this means that we've walked off the end of the call stack and are done.
		while (Context.Rip != 0)
		{
			SYMBOL_INFO& SymbolInfo = *::new (SymbolInfoBuffer) SYMBOL_INFO{};
			SymbolInfo.SizeOfStruct = SymbolInfoSize;
			SymbolInfo.MaxNameLen = SymbolNameSize;

			// Locate the symbol information based on the instruction-pointer.
			SymbolManager->FindSymbolThroughAddress(Context.Rip, SymbolInfo);

			// Display the context.
			// Note that we don't bother showing the XMM context,
			// although we have the nonvolatile portion of it.
			AEON_DEBUGGER_FORMAT_PRINT(Error,
									   "Frame={:0>4} Name={} Address=0x{:0>16X}",
									   ++Frame, static_cast<char*>(SymbolInfo.Name), SymbolInfo.Address);

			AEON_DEBUGGER_FORMAT_PRINT(Error,
									   " >> Registers >>\n"
									   "\t RIP=0x{:0>16X}\n"
			//						   "\t RAX=0x{:0>16X} RCX=0x{:0>16X} RDX=0x{:0>16X} RBX=0x{:0>16X}\n"
									   "\t RSP=0x{:0>16X} RBP=0x{:0>16X} RSI=0x{:0>16X} RDI=0x{:0>16X}\n"
			//						   "\t R08=0x{:0>16X} R09=0x{:0>16X} R10=0x{:0>16X} R11=0x{:0>16X}\n"
									   "\t R12=0x{:0>16X} R13=0x{:0>16X} R14=0x{:0>16X} R15=0x{:0>16X}",
									   Context.Rip,
			//						   Context.Rax, Context.Rcx, Context.Rdx, Context.Rbx,
									   Context.Rsp, Context.Rbp, Context.Rsi, Context.Rdi,
			//						   Context.R8 , Context.R9 , Context.R10, Context.R11,
									   Context.R12, Context.R13, Context.R14, Context.R15);


			// If we have stack-based register stores, then display them here.
			AEON_DEBUGGER_PRINT(Error, " >> Stack-cached registers >>");

			bool HasCachedRegisters = false;
			for (szint Idx = 0; Idx < 16; ++Idx)
			{
				// HACK Not really unused, but stops compiler from complaining
				[[maybe_unused]]
				constexpr char const* RegisterNames[16] =
					{
						"RAX", "RCX", "RDX", "RBX",
						"RSP", "RBP", "RSI", "RDI",
						"R08", "R09", "R10", "R11",
						"R12", "R13", "R14", "R15"
					};

				if (NVContext.IntegerContext[Idx])
				{
					AEON_DEBUGGER_FORMAT_PRINT(Error,
											   "\t -> Register {} cached at 0x{:0>16X} (=> 0x{:0>16X})",
											   RegisterNames[Idx],
											   bit_cast<ptrint>(NVContext.IntegerContext[Idx]),
											   *NVContext.IntegerContext[Idx]);
					HasCachedRegisters = true;
				}
			}
			if (!HasCachedRegisters)
			{
				AEON_DEBUGGER_PRINT(Error, "\t ---");
			}

			StackUnwindFrame(Context, NVContext, UnwindHistory);
		}
	}
#else
#	error Not implemented yet!
	inline void PrintProgramStack(szint FramesToSkip = 0) { AEON_UNUSED(FramesToSkip); }
#endif

	AEON_NORETURN inline void CrashProgram(bool ShouldPrintStack, szint NumSkippedStackFrames)
	{
		if (ShouldPrintStack)
		{
			::Aeon::Debugger::PrintProgramStack(NumSkippedStackFrames + 1);
		}
    	// TODO register custom terminate handler (or register SIGABRT handler?)
    	//::std::terminate();
		std::abort();
	}
#endif // AEON_PLATFORM_WINDOWS
}

namespace AeonDebugger = Aeon::Debugger;

#endif