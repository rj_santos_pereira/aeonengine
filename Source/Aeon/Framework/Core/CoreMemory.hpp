#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CORE_COREMEMORY
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CORE_COREMEMORY

#include "CoreMath.hpp"

namespace Aeon::Memory
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Platform constants

	// The size of a byte in bits, typically 8 in most modern platforms
	inline constexpr szint PlatformByteBitSize = CHAR_BIT;

	// The size of the CPU word in bits, 32 or 64 bits in modern platforms
	inline constexpr szint PlatformWordBitSize = sizeof(voidptr) * PlatformByteBitSize;

	// The default alignment of any allocation through std::malloc and related functions
	inline constexpr algnint PlatformDefaultAllocAlignment = algnint(__STDCPP_DEFAULT_NEW_ALIGNMENT__);

#if AEON_ARCHITECTURE_X86
	// NOTE Technically, the AMD64 spec defines the physical address limit to be 52-bit,
	//  but that would increase the size of the DMemAllocInfo object,
	//  and most processors/OSs are currently limited to 47/48-bit regardless (as of 2024).
	//  See: https://stackoverflow.com/questions/52712084
	inline constexpr szint PlatformAddressSpaceSize
		= AEON_ARCHITECTURE_X86_64
		? 48 /* x86-64 long mode (256 TB) */
		: 32;

#if AEON_ARCHITECTURE_X86_64
	inline constexpr algnint PlatformScalarRegisterAlignment = 8_algn;
#else
	inline constexpr algnint PlatformScalarRegisterAlignment = 4_algn;
#endif

#if defined AEON_SUPPORTS_AVX512
	inline constexpr algnint PlatformVectorRegisterAlignment = 64_algn; // 512-bits
#elif defined AEON_SUPPORTS_AVX
	inline constexpr algnint PlatformVectorRegisterAlignment = 32_algn; // 256-bits
#elif defined AEON_SUPPORTS_SSE
	inline constexpr algnint PlatformVectorRegisterAlignment = 16_algn; // 128-bits
#endif

	// NOTE These constants are specific to x86
#if defined AEON_SUPPORTS_AVX512
	inline constexpr algnint x86PlatformAVX512Alignment = 64_algn; // 512-bits
#endif
#if defined AEON_SUPPORTS_AVX
	inline constexpr algnint x86PlatformAVXAlignment = 32_algn; // 256-bits
#endif
#if defined AEON_SUPPORTS_SSE
	inline constexpr algnint x86PlatformSSEAlignment = 16_algn; // 128-bits
#endif

#else
#	error Not implemented yet!
#endif

	// TODO these will likely need more code to be properly determined (check architecture)

	inline constexpr szint PlatformCacheLineSize = std::hardware_constructive_interference_size;

	inline constexpr szint PlatformCacheLinePrefetchSize =
#	if AEON_ARCHITECTURE_X86_64
		// NOTE See https://stackoverflow.com/a/52158819 and Peter Cordes comments for rationale
		std::hardware_constructive_interference_size * 2
#	else
		std::hardware_destructive_interference_size
#	endif
		;
}

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SizeOf, BitSizeOf, AlignOf, AddressOf

	namespace ZzInternal
	{
		template <CObjectElseVoid ObjType>
		AEON_FORCEINLINE constexpr szint SizeOf(szint ObjCount = 1) noexcept
		{
			return szint(sizeof(ObjType) * ObjCount);
		}
		template <>
		AEON_FORCEINLINE constexpr szint SizeOf<void>(szint) noexcept
		{
			return szint(0);
		}

		template <CObjectElseVoid ObjType>
		AEON_FORCEINLINE constexpr szint BitSizeOf(szint ObjCount = 1) noexcept
		{
			return szint(sizeof(ObjType) * ::Aeon::Memory::PlatformByteBitSize * ObjCount);
		}
		template <>
		AEON_FORCEINLINE constexpr szint BitSizeOf<void>(szint) noexcept
		{
			return szint(0);
		}

		template <CObjectElseVoid ObjType>
		AEON_FORCEINLINE constexpr algnint AlignOf() noexcept
		{
			return algnint(alignof(ObjType));
		}
		template <>
		AEON_FORCEINLINE constexpr algnint AlignOf<void>() noexcept
		{
			// Use byte alignment (which should be 1); this is usually more useful than returning 0, since alignment can never be 0
			return algnint(alignof(byte));
		}

		template <CObjectElseVoid ObjType>
		AEON_FORCEINLINE constexpr algnint AllocAlignOf() noexcept
		{
			return ::AeonMath::Max(algnint(alignof(ObjType)), ::Aeon::Memory::PlatformDefaultAllocAlignment);
		}
		template <>
		AEON_FORCEINLINE constexpr algnint AllocAlignOf<void>() noexcept
		{
			// Use default std::malloc alignment; this is usually more useful than returning 0, since alignment can never be 0
			return ::Aeon::Memory::PlatformDefaultAllocAlignment;
		}

		template <class ObjType>
		AEON_FORCEINLINE constexpr ObjType* AddressOf(ObjType& Object) noexcept
		{
			return addressof(Object);
		}
	}

	// NOTE: the function prototypes are declared just to be indexed by an IDE,
	//  the actual methods are invoked through the macros

	template <class ClassType>
	constexpr szint SizeOf(ClassType ObjType, szint ObjCount = 1) noexcept;
#define SizeOf(Type, ...) ZzInternal::SizeOf<Type>(__VA_ARGS__)

	template <class ClassType>
	constexpr szint BitSizeOf(ClassType ObjType, szint ObjCount = 1) noexcept;
#define BitSizeOf(Type, ...) ZzInternal::BitSizeOf<Type>(__VA_ARGS__)

	template <class ClassType>
	constexpr algnint AlignOf(ClassType ObjType) noexcept;
#define AlignOf(Type) ZzInternal::AlignOf<Type>()

#if 0
	template <class ClassType>
	constexpr algnint AllocAlignOf(ClassType ObjType) noexcept;
#define AllocAlignOf(Type) ZzInternal::AllocAlignOf<Type>()
#endif

	template <class ObjectType>
	constexpr ObjectType* AddressOf(ObjectType& Object) noexcept;
#define AddressOf(Expression) ZzInternal::AddressOf(Expression)
}

namespace Aeon::Memory
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  IsBlockPointerValid, IsBlockSizeValid, IsBlockAlignmentValid

	AEON_NODISCARD inline constexpr bool IsBlockPointerValid(cvvoidptr MemPtr, algnint Alignment) noexcept
	{
		return MemPtr != nullptr && AeonMath::ModPowerOfTwo(bit_cast<szint>(MemPtr), szint(Alignment)) == 0;
	}

	AEON_NODISCARD inline constexpr bool IsBlockSizeValid(szint Size, algnint Alignment) noexcept
	{
		return Size != 0_sz && AeonMath::ModPowerOfTwo(Size, szint(Alignment)) == 0;
	}

	AEON_NODISCARD inline constexpr bool IsBlockAlignmentValid(algnint Alignment) noexcept
	{
		return Alignment != 0_algn && AeonMath::IsPowerOfTwo(szint(Alignment));
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CheckBlockOverlap

	AEON_NODISCARD inline constexpr bool CheckBlockOverlap(cvvoidptr MemPtr1, cvvoidptr MemPtr2, szint Size) noexcept
	{
		return (MemPtr1 == MemPtr2)
			|| (MemPtr1 < MemPtr2 && cvbyteptr(MemPtr1) + Size > MemPtr2)
			|| (MemPtr2 < MemPtr1 && cvbyteptr(MemPtr2) + Size > MemPtr1);
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  StartObjectLifetime, StartObjectArrayLifetime

	// TODO consider renaming all overloads to StartLifetime

	template <CImplicitLifetimeObject ObjType>
	AEON_NODISCARD TAddPointer<ObjType> StartObjectLifetime(voidptr MemPtr) noexcept
	requires CNotArray<ObjType>
	{
		// NOTE See https://www.open-std.org/jtc1/sc22/wg21/docs/papers/2020/p0593r6.html
		//  and https://www.open-std.org/jtc1/sc22/wg21/docs/papers/2022/p2590r2.pdf,
		//  as well as https://stackoverflow.com/questions/76445860/implementation-of-stdstart-lifetime-as
		//  for details behind the code. Fair warning: this is some language-lawyer bullsh*t, I tell you...
		//  Note that this should not emit any machine code whatsoever, at least not with higher optimization flags.
		constexpr szint ObjSize = Aeon::SizeOf(ObjType);
		// NOTE See https://gcc.gnu.org/bugzilla/show_bug.cgi?id=95349#c1,
		//  and https://godbolt.org/z/43aPz8P6P
		//  std::launder is used to break escape analysis and ensure the compiler has no information on the origin of the pointer.
		//  GCC produces the wrong result when laundering MemPtr in the final statement, so it's also needed here
		//  (it would otherwise work "correctly" if the final std::launder were also omitted, but the logic wouldn't be _technically_ correct).
		auto MemLaunderedPtr = ::std::launder(static_cast<byteptr>(MemPtr));
		// We use std::memmove with the same 'source' and 'destination' address,
		// which "blesses" the storage pointed to MemPtr as potentially containing an implicit-lifetime object, per P0593R6.
		// This should emit no code, as the source and destination "overlap", so the bytes are preserved.
		// As an aside, 'new (Ptr) std::byte[Size]' results in indeterminate storage contents,
		// so it would not preserve the object representation of any existing objects, and would be incorrect.
		::std::memmove(MemPtr, MemLaunderedPtr, ObjSize);
		// Casting and laundering the pointer to an actual object marks it as 'alive' to the compiler, i.e.,
		// per P0593R6, the program only has well-defined behavior if the object already exists in storage, so the compiler makes it so.
		return ::std::launder(static_cast<ObjType*>(MemPtr));
	}
	template <CImplicitLifetimeObject ObjType>
	AEON_NODISCARD TAddPointer<ObjType const> StartObjectLifetime(cvoidptr MemPtr) noexcept
	requires CNotArray<ObjType>
	{
		return StartObjectLifetime<ObjType>(const_cast<voidptr>(MemPtr));
	}
	template <CImplicitLifetimeObject ObjType>
	AEON_NODISCARD TAddPointer<ObjType volatile> StartObjectLifetime(vvoidptr MemPtr) noexcept
	requires CNotArray<ObjType>
	{
		return StartObjectLifetime<ObjType>(const_cast<voidptr>(MemPtr));
	}
	template <CImplicitLifetimeObject ObjType>
	AEON_NODISCARD TAddPointer<ObjType const volatile> StartObjectLifetime(cvvoidptr MemPtr) noexcept
	requires CNotArray<ObjType>
	{
		return StartObjectLifetime<ObjType>(const_cast<voidptr>(MemPtr));
	}

	template <CBoundedArray ObjArrType> // Note that arrays are ILOs
	AEON_NODISCARD TAddPointer<TRemoveArrayDimension<ObjArrType>> StartObjectArrayLifetime(voidptr MemPtr) noexcept
	{
		// NOTE See https://www.open-std.org/jtc1/sc22/wg21/docs/papers/2023/p2679r2.pdf for reasoning behind the code;
		//  in particular, this deviates from 'start_lifetime_as/start_lifetime_as_array' by making
		//  StartObjectLifetime (analogous to start_lifetime_as) handle non-array objects,
		//  and StartObjectArrayLifetime (analogous to start_lifetime_as_array) handle both bounded and unbounded arrays.
		//  Consequently, the return type for bounded arrays also changes, as it returns the pointer to the first element.
		using ObjType = TRemoveArrayDimension<ObjArrType>;

	//	AEON_ASSERT(::Aeon::Memory::IsBlockPointerValid(MemPtr, ::Aeon::Memory::AlignOf(ObjType)));

		constexpr szint ObjArrSize = Aeon::SizeOf(ObjArrType);
		auto MemLaunderedPtr = ::std::launder(static_cast<byteptr>(MemPtr));
		::std::memmove(MemPtr, MemLaunderedPtr, ObjArrSize);
		return std::launder(static_cast<ObjType*>(MemPtr));
	}
	template <CBoundedArray ObjArrType>
	AEON_NODISCARD TAddPointer<TRemoveArrayDimension<ObjArrType> const> StartObjectArrayLifetime(cvoidptr MemPtr) noexcept
	{
		return StartObjectArrayLifetime<ObjArrType>(const_cast<voidptr>(MemPtr));
	}
	template <CBoundedArray ObjArrType>
	AEON_NODISCARD TAddPointer<TRemoveArrayDimension<ObjArrType> volatile> StartObjectArrayLifetime(vvoidptr MemPtr) noexcept
	{
		return StartObjectArrayLifetime<ObjArrType>(const_cast<voidptr>(MemPtr));
	}
	template <CBoundedArray ObjArrType>
	AEON_NODISCARD TAddPointer<TRemoveArrayDimension<ObjArrType> const volatile> StartObjectArrayLifetime(cvvoidptr MemPtr) noexcept
	{
		return StartObjectArrayLifetime<ObjArrType>(const_cast<voidptr>(MemPtr));
	}

	template <CUnboundedArray ObjArrType> // Note that arrays are ILOs
	AEON_NODISCARD TAddPointer<TRemoveArrayDimension<ObjArrType>> StartObjectArrayLifetime(voidptr MemPtr, szint NumObjs) noexcept
	{
		using ObjType = TRemoveArrayDimension<ObjArrType>;

		// CHECKME Should we really permit zero objects?
		// Note that the pointer is not dereferenceable when returning through this (consequently, we cannot launder it) per P2679R2
		if (NumObjs == 0)
		{
			return static_cast<ObjType*>(MemPtr);
		}

	//	AEON_ASSERT(::Aeon::Memory::IsBlockPointerValid(MemPtr, ::Aeon::Memory::AlignOf(ObjType)));

		szint ObjArrSize = Aeon::SizeOf(ObjType, NumObjs);
		auto MemLaunderedPtr = ::std::launder(static_cast<byteptr>(MemPtr));
		::std::memmove(MemPtr, MemLaunderedPtr, ObjArrSize);
		return ::std::launder(static_cast<ObjType*>(MemPtr));
	}
	template <CUnboundedArray ObjArrType>
	AEON_NODISCARD TAddPointer<TRemoveArrayDimension<ObjArrType> const> StartObjectArrayLifetime(cvoidptr MemPtr, szint NumObjs) noexcept
	{
		return StartObjectArrayLifetime<ObjArrType>(const_cast<voidptr>(MemPtr), NumObjs);
	}
	template <CUnboundedArray ObjArrType>
	AEON_NODISCARD TAddPointer<TRemoveArrayDimension<ObjArrType> volatile> StartObjectArrayLifetime(vvoidptr MemPtr, szint NumObjs) noexcept
	{
		return StartObjectArrayLifetime<ObjArrType>(const_cast<voidptr>(MemPtr), NumObjs);
	}
	template <CUnboundedArray ObjArrType>
	AEON_NODISCARD TAddPointer<TRemoveArrayDimension<ObjArrType> const volatile> StartObjectArrayLifetime(cvvoidptr MemPtr, szint NumObjs) noexcept
	{
		return StartObjectArrayLifetime<ObjArrType>(const_cast<voidptr>(MemPtr), NumObjs);
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MemConstruct, MemDestruct, MemAccess

// TODO consider moving these functions to Aeon namespace

	template <CScalarObject ObjType, class ... ArgTypes>
	AEON_SPEC(FORCEINLINE) // CHECKME Should this be NODISCARD?
	constexpr TAddPointer<ObjType> MemConstruct(TAddPointer<TPropagateConstVolatile<ObjType, void>> MemPtr, ArgTypes&& ... Args)
	requires CConstructible<ObjType, ArgTypes ...>
	{
		// Currently we don't support array creation through this, see also:
		//  https://stackoverflow.com/questions/8720425
		//  While it was corrected through a DR,
		//  we should be aware if we ever decide to support array creation.

	//	AEON_ASSERT(IsBlockPointerValid(MemPtr, Aeon::AlignOf(ObjType)));
		using DecayedObjType = TDecayType<ObjType>;
		return ::new(const_cast<voidptr>(MemPtr)) DecayedObjType(Forward<ArgTypes>(Args) ...);
	}

	template <CScalarObject ObjType>
	AEON_SPEC(NODISCARD, FORCEINLINE)
	constexpr TAddPointer<ObjType> MemAccess(TAddPointer<TPropagateConstVolatile<ObjType, void>> MemPtr) noexcept
	{
	//	AEON_ASSERT(IsBlockPointerValid(MemPtr, Aeon::AlignOf(ObjType)));
		// On the usage of std::launder:
		//	https://stackoverflow.com/questions/39382501
		//	https://stackoverflow.com/questions/68863124
		// On the usage of 'AnyType* -> voidptr -> static_cast<Type*>' over 'AnyType* -> reinterpret_cast<Type*>':
		//	https://stackoverflow.com/questions/310451
		//	https://stackoverflow.com/questions/1863069
		return ::std::launder(static_cast<TAddPointer<ObjType>>(MemPtr));
	}

	template <CScalarObject ObjType>
	AEON_SPEC(FORCEINLINE)
	constexpr void MemDestruct(TAddPointer<TPropagateConstVolatile<ObjType, void>> MemPtr)
	requires CDestructible<ObjType>
	{
	//	AEON_ASSERT(IsBlockPointerValid(MemPtr, Aeon::AlignOf(ObjType)));
		using DecayedObjType = TDecayType<ObjType>;
		(*::std::launder(static_cast<TAddPointer<ObjType>>(MemPtr))).~DecayedObjType();
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// EMemAllocExOptions

	// FIXME make this a regular enum (or struct enum)

	AEON_DECLARE_FLAG_ENUM(EMemAllocExOptions, uint8)
	(
		AEON_FLAG_ENUM_RULE_POWER,
		Default,
		RelaxAlignConstraint,
		PlaceAllocInfoInBuffer // TODO implement this
	);

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ZzInternal namespace

	namespace ZzInternal
	{
#pragma region Example Allocator
//		struct SMemoryAllocator
//		{
//			voidptr Allocate(szint Size, algnint Alignment, EMemAllocOptionsEx::Flag Options);
//			voidptr Reallocate(voidptr MemPtr, szint Size);
//			void Deallocate(voidptr MemPtr);
//			auto QueryAllocationInfo(voidptr MemPtr);
//
//			int32 Compare(cvoidptr MemPtr1, cvoidptr MemPtr2, szint NumBytes);
//			void Copy(voidptr DstPtr, cvoidptr SrcPtr, szint NumBytes);
//			void Move(voidptr DstPtr, voidptr SrcPtr, szint NumBytes);
//			void Zero(voidptr MemPtr, szint NumBytes);
//		}
#pragma endregion Example Allocator

		constinit inline
#	if AEON_USING_MEMORY_MIMALLOC_ALLOCATOR
		struct SMimallocMemoryAllocator
		{
			// TODO
		}
#	else
		struct SBasicMemoryAllocator
		{
			struct DMemAllocInfo
			{
				// NOTE: We want to keep the size of this object a power of two (currently 8 bytes on most platforms),
				//  as that greatly simplifies a lot of the logic.

				// Actual buffer size will be 'Size + Offset', 'Size' refers to the usable part of the buffer
				szint Size : PlatformAddressSpaceSize;
				// Extra info about the allocation, useful for deleters to know how many destructors to invoke
				// Alignment stored as the exponent of a power of two (potentially useful for aligning at "huge-page" boundaries)
				szint Alignment : 6;
				// Offset to the actual start of the buffer, also stored as the exponent of a power of two;
				// Generally, only the bytes at 'MemPtr + Offset' are guaranteed to be aligned according to 'Alignment'
				szint Offset : 6;

				// Whether the constraint that 'Size' should be a multiple of the 'Alignment' was relaxed for this allocation
				szint RelaxAlignConstraint : 1;
			};

			static constexpr szint AllocInfoObjSize = Aeon::SizeOf(DMemAllocInfo);
			static constexpr algnint AllocInfoObjAlignment = Aeon::AlignOf(DMemAllocInfo);

			static constexpr algnint DefaultAllocAlignment = ::Aeon::Memory::PlatformDefaultAllocAlignment;

			// [DMemAllocInfo.Size.Invariant]
			// This allows us to compress the Offset field in the object itself.
			static_assert(AeonMath::IsPowerOfTwo(AllocInfoObjSize));

			// [DMemAllocInfo.Align.Invariant]
			// This is a necessary invariant for the code in Allocate/Reallocate to be correct.
			// This implies AllocInfoObjSize >= AllocInfoObjAlignment,
			// and ensures that an AllocInfo object constructed at 'MemPtr + Offset - AllocInfoObjSize' is aligned.
			static_assert(AeonMath::ModPowerOfTwo(AllocInfoObjSize, szint(AllocInfoObjAlignment)) == 0);

			voidptr Allocate(szint Size, algnint Alignment, EMemAllocExOptions Options)
			{
				AEON_FAILSAFE(::Aeon::Memory::IsBlockAlignmentValid(Alignment),
							  return nullptr,
							  "[ERROR] (MemAlloc) Alignment must be a power of two");

				bool RelaxAlignConstraint = ::Aeon::HasAnyFlags(Options, EMemAllocExOptions::RelaxAlignConstraint);

				// NOTE This is not strictly a requirement by any platform,
				//  but we usually enforce it to prevent possible errors when specifying alignment
				AEON_FAILSAFE(::Aeon::Memory::IsBlockSizeValid(Size, RelaxAlignConstraint ? 1_algn : Alignment),
							  return nullptr,
							  "[ERROR] (MemAlloc) Size must be a non-zero multiple of Alignment");

				// Ensure AllocInfo object is also aligned, at worst this makes allocations
				// of certain scalar types (e.g. bool, char, int32) stricter than necessary.
				// Note that any specific alignment boundary is also a boundary for all laxer alignments,
				// since object alignment is defined in terms of powers of two.
				szint ActualAlignment = static_cast<szint>(::AeonMath::Max(Alignment, AllocInfoObjAlignment));

				szint Offset = AllocInfoObjSize;
#			if not AEON_PLATFORM_WINDOWS
				// If the size of AllocInfo is not a multiple of the alignment,
				// we need to increase the offset to both support the object and align everything correctly.
				// Note that this logic also applies when sizeof(AllocInfo) < Alignment.
				// CHECKME For huge-page alignments, this is hugely wasteful, so consider an alternative implementation...
				if (szint AlignmentError = ::AeonMath::ModPowerOfTwo(Offset, ActualAlignment))
				{
					Offset += (ActualAlignment - AlignmentError);
				}
#			endif

				szint ActualSize = Size + Offset;

#			if AEON_PLATFORM_WINDOWS
				voidptr MemPtr = ::_aligned_offset_malloc(ActualSize, ActualAlignment, Offset);
#			else
				bool IsOverAligned = static_cast<algnint>(ActualAlignment) > DefaultAllocAlignment;
				voidptr MemPtr = IsOverAligned
								 ? ::std::aligned_alloc(ActualAlignment, ActualSize)
								 : ::std::malloc(ActualSize);
#			endif

				// TODO print platform stats
				AEON_FAILSAFE(MemPtr,
							  return nullptr,
							  "[WARNING] (MemAlloc) Allocation function failed - potentially out-of-memory");

				// NOTE: Cast to pointer-to-byte should be valid under P0593R6,
				//  will implicitly create an array of bytes,
				//  since the 'malloc' family of functions is blessed;
				//  Other allocators might need to use StartObjectLifetime to be well-defined.

				voidptr OffsetMemPtr = static_cast<byteptr>(MemPtr) + Offset;

				// This should never happen
				AEON_ASSERT(::Aeon::Memory::IsBlockPointerValid(OffsetMemPtr, static_cast<algnint>(ActualAlignment)),
							"[FATAL] (MemAlloc) Memory block does not respect alignment requirement");

				voidptr AllocInfoObjPtr = static_cast<byteptr>(OffsetMemPtr) - AllocInfoObjSize;

				// This should never happen, see [DMemAllocInfo.Align.Invariant]
				AEON_ASSERT(IsBlockPointerValid(AllocInfoObjPtr, AllocInfoObjAlignment));

				// Store the original info;
				// This is used by Realloc to know the requested alignment (this is necessary to check if reallocation size is valid),
				// from which the actual alignment may easily be computed; and the size of the old buffer to copy data to the new buffer.
				::new(AllocInfoObjPtr) DMemAllocInfo{ .Size = Size,
													  .Alignment = AeonMath::LogPowerOfTwo(szint(Alignment)),
													  .Offset = AeonMath::LogPowerOfTwo(Offset),
													  .RelaxAlignConstraint = RelaxAlignConstraint };

				return OffsetMemPtr;
			}

			voidptr Reallocate(voidptr OldOffsetMemPtr, szint NewSize)
			{
				AEON_FAILSAFE(OldOffsetMemPtr,
							  return nullptr,
							  "[ERROR] (MemRealloc) OldOffsetMemPtr must be a valid pointer");

				voidptr OldAllocInfoObjPtr = static_cast<byteptr>(OldOffsetMemPtr) - AllocInfoObjSize;

				// Cache additional info, we'll need to update the size
				auto AllocInfo = *::std::launder(static_cast<DMemAllocInfo*>(OldAllocInfoObjPtr));

				algnint Alignment = static_cast<algnint>(1_sz << AllocInfo.Alignment);
				szint Offset = 1_sz << AllocInfo.Offset;

				// CHECKME should this really be handled through FAILSAFE?
				AEON_FAILSAFE(NewSize != AllocInfo.Size,
							  return OldOffsetMemPtr,
							  "[WARNING] (MemRealloc) NewSize should be different from the buffer's current size");

				AEON_FAILSAFE(::Aeon::Memory::IsBlockSizeValid(NewSize, AllocInfo.RelaxAlignConstraint ? 1_algn : Alignment),
							  return nullptr,
							  "[ERROR] (MemRealloc) NewSize must be a non-zero multiple of Alignment");

				voidptr OldMemPtr = static_cast<byteptr>(OldOffsetMemPtr) - Offset;

				szint ActualAlignment = static_cast<szint>(AeonMath::Max(Alignment, AllocInfoObjAlignment));

				// Note that the Offset is a function of the AllocInfo object's size and alignment, and of the requested alignment,
				// so there is no need to recompute it, since none of those values will change.

				szint ActualSize = NewSize + Offset;

#			if AEON_PLATFORM_WINDOWS
				voidptr NewMemPtr = ::_aligned_offset_realloc(OldMemPtr, ActualSize, ActualAlignment, Offset);
#			else
				// NOTE: std::realloc does not respect alignment, see: https://stackoverflow.com/questions/64884745/
				//  If the allocation has extended alignment requirements, then this will instead allocate a new aligned buffer,
				//  copy the data from the old buffer, and deallocate it afterwards.

				bool IsOverAligned = static_cast<algnint>(ActualAlignment) > DefaultAllocAlignment;
				voidptr NewMemPtr = IsOverAligned
									? ::std::aligned_alloc(ActualAlignment, ActualSize)
									: ::std::realloc(OldMemPtr, ActualSize);
#			endif

				// Note that if this fails, the original block is still valid
				// TODO print platform stats
				AEON_FAILSAFE(NewMemPtr,
							  return nullptr,
							  "[WARNING] (MemRealloc) Reallocation function failed - potentially out-of-memory");

				voidptr NewOffsetMemPtr = static_cast<byteptr>(NewMemPtr) + Offset;

				// Sanity check: this should never happen (something is wrong with the platform otherwise)
				AEON_ASSERT(IsBlockPointerValid(NewOffsetMemPtr, static_cast<algnint>(ActualAlignment)),
							"[FATAL] (MemRealloc) Memory block does not respect alignment requirement");

				voidptr NewAllocInfoObjPtr = static_cast<byteptr>(NewOffsetMemPtr) - AllocInfoObjSize;

				// This should never happen, see [DMemAllocInfo.Align.Invariant]
				AEON_ASSERT(IsBlockPointerValid(NewAllocInfoObjPtr, AllocInfoObjAlignment));

#			if not AEON_PLATFORM_WINDOWS
				if (IsOverAligned)
				{
					szint CopySize = AeonMath::Min(NewSize, AllocInfo.Size);
					::std::memcpy(NewOffsetMemPtr, OldOffsetMemPtr, CopySize);
					::std::free(OldMemPtr);
				}
#			endif

				// Now overwrite the new size, the old size is no longer needed
				AllocInfo.Size = NewSize;

				::new(NewAllocInfoObjPtr) DMemAllocInfo(AllocInfo);

				return NewOffsetMemPtr;
			}

			void Deallocate(voidptr OffsetMemPtr)
			{
				AEON_FAILSAFE(OffsetMemPtr,
							  return,
							  "ERROR [MemDealloc] OffsetMemPtr must be a valid pointer");

				voidptr AllocInfoObjPtr = static_cast<byteptr>(OffsetMemPtr) - AllocInfoObjSize;

				auto AllocInfo = *::std::launder(static_cast<DMemAllocInfo*>(AllocInfoObjPtr));

				szint const Offset = 1_sz << AllocInfo.Offset;

				voidptr MemPtr = static_cast<byteptr>(OffsetMemPtr) - Offset;

#			if AEON_PLATFORM_WINDOWS
				::_aligned_free(MemPtr);
#			else
				::std::free(MemPtr);
#			endif
			}

#		if 1
			DMemAllocInfo QueryAllocationInfo(voidptr MemPtr)
			{
				AEON_FAILSAFE(MemPtr,
							  return DMemAllocInfo{},
							  "ERROR [QueryAllocInfo] MemPtr must be a valid pointer");

				voidptr AllocInfoObjPtr = static_cast<byteptr>(MemPtr) - AllocInfoObjSize;

				return *::std::launder(static_cast<DMemAllocInfo*>(AllocInfoObjPtr));
			}
#		endif

			// TODO maybe make unchecked/fast versions of these functions vvvv

			int32 Compare(cvoidptr MemPtr1, cvoidptr MemPtr2, szint NumBytes)
			{
				AEON_FAILSAFE(MemPtr1 && MemPtr2,
							  return false,
							  "ERROR [MemCompare] MemPtr1 and MemPtr2 must be valid pointers");

				// NOTE std::memcmp will also compare padding bytes between (sub)objects
				return NumBytes ? ::std::memcmp(MemPtr1, MemPtr2, NumBytes) : 0;
			}

			void Copy(voidptr DstPtr, cvoidptr SrcPtr, szint NumBytes)
			{
				AEON_FAILSAFE(SrcPtr && DstPtr,
							  return,
							  "ERROR [MemCopy] SrcPtr and DstPtr must be valid pointers");

				if (NumBytes == 0)
				{
					return;
				}

				AEON_FAILSAFE(!CheckBlockOverlap(SrcPtr, DstPtr, NumBytes), return,
							  "ERROR [MemCopy] Memory blocks at SrcPtr and DstPtr must not overlap");

				::std::memcpy(DstPtr, SrcPtr, NumBytes);
			}

			void Move(voidptr DstPtr, voidptr SrcPtr, szint NumBytes)
			{
				AEON_FAILSAFE(SrcPtr && DstPtr,
							  return,
							  "ERROR [MemMove] SrcPtr and DstPtr must be valid pointers");

				// Note that unlike MemCopy, MemMove allows for overlapping blocks;
				// if SrcPtr == DstPtr, the blocks perfectly overlap, and there is no need to move any memory
				if (NumBytes == 0 || SrcPtr == DstPtr)
				{
					return;
				}

				::std::memmove(DstPtr, SrcPtr, NumBytes);
			}

			void Zero(voidptr MemPtr, szint NumBytes)
			{
				AEON_FAILSAFE(MemPtr,
							  return,
							  "ERROR [MemZero] MemPtr must be a valid pointer");

				if (NumBytes == 0)
				{
					return;
				}

				::std::memset(MemPtr, 0, NumBytes);
			}
		}
#	endif
		DefaultGlobalAllocator;
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MemAlloc, MemRealloc, MemDealloc

#pragma warning(push)
// C4102: unreferenced label
// C4702: unreachable code
#pragma warning(disable: 4102 4702)

	// FOR INTERNAL USE
	template <CILOElseVoid Type>
	AEON_SPEC(NODISCARD) TAddPointer<TRemoveArrayDimension<Type>> MemAlloc_Ex(szint Size, algnint Alignment, EMemAllocExOptions Options)
	{
		voidptr NewMemPtr = ::Aeon::Memory::ZzInternal::DefaultGlobalAllocator.Allocate(Size, Alignment, Options);
		if (NewMemPtr)
		{
			if constexpr (VIsUnboundedArray<Type>)
			{
				constexpr szint ElementSize = Aeon::SizeOf(TRemoveArrayDimension<Type>);
				if constexpr (ElementSize > 1)
				{
					auto AllocInfo = ::Aeon::Memory::ZzInternal::DefaultGlobalAllocator.QueryAllocationInfo(NewMemPtr);

					// CHECKME Does optimizer use reciprocal mul for this?
					szint ElementCount = Size / ElementSize;
					szint Padding = Size % ElementSize;

					AEON_FAILSAFE(ElementCount > 0, goto ExitMemAlloc,
								  "Undersized allocation for unbounded array. This is an error.");
					AEON_FAILSAFE(AllocInfo.RelaxAlignConstraint || Padding == 0, void(),
								  "Oversized allocation for unbounded array. This might be an error.");

					return StartObjectArrayLifetime<Type>(NewMemPtr, ElementCount);
				}
				else
				{
					return StartObjectArrayLifetime<Type>(NewMemPtr, Size);
				}
			}
			else if constexpr (VIsBoundedArray<Type>)
			{
				auto AllocInfo = ::Aeon::Memory::ZzInternal::DefaultGlobalAllocator.QueryAllocationInfo(NewMemPtr);

				AEON_FAILSAFE(Size >= Aeon::SizeOf(Type), goto ExitMemAlloc,
							  "Undersized allocation for bounded array. This is an error.");
				AEON_FAILSAFE(AllocInfo.RelaxAlignConstraint || Size == Aeon::SizeOf(Type), void(),
							  "Oversized allocation for bounded array. This might be an error.");

				return StartObjectArrayLifetime<Type>(NewMemPtr);
			}
			else if constexpr (VIsImplicitLifetimeObject<Type>)
			{
				return StartObjectLifetime<Type>(NewMemPtr);
			}
		}
	ExitMemAlloc:
		return static_cast<TAddPointer<TRemoveArrayDimension<Type>>>(NewMemPtr);
	}

	/// Allocates a buffer of uninitialized memory with capacity of @p Size bytes and address aligned to a byte boundary of @p Alignment bytes.
	/// The type parameter ensures that certain operations on the returned buffer are immediately well-defined, namely:
	/// <br>A buffer which is intended to be used as storage for an implicit-lifetime object is immediately initialized with such an object;
	/// the object can be used immediately after the return of this function without explicit construction.
	/// <br>A buffer which is intended to be used as an array of objects of some type @c T should be instantiated as @c T[] or @c T[N],
	/// where @c N is the amount of objects in the array.
	/// @tparam Type The type of the object(s) with which the buffer will be populated.<br>May be of bounded/unbounded array type, or @c void type (default).
	/// @param Size The amount of bytes that should be allocated.
	/// @param Alignment The byte alignment of the address at which the buffer should be allocated.
	/// @return Pointer to the allocated buffer of memory.
	template <CILOElseVoid Type = void>
	AEON_SPEC(NODISCARD, FORCEINLINE) TAddPointer<TRemoveArrayDimension<Type>> MemAlloc(szint Size/* = Aeon::SizeOf(Type)*/,
																						algnint Alignment/* = Aeon::AlignOf(Type)*/)
	{
		return MemAlloc_Ex<Type>(Size, Alignment, EMemAllocExOptions::Default);
	}

	///	Reallocates a buffer of memory that was previously allocated using @c MemAlloc to a capacity of @p Size bytes.
	/// <br> If the new buffer is larger than the old buffer, the full contents of the old buffer will be copied to the new buffer,
	/// and any remaining bytes on the new buffer will be left uninitialized.
	/// If the new buffer is smaller than the old buffer, a total of @p Size bytes of the contents of the old buffer will be copied to the new buffer.
	///	@tparam Type - The type of the object(s) with which the buffer is/will be populated.
	///	May be of bounded/unbounded array type, or @c void type (default).
	/// @param MemPtr - Pointer to the old allocated buffer.
	///	@param Size - The amount of bytes that should be allocated.
	///	@return Pointer to the new reallocated buffer.
	template <CILOElseVoid Type = void>
	AEON_SPEC(NODISCARD) TAddPointer<TRemoveArrayDimension<Type>> MemRealloc(voidptr MemPtr, szint Size)
	requires CTriviallyCopyable<Type> || CVoid<Type>
	{
		voidptr NewMemPtr = ::Aeon::Memory::ZzInternal::DefaultGlobalAllocator.Reallocate(MemPtr, Size);
		if (NewMemPtr)
		{
			if constexpr (VIsUnboundedArray<Type>)
			{
				auto AllocInfo = ::Aeon::Memory::ZzInternal::DefaultGlobalAllocator.QueryAllocationInfo(NewMemPtr);

				constexpr szint ElementSize = Aeon::SizeOf(TRemoveArrayDimension<Type>);
				if constexpr (ElementSize > 1)
				{
					szint ElementCount = Size / ElementSize;
					szint Padding = Size % ElementSize;

					AEON_FAILSAFE(ElementCount > 0, goto ExitMemRealloc,
								  "Undersized allocation for unbounded array. This is an error.");
					AEON_FAILSAFE(AllocInfo.RelaxAlignConstraint || Padding == 0, void(),
								  "Oversized allocation for unbounded array. This might be an error.");

					return StartObjectArrayLifetime<Type>(NewMemPtr, ElementCount);
				}
				else
				{
					return StartObjectArrayLifetime<Type>(NewMemPtr, Size);
				}
			}
			else if constexpr (VIsBoundedArray<Type>)
			{
				auto AllocInfo = ::Aeon::Memory::ZzInternal::DefaultGlobalAllocator.QueryAllocationInfo(NewMemPtr);

				AEON_FAILSAFE(Size >= Aeon::SizeOf(Type), goto ExitMemRealloc,
							  "Undersized allocation for bounded array. This is an error.");
				AEON_FAILSAFE(AllocInfo.RelaxAlignConstraint || Size == Aeon::SizeOf(Type), void(),
							  "Oversized allocation for bounded array. This might be an error.");

				return StartObjectArrayLifetime<Type>(NewMemPtr);
			}
			else if constexpr (VIsImplicitLifetimeObject<Type>)
			{
				return StartObjectLifetime<Type>(NewMemPtr);
			}
		}
	ExitMemRealloc:
		return static_cast<TAddPointer<TRemoveArrayDimension<Type>>>(NewMemPtr);
	}

#pragma warning(pop)

	/// Deallocates a buffer of memory that was previously allocated with @c MemAlloc/MemRealloc.
	/// No destructors are called for any objects that may be using the buffer as storage.
	/// @param MemPtr - Pointer to the buffer of memory to be deallocated.
	AEON_SPEC(FORCEINLINE) inline void MemDealloc(voidptr MemPtr)
	{
		::Aeon::Memory::ZzInternal::DefaultGlobalAllocator.Deallocate(MemPtr);
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MemCompare, MemCopy, MemMove, MemZero

	AEON_SPEC(FORCEINLINE) inline int32 MemCompare(cvoidptr MemPtr1, cvoidptr MemPtr2, szint NumBytes)
	{
		return ::Aeon::Memory::ZzInternal::DefaultGlobalAllocator.Compare(MemPtr1, MemPtr2, NumBytes);
	}

	AEON_SPEC(FORCEINLINE) inline void MemCopy(voidptr DstPtr, cvoidptr SrcPtr, szint NumBytes)
	{
		::Aeon::Memory::ZzInternal::DefaultGlobalAllocator.Copy(DstPtr, SrcPtr, NumBytes);
	}

	AEON_SPEC(FORCEINLINE) inline void MemMove(voidptr DstPtr, voidptr SrcPtr, szint NumBytes)
	{
		::Aeon::Memory::ZzInternal::DefaultGlobalAllocator.Move(DstPtr, SrcPtr, NumBytes);
	}

	AEON_SPEC(FORCEINLINE) inline void MemZero(voidptr MemPtr, szint NumBytes)
	{
		::Aeon::Memory::ZzInternal::DefaultGlobalAllocator.Zero(MemPtr, NumBytes);
	}

	namespace ZzInternal
	{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ConstructArray

		template <CBoundedArray ObjArrType, class InitObjType>
		TAddPointer<TRemoveArrayDimension<ObjArrType>> ConstructArray(voidptr AllocPtr, InitList<InitObjType> InitList)
		requires (CSameClass<InitObjType, TRemoveArrayDimension<ObjArrType>>
				 || CSameClass<InitObjType, GRvalRefWrapper<TRemoveArrayDimension<ObjArrType>>>)
				 && (CDefaultConstructible<InitObjType>
				 || CMoveableElseCopyable<InitObjType>)
		{
			using ObjType = TRemoveArrayDimension<ObjArrType>;
			constexpr szint ObjCountValue = VArrayDimensionSize<ObjArrType>;

			auto InitObjData = InitListOps::Data(InitList);
			auto InitObjCount = InitListOps::Count(InitList);

			// If type is default constructible, the initializer list simply must not have more than ObjCountValue elements,
			// otherwise, the initializer list is required to have exactly ObjCountValue elements.
			AEON_FAILSAFE(InitObjCount <= ObjCountValue,
						  AeonDebugger::CrashProgram(true, 0),
						  "Excess elements in array initializer");

			if constexpr (!VIsMoveableElseCopyable<ObjType>)
			{
				AEON_FAILSAFE(InitObjCount == 0,
							  AeonDebugger::CrashProgram(true, 0),
							  "No appropriate constructor available "
							  "in explicit initialization of array element referencing a deleted function");
			}
			if constexpr (!VIsDefaultConstructible<ObjType>)
			{
				AEON_FAILSAFE(InitObjCount == ObjCountValue,
							  AeonDebugger::CrashProgram(true, 0),
							  "No appropriate default constructor available "
							  "in implicit initialization of array element with omitted initializer");
			}

			auto ObjArrPtr = StartObjectArrayLifetime<ObjArrType>(AllocPtr);

			szint ObjIdx = 0;
			if constexpr (VIsMoveableElseCopyable<ObjType>)
			{
				while (ObjIdx < InitObjCount)
				{
					MemConstruct<ObjType>(ObjArrPtr + ObjIdx, MoveElseCopy(InitObjData[ObjIdx]));
					++ObjIdx;
				}
			}
			if constexpr (VIsDefaultConstructible<ObjType>)
			{
				while (ObjIdx < ObjCountValue)
				{
					MemConstruct<ObjType>(ObjArrPtr + ObjIdx);
					++ObjIdx;
				}
			}

			return ObjArrPtr;
		}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// AllocArray_WithNewExprInfo, DestructArray_WithNewExprInfo, DeallocArray_WithNewExprInfo

		struct SNewExprInfo
		{
			// NOTE Beware, here be dragons...
			//  This code was derived by running experiments with operator new[]
			//  in order to determine in which situations extra storage is allocated.
			//  This is one of the few places where ABI stability sorta plays into our favor...
			//  as it is unlikely that the implementation will change how (or when) new[] stores this extra data.
			//   - Windows: new[] allocates extra storage when the object type ObjType is non-trivially destructible;
			//  	and this storage is of size = max(alignof(std::max_align_t), alignof(ObjType));
			//  	it uses this storage to store an integer indicating the size of the array,
			//  	to assist delete[] in knowing how many objects it should destruct.
			//		TODO test/port other platforms

#		if AEON_PLATFORM_WINDOWS and AEON_ARCHITECTURE_X86_64 // TODO remove arch check once FIX ME below is addressed
			template <class ObjType>
			static constexpr bool NeedsInfoBlock = !VIsTriviallyDestructible<ObjType>;

			// FIXME Ensure ABI uses std::max_align_t and not something like szint (maybe compare with 32-bit platform?)
			template <class ObjType>
			static constexpr szint InfoBlockSize = szint(AeonMath::Max(Aeon::AlignOf(ObjType), Aeon::AlignOf(std::max_align_t)));
#		else
#			error Not implemented yet!
#		endif
		};

		template <CBoundedArray ObjArrType>
		byteptr AllocArray_WithNewExprInfo(szint Size = Aeon::SizeOf(ObjArrType),
										   algnint Alignment = Aeon::AlignOf(ObjArrType),
										   EMemAllocExOptions Options = EMemAllocExOptions::Default)
		{
			using ObjType = TRemoveArrayDimension<ObjArrType>;
			constexpr szint ObjCount = VArrayDimensionSize<ObjArrType>;

			constexpr bool NeedsInfoBlock = SNewExprInfo::NeedsInfoBlock<ObjType>;
			constexpr szint InfoBlockSize = NeedsInfoBlock ? SNewExprInfo::InfoBlockSize<ObjType> : 0;

			auto AllocPtr = MemAlloc_Ex<byte[]>(Size + InfoBlockSize, Alignment, Options);
			if constexpr (NeedsInfoBlock)
			{
				// NOTE Currently constructing a szint object,
				//  which seems an appropriate, likely type for holding the object count
				MemConstruct<szint>(AllocPtr, ObjCount);
			}

			AllocPtr += InfoBlockSize;

			AEON_ASSERT(IsBlockPointerValid(AllocPtr, Aeon::AlignOf(ObjType)));

			return AllocPtr;
		}

		template <CUnboundedArray ObjArrType>
		void DestructArray_WithNewExprInfo(TAddPointer<TRemoveArrayDimension<ObjArrType>> ObjArrPtr)
		{
			using ObjType = TRemoveArrayDimension<ObjArrType>;

			constexpr bool HasInfoBlock = SNewExprInfo::NeedsInfoBlock<ObjType>;
			if constexpr (HasInfoBlock)
			{
				byteptr AllocPtr = reinterpret_cast<byteptr>(ObjArrPtr);

				constexpr szint InfoBlockSize = SNewExprInfo::InfoBlockSize<ObjType>;

				AllocPtr -= InfoBlockSize;

				szint ObjCount = *MemAccess<szint>(AllocPtr);
				szint ObjIdx = 0;

				while (ObjIdx < ObjCount)
				{
					MemDestruct<ObjType>(ObjArrPtr + ObjIdx);
					++ObjIdx;
				}
			}
		}

		template <CUnboundedArray ObjArrType>
		void DeallocArray_WithNewExprInfo(voidptr ArrPtr)
		{
			using ObjType = TRemoveArrayDimension<ObjArrType>;
			
			byteptr AllocPtr = reinterpret_cast<byteptr>(ArrPtr);

			// NOTE If any cleanup logic is ever necessary for the info block, add it to this function

			constexpr bool HasInfoBlock = SNewExprInfo::NeedsInfoBlock<ObjType>;
			constexpr szint InfoBlockSize = HasInfoBlock ? SNewExprInfo::InfoBlockSize<ObjType> : 0;

			AllocPtr -= InfoBlockSize;

			MemDealloc(AllocPtr);
		}
	}
}

namespace AeonMemory = Aeon::Memory;

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GStorage

	template <CScalarObject ObjType>
	class GStorage
	{
	public:
		using ObjectType = ObjType;

		GStorage() = default;

		GStorage(GStorage const&) = delete;
		GStorage(GStorage&&) = delete;

		GStorage& operator=(GStorage const&) = delete;
		GStorage& operator=(GStorage&&) = delete;

		template <class ... ArgTypes>
		ObjectType* Construct(ArgTypes&& ... Args)
		{
			return AeonMemory::MemConstruct<ObjectType>(Storage, Aeon::Forward<ArgTypes>(Args) ...);
		}
		void Destruct()
		{
			AeonMemory::MemDestruct<ObjectType>(Storage);
		}

		ObjectType const& operator*() const
		{
			return *AeonMemory::MemAccess<ObjectType const>(Storage);
		}
		ObjectType& operator*()
		{
			return AEON_INVOKE_CONST_OVERLOAD(operator*);
		}

		ObjectType const* operator&() const
		{
			return AeonMemory::MemAccess<ObjectType const>(Storage);
		}
		ObjectType* operator&()
		{
			return AEON_INVOKE_CONST_OVERLOAD(operator&);
		}

		ObjectType const* operator->() const
		{
			return AeonMemory::MemAccess<ObjectType const>(Storage);
		}
		ObjectType* operator->()
		{
			return AEON_INVOKE_CONST_OVERLOAD(operator->);
		}

	private:
		alignas(ObjectType) byte Storage[Aeon::SizeOf(ObjectType)];

	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GOpaque

	template <CClass ObjType>
	requires CNotConst<ObjType> && CNotVolatile<ObjType>
	class GOpaque
	{
	private:
		GOpaque()
			: ObjectPtr(nullptr)
		{
		}

	public:
		using ObjectType = ObjType;

		GOpaque(ObjType& NewObjectRef, szint NewObjectSize = sizeof(ObjType))
			: ObjectPtr(addressof(NewObjectRef))
			, ObjectSz(NewObjectSize)
		{
		}

		GOpaque(ObjType&&) = delete;

		GOpaque(GOpaque const& Other) = default;
		GOpaque(GOpaque&& Other) = default;

		GOpaque& operator=(GOpaque const& Other) = default;
		GOpaque& operator=(GOpaque&& Other) = default;

		ObjType& Access() const
		{
			return *ObjectPtr;
		}
		bool IsValid() const
		{
			return ObjectPtr;
		}

		AEON_FORCEINLINE operator ObjType&() const
		{
			return Access();
		}
		AEON_FORCEINLINE explicit operator bool() const
		{
			return IsValid();
		}

		bool operator==(GOpaque const& Other) const
		{
			return AeonMemory::MemCompare(ObjectPtr, Other.ObjectPtr, ObjectSz) == 0;
		}
		strong_ordering operator<=>(GOpaque const& Other) const
		{
			return AeonMemory::MemCompare(ObjectPtr, Other.ObjectPtr, ObjectSz) <=> 0;
		}

		static GOpaque Null()
		{
			return {};
		}

		cbyteptr ObjectPointer() const
		{
			return reinterpret_cast<cbyteptr>(ObjectPtr);
		}
		szint ObjectSize() const
		{
			return ObjectSz;
		}

	private:
		ObjType* ObjectPtr;
		szint ObjectSz;
	};

	template <class ObjType>
	struct ZzInternal_GDefaultObjectHasherBase<GOpaque<ObjType>, uint32, true>
	{
		AEON_FORCEINLINE static uint32 Hash(GOpaque<ObjType> const& Object)
		{
			return FNV1aHash<uint32>(Object.ObjectPointer(), Object.ObjectSize());
		}
	};
	template <class ObjType>
	struct ZzInternal_GDefaultObjectHasherBase<GOpaque<ObjType>, uint64, true>
	{
		AEON_FORCEINLINE static uint64 Hash(GOpaque<ObjType> const& Object)
		{
			return FNV1aHash<uint64>(Object.ObjectPointer(), Object.ObjectSize());
		}
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// New, Delete

	// TODO improve docs

	/// Allocates memory for and creates an object on the free-store.
	/// @tparam [Mandatory] ObjType - The type of the object to be created;
	/// @tparam [Variadic] ParamTypes - The types of the arguments to be passed to the object's constructor.
	/// @param [Variadic] ObjParams - The arguments to be passed to the object's constructor.
	/// @return Pointer to the newly-created object, or nullptr if the operation fails.
	template <CScalarObject ObjType, class ... ArgTypes>
	TAddPointer<ObjType> New(ArgTypes&& ... InitArgs)
	{
		auto AllocPtr = AeonMemory::MemAlloc(Aeon::SizeOf(ObjType), Aeon::AlignOf(ObjType));
		return AeonMemory::MemConstruct<ObjType>(AllocPtr, Aeon::Forward<ArgTypes>(InitArgs) ...);
	}

	template <CBoundedArray ObjArrType>
	TAddPointer<TRemoveArrayDimension<ObjArrType>> New(InitList<TRemoveArrayDimension<ObjArrType>> InitList = {})
	{
		auto AllocPtr = AeonMemory::ZzInternal::AllocArray_WithNewExprInfo<ObjArrType>();
		return AeonMemory::ZzInternal::ConstructArray<ObjArrType>(AllocPtr, InitList);
	}

	// FIXME Add support for this
	template <CUnboundedArray ObjArrType>
	TAddPointer<TRemoveArrayDimension<ObjArrType>> New(/*szint ObjCount,*/ InitList<TRemoveArrayDimension<ObjArrType>> InitList = {}) = delete;

	/// Destroys an object on the free-store and deallocates the memory that the object was using.
	/// @tparam ObjType - The type of the object to be destroyed.
	/// @param ObjPtr - Pointer to the object to be destroyed.
	template <CScalarObject ObjType>
	void Delete(TAddPointer<ObjType> ObjPtr)
	{
		if (ObjPtr)
		{
			if constexpr (!VIsTriviallyDestructible<ObjType>)
			{
				AeonMemory::MemDestruct<ObjType>(ObjPtr);
			}
			AeonMemory::MemDealloc(ObjPtr);
		}
	}

	template <CUnboundedArray ObjArrType>
	void Delete(TAddPointer<TRemoveArrayDimension<ObjArrType>> ObjPtr)
	{
		using ObjType = TRemoveArrayDimension<ObjArrType>;

		if (ObjPtr)
		{
			if constexpr (!VIsTriviallyDestructible<ObjType>)
			{
				AeonMemory::ZzInternal::DestructArray_WithNewExprInfo<ObjArrType>(ObjPtr);
			}
			AeonMemory::ZzInternal::DeallocArray_WithNewExprInfo<ObjArrType>(ObjPtr);
		}
	}

	// Bounded array in Delete expression is not allowed
	template <CBoundedArray ObjArrType>
	void Delete(TAddPointer<TRemoveArrayDimension<ObjArrType>> ObjPtr) = delete;
}

#define ZZINTERNAL_AEON_OVERRIDE_STANDARD_ALLOCATION_FUNCTIONS \
    voidptr operator new(szint Size)                                                                                                                 \
    {                                                                                                                                                \
		return ::AeonMemory::ZzInternal::DefaultGlobalAllocator.Allocate(Size, algnint(1), ::AeonMemory::EMemAllocExOptions::Default);               \
	}                                                                                                                                                \
    voidptr operator new[](szint Size)                                                                                                               \
    {                                                                                                                                                \
		return ::AeonMemory::ZzInternal::DefaultGlobalAllocator.Allocate(Size, algnint(1), ::AeonMemory::EMemAllocExOptions::Default);               \
	}                                                                                                                                                \
    voidptr operator new(szint Size, noexcept_tag const&) noexcept                                                                                   \
    {                                                                                                                                                \
		return ::AeonMemory::ZzInternal::DefaultGlobalAllocator.Allocate(Size, algnint(1), ::AeonMemory::EMemAllocExOptions::Default);               \
	}                                                                                                                                                \
    voidptr operator new[](szint Size, noexcept_tag const&) noexcept                                                                                 \
    {                                                                                                                                                \
		return ::AeonMemory::ZzInternal::DefaultGlobalAllocator.Allocate(Size, algnint(1), ::AeonMemory::EMemAllocExOptions::Default);               \
	}                                                                                                                                                \
    voidptr operator new(szint Size, algnint Alignment)                                                                                              \
    {                                                                                                                                                \
		return ::AeonMemory::ZzInternal::DefaultGlobalAllocator.Allocate(Size, Alignment, ::AeonMemory::EMemAllocExOptions::Default);                \
	}                                                                                                                                                \
    voidptr operator new[](szint Size, algnint Alignment)                                                                                            \
    {                                                                                                                                                \
		return ::AeonMemory::ZzInternal::DefaultGlobalAllocator.Allocate(Size, Alignment, ::AeonMemory::EMemAllocExOptions::Default);                \
	}                                                                                                                                                \
    voidptr operator new(szint Size, algnint Alignment, noexcept_tag const&) noexcept                                                                \
    {                                                                                                                                                \
		return ::AeonMemory::ZzInternal::DefaultGlobalAllocator.Allocate(Size, Alignment, ::AeonMemory::EMemAllocExOptions::Default);                \
	}                                                                                                                                                \
    voidptr operator new[](szint Size, algnint Alignment, noexcept_tag const&) noexcept                                                              \
    {                                                                                                                                                \
		return ::AeonMemory::ZzInternal::DefaultGlobalAllocator.Allocate(Size, Alignment, ::AeonMemory::EMemAllocExOptions::Default);                \
	}                                                                                                                                                \
    void operator delete(voidptr MemPtr) noexcept                                                                                                    \
	{                                                                                                                                                \
		::AeonMemory::ZzInternal::DefaultGlobalAllocator.Deallocate(MemPtr);                                                                         \
	}                                                                                                                                                \
    void operator delete[](voidptr MemPtr) noexcept                                                                                                  \
	{                                                                                                                                                \
		::AeonMemory::ZzInternal::DefaultGlobalAllocator.Deallocate(MemPtr);                                                                         \
	}                                                                                                                                                \
    void operator delete(voidptr MemPtr, noexcept_tag const&) noexcept                                                                               \
	{                                                                                                                                                \
		::AeonMemory::ZzInternal::DefaultGlobalAllocator.Deallocate(MemPtr);                                                                         \
	}                                                                                                                                                \
    void operator delete[](voidptr MemPtr, noexcept_tag const&) noexcept                                                                             \
	{                                                                                                                                                \
		::AeonMemory::ZzInternal::DefaultGlobalAllocator.Deallocate(MemPtr);                                                                         \
	}                                                                                                                                                \
    void operator delete(voidptr MemPtr, szint) noexcept                                                                                             \
	{                                                                                                                                                \
		::AeonMemory::ZzInternal::DefaultGlobalAllocator.Deallocate(MemPtr);                                                                         \
	}                                                                                                                                                \
    void operator delete[](voidptr MemPtr, szint) noexcept                                                                                           \
	{                                                                                                                                                \
		::AeonMemory::ZzInternal::DefaultGlobalAllocator.Deallocate(MemPtr);                                                                         \
	}                                                                                                                                                \
    void operator delete(voidptr MemPtr, szint, noexcept_tag const&) noexcept                                                                        \
	{                                                                                                                                                \
		::AeonMemory::ZzInternal::DefaultGlobalAllocator.Deallocate(MemPtr);                                                                         \
	}                                                                                                                                                \
    void operator delete[](voidptr MemPtr, szint, noexcept_tag const&) noexcept                                                                      \
	{                                                                                                                                                \
		::AeonMemory::ZzInternal::DefaultGlobalAllocator.Deallocate(MemPtr);                                                                         \
	}                                                                                                                                                \
    void operator delete(voidptr MemPtr, algnint) noexcept                                                                                           \
	{                                                                                                                                                \
		::AeonMemory::ZzInternal::DefaultGlobalAllocator.Deallocate(MemPtr);                                                                         \
	}                                                                                                                                                \
    void operator delete[](voidptr MemPtr, algnint) noexcept                                                                                         \
	{                                                                                                                                                \
		::AeonMemory::ZzInternal::DefaultGlobalAllocator.Deallocate(MemPtr);                                                                         \
	}                                                                                                                                                \
    void operator delete(voidptr MemPtr, algnint, noexcept_tag const&) noexcept                                                                      \
	{                                                                                                                                                \
		::AeonMemory::ZzInternal::DefaultGlobalAllocator.Deallocate(MemPtr);                                                                         \
	}                                                                                                                                                \
    void operator delete[](voidptr MemPtr, algnint, noexcept_tag const&) noexcept                                                                    \
	{                                                                                                                                                \
		::AeonMemory::ZzInternal::DefaultGlobalAllocator.Deallocate(MemPtr);                                                                         \
	}                                                                                                                                                \
    void operator delete(voidptr MemPtr, szint, algnint) noexcept                                                                                    \
	{                                                                                                                                                \
		::AeonMemory::ZzInternal::DefaultGlobalAllocator.Deallocate(MemPtr);                                                                         \
	}                                                                                                                                                \
    void operator delete[](voidptr MemPtr, szint, algnint) noexcept                                                                                  \
	{                                                                                                                                                \
		::AeonMemory::ZzInternal::DefaultGlobalAllocator.Deallocate(MemPtr);                                                                         \
	}                                                                                                                                                \
    void operator delete(voidptr MemPtr, szint, algnint, noexcept_tag const&) noexcept                                                               \
	{                                                                                                                                                \
		::AeonMemory::ZzInternal::DefaultGlobalAllocator.Deallocate(MemPtr);                                                                         \
	}                                                                                                                                                \
    void operator delete[](voidptr MemPtr, szint, algnint, noexcept_tag const&) noexcept                                                             \
	{                                                                                                                                                \
		::AeonMemory::ZzInternal::DefaultGlobalAllocator.Deallocate(MemPtr);                                                                         \
	}

#endif