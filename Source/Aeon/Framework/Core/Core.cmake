target_sources(${PROJECT_NAME}
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/CoreDirectives.hpp

		${CMAKE_CURRENT_SOURCE_DIR}/CoreStandardLibrary.hpp
        ${CMAKE_CURRENT_SOURCE_DIR}/CorePlatformLibrary.hpp

		${CMAKE_CURRENT_SOURCE_DIR}/CoreBasicTypes.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/CoreMetaTypes.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/CoreUtilities.hpp

		${CMAKE_CURRENT_SOURCE_DIR}/CorePreprocessor.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/CoreDebugger.hpp

		${CMAKE_CURRENT_SOURCE_DIR}/CoreMath.hpp

		${CMAKE_CURRENT_SOURCE_DIR}/CoreMemory.hpp

#		${CMAKE_CURRENT_SOURCE_DIR}/CoreAtomicOperations.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/CoreAtomic.hpp
		${CMAKE_CURRENT_SOURCE_DIR}/CoreSIMD.hpp
	)