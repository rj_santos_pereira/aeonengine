#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CORE_COREPLATFORMLIBRARY
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CORE_COREPLATFORMLIBRARY

#include "CoreDirectives.hpp"

#if AEON_PLATFORM_WINDOWS
#include <Windows.h>
#include <debugapi.h>
#include <dbghelp.h>
#include <fcntl.h>
#include <io.h>
#include <psapi.h>
#include <shellapi.h>
#include <timeapi.h>

// Undef some pesky macros that clash with some of our functions and macros

#undef NODISCARD
#define NODISCARD NODISCARD
#undef NORETURN
#define NORETURN NORETURN

#undef FORCEINLINE
#define FORCEINLINE FORCEINLINE
#undef FORCENOINLINE
#define FORCENOINLINE FORCENOINLINE

#undef Yield
#undef YieldProcessor
#undef FastFence
#undef LoadFence
#undef StoreFence
#undef MemoryFence
#undef MemoryBarrier

#else
#error Unsupported platform!
#endif

#endif