#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CORE_CORESTAMDARDLIBRARY
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CORE_CORESTAMDARDLIBRARY

#include "CoreDirectives.hpp"

// TODO maybe comment unused includes after determining core library

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Concepts

// Fundamental library concepts
#include <concepts>



// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Coroutines

// Coroutine support library
#include <coroutine>



// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Utilities

// std::any class
#include <any>
// std::bitset class template
#include <bitset>
// C++ time utilites
#include <chrono>
// Three-way comparison operator support
#include <compare>
// Macro (and function) that saves (and jumps) to an execution context
#include <csetjmp>
// Functions and macro constants for signal management
#include <csignal>
// Handling of variable length argument lists
#include <cstdarg>
// Standard macros and typedefs
#include <cstddef>
// General purpose utilities: program control, dynamic memory allocation, random numbers, sort and search
#include <cstdlib>
// C-style time/date utilites
#include <ctime>
// Function objects, Function invocations, Bind operations and Reference wrappers
#include <functional>
// std::initializer_list class template
#include <initializer_list>
// std::optional class template
#include <optional>
// Supplies means to obtain source code location
#include <source_location>
// std::tuple class template
#include <tuple>
// Compile-time type information
#include <type_traits>
// std::type_index
#include <typeindex>
// Runtime type information utilities
#include <typeinfo>
// Various utility components
#include <utility>
// std::variant class template
#include <variant>
// Supplies implementation-dependent library information
#include <version>

namespace stdtm
{
	// NOTE The type names in std::chrono are extraordinarily opaque and verbose,
	//  so this library makes some better aliases to the types it uses
	// NOTE See https://github.com/HowardHinnant/date/wiki/Examples-and-Recipes#components_to_time_point for more info

	using namespace std::chrono;

	using tz_info = std::chrono::sys_info;
	using tz = std::chrono::time_zone;

	using sys_clock = std::chrono::system_clock;
//	using utc_clock = std::chrono::utc_clock;

	using sys_timepoint = std::chrono::system_clock::time_point; // System time_point with sys_clock::period granularity (usually 100 ns)
	using sys_timespan = std::chrono::system_clock::duration; // System time_span with sys_clock::period granularity (usually 100 ns)

	// Local (timezone-based) time_point with sys_clock::period granularity
	using local_timepoint = std::chrono::local_time<std::chrono::system_clock::duration>;
	using utc_timepoint = std::chrono::utc_time<std::chrono::system_clock::duration>;

	using sec_timepoint = std::chrono::sys_seconds; // sys_time_point with second granularity
	using sec_timespan = std::chrono::seconds; // sys_time_span with second granularity

	// Minimum granularity for timezone offset computation
	using min_timespan = std::chrono::minutes; // sys_time_span with minute granularity

	using day_timepoint = std::chrono::sys_days; // sys_time_point with day granularity
	using day_timespan = std::chrono::days; // sys_time_span with day granularity

	using month_timespan = std::chrono::months; // sys_time_point with month granularity
	using year_timespan = std::chrono::years; // sys_time_span with year granularity

	using date_desc = std::chrono::year_month_day;
	using weekdate_desc = std::chrono::year_month_weekday;

	using time_desc = std::chrono::hh_mm_ss<std::chrono::system_clock::duration>;
}



// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Dynamic memory management

// High-level memory management utilities
#include <memory>
// Polymorphic allocators and memory resources
#include <memory_resource>

// Low-level memory management utilities
#include <new>
// Nested allocator class
#include <scoped_allocator>



// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Numeric limits

// Limits of floating-point types
#include <cfloat>
// Formatting macros, intmax_t and uintmax_t math and conversions
#include <cinttypes>
// Limits of integral types
#include <climits>
// Fixed-width integer types and limits of other types
#include <cstdint>
// Uniform way to query properties of arithmetic types
#include <limits>



// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Error handling

// Conditionally compiled macro that compares its argument to zero
#include <cassert>
// Macro containing the last error number
#include <cerrno>
// Exception handling utilities
#include <exception>
// Standard exception objects
#include <stdexcept>
// Defines std::error_code, a platform-dependent error code
#include <system_error>



// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Strings

// Functions to determine the category of narrow characters
#include <cctype>
// std::to_chars and std::from_chars
#include <charconv>

// Various narrow character string handling functions
#include <cstring>
// C-style Unicode character conversion functions
#include <cuchar>
// Various wide and multibyte string handling functions
#include <cwchar>
// Functions to determine the category of wide characters
#include <cwctype>
// Formatting library including std::format
#if AEON_USING_FMT_LIBRARY
#	define ZZINTERNAL_AEON_FMT_NS fmt

#	pragma warning(push)
#	pragma warning(disable: 4127) // MSVC warning C4127: conditional expression is constant

#	include "fmt/format.h"
#	include "fmt/chrono.h"
#	include "fmt/color.h"

#	pragma warning(pop)
#else
#	define ZZINTERNAL_AEON_FMT_NS std

#	include <format>
#endif
// std::basic_string class template
#include <string>
// std::basic_string_view class template
#include <string_view>



// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Containers

// std::array container
#include <array>
// std::deque container
#include <deque>
// std::forward_list container
#include <forward_list>
// std::list container
#include <list>
// std::map and std::multimap associative containers
#include <map>
// std::queue and std::priority_queue container adaptors
#include <queue>
// std::set and std::multiset associative containers
#include <set>
// std::span view
#include <span>
// std::stack container adaptor
#include <stack>
// std::unordered_map and std::unordered_multimap unordered associative containers
#include <unordered_map>
// std::unordered_set and std::unordered_multiset unordered associative containers
#include <unordered_set>
// std::vector container
#include <vector>



// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Iterators

// Range iterators
#include <iterator>



// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Ranges

// Range access, primitives, requirements, utilities and adaptors
#include <ranges>



// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Algorithms

// Algorithms that operate on ranges
#include <algorithm>
// Predefined execution policies for parallel versions of the algorithms
#include <execution>



// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Numerics

// Bit manipulation functions
#include <bit>
// Floating-point environment access functions
#include <cfenv>
// Common mathematics functions
#include <cmath>
// Complex number type
#include <complex>
// AeonMath constants
#include <numbers>
// Numeric operations on values in ranges
#include <numeric>
// Random number generators and distributions
#include <random>
// Compile-time rational arithmetic
#include <ratio>
// Class for representing and manipulating arrays of values
#include <valarray>



// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Localization

// C localization utilities
#include <clocale>
// Unicode conversion facilities
#include <codecvt>
// Localization utilities
#include <locale>



// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Input/output

// C-style input-output functions
#include <cstdio>
// std::basic_fstream, std::basic_ifstream, std::basic_ofstream class templates and several typedefs
#include <fstream>
// Helper functions to control the format of input and output
#include <iomanip>
// std::ios_base class, std::basic_ios class template and several typedefs
#include <ios>
//  Forward declarations of all classes in the input/output library
//#include <iosfwd>
// Several standard stream objects
#include <iostream>
// std::basic_istream class template and several typedefs
#include <istream>
// std::basic_ostream, std::basic_iostream class templates and several typedefs
#include <ostream>
// std::basic_stringstream, std::basic_istringstream, std::basic_ostringstream class templates and several typedefs
#include <sstream>
// std::basic_streambuf class template
#include <streambuf>
// std::basic_osyncstream, std::basic_syncbuf, and typedefs
#include <syncstream>



// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Filesystem

// std::path class and supporting functions
#include <filesystem>

namespace stdfs = std::filesystem;



// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Regular expressions

// Classes, algorithms and iterators to support regular expression processing
#include <regex>



// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Atomic operations

// Atomic operations library
#include <atomic>



// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Thread support

// Barriers
#include <barrier>
// Thread waiting conditions
#include <condition_variable>
// Primitives for asynchronous computations
#include <future>
// Latches
#include <latch>
// Mutual exclusion primitives
#include <mutex>
// Semaphores
#include <semaphore>
// Shared mutual exclusion primitives
#include <shared_mutex>
// Stop tokens for std::jthread
#include <stop_token>
// std::thread class and supporting functions
#include <thread>



#endif
