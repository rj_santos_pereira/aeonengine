#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CORE_COREATOMICOPERATIONS
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CORE_COREATOMICOPERATIONS

#include "CoreMemory.hpp"

namespace Aeon::Atomic::Internal
{
	template <class Type>
	// Load, Store, Xchg, CmpXchg
	concept CLinearizableObject = CBool<Type> || CEnum<Type> || CNumeric<Type> || CPointer<Type>;

	template <class Type>
	// Inc, Dec, Add, Sub
	concept CLinearizableIndexableObject = CNumeric<Type> || CObjectPointer<Type>;

	template <class Type>
	// Mul, Div, Neg
	concept CLinearizableComputableObject = CNumeric<Type>;

	template <class Type>
	// Mod, Shl, Shr
	concept CLinearizableIntegralComputableObject = CInteger<Type>;

	template <class Type>
	// Not, And, Or, Xor
	concept CLinearizableBitwiseComputableObject = CBool<Type> || CEnum<Type> || CInteger<Type>;

	// Enum that represents possible memory orderings to use in atomic operations
	enum struct EMemoryOrder			: uint8
	{
		Relaxed 						= 0b0000'0000,
		Acquire 						= 0b0000'0001,
		Release 						= 0b0000'0010,
		AcqRel 							= 0b0000'0011,
		SeqCst 							= 0b0000'0111,
	};

	template <class ObjectType, class = void>
	struct ZzInternal_TLinearizableObjectUnderlyingStorage;

	template <class ObjectType>
	struct ZzInternal_TLinearizableObjectUnderlyingStorage<ObjectType, TEnableIf<VIsBool<ObjectType>>>
	{
		using Type = uint8;
	};
	template <class ObjectType>
	struct ZzInternal_TLinearizableObjectUnderlyingStorage<ObjectType, TEnableIf<VIsEnum<ObjectType>>>
	{
		using Type = TEnumUnderlyingType<ObjectType>;
	};
	template <class ObjectType>
	struct ZzInternal_TLinearizableObjectUnderlyingStorage<ObjectType, TEnableIf<VIsInteger<ObjectType>>>
	{
		using Type = ObjectType;
	};
	template <class ObjectType>
	struct ZzInternal_TLinearizableObjectUnderlyingStorage<ObjectType, TEnableIf<VIsFloatingPoint<ObjectType>>>
	{
		using Type = TMakeSignMagnitudeInteger<false, sizeof(ObjectType)>;
	};
	template <class ObjectType>
	struct ZzInternal_TLinearizableObjectUnderlyingStorage<ObjectType, TEnableIf<VIsPointer<ObjectType>>>
	{
		using Type = diffint;
	};

	template <class ObjectType>
	using TLinearizableObjectUnderlyingStorage = typename ZzInternal_TLinearizableObjectUnderlyingStorage<ObjectType>::Type;

	template <class ObjectType>
	AEON_SPEC(NODISCARD, FORCEINLINE)
	ObjectType CastToObject(TLinearizableObjectUnderlyingStorage<ObjectType> Value) noexcept
	{
		if constexpr (VIsBool<ObjectType> || VIsEnum<ObjectType>)
		{
			return static_cast<ObjectType>(Value);
		}
		else if constexpr (VIsFloatingPoint<ObjectType> || VIsPointer<ObjectType>)
		{
			return bit_cast<ObjectType>(Value);
		}
		else
		{
			return Value;
		}
	}
	template <class ObjectType>
	AEON_SPEC(NODISCARD, FORCEINLINE)
	TLinearizableObjectUnderlyingStorage<ObjectType> CastToStorage(ObjectType Value) noexcept
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;
		if constexpr (VIsBool<ObjectType> || VIsEnum<ObjectType>)
		{
			return static_cast<StorageType>(Value);
		}
		else if constexpr (VIsFloatingPoint<ObjectType> || VIsPointer<ObjectType>)
		{
			return bit_cast<StorageType>(Value);
		}
		else
		{
			return Value;
		}
	}
}

namespace Aeon::Atomic::Internal
{
	// NOTE For now, only Windows needs the ObjectSizeValue parameter, to determine which function to call in certain cases
	//  (since there are 1/2/4/8/16-byte variants of the _Interlocked functions)

	// TODO maybe it would be more compact to generalize the _Interlocked functions instead; parameterize on sizeof(ObjectType)

	template <class ObjectType, EMemoryOrder, szint = Aeon::SizeOf(ObjectType)>
	struct GAtomicOperation_Load; // Bool, Enum, Int, Flt, Ptr
	template <class ObjectType, EMemoryOrder, szint = Aeon::SizeOf(ObjectType)>
	struct GAtomicOperation_Store; // Bool, Enum, Int, Flt, Ptr
	template <class ObjectType, EMemoryOrder, szint = Aeon::SizeOf(ObjectType)>
	struct GAtomicOperation_Xchg; // Bool, Enum, Int, Flt, Ptr
	template <class ObjectType, EMemoryOrder, szint = Aeon::SizeOf(ObjectType)>
	struct GAtomicOperation_CmpXchg; // Bool, Enum, Int, Flt, Ptr

	template <class ObjectType, EMemoryOrder, szint = Aeon::SizeOf(ObjectType)>
	struct GAtomicOperation_Inc; // Int, Flt, ObjPtr
	template <class ObjectType, EMemoryOrder, szint = Aeon::SizeOf(ObjectType)>
	struct GAtomicOperation_Dec; // Int, Flt, ObjPtr

	template <class ObjectType, EMemoryOrder, szint = Aeon::SizeOf(ObjectType)>
	struct GAtomicOperation_Neg; // Int, Flt
	template <class ObjectType, EMemoryOrder, szint = Aeon::SizeOf(ObjectType)>
	struct GAtomicOperation_Add; // Int, Flt, ObjPtr
	template <class ObjectType, EMemoryOrder, szint = Aeon::SizeOf(ObjectType)>
	struct GAtomicOperation_Sub; // Int, Flt, ObjPtr
	template <class ObjectType, EMemoryOrder, szint = Aeon::SizeOf(ObjectType)>
	struct GAtomicOperation_Mul; // Int, Flt
	template <class ObjectType, EMemoryOrder, szint = Aeon::SizeOf(ObjectType)>
	struct GAtomicOperation_Div; // Int, Flt
	template <class ObjectType, EMemoryOrder, szint = Aeon::SizeOf(ObjectType)>
	struct GAtomicOperation_Mod; // Int

	template <class ObjectType, EMemoryOrder, szint = Aeon::SizeOf(ObjectType)>
	struct GAtomicOperation_Not; // Bool, Enum, Int
	template <class ObjectType, EMemoryOrder, szint = Aeon::SizeOf(ObjectType)>
	struct GAtomicOperation_And; // Bool, Enum, Int
	template <class ObjectType, EMemoryOrder, szint = Aeon::SizeOf(ObjectType)>
	struct GAtomicOperation_Or; // Bool, Enum, Int
	template <class ObjectType, EMemoryOrder, szint = Aeon::SizeOf(ObjectType)>
	struct GAtomicOperation_Xor; // Bool, Enum, Int
	template <class ObjectType, EMemoryOrder, szint = Aeon::SizeOf(ObjectType)>
	struct GAtomicOperation_Shl; // Int
	template <class ObjectType, EMemoryOrder, szint = Aeon::SizeOf(ObjectType)>
	struct GAtomicOperation_Shr; // Int

#if AEON_ARCHITECTURE_X86 or AEON_ARCHITECTURE_X86_64

#if AEON_PLATFORM_WINDOWS

#define ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP(CasSize, OpExpr) \
	AEON_TOKEN_CONCAT(ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP_OVERLOAD_, CasSize)(OpExpr)

#define ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP_OVERLOAD_1(OpExpr) \
	ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP_INVOKE(OpExpr,\
                                            	OldValue = NewValue,\
                                            	OldValue != NewValue,\
												_InterlockedCompareExchange8,\
												reinterpret_cast<char volatile*>(Target),\
												static_cast<char>(NewValue),\
												static_cast<char>(OldValue))
#define ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP_OVERLOAD_2(OpExpr) \
	ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP_INVOKE(OpExpr,\
                                            	OldValue = NewValue,\
                                            	OldValue != NewValue,\
												_InterlockedCompareExchange16,\
												reinterpret_cast<short volatile*>(Target),\
												static_cast<short>(NewValue),\
												static_cast<short>(OldValue))
#define ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP_OVERLOAD_4(OpExpr) \
	ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP_INVOKE(OpExpr,\
                                            	OldValue = NewValue,\
                                            	OldValue != NewValue,\
												_InterlockedCompareExchange,\
												reinterpret_cast<long volatile*>(Target),\
												static_cast<long>(NewValue),\
												static_cast<long>(OldValue))
#define ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP_OVERLOAD_8(OpExpr) \
	ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP_INVOKE(OpExpr,\
												OldValue = NewValue,\
												OldValue != NewValue,\
												_InterlockedCompareExchange64,\
												reinterpret_cast<long long volatile*>(Target),\
												static_cast<long long>(NewValue),\
												static_cast<long long>(OldValue))
#define ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP_OVERLOAD_16(OpExpr) \
	ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP_INVOKE(OpExpr,\
												,\
												NewValue != 1,\
												_InterlockedCompareExchange128,\
												reinterpret_cast<long long volatile*>(Target),\
												static_cast<long long>(NewValue.High),\
												static_cast<long long>(NewValue.Low),\
												reinterpret_cast<long long*>(Aeon::AddressOf(OldValue)))

#define ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP_INVOKE(OpExpr, SetupExpr, LoopExpr, CasExpr, ...) \
	StorageType OldValue = *static_cast<StorageType volatile*>(Target);                                                                              \
	StorageType NewValue = OldValue;                                                                                                                 \
	_ReadWriteBarrier();                                                                                                                             \
	do                                                                                                                                               \
	{                                                                                                                                                \
		SetupExpr;                                                                                                                                   \
		NewValue = OpExpr;                                                                                                                           \
		NewValue = static_cast<StorageType>(CasExpr(__VA_ARGS__));                                                                                   \
	}                                                                                                                                                \
	while (LoopExpr);

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GAtomicOperation_Load

	// Relaxed

	template <class ObjectType, szint ObjectSizeValue>
	struct GAtomicOperation_Load<ObjectType, EMemoryOrder::Relaxed, ObjectSizeValue>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target) noexcept
		{
			// May be reordered by compiler
			StorageType Result = *static_cast<StorageType volatile*>(Target);
			return CastToObject<ObjectType>(Result);
		}
	};

	// Acquire

	template <class ObjectType, szint ObjectSizeValue>
	struct GAtomicOperation_Load<ObjectType, EMemoryOrder::Acquire, ObjectSizeValue>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target) noexcept
		{
			// LoadLoad and LoadStore barriers are given by default on x86
			StorageType Result = *static_cast<StorageType volatile*>(Target);
			
			// Prevent compiler reordering
			_ReadWriteBarrier();
			// ALTERNATIVE: use LFENCE to prevent compiler reorder; overkill
			//_mm_lfence();
			
			return CastToObject<ObjectType>(Result);
		}
	};

	// Release (Illegal)

	template <class ObjectType, szint ObjectSizeValue>
	struct GAtomicOperation_Load<ObjectType, EMemoryOrder::Release, ObjectSizeValue>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target) noexcept
		{
			AEON_UNUSED(Target);
			static_assert(VAlwaysFalse<ObjectType>, "Illegal memory ordering for Load operation");
			return ObjectType{};
		}
	};

	// AcqRel (Illegal)

	template <class ObjectType, szint ObjectSizeValue>
	struct GAtomicOperation_Load<ObjectType, EMemoryOrder::AcqRel, ObjectSizeValue>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target) noexcept
		{
			AEON_UNUSED(Target);
			static_assert(VAlwaysFalse<ObjectType>, "Illegal memory ordering for Load operation");
			return ObjectType{};
		}
	};

	// SeqCst

	template <class ObjectType, szint ObjectSizeValue>
	struct GAtomicOperation_Load<ObjectType, EMemoryOrder::SeqCst, ObjectSizeValue>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target) noexcept
		{
			// LoadLoad and LoadStore barriers are given by default on x86
			StorageType Result = *static_cast<StorageType volatile*>(Target);
			
			// Prevent compiler reordering
			_ReadWriteBarrier();
			// ALTERNATIVE: use LFENCE to prevent compiler reorder; overkill
			//_mm_lfence();
			
			return CastToObject<ObjectType>(Result);
		}
	};

	// Relaxed, Acquire, Release, AcqRel, SeqCst

	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Load<ObjectType, MemoryOrderValue, 16>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target) noexcept
		{
			// Atomic load
			StorageType Result = 0;
			_InterlockedCompareExchange128(reinterpret_cast<long long volatile*>(Target),
										   static_cast<long long>(0), static_cast<long long>(0),
										   reinterpret_cast<long long*>(Aeon::AddressOf(Result)));
			return CastToObject<ObjectType>(Result);
		}
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GAtomicOperation_Store

	// Relaxed

	template <class ObjectType, szint ObjectSizeValue>
	struct GAtomicOperation_Store<ObjectType, EMemoryOrder::Relaxed, ObjectSizeValue>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static void Execute(StorageType* Target, ObjectType Value) noexcept
		{
			// May be reordered by compiler
			*static_cast<StorageType volatile*>(Target) = CastToStorage(Value);
		}
	};

	// Acquire (Illegal)

	template <class ObjectType, szint ObjectSizeValue>
	struct GAtomicOperation_Store<ObjectType, EMemoryOrder::Acquire, ObjectSizeValue>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static void Execute(StorageType* Target, ObjectType Value) noexcept
		{
			AEON_UNUSED(Target, Value);
			static_assert(VAlwaysFalse<ObjectType>, "Illegal memory ordering for Store operation");
		}
	};

	// Release

	template <class ObjectType, szint ObjectSizeValue>
	struct GAtomicOperation_Store<ObjectType, EMemoryOrder::Release, ObjectSizeValue>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static void Execute(StorageType* Target, ObjectType Value) noexcept
		{
			// LoadStore and StoreStore barriers are (also) given by default on x86
			
			// Prevent compiler reordering
			_ReadWriteBarrier();
			// ALTERNATIVE: use SFENCE to prevent compiler reorder; overkill
			//_mm_sfence();
			
			*static_cast<StorageType volatile*>(Target) = CastToStorage(Value);
		}
	};

	// AcqRel (Illegal)

	template <class ObjectType, szint ObjectSizeValue>
	struct GAtomicOperation_Store<ObjectType, EMemoryOrder::AcqRel, ObjectSizeValue>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static void Execute(StorageType* Target, ObjectType Value) noexcept
		{
			AEON_UNUSED(Target, Value);
			static_assert(VAlwaysFalse<ObjectType>, "Illegal memory ordering for Store operation");
		}
	};

	// SeqCst

	// TODO non-temporal (streaming) stores don't follow the usual rules, and may need SFENCE instructions to re-establish SeqCst semantics; see
	//  https://stackoverflow.com/questions/55231677/x86-mfence-and-c-memory-barrier
	//  https://stackoverflow.com/questions/50609934/globally-invisible-load-instructions
	//  https://stackoverflow.com/questions/35830641/can-x86-reorder-a-narrow-store-with-a-wider-load-that-fully-contains-it/35910141#35910141

	template <class ObjectType>
	struct GAtomicOperation_Store<ObjectType, EMemoryOrder::SeqCst, 1>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static void Execute(StorageType* Target, ObjectType Value) noexcept
		{
			// CHECKME benchmark this and compare with MFENCE
			_InterlockedExchange8(reinterpret_cast<char volatile*>(Target),
								  static_cast<char>(CastToStorage(Value)));
			
			// Alternative: use MFENCE, which gives StoreLoad barriers,
			// but is less supported on older CPUs and (potentially) less performant on newer ones
			//_mm_mfence();
			//*reinterpret_cast<char volatile*>(Target) = Value;
			//_mm_mfence();
		}
	};
	template <class ObjectType>
	struct GAtomicOperation_Store<ObjectType, EMemoryOrder::SeqCst, 2>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static void Execute(StorageType* Target, ObjectType Value) noexcept
		{
			_InterlockedExchange16(reinterpret_cast<short volatile*>(Target),
								   static_cast<short>(CastToStorage(Value)));
		}
	};
	template <class ObjectType>
	struct GAtomicOperation_Store<ObjectType, EMemoryOrder::SeqCst, 4>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static void Execute(StorageType* Target, ObjectType Value) noexcept
		{
			_InterlockedExchange(reinterpret_cast<long volatile*>(Target),
								 static_cast<long>(CastToStorage(Value)));
		}
	};
	template <class ObjectType>
	struct GAtomicOperation_Store<ObjectType, EMemoryOrder::SeqCst, 8>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static void Execute(StorageType* Target, ObjectType Value) noexcept
		{
			_InterlockedExchange64(reinterpret_cast<long long volatile*>(Target),
								   static_cast<long long>(CastToStorage(Value)));
		}
	};

	// Relaxed, Acquire, Release, AcqRel, SeqCst

	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Store<ObjectType, MemoryOrderValue, 16>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static void Execute(StorageType* Target, ObjectType Value) noexcept
		{
			ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP(16, Value);
		}
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GAtomicOperation_Xchg

	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Xchg<ObjectType, MemoryOrderValue, 1>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Value) noexcept
		{
			StorageType OldValue = static_cast<StorageType>(_InterlockedExchange8(reinterpret_cast<char volatile*>(Target),
																				  static_cast<char>(CastToStorage(Value))));
			return CastToObject<ObjectType>(OldValue);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Xchg<ObjectType, MemoryOrderValue, 2>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Value) noexcept
		{
			StorageType OldValue = static_cast<StorageType>(_InterlockedExchange16(reinterpret_cast<short volatile*>(Target),
																				   static_cast<short>(CastToStorage(Value))));
			return CastToObject<ObjectType>(OldValue);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Xchg<ObjectType, MemoryOrderValue, 4>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Value) noexcept
		{
			StorageType OldValue = static_cast<StorageType>(_InterlockedExchange(reinterpret_cast<long volatile*>(Target),
																				 static_cast<long>(CastToStorage(Value))));
			return CastToObject<ObjectType>(OldValue);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Xchg<ObjectType, MemoryOrderValue, 8>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Value) noexcept
		{
			StorageType OldValue = static_cast<StorageType>(_InterlockedExchange64(reinterpret_cast<long long volatile*>(Target),
																				   static_cast<long long>(CastToStorage(Value))));
			return CastToObject<ObjectType>(OldValue);
		}
	};

	// Relaxed, Acquire, Release, AcqRel, SeqCst

	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Xchg<ObjectType, MemoryOrderValue, 16>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Value) noexcept
		{
			ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP(16, Value);
			return CastToObject<ObjectType>(OldValue);
		}
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GAtomicOperation_CmpXchg

	// Relaxed, Acquire, Release, AcqRel, SeqCst

	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_CmpXchg<ObjectType, MemoryOrderValue, 1>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType NewValue, ObjectType ExpectedValue) noexcept
		{
			StorageType OldValue = static_cast<StorageType>(
				_InterlockedCompareExchange8(reinterpret_cast<char volatile*>(Target),
											 static_cast<char>(CastToStorage(NewValue)),
											 static_cast<char>(CastToStorage(ExpectedValue)))
				);
			return CastToObject<ObjectType>(OldValue);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_CmpXchg<ObjectType, MemoryOrderValue, 2>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType NewValue, ObjectType ExpectedValue) noexcept
		{
			StorageType OldValue = static_cast<StorageType>(
				_InterlockedCompareExchange16(reinterpret_cast<short volatile*>(Target),
											  static_cast<short>(CastToStorage(NewValue)),
											  static_cast<short>(CastToStorage(ExpectedValue)))
				);
			return CastToObject<ObjectType>(OldValue);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_CmpXchg<ObjectType, MemoryOrderValue, 4>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType NewValue, ObjectType ExpectedValue) noexcept
		{
			StorageType OldValue = static_cast<StorageType>(
				_InterlockedCompareExchange(reinterpret_cast<long volatile*>(Target),
											static_cast<long>(CastToStorage(NewValue)),
											static_cast<long>(CastToStorage(ExpectedValue)))
				);
			return CastToObject<ObjectType>(OldValue);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_CmpXchg<ObjectType, MemoryOrderValue, 8>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType NewValue, ObjectType ExpectedValue) noexcept
		{
			StorageType OldValue = static_cast<StorageType>(
				_InterlockedCompareExchange64(reinterpret_cast<long long volatile*>(Target),
											  static_cast<long long>(CastToStorage(NewValue)),
											  static_cast<long long>(CastToStorage(ExpectedValue)))
				);
			return CastToObject<ObjectType>(OldValue);
		}
	};

	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_CmpXchg<ObjectType, MemoryOrderValue, 16>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType NewValue, ObjectType ExpectedValue) noexcept
		{
			StorageType OldValue = CastToStorage(ExpectedValue);
			_InterlockedCompareExchange128(reinterpret_cast<long long volatile*>(Target),
										   static_cast<long long>(NewValue.High),
										   static_cast<long long>(NewValue.Low),
										   reinterpret_cast<long long*>(Aeon::AddressOf(OldValue)));
			return CastToObject<ObjectType>(OldValue);
		}
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GAtomicOperation_Inc

	// Relaxed, Acquire, Release, AcqRel, SeqCst

	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Inc<ObjectType, MemoryOrderValue, 1>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target) noexcept
		{
			StorageType Result = static_cast<StorageType>(_InterlockedExchangeAdd8(reinterpret_cast<char volatile*>(Target),
																				   static_cast<char>(+1)));
			return CastToObject<ObjectType>(Result);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Inc<ObjectType, MemoryOrderValue, 2>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target) noexcept
		{
			StorageType Result = static_cast<StorageType>(_InterlockedExchangeAdd16(reinterpret_cast<short volatile*>(Target),
																					static_cast<short>(+1)));
			return CastToObject<ObjectType>(Result);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Inc<ObjectType, MemoryOrderValue, 4>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target) noexcept
		{
			// Note to future self: _InterlockedIncrement (and Decrement) return the NEW value, not the old one
			StorageType Result = static_cast<StorageType>(_InterlockedExchangeAdd(reinterpret_cast<long volatile*>(Target),
																				  static_cast<long>(+1)));
			return CastToObject<ObjectType>(Result);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Inc<ObjectType, MemoryOrderValue, 8>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target) noexcept
		{
			StorageType Result = static_cast<StorageType>(_InterlockedExchangeAdd64(reinterpret_cast<long long volatile*>(Target),
																					static_cast<long long>(+1)));
			return CastToObject<ObjectType>(Result);
		}
	};

	// Floating-Point

	template <EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Inc<float32, MemoryOrderValue, 4>
	{
		using ObjectType = float32;
		using StorageType = uint32;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target) noexcept
		{
			ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP(4, CastToStorage(CastToObject<ObjectType>(OldValue) + 1.0_f32));
			return CastToObject<ObjectType>(OldValue);
		}
	};
	template <EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Inc<float64, MemoryOrderValue, 8>
	{
		using ObjectType = float64;
		using StorageType = uint64;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target) noexcept
		{
			ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP(8, CastToStorage(CastToObject<ObjectType>(OldValue) + 1.0_f64));
			return CastToObject<ObjectType>(OldValue);
		}
	};

	// Pointer

	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Inc<ObjectType*, MemoryOrderValue, 4>
	{
		using StorageType = ptrint;

		AEON_FORCEINLINE static ObjectType* Execute(StorageType* Target) noexcept
		{
			StorageType Result = static_cast<StorageType>(
				_InterlockedExchangeAdd(reinterpret_cast<long volatile*>(Target),
										static_cast<long>(+Aeon::SizeOf(ObjectType)))
				);
			return CastToObject<ObjectType*>(Result);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Inc<ObjectType*, MemoryOrderValue, 8>
	{
		using StorageType = ptrint;

		AEON_FORCEINLINE static ObjectType* Execute(StorageType* Target) noexcept
		{
			StorageType Result = static_cast<StorageType>(
				_InterlockedExchangeAdd64(reinterpret_cast<long long volatile*>(Target),
										  static_cast<long long>(+Aeon::SizeOf(ObjectType)))
				);
			return CastToObject<ObjectType*>(Result);
		}
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GAtomicOperation_Dec

	// Relaxed, Acquire, Release, AcqRel, SeqCst

	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Dec<ObjectType, MemoryOrderValue, 1>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target) noexcept
		{
			StorageType Result = static_cast<StorageType>(_InterlockedExchangeAdd8(reinterpret_cast<char volatile*>(Target),
																				   static_cast<char>(-1)));
			return CastToObject<ObjectType>(Result);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Dec<ObjectType, MemoryOrderValue, 2>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target) noexcept
		{
			StorageType Result = static_cast<StorageType>(_InterlockedExchangeAdd16(reinterpret_cast<short volatile*>(Target),
																					static_cast<short>(-1)));
			return CastToObject<ObjectType>(Result);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Dec<ObjectType, MemoryOrderValue, 4>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target) noexcept
		{
			StorageType Result = static_cast<StorageType>(_InterlockedExchangeAdd(reinterpret_cast<long volatile*>(Target),
																				  static_cast<long>(-1)));
			return CastToObject<ObjectType>(Result);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Dec<ObjectType, MemoryOrderValue, 8>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target) noexcept
		{
			StorageType Result = static_cast<StorageType>(_InterlockedExchangeAdd64(reinterpret_cast<long long volatile*>(Target),
																					static_cast<long long>(-1)));
			return CastToObject<ObjectType>(Result);
		}
	};

	// Floating-Point

	template <EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Dec<float32, MemoryOrderValue, 4>
	{
		using ObjectType = float32;
		using StorageType = uint32;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target) noexcept
		{
			ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP(4, CastToStorage(CastToObject<ObjectType>(OldValue) - 1.0_f32));
			return CastToObject<ObjectType>(OldValue);
		}
	};
	template <EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Dec<float64, MemoryOrderValue, 8>
	{
		using ObjectType = float64;
		using StorageType = uint64;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target) noexcept
		{
			ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP(8, CastToStorage(CastToObject<ObjectType>(OldValue) - 1.0_f64));
			return CastToObject<ObjectType>(OldValue);
		}
	};

	// Pointer

	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Dec<ObjectType*, MemoryOrderValue, 4>
	{
		using StorageType = ptrint;

		AEON_FORCEINLINE static ObjectType* Execute(StorageType* Target) noexcept
		{
			StorageType Result = static_cast<StorageType>(
				_InterlockedExchangeAdd(reinterpret_cast<long volatile*>(Target),
										static_cast<long>(-Aeon::SizeOf(ObjectType)))
				);
			return CastToObject<ObjectType*>(Result);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Dec<ObjectType*, MemoryOrderValue, 8>
	{
		using StorageType = ptrint;

		AEON_FORCEINLINE static ObjectType* Execute(StorageType* Target) noexcept
		{
			StorageType Result = static_cast<StorageType>(
				_InterlockedExchangeAdd64(reinterpret_cast<long long volatile*>(Target),
										  static_cast<long long>(-Aeon::SizeOf(ObjectType)))
				);
			return CastToObject<ObjectType*>(Result);
		}
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GAtomicOperation_Neg

	// Relaxed, Acquire, Release, AcqRel, SeqCst

	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Neg<ObjectType, MemoryOrderValue, 1>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target) noexcept
		{
			ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP(1, -OldValue);
			return CastToObject<ObjectType>(OldValue);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Neg<ObjectType, MemoryOrderValue, 2>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target) noexcept
		{
			ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP(2, -OldValue);
			return CastToObject<ObjectType>(OldValue);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Neg<ObjectType, MemoryOrderValue, 4>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target) noexcept
		{
			// Adapted to work for floating-point as well
			ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP(4, CastToStorage(-CastToObject<ObjectType>(OldValue)));
			return CastToObject<ObjectType>(OldValue);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Neg<ObjectType, MemoryOrderValue, 8>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target) noexcept
		{
			// Adapted to work for floating-point as well
			ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP(8, CastToStorage(-CastToObject<ObjectType>(OldValue)));
			return CastToObject<ObjectType>(OldValue);
		}
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GAtomicOperation_Add

	// Relaxed, Acquire, Release, AcqRel, SeqCst

	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Add<ObjectType, MemoryOrderValue, 1>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			StorageType Result = static_cast<StorageType>(_InterlockedExchangeAdd8(reinterpret_cast<char volatile*>(Target),
																				   static_cast<char>(+Operand)));
			return CastToObject<ObjectType>(Result);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Add<ObjectType, MemoryOrderValue, 2>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			StorageType Result = static_cast<StorageType>(_InterlockedExchangeAdd16(reinterpret_cast<short volatile*>(Target),
																					static_cast<short>(+Operand)));
			return CastToObject<ObjectType>(Result);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Add<ObjectType, MemoryOrderValue, 4>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			StorageType Result = static_cast<StorageType>(_InterlockedExchangeAdd(reinterpret_cast<long volatile*>(Target),
																				  static_cast<long>(+Operand)));
			return CastToObject<ObjectType>(Result);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Add<ObjectType, MemoryOrderValue, 8>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			StorageType Result = static_cast<StorageType>(_InterlockedExchangeAdd64(reinterpret_cast<long long volatile*>(Target),
																					static_cast<long long>(+Operand)));
			return CastToObject<ObjectType>(Result);
		}
	};

	// Floating-Point

	template <EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Add<float32, MemoryOrderValue, 4>
	{
		using ObjectType = float32;
		using StorageType = uint32;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP(4, CastToStorage(CastToObject<ObjectType>(OldValue) + Operand));
			return CastToObject<ObjectType>(OldValue);
		}
	};
	template <EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Add<float64, MemoryOrderValue, 8>
	{
		using ObjectType = float64;
		using StorageType = uint64;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP(8, CastToStorage(CastToObject<ObjectType>(OldValue) + Operand));
			return CastToObject<ObjectType>(OldValue);
		}
	};

	// Pointer

	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Add<ObjectType*, MemoryOrderValue, 4>
	{
		using StorageType = ptrint;

		AEON_FORCEINLINE static ObjectType* Execute(StorageType* Target, StorageType Operand) noexcept
		{
			StorageType Result = static_cast<StorageType>(
				_InterlockedExchangeAdd(reinterpret_cast<long volatile*>(Target),
										static_cast<long>(+Aeon::SizeOf(ObjectType, Operand)))
			);
			return CastToObject<ObjectType*>(Result);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Add<ObjectType*, MemoryOrderValue, 8>
	{
		using StorageType = ptrint;

		AEON_FORCEINLINE static ObjectType* Execute(StorageType* Target, StorageType Operand) noexcept
		{
			StorageType Result = static_cast<StorageType>(
				_InterlockedExchangeAdd64(reinterpret_cast<long long volatile*>(Target),
										  static_cast<long long>(+Aeon::SizeOf(ObjectType, Operand)))
			);
			return CastToObject<ObjectType*>(Result);
		}
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GAtomicOperation_Sub

	// Relaxed, Acquire, Release, AcqRel, SeqCst

	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Sub<ObjectType, MemoryOrderValue, 1>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			StorageType Result = static_cast<StorageType>(_InterlockedExchangeAdd8(reinterpret_cast<char volatile*>(Target),
																				   static_cast<char>(-Operand)));
			return CastToObject<ObjectType>(Result);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Sub<ObjectType, MemoryOrderValue, 2>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			StorageType Result = static_cast<StorageType>(_InterlockedExchangeAdd16(reinterpret_cast<short volatile*>(Target),
																					static_cast<short>(-Operand)));
			return CastToObject<ObjectType>(Result);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Sub<ObjectType, MemoryOrderValue, 4>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			StorageType Result = static_cast<StorageType>(_InterlockedExchangeAdd(reinterpret_cast<long volatile*>(Target),
																				  static_cast<long>(-Operand)));
			return CastToObject<ObjectType>(Result);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Sub<ObjectType, MemoryOrderValue, 8>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			StorageType Result = static_cast<StorageType>(_InterlockedExchangeAdd64(reinterpret_cast<long long volatile*>(Target),
																					static_cast<long long>(-Operand)));
			return CastToObject<ObjectType>(Result);
		}
	};

	// Floating-Point

	template <EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Sub<float32, MemoryOrderValue, 4>
	{
		using ObjectType = float32;
		using StorageType = uint32;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP(4, CastToStorage(CastToObject<ObjectType>(OldValue) - Operand));
			return CastToObject<ObjectType>(OldValue);
		}
	};
	template <EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Sub<float64, MemoryOrderValue, 8>
	{
		using ObjectType = float64;
		using StorageType = uint64;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP(8, CastToStorage(CastToObject<ObjectType>(OldValue) - Operand));
			return CastToObject<ObjectType>(OldValue);
		}
	};

	// Pointer

	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Sub<ObjectType*, MemoryOrderValue, 4>
	{
		using StorageType = ptrint;

		AEON_FORCEINLINE static ObjectType* Execute(StorageType* Target, StorageType Operand) noexcept
		{
			StorageType Result = static_cast<StorageType>(
				_InterlockedExchangeAdd(reinterpret_cast<long volatile*>(Target),
										static_cast<long>(-Aeon::SizeOf(ObjectType, Operand)))
			);
			return CastToObject<ObjectType*>(Result);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Sub<ObjectType*, MemoryOrderValue, 8>
	{
		using StorageType = ptrint;

		AEON_FORCEINLINE static ObjectType* Execute(StorageType* Target, StorageType Operand) noexcept
		{
			StorageType Result = static_cast<StorageType>(
				_InterlockedExchangeAdd64(reinterpret_cast<long long volatile*>(Target),
										  static_cast<long long>(-Aeon::SizeOf(ObjectType, Operand)))
			);
			return CastToObject<ObjectType*>(Result);
		}
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GAtomicOperation_Mul

	// Relaxed, Acquire, Release, AcqRel, SeqCst

	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Mul<ObjectType, MemoryOrderValue, 1>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP(1, OldValue * Operand);
			return CastToObject<ObjectType>(OldValue);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Mul<ObjectType, MemoryOrderValue, 2>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP(2, OldValue * Operand);
			return CastToObject<ObjectType>(OldValue);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Mul<ObjectType, MemoryOrderValue, 4>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP(4, CastToStorage(CastToObject<ObjectType>(OldValue) * Operand));
			return CastToObject<ObjectType>(OldValue);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Mul<ObjectType, MemoryOrderValue, 8>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP(8, CastToStorage(CastToObject<ObjectType>(OldValue) * Operand));
			return CastToObject<ObjectType>(OldValue);
		}
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GAtomicOperation_Div

	// Relaxed, Acquire, Release, AcqRel, SeqCst

	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Div<ObjectType, MemoryOrderValue, 1>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP(1, OldValue / Operand);
			return CastToObject<ObjectType>(OldValue);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Div<ObjectType, MemoryOrderValue, 2>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP(2, OldValue / Operand);
			return CastToObject<ObjectType>(OldValue);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Div<ObjectType, MemoryOrderValue, 4>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP(4, CastToStorage(CastToObject<ObjectType>(OldValue) / Operand));
			return CastToObject<ObjectType>(OldValue);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Div<ObjectType, MemoryOrderValue, 8>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP(8, CastToStorage(CastToObject<ObjectType>(OldValue) / Operand));
			return CastToObject<ObjectType>(OldValue);
		}
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GAtomicOperation_Mod

	// Relaxed, Acquire, Release, AcqRel, SeqCst

	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Mod<ObjectType, MemoryOrderValue, 1>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP(1, OldValue % Operand);
			return CastToObject<ObjectType>(OldValue);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Mod<ObjectType, MemoryOrderValue, 2>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP(2, OldValue % Operand);
			return CastToObject<ObjectType>(OldValue);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Mod<ObjectType, MemoryOrderValue, 4>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP(4, OldValue % Operand);
			return CastToObject<ObjectType>(OldValue);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Mod<ObjectType, MemoryOrderValue, 8>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP(8, OldValue % Operand);
			return CastToObject<ObjectType>(OldValue);
		}
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GAtomicOperation_Not

	// Relaxed, Acquire, Release, AcqRel, SeqCst

	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Not<ObjectType, MemoryOrderValue, 1>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target) noexcept
		{
			StorageType Result = static_cast<StorageType>(_InterlockedXor8(reinterpret_cast<char volatile*>(Target),
																		   static_cast<char>(CastToStorage(AeonMath::MakeBitMask<8>()))));
			return CastToObject<ObjectType>(Result);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Not<ObjectType, MemoryOrderValue, 2>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target) noexcept
		{
			StorageType Result = static_cast<StorageType>(_InterlockedXor16(reinterpret_cast<short volatile*>(Target),
																			static_cast<short>(CastToStorage(AeonMath::MakeBitMask<16>()))));
			return CastToObject<ObjectType>(Result);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Not<ObjectType, MemoryOrderValue, 4>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target) noexcept
		{
			StorageType Result = static_cast<StorageType>(_InterlockedXor(reinterpret_cast<long volatile*>(Target),
																		  static_cast<long>(CastToStorage(AeonMath::MakeBitMask<32>()))));
			return CastToObject<ObjectType>(Result);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Not<ObjectType, MemoryOrderValue, 8>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target) noexcept
		{
			StorageType Result = static_cast<StorageType>(_InterlockedXor64(reinterpret_cast<long long volatile*>(Target),
																			static_cast<long long>(CastToStorage(AeonMath::MakeBitMask<64>()))));
			return CastToObject<ObjectType>(Result);
		}
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GAtomicOperation_And

	// Relaxed, Acquire, Release, AcqRel, SeqCst

	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_And<ObjectType, MemoryOrderValue, 1>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			StorageType Result = static_cast<StorageType>(_InterlockedAnd8(reinterpret_cast<char volatile*>(Target),
																		   static_cast<char>(CastToStorage(Operand))));
			return CastToObject<ObjectType>(Result);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_And<ObjectType, MemoryOrderValue, 2>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			StorageType Result = static_cast<StorageType>(_InterlockedAnd16(reinterpret_cast<short volatile*>(Target),
																			static_cast<short>(CastToStorage(Operand))));
			return CastToObject<ObjectType>(Result);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_And<ObjectType, MemoryOrderValue, 4>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			StorageType Result = static_cast<StorageType>(_InterlockedAnd(reinterpret_cast<long volatile*>(Target),
																		  static_cast<long>(CastToStorage(Operand))));
			return CastToObject<ObjectType>(Result);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_And<ObjectType, MemoryOrderValue, 8>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			StorageType Result = static_cast<StorageType>(_InterlockedAnd64(reinterpret_cast<long long volatile*>(Target),
																			static_cast<long long>(CastToStorage(Operand))));
			return CastToObject<ObjectType>(Result);
		}
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GAtomicOperation_Or

	// Relaxed, Acquire, Release, AcqRel, SeqCst

	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Or<ObjectType, MemoryOrderValue, 1>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			StorageType Result = static_cast<StorageType>(_InterlockedOr8(reinterpret_cast<char volatile*>(Target),
																		  static_cast<char>(CastToStorage(Operand))));
			return CastToObject<ObjectType>(Result);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Or<ObjectType, MemoryOrderValue, 2>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			StorageType Result = static_cast<StorageType>(_InterlockedOr16(reinterpret_cast<short volatile*>(Target),
																		   static_cast<short>(CastToStorage(Operand))));
			return CastToObject<ObjectType>(Result);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Or<ObjectType, MemoryOrderValue, 4>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			StorageType Result = static_cast<StorageType>(_InterlockedOr(reinterpret_cast<long volatile*>(Target),
																		 static_cast<long>(CastToStorage(Operand))));
			return CastToObject<ObjectType>(Result);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Or<ObjectType, MemoryOrderValue, 8>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			StorageType Result = static_cast<StorageType>(_InterlockedOr64(reinterpret_cast<long long volatile*>(Target),
																		   static_cast<long long>(CastToStorage(Operand))));
			return CastToObject<ObjectType>(Result);
		}
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GAtomicOperation_Xor

	// Relaxed, Acquire, Release, AcqRel, SeqCst

	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Xor<ObjectType, MemoryOrderValue, 1>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			StorageType Result = static_cast<StorageType>(_InterlockedXor8(reinterpret_cast<char volatile*>(Target),
																		   static_cast<char>(CastToStorage(Operand))));
			return CastToObject<ObjectType>(Result);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Xor<ObjectType, MemoryOrderValue, 2>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			StorageType Result = static_cast<StorageType>(_InterlockedXor16(reinterpret_cast<short volatile*>(Target),
																			static_cast<short>(CastToStorage(Operand))));
			return CastToObject<ObjectType>(Result);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Xor<ObjectType, MemoryOrderValue, 4>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			StorageType Result = static_cast<StorageType>(_InterlockedXor(reinterpret_cast<long volatile*>(Target),
																		  static_cast<long>(CastToStorage(Operand))));
			return CastToObject<ObjectType>(Result);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Xor<ObjectType, MemoryOrderValue, 8>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			StorageType Result = static_cast<StorageType>(_InterlockedXor64(reinterpret_cast<long long volatile*>(Target),
																			static_cast<long long>(CastToStorage(Operand))));
			return CastToObject<ObjectType>(Result);
		}
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GAtomicOperation_Shl

	// Relaxed, Acquire, Release, AcqRel, SeqCst

	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Shl<ObjectType, MemoryOrderValue, 1>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP(1, OldValue << Operand);
			return CastToObject<ObjectType>(OldValue);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Shl<ObjectType, MemoryOrderValue, 2>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP(2, OldValue << Operand);
			return CastToObject<ObjectType>(OldValue);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Shl<ObjectType, MemoryOrderValue, 4>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP(4, OldValue << Operand);
			return CastToObject<ObjectType>(OldValue);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Shl<ObjectType, MemoryOrderValue, 8>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP(8, OldValue << Operand);
			return CastToObject<ObjectType>(OldValue);
		}
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GAtomicOperation_Shr

	// Relaxed, Acquire, Release, AcqRel, SeqCst

	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Shr<ObjectType, MemoryOrderValue, 1>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP(1, OldValue >> Operand);
			return CastToObject<ObjectType>(OldValue);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Shr<ObjectType, MemoryOrderValue, 2>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP(2, OldValue >> Operand);
			return CastToObject<ObjectType>(OldValue);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Shr<ObjectType, MemoryOrderValue, 4>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP(4, OldValue >> Operand);
			return CastToObject<ObjectType>(OldValue);
		}
	};
	template <class ObjectType, EMemoryOrder MemoryOrderValue>
	struct GAtomicOperation_Shr<ObjectType, MemoryOrderValue, 8>
	{
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		AEON_FORCEINLINE static ObjectType Execute(StorageType* Target, ObjectType Operand) noexcept
		{
			ZZINTERNAL_AEON_ATOMIC_FALLBACK_CAS_OP(8, OldValue >> Operand);
			return CastToObject<ObjectType>(OldValue);
		}
	};

#endif // AEON_PLATFORM

#elif AEON_ARCHITECTURE_ARM or AEON_ARCHITECTURE_ARM64
#error Not implemented yet!
#endif // AEON_ARCHITECTURE
}

#endif