#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CORE_COREMATH
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CORE_COREMATH

#include "CorePreprocessor.hpp"
#include "CoreDebugger.hpp"

// TODO move this elsewhere (maybe define it in cmake?)
#define AEON_MATH_DEFAULT_WORD_SIZE 32

// Resources
//  - Floating-point visualizers and calculators:
// 		https://www.exploringbinary.com/floating-point-converter/
// 		http://weitz.de/ieee/
// 		https://lukaskollmer.de/ieee-754-visualizer/
// 		https://www.h-schmidt.net/FloatConverter/IEEE754.html

namespace Aeon::Math
{
	// TODO should these methods take a const lref?

	// TODO implement most methods based on stdlib basic math <cmath>
	//  - prob skip fp manip funcs (except modf, ilogb, and maybe nextafter and copysign)
	//  - skip special funcs

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  Min, Max

	template <CObject Type, CObject ... Types>
	constexpr Type Min(Type Value1, Type Value2, Types ... Values)
	requires (... && CSameClass<Type, Types>)
	{
		Type MinValue = Value1 < Value2 ? Value1 : Value2;
		(..., (MinValue = MinValue < Values ? MinValue : Values));
		return MinValue;
	}

	template <CObject Type, CObject ... Types>
	constexpr Type Max(Type Value1, Type Value2, Types ... Values)
	requires (... && CSameClass<Type, Types>)
	{
		Type MaxValue = Value2 < Value1 ? Value1 : Value2;
		(..., (MaxValue = Values < MaxValue ? MaxValue : Values));
		return MaxValue;
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  Clamp, IsClamped

	template <CObject Type>
	constexpr Type Clamp(Type Value, Type MinValue, Type MaxValue)
	{
		AEON_ASSERT(!(MaxValue < MinValue));
		return MinValue < Value ? (Value < MaxValue ? Value : MaxValue) : MinValue;
	}

	template <CObject Type>
	constexpr bool IsClamped(Type Value, Type MinValue, Type MaxValue)
	{
		AEON_ASSERT(!(MaxValue < MinValue));
		return !(Value < MinValue || MaxValue < Value);
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  AbsUnsafe, Abs

	template <CNumeric NumType>
	constexpr NumType AbsUnsafe(NumType Value)
	{
		if constexpr (VIsFloatingPoint<NumType>)
		{
			// NOTE Adding zero converts the special case -0.0 into +0.0
			return Value < NumType(0) ? -Value : +Value + NumType(0);
		}
		else if constexpr (VIsSigned<NumType>)
		{
			return Value < NumType(0) ? -Value : +Value;
		}
		else
		{
			return Value;
		}
	}

	template <CNumeric NumType>
	constexpr NumType Abs(NumType Value)
	{
		if constexpr (VIsInteger<NumType> && VIsSigned<NumType>)
		{
			if (Value < NumType(0))
			{
				constexpr bool HasNonRepresentableValues = TNumericProperties<NumType>::Minimum < -TNumericProperties<NumType>::Maximum;
				if constexpr (HasNonRepresentableValues)
				{
					if (Value < -TNumericProperties<NumType>::Maximum)
					{
						if_not_consteval
						{
							constexpr STypeInfo NumTypeInfo = typeid(NumType);
							AEON_DEBUGGER_FORMAT_PRINT(Warn, "Machine cannot represent absolute value of '{0:}' with type '{1:}', "
															 "returning TNumericProperties<{1:}>::Maximum",
															 Value, NumTypeInfo.Name());
						}
						return TNumericProperties<NumType>::Maximum;
					}
				}
				return -Value;
			}
			return +Value;
		}
		else
		{
			return AbsUnsafe(Value);
		}
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  IsEven, IsOdd

	template <CInteger IntType>
	constexpr bool IsEven(IntType Value)
	{
		return (Value & 1) == 0;
	}
	template <CInteger IntType>
	constexpr bool IsOdd(IntType Value)
	{
		return (Value & 1) == 1;
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  Wrap

	template <CSigned NumType>
	constexpr NumType Wrap(NumType Value, NumType MinValue, NumType MaxValue)
	{
		AEON_ASSERT(!(MaxValue < MinValue));
		NumType Range = MaxValue - MinValue;
		// HACK Force Value to not be farther from the closest limit than the distance between the limits;
		//  This simplifies the computation, two simple conditionals instead of looping while reducing the value until it's close to the range.
		AEON_ASSERT(!(Range < Max(MinValue - Value, Value - MaxValue)));
		return (Value < MinValue) ? (Value + Range) : ((MaxValue < Value) ? (Value - Range) : Value);
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  ApproxEqual, ApproxZero, ApproxEqualULP

	// Logic based on https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/

	template <CFloatingPoint FloatType>
	constexpr bool IsApproxEqual(FloatType Value1, FloatType Value2, FloatType Tolerance = TNumericProperties<FloatType>::Epsilon)
	{
		// Relative epsilon comparison
		return Abs(Value1 - Value2) <= ((Abs(Value1) + Abs(Value2)) * Tolerance);
	}

	template <CFloatingPoint FloatType>
	constexpr bool IsApproxZero(FloatType Value, FloatType Tolerance = TNumericProperties<FloatType>::Epsilon)
	{
		// Absolute epsilon comparison (relative comparison doesn't work too well for values close to zero)
		return Abs(Value) <= Tolerance;
	}

	template <CFloatingPoint FloatType>
	struct ZzInternal_TMakeFloatULPType;
	template <>
	struct ZzInternal_TMakeFloatULPType<float32>
		: TClass<uint32> {};
	template <>
	struct ZzInternal_TMakeFloatULPType<float64>
		: TClass<uint64> {};

	template <CFloatingPoint FloatType>
	struct ZzInternal_TMakeFloatULPValue
		: TObject<typename ZzInternal_TMakeFloatULPType<FloatType>::Type(TNumericProperties<FloatType>::MinimumWritableDigits)> {};

	template <CFloatingPoint FloatType>
	using TMakeFloatULPType = typename ZzInternal_TMakeFloatULPType<FloatType>::Type;

	template <CFloatingPoint FloatType>
	inline constexpr TMakeFloatULPType<FloatType> VMakeFloatULPValue = ZzInternal_TMakeFloatULPValue<FloatType>::Value;

	template <CStandardFloatingPoint FloatType>
	constexpr bool ApproxEqualULP(FloatType Value1, FloatType Value2, TMakeFloatULPType<FloatType> Tolerance = VMakeFloatULPValue<FloatType>)
	{
		// FIXME review this
		using ULPType = TMakeFloatULPType<FloatType>;
		if ((Value1 < 0.0) == (Value2 <= 0.0))
		{
			ULPType BitValue1 = bit_cast<ULPType>(AbsUnsafe(Value1));
			ULPType BitValue2 = bit_cast<ULPType>(AbsUnsafe(Value2));
			ULPType BitDistance = BitValue1 < BitValue2 ? BitValue2 - BitValue1 : BitValue1 - BitValue2;
			return BitDistance <= Tolerance;
		}
		return Value1 == Value2;
	}

#if 0
	template <CI3EFloatingPoint FloatType>
	constexpr bool AlmostZeroULP(FloatType Value, TMakeFloatULPType<FloatType> Tolerance = VMakeFloatULPValue<FloatType>)
	{
		using ULPType = TMakeFloatULPType<FloatType>;
		if (Value != 0.0)
		{
			ULPType BitValue = bit_cast<ULPType>(Abs(Value));
			ULPType BitAfterZero = bit_cast<ULPType>(TNumericProperties<FloatType>::Smallest);
			ULPType BitDistance = BitValue - BitAfterZero + 1;
			return BitDistance <= Tolerance;
		}
		return true;
	}
#endif

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GetBit, SetBit, UnsetBit, FlipBit, MakeBitMask

	template <szint BitIndexValue, CInteger IntType>
	constexpr IntType GetBit(IntType Value)
	requires (BitIndexValue < TNumericProperties<IntType>::RadixDigits)
	{
		return IntType(Value & (IntType(1) << BitIndexValue));
	}
	template <CInteger IntType>
	constexpr IntType GetBit(IntType Value, szint BitIndex)
	{
		AEON_ASSERT(BitIndex < TNumericProperties<IntType>::RadixDigits);
		return IntType(Value & (IntType(1) << BitIndex));
	}

	template <szint BitIndexValue, CInteger IntType>
	constexpr IntType SetBit(IntType Value)
	requires (BitIndexValue < TNumericProperties<IntType>::RadixDigits)
	{
		return IntType(Value | (IntType(1) << BitIndexValue));
	}
	template <CInteger IntType>
	constexpr IntType SetBit(IntType Value, szint BitIndex)
	{
		AEON_ASSERT(BitIndex < TNumericProperties<IntType>::RadixDigits);
		return IntType(Value | (IntType(1) << BitIndex));
	}

	template <szint BitIndexValue, CInteger IntType>
	constexpr IntType UnsetBit(IntType Value)
	requires (BitIndexValue < TNumericProperties<IntType>::RadixDigits)
	{
		return IntType(Value & ~(IntType(1) << BitIndexValue));
	}
	template <CInteger IntType>
	constexpr IntType UnsetBit(IntType Value, szint BitIndex)
	{
		AEON_ASSERT(BitIndex < TNumericProperties<IntType>::RadixDigits);
		return IntType(Value & ~(IntType(1) << BitIndex));
	}

	template <szint BitIndexValue, CInteger IntType>
	constexpr IntType FlipBit(IntType Value)
	requires (BitIndexValue < TNumericProperties<IntType>::RadixDigits)
	{
		return IntType(Value ^ (IntType(1) << BitIndexValue));
	}
	template <CInteger IntType>
	constexpr IntType FlipBit(IntType Value, szint BitIndex)
	{
		AEON_ASSERT(BitIndex < TNumericProperties<IntType>::RadixDigits);
		return IntType(Value ^ (IntType(1) << BitIndex));
	}

	template <CUnsignedInteger BitType, szint BitCountValue, szint BitOffsetValue = 0>
	constexpr BitType MakeBitMask() // CHECKME could we make this consteval?
	requires (BitCountValue + BitOffsetValue <= TNumericProperties<BitType>::RadixDigits)
	{
		constexpr BitType BitCountLimit = TNumericProperties<BitType>::RadixDigits - 1;
		return ((BitType(BitCountValue <= BitCountLimit) << (BitCountValue & BitCountLimit)) - 1) << BitOffsetValue;
	}
	template <CUnsignedInteger BitType>
	constexpr BitType MakeBitMask(szint BitCount, szint BitOffset = 0)
	{
		AEON_ASSERT(BitCount + BitOffset <= TNumericProperties<BitType>::RadixDigits);
		constexpr BitType BitCountLimit = TNumericProperties<BitType>::RadixDigits - 1;
		return ((BitType(BitCount <= BitCountLimit) << (BitCount & BitCountLimit)) - 1) << BitOffset;
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  TDetectFloatNumLayout, DIsoFP32Layout, DIsoFP64Layout

	struct DIso60559Fp32Layout
	{
		using BitType								= uint32;

		static constexpr uint8 MantissaBitCount 	= 23;
		static constexpr uint8 ExponentBitCount 	= 8;
		static constexpr uint8 SignBitCount 		= 1;

		static constexpr uint8 MantissaBitOffset 	= 0;
		static constexpr uint8 ExponentBitOffset 	= MantissaBitOffset + MantissaBitCount;
		static constexpr uint8 SignBitOffset 		= ExponentBitOffset + ExponentBitCount;

		static constexpr int32 ExponentBias 		= -127;

		BitType Mantissa 							: MantissaBitCount;
		BitType Exponent 							: ExponentBitCount;
		BitType Sign 								: SignBitCount;
	};

	struct DIso60559Fp64Layout
	{
		using BitType								= uint64;

		static constexpr uint8 MantissaBitCount 	= 52;
		static constexpr uint8 ExponentBitCount 	= 11;
		static constexpr uint8 SignBitCount 		= 1;

		static constexpr uint8 MantissaBitOffset 	= 0;
		static constexpr uint8 ExponentBitOffset 	= MantissaBitOffset + MantissaBitCount;
		static constexpr uint8 SignBitOffset 		= ExponentBitOffset + ExponentBitCount;

		static constexpr int32 ExponentBias 		= -1023;

		BitType Mantissa 							: MantissaBitCount;
		BitType Exponent 							: ExponentBitCount;
		BitType Sign 								: SignBitCount;
	};

	template <CFloatingPoint FloatType>
	using TDetectFloatNumLayout =
		AEON_TRAIT_IF(VIsSameClass<FloatType, float32> && TNumericProperties<FloatType>::IsISO60559Conforming)
		<
			DIso60559Fp32Layout
		>
		AEON_TRAIT_ELSEIF(VIsSameClass<FloatType, float64> && TNumericProperties<FloatType>::IsISO60559Conforming)
		<
			DIso60559Fp64Layout
		>
		AEON_TRAIT_ELSE
		<
			TNullTypeTag
		>
		AEON_TRAIT_ENDIF;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  DecomposeFloat, ComposeFloat

	template <CFloatingPoint FloatType>
	constexpr TDetectFloatNumLayout<FloatType> DecomposeFloat(FloatType Value)
	{
#	if not AEON_IS_LITTLE_ENDIAN_ARCH
#		error FIXME
#	endif

		using LayoutType = TDetectFloatNumLayout<FloatType>;
		using BitType = typename LayoutType::BitType;

		constexpr BitType MantissaMask 	= MakeBitMask<BitType, LayoutType::MantissaBitCount, LayoutType::MantissaBitOffset>();
		constexpr BitType ExponentMask 	= MakeBitMask<BitType, LayoutType::ExponentBitCount, LayoutType::ExponentBitOffset>();
		constexpr BitType SignMask 		= MakeBitMask<BitType, LayoutType::SignBitCount, LayoutType::SignBitOffset>();

		BitType BitValue = bit_cast<BitType>(Value);

		return { .Mantissa = (BitValue & MantissaMask) >> LayoutType::MantissaBitOffset,
				 .Exponent = (BitValue & ExponentMask) >> LayoutType::ExponentBitOffset,
				 .Sign = (BitValue & SignMask) >> LayoutType::SignBitOffset };
	}

	template <CFloatingPoint FloatType>
	constexpr FloatType ComposeFloat(TDetectFloatNumLayout<FloatType> Value)
	{
#	if not AEON_IS_LITTLE_ENDIAN_ARCH
#		error FIXME
#	endif

		using LayoutType = TDetectFloatNumLayout<FloatType>;
		using BitType = typename LayoutType::BitType;

		// HACK Clang seems to freak out without the cast to BitType,
		//  as it assumes that the type of the bitfields is (u)int32 when FloatType is float64
		BitType BitValue = (BitType(Value.Mantissa) << LayoutType::MantissaBitOffset)
						 | (BitType(Value.Exponent) << LayoutType::ExponentBitOffset)
						 | (BitType(Value.Sign) << LayoutType::SignBitOffset);

		return bit_cast<FloatType>(BitValue);
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  IsNaN, IsFinite, IsInfinity

	template <CStandardFloatingPoint FloatType>
	constexpr bool IsNaN(FloatType Value)
	{
		// NOTE NaNs cannot appear as the result of a constant-evaluated expression,
		//  e.g. any arithmetic operation that uses NaN as an operand will result in a NaN,
		//  and so such an expression cannot be constant-evaluated;
		//  but may otherwise be produced through constexpr values,
		//  and thus be part of a subexpression that produces any other result.
		return Value != Value;
	}

	template <CStandardFloatingPoint FloatType>
	constexpr bool IsInfinity(FloatType Value)
	{
		auto DecompValue = DecomposeFloat(Value);
		using LayoutType = decltype(DecompValue);

		using BitType = typename LayoutType::BitType;
		constexpr BitType InfinityMask = MakeBitMask<BitType, LayoutType::ExponentBitCount>();

		return DecompValue.Exponent == InfinityMask && DecompValue.Mantissa == 0;
	}

#if 0
	template <CStandardFloatingPoint FloatType>
	constexpr bool IsInfinity(FloatType Value)
	{
		return !IsNaN(Value) && IsNaN(Value - Value);
	}
#endif

	template <CStandardFloatingPoint FloatType>
	constexpr bool IsFinite(FloatType Value)
	{
		auto DecompValue = DecomposeFloat(Value);
		using LayoutType = decltype(DecompValue);

		using BitType = typename LayoutType::BitType;
		constexpr BitType InfinityMask = MakeBitMask<BitType, LayoutType::ExponentBitCount>();

		return DecompValue.Exponent != InfinityMask;
	}

#if 0
	template <CStandardFloatingPoint FloatType>
	constexpr bool IsFinite(FloatType Value)
	{
		return !IsNaN(Value) && !IsNaN(Value - Value);
	}
#endif

	template <CStandardFloatingPoint FloatType>
	constexpr FloatType MakeNaN(bool IsSignaling = false, typename TDetectFloatNumLayout<FloatType>::BitType Payload = 0)
	{
		using LayoutType = TDetectFloatNumLayout<FloatType>;
		using BitType = typename LayoutType::BitType;

		LayoutType NaNLayout = DecomposeFloat(IsSignaling ? TNumericProperties<FloatType>::SignalingNaN : TNumericProperties<FloatType>::QuietNaN);

	//	NaNLayout.Sign = 0;
	//	NaNLayout.Exponent = MakeBitMask<BitType, LayoutType::ExponentBitCount>();
		NaNLayout.Mantissa = Payload & MakeBitMask<BitType, LayoutType::MantissaBitCount - 1>();

		// NOTE On x86, ARM, and basically every modern architecture,
		//  the signaling bit (is actually a "is_quiet" flag) is the most significant bit of the mantissa.
	//	if (!IsSignaling)
	//	{
	//		SetBit<LayoutType::MantissaBitCount - 1>(NaNLayout.Mantissa);
	//	}

		if (NaNLayout.Mantissa == 0)
		{
			SetBit<LayoutType::MantissaBitCount - 2>(NaNLayout.Mantissa);
		}

		return ComposeFloat<FloatType>(NaNLayout);
	}

	static_assert(IsFinite(0.0_f32) && IsFinite(0.0_f64) &&
				  !IsInfinity(0.0_f32) && !IsInfinity(0.0_f64) &&
				  !IsNaN(0.0_f32) && !IsNaN(0.0_f64));
	static_assert(!IsFinite(TNumericProperties<float32>::Infinity) && !IsFinite(TNumericProperties<float64>::Infinity) &&
				  IsInfinity(TNumericProperties<float32>::Infinity) && IsInfinity(TNumericProperties<float64>::Infinity) &&
				  !IsNaN(TNumericProperties<float32>::Infinity) && !IsNaN(TNumericProperties<float64>::Infinity));
	static_assert(!IsFinite(TNumericProperties<float32>::QuietNaN) && !IsFinite(TNumericProperties<float64>::QuietNaN) &&
				  !IsInfinity(TNumericProperties<float32>::QuietNaN) && !IsInfinity(TNumericProperties<float64>::QuietNaN) &&
				  IsNaN(TNumericProperties<float32>::QuietNaN) && IsNaN(TNumericProperties<float64>::QuietNaN));
	static_assert(!IsFinite(TNumericProperties<float32>::SignalingNaN) && !IsFinite(TNumericProperties<float64>::SignalingNaN) &&
				  !IsInfinity(TNumericProperties<float32>::SignalingNaN) && !IsInfinity(TNumericProperties<float64>::SignalingNaN) &&
				  IsNaN(TNumericProperties<float32>::SignalingNaN) && IsNaN(TNumericProperties<float64>::SignalingNaN));


// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  Trunc, Round, Floor, Ceil

	template <CFloatingPoint FloatType>
	AEON_FORCEINLINE constexpr FloatType Trunc(FloatType Value)
	{
		if_consteval
		{
			// Simple cases, early exit
			if (!IsFinite(Value) || Value == FloatType(0))
			{
				return Value;
			}

			auto DecompValue = DecomposeFloat(Value);
			using LayoutType = decltype(DecompValue);

			// Compute the actual exponent, considering the bias
			int32 Exponent = int32(DecompValue.Exponent) + LayoutType::ExponentBias;

			// If exponent is negative, number is always between +-0 and +-1 (non-inclusive), so return +-0
			if (Exponent < 0)
			{
				return DecompValue.Sign ? -FloatType(0) : +FloatType(0);
			}
			// If exponent is non-negative, but smaller than the mantissa's precision,
			//  we need to mask out all bits of the mantissa with index smaller than (precision - exponent),
			//  as those bits represent the fractional part of the number
			else if (Exponent < LayoutType::MantissaBitCount)
			{
				using BitType = typename LayoutType::BitType;
				BitType MantissaMask = MakeBitMask<BitType>(Exponent, LayoutType::MantissaBitCount - Exponent);

				// Mask out the fractional part bits and overwrite the decomposed value
				DecompValue.Mantissa = DecompValue.Mantissa & MantissaMask;

				// Now just recompose the value with the new mantissa
				return ComposeFloat<FloatType>(DecompValue);
			}
			// If exponent is positive and equal or greater than the mantissa's precision,
			//  the mantissa cannot represent fractional numbers anymore,
			//  and the number is already integral
			else
			{
				return Value;
			}
		}
		else
		{
			return std::trunc(Value);
		}
	}

	template <CFloatingPoint FloatType>
	AEON_FORCEINLINE constexpr FloatType Round(FloatType Value)
	{
		if_consteval
		{
			// Simple cases, early exit
			if (!IsFinite(Value) || Value == FloatType(0))
			{
				return Value;
			}

			auto DecompValue = DecomposeFloat(Value);
			using LayoutType = decltype(DecompValue);

			// Compute the actual exponent, considering the bias
			int32 Exponent = int32(DecompValue.Exponent) + LayoutType::ExponentBias;

			// If exponent is smaller than -1, number is always between +-0.0 and +-0.5 (non-inclusive), so return +-0.0
			if (Exponent < -1)
			{
				return DecompValue.Sign ? -FloatType(0) : +FloatType(0);
			}
			// If exponent is equal to -1, number is always between +-0.5 and +-1.0 (non-inclusive), so return +-1.0
			// NOTE This case cannot be coalesced with the case below because MakeBitMask (and GetBit) would break
			else if (Exponent == -1)
			{
				return DecompValue.Sign ? -FloatType(1) : +FloatType(1);
			}
			// If exponent is non-negative, but smaller than the mantissa's precision,
			//  we need to mask out all bits of the mantissa with index smaller than (precision - exponent),
			//  as those bits represent the fractional part of the number
			else if (Exponent < LayoutType::MantissaBitCount)
			{
				using BitType = typename LayoutType::BitType;
				BitType MantissaMask = MakeBitMask<BitType>(Exponent, LayoutType::MantissaBitCount - Exponent);
				// Note that the bit with index = (precision - exponent - 1) represents 2^-1 (+-0.5),
				// which is a point of discontinuity of the rounding function,
				// and it's enough to check this bit for the domain considered,
				// i.e. the mantissa bits with the current exponent.
				bool RoundTowardInfinity = GetBit<BitType>(DecompValue.Mantissa, LayoutType::MantissaBitCount - Exponent - 1) != 0;

				// Mask out the fractional part bits and overwrite the decomposed value
				DecompValue.Mantissa = DecompValue.Mantissa & MantissaMask;

				// Recompose the value with the new mantissa
				FloatType NewValue = ComposeFloat<FloatType>(DecompValue);

				// Finally, check if we need to round towards infinity, and simply do a fp-add if so
				if (RoundTowardInfinity)
				{
					NewValue += DecompValue.Sign ? -FloatType(1) : +FloatType(1);
				}

				return NewValue;
			}
			// If exponent is positive and equal or greater than the mantissa's precision,
			//  the mantissa cannot represent fractional numbers anymore,
			//  and the number is already integral
			else
			{
				return Value;
			}
		}
		else
		{
			return std::round(Value);
		}
	}

	template <CFloatingPoint FloatType>
	AEON_FORCEINLINE constexpr FloatType Floor(FloatType Value)
	{
		if_consteval
		{
			// Simple cases, early exit
			if (!IsFinite(Value) || Value == FloatType(0))
			{
				return Value;
			}

			auto DecompValue = DecomposeFloat(Value);
			using LayoutType = decltype(DecompValue);

			// Compute the actual exponent, considering the bias
			int32 Exponent = int32(DecompValue.Exponent) + LayoutType::ExponentBias;

			// If exponent is negative, number is always between +-0 and +-1 (non-inclusive),
			// so return -1 or +0
			if (Exponent < 0)
			{
				return DecompValue.Sign ? -FloatType(1) : +FloatType(0);
			}
			// If exponent is non-negative, but smaller than the mantissa's precision,
			//  we need to mask out all bits of the mantissa with index smaller than (precision - exponent),
			//  as those bits represent the fractional part of the number
			else if (Exponent < LayoutType::MantissaBitCount)
			{
				using BitType = typename LayoutType::BitType;
				BitType MantissaMask = MakeBitMask<BitType>(Exponent, LayoutType::MantissaBitCount - Exponent);

				// Note that we only need to round to negative infinity when the value is negative,
				// as for positive values, the truncation suffices (i.e., for x >= 0, trunc and floor are equal);
				// It suffices to check whether the part of the mantissa we are masking out is non-zero,
				// as that corresponds to the fractional part of the number
				bool RoundTowardNegInfinity = DecompValue.Sign != 0 && (DecompValue.Mantissa & ~MantissaMask) != 0;

				// Mask out the fractional part bits and overwrite the decomposed value
				DecompValue.Mantissa = DecompValue.Mantissa & MantissaMask;

				// Recompose the value with the new mantissa
				FloatType NewValue = ComposeFloat<FloatType>(DecompValue);

				// Finally, check if we need to round towards negative infinity, and simply do a fp-add if so
				if (RoundTowardNegInfinity)
				{
					NewValue -= FloatType(1);
				}

				return NewValue;
			}
			// If exponent is positive and equal or greater than the mantissa's precision,
			//  the mantissa cannot represent fractional numbers anymore,
			//  and the number is already integral
			else
			{
				return Value;
			}
		}
		else
		{
			return std::floor(Value);
		}
	}

	template <CFloatingPoint FloatType>
	AEON_FORCEINLINE constexpr FloatType Ceil(FloatType Value)
	{
		if_consteval
		{
			// Simple cases, early exit
			if (!IsFinite(Value) || Value == FloatType(0))
			{
				return Value;
			}

			auto DecompValue = DecomposeFloat(Value);
			using LayoutType = decltype(DecompValue);

			// Compute the actual exponent, considering the bias
			int32 Exponent = int32(DecompValue.Exponent) + LayoutType::ExponentBias;

			// If exponent is negative, number is always between +-0 and +-1 (non-inclusive),
			// so return -0 or +1
			if (Exponent < 0)
			{
				return DecompValue.Sign ? -FloatType(0) : +FloatType(1);
			}
			// If exponent is non-negative, but smaller than the mantissa's precision,
			//  we need to mask out all bits of the mantissa with index smaller than (precision - exponent),
			//  as those bits represent the fractional part of the number
			else if (Exponent < LayoutType::MantissaBitCount)
			{
				using BitType = typename LayoutType::BitType;
				BitType MantissaMask = MakeBitMask<BitType>(Exponent, LayoutType::MantissaBitCount - Exponent);

				// Note that we only need to round to positive infinity when the value is positive,
				// as for negative values, the truncation suffices (i.e., for x <= 0, trunc and ceil are equal);
				// It suffices to check whether the part of the mantissa we are masking out is non-zero,
				// as that corresponds to the fractional part of the number
				bool RoundTowardPosInfinity = DecompValue.Sign == 0 && (DecompValue.Mantissa & ~MantissaMask) != 0;

				// Mask out the fractional part bits and overwrite the decomposed value
				DecompValue.Mantissa = DecompValue.Mantissa & MantissaMask;

				// Recompose the value with the new mantissa
				FloatType NewValue = ComposeFloat<FloatType>(DecompValue);

				// Finally, check if we need to round towards positive infinity, and simply do a fp-add if so
				if (RoundTowardPosInfinity)
				{
					NewValue += FloatType(1);
				}

				return NewValue;
			}
			// If exponent is positive and equal or greater than the mantissa's precision,
			//  the mantissa cannot represent fractional numbers anymore,
			//  and the number is already integral
			else
			{
				return Value;
			}
		}
		else
		{
			return std::ceil(Value);
		}
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  Mod, Frac

	template <CFloatingPoint FloatType>
	AEON_FORCEINLINE constexpr FloatType Mod(FloatType Num, FloatType Denom)
	{
		if_consteval
		{
			FloatType TruncQuot = Trunc(Num / Denom);
			return Num - TruncQuot * Denom;
		}
		else
		{
			return std::fmod(Num, Denom);
		}
	}

	template <CFloatingPoint FloatType>
	AEON_FORCEINLINE constexpr FloatType Frac(FloatType Value)
	{
		if_consteval
		{
			FloatType TruncValue = Trunc(Value);
			return Value - TruncValue;
		}
		else
		{
			return std::fmod(Value, FloatType(1));
		}
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  Rem, RemQuo

	template <CFloatingPoint FloatType>
	AEON_FORCEINLINE constexpr FloatType Rem(FloatType Num, FloatType Denom)
	{
		if_consteval
		{
			FloatType Quot = Num / Denom;

			FloatType LoQuot = Floor(Quot);
			FloatType HiQuot = Ceil(Quot);

			// Choose TruncQuot based on distance of adjacent integer quotients to actual quotient
			FloatType LoQuotDist = Quot - LoQuot;
			FloatType HiQuotDist = HiQuot - Quot;
			FloatType TruncQuot;
			if (LoQuotDist < HiQuotDist || LoQuot == HiQuot)
			{
				TruncQuot = LoQuot;
			}
			else if (HiQuotDist < LoQuotDist)
			{
				TruncQuot = HiQuot;
			}
			// When distance is equal for adjacent quotients, pick the quotient that is even, as per std::remainder
			else
			{
				auto DecompQuot = DecomposeFloat(LoQuot);
				using LayoutType = decltype(DecompQuot);

				int32 Exponent = int32(DecompQuot.Exponent) + LayoutType::ExponentBias;
				int32 ZeroExponentBitIndex = LayoutType::MantissaBitCount - Exponent;

				// If there is a bit of the mantissa corresponding to 2^0, henceforth the zexp-bit,
				// that bit dictates whether the number is even or odd, so we need to check it.
				// If there is no zexp-bit, because either the exponent itself is zero, or it is too large (greater than precision),
				// then we need only check the former; for the latter case, only even numbers are representable.
				bool IsEven = IsClamped(ZeroExponentBitIndex, 0, LayoutType::MantissaBitCount - 1)
					? GetBit(DecompQuot.Mantissa, ZeroExponentBitIndex) == 0
					: Exponent != 0;

				TruncQuot = IsEven ? LoQuot : HiQuot;
			}

			return Num - TruncQuot * Denom;
		}
		else
		{
			return std::remainder(Num, Denom);
		}
	}

	template <CFloatingPoint FloatType>
	struct GRemQuoResult
	{
		FloatType Rem;
		fltint Quo;
	};

	template <CFloatingPoint FloatType>
	AEON_FORCEINLINE constexpr GRemQuoResult<FloatType> RemQuo(FloatType Num, FloatType Denom)
	{
		if_consteval
		{
			FloatType Quot = Num / Denom;

			FloatType LoQuot = Floor(Quot);
			FloatType HiQuot = Ceil(Quot);

			// Choose TruncQuot based on distance of adjacent integer quotients to actual quotient
			FloatType LoQuotDist = Quot - LoQuot;
			FloatType HiQuotDist = HiQuot - Quot;
			FloatType TruncQuot;
			if (LoQuotDist < HiQuotDist || LoQuot == HiQuot)
			{
				TruncQuot = LoQuot;
			}
			else if (HiQuotDist < LoQuotDist)
			{
				TruncQuot = HiQuot;
			}
			// When distance is equal for adjacent quotients, pick the quotient that is even, as per std::remainder
			else
			{
				auto DecompQuot = DecomposeFloat(LoQuot);
				using LayoutType = decltype(DecompQuot);

				int32 Exponent = int32(DecompQuot.Exponent) + LayoutType::ExponentBias;
				int32 ZeroExponentBitIndex = LayoutType::MantissaBitCount - Exponent;

				// If there is a bit of the mantissa corresponding to 2^0, henceforth the zexp-bit,
				// that bit dictates whether the number is even or odd, so we need to check it.
				// If there is no zexp-bit, because either the exponent itself is zero, or it is too large (greater than precision),
				// then we need only check the former; for the latter case, only even numbers are representable.
				bool IsEven = IsClamped(ZeroExponentBitIndex, 0, LayoutType::MantissaBitCount - 1)
							  ? GetBit(DecompQuot.Mantissa, ZeroExponentBitIndex) == 0
							  : Exponent != 0;

				TruncQuot = IsEven ? LoQuot : HiQuot;
			}

			// HACK This works for now, since casting int64::max to float64 creates a number that is max + 1,
			//  but generally casting int to float might create a number much greater than the actual maximum
			AEON_CONSTEVAL_ASSERT(float64(TruncQuot) < float64(TNumericProperties<fltint>::Maximum));

#		if 0
			constexpr fltint PartQuotLimit = TNumericProperties<fltint>::Maximum;
			if (Abs(TruncQuot) > PartQuotLimit)
			{
				// FIXME compute (at least) 3 LSBs of TruncQuot
				//  see x87 FPREM1
			}
#		endif

			return { Num - TruncQuot * Denom, fltint(TruncQuot) };
		}
		else
		{
			int32 Quot;
			FloatType Remd = std::remquo(Num, Denom, &Quot);
			return { Remd, fltint(Quot) };
		}
	}

	// TODO maybe remove this
#if 0
//  Represents a floating-point number f as x*10^e,
//  where -1 <= x <= +1 and e >= 0
	template <CFloatingPoint FloatType>
	constexpr FloatType Fractional(FloatType Value, FloatType& Exponent)
	{
		Exponent = FloatType(0.0);
		while (Abs(Value) > FloatType(1.0))
		{
			Value *= FloatType(0.1);
			Exponent += FloatType(1.0);
		}
		return Value;
	}
#endif

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  Lzcnt, Tzcnt, Popcnt

	template <CUnsignedInteger IntType>
	AEON_FORCEINLINE constexpr IntType Lzcnt(IntType Value)
	{
		return IntType(std::countl_zero(Value));
	}

	template <CUnsignedInteger IntType>
	AEON_FORCEINLINE constexpr IntType Tzcnt(IntType Value)
	{
		return IntType(std::countr_zero(Value));
	}

	template <CUnsignedInteger IntType>
	AEON_FORCEINLINE constexpr IntType Popcnt(IntType Value)
	{
		return IntType(std::popcount(Value));
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  IsPowerOfTwo, LogPowerOfTwo, MulPowerOfTwo, DivPowerOfTwo, ModPowerOfTwo, NextPowerOfTwo, PrevPowerOfTwo

#if 0
	template <CUnsignedInteger IntType>
	constexpr bool IsNonZeroPowerOfTwo(IntType Value)
	{
		return Value != IntType(0) && (Value & (Value - 1)) == IntType(0);
	}
#endif

	template <CUnsignedInteger IntType>
	constexpr bool IsPowerOfTwo(IntType Value)
	{
		// This expression also considers 0 a power of two, which is useful in some situations
		// TODO move this comment to the doc of the function
		return (Value & (Value - 1)) == IntType(0);
	}

	template <CUnsignedInteger IntType>
	constexpr IntType LogPowerOfTwo(IntType Value) // TODO rename this (Log2PowerOfTwo ?)
	{
		return Tzcnt(Value);
	}

	template <CInteger IntNumType, CUnsignedInteger IntDenomType>
	constexpr IntNumType MulPowerOfTwo(IntNumType NumValue, IntDenomType DenomValue)
	{
		// Assumes that DenomValue is a power of two
		// CHECKME test performance of this vs simple mul, Intel's probably faster with mul, and AMD might be faster only on recent archs (Zen4)
	//	return IntNumType(NumValue << Tzcnt(DenomValue));
		return IntNumType(NumValue * DenomValue);
	}

	template <CInteger IntNumType, CUnsignedInteger IntDenomType>
	constexpr IntNumType DivPowerOfTwo(IntNumType NumValue, IntDenomType DenomValue)
	{
		// Assumes that DenomValue is a power of two
		return IntNumType(NumValue >> Tzcnt(DenomValue));
	}

	template <CInteger IntNumType, CUnsignedInteger IntDenomType>
	constexpr IntNumType ModPowerOfTwo(IntNumType NumValue, IntDenomType DenomValue)
	{
		// Assumes that DenomValue is a power of two
		return IntNumType(NumValue & (DenomValue - 1));
	}

	template <CUnsignedInteger IntType>
	constexpr IntType NextPowerOfTwo(IntType Value)
	{
		// std::bit_ceil
		return Value > 2 ? IntType(1) << (TNumericProperties<IntType>::RadixDigits - Lzcnt(Value - 1)) : Value;
	}

	template <CUnsignedInteger IntType>
	constexpr IntType PrevPowerOfTwo(IntType Value)
	{
		// std::bit_floor
		return Value > 2 ? IntType(1) << (TNumericProperties<IntType>::RadixDigits - Lzcnt(Value) - 1) : Value;
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  Log2, Log10

	template <CUnsignedInteger IntType>
	constexpr IntType Log2(IntType Value)
	{
		return IntType(TNumericProperties<IntType>::RadixDigits - Lzcnt(Value));
	}

	template <CUnsignedInteger IntType>
	constexpr IntType Log10(IntType Value)
	{
		constexpr uint64 PowersOf10[] =
			{
										1_u64, 							10_u64, 						100_u64,
									1'000_u64, 						10'000_u64, 					100'000_u64,
								1'000'000_u64, 					10'000'000_u64, 				100'000'000_u64,
							1'000'000'000_u64, 				10'000'000'000_u64, 			100'000'000'000_u64,
						1'000'000'000'000_u64, 			10'000'000'000'000_u64,			100'000'000'000'000_u64,
					1'000'000'000'000'000_u64, 		10'000'000'000'000'000_u64, 	100'000'000'000'000'000_u64,
				1'000'000'000'000'000'000_u64, 	10'000'000'000'000'000'000_u64,
			};

		// We multiply log2(Value) by the reciprocal of log2(10), approximated as a fraction (that fits in u8)
		// CHECKME ensure for each power of ten that this approximation gives the correct result;
		//  it suffices to check the boundaries for each power, as the (math) function that computes LogBase10 is linear and continuous
		IntType LogBase10 = Log2(Value) * IntType(77) / IntType(255);
		return LogBase10 - (Value < PowersOf10[LogBase10]);
	}

	// TODO overloads for floating-point

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  EncodeFloat, DecodeFloat

	// From http://stereopsis.com/radix.html
	// Keywords: radix sort histogramming float encoding

#pragma warning(push)
#pragma warning(disable: 4141)

	inline constexpr uint32 EncodeFloat(float32 Value)
	{
		auto BitValue = bit_cast<uint32>(Value);
		uint32 Mask = (int32(BitValue) >> 31) | 0x8000'0000;
		return BitValue ^ Mask;
	}

	inline constexpr uint64 EncodeFloat(float64 Value)
	{
		auto BitValue = bit_cast<uint64>(Value);
		uint64 Mask = (int64(BitValue) >> 63) | 0x8000'0000'0000'0000;
		return BitValue ^ Mask;
	}

	inline constexpr float32 DecodeFloat(uint32 BitValue)
	{
		uint32 Mask = ((BitValue >> 31) - 1) | 0x8000'0000;
		BitValue ^= Mask;
		return bit_cast<float32>(BitValue);
	}

	inline constexpr float64 DecodeFloat(uint64 BitValue)
	{
		uint64 Mask = ((BitValue >> 63) - 1) | 0x8000'0000'0000'0000;
		BitValue ^= Mask;
		return bit_cast<float64>(BitValue);
	}

#pragma warning(pop)

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Square, Cube, Square-root (Sqrt), Cube-root (Cbrt)

	// TODO use https://en.cppreference.com/w/cpp/numeric/math and https://en.cppreference.com/w/cpp/numeric/special_functions
	//  for reference (see also: https://www.open-std.org/jtc1/sc22/wg21/docs/papers/2003/n1514.pdf)

	template <CNumeric NumType>
	AEON_FORCEINLINE constexpr NumType Square(NumType Value)
	{
		return Value * Value;
	}

	template <CNumeric NumType>
	AEON_FORCEINLINE constexpr NumType Cube(NumType Value)
	{
		return Value * Value * Value;
	}

	// Constexpr root using Newton-Raphson method.
	// For nth-root, equation takes the form: (1.0 / N) * ((N - 1) * Elem1 + InitialValue / Pow(Elem1, N - 1))

	template <CFloatingPoint FloatType>
	AEON_FORCEINLINE constexpr FloatType Sqrt(FloatType Value)
	{
		if_consteval
		{
			Value = Value >= FloatType(0.0) ? Value : FloatType(0.0);

			FloatType InitialValue = Value;
			FloatType LastValue;

			if (Value != FloatType(0.0))
			{
				do
				{
					LastValue = Value;
					Value = FloatType(1.0 / 2.0) * (Value + InitialValue / Value);
				}
				while (Value != LastValue);
			}

			return Value;
		}
		else
		{
			return std::sqrt(Value);
		}
	}

	template <CFloatingPoint FloatType>
	AEON_FORCEINLINE constexpr FloatType Cbrt(FloatType Value)
	{
		if_consteval
		{
			Value = Value >= FloatType(0.0) ? Value : FloatType(0.0);

			FloatType InitialValue = Value;
			FloatType LastValue;

			if (Value != FloatType(0.0))
			{
				do
				{
					LastValue = Value;
					Value = FloatType(1.0 / 3.0) * (FloatType(2.0) * Value + InitialValue / (Value * Value));
				}
				while (Value != LastValue);
			}

			return Value;
		}
		else
		{
			return std::cbrt(Value);
		}
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  Power (Pow), Nth-power, Nth-root

	template <CInteger IntType>
	AEON_FORCEINLINE constexpr IntType Pow(IntType Base, IntType Exponent)
	{
		if_consteval
		{
			// TODO power in terms of the exp to the power of natural logarithm
			//  see https://en.wikipedia.org/wiki/Natural_logarithm#Efficient_computation

			Exponent = Exponent >= 0 ? Exponent : 0;

			IntType Result(1.0);
			for (IntType Current = 0; Current < Exponent; ++Current)
			{
				Result *= Base;
			}
			return Result;
		}
		else
		{
			return IntType(std::pow(Base, Exponent));
		}
	}

	template <CFloatingPoint FloatType>
	AEON_FORCEINLINE FloatType Pow(FloatType Base, FloatType Exponent)
	{
		return std::pow(Base, Exponent);
	}

	// TODO remove restriction to positive Exponents
	template <szint ExponentValue, CFloatingPoint FloatType>
	AEON_FORCEINLINE constexpr FloatType NthPower(FloatType Base)
	{
		if constexpr (ExponentValue == 2)
		{
			return Square(Base);
		}
		else if constexpr(ExponentValue == 3)
		{
			return Cube(Base);
		}
		else
		{
			if_consteval
			{
				// TODO power in terms of the exp to the power of natural logarithm
				//  see https://en.wikipedia.org/wiki/Natural_logarithm#Efficient_computation

				FloatType Result(1.0);
				for (szint Current = 0; Current < ExponentValue; ++Current)
				{
					Result *= Base;
				}
				return Result;
			}
			else
			{
				return Pow(Base, FloatType(ExponentValue));
			}
		}
	}

	// TODO remove restriction to positive Degrees
	template <szint DegreeValue, CFloatingPoint FloatType>
	AEON_FORCEINLINE constexpr FloatType NthRoot(FloatType Radicand)
	{
		static_assert(DegreeValue > 1);
		if constexpr (DegreeValue == 2)
		{
			return Sqrt(Radicand);
		}
		else if constexpr(DegreeValue == 3)
		{
			return Cbrt(Radicand);
		}
		else
		{
			if_consteval
			{
				Radicand = Radicand >= FloatType(0.0) ? Radicand : FloatType(0.0);

				FloatType InitialValue = Radicand;
				FloatType LastValue;

				if (Radicand != FloatType(0.0))
				do
				{
					LastValue = Radicand;
					Radicand = FloatType(1.0 / DegreeValue)
							 * (FloatType(DegreeValue - 1) * Radicand + InitialValue / NthPower<DegreeValue - 1>(Radicand));
				}
				while (Radicand != LastValue);

				return Radicand;
			}
			else
			{
				return Pow(Radicand, FloatType(1.0 / DegreeValue));
			}
		}
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Reciprocal Square-root (RSqrt)

	// Resources:
	// 	https://web.archive.org/web/20211215200301/https://cs.uwaterloo.ca/~m32rober/rsqrt.pdf
	// 	https://arxiv.org/pdf/1603.04483.pdf

	struct ENewtonRaphsonIterations
	{
		enum Flag : uint8
		{
			Zero = 0,
			One = 1,
			Two = 2,
		};
	};

	// TODO replace these with SSE rsqrtss

	template <ENewtonRaphsonIterations::Flag NumIterationsValue = ENewtonRaphsonIterations::One>
	constexpr float32 RSqrt(float32 Value)
	{
		float32 HalfInitialValue = Value * 0.5_f32;

		auto BitValue = bit_cast<uint32>(Value);
		BitValue = 0x5F37'5A86_u32 - (BitValue >> 1);
		Value = bit_cast<float32>(BitValue);

		if constexpr (NumIterationsValue >= ENewtonRaphsonIterations::One)
		{
			Value *= 1.5_f32 - HalfInitialValue * Value * Value;
		}
		if constexpr (NumIterationsValue == ENewtonRaphsonIterations::Two)
		{
			Value *= 1.5_f32 - HalfInitialValue * Value * Value;
		}

		return Value;
	}

	template <ENewtonRaphsonIterations::Flag NumIterationsValue = ENewtonRaphsonIterations::One>
	constexpr float64 RSqrt(float64 Value)
	{
		float64 HalfInitialValue = Value * 0.5_f64;

		auto BitValue = bit_cast<uint64>(Value);
		BitValue = 0x5FE6'EB50'C7B5'37A9_u64 - (BitValue >> 1);
		Value = bit_cast<float64>(BitValue);

		if constexpr (NumIterationsValue >= ENewtonRaphsonIterations::One)
		{
			Value *= 1.5_f64 - HalfInitialValue * Value * Value;
		}
		if constexpr (NumIterationsValue == ENewtonRaphsonIterations::Two)
		{
			Value *= 1.5_f64 - HalfInitialValue * Value * Value;
		}

		return Value;
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  Common constants

#if 0
	// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  Compute Pi

	template <class FloatType, uint64 ... DigitValues>
	consteval FloatType ComputePi_Internal(TNumericSequence<uint64, DigitValues ...>)
	{
		// Using the BBP Formula
		return FloatType((... + (1.0 / (AeonMath::Pow(16_u64, DigitValues))
								 * (4.0 / (8 * DigitValues + 1)
									- 2.0 / (8 * DigitValues + 4)
									- 1.0 / (8 * DigitValues + 5)
									- 1.0 / (8 * DigitValues + 6)))));
	}

	template <CFloatingPoint FloatType>
	consteval FloatType ComputePi()
	{
		return AeonMath::ComputePi_Internal<FloatType>(TMakeIntSequence<uint64,
																		uint64(TNumericProperties<FloatType>::WritableMinimumDigits)>{ });
	}
#endif

	template <CFloatingPoint FloatType>
	struct Constants;

#define ZZINTERNAL_AEON_DECLARE_MATH_CONSTANTS_NAMESPACE(BitSize) \
	template <>                                                                                                                                      \
	struct Constants<AEON_TOKEN_CONCAT(float, BitSize)>                                                                                              \
	{                                                                                                                                                \
		/* Transcendental constants */                                                                                                               \
	                                                                                                                                                 \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) GoldenRatio = std::numbers::phi_v<AEON_TOKEN_CONCAT(float, BitSize)>;                     \
	                                                                                                                                                 \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) Euler = std::numbers::e_v<AEON_TOKEN_CONCAT(float, BitSize)>;                             \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) Pi = std::numbers::pi_v<AEON_TOKEN_CONCAT(float, BitSize)>;                               \
		                                                                                                                                             \
		/*static constexpr AEON_TOKEN_CONCAT(float, BitSize) EulerMascheroni = std::numbers::egamma_v<AEON_TOKEN_CONCAT(float, BitSize)>;*/          \
	                                                                                                                                                 \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) InversePi = std::numbers::inv_pi_v<AEON_TOKEN_CONCAT(float, BitSize)>;                    \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) InverseSqrtPi = std::numbers::inv_sqrtpi_v<AEON_TOKEN_CONCAT(float, BitSize)>;            \
	                                                                                                                                                 \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) Ln_2 = std::numbers::ln2_v<AEON_TOKEN_CONCAT(float, BitSize)>;                            \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) Ln_10 = std::numbers::ln10_v<AEON_TOKEN_CONCAT(float, BitSize)>;                          \
	                                                                                                                                                 \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) Log2_Euler = std::numbers::log2e_v<AEON_TOKEN_CONCAT(float, BitSize)>;                    \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) Log10_Euler = std::numbers::log10e_v<AEON_TOKEN_CONCAT(float, BitSize)>;                  \
	                                                                                                                                                 \
        /* 6.283185307179586476 */                                                                                                                   \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) Tau = AEON_TOKEN_CONCAT(2.0_f, BitSize) * Pi;                                             \
                                                                                                                                                     \
		/* 1.570796326794896619 */	                                                                                                                 \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) HalfPi = AEON_TOKEN_CONCAT(0.5_f, BitSize) * Pi;                                          \
                                                                                                                                                     \
	    /* 1.047197551196597746 */                                                                                                                   \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) OneThirdPi                                                                                \
			= (AEON_TOKEN_CONCAT(1.0_f, BitSize) / AEON_TOKEN_CONCAT(3.0_f, BitSize)) * Pi;                                                          \
        /* 2.094395102393195492 */                                                                                                                   \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) TwoThirdsPi = (AEON_TOKEN_CONCAT(2.0_f, BitSize) / AEON_TOKEN_CONCAT(3.0_f, BitSize)) * Pi;             \
	                                                                                                                                                 \
        /* 0.785398163397448309 */                                                                                                                   \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) OneFourthPi = (AEON_TOKEN_CONCAT(1.0_f, BitSize) / AEON_TOKEN_CONCAT(4.0_f, BitSize)) * Pi;             \
		/* 2.356194490192344928 */                                                                                                                   \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) ThreeFourthsPi = (AEON_TOKEN_CONCAT(3.0_f, BitSize) / AEON_TOKEN_CONCAT(4.0_f, BitSize)) * Pi;          \
                                                                                                                                                     \
	    /* 0.523598775598298873 */                                                                                                                   \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) OneSixthPi = (AEON_TOKEN_CONCAT(1.0_f, BitSize) / AEON_TOKEN_CONCAT(6.0_f, BitSize)) * Pi;              \
		/* 2.617993877991494365 */                                                                                                                   \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) FiveSixthsPi = (AEON_TOKEN_CONCAT(5.0_f, BitSize) / AEON_TOKEN_CONCAT(6.0_f, BitSize)) * Pi;            \
	                                                                                                                                                 \
		/* Common ratio constants */                                                                                                                 \
	                                                                                                                                                 \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) SqrtOneHalf = Sqrt(AEON_TOKEN_CONCAT(1.0_f, BitSize) / AEON_TOKEN_CONCAT(2.0_f, BitSize));              \
	                                                                                                                                                 \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) SqrtOneThird = Sqrt(AEON_TOKEN_CONCAT(1.0_f, BitSize) / AEON_TOKEN_CONCAT(3.0_f, BitSize));             \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) SqrtTwoThirds = Sqrt(AEON_TOKEN_CONCAT(2.0_f, BitSize) / AEON_TOKEN_CONCAT(3.0_f, BitSize));            \
	                                                                                                                                                 \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) SqrtOneFourth = Sqrt(AEON_TOKEN_CONCAT(1.0_f, BitSize) / AEON_TOKEN_CONCAT(4.0_f, BitSize));            \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) SqrtThreeFourths = Sqrt(AEON_TOKEN_CONCAT(3.0_f, BitSize) / AEON_TOKEN_CONCAT(4.0_f, BitSize));         \
	                                                                                                                                                 \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) SqrtTwo = Sqrt(AEON_TOKEN_CONCAT(2.0_f, BitSize));                                        \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) SqrtThree = Sqrt(AEON_TOKEN_CONCAT(3.0_f, BitSize));                                      \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) SqrtSix = Sqrt(AEON_TOKEN_CONCAT(6.0_f, BitSize));                                        \
	                                                                                                                                                 \
		/* Trigonometric constants */                                                                                                                \
		/* TODO maybe better names */                                                                                                                \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) Sin_0 = AEON_TOKEN_CONCAT(0.0_f, BitSize);                                                \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) Cos_0 = AEON_TOKEN_CONCAT(1.0_f, BitSize);                                                \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) Tan_0 = AEON_TOKEN_CONCAT(0.0_f, BitSize);                                                \
	                                                                                                                                                 \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) Sin_15 = (SqrtSix - SqrtTwo) / AEON_TOKEN_CONCAT(4.0_f, BitSize);                         \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) Cos_15 = (SqrtSix + SqrtTwo) / AEON_TOKEN_CONCAT(4.0_f, BitSize);                         \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) Tan_15 = AEON_TOKEN_CONCAT(2.0_f, BitSize) - SqrtThree;                                   \
	                                                                                                                                                 \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) Sin_30 = AEON_TOKEN_CONCAT(0.5_f, BitSize);                                               \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) Cos_30 = SqrtThreeFourths;                                                                \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) Tan_30 = SqrtOneThird;                                                                    \
	                                                                                                                                                 \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) Sin_45 = SqrtOneHalf;                                                                     \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) Cos_45 = SqrtOneHalf;                                                                     \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) Tan_45 = AEON_TOKEN_CONCAT(1.0_f, BitSize);                                               \
	                                                                                                                                                 \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) Sin_60 = SqrtThreeFourths;                                                                \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) Cos_60 = AEON_TOKEN_CONCAT(0.5_f, BitSize);                                               \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) Tan_60 = SqrtThree;                                                                       \
	                                                                                                                                                 \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) Sin_75 = (SqrtSix + SqrtTwo) / AEON_TOKEN_CONCAT(4.0_f, BitSize);                         \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) Cos_75 = (SqrtSix - SqrtTwo) / AEON_TOKEN_CONCAT(4.0_f, BitSize);                         \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) Tan_75 = AEON_TOKEN_CONCAT(2.0_f, BitSize) + SqrtThree;                                   \
	                                                                                                                                                 \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) Sin_90 = AEON_TOKEN_CONCAT(1.0_f, BitSize);                                               \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) Cos_90 = AEON_TOKEN_CONCAT(0.0_f, BitSize);                                               \
		static constexpr AEON_TOKEN_CONCAT(float, BitSize) Tan_90 = TNumericProperties<AEON_TOKEN_CONCAT(float, BitSize)>::Infinity;                 \
	};                                                                                                                                               \
	using AEON_TOKEN_CONCAT(FP, BitSize, Constants) = Constants<AEON_TOKEN_CONCAT(float, BitSize)>;

	ZZINTERNAL_AEON_DECLARE_MATH_CONSTANTS_NAMESPACE(32);
	ZZINTERNAL_AEON_DECLARE_MATH_CONSTANTS_NAMESPACE(64);

#undef ZZINTERNAL_AEON_DECLARE_MATH_CONSTANTS_NAMESPACE

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  Radians-degrees conversion

	template <CFloatingPoint FloatType>
	AEON_FORCEINLINE constexpr FloatType RadToDeg(FloatType Value)
	{
		return FloatType(Value * 57.29577951308232); // 180 / Pi
	//	return FloatType(Value / 0.017453292519943295);
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  Degrees-radians conversion

	template <CFloatingPoint FloatType>
	AEON_FORCEINLINE constexpr FloatType DegToRad(FloatType Value)
	{
		return FloatType(Value * 0.017453292519943295); // Pi / 180
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  FastSin, FastCos

	namespace ZzInternal
	{
		enum struct EFastTrigFunc
		{
			Sin = 1,
			Cos
		};
	
		template <EFastTrigFunc TrigFuncValue, CFloatingPoint FloatType>
		constexpr FloatType FastSinCos(FloatType Value)
		{
			// The generator expression for the coefficients is 1/n!, where n is the index
			constexpr float64 Coefficients[20] =
				{
					1.0,					1.0,					0.5,					0.16666666666666666,	4.1666666666666664E-2,
					8.333333333333333E-3,	1.388888888888889E-3,	1.984126984126984E-4,	2.48015873015873E-5,	2.7557319223985893E-6,
					2.755731922398589E-7,	2.505210838544172E-8,	2.08767569878681E-9,	1.6059043836821613E-10,	1.1470745597729725E-11,
					7.647163731819816E-13,	4.779477332387385E-14,	2.8114572543455206E-15,	1.5619206968586225E-16,	8.22063524662433E-18,
				};
	
			auto ValueSq = Square<float64>(Value);

			// CHECKME benchmark this (also benchmark alt impl with split mul statements)
			//  also see: https://math.stackexchange.com/questions/2853310

#		if 1
			if constexpr (TrigFuncValue == EFastTrigFunc::Sin)
			{
				return FloatType(
					Coefficients[1] * Value
					- Coefficients[3] * Value * ValueSq
					+ Coefficients[5] * Value * ValueSq * ValueSq
					- Coefficients[7] * Value * ValueSq * ValueSq * ValueSq
					+ Coefficients[9] * Value * ValueSq * ValueSq * ValueSq * ValueSq
					- Coefficients[11] * Value * ValueSq * ValueSq * ValueSq * ValueSq * ValueSq
					+ Coefficients[13] * Value * ValueSq * ValueSq * ValueSq * ValueSq * ValueSq * ValueSq
					- Coefficients[15] * Value * ValueSq * ValueSq * ValueSq * ValueSq * ValueSq * ValueSq * ValueSq
					+ Coefficients[17] * Value * ValueSq * ValueSq * ValueSq * ValueSq * ValueSq * ValueSq * ValueSq * ValueSq
					- Coefficients[19] * Value * ValueSq * ValueSq * ValueSq * ValueSq * ValueSq * ValueSq * ValueSq * ValueSq * ValueSq
				);
			}
			else if constexpr (TrigFuncValue == EFastTrigFunc::Cos)
			{
				return FloatType(
					Coefficients[0]
					- Coefficients[2] * ValueSq
					+ Coefficients[4] * ValueSq * ValueSq
					- Coefficients[6] * ValueSq * ValueSq * ValueSq
					+ Coefficients[8] * ValueSq * ValueSq * ValueSq * ValueSq
					- Coefficients[10] * ValueSq * ValueSq * ValueSq * ValueSq * ValueSq
					+ Coefficients[12] * ValueSq * ValueSq * ValueSq * ValueSq * ValueSq * ValueSq
					- Coefficients[14] * ValueSq * ValueSq * ValueSq * ValueSq * ValueSq * ValueSq * ValueSq
					+ Coefficients[16] * ValueSq * ValueSq * ValueSq * ValueSq * ValueSq * ValueSq * ValueSq * ValueSq
					- Coefficients[18] * ValueSq * ValueSq * ValueSq * ValueSq * ValueSq * ValueSq * ValueSq * ValueSq * ValueSq
				);
			}
#		else
			if constexpr (FastTrigFuncValue == EFastTrigFunc::Sin)
			{
				float64 Variable = Value; // x^1
				float64 Result = Coefficients[1] * Variable;
				Variable *= ValueSq; // x^3
				Result -= Coefficients[3] * Variable;
				Variable *= ValueSq; // x^5
				Result += Coefficients[5] * Variable;
				Variable *= ValueSq; // x^7
				Result -= Coefficients[7] * Variable;
				Variable *= ValueSq; // x^9
				Result += Coefficients[9] * Variable;
				Variable *= ValueSq; // x^11
				Result -= Coefficients[11] * Variable;
				Variable *= ValueSq; // x^13
				Result += Coefficients[13] * Variable;
				Variable *= ValueSq; // x^15
				Result -= Coefficients[15] * Variable;
				Variable *= ValueSq; // x^17
				Result += Coefficients[17] * Variable;
				Variable *= ValueSq; // x^19
				Result -= Coefficients[19] * Variable;
				return FloatType(Result);
			}
			else if constexpr (FastTrigFuncValue == EFastTrigFunc::Cos)
			{
				float64 Variable = FloatType(1); // x^0
				float64 Result = Coefficients[0] * Variable;
				Variable *= ValueSq; // x^2
				Result -= Coefficients[2] * Variable;
				Variable *= ValueSq; // x^4
				Result += Coefficients[4] * Variable;
				Variable *= ValueSq; // x^6
				Result -= Coefficients[6] * Variable;
				Variable *= ValueSq; // x^8
				Result += Coefficients[8] * Variable;
				Variable *= ValueSq; // x^10
				Result -= Coefficients[10] * Variable;
				Variable *= ValueSq; // x^12
				Result += Coefficients[12] * Variable;
				Variable *= ValueSq; // x^14
				Result -= Coefficients[14] * Variable;
				Variable *= ValueSq; // x^16
				Result += Coefficients[16] * Variable;
				Variable *= ValueSq; // x^18
				Result -= Coefficients[18] * Variable;
				return FloatType(Result);
			}
#		endif
			else
			{
				static_assert(VAlwaysFalse<FloatType>,
				    		  "'FastSinCos<TrigFuncValue, FloatType>(Value)': Invalid TrigFuncValue.");
			}
		}
	}

	template <CFloatingPoint FloatType>
	AEON_FORCEINLINE constexpr FloatType FastSin(FloatType Value)
	{
		return ZzInternal::FastSinCos<ZzInternal::EFastTrigFunc::Sin>(Value);
	}

	template <CFloatingPoint FloatType>
	AEON_FORCEINLINE constexpr FloatType FastCos(FloatType Value)
	{
		return ZzInternal::FastSinCos<ZzInternal::EFastTrigFunc::Cos>(Value);
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  Sin, Cos, Tan

	template <CFloatingPoint FloatType>
	AEON_FORCEINLINE constexpr FloatType Sin(FloatType Value)
	{
		if_consteval
		{
			auto [ClampValue, Quadrant] = RemQuo(Value, Constants<FloatType>::HalfPi);
			Quadrant = ModPowerOfTwo(Quadrant, 4_u32);
		//	FloatType ClampValue = RemQuo(Value, Constants<FloatType>::Pi, Quadrant);
		//	Quadrant = ModPowerOfTwo(Quadrant, 2_u32);

			switch (Quadrant)
			{
				default:
					AEON_DEBUGGER_BREAK(); // Should never happen
				case 0:
					return +FastSin(ClampValue);
				case 1:
					return +FastCos(ClampValue);
				case 2:
					return -FastSin(ClampValue);
				case 3:
					return -FastCos(ClampValue);
			}
		}
		else
		{
			return std::sin(Value);
		}
	}

	template <CFloatingPoint FloatType>
	AEON_FORCEINLINE constexpr FloatType Cos(FloatType Value)
	{
		if_consteval
		{
			auto [ClampValue, Quadrant] = RemQuo(Value, Constants<FloatType>::HalfPi);
			Quadrant = ModPowerOfTwo(Quadrant, 4_u32);
		//	FloatType ClampValue = RemQuo(Value, Constants<FloatType>::Pi, Quadrant);
		//	Quadrant = ModPowerOfTwo(Quadrant, 2_u32);

			switch (Quadrant)
			{
				default:
					AEON_DEBUGGER_BREAK(); // Should never happen
				case 0:
					return +FastCos(ClampValue);
				case 1:
					return -FastSin(ClampValue);
				case 2:
					return -FastCos(ClampValue);
				case 3:
					return +FastSin(ClampValue);
			}
		}
		else
		{
			return std::cos(Value);
		}
	}

	template <CFloatingPoint FloatType>
	AEON_FORCEINLINE constexpr FloatType Tan(FloatType Value)
	{
		if_consteval
		{
			return FastSin(Value) / FastCos(Value);
		}
		else
		{
			return std::tan(Value);
		}
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  ArcSin, ArcCos, ArcTan

	template <CFloatingPoint FloatType>
	AEON_FORCEINLINE FloatType ArcSin(FloatType Value)
	{
		return std::asin(Value);
	}

	template <CFloatingPoint FloatType>
	AEON_FORCEINLINE FloatType ArcCos(FloatType Value)
	{
		return std::acos(Value);
	}

	template <CFloatingPoint FloatType>
	AEON_FORCEINLINE FloatType ArcTan(FloatType Value)
	{
		return std::atan(Value);
	}

	template <CFloatingPoint FloatType>
	AEON_FORCEINLINE FloatType ArcTan(FloatType Num, FloatType Denom)
	{
		return std::atan2(Num, Denom);
	}

	// TODO maybe add hyperbolic functions?

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  Gamma

	template <CInteger IntType>
	AEON_FORCEINLINE constexpr IntType Gamma(IntType Value)
	{
		if_consteval
		{
			auto Result = IntType(Value > IntType(0) ? 1 : 0);

			Value = Value > IntType(0) ? Value - IntType(1) : IntType(0);
			for (IntType Current = IntType(2); Current <= Value; ++Current)
			{
				Result *= Current;
			}
			return Result;
		}
		else
		{
			return IntType(std::tgamma(Value));
		}
	}

	template <CFloatingPoint FloatType>
	AEON_FORCEINLINE FloatType Gamma(FloatType Value)
	{
		return std::tgamma(Value);
	}
}

namespace AeonMath = Aeon::Math;

#endif
