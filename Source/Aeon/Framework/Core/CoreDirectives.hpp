#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CORE_COREDIRECTIVES
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CORE_COREDIRECTIVES

// Platform-detection macros
// For more information:
// 	https://web.archive.org/web/20191012035921/http://nadeausoftware.com/articles/2012/01/c_c_tip_how_use_compiler_predefined_macros_detect_operating_system
// 	https://stackoverflow.com/questions/142508/how-do-i-check-os-with-a-preprocessor-directive/8249232#8249232

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Platform detection

// TODO do we want to detect CYGWIN?
#if (defined(_WIN32) or defined(__CYGWIN__)) // Windows or Cygwin POSIX under Windows
#	define AEON_PLATFORM_WINDOWS 1

#	ifndef AEON_PLATFORM_NAME
#		define AEON_PLATFORM_NAME Windows
#	endif
#	define AEON_PLATFORM_POSIX 0
#else
#	define AEON_PLATFORM_WINDOWS 0
#endif

#if (defined(__linux__) and not defined(__ANDROID__)) // Debian, Ubuntu, Gentoo, Fedora, openSUSE, RedHat, CentOS and others
#	define AEON_PLATFORM_LINUX 1

#	ifndef AEON_PLATFORM_NAME
#		define AEON_PLATFORM_NAME Linux
#	endif
#	define AEON_PLATFORM_POSIX 1
#else
#	define AEON_PLATFORM_LINUX 0
#endif

#if (defined(__ANDROID__)) // Android
#	define AEON_PLATFORM_ANDROID 1

#	ifndef AEON_PLATFORM_NAME
#		define AEON_PLATFORM_NAME Android
#	endif
#	define AEON_PLATFORM_POSIX 1
#else
#	define AEON_PLATFORM_ANDROID 0
#endif

#if (defined(__APPLE__)) // Apple macOS and iOS
#	include <TargetConditionals.h>

#	define AEON_PLATFORM_MACOS (TARGET_OS_MAC)
#	define AEON_PLATFORM_IOS (TARGET_OS_IOS and not TARGET_OS_SIMULATOR) // TODO how to handle simulator?

#	define AEON_PLATFORM_POSIX 1

#	ifndef AEON_PLATFORM_NAME
#		if AEON_PLATFORM_MACOS
#			define AEON_PLATFORM_NAME macOS
#		elif AAEON_PLATFORM_IOS
#			define AEON_PLATFORM_NAME iOS
#		endif
#	endif
#else
#	define AEON_PLATFORM_MACOS 0
#	define AEON_PLATFORM_IOS 0
#endif

// NOTE Only macOS is fully POSIX-compliant, but for most intents and purposes, we group all non-Windows platforms as POSIX.

// TODO maybe add FreeBSD

// If not yet identified
#ifndef AEON_PLATFORM_NAME
// TODO maybe error now
#	define AEON_PLATFORM_NAME Unknown
#endif

// If this fails, platform detection is messed up, fix it
static_assert((AEON_PLATFORM_WINDOWS + AEON_PLATFORM_LINUX + AEON_PLATFORM_ANDROID + AEON_PLATFORM_MACOS + AEON_PLATFORM_IOS) == 1);

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Architecture detection

#if AEON_PLATFORM_WINDOWS
#	if defined(_M_IX86) or defined(_M_X64)
#		if defined(_M_IX86)
#			define AEON_ARCHITECTURE_X86 1
#			define AEON_ARCHITECTURE_X86_64 0
			static_assert(sizeof(void*) == 4);
#		else
#			define AEON_ARCHITECTURE_X86 1
#			define AEON_ARCHITECTURE_X86_64 1
			static_assert(sizeof(void*) == 8);
#		endif
#		define AEON_IS_LITTLE_ENDIAN_ARCH 1
#	else
#		define AEON_ARCHITECTURE_X86 0
#		define AEON_ARCHITECTURE_X86_64 0
#	endif

#	if defined(_M_ARM) or defined(_M_ARM64)
// There are Application (A), Real-Time (R), and Microcontroller (M) architecture profiles, we'll only bother with the first
		// ARMv7-A
#		if defined(_M_ARM)
#			define AEON_ARCHITECTURE_ARM 1
#			define AEON_ARCHITECTURE_ARM64 0
			static_assert(sizeof(void*) == 4);
		// ARMv8-A, ARMv9-A
#		else
#			define AEON_ARCHITECTURE_ARM 1
#			define AEON_ARCHITECTURE_ARM64 1
			static_assert(sizeof(void*) == 8);
#		endif
#		define AEON_IS_LITTLE_ENDIAN_ARCH 0 // FIXME dunno, need to research this
#	else
#		define AEON_ARCHITECTURE_ARM 0
#		define AEON_ARCHITECTURE_ARM64 0
#	endif

#if AEON_ARCHITECTURE_X86_64 or AEON_ARCHITECTURE_ARM64
#	define AEON_DATAMODEL_ILP32 0
#	define AEON_DATAMODEL_LP64 (AEON_PLATFORM_POSIX)
#	define AEON_DATAMODEL_LLP64 (!AEON_PLATFORM_POSIX)
#elif AEON_ARCHITECTURE_X86 or AEON_ARCHITECTURE_ARM
#	define AEON_DATAMODEL_ILP32 1
#	define AEON_DATAMODEL_LP64 0
#	define AEON_DATAMODEL_LLP64 0
#else
#	error Platform not supported!
#endif

// NOTE Code throughout the library depends on these data models for their respective platforms (e.g. SFile::SeekOffset_Internal)
#if AEON_DATAMODEL_ILP32
	static_assert(sizeof(int) == sizeof(long));
	static_assert(sizeof(long long) == sizeof(void*) + sizeof(void*));
	static_assert(sizeof(void*) == sizeof(long));
#endif
#if AEON_DATAMODEL_LP64
	static_assert(sizeof(int) == sizeof(long) + sizeof(long));
	static_assert(sizeof(long long) == sizeof(void*));
	static_assert(sizeof(void*) == sizeof(long));
#endif
#if AEON_DATAMODEL_LLP64
	static_assert(sizeof(int) == sizeof(long));
	static_assert(sizeof(long long) == sizeof(void*));
	static_assert(sizeof(void*) == sizeof(long) + sizeof(long));
#endif

// CHECKME what other ISAs (if any) should we check for?

#else
#	error Not implemented yet!
#endif

// If this fails, architecture detection is messed up, fix it
static_assert((AEON_ARCHITECTURE_X86 + AEON_ARCHITECTURE_ARM) == 1);

// FIXME eventually, list all supported (tested) pairs (arch-platform) here, and ensure that detected platform conforms to supported pair

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Compiler detection

// TODO architecture and compiler detection macros
// 	https://sourceforge.net/p/predef/wiki/Home/

#if defined(_MSC_VER) and not defined(__clang__)
// For Windows and Android
// https://learn.microsoft.com/en-us/cpp/preprocessor/predefined-macros?view=msvc-170#microsoft-specific-predefined-macros
#	define AEON_COMPILER_MSVC _MSC_VER

// TODO move these to DefaultProjectConfig.cmake file
//#	pragma warning(disable: 4068) // unknown pragma
#	pragma warning(disable: 4324) // 'class1': structure was padded due to alignment specifier
#	pragma warning(disable: 4584) // 'class1' : base-class 'class2' is already a base-class of 'class3'

#else
#	define AEON_COMPILER_MSVC 0
#endif

#if defined(__clang__)
// For Windows, MacOS, and iOS
// FIXME MacOS apparently uses a different version of clang, identified as AppleClang
#	define AEON_COMPILER_CLANG __clang_major__
#else
#	define AEON_COMPILER_CLANG 0
#endif

// TODO remaining compilers

// For Linux
#define AEON_COMPILER_GCC 0
// For Windows and Linux
#define AEON_COMPILER_ICC 0

// If this fails, compiler detection is messed up, fix it
static_assert((int(bool(AEON_COMPILER_MSVC)) + int(bool(AEON_COMPILER_CLANG)) + int(bool(AEON_COMPILER_GCC)) + int(bool(AEON_COMPILER_ICC))) == 1);

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SIMD capabilities detection

// TODO should this be defined in CMake? prob, since code needs to be explicitly compiled with support for each extension
//  see https://stackoverflow.com/questions/28939652/how-to-detect-sse-sse2-avx-avx2-avx-512-avx-128-fma-kcvi-availability-at-compile

#if AEON_ARCHITECTURE_X86

// NOTE Need to define all macros for previous extensions in each instr. set,
//  instead of detecting and defining the macro for the respective extension,
//  because MSVC is a bit dumb and doesn't define the SSE macros when AVX is enabled.

// TODO different values for different subsets of AVX512?
#	if __AVX512F__
#		define AEON_SUPPORTS_AVX512	70
#		define AEON_SUPPORTS_AVX2	60
#		define AEON_SUPPORTS_AVX	50
#		define AEON_SUPPORTS_SSE4_2	41
#		define AEON_SUPPORTS_SSE4_1	40
#		define AEON_SUPPORTS_SSSE3	31
#		define AEON_SUPPORTS_SSE3	30
#		define AEON_SUPPORTS_SSE2	20
#		define AEON_SUPPORTS_SSE	10
#	elif __AVX2__
#		define AEON_SUPPORTS_AVX2	60
#		define AEON_SUPPORTS_AVX	50
#		define AEON_SUPPORTS_SSE4_2	41
#		define AEON_SUPPORTS_SSE4_1	40
#		define AEON_SUPPORTS_SSSE3	31
#		define AEON_SUPPORTS_SSE3	30
#		define AEON_SUPPORTS_SSE2	20
#		define AEON_SUPPORTS_SSE	10
#	elif __AVX__
#		define AEON_SUPPORTS_AVX	50
#		define AEON_SUPPORTS_SSE4_2	41
#		define AEON_SUPPORTS_SSE4_1	40
#		define AEON_SUPPORTS_SSSE3	31
#		define AEON_SUPPORTS_SSE3	30
#		define AEON_SUPPORTS_SSE2	20
#		define AEON_SUPPORTS_SSE	10
#	elif __SSE4_2__
#		define AEON_SUPPORTS_SSE4_2	41
#		define AEON_SUPPORTS_SSE4_1	40
#		define AEON_SUPPORTS_SSSE3	31
#		define AEON_SUPPORTS_SSE3	30
#		define AEON_SUPPORTS_SSE2	20
#		define AEON_SUPPORTS_SSE	10
#	elif __SSE4_1__
#		define AEON_SUPPORTS_SSE4_1	40
#		define AEON_SUPPORTS_SSSE3	31
#		define AEON_SUPPORTS_SSE3	30
#		define AEON_SUPPORTS_SSE2	20
#		define AEON_SUPPORTS_SSE	10
#	elif __SSSE3__
#		define AEON_SUPPORTS_SSSE3	31
#		define AEON_SUPPORTS_SSE3	30
#		define AEON_SUPPORTS_SSE2	20
#		define AEON_SUPPORTS_SSE	10
#	elif __SSE3__
#		define AEON_SUPPORTS_SSE3	30
#		define AEON_SUPPORTS_SSE2	20
#		define AEON_SUPPORTS_SSE	10
#	elif __SSE2__
#		define AEON_SUPPORTS_SSE2	20
#		define AEON_SUPPORTS_SSE	10
#	elif __SSE__
#		define AEON_SUPPORTS_SSE	10
#	else
#		define AEON_SSE_UNSUPPORTED	00
#	endif
#else
#	error Not implemented yet!
#endif

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Language directives

//#define FORCEINLINE FORCEINLINE
//#define FORCENOINLINE FORCENOINLINE
//#define NODISCARD NODISCARD

#if AEON_BUILD_DEVELOPMENT
#	define AEON_FORCEINLINE
#else
#	if AEON_COMPILER_MSVC
#		define AEON_FORCEINLINE __forceinline
#	elif AEON_COMPILER_CLANG
#		define AEON_FORCEINLINE __attribute__((always_inline))
#	else
#		define AEON_FORCEINLINE
#		error Platform not supported
#	endif
#endif

#if AEON_COMPILER_MSVC
#	define AEON_FORCENOINLINE __declspec(noinline)
#elif AEON_COMPILER_CLANG
#	define AEON_FORCENOINLINE __attribute__((noinline))
#else
#	define AEON_FORCENOINLINE
#	error Platform not supported
#endif

#define AEON_NODISCARD [[nodiscard]]
//#if AEON_COMPILER_MSVC
//#	define AEON_NODISCARD _Check_return_
//#elif AEON_COMPILER_CLANG or AEON_COMPILER_GCC
//#	define AEON_NODISCARD __attribute__((warn_unused_result))
//#else
//// TODO rest of them
//#	error Platform not supported
//#endif

#define AEON_NORETURN [[noreturn]]

// TODO maybe deprecate this (some specifiers need to appear in a certain order)
#define AEON_SPECLIST_APPLY(Spec, ...) \
	ZZINTERNAL_AEON_TOKEN_CONCAT(ZZINTERNAL_AEON_SPECLIST_APPLY_, AEON_LIST_SIZE(__VA_ARGS__))(Spec, __VA_ARGS__)
#define ZZINTERNAL_AEON_SPECLIST_APPLY_0( Spec, ...)
#define ZZINTERNAL_AEON_SPECLIST_APPLY_1( Spec, ...) Spec(AEON_LIST_HEAD(__VA_ARGS__))
#define ZZINTERNAL_AEON_SPECLIST_APPLY_2( Spec, ...) Spec(AEON_LIST_HEAD(__VA_ARGS__)) ZZINTERNAL_AEON_SPECLIST_APPLY_1(Spec, AEON_LIST_TAIL(__VA_ARGS__))
#define ZZINTERNAL_AEON_SPECLIST_APPLY_3( Spec, ...) Spec(AEON_LIST_HEAD(__VA_ARGS__)) ZZINTERNAL_AEON_SPECLIST_APPLY_2(Spec, AEON_LIST_TAIL(__VA_ARGS__))
#define ZZINTERNAL_AEON_SPECLIST_APPLY_4( Spec, ...) Spec(AEON_LIST_HEAD(__VA_ARGS__)) ZZINTERNAL_AEON_SPECLIST_APPLY_3(Spec, AEON_LIST_TAIL(__VA_ARGS__))
#define ZZINTERNAL_AEON_SPECLIST_APPLY_5( Spec, ...) Spec(AEON_LIST_HEAD(__VA_ARGS__)) ZZINTERNAL_AEON_SPECLIST_APPLY_4(Spec, AEON_LIST_TAIL(__VA_ARGS__))
#define ZZINTERNAL_AEON_SPECLIST_APPLY_6( Spec, ...) Spec(AEON_LIST_HEAD(__VA_ARGS__)) ZZINTERNAL_AEON_SPECLIST_APPLY_5(Spec, AEON_LIST_TAIL(__VA_ARGS__))
#define ZZINTERNAL_AEON_SPECLIST_APPLY_7( Spec, ...) Spec(AEON_LIST_HEAD(__VA_ARGS__)) ZZINTERNAL_AEON_SPECLIST_APPLY_6(Spec, AEON_LIST_TAIL(__VA_ARGS__))
#define ZZINTERNAL_AEON_SPECLIST_APPLY_8( Spec, ...) Spec(AEON_LIST_HEAD(__VA_ARGS__)) ZZINTERNAL_AEON_SPECLIST_APPLY_7(Spec, AEON_LIST_TAIL(__VA_ARGS__))
#define ZZINTERNAL_AEON_SPECLIST_APPLY_9( Spec, ...) Spec(AEON_LIST_HEAD(__VA_ARGS__)) ZZINTERNAL_AEON_SPECLIST_APPLY_8(Spec, AEON_LIST_TAIL(__VA_ARGS__))
#define ZZINTERNAL_AEON_SPECLIST_APPLY_10(Spec, ...) Spec(AEON_LIST_HEAD(__VA_ARGS__)) ZZINTERNAL_AEON_SPECLIST_APPLY_9(Spec, AEON_LIST_TAIL(__VA_ARGS__))

#define AEON_SPEC_APPLY(Spec) ZZINTERNAL_AEON_TOKEN_CONCAT(AEON_, Spec)
#define AEON_SPEC(...) AEON_SPECLIST_APPLY(AEON_SPEC_APPLY, __VA_ARGS__)

#endif