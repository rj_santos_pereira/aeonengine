#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CORE_COREATOMIC
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CORE_COREATOMIC

#include "CoreMemory.hpp"

// TODO doc this
#define alignas_atomic(...) AEON_TOKEN_CONCAT(ZZINTERNAL_AEON_ALIGNAS_ATOMIC_OVERLOAD_, AEON_LIST_SIZE(__VA_ARGS__))(__VA_ARGS__)

#define ZZINTERNAL_AEON_ALIGNAS_ATOMIC_OVERLOAD_0() alignas(::AeonMemory::PlatformCacheLineSize)
#define ZZINTERNAL_AEON_ALIGNAS_ATOMIC_OVERLOAD_1(Expression) alignas(::AeonMath::Max(Expression, ::AeonMemory::PlatformCacheLineSize))

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// EMemoryOrder

	// FIXME make this a regular enum (or struct enum)

	//	Enum that represents possible memory orderings to use in atomic operations.
	//	Each bit, when set, indicates whether certain barriers are emitted by operations with the specified memory order,
	//	i.e., for some memory order 'MemOrder = 0b0ABC', for each bit:
	//	 A - Emits #StoreLoad barrier;
	//	 B - Emits #StoreStore and #LoadStore barriers;
	//	 C - Emits #LoadLoad and #LoadStore barriers.
	// NOTE Don't change any bit carelessly, as GAtomicScalar::MemoryOrderConversionMap depends on these values
	AEON_DECLARE_FLAG_ENUM(EMemoryOrder, uint8)
	(
		AEON_FLAG_ENUM_RULE_AS_IS,
		Relaxed	= 0b0000,
		Acquire	= 0b0001,
		Release	= 0b0010,
		AcqRel	= 0b0011,
		SeqCst	= 0b0111,
	);

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIsThreadId

	template <class Type>
	concept CThreadId = std::is_same_v<Type, thread_id>;
	template <class Type>
	concept CNotThreadId = !CThreadId<Type>;

	template <class Type>
	using TIsThreadId = TValueTrait<CThreadId<Type>>;
	template <class Type>
	inline constexpr bool VIsThreadId = CThreadId<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CLinearizableObject and related concepts

	template <class Type>
	// Load, Store, Xchg, CmpXchg
	concept CLinearizableObject = CBool<Type> || CEnum<Type> || CNumeric<Type> || CPointer<Type> || CThreadId<Type>;

	template <class Type>
	// Inc, Dec, Add, Sub
	concept CLinearizableIndexableObject = CNumeric<Type> || CObjectPointer<Type>;

	template <class Type>
	// Mul, Div, Neg
	concept CLinearizableComputableObject = CNumeric<Type>;

	template <class Type>
	// Mod, Shl, Shr
	concept CLinearizableIntegralComputableObject = CInteger<Type>;

	template <class Type>
	// Not, And, Or, Xor
	concept CLinearizableBitwiseComputableObject = CBool<Type> || CEnum<Type> || CInteger<Type>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TLinearizableObjectUnderlyingStorage

	template <class ObjectType, class = void>
	struct ZzInternal_TLinearizableObjectUnderlyingStorage;

	template <class ObjectType>
	struct ZzInternal_TLinearizableObjectUnderlyingStorage<ObjectType, TEnableIf<VIsBool<ObjectType>>>
	{
		using Type = uint8;
	};
	template <class ObjectType>
	struct ZzInternal_TLinearizableObjectUnderlyingStorage<ObjectType, TEnableIf<VIsEnum<ObjectType>>>
	{
		using Type = TEnumUnderlyingType<ObjectType>;
	};
	template <class ObjectType>
	struct ZzInternal_TLinearizableObjectUnderlyingStorage<ObjectType, TEnableIf<VIsInteger<ObjectType>>>
	{
		using Type = ObjectType;
	};
	template <class ObjectType>
	struct ZzInternal_TLinearizableObjectUnderlyingStorage<ObjectType, TEnableIf<VIsFloatingPoint<ObjectType>>>
	{
		using Type = ObjectType; //TMakeSignMagnitudeInteger<false, sizeof(ObjectType)>;
	};
	template <class ObjectType>
	struct ZzInternal_TLinearizableObjectUnderlyingStorage<ObjectType, TEnableIf<VIsPointer<ObjectType>>>
	{
		using Type = ptrint;
	};
	template <class ObjectType>
	struct ZzInternal_TLinearizableObjectUnderlyingStorage<ObjectType, TEnableIf<VIsThreadId<ObjectType>>>
	{
		using Type = TMakeSignMagnitudeInteger<false, sizeof(thread_id)>;
	};

	template <class ObjectType>
	using TLinearizableObjectUnderlyingStorage = typename ZzInternal_TLinearizableObjectUnderlyingStorage<ObjectType>::Type;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  CMemoryOrderAsStrictAsRequired

//	inline consteval bool ZzInternal_IsMemoryOrderAsStrictAsRequired(EMemoryOrder MemoryOrder, EMemoryOrder RequiredMemoryOrder)
//	{
//		return ((MemoryOrder + EMemoryOrder::AcqRel) > RequiredMemoryOrder);
//	}

	template <EMemoryOrder MemoryOrderValue, EMemoryOrder RequiredMemoryOrderValue>
	concept CMemoryOrderAsStrictAsRequired = ((MemoryOrderValue + EMemoryOrder::AcqRel) > RequiredMemoryOrderValue);

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  CLoadOnlyMemoryOrder, CStoreOnlyMemoryOrder

	template <EMemoryOrder MemoryOrderValue>
	concept CLoadOnlyMemoryOrder = (MemoryOrderValue == EMemoryOrder::SeqCst || MemoryOrderValue == EMemoryOrder::Acquire
									|| MemoryOrderValue == EMemoryOrder::Relaxed);

	template <EMemoryOrder MemoryOrderValue>
	concept CStoreOnlyMemoryOrder = (MemoryOrderValue == EMemoryOrder::SeqCst || MemoryOrderValue == EMemoryOrder::Release
									 || MemoryOrderValue == EMemoryOrder::Relaxed);

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  CompilerFence, MemoryFence, LoadFence, StoreFence

	// CHECKME check implementation of fences for other archs (in different platforms)

	AEON_FORCEINLINE inline void CompilerFence() noexcept
	{
		std::atomic_signal_fence(std::memory_order::seq_cst);
	}

	AEON_FORCEINLINE inline void MemoryFence() noexcept
	{
		// Each ordering corresponds to the following instruction(s), per architecture:
		//  Order\Arch	| x86		| ARM		|
		//	SeqCst		| mfence	| dmb(ish)	|
		//	AcqRel		| compiler	| dmb(ish)	|
		//	Relaxed		| no-op		| no-op		|
		// As such, we are mainly concerned with simulating SeqCst, as Relaxed is redundant,
		// and AcqRel can be adequately replicated with either this or CompilerFence.
		// NOTE Actually, on x86, instead of 'mfence', a 'lock op' (e.g. lock xchg on Windows) is used,
		//  see: https://stackoverflow.com/a/50279772
		// CHECKME should we have a separate fence for AcqRel semantics?
		//  prob no, as a simple rmw (or load-store pair) is enough for most cases...
		std::atomic_thread_fence(std::memory_order::seq_cst);
	}

	// NOTE: See https://stackoverflow.com/a/50780314 for the purposes of Load and Store fences on x86;
	//  This might be potentially useful for copying data between renderer and GPU,
	//  see https://stackoverflow.com/questions/43343231 and https://stackoverflow.com/questions/19035677
	//  Also see and consider https://learn.microsoft.com/en-us/cpp/intrinsics/faststorefence?view=msvc-170
	//  for Windows platforms

	AEON_FORCEINLINE inline void LoadFence() noexcept
	{
#	if AEON_ARCHITECTURE_X86
		_mm_lfence(); // Requires SSE2
#	else
#		error Not implemented yet!
#	endif
	}

	AEON_FORCEINLINE inline void StoreFence() noexcept
	{
#	if AEON_ARCHITECTURE_X86
		_mm_sfence(); // Requires SSE
#	else
#		error Not implemented yet!
#	endif
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GAtomicScalar

	template <CLinearizableObject ObjectType,
			  EMemoryOrder DefaultMemoryOrderValue = EMemoryOrder::SeqCst,
	    	  EMemoryOrder RequiredMemoryOrderValue = EMemoryOrder::Relaxed>
	requires CDecayed<ObjectType>
	class GAtomicScalar
	{
		static_assert(DefaultMemoryOrderValue == EMemoryOrder::SeqCst
					  || DefaultMemoryOrderValue == EMemoryOrder::AcqRel
					  || DefaultMemoryOrderValue == EMemoryOrder::Relaxed,
					  "'GAtomicScalar<ObjectType, DefaultMemoryOrderValue, RequiredMemoryOrderValue>': "
					  "DefaultMemoryOrderValue must be either SeqCst, AcqRel, or Relaxed.");
		static_assert(RequiredMemoryOrderValue == EMemoryOrder::SeqCst
					  || RequiredMemoryOrderValue == EMemoryOrder::AcqRel
					  || RequiredMemoryOrderValue == EMemoryOrder::Relaxed,
					  "'GAtomicScalar<ObjectType, DefaultMemoryOrderValue, RequiredMemoryOrderValue>': "
					  "DefaultMemoryOrderValue must be either SeqCst, AcqRel, or Relaxed.");
		static_assert(CMemoryOrderAsStrictAsRequired<DefaultMemoryOrderValue, RequiredMemoryOrderValue>,
					  "'GAtomicScalar<ObjectType, DefaultMemoryOrderValue, RequiredMemoryOrderValue>': "
					  "DefaultMemoryOrderValue must be equally as or stricter than RequiredMemoryOrderValue.");

	protected:
		static constexpr EMemoryOrder DefaultLoadMemoryOrderValue = DefaultMemoryOrderValue == EMemoryOrder::AcqRel
																  ? EMemoryOrder::Acquire
																  : DefaultMemoryOrderValue;
		static constexpr EMemoryOrder DefaultStoreMemoryOrderValue = DefaultMemoryOrderValue == EMemoryOrder::AcqRel
																   ? EMemoryOrder::Release
																   : DefaultMemoryOrderValue;
		
	public:
		using StorageType = TLinearizableObjectUnderlyingStorage<ObjectType>;

		GAtomicScalar(ObjectType InitialValue = ObjectType{}) noexcept
			: Storage{ CastToStorage_Internal(InitialValue) }
		{
		}

		GAtomicScalar(GAtomicScalar const&) = delete;
		GAtomicScalar(GAtomicScalar&&) = delete;

		GAtomicScalar& operator=(ObjectType Value) noexcept
		{
			Store(Value);
			return *this;
		}

		explicit operator ObjectType() const noexcept
		{
			return Load();
		}

		template <EMemoryOrder MemoryOrderValue = DefaultLoadMemoryOrderValue>
		AEON_NODISCARD ObjectType Load() const noexcept
		requires CLoadOnlyMemoryOrder<MemoryOrderValue> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			return CastToObject_Internal(Storage.load(MemoryOrderConversionMap[MemoryOrderValue]));
		}

		template <EMemoryOrder MemoryOrderValue = DefaultLoadMemoryOrderValue>
		ObjectType WaitLoad(ObjectType OldValue) const noexcept
		requires CLoadOnlyMemoryOrder<MemoryOrderValue> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			static_assert(std::has_unique_object_representations_v<StorageType>);
#		if AEON_PLATFORM_WINDOWS
			StorageType ExpectedValueStorage = CastToStorage_Internal(OldValue);
			StorageType CurrentValueStorage;

			while (true)
			{
				CurrentValueStorage = Storage.load(MemoryOrderConversionMap[MemoryOrderValue]);

				if (ExpectedValueStorage != CurrentValueStorage)
				{
					break;
				}

				// Luckily, Windows allows access to the underlying storage of std::atomic
				__std_atomic_wait_direct(addressof(Storage._Storage), addressof(ExpectedValueStorage),
										 sizeof(StorageType), _Atomic_wait_no_timeout);
			}

			return CastToObject_Internal(CurrentValueStorage);
#		elif not AEON_PLATFORM_WINDOWS // TODO implement other platforms
#			error Not implemented yet!
#		else
			// If we can't make it work with a custom impl on some platform, use the default one
			StorageType OldValueStorage = CastToStorage_Internal(OldValue);
			ObjectType NewValue;
			// NOTE We need the loop since atomic::wait does a load with the proper MemoryOrder but does not return its value,
			//  so we could otherwise return the OldValue (due to the ABA problem);
			//  Though atomic::wait (on Windows at least) also suffers from this,
			//  it hides it by never unblocking/returning in such a situation...
			do
			{
				Storage.wait(OldValueStorage, MemoryOrderConversionMap[MemoryOrderValue]);
				// NOTE Load must have stronger ordering than Relaxed:
				//  while read-read coherence guarantees we read a value not older than the last read one (from atomic::wait),
				//  we could still have problems due to synchronizes-with (or lack thereof), e.g.:
				//  - Thread 1: non-atomic op on obj 1; atomic::store(1); atomic::notify; non-atomic op on obj 2; atomic::store(2); atomic::notify;
				//  - Thread 2: wait(0) sees store(1) [thread wakes up]; load(relaxed) sees store(2); [thread assumes synchronizes-with store(2)]
				//  This would cause a possible data race on accessing obj 2
				NewValue = Load<MemoryOrderValue>();
			}
			while (NewValue == OldValue);
			return NewValue;
#		endif
		}

		template <EMemoryOrder MemoryOrderValue = DefaultStoreMemoryOrderValue>
		void Store(ObjectType NewValue) noexcept
		requires CStoreOnlyMemoryOrder<MemoryOrderValue> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			Storage.store(CastToStorage_Internal(NewValue), MemoryOrderConversionMap[MemoryOrderValue]);
		}

		template <EMemoryOrder MemoryOrderValue = DefaultStoreMemoryOrderValue>
		void StoreNotify(ObjectType NewValue) noexcept
		requires CStoreOnlyMemoryOrder<MemoryOrderValue> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			Store<MemoryOrderValue>(NewValue);
			Notify();
		}

		template <EMemoryOrder MemoryOrderValue = DefaultStoreMemoryOrderValue>
		void StoreNotifyAll(ObjectType NewValue) noexcept
		requires CStoreOnlyMemoryOrder<MemoryOrderValue> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			Store<MemoryOrderValue>(NewValue);
			NotifyAll();
		}

		template <EMemoryOrder MemoryOrderValue = DefaultMemoryOrderValue>
		ObjectType Exchange(ObjectType NewValue) noexcept
		requires CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			return CastToObject_Internal(Storage.exchange(CastToStorage_Internal(NewValue), MemoryOrderConversionMap[MemoryOrderValue]));
		}

		template <EMemoryOrder MemoryOrderValue = DefaultMemoryOrderValue>
		bool CompareExchange(ObjectType& OldValue, ObjectType NewValue) noexcept
		requires CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			StorageType OldValueStorage = CastToStorage_Internal(OldValue);
			StorageType NewValueStorage = CastToStorage_Internal(NewValue);
			bool Success = Storage.compare_exchange_strong(OldValueStorage, NewValueStorage, MemoryOrderConversionMap[MemoryOrderValue]);
			OldValue = CastToObject_Internal(OldValueStorage);
			return Success;
		}

		template <EMemoryOrder MemoryOrderValue = DefaultMemoryOrderValue>
		bool CompareExchangeIncrement(ObjectType& ExpectedValue) noexcept
		requires CLinearizableIndexableObject<ObjectType> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			StorageType OldValueStorage = CastToStorage_Internal(ExpectedValue);
			StorageType NewValueStorage = CastToStorage_Internal(ExpectedValue + 1);
			bool Success = Storage.compare_exchange_strong(OldValueStorage, NewValueStorage, MemoryOrderConversionMap[MemoryOrderValue]);
			ExpectedValue = CastToObject_Internal(OldValueStorage);
			return Success;
		}

		template <EMemoryOrder MemoryOrderValue = DefaultMemoryOrderValue>
		bool CompareExchangeDecrement(ObjectType& ExpectedValue) noexcept
		requires CLinearizableIndexableObject<ObjectType> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			StorageType OldValueStorage = CastToStorage_Internal(ExpectedValue);
			StorageType NewValueStorage = CastToStorage_Internal(ExpectedValue - 1);
			bool Success = Storage.compare_exchange_strong(OldValueStorage, NewValueStorage, MemoryOrderConversionMap[MemoryOrderValue]);
			ExpectedValue = CastToObject_Internal(OldValueStorage);
			return Success;
		}

		template <EMemoryOrder MemoryOrderValue = DefaultMemoryOrderValue>
		bool CompareExchangeAdd(ObjectType& ExpectedValue, StorageType Value) noexcept
		requires CLinearizableIndexableObject<ObjectType> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			StorageType OldValueStorage = CastToStorage_Internal(ExpectedValue);
			StorageType NewValueStorage = CastToStorage_Internal(ExpectedValue) + Value;
			bool Success = Storage.compare_exchange_strong(OldValueStorage, NewValueStorage, MemoryOrderConversionMap[MemoryOrderValue]);
			ExpectedValue = CastToObject_Internal(OldValueStorage);
			return Success;
		}

		template <EMemoryOrder MemoryOrderValue = DefaultMemoryOrderValue>
		bool CompareExchangeSubtract(ObjectType& ExpectedValue, StorageType Value) noexcept
		requires CLinearizableIndexableObject<ObjectType> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			StorageType OldValueStorage = CastToStorage_Internal(ExpectedValue);
			StorageType NewValueStorage = CastToStorage_Internal(ExpectedValue) - Value;
			bool Success = Storage.compare_exchange_strong(OldValueStorage, NewValueStorage, MemoryOrderConversionMap[MemoryOrderValue]);
			ExpectedValue = CastToObject_Internal(OldValueStorage);
			return Success;
		}

		template <EMemoryOrder MemoryOrderValue = DefaultMemoryOrderValue>
		ObjectType FetchIncrement() noexcept
		requires CLinearizableIndexableObject<ObjectType> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			return CastToObject_Internal(Storage.fetch_add(ValueEpsilon(), MemoryOrderConversionMap[MemoryOrderValue]));
		}

		template <EMemoryOrder MemoryOrderValue = DefaultMemoryOrderValue>
		ObjectType Increment() noexcept
		requires CLinearizableIndexableObject<ObjectType> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			return FetchIncrement<MemoryOrderValue>() + 1;
		}

		template <EMemoryOrder MemoryOrderValue = DefaultMemoryOrderValue>
		ObjectType FetchDecrement() noexcept
		requires CLinearizableIndexableObject<ObjectType> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			return CastToObject_Internal(Storage.fetch_sub(ValueEpsilon(), MemoryOrderConversionMap[MemoryOrderValue]));
		}

		template <EMemoryOrder MemoryOrderValue = DefaultMemoryOrderValue>
		ObjectType Decrement() noexcept
		requires CLinearizableIndexableObject<ObjectType> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			return FetchDecrement<MemoryOrderValue>() - 1;
		}

		template <EMemoryOrder MemoryOrderValue = DefaultMemoryOrderValue>
		ObjectType FetchNegate() noexcept
		requires CLinearizableComputableObject<ObjectType> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			return CastToObject_Internal(ExecuteUsingCASLoop_Internal<MemoryOrderValue>([](ObjectType OldValue)
																						{ return -OldValue; }));
		}

		template <EMemoryOrder MemoryOrderValue = DefaultMemoryOrderValue>
		ObjectType Negate() noexcept
		requires CLinearizableComputableObject<ObjectType> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			return -FetchNegate<MemoryOrderValue>();
		}

		template <EMemoryOrder MemoryOrderValue = DefaultMemoryOrderValue>
		ObjectType FetchAdd(StorageType Value) noexcept
		requires CLinearizableIndexableObject<ObjectType> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			return CastToObject_Internal(Storage.fetch_add(Value * ValueEpsilon(), MemoryOrderConversionMap[MemoryOrderValue]));
		}

		template <EMemoryOrder MemoryOrderValue = DefaultMemoryOrderValue>
		ObjectType Add(StorageType Value) noexcept
		requires CLinearizableIndexableObject<ObjectType> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			return FetchAdd<MemoryOrderValue>(Value) + Value;
		}

		template <EMemoryOrder MemoryOrderValue = DefaultMemoryOrderValue>
		ObjectType FetchSubtract(StorageType Value) noexcept
		requires CLinearizableIndexableObject<ObjectType> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			return CastToObject_Internal(Storage.fetch_sub(Value * ValueEpsilon(), MemoryOrderConversionMap[MemoryOrderValue]));
		}

		template <EMemoryOrder MemoryOrderValue = DefaultMemoryOrderValue>
		ObjectType Subtract(StorageType Value) noexcept
		requires CLinearizableIndexableObject<ObjectType> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			return FetchSubtract<MemoryOrderValue>(Value) - Value;
		}

		template <EMemoryOrder MemoryOrderValue = DefaultMemoryOrderValue>
		ObjectType FetchMultiply(ObjectType Value) noexcept
		requires CLinearizableComputableObject<ObjectType> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			return CastToObject_Internal(ExecuteUsingCASLoop_Internal<MemoryOrderValue>([Value](ObjectType OldValue)
																						{ return OldValue * Value; }));
		}

		template <EMemoryOrder MemoryOrderValue = DefaultMemoryOrderValue>
		ObjectType Multiply(ObjectType Value) noexcept
		requires CLinearizableComputableObject<ObjectType> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			return FetchMultiply<MemoryOrderValue>(Value) * Value;
		}

		template <EMemoryOrder MemoryOrderValue = DefaultMemoryOrderValue>
		ObjectType FetchDivide(ObjectType Value) noexcept
		requires CLinearizableComputableObject<ObjectType> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			return CastToObject_Internal(ExecuteUsingCASLoop_Internal<MemoryOrderValue>([Value](ObjectType OldValue)
																						{ return OldValue / Value; }));
		}

		template <EMemoryOrder MemoryOrderValue = DefaultMemoryOrderValue>
		ObjectType Divide(ObjectType Value) noexcept
		requires CLinearizableComputableObject<ObjectType> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			return FetchDivide<MemoryOrderValue>(Value) / Value;
		}

		template <EMemoryOrder MemoryOrderValue = DefaultMemoryOrderValue>
		ObjectType FetchRemainder(ObjectType Value) noexcept
		requires CLinearizableIntegralComputableObject<ObjectType> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			return CastToObject_Internal(ExecuteUsingCASLoop_Internal<MemoryOrderValue>([Value](ObjectType OldValue)
																						{ return OldValue % Value; }));
		}

		template <EMemoryOrder MemoryOrderValue = DefaultMemoryOrderValue>
		ObjectType Remainder(ObjectType Value) noexcept
		requires CLinearizableIntegralComputableObject<ObjectType> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			return FetchRemainder<MemoryOrderValue>(Value) % Value;
		}

		template <EMemoryOrder MemoryOrderValue = DefaultMemoryOrderValue>
		ObjectType FetchLeftShift(ObjectType Value) noexcept
		requires CLinearizableIntegralComputableObject<ObjectType> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			return CastToObject_Internal(ExecuteUsingCASLoop_Internal<MemoryOrderValue>([Value](ObjectType OldValue)
																						{ return OldValue << Value; }));
		}

		template <EMemoryOrder MemoryOrderValue = DefaultMemoryOrderValue>
		ObjectType LeftShift(ObjectType Value) noexcept
		requires CLinearizableIntegralComputableObject<ObjectType> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			return FetchLeftShift<MemoryOrderValue>(Value) << Value;
		}

		template <EMemoryOrder MemoryOrderValue = DefaultMemoryOrderValue>
		ObjectType FetchRightShift(ObjectType Value) noexcept
		requires CLinearizableIntegralComputableObject<ObjectType> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			return CastToObject_Internal(ExecuteUsingCASLoop_Internal<MemoryOrderValue>([Value](ObjectType OldValue)
																						{ return OldValue >> Value; }));
		}

		template <EMemoryOrder MemoryOrderValue = DefaultMemoryOrderValue>
		ObjectType RightShift(ObjectType Value) noexcept
		requires CLinearizableIntegralComputableObject<ObjectType> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			return FetchRightShift<MemoryOrderValue>(Value) >> Value;
		}

		template <EMemoryOrder MemoryOrderValue = DefaultMemoryOrderValue>
		ObjectType FetchNot() noexcept
		requires CLinearizableBitwiseComputableObject<ObjectType> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			constexpr StorageType ValueMask = AeonMath::MakeBitMask<StorageType, Aeon::BitSizeOf(StorageType)>();
			return CastToObject_Internal(Storage.fetch_xor(ValueMask, MemoryOrderConversionMap[MemoryOrderValue]));
		}

		template <EMemoryOrder MemoryOrderValue = DefaultMemoryOrderValue>
		ObjectType Not() noexcept
		requires CLinearizableBitwiseComputableObject<ObjectType> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			constexpr StorageType ValueMask = AeonMath::MakeBitMask<StorageType, Aeon::BitSizeOf(StorageType)>();
			return CastToObject_Internal(Storage.fetch_xor(ValueMask, MemoryOrderConversionMap[MemoryOrderValue]) ^ ValueMask);
		}

		template <EMemoryOrder MemoryOrderValue = DefaultMemoryOrderValue>
		ObjectType FetchAnd(ObjectType Value) noexcept
		requires CLinearizableBitwiseComputableObject<ObjectType> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			StorageType ValueMask = CastToStorage_Internal(Value);
			return CastToObject_Internal(Storage.fetch_and(ValueMask, MemoryOrderConversionMap[MemoryOrderValue]));
		}

		template <EMemoryOrder MemoryOrderValue = DefaultMemoryOrderValue>
		ObjectType And(ObjectType Value) noexcept
		requires CLinearizableBitwiseComputableObject<ObjectType> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			StorageType ValueMask = CastToStorage_Internal(Value);
			return CastToObject_Internal(Storage.fetch_and(ValueMask, MemoryOrderConversionMap[MemoryOrderValue]) & ValueMask);
		}

		template <EMemoryOrder MemoryOrderValue = DefaultMemoryOrderValue>
		ObjectType FetchOr(ObjectType Value) noexcept
		requires CLinearizableBitwiseComputableObject<ObjectType> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			StorageType ValueMask = CastToStorage_Internal(Value);
			return CastToObject_Internal(Storage.fetch_or(ValueMask, MemoryOrderConversionMap[MemoryOrderValue]));
		}

		template <EMemoryOrder MemoryOrderValue = DefaultMemoryOrderValue>
		ObjectType Or(ObjectType Value) noexcept
		requires CLinearizableBitwiseComputableObject<ObjectType> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			StorageType ValueMask = CastToStorage_Internal(Value);
			return CastToObject_Internal(Storage.fetch_or(ValueMask, MemoryOrderConversionMap[MemoryOrderValue]) | ValueMask);
		}

		template <EMemoryOrder MemoryOrderValue = DefaultMemoryOrderValue>
		ObjectType FetchXor(ObjectType Value) noexcept
		requires CLinearizableBitwiseComputableObject<ObjectType> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			StorageType ValueMask = CastToStorage_Internal(Value);
			return CastToObject_Internal(Storage.fetch_xor(ValueMask, MemoryOrderConversionMap[MemoryOrderValue]));
		}

		template <EMemoryOrder MemoryOrderValue = DefaultMemoryOrderValue>
		ObjectType Xor(ObjectType Value) noexcept
		requires CLinearizableBitwiseComputableObject<ObjectType> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			StorageType ValueMask = CastToStorage_Internal(Value);
			return CastToObject_Internal(Storage.fetch_xor(ValueMask, MemoryOrderConversionMap[MemoryOrderValue]) ^ ValueMask);
		}

		void Notify() noexcept
		{
			// NOTE Some dicey info about this,
			//  the standard doesn't seem to currently guarantee synchronization between the notifying/waiting threads:
			//	https://stackoverflow.com/questions/70228390

			// CHECKME do we need to emit a compiler fence (actually a AcqRel fence) here?
			CompilerFence();
			Storage.notify_one();
		}

		void NotifyAll() noexcept
		{
			CompilerFence();
			Storage.notify_all();
		}

		friend void Swap(GAtomicScalar&, GAtomicScalar&) = delete;

	protected:
		template <EMemoryOrder MemoryOrderValue, class OperationType>
		StorageType ExecuteUsingCASLoop_Internal(OperationType&& Operation)
		{
			// CHECKME On x86, cmp_xchg_weak always calls cmp_xchg_strong,
			//  though the loop must always exist: ensure correct codegen;
			//  On ARM, might be different, read: https://github.com/microsoft/STL/issues/775
			StorageType OldValue = Storage.load(std::memory_order_relaxed);
			StorageType NewValue;
			do
			{
				NewValue = CastToStorage_Internal(Operation(CastToObject_Internal(OldValue)));
			}
			while (!Storage.compare_exchange_weak(OldValue, NewValue, MemoryOrderConversionMap[MemoryOrderValue]));
			return OldValue;
		}

		AEON_FORCEINLINE static ObjectType CastToObject_Internal(StorageType Value) noexcept
		{
			if constexpr (VIsBool<ObjectType> || VIsEnum<ObjectType>)
			{
				return static_cast<ObjectType>(Value);
			}
			else if constexpr (VIsPointer<ObjectType> || VIsThreadId<ObjectType>/*|| VIsFloatingPoint<ObjectType>*/)
			{
				return bit_cast<ObjectType>(Value);
			}
			else
			{
				return Value;
			}
		}
		AEON_FORCEINLINE static StorageType CastToStorage_Internal(ObjectType Value) noexcept
		{
			if constexpr (VIsBool<ObjectType> || VIsEnum<ObjectType>)
			{
				return static_cast<StorageType>(Value);
			}
			else if constexpr (VIsPointer<ObjectType> || VIsThreadId<ObjectType>/*|| VIsFloatingPoint<ObjectType>*/)
			{
				return bit_cast<StorageType>(Value);
			}
			else
			{
				return Value;
			}
		}

		std::atomic<StorageType> Storage;

		// NOTE This is used by methods [1] for LinearizableIndexableObjects to determine the actual value of the argument, i.e.,
		//  for int and float types, the value is used as is, but for pointer types,
		//  the value is scaled by the size of the underlying object, to ensure that pointer arithmetic produces the correct value.
		//  Additionally, some of these methods [2] have a parameter of StorageType as opposed to ObjectType
		//  (as seen in most other operations), to ensure that arguments of the correct type are passed to the functions, i.e.,
		//  for int and float types, StorageType is the same as ObjectType, but for pointer types,
		//  StorageType is ptrint, which is ideal for pointer representation.
		//  [1] Increment, Decrement, Add, Subtract, and Fetch<Op> and CompareExchange<Op> variants thereof.
		//  [2] Add, Subtract, and Fetch<Op> and CompareExchange<Op> variants thereof.
		static constexpr StorageType ValueEpsilon()
		{
			return VIsPointer<ObjectType> ? StorageType(Aeon::SizeOf(TRemovePointer<ObjectType>)) : 1;
		}

		static constexpr std::memory_order MemoryOrderConversionMap[8] = {
			// EMemoryOrder::Relaxed	// EMemoryOrder::Acquire	// EMemoryOrder::Release	// EMemoryOrder::AcqRel
			std::memory_order::relaxed, std::memory_order::acquire, std::memory_order::release, std::memory_order::acq_rel,
			// UNUSED					// UNUSED					// UNUSED					// EMemoryOrder::SeqCst
			std::memory_order::seq_cst, std::memory_order::seq_cst, std::memory_order::seq_cst, std::memory_order::seq_cst
		};
	};

	// TODO implement this (alternatively, implement multi-word-cas algorithm)
#if 0
	template <EMemoryOrder DefaultMemoryOrderValue = EMemoryOrder::SeqCst>
	class GDoubleWidthScalar
	{
	public:

	private:
		GAtomicScalar<ptrint> Storage[2];
	};
#endif

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GAtomicFlag

	template <EMemoryOrder DefaultMemoryOrderValue = EMemoryOrder::SeqCst, EMemoryOrder RequiredMemoryOrderValue = EMemoryOrder::Relaxed>
	class GAtomicFlag : protected GAtomicScalar<bool, DefaultMemoryOrderValue, RequiredMemoryOrderValue>
	{
		using Base = GAtomicScalar<bool, DefaultMemoryOrderValue, RequiredMemoryOrderValue>;

		using Base::DefaultLoadMemoryOrderValue;
		using Base::DefaultStoreMemoryOrderValue;

	public:
		GAtomicFlag() = default;

		GAtomicFlag(GAtomicFlag const&) = delete;
		GAtomicFlag(GAtomicFlag&&) = delete;

		template <EMemoryOrder MemoryOrderValue = DefaultLoadMemoryOrderValue>
		bool TestAndSet()
		requires CLoadOnlyMemoryOrder<MemoryOrderValue> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			// NOTE Yeah, technically, AcqRel would be well-defined for this operation,
			//  but the typical usage pattern of this is that the user is only interested in an acquire barrier for TestAndSet,
			//  so we sacrifice a bit of functionality for a bit of syntactic sugar.
			return Base::template Exchange<MemoryOrderValue>(true);
		}

		template <EMemoryOrder MemoryOrderValue = DefaultLoadMemoryOrderValue>
		bool Test() const
		requires CLoadOnlyMemoryOrder<MemoryOrderValue> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			return Base::template Load<MemoryOrderValue>();
		}

		template <EMemoryOrder MemoryOrderValue = DefaultLoadMemoryOrderValue>
		void Wait() const
		requires CLoadOnlyMemoryOrder<MemoryOrderValue> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			Base::template WaitLoad<MemoryOrderValue>(true);
		}

		template <EMemoryOrder MemoryOrderValue = DefaultStoreMemoryOrderValue>
		void Reset()
		requires CStoreOnlyMemoryOrder<MemoryOrderValue> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			Base::template StoreNotify<MemoryOrderValue>(false);
		}

		template <EMemoryOrder MemoryOrderValue = DefaultStoreMemoryOrderValue>
		void ResetAndBroadcast()
		requires CStoreOnlyMemoryOrder<MemoryOrderValue> && CMemoryOrderAsStrictAsRequired<MemoryOrderValue, RequiredMemoryOrderValue>
		{
			Base::template StoreNotifyAll<MemoryOrderValue>(false);
		}
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Common aliases for GAtomicScalar and GAtomicFlag

	// NOTE Always default to the strongest MemoryOrder that you need:
	//  if you don't know what you need, use the ones with the strictest memory order, i.e., SeqCst.

	template <EMemoryOrder DefaultMemoryOrderValue = EMemoryOrder::SeqCst, EMemoryOrder RequiredMemoryOrderValue = DefaultMemoryOrderValue>
	using GAtomicBool = GAtomicScalar<bool, DefaultMemoryOrderValue, RequiredMemoryOrderValue>;

	template <EMemoryOrder DefaultMemoryOrderValue = EMemoryOrder::SeqCst, EMemoryOrder RequiredMemoryOrderValue = DefaultMemoryOrderValue>
	using GAtomicInt8 = GAtomicScalar<int8, DefaultMemoryOrderValue, RequiredMemoryOrderValue>;
	template <EMemoryOrder DefaultMemoryOrderValue = EMemoryOrder::SeqCst, EMemoryOrder RequiredMemoryOrderValue = DefaultMemoryOrderValue>
	using GAtomicInt16 = GAtomicScalar<int16, DefaultMemoryOrderValue, RequiredMemoryOrderValue>;
	template <EMemoryOrder DefaultMemoryOrderValue = EMemoryOrder::SeqCst, EMemoryOrder RequiredMemoryOrderValue = DefaultMemoryOrderValue>
	using GAtomicInt32 = GAtomicScalar<int32, DefaultMemoryOrderValue, RequiredMemoryOrderValue>;
	template <EMemoryOrder DefaultMemoryOrderValue = EMemoryOrder::SeqCst, EMemoryOrder RequiredMemoryOrderValue = DefaultMemoryOrderValue>
	using GAtomicInt64 = GAtomicScalar<int64, DefaultMemoryOrderValue, RequiredMemoryOrderValue>;
//	template <EMemoryOrder DefaultMemoryOrderValue = EMemoryOrder::SeqCst, EMemoryOrder RequiredMemoryOrderValue = DefaultMemoryOrderValue>
//	using GAtomicInt128 = GAtomicScalar<int128, DefaultMemoryOrderValue, RequiredMemoryOrderValue>;

	template <EMemoryOrder DefaultMemoryOrderValue = EMemoryOrder::SeqCst, EMemoryOrder RequiredMemoryOrderValue = DefaultMemoryOrderValue>
	using GAtomicUint8 = GAtomicScalar<uint8, DefaultMemoryOrderValue, RequiredMemoryOrderValue>;
	template <EMemoryOrder DefaultMemoryOrderValue = EMemoryOrder::SeqCst, EMemoryOrder RequiredMemoryOrderValue = DefaultMemoryOrderValue>
	using GAtomicUint16	= GAtomicScalar<uint16, DefaultMemoryOrderValue, RequiredMemoryOrderValue>;
	template <EMemoryOrder DefaultMemoryOrderValue = EMemoryOrder::SeqCst, EMemoryOrder RequiredMemoryOrderValue = DefaultMemoryOrderValue>
	using GAtomicUint32 = GAtomicScalar<uint32, DefaultMemoryOrderValue, RequiredMemoryOrderValue>;
	template <EMemoryOrder DefaultMemoryOrderValue = EMemoryOrder::SeqCst, EMemoryOrder RequiredMemoryOrderValue = DefaultMemoryOrderValue>
	using GAtomicUint64 = GAtomicScalar<uint64, DefaultMemoryOrderValue, RequiredMemoryOrderValue>;
//	template <EMemoryOrder DefaultMemoryOrderValue = EMemoryOrder::SeqCst, EMemoryOrder RequiredMemoryOrderValue = DefaultMemoryOrderValue>
//	using GAtomicUint128 = GAtomicScalar<uint128, DefaultMemoryOrderValue, RequiredMemoryOrderValue>;

	template <EMemoryOrder DefaultMemoryOrderValue = EMemoryOrder::SeqCst, EMemoryOrder RequiredMemoryOrderValue = DefaultMemoryOrderValue>
	using GAtomicFloat32 = GAtomicScalar<float32, DefaultMemoryOrderValue, RequiredMemoryOrderValue>;
	template <EMemoryOrder DefaultMemoryOrderValue = EMemoryOrder::SeqCst, EMemoryOrder RequiredMemoryOrderValue = DefaultMemoryOrderValue>
	using GAtomicFloat64 = GAtomicScalar<float64, DefaultMemoryOrderValue, RequiredMemoryOrderValue>;

	template <CEnum EnumType, EMemoryOrder DefaultMemoryOrderValue = EMemoryOrder::SeqCst,
	    					  EMemoryOrder RequiredMemoryOrderValue = DefaultMemoryOrderValue>
	using GAtomicEnum = GAtomicScalar<EnumType, DefaultMemoryOrderValue, RequiredMemoryOrderValue>;

	template <CObject PointerType, EMemoryOrder DefaultMemoryOrderValue = EMemoryOrder::SeqCst,
	    						   EMemoryOrder RequiredMemoryOrderValue = DefaultMemoryOrderValue>
	using GAtomicPointer = GAtomicScalar<PointerType*, DefaultMemoryOrderValue, RequiredMemoryOrderValue>;

}

#endif