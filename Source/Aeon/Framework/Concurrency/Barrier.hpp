#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONCURRENCY_BARRIER
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONCURRENCY_BARRIER

#include "Aeon/Framework/Core.hpp"

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SBarrier

	class SBarrier
	{
	public:
		explicit SBarrier(uint16 InitState);

		SBarrier(SBarrier const&) = delete;
		SBarrier(SBarrier&&) = delete;

		void Sync();

		void SyncFinal();

		void WaitSync();

	private:
		std::barrier<> Barrier;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SSpinBarrier

	class SSpinBarrier
	{
	public:
		explicit SSpinBarrier(uint16 InitState);

		SSpinBarrier(SSpinBarrier const&) = delete;
		SSpinBarrier(SSpinBarrier&&) = delete;

		void Sync();

		void SyncFinal();

		void WaitSync();

	private:
		GAtomicUint16<EMemoryOrder::AcqRel, EMemoryOrder::Relaxed> BarrierState;
		GAtomicUint16<EMemoryOrder::Relaxed> BarrierInitState;
	};
}

#endif