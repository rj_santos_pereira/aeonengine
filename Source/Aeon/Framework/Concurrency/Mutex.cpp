#include "Aeon/Framework/Concurrency/Mutex.hpp"

#include "Aeon/Framework/Concurrency/Thread.hpp"

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SMutex

	void SMutex::Lock()
	{
		Mutex.lock();
	}

	void SMutex::Unlock()
	{
		Mutex.unlock();
	}

	bool SMutex::TryLock()
	{
		return Mutex.try_lock();
	}

	GScopeLock<SMutex> SMutex::LockScope()
	{
		return GScopeLock<SMutex>{ this };
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SSpinMutex

	void SSpinMutex::Lock()
	{
		do
		{
			while (MutexState.Load<EMemoryOrder::Relaxed>())
			{
				SThread::Yield();
				CompilerFence();
			}
		}
		while (MutexState.Exchange<EMemoryOrder::Acquire>(true));
	}

	void SSpinMutex::Unlock()
	{
		MutexState.Store(false);
	}

	bool SSpinMutex::TryLock()
	{
		return !MutexState.Exchange<EMemoryOrder::Acquire>(true);
	}

	GScopeLock<SSpinMutex> SSpinMutex::LockScope()
	{
		return GScopeLock<SSpinMutex>{ this };
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SCustomMutex

	namespace ZzInternal
	{
		void SCustomMutex::Lock()
		{
			szint CurrSpinCount = SpinCount;
			do
			{
				while (MutexState.Test<EMemoryOrder::Relaxed>())
				{
					if (CurrSpinCount > 0)
					{
						--CurrSpinCount;
					}
					else
					{
						MutexState.Wait<EMemoryOrder::Relaxed>();
					}
				}
			}
			while (MutexState.TestAndSet());
		}

		void SCustomMutex::Unlock()
		{
			MutexState.Reset();
		}

		bool SCustomMutex::TryLock()
		{
			return !MutexState.TestAndSet();
		}

		GScopeLock<SCustomMutex> SCustomMutex::LockScope()
		{
			return GScopeLock<SCustomMutex>{ this };
		}
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SRWMutex

	void SRWMutex::LockAsReader()
	{
		Mutex.lock_shared();
	}

	void SRWMutex::UnlockAsReader()
	{
		Mutex.unlock_shared();
	}

	bool SRWMutex::TryLockAsReader()
	{
		return Mutex.try_lock_shared();
	}

	GScopeReaderLock<SRWMutex> SRWMutex::LockScopeAsReader()
	{
		return GScopeReaderLock<SRWMutex>{ this };
	}

	void SRWMutex::LockAsWriter()
	{
		Mutex.lock();
	}

	void SRWMutex::UnlockAsWriter()
	{
		Mutex.unlock();
	}

	bool SRWMutex::TryLockAsWriter()
	{
		return Mutex.try_lock();
	}

	GScopeWriterLock<SRWMutex> SRWMutex::LockScopeAsWriter()
	{
		return GScopeWriterLock<SRWMutex>{ this };
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SRWSpinMutex

	void SRWSpinMutex::LockAsReader()
	{
		// NOTE It's preferable to initially load outside the loop and use an inner while instead of a do-while,
		//  to allow consecutive cmpxchg when readers are acquiring the mutex.

		uint16 State = MutexState.Load<EMemoryOrder::Relaxed>();
		do
		{
			while (State == EMutexState::Write)
			{
				SThread::Yield();
				CompilerFence();
				State = MutexState.Load<EMemoryOrder::Relaxed>();
			}
		}
		while (!MutexState.CompareExchangeIncrement<EMemoryOrder::Acquire>(State));
	}

	void SRWSpinMutex::UnlockAsReader()
	{
		MutexState.FetchDecrement<EMemoryOrder::Release>();
	}

	bool SRWSpinMutex::TryLockAsReader()
	{
		uint16 State = MutexState.Load<EMemoryOrder::Relaxed>();

		// Worst-case scenario: mutex is writer-locked, but can be released between the load and the cmpxchg
		if (State == EMutexState::Write)
		{
			State = EMutexState::Free;
		}

		return MutexState.CompareExchangeIncrement<EMemoryOrder::Acquire>(State);
	}

	GScopeReaderLock<SRWSpinMutex> SRWSpinMutex::LockScopeAsReader()
	{
		return GScopeReaderLock<SRWSpinMutex>{ this };
	}

	void SRWSpinMutex::LockAsWriter()
	{
		uint16 State = MutexState.Load<EMemoryOrder::Relaxed>();
		do
		{
			while (State != EMutexState::Free)
			{
				SThread::Yield();
				CompilerFence();
				State = MutexState.Load<EMemoryOrder::Relaxed>();
			}
		}
		while (!MutexState.CompareExchange<EMemoryOrder::Acquire>(State, EMutexState::Write));
	}

	void SRWSpinMutex::UnlockAsWriter()
	{
		MutexState.Store(EMutexState::Free);
	}

	bool SRWSpinMutex::TryLockAsWriter()
	{
		uint16 State = MutexState.Load<EMemoryOrder::Relaxed>();

		// Worst-case scenario: mutex is locked, but can be released between the load and the cmpxchg
		if (State != EMutexState::Free)
		{
			State = EMutexState::Free;
		}

		return MutexState.CompareExchange<EMemoryOrder::Acquire>(State, EMutexState::Write);
	}

	GScopeWriterLock<SRWSpinMutex> SRWSpinMutex::LockScopeAsWriter()
	{
		return GScopeWriterLock<SRWSpinMutex>{ this };
	}
}
