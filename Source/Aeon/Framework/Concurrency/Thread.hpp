#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONCURRENCY_THREAD
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONCURRENCY_THREAD

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Concurrency/Future.hpp"

#include "Aeon/Framework/Functional/Invoke.hpp"
#include "Aeon/Framework/Memory/UniquePtr.hpp"

#include "Aeon/Framework/Text/String.hpp"

#include "Aeon/Framework/Time/TimeSpan.hpp"
#include "Aeon/Framework/Time/TimePoint.hpp"

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  EThreadStatus

	AEON_DECLARE_FLAG_ENUM(EThreadStatus, uint8)
	(
		// For a flag 0xABCD, the meaning of each bit is as follows:
		// A - is main thread
		// B - has finished running
		// C - is running
		// D - start/stop/join requested (depending on value of other bits)
		AEON_FLAG_ENUM_RULE_AS_IS,

		AwaitingStart 			= 0b0000,
		Started 				= 0b0001,

		Running 				= 0b0010,
		Running_StopRequested	= 0b0011,

		AwaitingJoin			= 0b0100,
		Joined					= 0b0101,

		MainThread				= 0b1010,
	);

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  DThread

	struct DThreadParams
	{
	//	static constexpr SStringView MainThreadName = "Aeon_MainThread";

		static constexpr SStringView DefaultThreadName = "Aeon_Thread";
		static constexpr uint64 DefaultAffinityMask = AeonMath::MakeBitMask<uint64, 64>();

		// TODO maybe add stack size parameters? what else? (see: pthread_attr_init)
		SString Name = DefaultThreadName;
		uint64 AffinityMask = DefaultAffinityMask;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  SThread

	class SThread
	{
		AEON_DECLARE_TAG_CLASS(ConstructMainThread);

	public:
		static inline thread_id const NullId;

		static SThread InitializeMainThread_Internal()
		{
			AEON_FAILSAFE(MainThreadId == NullId, AeonDebugger::CrashProgram(true, 0), "Re-invoking InitializeMainThread");

			// Initialize static var with current thread's id,
			// This also prevents this method from being called again.
			MainThreadId = std::this_thread::get_id();

			SThread Thread{ DThreadParams{ .Name = "Aeon_MainThread"/*, .AffinityMask = 0x1_u64*/ } };
			// Init thread now, since it is the current running thread.
			Thread.InitializeThreadWithParams_Internal(Thread.Storage->Params);
			// Change status to MainThread
			Thread.Storage->StatusFlag.Store(EThreadStatus::MainThread);
			return Thread;
		}

		explicit SThread(DThreadParams ThreadParams)
			: Storage{ MakeUniquePtr<DThreadStorage>(Move(ThreadParams), EThreadStatus::AwaitingStart) }
		{
		}

		SThread(SThread const&) = delete;
		SThread(SThread&&) = default;

		SThread& operator=(SThread const&) = delete;
		SThread& operator=(SThread&&) = default;

		~SThread()
		{
			if (Storage && !IsMainThread_Internal() && HasStarted())
			{
				Storage->Thread.Destruct();
			}
		}

		template <class FuncType, class ... ArgTypes>
		auto Start(FuncType&& Func, ArgTypes&& ... Args)
		{
			using RetType = TDecayType<decltype(Aeon::Invoke(Forward<FuncType>(Func), Forward<ArgTypes>(Args) ...))>;

			// Ensure we are calling this for the first (and only) time
			AEON_ASSERT(Storage->StatusFlag.Load() == EThreadStatus::AwaitingStart);

			// This lambda is the starting point, does some setup before calling the passed function,
			// and some cleanup after finishing executing the function
			auto ThreadSetupLambda =
				[] (std::stop_token StopToken, GAtomicEnum<EThreadStatus, EMemoryOrder::Relaxed>& StatusFlagRef,
					DThreadParams const& Params, GPromise<RetType> Promise,
					auto&& Func, auto&& ... Args)
				{
					// Busy-wait, we shouldn't spin here for too long, or at all.
					EThreadStatus Status = EThreadStatus::Started;
					while (!StatusFlagRef.CompareExchange<EMemoryOrder::Acquire>(Status, EThreadStatus::Running))
					{
						Status = EThreadStatus::Started;
						Yield();
						CompilerFence();
					}

					// First things first, move the stop_token to the thread_local variable
					// so that the thread can check whenever a stop is requested.
					ThreadStopToken = Move(StopToken);

					// Now initialize/update any thread info with the passed parameters
					InitializeThreadWithParams_Internal(Params);

					// CHECKME should this use set_value_at_thread_exit instead?
					if constexpr (!VIsVoid<RetType>)
					{
						Promise.SetResult(Aeon::Invoke(Forward<FuncType>(Func), Forward<ArgTypes>(Args) ...));
					}
					else
					{
						Aeon::Invoke(Forward<FuncType>(Func), Forward<ArgTypes>(Args) ...);
						Promise.SetResult();
					}

					// No need for release semantics, join() will sync the threads (and GPromise/GFuture syncs the return value if necessary)
					StatusFlagRef.Store(EThreadStatus::AwaitingJoin);
				};

			GFuture<RetType> ThreadResult;

			Storage->Thread.Construct(ThreadSetupLambda, std::ref(Storage->StatusFlag),
									  Storage->Params, ThreadResult.MakePromise(),
									  Forward<FuncType>(Func), Forward<ArgTypes>(Args) ...);


			// Synchronize-with thread that we just created, so that the thread object appears fully constructed to the thread itself.
			// This also informs the destructor that the thread object was constructed and needs to be destroyed.
			Storage->StatusFlag.Store<EMemoryOrder::Release>(EThreadStatus::Started);

			return ThreadResult;
		}

		bool RequestStop()
		{
			EThreadStatus Status = EThreadStatus::Running;
			// Try to set the status flag, if RequestStop was already called before, this will fail;
			// if the thread has not yet started running, or if it has already finished running, this will also fail.
			return Storage->StatusFlag.CompareExchange(Status, EThreadStatus::Running_StopRequested)
			    && Storage->Thread->request_stop();
		}

		void Join()
		{
			if (HasStarted() && Storage->Thread->joinable())
			{
				Storage->Thread->join();
				Storage->StatusFlag.Store(EThreadStatus::Joined);
			}
		}

		bool HasStarted() const
		{
			return Storage->StatusFlag.Load() >= EThreadStatus::Started;
		}

		bool IsRunning() const
		{
			return Aeon::HasAnyFlags(Storage->StatusFlag.Load(), EThreadStatus::Running);
		}

		bool HasStopBeenRequested() const
		{
			return Storage->StatusFlag.Load() == EThreadStatus::Running_StopRequested;
		}

		EThreadStatus Status() const
		{
			return Storage->StatusFlag.Load();
		}

		thread_id Id() const
		{
			return IsMainThread_Internal() ? MainThreadId : (HasStarted() ? Storage->Thread->get_id() : NullId);
		}

		static bool ShouldStop()
		{
			return ThreadStopToken.stop_requested();
		}

		static thread_id ThisId()
		{
			// CHECKME maybe implement this as a sequential custom uint id instead
			return std::this_thread::get_id();
		}

		static void Sleep(STimeSpan const& TimeSpan)
		{
			std::this_thread::sleep_for(TimeSpan.AsNative());
		}
		static void SleepUntil(STimePoint const& TimePoint)
		{
			std::this_thread::sleep_until(TimePoint.AsNative());
		}

		static void Yield()
		{
			std::this_thread::yield();
		}

		static uint32 SystemThreadCount()
		{
			return std::thread::hardware_concurrency();
		}

	private:
		bool IsMainThread_Internal() const
		{
			return Storage->StatusFlag.Load() == EThreadStatus::MainThread;
		}

		static void InitializeThreadWithParams_Internal(DThreadParams const& Params)
		{
			char NameBuffer[16];

			// NOTE Unix has a limitation of 16-character (15 + null term) names for threads,
			//  we're imposing it for all platforms for now
			StringOps::Copy(NameBuffer, Params.Name.CharBuffer(),
							AeonMath::Min<szint>(Params.Name.CharCount(), sizeof NameBuffer - 1));

			// Set thread name, this is platform-specific
#		if AEON_PLATFORM_WINDOWS
			std::wstring_convert<std::codecvt_utf8_utf16<wchar>> NameConverter;

			auto Name_PlatformWin = NameConverter.from_bytes(NameBuffer);

			auto ThreadHandle = ::GetCurrentThread();

			::SetThreadDescription(ThreadHandle, Name_PlatformWin.c_str());
			::SetThreadAffinityMask(ThreadHandle, Params.AffinityMask);
#		else
#			error Not implemented yet!
#		endif
		}

		struct DThreadStorage
		{
			DThreadStorage(DThreadParams&& NewParams, EThreadStatus NewStatus)
				: Params{ NewParams }
				, StatusFlag{ NewStatus }
			{
				AEON_ASSERT(!Params.Name.IsEmpty());
				AEON_ASSERT(Params.AffinityMask != 0);
			}

			GStorage<std::jthread> Thread;
		//	std::stop_token StopToken;

			DThreadParams Params;
			GAtomicEnum<EThreadStatus, EMemoryOrder::Relaxed> StatusFlag;
		};

		// CHECKME Should we make this a shared ptr?
		GUniquePtr<DThreadStorage> Storage;

		thread_local static inline std::stop_token ThreadStopToken;
		// HACK Initialization with NullId is happening after init of MainThread global variable, so just default-init it
		static inline thread_id MainThreadId/* = NullId*/;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  MainThread

	[[maybe_unused]]
	inline SThread const MainThread = SThread::InitializeMainThread_Internal();
}

#endif