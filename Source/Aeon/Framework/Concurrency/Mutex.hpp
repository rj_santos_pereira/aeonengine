#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONCURRENCY_MUTEX
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONCURRENCY_MUTEX

#include "Aeon/Framework/Core.hpp"

//#include "Aeon/Framework/Time/TimeSpan.hpp"

namespace Aeon
{
	// CHECKME measure performance of these primitives on different platforms, compare with custom mutex,
	//  see: https://stackoverflow.com/questions/69990339

	template <class Type>
	concept CLockable = requires
	{
		requires requires (Type Mutex) { { Mutex.Lock() } -> CVoid; };
		requires requires (Type Mutex) { { Mutex.Unlock() } -> CVoid; };
	};

	template <class Type>
	concept CReadLockable = requires
	{
		requires requires (Type Mutex) { { Mutex.LockAsReader() } -> CVoid; };
		requires requires (Type Mutex) { { Mutex.UnlockAsReader() } -> CVoid; };
	};

	template <class Type>
	concept CWriteLockable = requires
	{
		requires requires (Type Mutex) { { Mutex.LockAsWriter() } -> CVoid; };
		requires requires (Type Mutex) { { Mutex.UnlockAsWriter() } -> CVoid; };
	};

	// NOTE Ideally we would constrain the parameter with the Lockable requirements,
	//  but that would cause the declaration of methods such as SMutex::LockScope (et al) to be ill-formed (incomplete type).
	//  Instead, we'll constrain the type on the constructor of these lock types.

	template <class LockableType>
	class GScopeLock;

	template <class LockableType>
	class GScopeReaderLock;

	template <class LockableType>
	class GScopeWriterLock;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SMutex

	class SMutex
	{
	public:
		SMutex() noexcept = default;

		SMutex(SMutex const&) = delete;
		SMutex(SMutex&&) = delete;

		void Lock();

		void Unlock();

		bool TryLock();

		GScopeLock<SMutex> LockScope();

	private:
		std::mutex Mutex;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SSpinMutex

	class SSpinMutex
	{
	public:
		SSpinMutex() noexcept = default;

		SSpinMutex(SSpinMutex const&) = delete;
		SSpinMutex(SSpinMutex&&) = delete;

		void Lock();

		void Unlock();

		bool TryLock();

		GScopeLock<SSpinMutex> LockScope();

	private:
		GAtomicBool<EMemoryOrder::AcqRel, EMemoryOrder::Relaxed> MutexState;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SCustomMutex

	namespace ZzInternal
	{
		class SCustomMutex
		{
		public:
			SCustomMutex() noexcept = default;

			SCustomMutex(SCustomMutex const&) = delete;
			SCustomMutex(SCustomMutex&&) = delete;

			void Lock();

			void Unlock();

			bool TryLock();

			GScopeLock<SCustomMutex> LockScope();

		private:
			GAtomicFlag<EMemoryOrder::AcqRel> MutexState;

			// TODO maybe make this adjustable
			static constexpr szint SpinCount = 4000;
		};
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GScopeLock

	template <class LockableType>
	class GScopeLock
	{
	public:
		explicit GScopeLock(LockableType* NewMutexPtr)
		requires CLockable<LockableType>
		    : MutexPtr(NewMutexPtr)
		{
			AEON_ASSERT(MutexPtr);
			MutexPtr->Lock();
		}

		GScopeLock(GScopeLock const&) = delete;
		GScopeLock(GScopeLock&&) = delete;

		~GScopeLock()
		{
			MutexPtr->Unlock();
		}

	private:
		LockableType* MutexPtr;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SScopeLock, SScopeSpinLock

	using SScopeLock = GScopeLock<SMutex>;
	using SScopeSpinLock = GScopeLock<SSpinMutex>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SRWMutex

	class SRWMutex
	{
	public:
		SRWMutex() noexcept = default;

		SRWMutex(SRWMutex const&) = delete;
		SRWMutex(SRWMutex&&) = delete;

		void LockAsReader();

		void UnlockAsReader();

		bool TryLockAsReader();

		GScopeReaderLock<SRWMutex> LockScopeAsReader();

		void LockAsWriter();

		void UnlockAsWriter();

		bool TryLockAsWriter();

		GScopeWriterLock<SRWMutex> LockScopeAsWriter();

	private:
		std::shared_mutex Mutex;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SRWSpinMutex

	class SRWSpinMutex
	{
	public:
		SRWSpinMutex() noexcept = default;

		SRWSpinMutex(SRWSpinMutex const&) = delete;
		SRWSpinMutex(SRWSpinMutex&&) = delete;

		void LockAsReader();

		void UnlockAsReader();

		bool TryLockAsReader();

		GScopeReaderLock<SRWSpinMutex> LockScopeAsReader();

		void LockAsWriter();

		void UnlockAsWriter();

		bool TryLockAsWriter();

		GScopeWriterLock<SRWSpinMutex> LockScopeAsWriter();

	private:
		struct EMutexState
		{
			enum : uint16
			{
				Free = 0x0,
				Write = 0xFFFF,
			};
		};

		GAtomicUint16<EMemoryOrder::AcqRel, EMemoryOrder::Relaxed> MutexState;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GScopeReaderLock

	template <class LockableType>
	class GScopeReaderLock
	{
	public:
		explicit GScopeReaderLock(LockableType* NewMutexPtr)
		requires CReadLockable<LockableType>
			: MutexPtr(NewMutexPtr)
		{
			AEON_ASSERT(MutexPtr);
			MutexPtr->LockAsReader();
		}

		GScopeReaderLock(GScopeReaderLock const&) = delete;
		GScopeReaderLock(GScopeReaderLock&&) = delete;

		~GScopeReaderLock()
		{
			MutexPtr->UnlockAsReader();
		}

	private:
		LockableType* MutexPtr;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SScopeReaderLock, SScopeReaderSpinLock

	using SScopeReaderLock = GScopeReaderLock<SRWMutex>;
	using SScopeReaderSpinLock = GScopeReaderLock<SRWSpinMutex>;

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GScopeWriterLock

	template <class LockableType>
	class GScopeWriterLock
	{
	public:
		explicit GScopeWriterLock(LockableType* NewMutexPtr)
		requires CWriteLockable<LockableType>
			: MutexPtr(NewMutexPtr)
		{
			AEON_ASSERT(MutexPtr);
			MutexPtr->LockAsWriter();
		}

		GScopeWriterLock(GScopeWriterLock const&) = delete;
		GScopeWriterLock(GScopeWriterLock&&) = delete;

		~GScopeWriterLock()
		{
			MutexPtr->UnlockAsWriter();
		}

	private:
		LockableType* MutexPtr;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SScopeWriterLock, SScopeWriterSpinLock

	using SScopeWriterLock = GScopeWriterLock<SRWMutex>;
	using SScopeWriterSpinLock = GScopeWriterLock<SRWSpinMutex>;

}

#endif