#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONCURRENCY_FUTURE
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONCURRENCY_FUTURE

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Memory/SharedPtr.hpp"

namespace Aeon
{
	AEON_DECLARE_TAG_CLASS(TDeferInit); // TODO move this elsewhere?

	namespace ZzInternal
	{
		AEON_DECLARE_FLAG_ENUM(EFutureState, uint8)
		(
			AEON_FLAG_ENUM_RULE_AS_IS,
			NotReady	= 0b00,
			Ready		= 0b01,
			Ready_Empty	= 0b11
		);

		template <CObjectElseVoid ObjType>
		class alignas_atomic() GFutureState
		{
		public:
			using ResultRefType = ObjType const&;
			using ResultType = ObjType;

			GFutureState()
				: ResultState(EFutureState::NotReady)
				, ResultPromised(false)
			{
			}

			~GFutureState()
			{
				// CHECKME Even though in theory, the SharedPtr that holds this object already synchronizes the ResultStorage value,
				//  we still need to do a load-acquire here, because otherwise (if relaxed) there would be no way of ensuring that the load
				//  would return the most up-to-date value, which could cause the object to not be destroyed.
				if (ResultState.Load())
				{
					ResultStorage.Destruct();
				}
			}

			// This does not impose a synchronization point between threads
			bool HasResult() const
			{
				return ResultState.Load<EMemoryOrder::Relaxed>();
			}

			// This does not impose a synchronization point between threads
			void WaitResult() const
			{
				AEON_ASSERT(ResultPromised.Load());

				// NOTE This can have MemoryOrder::Relaxed,
				//  since if this observes a 'Ready' value, a subsequent GetResult does a load-acquire to sync the result,
				//  and read-read coherence guarantees that we will see the most up-to-date value (that is, the 'Ready' value).
				AEON_EVAL_ASSERT(ResultState.WaitLoad<EMemoryOrder::Relaxed>(EFutureState::NotReady));
			}

			ResultRefType GetResult() const
			{
				AEON_ASSERT(ResultPromised.Load());

				AEON_EVAL_ASSERT(ResultState.WaitLoad(EFutureState::NotReady));

				AEON_FAILSAFE(ResultState.Load<EMemoryOrder::Relaxed>() == EFutureState::Ready,
							  void(),
							  "GFuture: Trying to obtain an already-obtained (empty) future!");

				return *ResultStorage;
			}
			ResultType TakeResult()
			requires CMoveable<ResultType>
			{
				AEON_ASSERT(ResultPromised.Load());

				AEON_EVAL_ASSERT(ResultState.WaitLoad(EFutureState::NotReady));

				// CHECKME Should this be AcqRel?
				AEON_FAILSAFE(ResultState.Exchange<EMemoryOrder::Relaxed>(EFutureState::Ready_Empty) == EFutureState::Ready,
							  void(),
							  "GFuture: Trying to obtain an already-obtained (empty) future!");

				return Move(*ResultStorage);
			}

			template <class NewObjType>
			void SetResult(NewObjType&& NewResult)
			{
				AEON_FAILSAFE(!ResultState.Load<EMemoryOrder::Relaxed>(),
							  return,
							  "GPromise: Trying to fulfill an already-fulfilled promise!");

				ResultStorage.Construct(Forward<NewObjType>(NewResult));

				ResultState.StoreNotifyAll(EFutureState::Ready);
			}

			bool MarkResultPromised()
			{
				return !ResultPromised.Exchange(true);
			}

		private:
			GStorage<ObjType> ResultStorage;
			GAtomicUint8<EMemoryOrder::AcqRel, EMemoryOrder::Relaxed> ResultState;
			GAtomicBool<EMemoryOrder::Relaxed> ResultPromised;
		};

		template <>
		class alignas_atomic() GFutureState<void>
		{
		public:
			using ResultRefType = void;
			using ResultType = void;

			GFutureState()
				: ResultState(false)
				, ResultPromised(false)
			{
			}

			// This does not impose a synchronization point between threads
			bool HasResult() const
			{
				return ResultState.Load<EMemoryOrder::Relaxed>();
			}

			// This does not impose a synchronization point between threads
			void WaitResult() const
			{
				AEON_ASSERT(ResultPromised.Load());

				AEON_EVAL_ASSERT(ResultState.WaitLoad<EMemoryOrder::Relaxed>(EFutureState::NotReady));
			}

			void GetResult() const
			{
				AEON_ASSERT(ResultPromised.Load());

				AEON_EVAL_ASSERT(ResultState.WaitLoad(EFutureState::NotReady));
			}
			void TakeResult()
			{
				GetResult();
			}

			void SetResult()
			{
				AEON_FAILSAFE(!ResultState.Load<EMemoryOrder::Relaxed>(),
							  return,
							  "GPromise: Trying to fulfill an already-fulfilled promise!");

				ResultState.StoreNotifyAll(EFutureState::Ready);
			}

			bool MarkResultPromised()
			{
				return !ResultPromised.Exchange(true);
			}

		private:
			GAtomicBool<EMemoryOrder::AcqRel, EMemoryOrder::Relaxed> ResultState;
			GAtomicBool<EMemoryOrder::Relaxed> ResultPromised;
		};
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GFuture

	template <CObjectElseVoid ObjType>
	class GPromise;

	template <CObjectElseVoid ObjType>
	class GFuture
	{
		AEON_DECLARE_TAG_CLASS(ConstructWithState);

		using StateType = ZzInternal::GFutureState<ObjType>;

	public:
		using ObjectType = ObjType;
		using ResultRefType = typename StateType::ResultRefType;
		using ResultType = typename StateType::ResultType;

		GFuture()
			: State(Aeon::MakeSharedPtr<StateType>())
		{
		}
		explicit GFuture(TDeferInit)
			: State()
		{
		}

		explicit GFuture(ConstructWithState, GSharedPtr<StateType> const& NewState)
			: State(NewState)
		{
		}

		GFuture(GFuture const& Other)
			: State(Other.State)
		{
		}
		GFuture(GFuture&& Other)
			: State(Move(Other.State))
		{
		}

		GFuture& operator=(GFuture Other)
		{
			Swap(State, Other.State);
			return *this;
		}

		bool HasResult() const
		{
			return State->HasResult();
		}

		void WaitForResult() const
		{
			return State->WaitResult();
		}

		// TODO consider implementing these

#if 0
		void WaitForResult(STimeSpan const& TimeSpan)
		{

		}

		void WaitForResult(STimePoint const& TimePoint)
		{

		}
#endif

		ResultRefType GetResult() const
		{
			return State->GetResult();
		}

		ResultType TakeResult()
		{
			return State->TakeResult();
		}

		// TODO consider implementing continuations (i.e. std::experimental::future::then)

		bool IsValid() const
		{
			return State;
		}

		GPromise<ObjectType> MakePromise();

	private:
		GSharedPtr<StateType> State;

		friend class GPromise<ObjectType>;
	};
	
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  GPromise

	template <CObjectElseVoid ObjType>
	class GPromise
	{
		AEON_DECLARE_TAG_CLASS(ConstructWithState);

		using StateType = ZzInternal::GFutureState<ObjType>;

	public:
		using ObjectType = ObjType;

		GPromise()
			: State(Aeon::MakeSharedPtr<StateType>())
		{
			State->MarkResultPromised();
		}
		explicit GPromise(TDeferInit)
			: State()
		{
		}

		explicit GPromise(ConstructWithState, GSharedPtr<StateType> const& NewState)
			: State(NewState)
		{
		}

		GPromise(GPromise const&)
			= delete;
		GPromise(GPromise&& Other)
			: State(Move(Other.State))
		{
		}

		~GPromise()
		{
			if (State)
			{
				AEON_FAILSAFE(State->HasResult(), void(), "Destroying promise with pending result!");
			}
		}

		GPromise& operator=(GPromise Other)
		{
			Swap(State, Other.State);
			return *this;
		}

		template <class NewObjectType>
		void SetResult(NewObjectType&& Object)
		requires CNotVoid<ObjectType>
		{
			State->SetResult(Forward<NewObjectType>(Object));
		}
		void SetResult()
		requires CVoid<ObjectType>
		{
			State->SetResult();
		}

		bool IsValid() const
		{
			return State;
		}

		GFuture<ObjType> MakeFuture() const;

	private:
		GSharedPtr<StateType> State;

		friend class GFuture<ObjectType>;
	};

	template <CObjectElseVoid ObjType>
	GPromise<ObjType> GFuture<ObjType>::MakePromise()
	{
		using PromiseType = GPromise<ObjType>;

		AEON_FAILSAFE(State->MarkResultPromised(), return PromiseType{}, "Trying to re-make a promise!");

		return PromiseType(PromiseType::ConstructWithState::Tag, State);
	}

	template <CObjectElseVoid ObjType>
	GFuture<ObjType> GPromise<ObjType>::MakeFuture() const
	{
		using FutureType = GFuture<ObjType>;
		return FutureType(FutureType::ConstructWithState::Tag, State);
	}
}

#endif