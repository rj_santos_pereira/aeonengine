#include "Aeon/Framework/Concurrency/Semaphore.hpp"

#include "Aeon/Framework/Concurrency/Thread.hpp"

namespace Aeon
{

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SSemaphore

	SSemaphore::SSemaphore(uint16 InitState)
		: Semaphore(InitState)
	{
	}

	void SSemaphore::Acquire()
	{
		return Semaphore.acquire();
	}

	void SSemaphore::Release(uint16 StateUpdate)
	{
		return Semaphore.release(StateUpdate);
	}

	bool SSemaphore::TryAcquire()
	{
		return Semaphore.try_acquire();
	}

	bool SSemaphore::TryAcquire(STimeSpan const& TimeSpan)
	{
		return Semaphore.try_acquire_for(TimeSpan.AsNative());
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SSpinSemaphore

	SSpinSemaphore::SSpinSemaphore(uint16 InitState)
		: SemaphoreState(InitState)
	{
	}

	void SSpinSemaphore::Acquire()
	{
		uint16 State = SemaphoreState.Load<EMemoryOrder::Relaxed>();
		do
		{
			while (State == 0)
			{
				SThread::Yield();
				CompilerFence();
				State = SemaphoreState.Load<EMemoryOrder::Relaxed>();
			}
		}
		while (!SemaphoreState.CompareExchangeDecrement<EMemoryOrder::Acquire>(State));
	}

	void SSpinSemaphore::Release(uint16 StateUpdate)
	{
		AEON_ASSERT(StateUpdate > 0);

		SemaphoreState.FetchAdd<EMemoryOrder::Release>(StateUpdate);
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SCustomSemaphore

	namespace ZzInternal
	{
		SCustomSemaphore::SCustomSemaphore(uint16 InitState)
			: SemaphoreState(InitState)
			, AwaitCounter(0)
		{
		}

		void SCustomSemaphore::Acquire()
		{
			// Start with an optimistic value, which will be the best-case path.
			uint16 State = 1;
			while (!SemaphoreState.CompareExchangeDecrement<EMemoryOrder::Acquire>(State)) // CHECKME should this be Acquire?
			{
				// If we couldn't obtain the semaphore, and the current value is zero,
				// then we can't decrement until some other thread calls Release,
				// so "hurry up and wait".
				while (State == 0)
				{
					// Mark this thread as a sleeper
					AwaitCounter.FetchIncrement();

					// CHECKME can this be Relaxed? at least on some archs (see StoreLoad_Reordering bench)?
					State = SemaphoreState.WaitLoad(0);

					// Unmark this thread as a sleeper
					AwaitCounter.FetchDecrement<EMemoryOrder::Relaxed>();
				}
				// Otherwise (or if some thread called Release), the current value is NOT zero,
				// so we can simply try again, with the hopefully up-to-date value.
			}
		}

		void SCustomSemaphore::Release(uint16 StateUpdate)
		{
			AEON_ASSERT(StateUpdate > 0);

			SemaphoreState.FetchAdd(StateUpdate);

			// Check how many sleeper threads we have.
			// CHECKME can this be Relaxed? at least on some archs (see StoreLoad_Reordering bench)?
			uint16 AwaitCount = AwaitCounter.Load();

			// If there are more threads waiting than there are available run slots,
			// notify just the number of threads equal to the number of slots that just became available.
			if (StateUpdate < AwaitCount)
			{
				while (StateUpdate-- > 0)
				{
					SemaphoreState.Notify();
				}
			}
			// Otherwise, notify every thread, if there is anyone sleeping.
			else if (AwaitCount > 0)
			{
				SemaphoreState.NotifyAll();
			}
		}
	}
}
