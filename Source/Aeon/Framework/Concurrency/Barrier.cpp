#include "Aeon/Framework/Concurrency/Barrier.hpp"

#include "Aeon/Framework/Concurrency/Thread.hpp"

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SBarrier

	SBarrier::SBarrier(uint16 InitState)
		: Barrier(InitState)
	{
		AEON_ASSERT(InitState != 0);
	}

	void SBarrier::Sync()
	{
		static_cast<void>(Barrier.arrive());
	}

	void SBarrier::SyncFinal()
	{
		Barrier.arrive_and_drop();
	}

	void SBarrier::WaitSync()
	{
		Barrier.arrive_and_wait();
	}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SSpinBarrier

	SSpinBarrier::SSpinBarrier(uint16 InitState)
		: BarrierState(InitState)
		, BarrierInitState(InitState)
	{
		AEON_ASSERT(InitState != 0);
	}

	void SSpinBarrier::Sync()
	{
		if (BarrierState.FetchDecrement() == 0)
		{
			BarrierState.Store(BarrierInitState.Load());
		}
	}

	void SSpinBarrier::SyncFinal()
	{
		// CHECKME ensure this is correct

		// NOTE This can be Relaxed because BarrierState will sync it for the subsequent load,
		//  even if another thread does this in the meanwhile
		AEON_EVAL_ASSERT(BarrierInitState.FetchDecrement() != 0);

		Sync();
	}

	void SSpinBarrier::WaitSync()
	{
		if (BarrierState.FetchDecrement() == 0)
		{
			BarrierState.Store(BarrierInitState.Load());
		}
		else
		{
			// Wait for all threads to sync
			while (BarrierState.Load<EMemoryOrder::Relaxed>() != 0)
			{
				SThread::Yield();
				CompilerFence();
			}

			// Wait for last thread to restore the barrier state with the init value (which may have been changed)
			while (BarrierState.Load() == 0)
			{
				SThread::Yield();
				CompilerFence();
			}
		}
	}
}
