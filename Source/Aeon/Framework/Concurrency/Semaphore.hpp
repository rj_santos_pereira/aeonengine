#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONCURRENCY_SEMAPHORE
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_CONCURRENCY_SEMAPHORE

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Time/TimeSpan.hpp"

// CHECKME measure performance of these primitives on different platforms,
//  see: https://stackoverflow.com/questions/69990339

namespace Aeon
{
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SSemaphore

	class SSemaphore
	{
		// NOTE Keep this under uint16 max, because std::counting_semaphore uses std::ptrdiff_t,
		//  and "the bit width of std::ptrdiff_t is not less than 17", per the standard.
		static constexpr uint16 SemaphoreLimit = TNumericProperties<uint16>::Maximum;

	public:
		explicit SSemaphore(uint16 InitState = 0);

		SSemaphore(SSemaphore const&) = delete;
		SSemaphore(SSemaphore&&) = delete;

		void Acquire();

		void Release(uint16 StateUpdate = 1);

		bool TryAcquire();

		bool TryAcquire(STimeSpan const& TimeSpan);

	private:
		std::counting_semaphore<SemaphoreLimit> Semaphore;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SSpinSemaphore

	class SSpinSemaphore
	{
	public:
		explicit SSpinSemaphore(uint16 InitState = 0);

		SSpinSemaphore(SSpinSemaphore const&) = delete;
		SSpinSemaphore(SSpinSemaphore&&) = delete;

		void Acquire();

		void Release(uint16 StateUpdate = 1);

	private:
		// Number of threads that can obtain the semaphore
		GAtomicUint16<EMemoryOrder::AcqRel, EMemoryOrder::Relaxed> SemaphoreState;
	};

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SCustomSemaphore

	namespace ZzInternal
	{
		class SCustomSemaphore
		{
		public:
			explicit SCustomSemaphore(uint16 InitState = 0);

			SCustomSemaphore(SCustomSemaphore const&) = delete;
			SCustomSemaphore(SCustomSemaphore&&) = delete;

			void Acquire();

			void Release(uint16 StateUpdate = 1);

		private:
			// Number of threads that can obtain the semaphore
			GAtomicUint16<EMemoryOrder::SeqCst, EMemoryOrder::AcqRel> SemaphoreState;
			// Number of threads waiting for the semaphore to be available
			GAtomicUint16<EMemoryOrder::SeqCst, EMemoryOrder::Relaxed> AwaitCounter;
		};
	}
}

#endif