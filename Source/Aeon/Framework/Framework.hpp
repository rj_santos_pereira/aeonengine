#ifndef ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_FRAMEWORK
#define ZZINTERNAL_INCLUDE_AEON_FRAMEWORK_FRAMEWORK

// This header coalesces all Framework headers useful for development
// TODO make this a PCH

#include "Aeon/Framework/Core.hpp"

#include "Aeon/Framework/Allocator/DynamicAllocator.hpp"
#include "Aeon/Framework/Allocator/StaticAllocator.hpp"
//#include "Aeon/Framework/Allocator/FixedAllocator.hpp"

#include "Aeon/Framework/Concurrency/Future.hpp"
#include "Aeon/Framework/Concurrency/Thread.hpp"
#include "Aeon/Framework/Concurrency/Mutex.hpp"
#include "Aeon/Framework/Concurrency/Semaphore.hpp"
#include "Aeon/Framework/Concurrency/Barrier.hpp"

#include "Aeon/Framework/Container/Iterator.hpp"
#include "Aeon/Framework/Container/Array.hpp"
#include "Aeon/Framework/Container/Dearray.hpp"
#include "Aeon/Framework/Container/Stack.hpp"
#include "Aeon/Framework/Container/Queue.hpp"
#include "Aeon/Framework/Container/ConcurrentQueue.hpp"
//#include "Aeon/Framework/Container/List.hpp"
#include "Aeon/Framework/Container/HashMap.hpp"
#include "Aeon/Framework/Container/HashSet.hpp"

#include "Aeon/Framework/Container/Tuple/Duple.hpp"
#include "Aeon/Framework/Container/Tuple/Triple.hpp"
#include "Aeon/Framework/Container/Tuple/Tuple.hpp"

#include "Aeon/Framework/Functional/Optional.hpp"
#include "Aeon/Framework/Functional/Relational.hpp"
#include "Aeon/Framework/Functional/Invoke.hpp"
#include "Aeon/Framework/Functional/BindInvoke.hpp"
#include "Aeon/Framework/Functional/Function.hpp"
#include "Aeon/Framework/Functional/Delegate.hpp"

#include "Aeon/Framework/Filesystem/Path.hpp"
#include "Aeon/Framework/Filesystem/Directory.hpp"
#include "Aeon/Framework/Filesystem/File.hpp"

#include "Aeon/Framework/Memory/ThriftyPtr.hpp"
//#include "Aeon/Framework/Memory/StaticUniquePtr.hpp"
#include "Aeon/Framework/Memory/UniquePtr.hpp"
#include "Aeon/Framework/Memory/SharedPtr.hpp"
#include "Aeon/Framework/Memory/WeakSharedPtr.hpp"

#include "Aeon/Framework/Math/Random/RandomGenerator.hpp"

#include "Aeon/Framework/Math/Vector.hpp"
#include "Aeon/Framework/Math/Rotation.hpp"
#include "Aeon/Framework/Math/Quaternion.hpp"
#include "Aeon/Framework/Math/Hyperplane.hpp"
#include "Aeon/Framework/Math/Matrix.hpp"

#include "Aeon/Framework/Text/StringView.hpp"
#include "Aeon/Framework/Text/String.hpp"

#include "Aeon/Framework/Time/Timezone.hpp"
#include "Aeon/Framework/Time/TimezoneDatabase.hpp"
#include "Aeon/Framework/Time/TimeSpan.hpp"
#include "Aeon/Framework/Time/TimePoint.hpp"
#include "Aeon/Framework/Time/DateTime.hpp"
#include "Aeon/Framework/Time/Stopwatch.hpp"

// TODO add Locale

#endif