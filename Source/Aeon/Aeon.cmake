
#target_sources(${PROJECT_NAME}
#    PUBLIC
#        ${CMAKE_CURRENT_SOURCE_DIR}/CoreFramework.hpp
#        ${CMAKE_CURRENT_SOURCE_DIR}/CoreLibrary.hpp
#    )

set(AEON_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR})

add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Engine)
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Framework)