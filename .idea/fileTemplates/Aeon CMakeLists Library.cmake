project(${NAME} VERSION XXX LANGUAGES XXX)

message(STATUS "Library: #[[${PROJECT_NAME}]]#")

#set( $LIB_FILE = $NAME.toUpperCase() + "_LIB_FILE" )
#set( $IMPLIB_FILE = $NAME.toUpperCase() + "_IMPLIB_FILE" )

add_library(#[[${PROJECT_NAME}]]# STATIC IMPORTED GLOBAL)
#[[#]]# add_library(#[[${PROJECT_NAME}]]# SHARED IMPORTED GLOBAL)

if (CMAKE_BUILD_TYPE MATCHES Debug)
	set($LIB_FILE xxxd.lib)
#[[#]]#	set($IMPLIB_FILE xxxd.lib)
else()
	set($LIB_FILE xxx.lib)
#[[#]]#	set($IMPLIB_FILE xxx.lib)
endif()

set_target_properties(
	#[[${PROJECT_NAME}]]# PROPERTIES
	INTERFACE_INCLUDE_DIRECTORIES "#[[${CMAKE_CURRENT_SOURCE_DIR}]]#/include"
#[[#]]#	INTERFACE_COMPILE_DEFINITIONS ""
	IMPORTED_LOCATION "#[[${CMAKE_CURRENT_SOURCE_DIR}]]#/lib/#[[${]]#$LIB_FILE#[[}]]#"
#[[#]]#	IMPORTED_IMPLIB "#[[${CMAKE_CURRENT_SOURCE_DIR}]]#/lib/#[[${]]#$IMPLIB_FILE#[[}]]#"
)
