$DefaultSystemLocale = Get-WinSystemLocale

#$SystemLocaleList = @([System.Globalization.CultureInfo]::new("en-US"))
$SystemLocaleList = Get-WinUserLanguageList | ForEach-Object { $_.LanguageTag }`
											| ForEach-Object { [System.Globalization.CultureInfo]::new($_) }

$HasDefault = $DefaultSystemLocale -in $SystemLocaleList	
if (-not $HasDefault)
{
	Write-Warning "Could not find the default locale in the list of system locales, adding it to the list"
	
	$SystemLocaleList += $DefaultSystemLocale
}

# Sort the list so that the default system locale is the first element
$SystemLocaleList = $SystemLocaleList | Sort-Object -Unique `
									  | Sort-Object { $_.LCID -ne $DefaultSystemLocale.LCID }`
									  | ForEach-Object { $_.Name }

Write-Output "Listing the system locales:"
Write-Output $SystemLocaleList

Write-Warning "Caching list in SystemLocales.txt"
Set-Content -Path ./Resource/SystemLocales.txt -Value $SystemLocaleList
