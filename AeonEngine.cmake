project(AeonEngine CXX)

message(STATUS "Platform name: ${CMAKE_SYSTEM_NAME}")
message(STATUS "Compiler: ${CMAKE_CXX_COMPILER_ID} ${CMAKE_CXX_COMPILER_VERSION}")
message(STATUS "Compiler driver: ${CMAKE_CXX_COMPILER_FRONTEND_VARIANT}")

# Obtain platform info

# Compute project SLOC:
# git ls-files | grep -E "(.cpp|.hpp|.cmake)" | grep -vE "(External)" | xargs wc -l

set(AEON_PLATFORM_NAME ${CMAKE_SYSTEM_NAME})

set(AEON_PLATFORM_WINDOWS OFF)
set(AEON_PLATFORM_LINUX OFF)
set(AEON_PLATFORM_MACOS OFF)
set(AEON_PLATFORM_ANDROID OFF)
set(AEON_PLATFORM_IOS OFF)

if(CMAKE_SYSTEM_NAME STREQUAL "Windows")
    set(AEON_PLATFORM_WINDOWS ON)
elseif(CMAKE_SYSTEM_NAME STREQUAL "Linux")
    set(AEON_PLATFORM_LINUX ON)
elseif(CMAKE_SYSTEM_NAME STREQUAL "Darwin")
    set(AEON_PLATFORM_MACOS ON)
elseif(CMAKE_SYSTEM_NAME STREQUAL "Android")
    message(FATAL_ERROR "Unsupported platform!") # FIXME uses the same as Linux, need extra logic to distinguish
    set(AEON_PLATFORM_ANDROID ON)
elseif(CMAKE_SYSTEM_NAME STREQUAL "iOS")
    message(FATAL_ERROR "Unsupported platform!") # FIXME uses the same as macOS, need extra logic to distinguish
    set(AEON_PLATFORM_IOS ON)
else()
    message(FATAL_ERROR "Unsupported platform!")
endif()

# Obtain compiler info

set(AEON_COMPILER_MSVC OFF)
set(AEON_COMPILER_CLANG OFF)
set(AEON_COMPILER_GCC OFF)
set(AEON_COMPILER_ICC OFF)

set(AEON_COMPILER_DRIVER_MSVC OFF)
set(AEON_COMPILER_DRIVER_GNU OFF)

if(CMAKE_CXX_COMPILER_ID MATCHES "MSVC")
    set(AEON_COMPILER_MSVC ON)
    set(AEON_COMPILER_DRIVER_MSVC ON)
elseif(CMAKE_CXX_COMPILER_ID MATCHES "Clang")
    # See https://stackoverflow.com/a/49483785 and the second comment
    set(AEON_COMPILER_CLANG ON)
    if(CMAKE_CXX_COMPILER_FRONTEND_VARIANT MATCHES "MSVC")
        set(AEON_COMPILER_DRIVER_MSVC ON)
    else()
        set(AEON_COMPILER_DRIVER_GNU ON)
    endif()
elseif(CMAKE_CXX_COMPILER_ID MATCHES "GNU")
    set(AEON_COMPILER_GCC ON)
    set(AEON_COMPILER_DRIVER_GNU ON)
elseif(CMAKE_CXX_COMPILER_ID MATCHES "Intel")
    set(AEON_COMPILER_ICC ON)
    # Intel compiler is weird, and accepts either one or both depending on the flag
    # See https://www.intel.com/content/www/us/en/docs/dpcpp-cpp-compiler/developer-guide-reference/2023-0/compiler-options.html
    # and e.g.: https://www.intel.com/content/www/us/en/docs/dpcpp-cpp-compiler/developer-guide-reference/2023-0/zp.html
    set(AEON_COMPILER_DRIVER_MSVC ON)
    set(AEON_COMPILER_DRIVER_GNU ON)
else()
    message(FATAL_ERROR "Unsupported compiler!")
endif()

# Obtain build info

set(AEON_BUILD_LOG_LEVEL -1 CACHE STRING "Default log level of the build")
if(CMAKE_BUILD_TYPE STREQUAL Debug)
    message(STATUS "Build type: Development")
    set(AEON_BUILD_TYPE "Development")
    add_compile_definitions (AEON_BUILD_DEVELOPMENT=1 AEON_BUILD_DISTRIBUTION_DEBUG=0 AEON_BUILD_DISTRIBUTION=0)
    if(AEON_BUILD_LOG_LEVEL LESS 0)
        set(AEON_BUILD_LOG_LEVEL 3)
    endif()
elseif(CMAKE_BUILD_TYPE STREQUAL RelWithDebInfo)
    message(STATUS "Build type: Distribution (Debug)")
    set(AEON_BUILD_TYPE "DistributionDebug")
    add_compile_definitions (AEON_BUILD_DEVELOPMENT=0 AEON_BUILD_DISTRIBUTION_DEBUG=1 AEON_BUILD_DISTRIBUTION=1)
    if(AEON_BUILD_LOG_LEVEL LESS 0)
        set(AEON_BUILD_LOG_LEVEL 3)
    endif()
elseif(CMAKE_BUILD_TYPE STREQUAL Release)
    message(STATUS "Build type: Distribution")
    set(AEON_BUILD_TYPE "Distribution")
    add_compile_definitions (AEON_BUILD_DEVELOPMENT=0 AEON_BUILD_DISTRIBUTION_DEBUG=0 AEON_BUILD_DISTRIBUTION=1)
    if(AEON_BUILD_LOG_LEVEL LESS 0)
        set(AEON_BUILD_LOG_LEVEL 2)
    endif()
else()
    message(WARNING "Unknown build type")
endif()
add_compile_definitions (AEON_BUILD_LOG_LEVEL=${AEON_BUILD_LOG_LEVEL})
message (STATUS "AEON_BUILD_LOG_LEVEL=${AEON_BUILD_LOG_LEVEL}")

# Make copy of all build flags, this may be useful whenever we want to change anything later

set(AEON_CXX_FLAGS_DEV ${CMAKE_CXX_FLAGS_DEBUG})
set(AEON_CXX_FLAGS_PROD_DBG ${CMAKE_CXX_FLAGS_RELWITHDEBINFO})
set(AEON_CXX_FLAGS_PROD ${CMAKE_CXX_FLAGS_RELEASE})

#set(USING_CLION_IDE CACHE BOOL OFF)
#if (${USING_CLION_IDE})
#    add_compile_definitions(AEON_USING_CLION_IDE=1)
#else()
#    add_compile_definitions(AEON_USING_CLION_IDE=0)
#endif()

set(AEON_USING_FMT_LIBRARY ON CACHE BOOL "Whether the fmt library should be used instead of standard library <format>")
if (${AEON_USING_FMT_LIBRARY})
    add_compile_definitions(AEON_USING_FMT_LIBRARY=1)
else()
    add_compile_definitions(AEON_USING_FMT_LIBRARY=0)
endif()

set(AEON_USING_GLFW_LIBRARY ON CACHE BOOL "Whether the GLFW windowing library should be used instead of some other platform-specific alternative")
if (${AEON_USING_GLFW_LIBRARY})
    add_compile_definitions(AEON_USING_GLFW_LIBRARY=1)
else()
    add_compile_definitions(AEON_USING_GLFW_LIBRARY=0)
endif()

#add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Config)
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Source)
#add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Resource)
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Tests)
#add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Tools)
